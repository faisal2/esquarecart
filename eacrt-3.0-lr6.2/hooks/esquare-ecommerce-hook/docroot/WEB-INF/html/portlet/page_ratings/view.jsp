<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portlet/page_ratings/init.jsp" %>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%

String currenturl = themeDisplay.getURLCurrent();
String className = Layout.class.getName();
long classPK = layout.getPlid();

if (currenturl.contains("_userView_WAR_esquareecommerceportlet_curProductId=")) {
	String curProductId = currenturl.substring(currenturl.lastIndexOf("_userView_WAR_esquareecommerceportlet_curProductId="));
	curProductId = curProductId.substring((curProductId.indexOf("=")+1), curProductId.indexOf("&"));
	className = String.valueOf(ClassNameLocalServiceUtil.getClassNameId("com.esquare.ecommerce.model.ProductDetails"));
	classPK = Long.parseLong(curProductId);
}

%>

<liferay-ui:ratings
	className="<%= className %>"
	classPK="<%= classPK %>"
/>
