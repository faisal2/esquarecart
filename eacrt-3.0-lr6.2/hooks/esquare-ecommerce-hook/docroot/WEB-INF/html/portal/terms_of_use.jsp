<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portal/init.jsp" %>
<%@ page import="com.liferay.portal.model.User"%>
<script src="/html/js/liferay/service.js" type="text/javascript"></script>
<%
themeDisplay.setIncludePortletCssJs(true);
%>
<%
String star = "&nbsp;<font color='red' style='font-size: medium;'>*</font>";
String currentURL = PortalUtil.getCurrentURL(request);
String referer = ParamUtil.getString(request, WebKeys.REFERER, currentURL);
if (referer.equals(themeDisplay.getPathMain() + "/portal/update_terms_of_use")) {
	referer = themeDisplay.getPathMain() + "?doAsUserId=" + themeDisplay.getDoAsUserId();	
}
String firstName = themeDisplay.getUser().getFirstName();
String lastName =  themeDisplay.getUser().getLastName();
%>
<div class="myprofile_heading"> My Profile </div>
<div  id="myprofile_div" class="ecart-normfontfam">
<aui:form action='<%= themeDisplay.getPathMain() + "/portal/update_terms_of_use" %>' name="fm" class="ecart-normfontfam">
	<aui:input name="doAsUserId" type="hidden" value="<%= themeDisplay.getDoAsUserId() %>" />
	<aui:input name="<%= WebKeys.REFERER %>" type="hidden" value="<%= referer %>" />

	<aui:fieldset>
		<div class="row-fluid">
			<div class="span4 offset2">
			<aui:input   name="firstName" value="<%=firstName%>" showRequiredLabel="false" cssClass="myprofile_fields" label='First Name<span style="color:red;font-size: 14px;">*</span>'>
				<aui:validator  name="required" />
				<aui:validator name="alpha" />
				<aui:validator name="maxLength" >'15'</aui:validator>
			</aui:input>	
			<aui:input label='Last Name<span style="color:red;font-size: 14px;">*</span>'  name="lastName" value="<%=lastName%>" showRequiredLabel="false" cssClass="myprofile_fields">
				<aui:validator  name="required" />
				<aui:validator name="alpha" />
				<aui:validator name="maxLength" >'15'</aui:validator>
			</aui:input>
					
					<aui:input label='Mobile<span style="color:red;font-size: 14px;">*</span>'  name="mobile" showRequiredLabel="false" cssClass="myprofile_fields">
				<aui:validator name="required" />
				<aui:validator name="digits" />
				<aui:validator name="rangeLength" >[8,15]</aui:validator>		
			</aui:input>
			
				
					
					
					
						<aui:select label='Country<span style="color:red;font-size: 14px;">*</span>' name='countryId' />
					
													
		</div>
									
			
		<div class="span6">			
						
						
						<aui:input label='Street Address<span style="color:red;font-size: 14px;">*</span>'  name="street" showRequiredLabel="false" cssClass="myprofile_fields">
				<aui:validator name="required" />
				<aui:validator name="maxLength" >'40'</aui:validator>
			</aui:input>
			
			
			<aui:input label='City<span style="color:red;font-size: 14px;">*</span>'  name="city" showRequiredLabel="false" cssClass="myprofile_fields">
				<aui:validator name="required" />
				<aui:validator name="alpha" />
				<aui:validator name="maxLength" >'25'</aui:validator>
			</aui:input>			
			
			<aui:input label='Zip Code<span style="color:red;font-size: 14px;">*</span>'  name="zip" showRequiredLabel="false" cssClass="myprofile_fields">
				<aui:validator name="required" />
				<aui:validator name="digits" />
				<aui:validator name="maxLength" >'6'</aui:validator>
			</aui:input>
		
			<aui:select label='Region<span style="color:red;font-size: 14px;">*</span>' name='regionId' />
		</div>
		</div>
		
</aui:fieldset>
<br>
<c:if test="<%= !user.isAgreedToTermsOfUse() %>">
	<div align="center">
		<input  type="submit" id="detail_pro_added_to_wishlist" value="Save & Continue"/>
	</div>
</c:if>
	
</aui:form>
</div>
<aui:script use="liferay-dynamic-select,aui-io-request">
					new Liferay.DynamicSelect(
						[
							{
								select: '<portlet:namespace />countryId',
								selectData: Liferay.Address.getCountries,
								selectDesc: 'name',
								selectId: 'countryId',
								selectVal: ''
							},
							{
								select: '<portlet:namespace />regionId',
								selectData: Liferay.Address.getRegions,
								selectDesc: 'name',
								selectId: 'regionId',
								selectVal: ''
							}
						]
				);
</aui:script>	
<aui:script	use="aui-form-validator, aui-overlay-context-panel,aui-base,event">
var validator = new A.FormValidator({
boundingBox: document.<portlet:namespace />fm,
rules: {
<portlet:namespace />regionId: {
required: true
},
<portlet:namespace />countryId: {
required: true
}
}
,
fieldStrings: {
<portlet:namespace />regionId: {
required: 'Please Select Region'
},
<portlet:namespace />countryId: {
required: 'Please Select Country'
}
}
});
</aui:script>
<style>
#navigation {
    display: none;
}

.aui-field-input, .aui-field-labels-top .aui-field-input{
width: 221px;
}

.dashboard-fields-lable, .aui-field-label, .aui-field-label-inline-label, .aui-choice-label{
margin-left: 7px;}


.myprofile_heading {
    font-family: centurygothic;
    font-size: large;
}
</style>