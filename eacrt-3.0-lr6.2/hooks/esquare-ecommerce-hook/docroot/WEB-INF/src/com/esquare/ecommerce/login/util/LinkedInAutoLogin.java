/**
 * Copyright (c) 2013  Componence Services B.V.(Inc), All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Componence Services B.V
 *
 */

package com.esquare.ecommerce.login.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.security.auth.AutoLoginException;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
/**
 * @author mohammad azharuddin
 */
public class LinkedInAutoLogin implements AutoLogin {


	public String[] login(HttpServletRequest request, HttpServletResponse response) throws AutoLoginException {
		String[] credentials = null;
		try {
			long companyId = PortalUtil.getCompanyId(request);

			HttpSession session = request.getSession();

			String email = (String)session.getAttribute(
				Constants.LINKED_IN_LOGIN);

			if (Validator.isNull(email) || !Validator.isEmailAddress(email)) {
				return credentials;
			}

			session.removeAttribute(Constants.LINKED_IN_LOGIN);

			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);

			credentials = new String[3];

			credentials[0] = String.valueOf(user.getUserId());
			credentials[1] = user.getPassword();
			credentials[2] = Boolean.FALSE.toString();
		}
		catch (Exception e) {
			_log.error(e, e);
		}

		return credentials;
	}


	@Override
	public String[] handleException(HttpServletRequest request,
			HttpServletResponse response, Exception e)
			throws AutoLoginException {
		// TODO Auto-generated method stub
		return null;
	}
	private static Log _log = LogFactoryUtil.getLog(LinkedInAutoLogin.class);
}
