package com.esquare.ecommerce.login.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;



public class CustomLandingPageAction extends Action {
	@Override
	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {
		
		try {

			doRun(request, response);

		} catch (Exception e) {
			throw new ActionException(e);
		}

	}

	private void doRun(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
		String cartURL = (String) session.getAttribute("cart_redirect");
		
		if(cartURL!=null) {
			session.removeAttribute("cart_redirect");
			session.setAttribute(WebKeys.LAST_PATH, new LastPath(StringPool.BLANK,
					cartURL));
		}

	}

}
