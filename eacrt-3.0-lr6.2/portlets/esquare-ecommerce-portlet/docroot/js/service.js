Liferay.Service.register("Liferay.Service.EEC", "com.esquare.ecommerce.service", "esquare-ecommerce-portlet");

Liferay.Service.registerClass(
	Liferay.Service.EEC, "Catalog",
	{
		addCatalog: true,
		updateCatalog: true,
		deleteCatalog: true
	}
);