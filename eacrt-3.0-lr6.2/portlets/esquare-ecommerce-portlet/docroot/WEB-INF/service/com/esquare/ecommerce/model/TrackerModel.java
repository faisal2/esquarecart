package com.esquare.ecommerce.model;

import java.util.Date;
import java.util.HashMap;
import javax.persistence.criteria.CriteriaBuilder.In;

public class TrackerModel {
		
		private Date date;
		private String locaion;
		private String status;
		private String orderItemIds;
		private String tackerId;
		private String comment;
		private HashMap<Long,Integer> orderItemQuantity;
	    
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public HashMap<Long, Integer> getOrderItemQuantity() {
			return orderItemQuantity;
		}
		public void setOrderItemQuantity(HashMap<Long, Integer> orderItemQuantity) {
			this.orderItemQuantity = orderItemQuantity;
		}
		
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		
		
		public String getLocaion() {
			return locaion;
		}
		public void setLocaion(String locaion) {
			this.locaion = locaion;
		}
		
		public String getOrderItemIds() {
			return orderItemIds;
		}
		public void setOrderItemIds(String orderItemIds) {
			this.orderItemIds = orderItemIds;
		}
		
		public String getTackerId() {
			return tackerId;
		}
		public void setTackerId(String tackerId) {
			this.tackerId = tackerId;
		}
		
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		
}
