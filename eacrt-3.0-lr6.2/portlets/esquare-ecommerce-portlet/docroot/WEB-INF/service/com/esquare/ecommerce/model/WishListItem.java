package com.esquare.ecommerce.model;

import java.io.Serializable;

import com.esquare.ecommerce.model.ProductInventory;

/**
 * @author Esquare
 */
public interface WishListItem
	extends Comparable<WishListItem>, Serializable {

	public String getWishListItemId();

	public ProductInventory getProductInventory();

}