package com.esquare.ecommerce.model;

import java.io.Serializable;

import com.esquare.ecommerce.model.ProductInventory;

/**
 * @author mohammad azaruddin
 */
public interface CartItem
	extends Comparable<CartItem>, Serializable {

	public String getCartItemId();

	public ProductInventory getProductInventory();

}