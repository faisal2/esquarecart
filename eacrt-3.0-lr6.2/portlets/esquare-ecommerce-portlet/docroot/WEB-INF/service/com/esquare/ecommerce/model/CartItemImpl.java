/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model;

import com.esquare.ecommerce.model.ProductInventory;
import com.liferay.portal.kernel.util.HashCode;
import com.liferay.portal.kernel.util.HashCodeFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * @author mohammad azaruddin
 */
public class CartItemImpl implements CartItem {

	public CartItemImpl(ProductInventory productInventory) {
		_productInventory = productInventory;
	}

	public int compareTo(CartItem cartItem) {
		if (cartItem == null) {
			return -1;
		}

		int value = getProductInventory().compareTo(cartItem.getProductInventory());

		return value;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		CartItem cartItem = (CartItem) obj;

		if (getProductInventory().equals(cartItem.getProductInventory())) {

			return true;
		} else {
			return false;
		}
	}

	public String getCartItemId() {
		return String.valueOf(getProductInventory().getInventoryId());
	}

	public ProductInventory getProductInventory() {
		return _productInventory;
	}

	@Override
	public int hashCode() {
		HashCode hashCode = HashCodeFactoryUtil.getHashCode();

		hashCode.append(_productInventory.getInventoryId());

		return hashCode.toHashCode();
	}

	private ProductInventory _productInventory;

}