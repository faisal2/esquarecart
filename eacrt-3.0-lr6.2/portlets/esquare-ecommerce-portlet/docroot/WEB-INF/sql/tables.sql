create table EEC_CancelledOrderItems (
	orderItemId LONG not null primary key,
	companyId LONG,
	userId LONG,
	orderId LONG,
	price DOUBLE,
	quantity INTEGER,
	cancelledDate DATE null
);

create table EEC_Cart (
	cartId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	productInventoryIds STRING null,
	couponCodes VARCHAR(75) null,
	walletAmount DOUBLE,
	altShipping INTEGER,
	insure BOOLEAN
);

create table EEC_CartPermission (
	permissionId LONG not null primary key,
	companyId LONG,
	permissionKey VARCHAR(75) null,
	Value VARCHAR(75) null,
	price INTEGER,
	unlimitedPck BOOLEAN
);

create table EEC_Catalog (
	catalogId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	parentCatalogId LONG,
	name VARCHAR(75) null,
	description STRING null,
	folderId LONG,
	showNavigation BOOLEAN
);

create table EEC_Coupon (
	couponId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	version DOUBLE,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	couponCode VARCHAR(75) null,
	couponName VARCHAR(75) null,
	description VARCHAR(75) null,
	couponLimit INTEGER,
	userLimit INTEGER,
	discountType VARCHAR(75) null,
	discount DOUBLE,
	minOrder DOUBLE,
	discountLimit VARCHAR(75) null,
	limitCatalogs STRING null,
	limitProducts STRING null,
	startDate DATE null,
	endDate DATE null,
	active_ BOOLEAN,
	archived BOOLEAN,
	neverExpire BOOLEAN
);

create table EEC_EBSOrderTracker (
	ebsOrderTrackerId LONG not null primary key,
	companyId LONG,
	orderId LONG,
	userId LONG,
	responseCode LONG,
	responseMessage VARCHAR(75) null,
	createDate DATE null,
	paymentId LONG,
	paymentMethod LONG,
	amount VARCHAR(75) null,
	isFlagged VARCHAR(75) null,
	transactionId LONG
);

create table EEC_EcartEbsPayment (
	ebsTrackId LONG not null primary key,
	companyId LONG,
	ecartAutoId VARCHAR(75) null,
	userId LONG,
	responseCode LONG,
	responseMessage VARCHAR(75) null,
	paidDate DATE null,
	paymentId LONG,
	paymentMethod LONG,
	amount VARCHAR(75) null,
	isFlagged VARCHAR(75) null,
	transactionId LONG
);

create table EEC_EcartPackageBilling (
	billId LONG not null primary key,
	companyId LONG,
	userId LONG,
	billingDate DATE null,
	packageType VARCHAR(75) null,
	amount DOUBLE,
	transactionId LONG
);

create table EEC_EcustomField (
	customFieldRecId LONG not null primary key,
	companyId LONG,
	title VARCHAR(75) null,
	xml STRING null
);

create table EEC_EcustomFieldValue (
	customValueRecId LONG not null primary key,
	companyId LONG,
	inventoryId LONG,
	xml STRING null
);

create table EEC_Instance (
	companyId LONG not null primary key,
	instanceUserId LONG,
	createdDate DATE null,
	expiryDate DATE null,
	packageType VARCHAR(75) null,
	currentTemplate VARCHAR(75) null
);

create table EEC_MyWallet (
	userId LONG not null primary key,
	companyId LONG,
	amount DOUBLE
);

create table EEC_Order (
	orderId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	number_ VARCHAR(75) null,
	tax DOUBLE,
	shipping DOUBLE,
	altShipping VARCHAR(75) null,
	requiresShipping BOOLEAN,
	insure BOOLEAN,
	insurance DOUBLE,
	couponCodes VARCHAR(75) null,
	couponVersion DOUBLE,
	couponDiscount DOUBLE,
	walletAmount DOUBLE,
	billingFirstName VARCHAR(75) null,
	billingLastName VARCHAR(75) null,
	billingEmailAddress VARCHAR(75) null,
	billingCompany VARCHAR(75) null,
	billingStreet VARCHAR(75) null,
	billingCity VARCHAR(75) null,
	billingState VARCHAR(75) null,
	billingZip VARCHAR(75) null,
	billingCountry VARCHAR(75) null,
	billingPhone VARCHAR(75) null,
	shipToBilling BOOLEAN,
	shippingFirstName VARCHAR(75) null,
	shippingLastName VARCHAR(75) null,
	shippingEmailAddress VARCHAR(75) null,
	shippingCompany VARCHAR(75) null,
	shippingStreet VARCHAR(75) null,
	shippingCity VARCHAR(75) null,
	shippingState VARCHAR(75) null,
	shippingZip VARCHAR(75) null,
	shippingCountry VARCHAR(75) null,
	shippingPhone VARCHAR(75) null,
	comments STRING null,
	sendOrderEmail BOOLEAN,
	paymentType VARCHAR(75) null,
	paymentStatus VARCHAR(75) null,
	status VARCHAR(75) null,
	history STRING null
);

create table EEC_OrderItem (
	orderItemId LONG not null primary key,
	orderId LONG,
	companyId LONG,
	productInventoryId VARCHAR(75) null,
	sku VARCHAR(75) null,
	name STRING null,
	description STRING null,
	properties STRING null,
	price DOUBLE,
	tax DOUBLE,
	shippingPrice DOUBLE,
	quantity INTEGER,
	shippingTrackerId VARCHAR(75) null,
	orderItemStatus VARCHAR(75) null,
	createdDate DATE null,
	deliveredDate DATE null,
	modifiedDate DATE null,
	expectedDeliveryDate DATE null,
	cancelByUser BOOLEAN,
	canceledByUserId LONG
);

create table EEC_OrderRefundTracker (
	refundTrackerId LONG not null primary key,
	companyId LONG,
	userId LONG,
	referenceNumber LONG,
	amountClaimed DOUBLE,
	paymentStatus BOOLEAN,
	paymentMode VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	expectedDeliveryDate DATE null,
	comments VARCHAR(75) null
);

create table EEC_PaymentConfiguration (
	id_ LONG not null primary key,
	companyId LONG,
	userId LONG,
	paymentGatewayType VARCHAR(75) null,
	merchantId VARCHAR(75) null,
	secretKey VARCHAR(75) null,
	status BOOLEAN,
	createDate DATE null,
	modifiedDate DATE null
);

create table EEC_PaymentUserDetail (
	recId LONG not null primary key,
	companyId LONG,
	userId LONG,
	email VARCHAR(75) null,
	name VARCHAR(75) null,
	phoneNo LONG,
	pinCode LONG,
	address VARCHAR(75) null,
	country VARCHAR(75) null,
	state_ VARCHAR(75) null,
	city VARCHAR(75) null
);

create table EEC_ProductDetails (
	productDetailsId LONG not null primary key,
	companyId LONG,
	userId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	catalogId LONG,
	name STRING null,
	description STRING null,
	visibility BOOLEAN,
	relatedProductIds VARCHAR(75) null
);

create table EEC_ProductImages (
	recId LONG not null primary key,
	inventoryId LONG,
	imageId LONG
);

create table EEC_ProductInventory (
	inventoryId LONG not null primary key,
	companyId LONG,
	createDate DATE null,
	modifiedDate DATE null,
	productDetailsId LONG,
	customFieldRecId LONG,
	sku VARCHAR(75) null,
	price DOUBLE,
	discount DOUBLE,
	quantity LONG,
	barcode VARCHAR(75) null,
	taxes BOOLEAN,
	freeShipping BOOLEAN,
	weight DOUBLE,
	shippingFormula VARCHAR(75) null,
	outOfStockPurchase BOOLEAN,
	similarProducts BOOLEAN,
	limitCatalogs VARCHAR(75) null,
	limitProducts VARCHAR(75) null,
	visibility BOOLEAN,
	column1 VARCHAR(75) null,
	column2 VARCHAR(75) null,
	column3 VARCHAR(75) null
);

create table EEC_ShippingRates (
	recId LONG not null primary key,
	companyId LONG,
	countryId VARCHAR(75) null,
	name VARCHAR(75) null,
	shippingType VARCHAR(75) null,
	rangeFrom DOUBLE,
	rangeTo DOUBLE,
	price DOUBLE,
	xml STRING null
);

create table EEC_TaxesSetting (
	recId LONG not null primary key,
	companyId LONG,
	countryId VARCHAR(75) null,
	name VARCHAR(75) null,
	taxRate DOUBLE,
	xml STRING null
);

create table EEC_TemplateManager (
	recId LONG not null primary key,
	title VARCHAR(75) null,
	cssId VARCHAR(75) null,
	larName VARCHAR(75) null,
	templateDesc VARCHAR(75) null,
	folderId LONG,
	packageType VARCHAR(75) null,
	modifiedDate DATE null
);

create table EEC_WishList (
	wishListId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	productInventoryIds STRING null
);