create index IX_E15C57B on EEC_CancelledOrderItems (companyId);
create index IX_391F72B5 on EEC_CancelledOrderItems (companyId, userId);

create index IX_4FA8600 on EEC_Cart (companyId, userId);
create unique index IX_B23A2402 on EEC_Cart (groupId, userId);

create index IX_919EF137 on EEC_CartPermission (companyId);
create index IX_CCABE8A9 on EEC_CartPermission (companyId, permissionKey);

create index IX_E2A75065 on EEC_Catalog (companyId);
create index IX_B500FA75 on EEC_Catalog (companyId, parentCatalogId);
create index IX_DC0A988A on EEC_Catalog (companyId, showNavigation);

create index IX_2BCD0425 on EEC_Coupon (couponCode, archived);
create index IX_215B5E59 on EEC_Coupon (couponCode, version);

create index IX_84803864 on EEC_EcartPackageBilling (companyId);
create index IX_63E18625 on EEC_EcartPackageBilling (companyId, transactionId);

create index IX_BA408AE2 on EEC_EcustomField (companyId);
create index IX_CEDC55E6 on EEC_EcustomField (companyId, title);

create index IX_B39860F6 on EEC_EcustomFieldValue (companyId, inventoryId);

create index IX_4301E380 on EEC_Order (companyId, couponCodes);
create index IX_3C103E82 on EEC_Order (companyId, couponCodes, couponVersion);
create index IX_7E993B96 on EEC_Order (companyId, number_);
create index IX_3EFED536 on EEC_Order (companyId, status);
create index IX_C7F48986 on EEC_Order (companyId, userId, couponCodes);
create index IX_B02BD49C on EEC_Order (companyId, userId, number_);
create index IX_27D20370 on EEC_Order (companyId, userId, status);
create index IX_F91C0D2 on EEC_Order (groupId);
create index IX_220E99F2 on EEC_Order (groupId, userId, status);
create unique index IX_CA53540E on EEC_Order (number_);

create index IX_F66CAE0E on EEC_OrderItem (orderId);
create index IX_1B60CEF5 on EEC_OrderItem (orderId, orderItemStatus);
create index IX_1E113CAA on EEC_OrderItem (orderId, productInventoryId);

create index IX_96803894 on EEC_OrderRefundTracker (companyId, paymentStatus);

create index IX_DFE93056 on EEC_PaymentConfiguration (companyId);
create index IX_3BA40982 on EEC_PaymentConfiguration (companyId, paymentGatewayType);

create index IX_8DA917C on EEC_PaymentUserDetail (companyId);
create index IX_A383EB66 on EEC_PaymentUserDetail (userId);

create index IX_37A13273 on EEC_ProductDetails (companyId);
create index IX_5097D171 on EEC_ProductDetails (companyId, catalogId);
create index IX_43ADC8F7 on EEC_ProductDetails (companyId, catalogId, visibility);
create unique index IX_5A7A9FF2 on EEC_ProductDetails (companyId, name);
create index IX_67B3B6F9 on EEC_ProductDetails (companyId, visibility);

create index IX_720DF816 on EEC_ProductImages (inventoryId);

create index IX_769CEAD4 on EEC_ProductInventory (companyId, sku);
create index IX_C6FAE71 on EEC_ProductInventory (productDetailsId);

create index IX_95D14479 on EEC_ShippingRates (companyId);
create index IX_C43850A8 on EEC_ShippingRates (companyId, countryId);
create index IX_E92699D5 on EEC_ShippingRates (companyId, countryId, shippingType, rangeFrom, rangeTo);

create index IX_DDD1E5CF on EEC_TaxesSetting (companyId);
create index IX_CAE69D92 on EEC_TaxesSetting (companyId, countryId);

create index IX_36FE68D3 on EEC_TemplateManager (packageType);

create index IX_C2EC725B on EEC_WishList (companyId, userId);
create index IX_7CE16B1D on EEC_WishList (groupId, userId);