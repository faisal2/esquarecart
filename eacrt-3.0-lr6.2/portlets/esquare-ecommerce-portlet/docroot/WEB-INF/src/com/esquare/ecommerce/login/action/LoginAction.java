package com.esquare.ecommerce.login.action;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.esquare.ecommerce.util.SocialNetworkConnectUtil;
import com.liferay.portal.AddressCityException;
import com.liferay.portal.AddressStreetException;
import com.liferay.portal.AddressZipException;
import com.liferay.portal.CompanyMaxUsersException;
import com.liferay.portal.ContactFirstNameException;
import com.liferay.portal.ContactFullNameException;
import com.liferay.portal.ContactLastNameException;
import com.liferay.portal.CookieNotSupportedException;
import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.DuplicateUserScreenNameException;
import com.liferay.portal.EmailAddressException;
import com.liferay.portal.GroupFriendlyURLException;
import com.liferay.portal.NoSuchCountryException;
import com.liferay.portal.NoSuchListTypeException;
import com.liferay.portal.NoSuchOrganizationException;
import com.liferay.portal.NoSuchRegionException;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.OrganizationParentException;
import com.liferay.portal.PasswordExpiredException;
import com.liferay.portal.PhoneNumberException;
import com.liferay.portal.RequiredFieldException;
import com.liferay.portal.RequiredReminderQueryException;
import com.liferay.portal.RequiredUserException;
import com.liferay.portal.ReservedUserEmailAddressException;
import com.liferay.portal.ReservedUserScreenNameException;
import com.liferay.portal.SendPasswordException;
import com.liferay.portal.TermsOfUseException;
import com.liferay.portal.UserActiveException;
import com.liferay.portal.UserEmailAddressException;
import com.liferay.portal.UserIdException;
import com.liferay.portal.UserLockoutException;
import com.liferay.portal.UserPasswordException;
import com.liferay.portal.UserReminderQueryException;
import com.liferay.portal.UserScreenNameException;
import com.liferay.portal.UserSmsException;
import com.liferay.portal.WebsiteURLException;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassInvoker;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.Resource;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserConstants;
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.NoSuchTableException;
import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.model.ExpandoColumn;
import com.liferay.portlet.expando.model.ExpandoColumnConstants;
import com.liferay.portlet.expando.model.ExpandoTable;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoColumnLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoTableLocalServiceUtil;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class LoginAction
 */
public class LoginAction extends MVCPortlet {
	private static final String LINKED_IN_CUSTOM_ID = "linkedInCustomId";
	private static final String TWITTER_CUSTOM_ID = "twitterId";

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		String resultURL = ParamUtil.getString(actionRequest,"resultURL");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		boolean isPopupLogin = ParamUtil.getBoolean(actionRequest,
				"isPopupLogin", false);
		String twitterId = ParamUtil.getString(actionRequest, "twitterId");
		String firstName = ParamUtil.getString(actionRequest, "firstName");
		String screenName = ParamUtil.getString(actionRequest, "screenName");
		String redirectMainPath = themeDisplay.getPathMain();
		String redirect = ParamUtil.getString(actionRequest, "redirect");
		if (Validator.isNotNull(redirect))
			redirectMainPath = redirect;
		HttpSession session = PortalUtil.getHttpServletRequest(actionRequest)
				.getSession();

		Boolean twitterLoginPending = (Boolean) session
				.getAttribute(EECConstants.TWITTER_LOGIN_PENDING);
		PortletURL redirectURL = EECUtil.getPortletURL(actionRequest,
				EECConstants.LOGIN_PORTLET_NAME, themeDisplay.getPlid(),
				ActionRequest.RENDER_PHASE,
				"/html/eec/common/login/popup_redirect.jsp", null,
				LiferayWindowState.POP_UP, PortletMode.VIEW);
		if (cmd.equals(EECConstants.ADD)) {
			try {
				if ((twitterLoginPending != null)
						&& twitterLoginPending.booleanValue()
						&& Validator.isNotNull(twitterId)) {
					addTwitterUser(actionRequest, actionResponse);
				} else {
					addUser(actionRequest, actionResponse);
					if (isPopupLogin) {
						session.setAttribute("isPopupLogin", "isPopupLogin");
						actionResponse.sendRedirect(redirectURL.toString());
					} else {
						actionResponse.sendRedirect(redirectMainPath);
					}
				}

			} catch (Exception e) {

				if (e instanceof DuplicateUserEmailAddressException
						|| e instanceof DuplicateUserScreenNameException
						|| e instanceof AddressCityException
						|| e instanceof AddressStreetException
						|| e instanceof AddressZipException
						|| e instanceof CaptchaMaxChallengesException
						|| e instanceof CaptchaTextException
						|| e instanceof CompanyMaxUsersException
						|| e instanceof ContactFirstNameException
						|| e instanceof ContactFullNameException
						|| e instanceof ContactLastNameException
						|| e instanceof EmailAddressException
						|| e instanceof GroupFriendlyURLException
						|| e instanceof NoSuchCountryException
						|| e instanceof NoSuchListTypeException
						|| e instanceof NoSuchOrganizationException
						|| e instanceof NoSuchRegionException
						|| e instanceof OrganizationParentException
						|| e instanceof PhoneNumberException
						|| e instanceof RequiredFieldException
						|| e instanceof RequiredUserException
						|| e instanceof ReservedUserEmailAddressException
						|| e instanceof ReservedUserScreenNameException
						|| e instanceof TermsOfUseException
						|| e instanceof UserEmailAddressException
						|| e instanceof UserIdException
						|| e instanceof UserPasswordException
						|| e instanceof UserScreenNameException
						|| e instanceof UserSmsException
						|| e instanceof WebsiteURLException) {
					SessionErrors.add(actionRequest, e.getClass(), e);
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/login/create_account.jsp");
					if (Validator.isNotNull(twitterId)) {
						actionResponse.setRenderParameter("firstName",
								firstName);
						actionResponse.setRenderParameter("screenName",
								screenName);
						actionResponse.setRenderParameter("twitterId",
								twitterId);
					}
				}
				if (isPopupLogin)
					actionRequest.setAttribute("isPopupLogin", "true");
			}
		} else if (cmd.equals(EECConstants.LOGIN)) {
			String login = null;
			String password = null;
			boolean rememberMe = false;
			try {
				login = ParamUtil.getString(actionRequest, "login");
				password = actionRequest.getParameter("password");
				rememberMe = ParamUtil.getBoolean(actionRequest, "rememberMe",
						false);
				login(actionRequest, actionResponse, themeDisplay, login,
						password, rememberMe);

				if (isPopupLogin) {
					HttpServletRequest request = PortalUtil
							.getHttpServletRequest(actionRequest);
					HttpSession httpSession = request.getSession();
					httpSession.setAttribute("isPopupLogin", "isPopupLogin");
					actionResponse.sendRedirect(redirectURL.toString());
				} else {

					actionResponse.sendRedirect(redirectMainPath);
				}
			} catch (Exception e) {
				if (e instanceof AuthException) {
					Throwable cause = e.getCause();

					if (cause instanceof PasswordExpiredException
							|| cause instanceof UserLockoutException) {

						SessionErrors.add(actionRequest, cause.getClass());
					} else {
						if (_log.isInfoEnabled()) {
							actionResponse.sendRedirect(resultURL);
							_log.info("Authentication failed");
						}

						SessionErrors.add(actionRequest, e.getClass());
					}
				} else if (e instanceof CompanyMaxUsersException
						|| e instanceof CookieNotSupportedException
						|| e instanceof NoSuchUserException
						|| e instanceof PasswordExpiredException
						|| e instanceof UserEmailAddressException
						|| e instanceof UserIdException
						|| e instanceof UserLockoutException
						|| e instanceof UserPasswordException
						|| e instanceof UserScreenNameException) {

					SessionErrors.add(actionRequest, e.getClass());
				} else {
					_log.info(e.getClass());

					PortalUtil.sendError(e, actionRequest, actionResponse);
				}

				if (isPopupLogin)
					actionRequest.setAttribute("isPopupLogin", "true");
			}
		} else if (cmd.equals(EECConstants.TWITTER)) {
			try {
				executeTwitter(actionRequest, actionResponse);
			} catch (Exception e) {
			}
		} else if (cmd.equals(EECConstants.LINKEDIN)) {
			long companyId = PortalUtil.getCompanyId(actionRequest);
			String linkedinId = ParamUtil.getString(actionRequest, "idIn");
			User user = getUserBySocialNetworkId(linkedinId,
					LINKED_IN_CUSTOM_ID, companyId);
			if (Validator.isNull(user)) {

				try {
					user = createLinkedUser(actionRequest, companyId,
							linkedinId, user);
				} catch (SystemException e) {
					_log.info(e.getClass());
				} catch (PortalException e) {
					_log.info(e.getClass());
				}
			}

			if (Validator.isNotNull(user)) {
				setExistingUserOnSession(actionRequest, user,
						EECConstants.LINKED_IN_LOGIN);
				HttpServletRequest request = PortalUtil
						.getHttpServletRequest(actionRequest);
				HttpSession httpSession = request.getSession();
				httpSession.setAttribute("isPopupLogin", "isPopupLogin");
				actionResponse.sendRedirect(redirectMainPath);
			}
		} else if (cmd.equals(EECConstants.FORGOT_PASSWORD_ACTION)) {
			try {
				if (PortletPropsValues.USERS_REMINDER_QUERIES_ENABLED) {
					checkReminderQueries(actionRequest, actionResponse);

				} else {
					checkCaptcha(actionRequest);

					sendPassword(actionRequest, actionResponse);
				}
			} catch (Exception e) {
				if (e instanceof CaptchaTextException
						|| e instanceof NoSuchUserException
						|| e instanceof RequiredReminderQueryException
						|| e instanceof SendPasswordException
						|| e instanceof UserActiveException
						|| e instanceof UserEmailAddressException
						|| e instanceof UserReminderQueryException) {
					SessionErrors.add(actionRequest, e.getClass());
				} else {
					PortalUtil.sendError(e, actionRequest, actionResponse);
				}
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/login/forgot_password.jsp");
			}

		} else if (cmd.equals(EECConstants.WISHLIST_UPDATE)) {
			String productInventoryId = actionRequest
					.getParameter("productInventoryIds");
			PortletSession portletSession=actionRequest.getPortletSession();
			portletSession.setAttribute(EECConstants.WISHLIST_UPDATE_ATTRIBUTE,String.valueOf(productInventoryId),PortletSession.APPLICATION_SCOPE);
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/login/login.jsp");
			if (isPopupLogin) {
				actionRequest.setAttribute("isPopupLogin", "true");

			}
		}

	}

	protected void checkReminderQueries(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {

		PortletSession portletSession = actionRequest.getPortletSession();

		int step = ParamUtil.getInteger(actionRequest, "step");

		if (step == 1) {
			checkCaptcha(actionRequest);

			portletSession
					.removeAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_ATTEMPTS);
			portletSession
					.removeAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_USER_EMAIL_ADDRESS);
		}

		User user = getUser(actionRequest);

		portletSession.setAttribute(
				WebKeys.FORGOT_PASSWORD_REMINDER_USER_EMAIL_ADDRESS,
				user.getEmailAddress());

		actionRequest.setAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_USER, user);

		if (step == 2) {
			Integer reminderAttempts = (Integer) portletSession
					.getAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_ATTEMPTS);

			if (reminderAttempts == null) {
				reminderAttempts = 0;
			} else if (reminderAttempts > 2) {
				checkCaptcha(actionRequest);
			}

			reminderAttempts++;

			portletSession
					.setAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_ATTEMPTS,
							reminderAttempts);

			sendPassword(actionRequest, actionResponse);
		}
		if (step != 2) {
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/login/forgot_password.jsp");
		}
	}

	protected User getUser(ActionRequest actionRequest) throws Exception {
		PortletSession portletSession = actionRequest.getPortletSession();

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		String sessionEmailAddress = (String) portletSession
				.getAttribute(WebKeys.FORGOT_PASSWORD_REMINDER_USER_EMAIL_ADDRESS);

		User user = null;

		if (Validator.isNotNull(sessionEmailAddress)) {
			user = UserLocalServiceUtil.getUserByEmailAddress(
					themeDisplay.getCompanyId(), sessionEmailAddress);
		} else {
			long userId = ParamUtil.getLong(actionRequest, "userId");
			String screenName = ParamUtil
					.getString(actionRequest, "screenName");
			String emailAddress = ParamUtil.getString(actionRequest,
					"emailAddress");

			if (Validator.isNotNull(emailAddress)) {
				user = UserLocalServiceUtil.getUserByEmailAddress(
						themeDisplay.getCompanyId(), emailAddress);
			} else if (Validator.isNotNull(screenName)) {
				user = UserLocalServiceUtil.getUserByScreenName(
						themeDisplay.getCompanyId(), screenName);
			} else if (userId > 0) {
				user = UserLocalServiceUtil.getUserById(userId);
			} else {
				throw new NoSuchUserException();
			}
		}

		if (!user.isActive()) {
			throw new UserActiveException();
		}

		return user;
	}

	protected void sendPassword(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		Company company = themeDisplay.getCompany();

		User user = getUser(actionRequest);

		if (PortletPropsValues.USERS_REMINDER_QUERIES_ENABLED) {
			if (PortletPropsValues.USERS_REMINDER_QUERIES_REQUIRED
					&& !user.hasReminderQuery()) {

				throw new RequiredReminderQueryException(
						"No reminder query or answer is defined for user "
								+ user.getUserId());
			}

			String answer = ParamUtil.getString(actionRequest, "answer");

			if (!user.getReminderQueryAnswer().equals(answer)) {
				throw new UserReminderQueryException();
			}
		}

		PortletPreferences preferences = actionRequest.getPreferences();

		String languageId = LanguageUtil.getLanguageId(actionRequest);

		String emailFromName = preferences.getValue("emailFromName", null);
		String emailFromAddress = preferences
				.getValue("emailFromAddress", null);
		String emailToAddress = user.getEmailAddress();

		String emailParam = "emailPasswordSent";

		if (company.isSendPasswordResetLink()) {
			emailParam = "emailPasswordReset";
		}

		String subject = preferences.getValue(emailParam + "Subject_"
				+ languageId, null);
		String body = preferences.getValue(emailParam + "Body_" + languageId,
				null);
  
  	ClassLoader pcl = PortalClassLoaderUtil.getClassLoader();
	Class lClass = pcl.loadClass("com.liferay.portlet.login.util.LoginUtil");
	
	/*MethodKey key = new MethodKey(
			"com.liferay.portlet.login.util.LoginUtil", "sendPassword",
			ActionRequest.class, String.class, String.class, String.class,
			String.class, String.class);*/
	
	MethodKey key = new MethodKey(lClass, "sendPassword",
			ActionRequest.class, String.class, String.class, String.class,
			String.class, String.class);
	
		
		try {
			PortalClassInvoker.invoke(false, key, new Object[] { actionRequest,
					emailFromName, emailFromAddress, emailToAddress, subject,
					body });
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		sendRedirect(actionRequest, actionResponse);
	}

	protected void addTwitterUser(ActionRequest actionRequest,
			ActionResponse actionResponse) throws SystemException,
			PortalException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(actionRequest);
		HttpSession session = request.getSession();
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		Company company = themeDisplay.getCompany();
		boolean autoPassword = true;
		String password1 = null;
		String password2 = null;
		boolean autoScreenName = true;
		String screenName = null;
		String emailAddress = ParamUtil
				.getString(actionRequest, "emailAddress");
		long facebookId = ParamUtil.getLong(actionRequest, "facebookId");
		String openId = ParamUtil.getString(actionRequest, "openId");
		String firstName = ParamUtil.getString(actionRequest, "firstName");
		String middleName = ParamUtil.getString(actionRequest, "middleName");
		String lastName = ParamUtil.getString(actionRequest, "lastName");
		int prefixId = ParamUtil.getInteger(actionRequest, "prefixId");
		int suffixId = ParamUtil.getInteger(actionRequest, "suffixId");
		boolean male = ParamUtil.getBoolean(actionRequest, "male", true);
		int birthdayMonth = ParamUtil.getInteger(actionRequest,
				"birthdayMonth", 01);
		int birthdayDay = ParamUtil
				.getInteger(actionRequest, "birthdayDay", 01);
		int birthdayYear = ParamUtil.getInteger(actionRequest, "birthdayYear",
				1970);
		String jobTitle = ParamUtil.getString(actionRequest, "jobTitle");
		long[] groupIds = null;
		long[] organizationIds = null;
		long[] roleIds = null;
		long[] userGroupIds = null;
		boolean sendEmail = false;
		String twitterId = ParamUtil.getString(actionRequest, "twitterId");
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				User.class.getName(), actionRequest);
		User user = UserServiceUtil.addUserWithWorkflow(company.getCompanyId(),
				autoPassword, password1, password2, autoScreenName, screenName,
				emailAddress, facebookId, openId, themeDisplay.getLocale(),
				firstName, middleName, lastName, prefixId, suffixId, male,
				birthdayMonth, birthdayDay, birthdayYear, jobTitle, groupIds,
				organizationIds, roleIds, userGroupIds, sendEmail,
				serviceContext);

		UserLocalServiceUtil.updateLastLogin(user.getUserId(),
				user.getLoginIP());
		UserLocalServiceUtil.updatePasswordReset(user.getUserId(), false);
		setUserExpandoValue(user, twitterId, themeDisplay.getCompanyId(),
				User.class.getName(), TWITTER_CUSTOM_ID);
		setExistingUserOnSession(actionRequest, user,
				EECConstants.TWITTER_ID_LOGIN);
		session.removeAttribute(EECConstants.TWITTER_LOGIN_PENDING);
		// Send redirect

		try {
			session.setAttribute("isPopupLogin", "isPopupLoginTwitter");
			PortletURL portletURL = EECUtil.getPortletURL(actionRequest,
					EECConstants.LOGIN_PORTLET_NAME, themeDisplay.getPlid(),
					ActionRequest.RENDER_PHASE,
					"/html/eec/common/login/popup_redirect.jsp", null,
					LiferayWindowState.POP_UP, LiferayPortletMode.VIEW);
			actionResponse.sendRedirect(portletURL.toString());
		} catch (IOException e) {
			_log.info(e.getClass());
		}
	}

	protected void checkCaptcha(ActionRequest actionRequest)
			throws CaptchaException {
		if (PortletPropsValues.CAPTCHA_CHECK_PORTAL_SEND_PASSWORD) {
			CaptchaUtil.check(actionRequest);
		}
	}

	protected void addUser(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException,
			SystemException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String emailAddress = ParamUtil
				.getString(actionRequest, "emailAddress");
		String password1 = ParamUtil.getString(actionRequest, "password1");
		String password2 = ParamUtil.getString(actionRequest, "password2");
		boolean autoScreenName = true;
		String screenName = null;
		String firstName = ParamUtil.getString(actionRequest, "firstName");
		String middleName = StringPool.BLANK;
		String lastName = StringPool.BLANK;
		long facebookId = 0l;
		String openId = StringPool.BLANK;
		int prefixId = 0;
		int suffixId = 0;
		boolean male = true;
		int birthdayDay = 01;
		int birthdayMonth = 01;
		int birthdayYear = 1970;
		String jobTitle = StringPool.BLANK;
		long[] groupIds = null;
		long[] organizationIds = null;
		long[] roleIds = null;
		long[] userGroupIds = null;
		boolean sendEmail = true;
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				User.class.getName(), actionRequest);
		User user = UserServiceUtil.addUserWithWorkflow(
				themeDisplay.getCompanyId(), false, password1, password2,
				autoScreenName, screenName, emailAddress, facebookId, openId,
				themeDisplay.getLocale(), firstName, middleName, lastName,
				prefixId, suffixId, male, birthdayMonth, birthdayDay,
				birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
				userGroupIds, sendEmail, serviceContext);
		UserLocalServiceUtil.updateLastLogin(user.getUserId(),
				user.getLoginIP());
		UserLocalServiceUtil.updatePasswordReset(user.getUserId(), false);
		try {
			login(actionRequest, actionResponse, themeDisplay, emailAddress,
					password1, false);
		} catch (Exception e) {
			_log.info(e.getClass());
		}

	}

	protected void login(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay,
			String login, String password, boolean rememberMe) throws Exception {
		
		
	 	ClassLoader pcl = PortalClassLoaderUtil.getClassLoader();
		Class lClass = pcl.loadClass("com.liferay.portlet.login.util.LoginUtil");
		
		MethodKey key = new MethodKey(
				lClass, "login",
				HttpServletRequest.class, HttpServletResponse.class,
				String.class, String.class, boolean.class, String.class);
		
		PortalClassInvoker.invoke(false, key,
				new Object[] { PortalUtil.getHttpServletRequest(actionRequest),
						PortalUtil.getHttpServletResponse(actionResponse),
						login, password, rememberMe, null });
	}

	public String executeTwitter(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(actionRequest);
		HttpSession session = request.getSession();
		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();
		String twitterApiKey = SocialNetworkConnectUtil
				.getTwitterAppId(companyId);
		String twitterApiSecret = SocialNetworkConnectUtil
				.getTwitterAppSecret(companyId);
		OAuthService service = null;
		try {
			service = new ServiceBuilder().provider(TwitterApi.class)
					.apiKey(twitterApiKey).apiSecret(twitterApiSecret).build();
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		String oauthVerifier = ParamUtil.getString(request, "oauth_verifier");
		String oauthToken = ParamUtil.getString(request, "oauth_token");
		if (Validator.isNull(oauthVerifier) || Validator.isNull(oauthToken)) {
			return null;
		}

		Verifier v = new Verifier(oauthVerifier);
		Token requestToken = new Token(oauthToken, twitterApiSecret);
		Token accessToken = service.getAccessToken(requestToken, v);
		String verifyCredentialsURL = SocialNetworkConnectUtil
				.getTwitterVerifyCredentialURL();
		OAuthRequest authrequest = new OAuthRequest(Verb.GET,
				verifyCredentialsURL);

		service.signRequest(accessToken, authrequest);

		String bodyResponse = authrequest.send().getBody();
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(bodyResponse);
		String twitterId = jsonObject.getString("id").toString();
		User user = getUserBySocialNetworkId(twitterId, TWITTER_CUSTOM_ID,
				companyId);

		if (Validator.isNotNull(user)) {
			setExistingUserOnSession(actionRequest, user,
					EECConstants.TWITTER_ID_LOGIN);
			try {
				HttpSession httpSession = request.getSession();
				httpSession.setAttribute("isPopupLogin", "isPopupLoginTwitter");
				PortletURL portletURL = EECUtil.getPortletURL(actionRequest,
						EECConstants.LOGIN_PORTLET_NAME,
						themeDisplay.getPlid(), ActionRequest.RENDER_PHASE,
						"/html/eec/common/login/popup_redirect.jsp", null,
						LiferayWindowState.POP_UP, LiferayPortletMode.VIEW);
				actionResponse.sendRedirect(portletURL.toString());
			} catch (IOException e) {
				_log.info(e.getClass());
			}
			return null;
		}

		String firstName = jsonObject.getString("name").toString();
		String screenName = jsonObject.getString("screen_name").toString();
		session.setAttribute(EECConstants.TWITTER_LOGIN_PENDING, Boolean.TRUE);
		actionResponse.setRenderParameter("firstName", firstName);
		actionResponse.setRenderParameter("screenName", screenName);
		actionResponse.setRenderParameter("twitterId", twitterId);
		actionResponse.setWindowState(LiferayWindowState.POP_UP);
		actionResponse.setRenderParameter("jspPage",
				"/html/eec/common/login/create_account.jsp");
		return null;
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String cmd = ParamUtil.getString(resourceRequest, EECConstants.CMD);
		 if (cmd.equals(EECConstants.CAPTCHA)) {
			try {
				CaptchaUtil.serveImage(resourceRequest, resourceResponse);
			} catch (Exception e) {
				_log.error(e);
			}
		} 
		
		long companyId = PortalUtil.getCompanyId(resourceRequest);
		String linkedinId = ParamUtil.getString(resourceRequest, "idIn");
		User user = getUserBySocialNetworkId(linkedinId, LINKED_IN_CUSTOM_ID,
				companyId);
		if (user != null && user.getUserId() > 0) {
			resourceResponse.setContentType("text/html");
			resourceResponse.getWriter().write("existingUser");
			setExistingUserOnSession(resourceRequest, user,
					EECConstants.LINKED_IN_LOGIN);
		}

		super.serveResource(resourceRequest, resourceResponse);
	}

	private User getUserBySocialNetworkId(String socialNetworkId,
			String columnName, long companyId) {
		List<ExpandoValue> values;
		try {
			values = ExpandoValueLocalServiceUtil.getColumnValues(companyId,
					ClassNameLocalServiceUtil.getClassNameId(User.class),
					ExpandoTableConstants.DEFAULT_TABLE_NAME, columnName,
					socialNetworkId, -1, -1);

			if (values != null && !values.isEmpty()) {
				long userId = values.get(0).getClassPK();
				return UserLocalServiceUtil.getUser(userId);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		return null;
	}

	private void setExistingUserOnSession(PortletRequest portletRequest,
			User user, String socialNetworkType) {
		HttpServletRequest req = PortalUtil
				.getHttpServletRequest(portletRequest);
		HttpSession session = req.getSession();
		session.setAttribute(socialNetworkType, user.getEmailAddress());
	}

	private User createLinkedUser(ActionRequest actionRequest, long companyId,
			String linkedinId, User user) throws SystemException,
			PortalException {
		String firstName = ParamUtil
				.getString(actionRequest, "firstNameIn", "");
		String lastName = ParamUtil.getString(actionRequest, "lastNameIn", "");
		String email = ParamUtil.getString(actionRequest, "emailIn", null);

		long creatorUserId = 0;
		boolean autoPassword = true;
		String password1 = StringPool.BLANK;
		String password2 = StringPool.BLANK;
		boolean autoScreenName = true;
		String screenName = StringPool.BLANK;
		long facebookId = 0;
		String openId = StringPool.BLANK;
		Locale locale = LocaleUtil.getDefault();
		String middleName = ParamUtil
				.getString(actionRequest, "maidenName", "");
		int prefixId = 0;
		int suffixId = 0;
		boolean male = ParamUtil.getBoolean(actionRequest, "gender", true);
		int birthdayMonth = ParamUtil.getInteger(actionRequest, "month", 1);// -1
		int birthdayDay = ParamUtil.getInteger(actionRequest, "day", 1);
		int birthdayYear = ParamUtil.getInteger(actionRequest, "year", 1970);
		String dobString = String.valueOf(birthdayDay)
				+ String.valueOf(birthdayMonth) + String.valueOf(birthdayYear);
		Date dob = null;
		try {
			dob = new SimpleDateFormat("ddMMyyyy").parse(dobString);
		} catch (ParseException e) {
			_log.info(e.getClass());
		}
		String jobTitle = ParamUtil.getString(actionRequest, "positionsIn");
		;
		long[] groupIds = null;
		long[] organizationIds = null;

		long[] roleIds = null;
		long[] userGroupIds = null;
		boolean sendEmail = true;
		ServiceContext serviceContext = new ServiceContext();
		try {
			user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
			user.setFirstName(firstName);
			user.setMiddleName(middleName);
			user.setLastName(lastName);
			user.setJobTitle(jobTitle);
			Contact contact = null;
			contact = ContactLocalServiceUtil.getContact(user.getContactId());
			contact.setBirthday(dob);
			ContactLocalServiceUtil.updateContact(contact);
			user = UserLocalServiceUtil.updateUser(user);

		} catch (Exception e) {
			user = UserLocalServiceUtil.addUser(creatorUserId, companyId,
					autoPassword, password1, password2, autoScreenName,
					screenName, email, facebookId, openId, locale, firstName,
					middleName, lastName, prefixId, suffixId, male,
					birthdayMonth, birthdayDay, birthdayYear, jobTitle,
					groupIds, organizationIds, roleIds, userGroupIds,
					sendEmail, serviceContext);
		}
		UserLocalServiceUtil.updateLastLogin(user.getUserId(),
				user.getLoginIP());
		UserLocalServiceUtil.updatePasswordReset(user.getUserId(), false);
		UserLocalServiceUtil.updateEmailAddressVerified(user.getUserId(), true);
		setUserExpandoValue(user, linkedinId, companyId, User.class.getName(),
				LINKED_IN_CUSTOM_ID);
		return user;
	}

	private void setUserExpandoValue(User user, String customFieldValue,
			long companyId, String className, String customFieldName) {
		ExpandoBridge expandoBridge = user.getExpandoBridge();
		setExpandoValue(expandoBridge, customFieldValue, companyId,
				User.class.getName(), customFieldName);
	}

	private void setExpandoValue(ExpandoBridge expandoBridge,
			String customFieldValue, long companyId, String className,
			String customFieldName) {
		ExpandoTable table = null;
		try {
			table = ExpandoTableLocalServiceUtil.getTable(companyId, className,
					ExpandoTableConstants.DEFAULT_TABLE_NAME);

		} catch (NoSuchTableException e) {
			try {
				table = ExpandoTableLocalServiceUtil.addTable(companyId,
						className, ExpandoTableConstants.DEFAULT_TABLE_NAME);
			} catch (Exception ex) {
				_log.error(ex.getMessage());
			}
		} catch (Exception ex) {
			_log.error(ex.getMessage());
		}
		ExpandoColumn column = null;
		try {
			column = ExpandoColumnLocalServiceUtil.getColumn(companyId,
					className, ExpandoTableConstants.DEFAULT_TABLE_NAME,
					customFieldName);
		} catch (Exception ex) {
			_log.error(ex.getMessage());
		}

		if (Validator.isNull(column)) {
			try {
				column = ExpandoColumnLocalServiceUtil.addColumn(
						table.getTableId(), customFieldName,
						ExpandoColumnConstants.STRING);
				UnicodeProperties properties = expandoBridge
						.getAttributeProperties(customFieldName);
				properties.put("hidden", "1");
				ExpandoColumnLocalServiceUtil.updateTypeSettings(
						column.getColumnId(), properties.toString());
				Role roleUser = RoleLocalServiceUtil.getRole(companyId,
						RoleConstants.GUEST);
				
//				check here 
				/*ResourceLocalServiceUtil.addResources(
						companyId, ExpandoColumn.class.getName(),
						ResourceConstants.SCOPE_INDIVIDUAL,
						String.valueOf(column.getColumnId()));*/
				
				/*ResourcePermissionLocalServiceUtil.setResourcePermissions(
						companyId, ExpandoColumn.class.getName(),
						resource.getScope(), resource.getPrimKey(),
						roleUser.getRoleId(), new String[] { ActionKeys.VIEW,
								ActionKeys.DELETE, ActionKeys.UPDATE });*/
				ResourceLocalServiceUtil.addResources(companyId, 01, 01 , ExpandoColumn.class.getName(), column.getColumnId(), true, true, true);

				ResourcePermissionLocalServiceUtil.setResourcePermissions(
						companyId, ExpandoColumn.class.getName(),
						ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(column.getColumnId()),
						roleUser.getRoleId(), new String[] { ActionKeys.VIEW,
								ActionKeys.DELETE, ActionKeys.UPDATE });
			} catch (Exception ex) {
				_log.error(ex.getMessage());
			}
		}
		expandoBridge.setAttribute(customFieldName, customFieldValue, false);
	}

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		Company company = themeDisplay.getCompany();
		String cmd = ParamUtil.getString(renderRequest, EECConstants.CMD);
		if (Validator.isNotNull(cmd)
				&& cmd.equals(EECConstants.FORGOT_PASSWORD_RENDER)) {
			try {
				if (!company.isSendPassword()
						&& !company.isSendPasswordResetLink()) {
					viewTemplate = "/html/eec/common/login/login.jsp";
				}
			} catch (SystemException e) {
				_log.info(e.getClass());
			}

			renderResponse.setTitle(themeDisplay.translate("forgot-password"));
			viewTemplate = "/html/eec/common/login/forgot_password.jsp";
		} else {
			viewTemplate = "/html/eec/common/login/login.jsp";
		}
		super.doView(renderRequest, renderResponse);
	}

	private static Log _log = LogFactoryUtil.getLog(LoginAction.class);

}