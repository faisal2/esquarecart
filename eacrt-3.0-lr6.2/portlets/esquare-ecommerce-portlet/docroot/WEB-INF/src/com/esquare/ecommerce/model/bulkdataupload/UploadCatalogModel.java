package com.esquare.ecommerce.model.bulkdataupload;

import java.util.List;

public class UploadCatalogModel {

	private List<String> catalogs;
	
	private Boolean showNavigation;
	
	List<UploadProductModel> uploadProductModel;
	
	public void setShowNavigation(Boolean showNavigation) {
		this.showNavigation = showNavigation;
	}
	
	public Boolean getShowNavigation() {
		return showNavigation;
	}
	
	
	
	public void setUploadProductModel(
			List<UploadProductModel> uploadProductModel) {
		this.uploadProductModel = uploadProductModel;
	}
	
	public List<UploadProductModel> getUploadProductModel() {
		return uploadProductModel;
	}
	
	public void setCatalogs(List<String> catalogs) {
		this.catalogs = catalogs;
	}
	
	public List<String> getCatalogs() {
		return catalogs;
	}
	
}