package com.esquare.ecommerce.product.util;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

public class ECCProductDetailsUtil {

	public static void getProductDetail(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortalException,
			SystemException {
		long productDetailsId = ParamUtil.getLong(renderRequest,
				"productDetailsId");

		ProductDetails productDetails = null;

		if ((productDetailsId > 0)
				&& (productDetailsId != EECConstants.DEFAULT_PARENT_CATALOG_ID)) {

			productDetails = ProductDetailsLocalServiceUtil
					.getProductDetails(productDetailsId);
			renderRequest.setAttribute("PRODUCT_DETAILS", productDetails);
		}

	}public static String getSubCatalogPopupList(long groupId, long catalogId,long productInventoryId,String nameSpace,String relatedProductIds) 
			throws SystemException, PortalException{
			StringBuffer sb = new StringBuffer();
			sb.append("<div style=\"display:block;\" id=\"rowId"+catalogId+"\">");
			getProductList(groupId,catalogId,productInventoryId,sb,relatedProductIds);
			getPopupList(groupId, catalogId, productInventoryId,nameSpace,sb,relatedProductIds);
			sb.append("</div>");
			return sb.toString();
		}

		public static void  getPopupList(long groupId, long catalogId,long productInventoryId, String nameSpace, StringBuffer sb,String relatedProductIds) 
			throws SystemException, PortalException{
			int count = CatalogLocalServiceUtil.getCatalogsCount(groupId, catalogId);
		     if(count > 0) {
		             List list = CatalogLocalServiceUtil.getCatalogs(groupId, catalogId, 0, count);
		             sb.append("<ul id=\"sitemap\">");
		             for (int i=0; i < list.size(); i++) {
		                     Catalog catalog = (Catalog)list.get(i);
		                     catalog = catalog.toEscapedModel();
		                     sb.append("<li style='background:#F2F2F2;'>");
		                     sb.append("<div>");
		                     sb.append("<a href=\"\" >");
		                     sb.append(""+catalog.getName()+"</a><br/>");
		                 	 if(catalog.getParentCatalogId() != 0) getProductList(groupId,catalog.getCatalogId(),productInventoryId,sb,relatedProductIds);
		                     sb.append("</div>");
		                     if(catalog.getParentCatalogId() != 0) {
		                    	 getPopupList(groupId, catalog.getCatalogId(),productInventoryId,nameSpace,sb,relatedProductIds);
		                     }
		                     sb.append("</li>");
		             }
		             sb.append("</ul>");
		     }
			
		}
		private static void getProductList(long groupId,long catalogId,long productInventoryId, StringBuffer sb,String relatedProductIds) throws SystemException, PortalException {
			List<ProductInventory> productInventoryList = ProductInventoryLocalServiceUtil.getRelatedProductList(catalogId);
			if(Validator.isNotNull(productInventoryList)){
					for(ProductInventory inventory:productInventoryList) {
						if(productInventoryId!=inventory.getInventoryId()){
							 ProductDetails productDetails = ProductDetailsLocalServiceUtil.getProductDetails(inventory.getProductDetailsId());
		                     sb.append("<div style='padding-left:50px'>");
		                     sb.append("<input type='checkbox' name='productDetailIds' id=\"productDetailIds_");
		                     sb.append(inventory.getInventoryId());
		                     sb.append("\"value=\"");
		                     sb.append(inventory.getInventoryId());
		                     sb.append("\"");
		                     if(Validator.isNotNull(relatedProductIds) && relatedProductIds.contains(String.valueOf(inventory.getInventoryId()))) {
		                    	 sb.append("checked");
		                     }
		                     sb.append(">&nbsp;&nbsp;"+productDetails.getName());
		                     sb.append("</input><br/></div>");
						}
                     
				}
			}
			
		}
}