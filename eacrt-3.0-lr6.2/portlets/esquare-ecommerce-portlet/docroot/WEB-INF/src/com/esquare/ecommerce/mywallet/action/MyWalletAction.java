package com.esquare.ecommerce.mywallet.action;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.esquare.ecommerce.WalletLimitException;
import com.esquare.ecommerce.model.OrderRefundTracker;
import com.esquare.ecommerce.service.OrderRefundTrackerLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import java.text.SimpleDateFormat;

public class MyWalletAction extends MVCPortlet {
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.EDIT)) {
			updateWallet(actionRequest, actionResponse);
		} else if (cmd.equals(EECConstants.DELETE)) {
			deleteWallet(actionRequest);
		}
		super.processAction(actionRequest, actionResponse);
	}

	private void updateWallet(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long refundTrackerId = ParamUtil.getLong(actionRequest,
				"refundTrackerId");
		String tabStatus = ParamUtil.getString(actionRequest,"tabStatus");
		long userId = 0;
		Date expectedDeliveryDate  = null;
		double claimWalletAmount = ParamUtil.getDouble(actionRequest,
				"claimWalletAmount");
		String claimMethod = ParamUtil.getString(actionRequest, "claimMethod");
		String comments = ParamUtil.getString(actionRequest, "comments");
		String referenceNumber = ParamUtil.getString(actionRequest,
				"referenceNumber");
		double walletAmount = 0;
		String expectedDelivery = ParamUtil.getString(actionRequest,
				"dateV");
		String expectedTime = ParamUtil.getString(actionRequest,"timeV");
		String expectedDateTime = expectedDelivery+" "+expectedTime;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try{
		expectedDeliveryDate = sdf.parse(expectedDateTime);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(expectedDeliveryDate+"::::::::::::::::::::::expectedDeliveryDate:::");
		
		OrderRefundTracker orderRefundTracker = null;
		boolean paymentStatus = Boolean.FALSE;
		try {
			if (refundTrackerId <= 0) {
				walletAmount = EECUtil.myWalletAmount(themeDisplay.getUserId());
				userId = themeDisplay.getUserId();
				if (walletAmount < claimWalletAmount) {
					SessionErrors.add(actionRequest,
							new WalletLimitException().getClass());
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/my_wallet/view.jsp");
					return;
				}
			} else {
				orderRefundTracker = OrderRefundTrackerLocalServiceUtil
						.getOrderRefundTracker(refundTrackerId);
				userId = orderRefundTracker.getUserId();
				//expectedDeliveryDate = getDate(actionRequest, refundTrackerId);
				paymentStatus = Boolean.TRUE;
			}
			
			OrderRefundTrackerLocalServiceUtil.updateOrderRefundTracker(
					refundTrackerId, themeDisplay.getCompanyId(), userId,
					referenceNumber, claimWalletAmount, paymentStatus,
					claimMethod, comments, expectedDeliveryDate);
			actionResponse.setRenderParameter("tabs1",tabStatus);

		} catch (Exception e) {
			_log.info(e.getClass());
		}

	}

	public Date getDate(ActionRequest actionRequest, long refundTrackerId) {
		ThemeDisplay themedisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		int month = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "m" + refundTrackerId : "m");
		int day = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "d" + refundTrackerId : "d");
		int year = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "y" + refundTrackerId : "y");
		int hour = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "hour" + refundTrackerId : "hour");
		int minute = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "min" + refundTrackerId : "min");
		int amPm = ParamUtil.getInteger(actionRequest,
				(refundTrackerId > 0) ? "ampm" + refundTrackerId : "ampm");
		if (amPm == Calendar.PM) {
			hour += 12;
		}
		try {
			return PortalUtil.getDate(month, day, year, hour, minute,
					themedisplay.getUser().getTimeZone(),
					(Class<? extends PortalException>) null);
		} catch (PortalException pe) {
			return null;
		}
	}

	private void deleteWallet(ActionRequest actionRequest) {
		long[] deleteRefundTrackerIds = StringUtil.split(
				ParamUtil.getString(actionRequest, "deleteRefundTrackerIds"),
				0L);
		if (Validator.isNotNull(deleteRefundTrackerIds)) {
			for (int i = 0; i < deleteRefundTrackerIds.length; i++) {
				try {
					OrderRefundTrackerLocalServiceUtil
							.deleteOrderRefundTracker(deleteRefundTrackerIds[i]);
				} catch (Exception e) {
					_log.info(e.getClass());
				}
			}
		}

	}

	private static Log _log = LogFactoryUtil.getLog(MyWalletAction.class);
}
