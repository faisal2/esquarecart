package com.esquare.ecommerce.util;

import java.net.URL;
import java.net.URLConnection;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class DNSSettings {

	private static String ZONOMI_URL = "https://zonomi.com/app/dns/dyndns.jsp?";

	private static String IP_ADDRESS = "52.4.55.107";

	private static String API_KEY = "34837668156563731553467770894076522487";

	public static void createDomain(String domainName) {

		// "https://zonomi.com/app/dns/dyndns.jsp?action=SET&name=saq.esquarecart.com&value=54.214.16.8&type=A&api_key="+API_KEY

		StringBuffer url = new StringBuffer();
		url.append(ZONOMI_URL);
		url.append("action=SET&name=");
		url.append(domainName);
		url.append("&value=");
		url.append(IP_ADDRESS);
		url.append("&type=A&api_key=");
		url.append(API_KEY);
		runURL(url.toString());

	}

	public static void deleteDomain(String domainName) {

		// "https://zonomi.com/app/dns/dyndns.jsp?action=DELETE&name=example.com&type=A&api_key=apikeyvaluehere

		StringBuffer url = new StringBuffer();
		url.append(ZONOMI_URL);
		url.append("action=DELETE&name=");
		url.append(domainName);
		url.append("&type=A&api_key=");
		url.append(API_KEY);
		runURL(url.toString());

	}

	public static void updateDomain(String oldDomainName, String newDomainName) {
		deleteDomain(oldDomainName);
		createDomain(newDomainName);
	}

	private static void runURL(String connectionURL) {
		URL url;
		try {

			url = new URL(connectionURL);
			URLConnection conn = url.openConnection();
			conn.getInputStream();

		} catch (Exception e) {
			_log.info(e.getClass());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(DNSSettings.class);
}