
package com.esquare.ecommerce.template.action;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.TemplateManager;
import com.esquare.ecommerce.model.impl.TemplateManagerImpl;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.TemplateManagerLocalServiceUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.lar.PortletDataHandlerKeys;
import com.liferay.portal.kernel.lar.UserIdStrategy;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.NoSuchFileEntryException;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class TemplateManager
 */
public class TemplateManagerAction extends MVCPortlet {

	public void addTemplate(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		try {

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
					.getAttribute(WebKeys.THEME_DISPLAY);

			UploadPortletRequest uploadPortletRequest = PortalUtil
					.getUploadPortletRequest(actionRequest);

			String cmd = uploadPortletRequest.getParameter(Constants.CMD);
			String templateRecId = uploadPortletRequest.getParameter("recId");
			String title = uploadPortletRequest.getParameter("title");
			String larName = uploadPortletRequest.getParameter("larName");
			String templateDesc = uploadPortletRequest
					.getParameter("templateDesc");
			String packageType = uploadPortletRequest
					.getParameter("packageType");
			String cssId = uploadPortletRequest.getParameter("cssId");

			long groupId = GroupLocalServiceUtil.getGroup(
					themeDisplay.getCompanyId(), GroupConstants.GUEST)
					.getGroupId();

			String formFieldsIndexe = uploadPortletRequest
					.getParameter("formFieldsIndexes");
			String[] formFieldsIndexes = formFieldsIndexe.split(",");
			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					DLFileEntry.class.getName(), actionRequest);

			Folder parentFolder;
			Folder folder = null;

			if (Validator.isNotNull(cmd) && cmd.equals(Constants.EDIT)) {
				String folderId = uploadPortletRequest.getParameter("folderId");
				folder = DLAppServiceUtil.getFolder(Long.parseLong(folderId));
			} else {
				try {
					parentFolder = DLAppServiceUtil.getFolder(groupId, 0L,
							PortletPropsValues.TEMPLATE_ECOMMERCE_TEMPLATES);
				} catch (Exception e) {
					parentFolder = DLAppServiceUtil.addFolder(groupId, 0L,
							PortletPropsValues.TEMPLATE_ECOMMERCE_TEMPLATES,
							PortletPropsValues.TEMPLATE_ECOMMERCE_TEMPLATES,
							serviceContext);
				}
				folder = DLAppServiceUtil.addFolder(
						parentFolder.getRepositoryId(),
						parentFolder.getFolderId(), (larName + cssId),
						templateDesc, serviceContext);
			}

			long repositoryIds = folder.getRepositoryId();
			long folderId = folder.getFolderId();
			long fileEntryTypeId = 0L;
			String sourceFileName = StringPool.BLANK;
			File file = null;
			long size = 0l;
			String fieldName = PortletPropsValues.TEMPLATE_ADD_IMAGE;
			String imageNames = StringPool.BLANK;

			for (int i = 0; i < formFieldsIndexes.length; i++) {

				if (i > 0
						|| (Validator.isNotNull(cmd) && cmd
								.equals(Constants.EDIT))) {
					fieldName = PortletPropsValues.TEMPLATE_ADD_IMAGE
							+ formFieldsIndexes[i];
				}

				imageNames += PortletPropsValues.TEMPLATE_IMAGE
						+ formFieldsIndexes[i] + ",";
				sourceFileName = uploadPortletRequest.getFileName(fieldName);
				file = uploadPortletRequest.getFile(fieldName);
				size = uploadPortletRequest.getSize(fieldName);

				if (file != null) {
					DLFileEntry dlFileEntry = null;
					if (Validator.isNotNull(cmd) && cmd.equals(Constants.EDIT)) {
						try {
							DLFileEntry dLFileEntry = DLFileEntryLocalServiceUtil
									.getFileEntry(groupId, folderId,
											PortletPropsValues.TEMPLATE_IMAGE
													+ (i + 1));

							if (Validator.isNotNull(sourceFileName)) {
								dlFileEntry = DLFileEntryLocalServiceUtil
										.updateFileEntry(
												themeDisplay.getUserId(),
												dLFileEntry.getFileEntryId(),
												sourceFileName,
												MimeTypesUtil
														.getContentType(file),
												(PortletPropsValues.TEMPLATE_IMAGE + (i + 1)),
												(PortletPropsValues.TEMPLATE_IMAGE + (i + 1)),
												StringPool.BLANK, true,
												fileEntryTypeId, null, file,
												null, size, serviceContext);
							}
						} catch (NoSuchFileEntryException e) {
//							check here
							
							DLAppServiceUtil.addFileEntry(repositoryIds, folderId, sourceFileName, MimeTypesUtil.getContentType(file), (PortletPropsValues.TEMPLATE_IMAGE + (i + 1)),
									(PortletPropsValues.TEMPLATE_IMAGE + (i + 1)), StringPool.BLANK, file, serviceContext);
						}
					} else {
						
//						check here
						
						 
						DLAppServiceUtil.addFileEntry(repositoryIds, folderId, sourceFileName, MimeTypesUtil.getContentType(file), (PortletPropsValues.TEMPLATE_IMAGE + (i + 1)),
								(PortletPropsValues.TEMPLATE_IMAGE + (i + 1)), StringPool.BLANK, file, serviceContext);
					}
				}
			}

			if (Validator.isNotNull(cmd) && cmd.equals(Constants.EDIT)) {
				List<DLFileEntry> dlFileEntries = DLFileEntryLocalServiceUtil
						.getFileEntries(groupId, folderId, 0, -1, null);
				for (DLFileEntry dLFileEntry : dlFileEntries) {
					if (!imageNames.contains(dLFileEntry.getTitle())) {
						DLFileEntryLocalServiceUtil
								.deleteDLFileEntry(dLFileEntry.getFileEntryId());
					}
				}
			}

			TemplateManager templateManager = null;
			if (Validator.isNotNull(cmd) && cmd.equals(Constants.EDIT)) {
				templateManager = TemplateManagerLocalServiceUtil
						.getTemplateManager(Long.parseLong(templateRecId));
				templateManager.setRecId(Long.parseLong(templateRecId));
			} else {
				templateManager = new TemplateManagerImpl();
				long recId = CounterLocalServiceUtil.increment(this.getClass()
						.getName());
				templateManager.setRecId(recId);
			}

			try {
				templateManager.setTitle(title);
				templateManager.setCssId(cssId);
				templateManager.setLarName(larName);
				templateManager.setTemplateDesc(templateDesc);
				templateManager.setPackageType(packageType);
				templateManager.setFolderId(folderId);
				templateManager.setModifiedDate(new Date());
				if (Validator.isNotNull(cmd) && cmd.equals(Constants.EDIT)) {
					TemplateManagerLocalServiceUtil
							.updateTemplateManager(templateManager);
				} else {
					TemplateManagerLocalServiceUtil
							.addTemplateManager(templateManager);
				}
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	public void applylar(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException,
			SystemException {
		String templateName = ParamUtil
				.getString(actionRequest, "templateName");
		System.out.println("Tempalate name >>>"+templateName);
		String cssId = ParamUtil.getString(actionRequest, "cssId");

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		Instance instance = InstanceLocalServiceUtil.getInstance(themeDisplay
				.getCompanyId());
		long groupId = GroupLocalServiceUtil.getGroup(
				themeDisplay.getCompanyId(), GroupConstants.GUEST).getGroupId();
		if (!instance.getCurrentTemplate().equals(templateName)) {
			File b2bLar = new File(
					PortletPropsValues.DEFAULT_TEMPLATE_LAR_FOLDER_PATH
							+ templateName);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					User.class.getName(), actionRequest);

			// updating package name in instance
			instance.setCurrentTemplate(templateName);
			InstanceLocalServiceUtil.updateInstance(instance);

			addDefaultLayoutsByLAR(themeDisplay.getUserId(), groupId, false,
					b2bLar, serviceContext);
		}

		try {
			LayoutSet layoutSet = LayoutSetLocalServiceUtil.getLayoutSet(
					groupId, false);
			layoutSet.setColorSchemeId(cssId);
			LayoutSetLocalServiceUtil.updateLayoutSet(layoutSet);
			SessionMessages.add(actionRequest, "request_processed",
					"theme-applied-successfully");
		} catch (Exception e) {
		}
	}

	protected static Map<String, String[]> getLayoutSetPrototypesParameters(
			boolean importData) {

//		check here
		
		Map<String, String[]> parameterMap = new LinkedHashMap<String, String[]>();

		parameterMap.put(PortletDataHandlerKeys.CATEGORIES,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.DELETE_MISSING_LAYOUTS,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.DELETE_PORTLET_DATA,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.IGNORE_LAST_PUBLISH_DATE,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.LAYOUT_SET_SETTINGS,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(
				PortletDataHandlerKeys.LAYOUT_SET_PROTOTYPE_LINK_ENABLED,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PERMISSIONS,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_CONFIGURATION,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_CONFIGURATION_ALL,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_SETUP_ALL,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.THEME_REFERENCE,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.UPDATE_LAST_PUBLISH_DATE,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.USER_ID_STRATEGY,
				new String[] { UserIdStrategy.CURRENT_USER_ID });

		if (importData) {
			parameterMap
					.put(PortletDataHandlerKeys.DATA_STRATEGY,
							new String[] { PortletDataHandlerKeys.DATA_STRATEGY_MIRROR });
			parameterMap.put(PortletDataHandlerKeys.LOGO,
					new String[] { Boolean.TRUE.toString() });
			parameterMap.put(PortletDataHandlerKeys.PORTLET_DATA,
					new String[] { Boolean.TRUE.toString() });
			parameterMap.put(PortletDataHandlerKeys.PORTLET_DATA_ALL,
					new String[] { Boolean.TRUE.toString() });
		}
		return parameterMap;
	}

	protected void addDefaultLayoutsByLAR(long userId, long groupId,
			boolean privateLayout, File larFile, ServiceContext serviceContext) {
		System.out.println("larFile>>>>"+larFile.getAbsolutePath());

		try {
			LayoutLocalServiceUtil.importLayouts(userId, groupId,
					privateLayout, getLayoutSetPrototypesParameters(true),
					larFile);
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	private void deleteTemp(ActionRequest actionRequest,
			ActionResponse actionResponse) throws NumberFormatException,
			PortalException, SystemException {
		String[] templateRecIds = actionRequest
				.getParameterValues("deleteItem");
		if (Validator.isNotNull(templateRecIds)) {
			for (int i = 0; i < templateRecIds.length; i++) {
				TemplateManager templateManager = TemplateManagerLocalServiceUtil
						.getTemplateManager(Long.parseLong(templateRecIds[i]));
				DLAppServiceUtil.deleteFolder(templateManager.getFolderId());
				TemplateManagerLocalServiceUtil
						.deleteTemplateManager(templateManager);
			}
		}
	}

	public void tempDelete(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);
		try {
			if (cmd.equals(Constants.DELETE)) {
				deleteTemp(actionRequest, actionResponse);
			}
		} catch (Exception e) {
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(renderRequest, Constants.CMD);
		if (Validator.isNotNull(cmd) && cmd.equalsIgnoreCase(Constants.EDIT)) {
			String templateRecId = ParamUtil.getString(renderRequest, "tempId");
			renderRequest.setAttribute("templateId", templateRecId);

			viewTemplate = "/html/eec/common/template_manager/edit_template.jsp";
		} else {
			viewTemplate = "/html/eec/common/template_manager/view.jsp";
		}

		super.doView(renderRequest, renderResponse);
	}

	private static Log _log = LogFactoryUtil
			.getLog(TemplateManagerAction.class);
}
