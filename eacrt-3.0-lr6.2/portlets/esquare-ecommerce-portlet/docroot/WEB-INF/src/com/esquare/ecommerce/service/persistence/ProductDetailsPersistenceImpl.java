/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchProductDetailsException;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.impl.ProductDetailsImpl;
import com.esquare.ecommerce.model.impl.ProductDetailsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the product details service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see ProductDetailsPersistence
 * @see ProductDetailsUtil
 * @generated
 */
public class ProductDetailsPersistenceImpl extends BasePersistenceImpl<ProductDetails>
	implements ProductDetailsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductDetailsUtil} to access the product details persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductDetailsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			ProductDetailsModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductDetailsModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the product detailses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the product detailses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @return the range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByCompanyId(long companyId, int start,
		int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the product detailses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByCompanyId(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ProductDetails> list = (List<ProductDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ProductDetails productDetails : list) {
				if ((companyId != productDetails.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductDetails>(list);
				}
				else {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ProductDetails> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ProductDetails> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the product detailses before and after the current product details in the ordered set where companyId = &#63;.
	 *
	 * @param productDetailsId the primary key of the current product details
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails[] findByCompanyId_PrevAndNext(long productDetailsId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = findByPrimaryKey(productDetailsId);

		Session session = null;

		try {
			session = openSession();

			ProductDetails[] array = new ProductDetailsImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, productDetails,
					companyId, orderByComparator, true);

			array[1] = productDetails;

			array[2] = getByCompanyId_PrevAndNext(session, productDetails,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductDetails getByCompanyId_PrevAndNext(Session session,
		ProductDetails productDetails, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productDetails);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductDetails> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the product detailses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (ProductDetails productDetails : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(productDetails);
		}
	}

	/**
	 * Returns the number of product detailses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "productDetails.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCTDETAILSLIST =
		new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByProductDetailsList",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTDETAILSLIST =
		new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProductDetailsList",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			ProductDetailsModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductDetailsModelImpl.VISIBILITY_COLUMN_BITMASK |
			ProductDetailsModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PRODUCTDETAILSLIST = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProductDetailsList",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the product detailses where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @return the matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByProductDetailsList(long companyId,
		boolean visibility) throws SystemException {
		return findByProductDetailsList(companyId, visibility,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product detailses where companyId = &#63; and visibility = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @return the range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByProductDetailsList(long companyId,
		boolean visibility, int start, int end) throws SystemException {
		return findByProductDetailsList(companyId, visibility, start, end, null);
	}

	/**
	 * Returns an ordered range of all the product detailses where companyId = &#63; and visibility = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByProductDetailsList(long companyId,
		boolean visibility, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTDETAILSLIST;
			finderArgs = new Object[] { companyId, visibility };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCTDETAILSLIST;
			finderArgs = new Object[] {
					companyId, visibility,
					
					start, end, orderByComparator
				};
		}

		List<ProductDetails> list = (List<ProductDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ProductDetails productDetails : list) {
				if ((companyId != productDetails.getCompanyId()) ||
						(visibility != productDetails.getVisibility())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_COMPANYID_2);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_VISIBILITY_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(visibility);

				if (!pagination) {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductDetails>(list);
				}
				else {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByProductDetailsList_First(long companyId,
		boolean visibility, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByProductDetailsList_First(companyId,
				visibility, orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", visibility=");
		msg.append(visibility);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByProductDetailsList_First(long companyId,
		boolean visibility, OrderByComparator orderByComparator)
		throws SystemException {
		List<ProductDetails> list = findByProductDetailsList(companyId,
				visibility, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByProductDetailsList_Last(long companyId,
		boolean visibility, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByProductDetailsList_Last(companyId,
				visibility, orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", visibility=");
		msg.append(visibility);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByProductDetailsList_Last(long companyId,
		boolean visibility, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByProductDetailsList(companyId, visibility);

		if (count == 0) {
			return null;
		}

		List<ProductDetails> list = findByProductDetailsList(companyId,
				visibility, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the product detailses before and after the current product details in the ordered set where companyId = &#63; and visibility = &#63;.
	 *
	 * @param productDetailsId the primary key of the current product details
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails[] findByProductDetailsList_PrevAndNext(
		long productDetailsId, long companyId, boolean visibility,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = findByPrimaryKey(productDetailsId);

		Session session = null;

		try {
			session = openSession();

			ProductDetails[] array = new ProductDetailsImpl[3];

			array[0] = getByProductDetailsList_PrevAndNext(session,
					productDetails, companyId, visibility, orderByComparator,
					true);

			array[1] = productDetails;

			array[2] = getByProductDetailsList_PrevAndNext(session,
					productDetails, companyId, visibility, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductDetails getByProductDetailsList_PrevAndNext(
		Session session, ProductDetails productDetails, long companyId,
		boolean visibility, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

		query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_COMPANYID_2);

		query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_VISIBILITY_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(visibility);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productDetails);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductDetails> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the product detailses where companyId = &#63; and visibility = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByProductDetailsList(long companyId, boolean visibility)
		throws SystemException {
		for (ProductDetails productDetails : findByProductDetailsList(
				companyId, visibility, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(productDetails);
		}
	}

	/**
	 * Returns the number of product detailses where companyId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param visibility the visibility
	 * @return the number of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByProductDetailsList(long companyId, boolean visibility)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PRODUCTDETAILSLIST;

		Object[] finderArgs = new Object[] { companyId, visibility };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_COMPANYID_2);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSLIST_VISIBILITY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(visibility);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PRODUCTDETAILSLIST_COMPANYID_2 = "productDetails.companyId = ? AND ";
	private static final String _FINDER_COLUMN_PRODUCTDETAILSLIST_VISIBILITY_2 = "productDetails.visibility = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_C_V = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_C_V",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C_V = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_C_V",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Boolean.class.getName()
			},
			ProductDetailsModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductDetailsModelImpl.CATALOGID_COLUMN_BITMASK |
			ProductDetailsModelImpl.VISIBILITY_COLUMN_BITMASK |
			ProductDetailsModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_C_V = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_C_V",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns all the product detailses where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @return the matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C_V(long companyId, long catalogId,
		boolean visibility) throws SystemException {
		return findByC_C_V(companyId, catalogId, visibility, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product detailses where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @return the range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C_V(long companyId, long catalogId,
		boolean visibility, int start, int end) throws SystemException {
		return findByC_C_V(companyId, catalogId, visibility, start, end, null);
	}

	/**
	 * Returns an ordered range of all the product detailses where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C_V(long companyId, long catalogId,
		boolean visibility, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C_V;
			finderArgs = new Object[] { companyId, catalogId, visibility };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_C_V;
			finderArgs = new Object[] {
					companyId, catalogId, visibility,
					
					start, end, orderByComparator
				};
		}

		List<ProductDetails> list = (List<ProductDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ProductDetails productDetails : list) {
				if ((companyId != productDetails.getCompanyId()) ||
						(catalogId != productDetails.getCatalogId()) ||
						(visibility != productDetails.getVisibility())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_C_C_V_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_C_V_CATALOGID_2);

			query.append(_FINDER_COLUMN_C_C_V_VISIBILITY_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(catalogId);

				qPos.add(visibility);

				if (!pagination) {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductDetails>(list);
				}
				else {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByC_C_V_First(long companyId, long catalogId,
		boolean visibility, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByC_C_V_First(companyId,
				catalogId, visibility, orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", catalogId=");
		msg.append(catalogId);

		msg.append(", visibility=");
		msg.append(visibility);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByC_C_V_First(long companyId, long catalogId,
		boolean visibility, OrderByComparator orderByComparator)
		throws SystemException {
		List<ProductDetails> list = findByC_C_V(companyId, catalogId,
				visibility, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByC_C_V_Last(long companyId, long catalogId,
		boolean visibility, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByC_C_V_Last(companyId, catalogId,
				visibility, orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", catalogId=");
		msg.append(catalogId);

		msg.append(", visibility=");
		msg.append(visibility);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByC_C_V_Last(long companyId, long catalogId,
		boolean visibility, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByC_C_V(companyId, catalogId, visibility);

		if (count == 0) {
			return null;
		}

		List<ProductDetails> list = findByC_C_V(companyId, catalogId,
				visibility, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the product detailses before and after the current product details in the ordered set where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param productDetailsId the primary key of the current product details
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails[] findByC_C_V_PrevAndNext(long productDetailsId,
		long companyId, long catalogId, boolean visibility,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = findByPrimaryKey(productDetailsId);

		Session session = null;

		try {
			session = openSession();

			ProductDetails[] array = new ProductDetailsImpl[3];

			array[0] = getByC_C_V_PrevAndNext(session, productDetails,
					companyId, catalogId, visibility, orderByComparator, true);

			array[1] = productDetails;

			array[2] = getByC_C_V_PrevAndNext(session, productDetails,
					companyId, catalogId, visibility, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductDetails getByC_C_V_PrevAndNext(Session session,
		ProductDetails productDetails, long companyId, long catalogId,
		boolean visibility, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

		query.append(_FINDER_COLUMN_C_C_V_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_C_V_CATALOGID_2);

		query.append(_FINDER_COLUMN_C_C_V_VISIBILITY_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(catalogId);

		qPos.add(visibility);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productDetails);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductDetails> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the product detailses where companyId = &#63; and catalogId = &#63; and visibility = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_C_V(long companyId, long catalogId, boolean visibility)
		throws SystemException {
		for (ProductDetails productDetails : findByC_C_V(companyId, catalogId,
				visibility, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(productDetails);
		}
	}

	/**
	 * Returns the number of product detailses where companyId = &#63; and catalogId = &#63; and visibility = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param visibility the visibility
	 * @return the number of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_C_V(long companyId, long catalogId, boolean visibility)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_C_V;

		Object[] finderArgs = new Object[] { companyId, catalogId, visibility };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_C_C_V_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_C_V_CATALOGID_2);

			query.append(_FINDER_COLUMN_C_C_V_VISIBILITY_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(catalogId);

				qPos.add(visibility);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_C_V_COMPANYID_2 = "productDetails.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_C_V_CATALOGID_2 = "productDetails.catalogId = ? AND ";
	private static final String _FINDER_COLUMN_C_C_V_VISIBILITY_2 = "productDetails.visibility = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_C = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByC_C",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_C",
			new String[] { Long.class.getName(), Long.class.getName() },
			ProductDetailsModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductDetailsModelImpl.CATALOGID_COLUMN_BITMASK |
			ProductDetailsModelImpl.CREATEDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_C = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_C",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the product detailses where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @return the matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C(long companyId, long catalogId)
		throws SystemException {
		return findByC_C(companyId, catalogId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product detailses where companyId = &#63; and catalogId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @return the range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C(long companyId, long catalogId,
		int start, int end) throws SystemException {
		return findByC_C(companyId, catalogId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the product detailses where companyId = &#63; and catalogId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findByC_C(long companyId, long catalogId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C;
			finderArgs = new Object[] { companyId, catalogId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_C;
			finderArgs = new Object[] {
					companyId, catalogId,
					
					start, end, orderByComparator
				};
		}

		List<ProductDetails> list = (List<ProductDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ProductDetails productDetails : list) {
				if ((companyId != productDetails.getCompanyId()) ||
						(catalogId != productDetails.getCatalogId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_C_C_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_C_CATALOGID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(catalogId);

				if (!pagination) {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductDetails>(list);
				}
				else {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByC_C_First(long companyId, long catalogId,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByC_C_First(companyId, catalogId,
				orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", catalogId=");
		msg.append(catalogId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the first product details in the ordered set where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByC_C_First(long companyId, long catalogId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ProductDetails> list = findByC_C(companyId, catalogId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByC_C_Last(long companyId, long catalogId,
		OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByC_C_Last(companyId, catalogId,
				orderByComparator);

		if (productDetails != null) {
			return productDetails;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", catalogId=");
		msg.append(catalogId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductDetailsException(msg.toString());
	}

	/**
	 * Returns the last product details in the ordered set where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByC_C_Last(long companyId, long catalogId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_C(companyId, catalogId);

		if (count == 0) {
			return null;
		}

		List<ProductDetails> list = findByC_C(companyId, catalogId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the product detailses before and after the current product details in the ordered set where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param productDetailsId the primary key of the current product details
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails[] findByC_C_PrevAndNext(long productDetailsId,
		long companyId, long catalogId, OrderByComparator orderByComparator)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = findByPrimaryKey(productDetailsId);

		Session session = null;

		try {
			session = openSession();

			ProductDetails[] array = new ProductDetailsImpl[3];

			array[0] = getByC_C_PrevAndNext(session, productDetails, companyId,
					catalogId, orderByComparator, true);

			array[1] = productDetails;

			array[2] = getByC_C_PrevAndNext(session, productDetails, companyId,
					catalogId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductDetails getByC_C_PrevAndNext(Session session,
		ProductDetails productDetails, long companyId, long catalogId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

		query.append(_FINDER_COLUMN_C_C_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_C_CATALOGID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductDetailsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(catalogId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productDetails);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductDetails> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the product detailses where companyId = &#63; and catalogId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_C(long companyId, long catalogId)
		throws SystemException {
		for (ProductDetails productDetails : findByC_C(companyId, catalogId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(productDetails);
		}
	}

	/**
	 * Returns the number of product detailses where companyId = &#63; and catalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param catalogId the catalog ID
	 * @return the number of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_C(long companyId, long catalogId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_C;

		Object[] finderArgs = new Object[] { companyId, catalogId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_C_C_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_C_CATALOGID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(catalogId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_C_COMPANYID_2 = "productDetails.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_C_CATALOGID_2 = "productDetails.catalogId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_NAME = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED,
			ProductDetailsImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByName",
			new String[] { Long.class.getName(), String.class.getName() },
			ProductDetailsModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductDetailsModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the product details where companyId = &#63; and name = &#63; or throws a {@link com.esquare.ecommerce.NoSuchProductDetailsException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the matching product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByName(long companyId, String name)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByName(companyId, name);

		if (productDetails == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", name=");
			msg.append(name);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductDetailsException(msg.toString());
		}

		return productDetails;
	}

	/**
	 * Returns the product details where companyId = &#63; and name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByName(long companyId, String name)
		throws SystemException {
		return fetchByName(companyId, name, true);
	}

	/**
	 * Returns the product details where companyId = &#63; and name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching product details, or <code>null</code> if a matching product details could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByName(long companyId, String name,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, name };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_NAME,
					finderArgs, this);
		}

		if (result instanceof ProductDetails) {
			ProductDetails productDetails = (ProductDetails)result;

			if ((companyId != productDetails.getCompanyId()) ||
					!Validator.equals(name, productDetails.getName())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_NAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				List<ProductDetails> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
						finderArgs, list);
				}
				else {
					ProductDetails productDetails = list.get(0);

					result = productDetails;

					cacheResult(productDetails);

					if ((productDetails.getCompanyId() != companyId) ||
							(productDetails.getName() == null) ||
							!productDetails.getName().equals(name)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
							finderArgs, productDetails);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProductDetails)result;
		}
	}

	/**
	 * Removes the product details where companyId = &#63; and name = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the product details that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails removeByName(long companyId, String name)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = findByName(companyId, name);

		return remove(productDetails);
	}

	/**
	 * Returns the number of product detailses where companyId = &#63; and name = &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the number of matching product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByName(long companyId, String name)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

		Object[] finderArgs = new Object[] { companyId, name };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PRODUCTDETAILS_WHERE);

			query.append(_FINDER_COLUMN_NAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_NAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_NAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NAME_COMPANYID_2 = "productDetails.companyId = ? AND ";
	private static final String _FINDER_COLUMN_NAME_NAME_1 = "productDetails.name IS NULL";
	private static final String _FINDER_COLUMN_NAME_NAME_2 = "productDetails.name = ?";
	private static final String _FINDER_COLUMN_NAME_NAME_3 = "(productDetails.name IS NULL OR productDetails.name = '')";

	public ProductDetailsPersistenceImpl() {
		setModelClass(ProductDetails.class);
	}

	/**
	 * Caches the product details in the entity cache if it is enabled.
	 *
	 * @param productDetails the product details
	 */
	@Override
	public void cacheResult(ProductDetails productDetails) {
		EntityCacheUtil.putResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsImpl.class, productDetails.getPrimaryKey(),
			productDetails);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
			new Object[] { productDetails.getCompanyId(), productDetails.getName() },
			productDetails);

		productDetails.resetOriginalValues();
	}

	/**
	 * Caches the product detailses in the entity cache if it is enabled.
	 *
	 * @param productDetailses the product detailses
	 */
	@Override
	public void cacheResult(List<ProductDetails> productDetailses) {
		for (ProductDetails productDetails : productDetailses) {
			if (EntityCacheUtil.getResult(
						ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
						ProductDetailsImpl.class, productDetails.getPrimaryKey()) == null) {
				cacheResult(productDetails);
			}
			else {
				productDetails.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all product detailses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductDetailsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductDetailsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the product details.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProductDetails productDetails) {
		EntityCacheUtil.removeResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsImpl.class, productDetails.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(productDetails);
	}

	@Override
	public void clearCache(List<ProductDetails> productDetailses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProductDetails productDetails : productDetailses) {
			EntityCacheUtil.removeResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
				ProductDetailsImpl.class, productDetails.getPrimaryKey());

			clearUniqueFindersCache(productDetails);
		}
	}

	protected void cacheUniqueFindersCache(ProductDetails productDetails) {
		if (productDetails.isNew()) {
			Object[] args = new Object[] {
					productDetails.getCompanyId(), productDetails.getName()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME, args,
				productDetails);
		}
		else {
			ProductDetailsModelImpl productDetailsModelImpl = (ProductDetailsModelImpl)productDetails;

			if ((productDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_NAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productDetails.getCompanyId(), productDetails.getName()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME, args,
					productDetails);
			}
		}
	}

	protected void clearUniqueFindersCache(ProductDetails productDetails) {
		ProductDetailsModelImpl productDetailsModelImpl = (ProductDetailsModelImpl)productDetails;

		Object[] args = new Object[] {
				productDetails.getCompanyId(), productDetails.getName()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME, args);

		if ((productDetailsModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_NAME.getColumnBitmask()) != 0) {
			args = new Object[] {
					productDetailsModelImpl.getOriginalCompanyId(),
					productDetailsModelImpl.getOriginalName()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME, args);
		}
	}

	/**
	 * Creates a new product details with the primary key. Does not add the product details to the database.
	 *
	 * @param productDetailsId the primary key for the new product details
	 * @return the new product details
	 */
	@Override
	public ProductDetails create(long productDetailsId) {
		ProductDetails productDetails = new ProductDetailsImpl();

		productDetails.setNew(true);
		productDetails.setPrimaryKey(productDetailsId);

		return productDetails;
	}

	/**
	 * Removes the product details with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param productDetailsId the primary key of the product details
	 * @return the product details that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails remove(long productDetailsId)
		throws NoSuchProductDetailsException, SystemException {
		return remove((Serializable)productDetailsId);
	}

	/**
	 * Removes the product details with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the product details
	 * @return the product details that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails remove(Serializable primaryKey)
		throws NoSuchProductDetailsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ProductDetails productDetails = (ProductDetails)session.get(ProductDetailsImpl.class,
					primaryKey);

			if (productDetails == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductDetailsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(productDetails);
		}
		catch (NoSuchProductDetailsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProductDetails removeImpl(ProductDetails productDetails)
		throws SystemException {
		productDetails = toUnwrappedModel(productDetails);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(productDetails)) {
				productDetails = (ProductDetails)session.get(ProductDetailsImpl.class,
						productDetails.getPrimaryKeyObj());
			}

			if (productDetails != null) {
				session.delete(productDetails);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (productDetails != null) {
			clearCache(productDetails);
		}

		return productDetails;
	}

	@Override
	public ProductDetails updateImpl(
		com.esquare.ecommerce.model.ProductDetails productDetails)
		throws SystemException {
		productDetails = toUnwrappedModel(productDetails);

		boolean isNew = productDetails.isNew();

		ProductDetailsModelImpl productDetailsModelImpl = (ProductDetailsModelImpl)productDetails;

		Session session = null;

		try {
			session = openSession();

			if (productDetails.isNew()) {
				session.save(productDetails);

				productDetails.setNew(false);
			}
			else {
				session.merge(productDetails);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductDetailsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((productDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productDetailsModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { productDetailsModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((productDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTDETAILSLIST.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productDetailsModelImpl.getOriginalCompanyId(),
						productDetailsModelImpl.getOriginalVisibility()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTDETAILSLIST,
					args);

				args = new Object[] {
						productDetailsModelImpl.getCompanyId(),
						productDetailsModelImpl.getVisibility()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSLIST,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTDETAILSLIST,
					args);
			}

			if ((productDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C_V.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productDetailsModelImpl.getOriginalCompanyId(),
						productDetailsModelImpl.getOriginalCatalogId(),
						productDetailsModelImpl.getOriginalVisibility()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_V, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C_V,
					args);

				args = new Object[] {
						productDetailsModelImpl.getCompanyId(),
						productDetailsModelImpl.getCatalogId(),
						productDetailsModelImpl.getVisibility()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C_V, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C_V,
					args);
			}

			if ((productDetailsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productDetailsModelImpl.getOriginalCompanyId(),
						productDetailsModelImpl.getOriginalCatalogId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C,
					args);

				args = new Object[] {
						productDetailsModelImpl.getCompanyId(),
						productDetailsModelImpl.getCatalogId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_C,
					args);
			}
		}

		EntityCacheUtil.putResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
			ProductDetailsImpl.class, productDetails.getPrimaryKey(),
			productDetails);

		clearUniqueFindersCache(productDetails);
		cacheUniqueFindersCache(productDetails);

		return productDetails;
	}

	protected ProductDetails toUnwrappedModel(ProductDetails productDetails) {
		if (productDetails instanceof ProductDetailsImpl) {
			return productDetails;
		}

		ProductDetailsImpl productDetailsImpl = new ProductDetailsImpl();

		productDetailsImpl.setNew(productDetails.isNew());
		productDetailsImpl.setPrimaryKey(productDetails.getPrimaryKey());

		productDetailsImpl.setProductDetailsId(productDetails.getProductDetailsId());
		productDetailsImpl.setCompanyId(productDetails.getCompanyId());
		productDetailsImpl.setUserId(productDetails.getUserId());
		productDetailsImpl.setCreateDate(productDetails.getCreateDate());
		productDetailsImpl.setModifiedDate(productDetails.getModifiedDate());
		productDetailsImpl.setCatalogId(productDetails.getCatalogId());
		productDetailsImpl.setName(productDetails.getName());
		productDetailsImpl.setDescription(productDetails.getDescription());
		productDetailsImpl.setVisibility(productDetails.isVisibility());
		productDetailsImpl.setRelatedProductIds(productDetails.getRelatedProductIds());

		return productDetailsImpl;
	}

	/**
	 * Returns the product details with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the product details
	 * @return the product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductDetailsException, SystemException {
		ProductDetails productDetails = fetchByPrimaryKey(primaryKey);

		if (productDetails == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductDetailsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return productDetails;
	}

	/**
	 * Returns the product details with the primary key or throws a {@link com.esquare.ecommerce.NoSuchProductDetailsException} if it could not be found.
	 *
	 * @param productDetailsId the primary key of the product details
	 * @return the product details
	 * @throws com.esquare.ecommerce.NoSuchProductDetailsException if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails findByPrimaryKey(long productDetailsId)
		throws NoSuchProductDetailsException, SystemException {
		return findByPrimaryKey((Serializable)productDetailsId);
	}

	/**
	 * Returns the product details with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the product details
	 * @return the product details, or <code>null</code> if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ProductDetails productDetails = (ProductDetails)EntityCacheUtil.getResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
				ProductDetailsImpl.class, primaryKey);

		if (productDetails == _nullProductDetails) {
			return null;
		}

		if (productDetails == null) {
			Session session = null;

			try {
				session = openSession();

				productDetails = (ProductDetails)session.get(ProductDetailsImpl.class,
						primaryKey);

				if (productDetails != null) {
					cacheResult(productDetails);
				}
				else {
					EntityCacheUtil.putResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
						ProductDetailsImpl.class, primaryKey,
						_nullProductDetails);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ProductDetailsModelImpl.ENTITY_CACHE_ENABLED,
					ProductDetailsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return productDetails;
	}

	/**
	 * Returns the product details with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param productDetailsId the primary key of the product details
	 * @return the product details, or <code>null</code> if a product details with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductDetails fetchByPrimaryKey(long productDetailsId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)productDetailsId);
	}

	/**
	 * Returns all the product detailses.
	 *
	 * @return the product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product detailses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @return the range of product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the product detailses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductDetailsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product detailses
	 * @param end the upper bound of the range of product detailses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductDetails> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProductDetails> list = (List<ProductDetails>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTDETAILS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTDETAILS;

				if (pagination) {
					sql = sql.concat(ProductDetailsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductDetails>(list);
				}
				else {
					list = (List<ProductDetails>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the product detailses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ProductDetails productDetails : findAll()) {
			remove(productDetails);
		}
	}

	/**
	 * Returns the number of product detailses.
	 *
	 * @return the number of product detailses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTDETAILS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the product details persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.ProductDetails")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ProductDetails>> listenersList = new ArrayList<ModelListener<ProductDetails>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ProductDetails>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductDetailsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRODUCTDETAILS = "SELECT productDetails FROM ProductDetails productDetails";
	private static final String _SQL_SELECT_PRODUCTDETAILS_WHERE = "SELECT productDetails FROM ProductDetails productDetails WHERE ";
	private static final String _SQL_COUNT_PRODUCTDETAILS = "SELECT COUNT(productDetails) FROM ProductDetails productDetails";
	private static final String _SQL_COUNT_PRODUCTDETAILS_WHERE = "SELECT COUNT(productDetails) FROM ProductDetails productDetails WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "productDetails.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProductDetails exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProductDetails exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductDetailsPersistenceImpl.class);
	private static ProductDetails _nullProductDetails = new ProductDetailsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ProductDetails> toCacheModel() {
				return _nullProductDetailsCacheModel;
			}
		};

	private static CacheModel<ProductDetails> _nullProductDetailsCacheModel = new CacheModel<ProductDetails>() {
			@Override
			public ProductDetails toEntityModel() {
				return _nullProductDetails;
			}
		};
}