/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import javax.portlet.ActionRequest;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import com.esquare.ecommerce.NoSuchCartException;
import com.esquare.ecommerce.NoSuchProductInventoryException;
import com.esquare.ecommerce.NoSuchWishListException;
import com.esquare.ecommerce.model.CancelledOrderItems;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.CartItemImpl;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.EcustomFieldValue;
import com.esquare.ecommerce.model.MyWallet;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.PaymentConfiguration;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.ShippingRates;
import com.esquare.ecommerce.model.ShippingRegion;
import com.esquare.ecommerce.model.TaxesSetting;
import com.esquare.ecommerce.model.TrackerModel;
import com.esquare.ecommerce.model.WishList;
import com.esquare.ecommerce.model.impl.CartImpl;
import com.esquare.ecommerce.model.impl.WishListImpl;
import com.esquare.ecommerce.service.CancelledOrderItemsLocalService;
import com.esquare.ecommerce.service.CancelledOrderItemsLocalServiceUtil;
import com.esquare.ecommerce.service.CartLocalServiceUtil;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldValueLocalServiceUtil;
import com.esquare.ecommerce.service.MyWalletLocalServiceUtil;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.PaymentConfigurationLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductImagesLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.service.ShippingRatesLocalServiceUtil;
import com.esquare.ecommerce.service.TaxesSettingLocalServiceUtil;
import com.esquare.ecommerce.service.WishListLocalServiceUtil;
import com.liferay.portal.kernel.bean.BeanPropertiesUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringReader;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMTemplate;
import com.liferay.portlet.dynamicdatamapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;
import java.net.URL;

/**
 * @author mohammad azharuddin
 */
public class EECUtil {

	public static Cart getCart(PortletRequest portletRequest)
			throws PortalException, SystemException {

		PortletSession portletSession = portletRequest.getPortletSession();

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String sessionCartId = Cart.class.getName()
				+ themeDisplay.getScopeGroupId();
		
		Cart cart = null;
		if (themeDisplay.isSignedIn()) {
			try{
			cart = (Cart) portletSession.getAttribute(sessionCartId,
					PortletSession.APPLICATION_SCOPE);
			}catch(Exception e){
				
			}

			if (cart != null) {
				portletSession.removeAttribute(sessionCartId,
						PortletSession.APPLICATION_SCOPE);
			}

			if ((cart != null) && (cart.getProductsSize() > 0)) {
				cart = CartLocalServiceUtil.updateCart(
						themeDisplay.getUserId(), themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(),
						cart.getProductInventoryIds(), cart.getCouponCodes(),
						cart.getAltShipping(), cart.isInsure());
			} else {
				try {
					cart = CartLocalServiceUtil.getCart(
							themeDisplay.getUserId(),
							themeDisplay.getCompanyId());

				} catch (NoSuchCartException nsce) {
					cart = getCart(themeDisplay);
					cart = CartLocalServiceUtil.updateCart(
							themeDisplay.getUserId(),
							themeDisplay.getCompanyId(),
							themeDisplay.getScopeGroupId(),
							cart.getProductInventoryIds(),
							cart.getCouponCodes(), cart.getAltShipping(),
							cart.isInsure());
				}
			}

			return cart;
		} else {
			try{
		  cart = (Cart) portletSession.getAttribute(sessionCartId,
					PortletSession.APPLICATION_SCOPE);
			}catch(Exception e){
				
			}
			if (cart == null) {
				cart = getCart(themeDisplay);

				portletSession.setAttribute(sessionCartId, (Cart) cart,
						PortletSession.APPLICATION_SCOPE);
			}

			return cart;
		}
	}

	public static Cart getCart(ThemeDisplay themeDisplay) {
		Cart cart = new CartImpl();

		cart.setGroupId(themeDisplay.getScopeGroupId());
		cart.setCompanyId(themeDisplay.getCompanyId());
		cart.setUserId(themeDisplay.getUserId());
		cart.setProductInventoryIds(StringPool.BLANK);
		cart.setCouponCodes(StringPool.BLANK);
		cart.setAltShipping(0);
		cart.setInsure(false);

		return (Cart) cart;
	}

	public static long getProductInventoryId(String productInventoryId) {
		int pos = productInventoryId.indexOf(CharPool.PIPE);

		if (pos != -1) {
			productInventoryId = productInventoryId.substring(0, pos);
		}

		return GetterUtil.getLong(productInventoryId);
	}

	public static PortletURL getPortletURL(PortletRequest actionRequest,
			String portletName, long layoutId, String portletPhase,
			String jspPage, String actionName, WindowState windowState,
			PortletMode portletMode) {
		PortletURL portletURL = PortletURLFactoryUtil.create(actionRequest,
				portletName, layoutId, portletPhase);
		try {
			if (Validator.isNotNull(windowState))
				portletURL.setWindowState(windowState);
		} catch (WindowStateException e) {
			_log.info(e.getClass());
		}
		try {
			if (Validator.isNotNull(portletMode))
				portletURL.setPortletMode(portletMode);
		} catch (PortletModeException e) {
			_log.info(e.getClass());
		}
		if (Validator.isNotNull(portletPhase)
				&& portletPhase.equals(ActionRequest.RENDER_PHASE)
				&& Validator.isNotNull(jspPage))
			portletURL.setParameter("jspPage", jspPage);
		if (Validator.isNotNull(portletPhase)
				&& portletPhase.equals(ActionRequest.ACTION_PHASE)
				&& Validator.isNotNull(actionName))
			portletURL.setParameter(ActionRequest.ACTION_NAME, actionName);
		return portletURL;
	}

	public static double calculateActualPrice(ProductInventory productInventory)
			throws NoSuchProductInventoryException, SystemException {
		return productInventory.getPrice()
				- calculateDiscountPrice(productInventory);
	}

	public static double calculateActualPrice(
			ProductInventory productInventory, int count)
			throws PortalException, SystemException {

		return calculatePrice(productInventory, count)
				- calculateDiscountPrice(productInventory, count);
	}

	public static double calculateAlternativeShipping(
			Map<CartItem, Integer> productInventories, int altShipping)
			throws PortalException, SystemException {

		double shipping = calculateShipping(productInventories);
		double alternativeShipping = shipping;

		// EECPreferences preferences = null;
		//
		// Iterator<Map.Entry<CartItem, Integer>> itr =
		// products.entrySet().iterator();
		//
		// while (itr.hasNext()) {
		// Map.Entry<CartItem, Integer> entry = itr.next();
		//
		// CartItem cartItem = entry.getKey();
		//
		// ProductDetails item = cartItem.getProductDetails();
		//
		// if (preferences == null) {
		// Catalog catalog = item.getCatalog();
		//
		// preferences = EECPreferences.getInstance(
		// catalog.getCompanyId(), catalog.getGroupId());
		//
		// break;
		// }
		// }
		//
		// // Calculate alternative shipping if shopping is configured to use
		// // alternative shipping and shipping price is greater than 0
		//
		// if ((preferences != null) &&
		// preferences.useAlternativeShipping() && (shipping > 0)) {
		//
		// double altShippingDelta = 0.0;
		//
		// try {
		// altShippingDelta = GetterUtil.getDouble(
		// preferences.getAlternativeShipping()[1][altShipping]);
		// }
		// catch (Exception e) {
		// return alternativeShipping;
		// }
		//
		// if (altShippingDelta > 0) {
		// alternativeShipping = shipping * altShippingDelta;
		// }
		// }

		return alternativeShipping;
	}

	public static double calculateInsurance(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		double insurance = 0.0;
		double subtotal = 0.0;

		// EECPreferences preferences = null;
		//
		// Iterator<Map.Entry<CartItem, Integer>> itr =
		// products.entrySet().iterator();
		//
		// while (itr.hasNext()) {
		// Map.Entry<CartItem, Integer> entry = itr.next();
		//
		// CartItem cartItem = entry.getKey();
		// Integer count = entry.getValue();
		//
		// ShoppingItem item = cartItem.getItem();
		//
		// if (preferences == null) {
		// ShoppingCategory category = item.getCategory();
		//
		// preferences = EECPreferences.getInstance(
		// category.getCompanyId(), category.getGroupId());
		// }
		//
		// ShoppingItemPrice itemPrice = _getItemPrice(item, count.intValue());
		//
		// subtotal += calculateActualPrice(itemPrice) * count.intValue();
		// }
		//
		// if ((preferences != null) && (subtotal > 0)) {
		// double insuranceRate = 0.0;
		//
		// double[] range = EECPreferences.INSURANCE_RANGE;
		//
		// for (int i = 0; i < range.length - 1; i++) {
		// if ((subtotal > range[i]) && (subtotal <= range[i + 1])) {
		// int rangeId = i / 2;
		// if (MathUtil.isOdd(i)) {
		// rangeId = (i + 1) / 2;
		// }
		//
		// insuranceRate = GetterUtil.getDouble(
		// preferences.getInsurance()[rangeId]);
		// }
		// }
		//
		// String formula = preferences.getInsuranceFormula();
		//
		// if (formula.equals("flat")) {
		// insurance += insuranceRate;
		// }
		// else if (formula.equals("percentage")) {
		// insurance += subtotal * insuranceRate;
		// }
		// }

		return insurance;
	}

	public static double calculateSubtotal2(
			Map<CartItem, Integer> productInventories, long orderId)
			throws PortalException, SystemException {

		double subtotal = 0.0;
		double orderItemPrice = 0.0;

		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();

			List<OrderItem> orderItems = OrderItemLocalServiceUtil
					.findByOrderItemId(orderId,
							String.valueOf(productInventory.getInventoryId()));

			for (OrderItem orderItem : orderItems) {
				orderItemPrice = orderItem.getPrice();
			}
			subtotal += calculatePrice(orderItemPrice, count.intValue());
		}
		return subtotal;
	}

	public static double calculateSubtotal(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		double subtotal = 0.0;

		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();
		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();
			subtotal += calculatePrice(productInventory, count.intValue());

		}

		return subtotal;
	}

	public static double calculatePrice(ProductInventory productInventory,
			int count) {
		return productInventory.getPrice() * count;
	}

	public static double calculatePrice(double orderPrice, int count) {
		return orderPrice * count;
	}

	public static double calculateActualSubtotal(
			Map<CartItem, Integer> productInventories, long orderId)
			throws PortalException, SystemException {

		return calculateSubtotal2(productInventories, orderId)
				- calculateDiscountSubtotal(productInventories);
	}

	public static double calculateActualSubtotal(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		return calculateSubtotal(productInventories)
				- calculateDiscountSubtotal(productInventories);
	}

	public static double calculateDiscountSubtotal(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		double subtotal = 0.0;

		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();

			ProductInventory productInventory = cartItem.getProductInventory();

			subtotal += calculateDiscountPrice(productInventory,
					count.intValue());
		}
		return subtotal;
	}

	public static double calculateDiscountPrice(
			ProductInventory productInventory)
			throws NoSuchProductInventoryException, SystemException {
		return productInventory.getPrice() * productInventory.getDiscount();
	}

	public static double calculateDiscountPrice(
			ProductInventory productInventory, int count)
			throws PortalException, SystemException {
		return productInventory.getPrice()
				* (productInventory.getDiscount() / 100) * count;

	}

	public static double calculateDiscountPercent(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		double discount = calculateDiscountSubtotal(productInventories)
				/ calculateSubtotal(productInventories);

		if (Double.isNaN(discount) || Double.isInfinite(discount)) {
			discount = 0.0;
		}

		return discount;
	}

	public static double calculateCouponDiscount(long companyId, long userId,
			Map<CartItem, Integer> productInventories, Coupon coupon,
			String countryId, String stateId) throws PortalException,
			SystemException, DocumentException {
		return calculateCouponDiscount(null, companyId, userId,
				productInventories, coupon, countryId, stateId);
	}

	public static double calculateCouponDiscount(PortletRequest portletRequest,
			long companyId, long userId,
			Map<CartItem, Integer> productInventories, Coupon coupon,
			String countryId, String stateId) throws PortalException,
			SystemException, DocumentException {
		double discount = 0.0;
		PortletSession portletSession = null;
		if (Validator.isNotNull(portletRequest))
			portletSession = portletRequest.getPortletSession();
		if (coupon == null || coupon.getCompanyId() != companyId) {
			if (Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError",
						"coupon is not available",
						PortletSession.APPLICATION_SCOPE);
			return discount;
		}
		if (!coupon.isActive() || !coupon.hasValidDateRange()) {
			if (Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError",
						"coupon is inactive or expired",
						PortletSession.APPLICATION_SCOPE);
			return discount;
		}
		// coupon limit count
		int couponUsedCount = 0; // we have to get count from order table based
									// on companyId & couponCode
		couponUsedCount = OrderLocalServiceUtil.getCouponUsedCount(companyId,
				coupon.getCouponCode());

		if (coupon.getCouponLimit() != 0
				&& couponUsedCount >= coupon.getCouponLimit()) {
			if (Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError",
						"coupon limit has reached",
						PortletSession.APPLICATION_SCOPE);
			return discount;
		}
		int userCouponCount = 0; // we have to get count from order table based
									// on companyId,couponID & UserId
		userCouponCount = OrderLocalServiceUtil.getCouponUsedByUserCount(
				companyId, userId, coupon.getCouponCode());
		if (coupon.getUserLimit() != 0
				&& userCouponCount >= coupon.getUserLimit()) {
			if (Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError",
						"coupon user limit has reached",
						PortletSession.APPLICATION_SCOPE);
			return discount;
		}
		Map<CartItem, Integer> newItems = EECUtil.getCouponApplicableItems(
				coupon, productInventories);
		double actualSubtotal = calculateActualSubtotal(newItems);
		if ((coupon.getMinOrder() > 0)
				&& (coupon.getMinOrder() > actualSubtotal)) {
			StringBuffer info = new StringBuffer(
					"coupon applicable only for cart containing minimum order of "
							+ coupon.getMinOrder());
			if (Validator.isNotNull(coupon.getLimitCatalogs()))
				info.append(" and products belong to "
						+ removeLastChar(coupon.getLimitCatalogs())
						+ " catalogs ");
			if (Validator.isNotNull(coupon.getLimitProducts()))
				info.append(" and " + removeLastChar(coupon.getLimitProducts())
						+ " products.");
			if (Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError", info.toString(),
						PortletSession.APPLICATION_SCOPE);
			return discount;
		}
		String type = coupon.getDiscountType();
		if (type.equals(EECConstants.DISCOUNT_TYPE_PERCENTAGE)) {
			discount = actualSubtotal * coupon.getDiscount() / 100;
			if (discount == 0) {
				StringBuffer info = new StringBuffer(
						"Coupon applicable only for ");
				if (Validator.isNotNull(coupon.getLimitCatalogs()))
					info.append("products belong to "
							+ removeLastChar(coupon.getLimitCatalogs())
							+ " catalogs ");
				if (Validator.isNotNull(coupon.getLimitProducts()))
					info.append(removeLastChar(coupon.getLimitProducts())
							+ " items.");
				if (Validator.isNotNull(portletRequest))
					portletSession.setAttribute("couponError", info.toString(),
							PortletSession.APPLICATION_SCOPE);
			}
		} else if (type.equals(EECConstants.DISCOUNT_TYPE_ACTUAL)) {
			if (actualSubtotal >= coupon.getDiscount()) {
				discount = coupon.getDiscount();
			}
			StringBuffer info = new StringBuffer();
			if (discount == 0 && actualSubtotal < coupon.getDiscount()) {
				info.append("there-is-an-error.please-contact-the-support-team");
			} else if (discount == 0) {
				info.append("Coupon applicable only for ");
				if (Validator.isNotNull(coupon.getLimitCatalogs()))
					info.append("products belong to "
							+ removeLastChar(coupon.getLimitCatalogs())
							+ " catalogs ");
				if (Validator.isNotNull(coupon.getLimitProducts()))
					info.append(removeLastChar(coupon.getLimitProducts())
							+ " items.");
			}
			if (discount == 0 && Validator.isNotNull(portletRequest))
				portletSession.setAttribute("couponError", info.toString(),
						PortletSession.APPLICATION_SCOPE);

		} else if (type.equals(EECConstants.DISCOUNT_TYPE_FREE_SHIPPING)
				&& Validator.isNotNull(countryId)) {
			discount = calculateShippingPrice(companyId, newItems, countryId,
					stateId);
		} else if (type.equals(EECConstants.DISCOUNT_TYPE_TAX_FREE)
				&& Validator.isNotNull(countryId)) {
			discount = calculateTax(companyId, newItems, countryId, stateId, userId, coupon);
		}
		return discount;

	}

	public static double calculateCouponDiscountCancelledItem(long companyId,
			long userId, long orderId, double remainingOrderActualSubtotal,
			Map<CartItem, Integer> canceledOrderItems, Coupon coupon,
			String countryId, String stateId) throws PortalException,
			SystemException, DocumentException {
		double discount = 0.0;
		if (coupon == null || coupon.getCompanyId() != companyId) {
			return discount;
		}
		Order order = OrderLocalServiceUtil.getOrder(orderId);
		Map<CartItem, Integer> newItems = EECUtil.getCouponApplicableItems(
				coupon, canceledOrderItems);

		double actualSubtotal = calculateActualSubtotal(newItems);
		String type = coupon.getDiscountType();
		if (type.equals(EECConstants.DISCOUNT_TYPE_PERCENTAGE)) {
			discount = actualSubtotal * coupon.getDiscount() / 100;

			if (remainingOrderActualSubtotal < coupon.getMinOrder()) {
				discount = order.getCouponDiscount();
			}

		} else if (type.equals(EECConstants.DISCOUNT_TYPE_ACTUAL)) {
			if (remainingOrderActualSubtotal < coupon.getMinOrder()) {
				discount = coupon.getDiscount();
			}
		} else if (type.equals(EECConstants.DISCOUNT_TYPE_FREE_SHIPPING)
				&& Validator.isNotNull(countryId)) {
			discount = calculateShippingPrice(companyId, newItems, countryId,
					stateId);
			if (remainingOrderActualSubtotal < coupon.getMinOrder()) {
				discount = order.getCouponDiscount();
			}
		} else if (type.equals(EECConstants.DISCOUNT_TYPE_TAX_FREE)
				&& Validator.isNotNull(countryId)) {
			discount = calculateTax(companyId, newItems, countryId, stateId, userId, coupon);
			if (remainingOrderActualSubtotal < coupon.getMinOrder()) {
				discount = order.getCouponDiscount();
			}
		}
		return discount;

	}

	public static Map<CartItem, Integer> getCouponApplicableItems(
			Coupon coupon, Map<CartItem, Integer> allItems)
			throws PortalException, SystemException {
		if (coupon == null) {
			return allItems;
		}

		String[] catalogs = StringUtil.split(coupon.getLimitCatalogs());
		String[] productDetailIds = StringUtil.split(coupon.getLimitProducts());
		if ((catalogs.length > 0) || (productDetailIds.length > 0)) {
			Set<String> catalogsSet = new HashSet<String>();

			for (String catalog : catalogs) {
				catalogsSet.add(catalog);
			}

			Set<String> productDetailSet = new HashSet<String>();

			for (String productDetail : productDetailIds) {
				productDetailSet.add(productDetail);
			}

			Map<CartItem, Integer> newItems = new HashMap<CartItem, Integer>();

			Iterator<Map.Entry<CartItem, Integer>> itr = allItems.entrySet()
					.iterator();

			while (itr.hasNext()) {
				Map.Entry<CartItem, Integer> entry = itr.next();

				CartItem cartItem = entry.getKey();
				Integer count = entry.getValue();

				ProductInventory productInventory = cartItem
						.getProductInventory();
				ProductDetails productDetails = ProductDetailsLocalServiceUtil
						.getProduct(productInventory.getProductDetailsId());
				Catalog catalog = CatalogLocalServiceUtil
						.getCatalog(productDetails.getCatalogId());
				
				StringBuffer sb = new StringBuffer();
				getCatalogList(catalog.getCompanyId(), catalog.getCatalogId(), sb);
				
				if (((catalogsSet.size() > 0) && (hasCatalog(catalogsSet, sb.toString())))
						|| ((productDetailSet.size() > 0) && (productDetailSet
								.contains(productDetails.getName())))) {

					newItems.put(cartItem, count);
				}
			}
			allItems = newItems;
		}
		return allItems;
	}
	
	private static boolean hasCatalog(Set<String> catalogsSet, String catalog) {
		
		boolean flag = false;
		if (!catalog.isEmpty() && catalog.contains(",")) {
			String[] catalogs = catalog.split(",");
			
			for (String val: catalogs) {
				if (catalogsSet.contains(val)) {
					flag = true;
					break;
				}
			}
		}
		
		return flag;
		
	}

	public static Calendar dateToCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	private static String removeLastChar(String str) {
		str = str.substring(0, str.length() - 1);
		return str;

	}

	public static double calculateActualSubtotal(List<OrderItem> orderItems) {

		double subtotal = 0.0;
		CancelledOrderItems cancelOrder = null;

		for (OrderItem orderItem : orderItems) {
			if(orderItem.getOrderItemStatus().equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)){
				try{
			cancelOrder = CancelledOrderItemsLocalServiceUtil.getCancelledOrderItems(orderItem.getOrderItemId());
			subtotal += cancelOrder.getPrice() * cancelOrder.getQuantity();
				}catch(Exception e){
					e.printStackTrace();
				}
			}else{
			subtotal += orderItem.getPrice() * orderItem.getQuantity();
			}

		}

		return subtotal;
	}

	public static double calculateShipping(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		double shipping = 0.0;
		double subtotal = 0.0;

		// EECPreferences preferences = null;
		//
		// Iterator<Map.Entry<CartItem, Integer>> itr =
		// products.entrySet().iterator();
		//
		// while (itr.hasNext()) {
		// Map.Entry<CartItem, Integer> entry = itr.next();
		//
		// CartItem cartItem = entry.getKey();
		// Integer count = entry.getValue();
		//
		// ShoppingItem item = cartItem.getItem();
		//
		// if (preferences == null) {
		// ShoppingCategory category = item.getCategory();
		//
		// preferences = EECPreferences.getInstance(
		// category.getCompanyId(), category.getGroupId());
		// }
		//
		// if (item.isRequiresShipping()) {
		// ShoppingItemPrice itemPrice = _getItemPrice(
		// item, count.intValue());
		//
		// if (itemPrice.isUseShippingFormula()) {
		// subtotal +=
		// calculateActualPrice(itemPrice) * count.intValue();
		// }
		// else {
		// shipping += itemPrice.getShipping() * count.intValue();
		// }
		// }
		// }
		//
		// if ((preferences != null) && (subtotal > 0)) {
		// double shippingRate = 0.0;
		//
		// double[] range = EECPreferences.SHIPPING_RANGE;
		//
		// for (int i = 0; i < range.length - 1; i++) {
		// if ((subtotal > range[i]) && (subtotal <= range[i + 1])) {
		// int rangeId = i / 2;
		// if (MathUtil.isOdd(i)) {
		// rangeId = (i + 1) / 2;
		// }
		//
		// shippingRate = GetterUtil.getDouble(
		// preferences.getShipping()[rangeId]);
		// }
		// }
		//
		// String formula = preferences.getShippingFormula();
		//
		// if (formula.equals("flat")) {
		// shipping += shippingRate;
		// }
		// else if (formula.equals("percentage")) {
		// shipping += subtotal * shippingRate;
		// }
		// }

		return shipping;
	}

	public static double calculateTax(Map<CartItem, Integer> productInventories)
			throws PortalException, SystemException {

		double tax = 0.0;
		double taxRate = 0.0;

		// ShoppingPreferences preferences = null;
		//
		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();
		//
		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();

			// ProductDetails product = cartItem.getProductDetails();

			// if (preferences == null) {
			// ShoppingCategory category = item.getCategory();
			//
			// preferences = ShoppingPreferences.getInstance(
			// category.getCompanyId(), category.getGroupId());
			//
			// break;
			// }
		}
		//
		// if ((preferences != null) &&
		// preferences.getTaxState().equals(stateId)) {
		//
		double subtotal = 0.0;

		itr = productInventories.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();

			ProductInventory productInventory = cartItem.getProductInventory();
			if (productInventory.isTaxes()) {
				subtotal += calculatePrice(productInventory, count.intValue());
			}
		}
		tax = (double) (subtotal * (taxRate / 100));
		// }
		return tax;
	}

	public static double calculateTotal(long companyId, long userId,
			Map<CartItem, Integer> productInventories, String countryId,
			String stateId, Coupon coupon, int altShipping, boolean insure)
			throws PortalException, SystemException, DocumentException {

		double actualSubtotal = calculateActualSubtotal(productInventories);
		// double tax = calculateTax(products, stateId);
		double shipping = calculateAlternativeShipping(productInventories,
				altShipping);

		double insurance = 0.0;
		if (insure) {
			insurance = calculateInsurance(productInventories);
		}

		double couponDiscount = calculateCouponDiscount(companyId, userId,
				productInventories, coupon, countryId, stateId);
		
		double tax = calculateTax(companyId, productInventories, countryId,
				stateId, userId, coupon);
		
		double shippingPrice = calculateShippingPrice(companyId,
				productInventories, countryId, stateId);
		double total = actualSubtotal + tax + shipping + insurance
				+ shippingPrice - couponDiscount;

		if (total < 0) {
			total = 0.0;
		}

		return total;
	}

	public static double calculateTotal(Order order) throws SystemException {

		List<OrderItem> orderItems = OrderItemLocalServiceUtil
				.getOrderItems(order.getOrderId());

		double total = calculateActualSubtotal(orderItems) + order.getTax()
				+ order.getShipping() + order.getInsurance()
				- order.getCouponDiscount();

		if (total < 0) {
			total = 0.0;
		}
		return total;
	}

	public static Date stringToDate(String dateInString, String pattern) {
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Date date = null;
		try {
			date = formatter.parse(dateInString);
		} catch (ParseException e) {
			_log.info(e.getClass());
		}
		return date;
	}

	public static String dateToString(Date date, String pattern) {
		String dateString = null;
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		dateString = formatter.format(date);
		return dateString;
	}

	public static int getMinQuantity(ProductDetails productDetails)
			throws PortalException, SystemException {

		int minQuantity = 0;
		// int minQuantity = productDetails.getMinQuantity();
		return minQuantity;
	}

	public static boolean isInStock(ProductInventory productInventory,
			Integer orderedQuantity) throws NoSuchProductInventoryException,
			SystemException {
		int stockQuantity = (int) productInventory.getQuantity();
		if (productInventory.getOutOfStockPurchase()
				|| (stockQuantity > 0 && stockQuantity >= orderedQuantity
						.intValue())) {
			return true;
		} else {
			return false;
		}
		/*
		 * if ((stockQuantity > 0) && (stockQuantity >=
		 * orderedQuantity.intValue())) { return true; } else { return false; }
		 */

	}

	public static boolean isInStock(ProductInventory productInventory)
			throws NoSuchProductInventoryException, SystemException {
		if (productInventory.getQuantity() > 0
				|| productInventory.getOutOfStockPurchase()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isOutOfStock(ProductInventory productInventory)
			throws NoSuchProductInventoryException, SystemException {
		if (productInventory.getQuantity() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean meetsMinOrder(
			Map<CartItem, Integer> productInventories) throws PortalException,
			SystemException {

		if ((PortletPropsValues.EEC_MINIMUM_ORDER > 0)
				&& (calculateSubtotal(productInventories) < PortletPropsValues.EEC_MINIMUM_ORDER)) {

			return false;
		} else {
			return true;
		}
	}

	public static String getPayPalRedirectURL(Order order, double total,
			String paypalResponseURL, long companyId) throws SystemException {

		PaymentConfiguration paymentConfiguration = PaymentConfigurationLocalServiceUtil
				.findByPaymentType(companyId, EECConstants.PAYPAL);

		NumberFormat doubleFormat = NumberFormat
				.getNumberInstance(Locale.ENGLISH);

		doubleFormat.setMaximumFractionDigits(2);
		doubleFormat.setMinimumFractionDigits(2);
		
		String inr = EECConstants.INR;
		String usd = EECConstants.USD;

		//String amount = doubleFormat.format(total);
		//int amounts = Integer.parseInt(amount);
		
		String usdAmount = doubleFormat.format(findExchangeRateAndConvert(inr,usd,total));
		

		String firstName = HttpUtil.encodeURL(order.getBillingFirstName());
		String lastName = HttpUtil.encodeURL(order.getBillingLastName());
		String address1 = HttpUtil.encodeURL(order.getBillingStreet());
		String city = HttpUtil.encodeURL(order.getBillingCity());
		String state = HttpUtil.encodeURL(order.getBillingState());
		String zip = HttpUtil.encodeURL(order.getBillingZip());
		String redirectURL = paypalResponseURL;

		StringBundler sb = new StringBundler();
		sb.append(EECConstants.PAYPAL_URL);
		sb.append("cmd=_xclick&");
		sb.append("business=").append(paymentConfiguration.getMerchantId())
				.append("&");
		sb.append("password=").append(paymentConfiguration.getSecretKey())
				.append("&");
		sb.append("item_name=").append(order.getOrderId()).append("&");
		sb.append("item_number=").append(order.getOrderId()).append("&");
		sb.append("invoice=").append(order.getOrderId()).append("&");
		sb.append("amount=").append(usdAmount).append("&");
		sb.append("return=").append(paypalResponseURL.toString()).append("&");
		sb.append("cert_id=").append(paymentConfiguration.getWorkingKey())
				.append("&");
		sb.append("cancel_return=").append(redirectURL).append("&");
		sb.append("first_name=").append(firstName).append("&");
		sb.append("last_name=").append(lastName).append("&");
		sb.append("address1=").append(address1).append("&");
		sb.append("city=").append(city).append("&");
		sb.append("state=").append(state).append("&");
		sb.append("zip=").append(zip).append("&");
		sb.append("no_note=1&");
		sb.append("currency_code=").append("USD").append("");

		return sb.toString();
	}
	
	// currency converter
	
	public static Double findExchangeRateAndConvert(String from, String to, double amount) {
        try {
        	System.out.println("Saleem"+amount);
            //Yahoo Finance API
            URL url = new URL("http://finance.yahoo.com/d/quotes.csv?f=l1&s="+ from + to + "=X");
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();
            if (line.length() > 0) {
                return Double.parseDouble(line) * amount;
            }
            reader.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

	// wishlist
	public static WishList getWishList(PortletRequest portletRequest)
			throws PortalException, SystemException {

		PortletSession portletSession = portletRequest.getPortletSession();

		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String sessionCartId = WishList.class.getName()
				+ themeDisplay.getScopeGroupId();
		if (themeDisplay.isSignedIn()) {
			WishList wishList = (WishList) portletSession
					.getAttribute(sessionCartId);

			if (wishList != null) {
				portletSession.removeAttribute(sessionCartId);
			}

			if ((wishList != null) && (wishList.getProductsSize() > 0)) {
				wishList = WishListLocalServiceUtil.updateWishList(
						themeDisplay.getUserId(), themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(),
						wishList.getProductInventoryIds());
			} else {
				try {
					wishList = WishListLocalServiceUtil.getWishList(
							themeDisplay.getUserId(),
							themeDisplay.getCompanyId());
				} catch (NoSuchWishListException nsce) {
					wishList = getWishList(themeDisplay);

					wishList = WishListLocalServiceUtil.updateWishList(
							themeDisplay.getUserId(),
							themeDisplay.getCompanyId(),
							themeDisplay.getScopeGroupId(),
							wishList.getProductInventoryIds());
				}
			}

			return wishList;
		} else {
			WishList wishList = (WishList) portletSession
					.getAttribute(sessionCartId);

			if (wishList == null) {
				wishList = getWishList(themeDisplay);
				portletSession.setAttribute(sessionCartId, wishList);
			}

			return wishList;
		}
	}

	public static WishList getWishList(ThemeDisplay themeDisplay) {
		WishList wishList = new WishListImpl();

		wishList.setGroupId(themeDisplay.getScopeGroupId());
		wishList.setCompanyId(themeDisplay.getCompanyId());
		wishList.setUserId(themeDisplay.getUserId());
		wishList.setProductInventoryIds(StringPool.BLANK);

		return wishList;
	}

	public static String getEmailContent(HttpServletRequest request,
			long companyId, String prefix) throws PortalException,
			SystemException {
		PortletPreferences preferences = PrefsPropsUtil
				.getPreferences(companyId);
		String emailBody = PrefsParamUtil.getString(preferences, request,
				prefix);
		if (Validator.isNotNull(emailBody)) {
			return emailBody;
		} else {
			return ContentUtil.get(PortletProps.get(prefix));
		}
	}

	public static String getTemplate(long companyId, String templateName) {

		String tempContent = StringPool.BLANK;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
					.forClass(DDMTemplate.class);
			dynamicQuery
					.add(RestrictionsFactoryUtil.eq("companyId", companyId));
			dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%"
					+ templateName + "%"));
			@SuppressWarnings("unchecked")
			List<DDMTemplate> templateList = (List<DDMTemplate>) DDMTemplateLocalServiceUtil
					.dynamicQuery(dynamicQuery);

			if (Validator.isNotNull(templateList)) {
				tempContent = templateList.get(0).getScript();
			}

		} catch (Exception e) {
		}
		return tempContent;
	}

	public static DDMTemplate getTemplateByGroupId(long groupId,
			String templateName) {

		DDMTemplate dDMTemplate = null;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
					.forClass(DDMTemplate.class);
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
			dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%"
					+ templateName + "%"));
			@SuppressWarnings("unchecked")
			List<DDMTemplate> templateList = (List<DDMTemplate>) DDMTemplateLocalServiceUtil
					.dynamicQuery(dynamicQuery);

			if (Validator.isNotNull(templateList)) {
				dDMTemplate = templateList.get(0);
			}

		} catch (Exception e) {
		}
		return dDMTemplate;
	}

	public static String getEsquareCartMailContent(long companyId,
			String templateName, String defaultTemplate) {
		String template = null;
		template = getTemplate(companyId, templateName);
		if (Validator.isNull(template)) {
			template = ContentUtil.get(defaultTemplate);
		}
		return template;
	}

	public static String getEmailName(HttpServletRequest request,
			long companyId, String prefix) throws PortalException,
			SystemException {
		PortletPreferences preferences = PrefsPropsUtil
				.getPreferences(companyId);
		String emailName = PrefsParamUtil.getString(preferences, request,
				prefix);
		if (Validator.isNotNull(emailName)) {
			return emailName;
		} else {
			Role role = RoleLocalServiceUtil.getRole(companyId,
					PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE);
			User shopAdmin = UserLocalServiceUtil
					.getRoleUsers(role.getRoleId()).get(0);
			return shopAdmin.getFullName();
		}
	}

	public static String getEmailAddress(HttpServletRequest request,
			long companyId, String prefix) throws PortalException,
			SystemException {
		PortletPreferences preferences = PrefsPropsUtil
				.getPreferences(companyId);
		String emailToAddress = PrefsParamUtil.getString(preferences, request,
				prefix);
		if (Validator.isNotNull(emailToAddress)) {
			return emailToAddress;
		} else {
			Role role = RoleLocalServiceUtil.getRole(companyId,
					PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE);
			User shopAdmin = UserLocalServiceUtil
					.getRoleUsers(role.getRoleId()).get(0);
			return shopAdmin.getEmailAddress();
		}
	}

	public static String addXmlContent(String xml, String element,
			String attrbute, String[] regionIds, String[] regions,
			String[] price) throws XMLStreamException {
		xml = _sanitizeXML(xml);
		XMLStreamReader xmlStreamReader = null;
		XMLStreamWriter xmlStreamWriter = null;

		ClassLoader portalClassLoader = PortalClassLoaderUtil.getClassLoader();

		Thread currentThread = Thread.currentThread();

		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		try {

			if (contextClassLoader != portalClassLoader) {
				currentThread.setContextClassLoader(portalClassLoader);
			}

			XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

			xmlStreamReader = xmlInputFactory
					.createXMLStreamReader(new UnsyncStringReader(xml));
			UnsyncStringWriter unsyncStringWriter = new UnsyncStringWriter();

			XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();

			xmlStreamWriter = xmlOutputFactory
					.createXMLStreamWriter(unsyncStringWriter);

			xmlStreamWriter.writeStartDocument();
			xmlStreamWriter.writeStartElement(_ROOT);

			for (int i = 0; i < regions.length; i++) {
				xmlStreamWriter.writeStartElement(element);
				xmlStreamWriter.writeAttribute(attrbute,
						String.valueOf(regionIds[i]));
				xmlStreamWriter
						.writeStartElement(PortletPropsValues.NAME_ATTRIBUTE);
				xmlStreamWriter.writeCData(regions[i]);
				xmlStreamWriter.writeEndElement();
				xmlStreamWriter
						.writeStartElement(PortletPropsValues.VALUE_ATTRIBUTE);
				xmlStreamWriter.writeCData(price[i]);
				xmlStreamWriter.writeEndElement();
				xmlStreamWriter.writeEndElement();
			}

			xmlStreamWriter.writeEndElement();
			xmlStreamWriter.writeEndDocument();

			xmlStreamWriter.close();
			xmlStreamWriter = null;

			xml = unsyncStringWriter.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (contextClassLoader != portalClassLoader) {
				currentThread.setContextClassLoader(contextClassLoader);
			}
			if (xmlStreamReader != null) {
				try {
					xmlStreamReader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			if (xmlStreamWriter != null) {
				try {
					xmlStreamWriter.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return xml;
	}

	public static String _sanitizeXML(String xml) {
		if (Validator.isNull(xml) || (xml.indexOf("<root") == -1)) {
			xml = _EMPTY_ROOT_NODE;
		}

		return xml;
	}

	public static String updateXmlContent(String xml, String status, Date date,
			String location, String orderItemIds, String shippingTrackerId,
			String comment, String reason, ActionRequest actionRequest) {

		StringBuffer node = new StringBuffer();
		node.append("<trackerStatus status=\"");
		node.append(status);
		node.append("\">");

		if (Validator.isNotNull(date)) {
			node.append("<Date><![CDATA[");
			node.append(date);
			node.append("]]></Date>");
		}
		if (Validator.isNotNull(location)) {
			node.append("<Location><![CDATA[");
			node.append(location);
			node.append("]]></Location>");
		}

		if (status.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)) {
			Map<Long, Integer> cancelOrderItemMap = new HashMap<Long, Integer>();
			long[] orderItemId = StringUtil.split(
					ParamUtil.getString(actionRequest, "orderItemIds"), 0L);
			for (int i = 0; i < orderItemId.length; i++) {
				int quantityToBeCancelled = ParamUtil.getInteger(actionRequest,
						"quantity" + orderItemId[i], 0);
				cancelOrderItemMap.put(orderItemId[i], quantityToBeCancelled);
			}
			node.append("<OrderItemQuantity><![CDATA[");
			node.append(cancelOrderItemMap);
			node.append("]]></OrderItemQuantity>");
		}
		if (Validator.isNotNull(orderItemIds)) {
			node.append("<OrderItemIds><![CDATA[");
			node.append(orderItemIds);
			node.append("]]></OrderItemIds>");
		}
		if (Validator.isNotNull(shippingTrackerId)) {
			node.append("<TrackerId><![CDATA[");
			node.append(shippingTrackerId);
			node.append("]]></TrackerId>");
		}
		if (Validator.isNotNull(comment)) {
			node.append("<Comment><![CDATA[");
			node.append(comment);
			node.append("]]></Comment>");
		}
		if (Validator.isNotNull(reason)) {
			node.append("<Reason><![CDATA[");
			node.append(comment);
			node.append("]]></Reason>");
		}
		node.append("</trackerStatus>");

		if (Validator.isNotNull(xml)) {
			xml = xml.substring(0, xml.indexOf("<root>") + 6) + node.toString()
					+ xml.substring(xml.indexOf("<root>") + 6);
		} else {
			xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root>"
					+ node.toString() + "</root>";
		}

		return xml;
	}

	public static String generateHistory(String historyXML)
			throws PortalException, SystemException {
		List<TrackerModel> list = EECUtil.readXMLContentOrder(HtmlUtil
				.unescape(historyXML));
		StringBuilder historyBuilder = new StringBuilder();
		historyBuilder
				.append("<div  style=\"background-color:white;color:black;font-size: 12px;font-family: arial;text-align: justify;margin-left: 20px\">");
		historyBuilder.append("<div id=\"showDiv\" > Order Summary </div>");
		for (TrackerModel trackerModel : list) {

			historyBuilder.append("<div class=\"bill_border\">  </div>");
			historyBuilder.append("<hr>");
			historyBuilder.append("<b>");
			historyBuilder.append(EECUtil.dateToString(trackerModel.getDate(),
					"dd-MM-yyyy HH:mm:ss"));
			historyBuilder.append("</b>");
			historyBuilder.append("<ul>");
			long[] orderItemIds = StringUtil.split(
					trackerModel.getOrderItemIds(), 0l);

			historyBuilder.append("<li>");
			historyBuilder.append("Order Items");
			historyBuilder.append(StringPool.SPACE).append(
					StringPool.COMMA_AND_SPACE);
			for (int i = 0; i < orderItemIds.length; i++) {
				historyBuilder.append(OrderItemLocalServiceUtil.getOrderItem(
						orderItemIds[i]).getName());
				if (i < orderItemIds.length - 1)
					historyBuilder.append(StringPool.SPACE)
							.append(StringPool.AMPERSAND)
							.append(StringPool.SPACE);
			}
			historyBuilder.append(" is ");
			historyBuilder.append(trackerModel.getStatus());
			/* historyBuilder.append(" on "); */
			historyBuilder.append("</li>");

			if (trackerModel.getStatus().equalsIgnoreCase(
					EECConstants.ORDER_STATUS_ONSHIPPING)) {
				historyBuilder
						.append("<li>The expected delivery date for <ul>");
				for (int i = 0; i < orderItemIds.length; i++) {
					OrderItem orderItem = OrderItemLocalServiceUtil
							.getOrderItem(orderItemIds[i]);
					historyBuilder.append("<li>");
					historyBuilder.append(orderItem.getName());
					historyBuilder.append(" is ");
					historyBuilder.append(EECUtil.dateToString(
							orderItem.getExpectedDeliveryDate(), "dd-MM-yyyy"));
					historyBuilder.append("</li>");
				}
				historyBuilder.append("</ul></li>");
			}

			HashMap<Long, Integer> hashMap = trackerModel
					.getOrderItemQuantity();
			if (Validator.isNotNull(hashMap)) {
				Iterator iter = hashMap.entrySet().iterator();
				historyBuilder.append("<li> Quantity <ul>");
				while (iter.hasNext()) {
					Map.Entry mEntry = (Map.Entry) iter.next();
					OrderItem orderItem = OrderItemLocalServiceUtil
							.getOrderItem((Long) mEntry.getKey());
					historyBuilder.append("<li>");
					historyBuilder.append("Order Item ");
					historyBuilder.append(orderItem.getName());
					historyBuilder.append(StringPool.COMMA);
					historyBuilder.append(" quantity ");
					historyBuilder.append(StringPool.OPEN_PARENTHESIS);
					historyBuilder.append((Integer) mEntry.getValue());
					historyBuilder.append(StringPool.CLOSE_PARENTHESIS);
					historyBuilder.append(" cancelled");

					historyBuilder.append("</li>");

				}
				historyBuilder.append("</ul></li>");
			}

			if (Validator.isNotNull(trackerModel.getTackerId())) {
				historyBuilder.append("<li> Shipping tracker id is ");
				historyBuilder.append(trackerModel.getTackerId());
				historyBuilder.append("</li>");
			}

			historyBuilder.append("<li> Locaion :");
			historyBuilder.append(trackerModel.getLocaion());
			historyBuilder.append("</li>");
			historyBuilder.append("<li> Comment:");
			historyBuilder.append(trackerModel.getComment());
			historyBuilder.append("</li>");

			historyBuilder.append("</ul>");
		}
		historyBuilder.append("</div>");
		return historyBuilder.toString();
	}

	public static List<TrackerModel> readXMLContentOrder(String xml) {
		List<TrackerModel> empList = new ArrayList<TrackerModel>();
		TrackerModel emp = null;
		boolean bDate = false;
		boolean bOrderItemIds = false;
		boolean bOrderItemQuantity = false;
		boolean bTrackerId = false;
		boolean bLocation = false;
		boolean bComment = false;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLStreamReader xmlStreamReader = xmlInputFactory
					.createXMLStreamReader(new UnsyncStringReader(xml));
			int event = xmlStreamReader.getEventType();
			while (true) {
				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (xmlStreamReader.getLocalName().equals("trackerStatus")) {
						emp = new TrackerModel();
						emp.setStatus(xmlStreamReader.getAttributeValue(0));
					} else if (xmlStreamReader.getLocalName().equals("Date")) {
						bDate = true;
					} else if (xmlStreamReader.getLocalName()
							.equals("Location")) {
						bLocation = true;
					} else if (xmlStreamReader.getLocalName().equals("Comment")) {
						bComment = true;
					} else if (xmlStreamReader.getLocalName().equals(
							"TrackerId")) {
						bTrackerId = true;
					} else if (xmlStreamReader.getLocalName().equals(
							"OrderItemIds")) {
						bOrderItemIds = true;
					} else if (xmlStreamReader.getLocalName().equals(
							"OrderItemQuantity")) {
						bOrderItemQuantity = true;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					if (bDate) {

						emp.setDate(stringToDate(xmlStreamReader.getText(),
								"EEE MMM dd HH:mm:ss z yyyy"));
						bDate = false;
					} else if (bLocation) {
						emp.setLocaion(xmlStreamReader.getText());
						bLocation = false;
					} else if (bOrderItemIds) {
						emp.setOrderItemIds(xmlStreamReader.getText());
						bOrderItemIds = false;
					} else if (bTrackerId) {
						emp.setTackerId(xmlStreamReader.getText());
						bTrackerId = false;
					} else if (bComment) {
						emp.setComment(xmlStreamReader.getText());
						bComment = false;
					} else if (bOrderItemQuantity) {
						emp.setOrderItemQuantity(StringTohashMap(xmlStreamReader
								.getText()));
						bOrderItemQuantity = false;
					}

					break;
				case XMLStreamConstants.END_ELEMENT:
					if (xmlStreamReader.getLocalName().equals("trackerStatus")) {
						empList.add(emp);
					}
					break;
				}
				if (!xmlStreamReader.hasNext())
					break;

				event = xmlStreamReader.next();
			}

		} catch (XMLStreamException e) {
			_log.info(e.getClass());
		}
		return empList;
	}

	// xml reader

	public static List<ShippingRegion> readXMLContent(String xml, String element) {
		List<ShippingRegion> empList = new ArrayList<ShippingRegion>();
		ShippingRegion emp = null;
		boolean bName = false;
		boolean bValue = false;
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLStreamReader xmlStreamReader = xmlInputFactory
					.createXMLStreamReader(new UnsyncStringReader(xml));
			int event = xmlStreamReader.getEventType();
			while (true) {
				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (xmlStreamReader.getLocalName().equals(element)) {
						emp = new ShippingRegion();
						emp.setId(Integer.parseInt(xmlStreamReader
								.getAttributeValue(0)));
					} else if (xmlStreamReader.getLocalName().equals(
							PortletPropsValues.NAME_ATTRIBUTE)) {
						bName = true;
					} else if (xmlStreamReader.getLocalName().equals(
							PortletPropsValues.VALUE_ATTRIBUTE)) {
						bValue = true;
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					if (bName) {
						emp.setName(xmlStreamReader.getText());
						bName = false;
					} else if (bValue) {
						if (element.equals(PortletPropsValues.REGION_TAG))
							emp.setPrice(Double.parseDouble(xmlStreamReader
									.getText()));
						else
							emp.setKeyValue(xmlStreamReader.getText());
						bValue = false;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					if (xmlStreamReader.getLocalName().equals(
							PortletPropsValues.REGION_TAG)
							|| xmlStreamReader.getLocalName().equals(
									PortletPropsValues.CUSTOM_FIELD_TAG)) {
						empList.add(emp);
					}
					break;
				}
				if (!xmlStreamReader.hasNext())
					break;

				event = xmlStreamReader.next();
			}

		} catch (XMLStreamException e) {
			_log.info(e.getClass());
		}
		return empList;
	}

	public static double calculateShippingPrice(long companyId,
			Map<CartItem, Integer> productInventories, String countryId,
			String stateId) throws SystemException, PortalException,
			DocumentException {

		double shippingPrice = 0.0;

		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();
		String amount = StringPool.BLANK;

		itr = productInventories.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();

			if (!productInventory.isFreeShipping()) {
				if (productInventory.getWeight() > 0) {
					amount = getShippingWeightAmount(companyId, countryId,
							stateId,
							PortletPropsValues.SHIPPING_WEIGHT_CRITERIA,
							productInventory.getWeight());
				} else {
					amount = getShippingWeightAmount(companyId, countryId,
							stateId,
							PortletPropsValues.SHIPPING_PRICE_CRITERIA,
							productInventory.getPrice());
				}
				if (Validator.isNotNull(amount)) {
					shippingPrice += Double.parseDouble(amount) * count;
				}
			}
		}
		return shippingPrice;
	}

	public static double calculateRefundShippingAmount(long companyId,
			Map<CartItem, Integer> productInventories, String countryId,
			String stateId, long orderId) throws SystemException,
			PortalException, DocumentException {

		double shippingPrice = 0.0;
		double finalshippingPrice = 0.0;
		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();

		itr = productInventories.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();

			ProductInventory productInventory = cartItem.getProductInventory();
			List<OrderItem> orderItems = OrderItemLocalServiceUtil
					.findByOrderItemId(orderId,
							String.valueOf(productInventory.getInventoryId()));
			for (OrderItem refundShippingPrice : orderItems) {
				shippingPrice = refundShippingPrice.getShippingPrice();
			}
			shippingPrice = calculateShippingPrice(shippingPrice,
					count.intValue());
			finalshippingPrice += shippingPrice;
		}
		return finalshippingPrice;
	}

	public static double calculateShippingPrice(double shippingPrice, int count) {
		return shippingPrice * count;
	}

	public static double orderItemShippingPrice(long companyId,
			ProductInventory productInventory, String countryId,
			String stateId, int count) throws SystemException, PortalException,
			DocumentException {

		double shippingPrice = 0.0;

		String amount = StringPool.BLANK;

		if (!productInventory.isFreeShipping()) {
			if (productInventory.getWeight() > 0) {
				amount = getShippingWeightAmount(companyId, countryId, stateId,
						PortletPropsValues.SHIPPING_WEIGHT_CRITERIA,
						productInventory.getWeight());
			} else {
				amount = getShippingWeightAmount(companyId, countryId, stateId,
						PortletPropsValues.SHIPPING_PRICE_CRITERIA,
						productInventory.getPrice());
			}
			if (Validator.isNotNull(amount)) {
				shippingPrice += Double.parseDouble(amount);
			}
		}

		return shippingPrice;
	}

	public static void getEBSResponse(ActionRequest actionRequest)
			throws IOException {
		HttpServletRequest httpReq = PortalUtil
				.getOriginalServletRequest(PortalUtil
						.getHttpServletRequest(actionRequest));
		String DR = httpReq.getParameter("DR");
		/* String DR = ParamUtil.getString(actionRequest, "DR"); */
		String key = PortletPropsValues.EBS_SECRET_KEY;
		StringBuffer data1 = new StringBuffer().append(DR);

		for (int i = 0; i < data1.length(); i++) {
			if (data1.charAt(i) == ' ')
				data1.setCharAt(i, '+');
		}
		Base64 base64 = new Base64();
		byte[] data = base64.decode(data1.toString());
		RC4 rc4 = new RC4(key);
		byte[] result = rc4.rc4(data);

		ByteArrayInputStream byteIn = new ByteArrayInputStream(result, 0,
				result.length);
		BufferedReader dataIn = new BufferedReader(
				new InputStreamReader(byteIn));
		String recvString1 = "";
		String recvString = "";
		recvString1 = dataIn.readLine();
		int i = 0;
		while (recvString1 != null) {
			i++;
			if (i > 705)
				break;
			recvString += recvString1 + "\n";
			recvString1 = dataIn.readLine();
		}
		recvString = recvString.replace("=&", "=--&");
		StringTokenizer st = new StringTokenizer(recvString, "=&");
		String field, value;
		HashMap<String, String> ebsMap = new HashMap<String, String>();
		while (st.hasMoreTokens()) {
			field = st.nextToken();
			value = st.nextToken();
			ebsMap.put(field, value);
		}
		actionRequest.setAttribute("EBSResponse", ebsMap);
	}

	public static String getShippingWeightAmount(long companyId,
			String countryId, String stateId, String shippingType, double value)
			throws DocumentException, SystemException {

		String amount = StringPool.BLANK;
		List<ShippingRates> shippingSetting = ShippingRatesLocalServiceUtil
				.getShippingSettingRecord(companyId, countryId, shippingType,
						value);

		if (Validator.isNotNull(shippingSetting) && shippingSetting.size() > 0) {
			String xml = shippingSetting.get(0).getXml();
			Iterator<Element> rootEle = null;
			Document doc = SAXReaderUtil.read(xml);
			Element ele = doc.getRootElement();
			rootEle = ele.elements(PortletPropsValues.REGION_TAG).iterator();
			while (rootEle.hasNext()) {
				Element eleNext = rootEle.next();
				String id = GetterUtil.getString(eleNext
						.attributeValue(PortletPropsValues.ID_ATTRIBUTE));
				if (id.contains(stateId)) {
					amount = eleNext
							.element(PortletPropsValues.VALUE_ATTRIBUTE)
							.getStringValue();
				}
			}
		}
		return amount;
	}

	public static double calculateTax(long companyId,
			Map<CartItem, Integer> productInventories, String countryId,
			String stateId, long userId, Coupon coupan) throws PortalException, SystemException,
			DocumentException {

		String taxRate = StringPool.BLANK;
		double subtotal = 0.0;
		double tax = 0.0;
		taxRate = getTaxRate(companyId, countryId, stateId);
		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();

			if (productInventory.isTaxes()) {
				Map<CartItem, Integer> map = new HashMap<CartItem, Integer>();
				map.put(cartItem, 1);
				System.out.println("=====================>>>>>"+calculateCouponDiscount(companyId, userId,
						map, coupan, countryId, stateId));
				subtotal += calculatePrice(productInventory, count.intValue()) - (calculatePrice(productInventory, count.intValue())*productInventory.getDiscount()/100) - calculateCouponDiscount(companyId, userId,
						map, coupan, countryId, stateId);
			}
		}

		if (Validator.isNotNull(taxRate)) {
			
			tax = (double) ((subtotal) * (Double.parseDouble(taxRate) / 100));
		}
		return tax;
	}
	
	public static double orderItemTax(long companyId,
			ProductInventory productInventory, String countryId,
			String stateId, int count) throws PortalException, SystemException,
			DocumentException {

		String taxRate = StringPool.BLANK;
		double subtotal = 0.0;
		double tax = 0.0;
		boolean taxes = productInventory.getTaxes();

		if (taxes) {
			if (productInventory.isTaxes()) {

				subtotal += productInventory.getPrice();

			}

			taxRate = getTaxRate(companyId, countryId, stateId);

			if (Validator.isNotNull(taxRate)) {

				tax = (double) (subtotal * (Double.parseDouble(taxRate) / 100));

			}
		}
		return tax;
	}

	public static double calculateRefundTaxAmount(long companyId,
			Map<CartItem, Integer> productInventories, String countryId,
			String stateId, long orderId) throws PortalException,
			SystemException, DocumentException {

		String taxRate = StringPool.BLANK;
		double finalTaxAmount = 0.0;
		double tax = 0.0;
		taxRate = getTaxRate(companyId, countryId, stateId);
		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();

			List<OrderItem> orderIt = OrderItemLocalServiceUtil
					.findByOrderItemId(orderId,
							String.valueOf(productInventory.getInventoryId()));
			for (OrderItem orderItemTax : orderIt) {
				tax = orderItemTax.getTax();
			}
			tax = calculateTaxPrice(tax, count);

			finalTaxAmount += tax;
		}
		return finalTaxAmount;
	}

	public static double calculateTaxPrice(double tax, int count) {
		return tax * count;
	}

	public static String getTaxRate(long companyId, String countryId,
			String stateId) throws SystemException, DocumentException {

		String taxRate = StringPool.BLANK;
		TaxesSetting taxesSetting = TaxesSettingLocalServiceUtil.getTaxesRate(
				companyId, countryId);
		if (Validator.isNotNull(taxesSetting)) {
			String xml = taxesSetting.getXml();
			Iterator<Element> rootEle = null;
			Document doc = SAXReaderUtil.read(xml);
			Element ele = doc.getRootElement();
			rootEle = ele.elements(PortletPropsValues.REGION_TAG).iterator();
			while (rootEle.hasNext()) {
				Element eleNext = rootEle.next();
				String id = GetterUtil.getString(eleNext
						.attributeValue(PortletPropsValues.ID_ATTRIBUTE));
				if (id.contains(stateId)) {
					taxRate = eleNext.element(
							PortletPropsValues.VALUE_ATTRIBUTE)
							.getStringValue();
				}
			}
		}
		return taxRate;
	}

	public static void getCatalogList(long companyId, long catalogId,
			StringBuffer sb) throws SystemException, PortalException {
			
			Catalog catalog = CatalogLocalServiceUtil.getCatalog(catalogId);
			if (Validator.isNotNull(catalog)) {
				sb.append(catalog.getName() + ",");
				if (catalog.getParentCatalogId() != 0) {
					getCatalogList(companyId, catalog.getParentCatalogId(), sb);
				}
			}
	}

	public static String getCompareProductDetails(String key,
			ProductInventory productInventory) throws SystemException,
			PortalException {
		StringBundler value = new StringBundler();
		ProductDetails productDetails = ProductDetailsLocalServiceUtil
				.getProductDetails(productInventory.getProductDetailsId());
		if (key.equalsIgnoreCase("description")) {

			// catalogId=productDetails.getCatalogId();
			value.append(BeanPropertiesUtil.getString(productDetails,
					key.toLowerCase(), StringPool.BLANK));
		} else if (key.equalsIgnoreCase("product")) {
			List<ProductImages> productImages = ProductImagesLocalServiceUtil
					.findByInventoryId(productInventory.getInventoryId());
			FileEntry dlFileEntry = DLAppServiceUtil.getFileEntry(productImages.get(0).getImageId());
			value.append("<center><img src=\"");
			value.append("/documents/");
			value.append(dlFileEntry.getGroupId());
			value.append("/");
			value.append(dlFileEntry.getFolderId());
			value.append("/");
			value.append(dlFileEntry.getTitle());
			value.append("/");
			value.append(dlFileEntry.getUuid());
			value.append("\"  style=\"width:auto;max-width:175px;max-height:140px;\" /></br>");
			value.append(BeanPropertiesUtil.getString(productDetails, "name",
					StringPool.BLANK));
			/* value.append("<br/>  <b>"); */

			value.append("<div class='compareRs' >");

			value.append("Rs. ");
			value.append(BeanPropertiesUtil.getString(productInventory,
					"price", StringPool.BLANK));
			value.append(" </div> <div class='comparestock' >");
			value.append(((productInventory.getQuantity() > 0) ? PortletPropsValues.PRODUCT_IN_STOCK
					: PortletPropsValues.PRODUCT_OUT_OF_STOCK));
			value.append("</div> </center>");

		}

		return value.toString();

	}

	public static String getCustomFieldsValue(long companyId,
			ShippingRegion customField, ProductInventory productInventory)
			throws SystemException {
		String value = StringPool.BLANK;
		EcustomFieldValue custvalue = EcustomFieldValueLocalServiceUtil
				.findByInventoryId(companyId, productInventory.getInventoryId());
		List<ShippingRegion> ecustomValuelist = EECUtil.readXMLContent(
				custvalue.getXml(), PortletPropsValues.CUSTOM_FIELD_TAG);
		HashMap<String, String> productMap = new HashMap<String, String>();
		for (ShippingRegion ecustomValue : ecustomValuelist) {
			productMap.put(ecustomValue.getName(), ecustomValue.getKeyValue());
		}
		String customValue = productMap
				.get(String.valueOf(customField.getId()));
		if (customValue != null)
			value = customValue;
		return value;

	}

	public static int getDigitsCount(double dvalue) {
		int count = 0;
		Long L = Math.round(dvalue);
		int ivalue = Integer.valueOf(L.intValue());
		while (ivalue > 0) {
			ivalue = ivalue / 10;
			count++;
		}
		return count;
	}

	public static double getMinPrice(double dvalue) {
		int count = getDigitsCount(dvalue);
		double price = 0;
		if (count <= 3) {
			price = dvalue - (dvalue % 100);
		} else {
			price = dvalue - (dvalue % 1000);
		}
		return price;
	}

	public static double getMaxPrice(double dvalue) {
		int count = getDigitsCount(dvalue);
		double price = 0;
		double remainder = 0;
		if (count <= 3) {
			remainder = ((dvalue % 100) == 0) ? 100 : (dvalue % 100);
			price = dvalue + (100 - remainder);
		} else {
			remainder = ((dvalue % 1000) == 0) ? 1000 : (dvalue % 1000);
			price = dvalue + (1000 - remainder);
		}
		return price;
	}

	public static int getPackageValue(String packageName) {
		switch (Days.valueOf(packageName)) {
		case Free:
			return 1;
		case Basic:
			return 2;
		case Standard:
			return 3;
		case Premium:
			return 4;
		case Enterprise:
			return 5;
		default:
			return 0;
		}
	}

	public static HashMap<Long, Integer> StringTohashMap(String hashMapInString) {
		HashMap<Long, Integer> valueMap = new HashMap<Long, Integer>();
		try {
			String[] arrValue = hashMapInString.substring(1,
					hashMapInString.length() - 1).split(",");
			for (String string : arrValue) {
				String[] mapPair = string.split("=");
				valueMap.put(Long.valueOf(mapPair[0].trim()),
						Integer.valueOf(mapPair[1].trim()));
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		return valueMap;

	}

	public static String getPreference(long companyId, String prefix)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId, prefix,
				PortletProps.get(prefix));
	}

	public static Double myWalletAmount(long userId) throws SystemException {
		double myWalletAmount = 0;
		MyWallet myWallet = MyWalletLocalServiceUtil.fetchMyWallet(userId);
		if (Validator.isNotNull(myWallet))
			myWalletAmount = myWallet.getAmount();
		return myWalletAmount;

	}

	public static Map<CartItem, Integer> getCanceledOrderItems(long companyId,
			ActionRequest actionRequest) throws PortalException,
			SystemException {
		long[] orderItemId = StringUtil.split(
				ParamUtil.getString(actionRequest, "orderItemIds"), 0L);
		Map<CartItem, Integer> map = new HashMap<CartItem, Integer>();
		for (int i = 0; i < orderItemId.length; i++) {
			OrderItem orderItem = OrderItemLocalServiceUtil
					.getOrderItem(orderItemId[i]);

			int quantityToBeCancelled = ParamUtil.getInteger(actionRequest,
					"quantity" + orderItemId[i], 0);

			map.put(new CartItemImpl(ProductInventoryLocalServiceUtil
					.getProductInventory(Long.valueOf(orderItem
							.getProductInventoryId()))), quantityToBeCancelled);

		}
		return map;
	}

	public static double amountPayable(double grandTotal, double walletAmount) {
		return (grandTotal - walletAmount > 0) ? (grandTotal - walletAmount)
				: 0;
	}

	public enum Days {
		Free, Basic, Standard, Premium, Enterprise
	}

	public static String getOrderItemDetails(HttpServletRequest request,
			long orderId, long companyId) {
		StringBuffer sb = new StringBuffer();
		String orderMailContent = StringPool.BLANK;
		try {
			List<OrderItem> orderItems = OrderItemLocalServiceUtil
					.getOrderItems(orderId);
			sb.append(EECUtil.getEmailContent(request, companyId,
					"Email.order.details.table.header"));
			orderMailContent = EECUtil.getEmailContent(request, companyId,
					"Email.order.details.table.content");
			for (OrderItem orderItem : orderItems) {
				String[] oldSubs = { "[$Name$]", "[$Quantity$]",
						"[$Expected Delivery Date$]", "[$Delivered Date$]",
						"[$Price$]" };
				String[] newSubs = { orderItem.getName(),
						String.valueOf(orderItem.getQuantity()),
						String.valueOf(orderItem.getExpectedDeliveryDate()),
						String.valueOf(orderItem.getDeliveredDate()),
						String.valueOf(orderItem.getPrice()) };
				sb.append(StringUtil
						.replace(orderMailContent, oldSubs, newSubs));
			}
			sb.append(EECUtil.getEmailContent(request, companyId,
					"Email.order.details.table.footer"));
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		return sb.toString();
	}

	public static final String _EMPTY_ROOT_NODE = "<root />";

	public static final String _ROOT = "root";

	public static final String[] CURRENCY_IDS;

	static {
		String[] ids = null;

		try {
			Set<String> set = new TreeSet<String>();

			Locale[] locales = Locale.getAvailableLocales();

			for (int i = 0; i < locales.length; i++) {
				Locale locale = locales[i];

				if (locale.getCountry().length() == 2) {
					Currency currency = Currency.getInstance(locale);

					String currencyId = currency.getCurrencyCode();

					set.add(currencyId);
				}
			}

			ids = set.toArray(new String[set.size()]);
		} catch (Exception e) {
			ids = new String[] { "USD", "CAD", "EUR", "GBP", "JPY" };
		} finally {
			CURRENCY_IDS = ids;
		}
	}

	public static String getwishListProductIds(long userId, long companyId) {
		String wishListProductIds = StringPool.BLANK;
		try {
			WishList wishList = WishListLocalServiceUtil.getWishList(userId,
					companyId);
			if (Validator.isNotNull(wishList)) {
				wishListProductIds = wishList.getProductInventoryIds();
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		return wishListProductIds;
	}

	public static NumberFormat getCurrencyFormat(long companyId) {
		String currencyType = PortletPropsValues.DEFAULT_CURRENCY_TYPE;
		try {
			String currencyPref = EECUtil.getPreference(companyId,
					"currencyType");
			if (Validator.isNotNull(currencyPref)) {
				currencyType = currencyPref;
			}
		} catch (SystemException e) {
		}
		Currency currency = Currency.getInstance(currencyType);

		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setCurrency(currency);
		if (currency.getSymbol().equalsIgnoreCase("INR")) {
			String rupeeSymbol = "Rs.";
			dfs.setCurrencySymbol(rupeeSymbol);
		}
		((DecimalFormat) currencyFormat).setDecimalFormatSymbols(dfs);
		currencyFormat.setMinimumFractionDigits(2);
		currencyFormat.setMaximumFractionDigits(2);
		currencyFormat.setCurrency(currency);
		return currencyFormat;
	}

	public static void updateWishList(PortletRequest p_request,
			long productInventoryId) {
		try {
			WishList wishList = EECUtil.getWishList(p_request);
			wishList.addProductInventoryId(productInventoryId);
			WishListLocalServiceUtil.updateWishList(wishList.getUserId(),
					wishList.getCompanyId(), wishList.getGroupId(),
					wishList.getProductInventoryIds());
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	public static Cookie getCookie(PortletRequest portletRequest,
			String cookieName) {
		Cookie cookie = null;
		Cookie[] cookies = null;
		cookies = portletRequest.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().compareTo(cookieName) == 0) {
					cookie = cookies[i];
					break;
				}
			}
		}
		return cookie;
	}

	public static String convertToCommaDelimited(String[] stringArray) {
		StringBuffer ret = new StringBuffer("");
		for (int i = 0; stringArray != null && i < stringArray.length; i++) {
			ret.append(stringArray[i]);
			if (i < stringArray.length - 1) {
				ret.append(',');
			}
		}
		return ret.toString();
	}

	public static String getProductNameByIds(String relatedProductIds) {
		StringBuffer sb = new StringBuffer();
		if (Validator.isNotNull(relatedProductIds)) {
			String[] limitedProductIds = relatedProductIds.split(",");
			for (String limitedProductId : limitedProductIds) {
				try {
					ProductInventory inventory = ProductInventoryLocalServiceUtil
							.getProductInventory(Long
									.parseLong(limitedProductId));
					ProductDetails pDetails = ProductDetailsLocalServiceUtil
							.getProductDetails(inventory.getProductDetailsId());
					sb.append(pDetails.getName());
					sb.append("\n");
				} catch (Exception e) {
				}
			}
		}
		return sb.toString();

	}

	public static JSONArray getOrderNumber(long companyId, long userId) {
		List<Order> filteredEntries = null;
		JSONArray results = JSONFactoryUtil.createJSONArray();
		if (userId > 0) {
			filteredEntries = getFilteredOrderNumberUser(companyId, userId);
		} else {
			filteredEntries = getFilteredOrderNumberAdmin(companyId);
		}

		for (Order entry : filteredEntries) {
			JSONObject listEntry = JSONFactoryUtil.createJSONObject();
			listEntry.put("name", entry.getNumber());
			results.put(listEntry);
		}
		return results;

	}

	private static List<Order> getFilteredOrderNumberAdmin(long companyId) {
		List<Order> orderList = null;
		try {
			orderList = OrderLocalServiceUtil.findByCompanyId(companyId,
					EECConstants.STATUS_CHECKOUT);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return orderList;
	}

	private static List<Order> getFilteredOrderNumberUser(long companyId, long userId) {
		List<Order> orderList = null;
		try {
			orderList = OrderLocalServiceUtil.findByUserId(companyId, userId,
					EECConstants.STATUS_CHECKOUT);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return orderList;
	}

	public static JSONArray getProductNames(long companyId) {
		JSONArray jSONArray = JSONFactoryUtil.createJSONArray();
		JSONObject jSonObject = null;
		try {
			List<ProductDetails> ProductDetails = ProductDetailsLocalServiceUtil
					.getProductDetails(companyId, -1, -1);
			for (ProductDetails productDetails : ProductDetails) {
				jSonObject = JSONFactoryUtil.createJSONObject();
				jSonObject.put("name", productDetails.getName());
				jSONArray.put(jSonObject);
			}
		} catch (Exception e) {
		}
		return jSONArray;

	}
	
	private static Log _log = LogFactoryUtil.getLog(EECUtil.class);
}