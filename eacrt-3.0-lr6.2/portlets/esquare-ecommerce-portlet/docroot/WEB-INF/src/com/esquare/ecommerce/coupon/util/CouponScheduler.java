package com.esquare.ecommerce.coupon.util;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.service.CouponLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.Validator;

public class CouponScheduler implements MessageListener {

	public void receive(Message message) throws MessageListenerException {
		List<Coupon> coupon = null;

		try {
			coupon = CouponLocalServiceUtil.getCoupons(-1, -1);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		for (Coupon coupon2 : coupon) {
			Date endDate = coupon2.getEndDate();
			if (Validator.isNotNull(endDate)) {
				if (new Date().after(endDate) && coupon2.getActive()) {
					try {
						CouponLocalServiceUtil.changeCouponStatus(
								coupon2.getCouponId(), Boolean.FALSE);
					} catch (SystemException e) {
					}
				}
			}
		}

	}

	private static Log _log = LogFactoryUtil.getLog(CouponScheduler.class);
}
