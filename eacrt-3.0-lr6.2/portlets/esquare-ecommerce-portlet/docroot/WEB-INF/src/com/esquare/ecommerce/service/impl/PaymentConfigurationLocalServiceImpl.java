/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.PaymentConfiguration;
import com.esquare.ecommerce.service.base.PaymentConfigurationLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.PaymentConfigurationUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * The implementation of the payment configuration local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.PaymentConfigurationLocalService}
 * interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.PaymentConfigurationLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.PaymentConfigurationLocalServiceUtil
 */
public class PaymentConfigurationLocalServiceImpl extends
		PaymentConfigurationLocalServiceBaseImpl {
	public List<PaymentConfiguration> findBycompanyId(long companyId) {
		List<PaymentConfiguration> paymentConfigurations = null;
		try {
			paymentConfigurations = PaymentConfigurationUtil
					.findByCompanyId(companyId);

		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		return paymentConfigurations;
	}

	public PaymentConfiguration findByPaymentType(long companyId,
			String paymentType) {
		PaymentConfiguration paymentConfigurations = null;
		try {
			paymentConfigurations = PaymentConfigurationUtil.findByC_PGT(
					companyId, paymentType);
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		return paymentConfigurations;
	}

	public PaymentConfiguration addPaymentConfiguration(long userId,
			long companyId, String paymentType, String merchantId,
			String secretKey, boolean status , String workingKey) throws SystemException {

		long id = counterLocalService.increment();

		PaymentConfiguration paymentConfiguration = paymentConfigurationPersistence
				.create(id);

		paymentConfiguration.setCompanyId(companyId);
		paymentConfiguration.setUserId(userId);
		paymentConfiguration.setPaymentGatewayType(paymentType);
		paymentConfiguration.setMerchantId(merchantId);
		paymentConfiguration.setSecretKey(secretKey);
		paymentConfiguration.setSecretKey(workingKey);
		paymentConfiguration.setStatus(status);

		paymentConfiguration.setCreateDate(new Date());

		paymentConfigurationPersistence.update(paymentConfiguration);
		return paymentConfiguration;

	}

	public PaymentConfiguration updatePaymentConfiguration(long id,
			String paymentType, String merchantId, String secretKey,
			boolean status, String workingKey) throws SystemException {

		PaymentConfiguration paymentConfiguration = paymentConfigurationPersistence
				.fetchByPrimaryKey(id);

		paymentConfiguration.setPaymentGatewayType(paymentType);
		paymentConfiguration.setMerchantId(merchantId);
		paymentConfiguration.setSecretKey(secretKey);
		paymentConfiguration.setWorkingKey(workingKey);
		paymentConfiguration.setStatus(status);

		paymentConfiguration.setModifiedDate(new Date());

		paymentConfigurationPersistence.update(paymentConfiguration);
		return paymentConfiguration;

	}

	private static Log _log = LogFactoryUtil
			.getLog(PaymentConfigurationLocalServiceImpl.class);
}