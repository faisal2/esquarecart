package com.esquare.ecommerce.service.persistence;

import java.util.List;

import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.impl.ProductDetailsImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ProductDetailsFinderImpl extends BasePersistenceImpl implements
		ProductDetailsFinder {
	@SuppressWarnings("unchecked")
	public List<ProductDetails> getOutofStockLessStock(long companyId,
			int quantity) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_LESSSTOCK_OUTOFSTOCK_DETAILS);
			if (quantity > 0) {
				sql = sql.concat("AND (EEC_ProductInventory.quantity > 0)");
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ProductDetails", ProductDetailsImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(companyId);
			qPos.add(quantity);
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List<ProductDetails>) query.list();
	}

	private static Log _log = LogFactoryUtil
			.getLog(ProductDetailsFinderImpl.class);
	public static String GET_LESSSTOCK_OUTOFSTOCK_DETAILS = "getLessStockOutofStockDetails";
}