/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchCartPermissionException;
import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.impl.CartPermissionImpl;
import com.esquare.ecommerce.model.impl.CartPermissionModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cart permission service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see CartPermissionPersistence
 * @see CartPermissionUtil
 * @generated
 */
public class CartPermissionPersistenceImpl extends BasePersistenceImpl<CartPermission>
	implements CartPermissionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CartPermissionUtil} to access the cart permission persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CartPermissionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED,
			CartPermissionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED,
			CartPermissionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED,
			CartPermissionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED,
			CartPermissionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			CartPermissionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the cart permissions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the cart permissions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of cart permissions
	 * @param end the upper bound of the range of cart permissions (not inclusive)
	 * @return the range of matching cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findByCompanyId(long companyId, int start,
		int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the cart permissions where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of cart permissions
	 * @param end the upper bound of the range of cart permissions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findByCompanyId(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<CartPermission> list = (List<CartPermission>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CartPermission cartPermission : list) {
				if ((companyId != cartPermission.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CARTPERMISSION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CartPermissionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<CartPermission>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CartPermission>(list);
				}
				else {
					list = (List<CartPermission>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first cart permission in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (cartPermission != null) {
			return cartPermission;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCartPermissionException(msg.toString());
	}

	/**
	 * Returns the first cart permission in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cart permission, or <code>null</code> if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CartPermission> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last cart permission in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (cartPermission != null) {
			return cartPermission;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCartPermissionException(msg.toString());
	}

	/**
	 * Returns the last cart permission in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cart permission, or <code>null</code> if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<CartPermission> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the cart permissions before and after the current cart permission in the ordered set where companyId = &#63;.
	 *
	 * @param permissionId the primary key of the current cart permission
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission[] findByCompanyId_PrevAndNext(long permissionId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = findByPrimaryKey(permissionId);

		Session session = null;

		try {
			session = openSession();

			CartPermission[] array = new CartPermissionImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, cartPermission,
					companyId, orderByComparator, true);

			array[1] = cartPermission;

			array[2] = getByCompanyId_PrevAndNext(session, cartPermission,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CartPermission getByCompanyId_PrevAndNext(Session session,
		CartPermission cartPermission, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CARTPERMISSION_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CartPermissionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(cartPermission);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CartPermission> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the cart permissions where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (CartPermission cartPermission : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(cartPermission);
		}
	}

	/**
	 * Returns the number of cart permissions where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CARTPERMISSION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "cartPermission.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_CI_PK = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED,
			CartPermissionImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByCI_PK",
			new String[] { Long.class.getName(), String.class.getName() },
			CartPermissionModelImpl.COMPANYID_COLUMN_BITMASK |
			CartPermissionModelImpl.PERMISSIONKEY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CI_PK = new FinderPath(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCI_PK",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the cart permission where companyId = &#63; and permissionKey = &#63; or throws a {@link com.esquare.ecommerce.NoSuchCartPermissionException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param permissionKey the permission key
	 * @return the matching cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission findByCI_PK(long companyId, String permissionKey)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = fetchByCI_PK(companyId, permissionKey);

		if (cartPermission == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", permissionKey=");
			msg.append(permissionKey);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCartPermissionException(msg.toString());
		}

		return cartPermission;
	}

	/**
	 * Returns the cart permission where companyId = &#63; and permissionKey = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param permissionKey the permission key
	 * @return the matching cart permission, or <code>null</code> if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByCI_PK(long companyId, String permissionKey)
		throws SystemException {
		return fetchByCI_PK(companyId, permissionKey, true);
	}

	/**
	 * Returns the cart permission where companyId = &#63; and permissionKey = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param permissionKey the permission key
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cart permission, or <code>null</code> if a matching cart permission could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByCI_PK(long companyId, String permissionKey,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, permissionKey };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CI_PK,
					finderArgs, this);
		}

		if (result instanceof CartPermission) {
			CartPermission cartPermission = (CartPermission)result;

			if ((companyId != cartPermission.getCompanyId()) ||
					!Validator.equals(permissionKey,
						cartPermission.getPermissionKey())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CARTPERMISSION_WHERE);

			query.append(_FINDER_COLUMN_CI_PK_COMPANYID_2);

			boolean bindPermissionKey = false;

			if (permissionKey == null) {
				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_1);
			}
			else if (permissionKey.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_3);
			}
			else {
				bindPermissionKey = true;

				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindPermissionKey) {
					qPos.add(permissionKey);
				}

				List<CartPermission> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CI_PK,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CartPermissionPersistenceImpl.fetchByCI_PK(long, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					CartPermission cartPermission = list.get(0);

					result = cartPermission;

					cacheResult(cartPermission);

					if ((cartPermission.getCompanyId() != companyId) ||
							(cartPermission.getPermissionKey() == null) ||
							!cartPermission.getPermissionKey()
											   .equals(permissionKey)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CI_PK,
							finderArgs, cartPermission);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CI_PK,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CartPermission)result;
		}
	}

	/**
	 * Removes the cart permission where companyId = &#63; and permissionKey = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param permissionKey the permission key
	 * @return the cart permission that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission removeByCI_PK(long companyId, String permissionKey)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = findByCI_PK(companyId, permissionKey);

		return remove(cartPermission);
	}

	/**
	 * Returns the number of cart permissions where companyId = &#63; and permissionKey = &#63;.
	 *
	 * @param companyId the company ID
	 * @param permissionKey the permission key
	 * @return the number of matching cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCI_PK(long companyId, String permissionKey)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CI_PK;

		Object[] finderArgs = new Object[] { companyId, permissionKey };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CARTPERMISSION_WHERE);

			query.append(_FINDER_COLUMN_CI_PK_COMPANYID_2);

			boolean bindPermissionKey = false;

			if (permissionKey == null) {
				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_1);
			}
			else if (permissionKey.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_3);
			}
			else {
				bindPermissionKey = true;

				query.append(_FINDER_COLUMN_CI_PK_PERMISSIONKEY_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindPermissionKey) {
					qPos.add(permissionKey);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CI_PK_COMPANYID_2 = "cartPermission.companyId = ? AND ";
	private static final String _FINDER_COLUMN_CI_PK_PERMISSIONKEY_1 = "cartPermission.permissionKey IS NULL";
	private static final String _FINDER_COLUMN_CI_PK_PERMISSIONKEY_2 = "cartPermission.permissionKey = ?";
	private static final String _FINDER_COLUMN_CI_PK_PERMISSIONKEY_3 = "(cartPermission.permissionKey IS NULL OR cartPermission.permissionKey = '')";

	public CartPermissionPersistenceImpl() {
		setModelClass(CartPermission.class);
	}

	/**
	 * Caches the cart permission in the entity cache if it is enabled.
	 *
	 * @param cartPermission the cart permission
	 */
	@Override
	public void cacheResult(CartPermission cartPermission) {
		EntityCacheUtil.putResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionImpl.class, cartPermission.getPrimaryKey(),
			cartPermission);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CI_PK,
			new Object[] {
				cartPermission.getCompanyId(), cartPermission.getPermissionKey()
			}, cartPermission);

		cartPermission.resetOriginalValues();
	}

	/**
	 * Caches the cart permissions in the entity cache if it is enabled.
	 *
	 * @param cartPermissions the cart permissions
	 */
	@Override
	public void cacheResult(List<CartPermission> cartPermissions) {
		for (CartPermission cartPermission : cartPermissions) {
			if (EntityCacheUtil.getResult(
						CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
						CartPermissionImpl.class, cartPermission.getPrimaryKey()) == null) {
				cacheResult(cartPermission);
			}
			else {
				cartPermission.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all cart permissions.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CartPermissionImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CartPermissionImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cart permission.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CartPermission cartPermission) {
		EntityCacheUtil.removeResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionImpl.class, cartPermission.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(cartPermission);
	}

	@Override
	public void clearCache(List<CartPermission> cartPermissions) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CartPermission cartPermission : cartPermissions) {
			EntityCacheUtil.removeResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
				CartPermissionImpl.class, cartPermission.getPrimaryKey());

			clearUniqueFindersCache(cartPermission);
		}
	}

	protected void cacheUniqueFindersCache(CartPermission cartPermission) {
		if (cartPermission.isNew()) {
			Object[] args = new Object[] {
					cartPermission.getCompanyId(),
					cartPermission.getPermissionKey()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CI_PK, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CI_PK, args,
				cartPermission);
		}
		else {
			CartPermissionModelImpl cartPermissionModelImpl = (CartPermissionModelImpl)cartPermission;

			if ((cartPermissionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CI_PK.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						cartPermission.getCompanyId(),
						cartPermission.getPermissionKey()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CI_PK, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CI_PK, args,
					cartPermission);
			}
		}
	}

	protected void clearUniqueFindersCache(CartPermission cartPermission) {
		CartPermissionModelImpl cartPermissionModelImpl = (CartPermissionModelImpl)cartPermission;

		Object[] args = new Object[] {
				cartPermission.getCompanyId(), cartPermission.getPermissionKey()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CI_PK, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CI_PK, args);

		if ((cartPermissionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CI_PK.getColumnBitmask()) != 0) {
			args = new Object[] {
					cartPermissionModelImpl.getOriginalCompanyId(),
					cartPermissionModelImpl.getOriginalPermissionKey()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CI_PK, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CI_PK, args);
		}
	}

	/**
	 * Creates a new cart permission with the primary key. Does not add the cart permission to the database.
	 *
	 * @param permissionId the primary key for the new cart permission
	 * @return the new cart permission
	 */
	@Override
	public CartPermission create(long permissionId) {
		CartPermission cartPermission = new CartPermissionImpl();

		cartPermission.setNew(true);
		cartPermission.setPrimaryKey(permissionId);

		return cartPermission;
	}

	/**
	 * Removes the cart permission with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param permissionId the primary key of the cart permission
	 * @return the cart permission that was removed
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission remove(long permissionId)
		throws NoSuchCartPermissionException, SystemException {
		return remove((Serializable)permissionId);
	}

	/**
	 * Removes the cart permission with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cart permission
	 * @return the cart permission that was removed
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission remove(Serializable primaryKey)
		throws NoSuchCartPermissionException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CartPermission cartPermission = (CartPermission)session.get(CartPermissionImpl.class,
					primaryKey);

			if (cartPermission == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCartPermissionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(cartPermission);
		}
		catch (NoSuchCartPermissionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CartPermission removeImpl(CartPermission cartPermission)
		throws SystemException {
		cartPermission = toUnwrappedModel(cartPermission);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(cartPermission)) {
				cartPermission = (CartPermission)session.get(CartPermissionImpl.class,
						cartPermission.getPrimaryKeyObj());
			}

			if (cartPermission != null) {
				session.delete(cartPermission);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (cartPermission != null) {
			clearCache(cartPermission);
		}

		return cartPermission;
	}

	@Override
	public CartPermission updateImpl(
		com.esquare.ecommerce.model.CartPermission cartPermission)
		throws SystemException {
		cartPermission = toUnwrappedModel(cartPermission);

		boolean isNew = cartPermission.isNew();

		CartPermissionModelImpl cartPermissionModelImpl = (CartPermissionModelImpl)cartPermission;

		Session session = null;

		try {
			session = openSession();

			if (cartPermission.isNew()) {
				session.save(cartPermission);

				cartPermission.setNew(false);
			}
			else {
				session.merge(cartPermission);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CartPermissionModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((cartPermissionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						cartPermissionModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { cartPermissionModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		EntityCacheUtil.putResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
			CartPermissionImpl.class, cartPermission.getPrimaryKey(),
			cartPermission);

		clearUniqueFindersCache(cartPermission);
		cacheUniqueFindersCache(cartPermission);

		return cartPermission;
	}

	protected CartPermission toUnwrappedModel(CartPermission cartPermission) {
		if (cartPermission instanceof CartPermissionImpl) {
			return cartPermission;
		}

		CartPermissionImpl cartPermissionImpl = new CartPermissionImpl();

		cartPermissionImpl.setNew(cartPermission.isNew());
		cartPermissionImpl.setPrimaryKey(cartPermission.getPrimaryKey());

		cartPermissionImpl.setPermissionId(cartPermission.getPermissionId());
		cartPermissionImpl.setCompanyId(cartPermission.getCompanyId());
		cartPermissionImpl.setPermissionKey(cartPermission.getPermissionKey());
		cartPermissionImpl.setValue(cartPermission.getValue());
		cartPermissionImpl.setPrice(cartPermission.getPrice());
		cartPermissionImpl.setUnlimitedPck(cartPermission.isUnlimitedPck());

		return cartPermissionImpl;
	}

	/**
	 * Returns the cart permission with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cart permission
	 * @return the cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCartPermissionException, SystemException {
		CartPermission cartPermission = fetchByPrimaryKey(primaryKey);

		if (cartPermission == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCartPermissionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return cartPermission;
	}

	/**
	 * Returns the cart permission with the primary key or throws a {@link com.esquare.ecommerce.NoSuchCartPermissionException} if it could not be found.
	 *
	 * @param permissionId the primary key of the cart permission
	 * @return the cart permission
	 * @throws com.esquare.ecommerce.NoSuchCartPermissionException if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission findByPrimaryKey(long permissionId)
		throws NoSuchCartPermissionException, SystemException {
		return findByPrimaryKey((Serializable)permissionId);
	}

	/**
	 * Returns the cart permission with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cart permission
	 * @return the cart permission, or <code>null</code> if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CartPermission cartPermission = (CartPermission)EntityCacheUtil.getResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
				CartPermissionImpl.class, primaryKey);

		if (cartPermission == _nullCartPermission) {
			return null;
		}

		if (cartPermission == null) {
			Session session = null;

			try {
				session = openSession();

				cartPermission = (CartPermission)session.get(CartPermissionImpl.class,
						primaryKey);

				if (cartPermission != null) {
					cacheResult(cartPermission);
				}
				else {
					EntityCacheUtil.putResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
						CartPermissionImpl.class, primaryKey,
						_nullCartPermission);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CartPermissionModelImpl.ENTITY_CACHE_ENABLED,
					CartPermissionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return cartPermission;
	}

	/**
	 * Returns the cart permission with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param permissionId the primary key of the cart permission
	 * @return the cart permission, or <code>null</code> if a cart permission with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CartPermission fetchByPrimaryKey(long permissionId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)permissionId);
	}

	/**
	 * Returns all the cart permissions.
	 *
	 * @return the cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cart permissions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cart permissions
	 * @param end the upper bound of the range of cart permissions (not inclusive)
	 * @return the range of cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the cart permissions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cart permissions
	 * @param end the upper bound of the range of cart permissions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CartPermission> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CartPermission> list = (List<CartPermission>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CARTPERMISSION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CARTPERMISSION;

				if (pagination) {
					sql = sql.concat(CartPermissionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CartPermission>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CartPermission>(list);
				}
				else {
					list = (List<CartPermission>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the cart permissions from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CartPermission cartPermission : findAll()) {
			remove(cartPermission);
		}
	}

	/**
	 * Returns the number of cart permissions.
	 *
	 * @return the number of cart permissions
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CARTPERMISSION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the cart permission persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.CartPermission")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CartPermission>> listenersList = new ArrayList<ModelListener<CartPermission>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CartPermission>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CartPermissionImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CARTPERMISSION = "SELECT cartPermission FROM CartPermission cartPermission";
	private static final String _SQL_SELECT_CARTPERMISSION_WHERE = "SELECT cartPermission FROM CartPermission cartPermission WHERE ";
	private static final String _SQL_COUNT_CARTPERMISSION = "SELECT COUNT(cartPermission) FROM CartPermission cartPermission";
	private static final String _SQL_COUNT_CARTPERMISSION_WHERE = "SELECT COUNT(cartPermission) FROM CartPermission cartPermission WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "cartPermission.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CartPermission exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CartPermission exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CartPermissionPersistenceImpl.class);
	private static CartPermission _nullCartPermission = new CartPermissionImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CartPermission> toCacheModel() {
				return _nullCartPermissionCacheModel;
			}
		};

	private static CacheModel<CartPermission> _nullCartPermissionCacheModel = new CacheModel<CartPermission>() {
			@Override
			public CartPermission toEntityModel() {
				return _nullCartPermission;
			}
		};
}