package com.esquare.ecommerce.storeSettings.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.xml.stream.XMLStreamException;

import com.esquare.ecommerce.model.SMSConfiguration;
import com.esquare.ecommerce.model.ShippingRates;
import com.esquare.ecommerce.model.TaxesSetting;
import com.esquare.ecommerce.model.impl.SMSConfigurationImpl;
import com.esquare.ecommerce.model.impl.ShippingRatesImpl;
import com.esquare.ecommerce.model.impl.TaxesSettingImpl;
import com.esquare.ecommerce.service.SMSConfigurationLocalServiceUtil;
import com.esquare.ecommerce.service.ShippingRatesLocalServiceUtil;
import com.esquare.ecommerce.service.TaxesSettingLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.EmailUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.esquare.ecommerce.util.SMSUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropertiesParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Country;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Region;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class StoreSettingsAction extends MVCPortlet {
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {

		String tabs = ParamUtil.getString(actionRequest, "tabs");
		String cmd = ParamUtil.getString(actionRequest,
				EECConstants.CMD_SETTING);
		try {
			if (cmd.equals(EECConstants.SHIPPING_SETTING)) {
				updateShippingRate(actionRequest, actionResponse);
			} else if (cmd.equals(EECConstants.TAXES_SETTING)) {
				updateTaxesRate(actionRequest, actionResponse);
			} else if (cmd.equals(EECConstants.EMAIL)) {
				updateEmailPreference(actionRequest, actionResponse);
			} else if (cmd.equals(EECConstants.SMS)) {
				if (tabs.equals(EECConstants.SMS)) {
					saveSMSConfiguration(actionRequest, actionResponse);
				} else {
					updateSMSPreference(actionRequest, actionResponse);
				}
			} else if (cmd.equals(EECConstants.SOCIAL_NETWORK)) {
				updatePreference(actionRequest, actionResponse);
				actionResponse.setRenderParameter("tabs", tabs);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}

		// General Settings
		UploadPortletRequest uploadRequest = PortalUtil
				.getUploadPortletRequest(actionRequest);
		String cmd_General = ParamUtil.getString(uploadRequest,
				EECConstants.CMD);
		try {
			if (cmd_General.equals(EECConstants.GENERAL_ADMIN_SETTINGS)) {
				updateGeneralSettings(uploadRequest);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}

	}

	private static void saveSMSConfiguration(ActionRequest actionRequest,
			ActionResponse actionResponse) throws SystemException {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String smsType = ParamUtil.getString(actionRequest, "type");
		String userName = ParamUtil.getString(actionRequest, "userName");
		String password = ParamUtil.getString(actionRequest, "password");

		SMSConfiguration smsConfiguration = new SMSConfigurationImpl();
		long id = CounterLocalServiceUtil.increment();
		smsConfiguration.setId(id);
		smsConfiguration.setCompanyId(themeDisplay.getCompanyId());
		smsConfiguration.setUserId(themeDisplay.getUserId());
		smsConfiguration.setSmsType(smsType);
		smsConfiguration.setUserName(userName);
		smsConfiguration.setPassword(password);

		SMSConfigurationLocalServiceUtil.addSMSConfiguration(smsConfiguration);
	}

	private void updateGeneralSettings(UploadPortletRequest uploadRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) uploadRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		try {
			UnicodeProperties properties = PropertiesParamUtil.getProperties(
					uploadRequest, "settings--");
			CompanyLocalServiceUtil.updatePreferences(
					themeDisplay.getCompanyId(), properties);
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		// Google Analytics
		Group liveGroup = null;
		try {
			liveGroup = GroupLocalServiceUtil.getGroup(
					themeDisplay.getCompanyId(), EECConstants.GUEST);
			UnicodeProperties typeSettingsProperties = liveGroup
					.getTypeSettingsProperties();
			String googleAnalyticsId = ParamUtil.getString(uploadRequest,
					"googleAnalyticsId",
					typeSettingsProperties.getProperty("googleAnalyticsId"));
			typeSettingsProperties.setProperty("googleAnalyticsId",
					googleAnalyticsId);

			GroupLocalServiceUtil.updateGroup(liveGroup.getGroupId(),
					typeSettingsProperties.toString());
		} catch (PortalException e) {
		} catch (SystemException e) {
		}

		// Change company logo
		File file = uploadRequest.getFile("myImageFile");
		try {
			LayoutSetLocalServiceUtil.updateLogo(liveGroup.getGroupId(), false,
					true, file);
		} catch (PortalException e) {
		} catch (SystemException e) {
		}

	}

	private void updateTaxesRate(ActionRequest actionRequest,
			ActionResponse actionResponse) throws SystemException,
			PortalException, XMLStreamException, IOException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long recId = ParamUtil.getLong(actionRequest, "recId");
		String redirect = ParamUtil.getString(actionRequest, "redirect");
		String country = ParamUtil.getString(actionRequest, "country");
		String name = ParamUtil.getString(actionRequest, "name");
		double taxRate = ParamUtil.getDouble(actionRequest, "taxRate");
		if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.EDIT)) {
			TaxesSetting taxesSetting = new TaxesSettingImpl();
			taxesSetting.setCompanyId(themeDisplay.getCompanyId());
			taxesSetting.setCountryId(country);
			taxesSetting.setName(name);
			taxesSetting.setTaxRate(taxRate);

			String xml = StringPool.BLANK;
			StringBuffer regionId = new StringBuffer();
			StringBuffer regionName = new StringBuffer();
			StringBuffer regiontaxes = new StringBuffer();
			Country countryobj = CountryServiceUtil.getCountryByA2(country);
			List<Region> regionlist = RegionServiceUtil.getRegions(countryobj
					.getCountryId());
			for (int i = 0; i < regionlist.size(); i++) {
				regionId.append(regionlist.get(i).getRegionId());
				regionId.append(",");
				regionName.append(regionlist.get(i).getName());
				regionName.append(",");
				regiontaxes.append(taxRate);
				regiontaxes.append(",");
			}

			if (recId <= 0) {
				taxesSetting.setRecId(CounterLocalServiceUtil
						.increment(TaxesSetting.class.getName()));
				String[] regionIds = regionId.toString().split(",");
				String[] regionNames = regionName.toString().split(",");
				String[] regionprice = regiontaxes.toString().split(",");
				xml = EECUtil.addXmlContent(StringPool.BLANK, "region", "id",
						regionIds, regionNames, regionprice);
				taxesSetting.setXml(xml);
				TaxesSettingLocalServiceUtil.addTaxesSetting(taxesSetting);
				actionResponse.sendRedirect(redirect);
			} else {
				StringBuffer regionValue = new StringBuffer();
				String[] regionIds = ParamUtil.getParameterValues(
						actionRequest, "regionId");
				// String[] regionprice =
				// ParamUtil.getParameterValues(actionRequest, "regionValue");
				String[] regionNames = ParamUtil.getParameterValues(
						actionRequest, "regionName");

				for (int i = 0; i < regionIds.length; i++) {
					regionValue.append(ParamUtil.getString(actionRequest,
							"regionValue" + i));
					regionValue.append(",");
				}

				String[] regionprice = regionValue.toString().split(",");
				taxesSetting.setRecId(recId);
				xml = EECUtil.addXmlContent(StringPool.BLANK, "region", "id",
						regionIds, regionNames, regionprice);
				taxesSetting.setXml(xml);
				TaxesSettingLocalServiceUtil.updateTaxesSetting(taxesSetting);
				actionResponse.sendRedirect(redirect);
			}
		} else if (cmd.equals(EECConstants.DELETE)) {
			long[] recIds = StringUtil
					.split(ParamUtil.getString(actionRequest,
							"deleteTaxesRecIds"), 0L);
			if (Validator.isNotNull(recIds)) {
				for (int i = 0; i < recIds.length; i++) {
					TaxesSettingLocalServiceUtil.deleteTaxesSetting(recIds[i]);
				}
			}
		}

	}

	private void updateShippingRate(ActionRequest actionRequest,
			ActionResponse actionResponse) throws SystemException,
			PortalException, XMLStreamException, IOException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long recId = ParamUtil.getLong(actionRequest, "recId");
		String redirect = ParamUtil.getString(actionRequest, "redirect");
		String country = ParamUtil.getString(actionRequest, "country");
		String name = ParamUtil.getString(actionRequest, "name");
		String criteriaType = ParamUtil
				.getString(actionRequest, "criteriaType");
		double rangeFrom = ParamUtil.getDouble(actionRequest, "rangeFrom");
		double rangeTo = ParamUtil.getDouble(actionRequest, "rangeTo");
		double price = ParamUtil.getLong(actionRequest, "price");
		if (cmd.equals(EECConstants.DELETE)) {
			ShippingRatesLocalServiceUtil.deleteShippingRates(recId);
		} else {
			if (cmd.equals(EECConstants.ADD_COUNTRY)) {
				name = PortletPropsValues.DEFAULT_SHIPPINGRATE_NAME;
				criteriaType = PortletPropsValues.SHIPPING_WEIGHT_CRITERIA;
				rangeFrom = PortletPropsValues.DEFAULT_SHIPPING_RANGE_FROM;
				rangeTo = PortletPropsValues.DEFAULT_SHIPPING_RANGE_TO;
				price = PortletPropsValues.DEFAULT_SHIPPING_PRICE;
			}

			ShippingRates shippingRates = new ShippingRatesImpl();
			shippingRates.setCompanyId(themeDisplay.getCompanyId());
			shippingRates.setCountryId(country);
			shippingRates.setName(name);
			shippingRates.setShippingType(criteriaType);
			shippingRates.setRangeFrom(rangeFrom);
			shippingRates.setRangeTo(rangeTo);
			shippingRates.setPrice(price);

			String xml = StringPool.BLANK;
			StringBuffer regionId = new StringBuffer();
			StringBuffer regionName = new StringBuffer();
			StringBuffer regionprices = new StringBuffer();

			Country countryobj = CountryServiceUtil.getCountryByA2(country);
			List<Region> regionlist = RegionServiceUtil.getRegions(countryobj
					.getCountryId());
			for (int i = 0; i < regionlist.size(); i++) {
				regionId.append(regionlist.get(i).getRegionId());
				regionId.append(",");
				regionName.append(regionlist.get(i).getName());
				regionName.append(",");
				regionprices.append(price);
				regionprices.append(",");
			}

			if (recId <= 0) {
				shippingRates.setRecId(CounterLocalServiceUtil
						.increment(ShippingRates.class.getName()));
				String[] regionIds = regionId.toString().split(",");
				String[] regionNames = regionName.toString().split(",");
				String[] regionprice = regionprices.toString().split(",");
				xml = EECUtil.addXmlContent(StringPool.BLANK, "region", "id",
						regionIds, regionNames, regionprice);
				shippingRates.setXml(xml);
				ShippingRatesLocalServiceUtil.addShippingRates(shippingRates);
				actionResponse.sendRedirect(redirect);
			} else {
				StringBuffer regionValue = new StringBuffer();
				String[] regionIds = ParamUtil.getParameterValues(
						actionRequest, "regionId");
				// String[] regionprice =
				// ParamUtil.getParameterValues(actionRequest, "regionValue");
				for (int i = 0; i < regionIds.length; i++) {
					regionValue.append(ParamUtil.getString(actionRequest,
							"regionValue" + i));
					regionValue.append(",");
				}
				String[] regionNames = ParamUtil.getParameterValues(
						actionRequest, "regionName");
				shippingRates.setRecId(recId);
				String[] regionprice = regionValue.toString().split(",");
				xml = EECUtil.addXmlContent(StringPool.BLANK, "region", "id",
						regionIds, regionNames, regionprice);
				shippingRates.setXml(xml);
				ShippingRatesLocalServiceUtil
						.updateShippingRates(shippingRates);
				actionResponse.sendRedirect(redirect);
			}
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		resourceResponse.setContentType("text/javascript");
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		// Instance instance = null;
		try {
			String cmd = ParamUtil.getString(resourceRequest, EECConstants.CMD);
			if (cmd.equals("SHPPING_NAME")) {
				resourceRequest.getParameter("shippingName");
				ParamUtil.getLong(resourceRequest, "companyId");
				boolean isshippingName = false;
				jsonObject.put("retVal", isshippingName);
				PrintWriter writer = resourceResponse.getWriter();
				writer.write(jsonObject.toString());
			} else if (cmd.equals(EECConstants.SHIPPING_SETTING)) {
				String cmd1 = ParamUtil.getString(resourceRequest,
						EECConstants.CMD1);
				if (cmd1.equals("RANGES")) {
					try {
						boolean isShippingRanges = false;
						String countryId = ParamUtil.getString(resourceRequest,
								"countryId");
						long companyId = ParamUtil.getLong(resourceRequest,
								"companyId");
						long recId = ParamUtil.getLong(resourceRequest,
								"recId", 0L);
						String shippingType = resourceRequest
								.getParameter("criteriaType");
						double rangeFrom = ParamUtil.getDouble(resourceRequest,
								"rangeFrom");
						double rangeTo = ParamUtil.getDouble(resourceRequest,
								"rangeTo");

						isShippingRanges = ShippingRatesLocalServiceUtil
								.findShippingRanges(companyId, countryId,
										recId, shippingType, rangeFrom, rangeTo);
						jsonObject.put("retVal", isShippingRanges);
					} catch (Exception e) {
						_log.info(e.getClass());
					}
				} else {
					String countryName = resourceRequest
							.getParameter("countryName");
					long companyId = ParamUtil.getLong(resourceRequest,
							"companyId");
					List<ShippingRates> shippingRatesList = ShippingRatesLocalServiceUtil
							.findByCountry(companyId, countryName);
					boolean isCountry = false;
					if (Validator.isNotNull(shippingRatesList)
							&& (shippingRatesList.size() > 0)) {
						isCountry = true;
					}
					jsonObject.put("retVal", isCountry);
				}
				PrintWriter writer = resourceResponse.getWriter();
				writer.write(jsonObject.toString());
			} else {
				String country = resourceRequest.getParameter("param1");
				String companyId = resourceRequest.getParameter("param2");
				TaxesSetting taxSetting = TaxesSettingLocalServiceUtil
						.getTaxesRate(Long.parseLong(companyId), country);
				ArrayList<String> strList = new ArrayList<String>();
				strList.add(String.valueOf(taxSetting.getRecId()));
				strList.add(String.valueOf(taxSetting.getCompanyId()));
				jsonObject.put("retVal1", "Returing First value from server");
				jsonObject.put("retVal2", strList.toString());
				PrintWriter writer = resourceResponse.getWriter();
				writer.write(jsonObject.toString());
			}

		} catch (Exception e) {
			// ignore
		}
	}

	public void updateEmailPreference(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		String tabs = ParamUtil.getString(actionRequest, "tabs");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String emailConfirmation_subject = ".emailConfirmation.subject";
		String emailConfirmation_body = ".emailConfirmation.body";
		String emailFromName = ParamUtil.getString(actionRequest,
				"emailFromName");
		String emailFromAddress = ParamUtil.getString(actionRequest,
				"emailFromAddress");
		String emailToName = ParamUtil.getString(actionRequest, "emailToName");
		String emailToAddress = ParamUtil.getString(actionRequest,
				"emailToAddress");
		String emailsubject = ParamUtil
				.getString(actionRequest, "emailsubject");
		String editorContent = ParamUtil.getString(actionRequest,
				"editorContent");
		try {
			PortletPreferences preferences = PrefsPropsUtil
					.getPreferences(themeDisplay.getCompanyId());
			if (tabs.equals("CreateAccount")) {

				preferences.setValue("admin.email.user.added.subject",
						emailsubject);
				preferences.setValue("admin.email.user.added.body",
						editorContent);
				preferences.store();
			} else if (tabs.equals("Email")) {
				preferences.setValue(PropsKeys.ADMIN_EMAIL_FROM_NAME,
						emailFromName);
				preferences.setValue(PropsKeys.ADMIN_EMAIL_FROM_ADDRESS,
						emailFromAddress);
				preferences.setValue(EECConstants.ADMIN_EMAIL_TO_NAME,
						emailToName);
				preferences.setValue(EECConstants.ADMIN_EMAIL_TO_ADDRESS,
						emailToAddress);
				preferences.store();
			} else {
				preferences.setValue(tabs + emailConfirmation_subject,
						emailsubject);
				preferences.setValue(tabs + emailConfirmation_body,
						editorContent);
				preferences.store();
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (ReadOnlyException e) {
			_log.info(e.getClass());
		} catch (ValidatorException e) {
			_log.info(e.getClass());
		} catch (IOException e) {
			_log.info(e.getClass());
		}
		actionResponse.setRenderParameter("tabs", tabs);
	}

	public void updateSMSPreference(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortletException,
			SystemException, IOException {

		String tabs = ParamUtil.getString(actionRequest, "tabs");
		String smsConfirmation_subject = ".smsConfirmation.subject";
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		String textMessage = ParamUtil.getString(actionRequest, "textMessage");

		try {
			PortletPreferences preferences = PrefsPropsUtil
					.getPreferences(themeDisplay.getCompanyId());

			preferences.setValue(tabs + smsConfirmation_subject, textMessage);
			preferences.store();

		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (ReadOnlyException e) {
			_log.info(e.getClass());
		} catch (ValidatorException e) {
			_log.info(e.getClass());
		} catch (IOException e) {
			_log.info(e.getClass());
		}

		actionResponse.setRenderParameter("tabs", tabs);

	}

	public void updatePreference(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException,
			SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		UnicodeProperties properties = PropertiesParamUtil.getProperties(
				actionRequest, "settings--");
		CompanyLocalServiceUtil.updatePreferences(themeDisplay.getCompanyId(),
				properties);
	}

	@SuppressWarnings("unused")
	private String callWay2SMS(String userName, String password, long id,
			String mobileNumber, String textMessage, String action) {
		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {

			// custid=undefined&HiddenAction=instantsms&Action="+action+"&login=&pass=&MobNo="+ phoneNumber+ "&textArea="+message;

			smsSenderURLQueryString.append("custid=undefined");
			smsSenderURLQueryString.append("&HiddenAction=instantsms");
			smsSenderURLQueryString.append("&Action="
					+ URLEncoder.encode(password, "UTF-8"));
			smsSenderURLQueryString.append("&login=&pass=&MobNo="
					+ URLEncoder.encode(mobileNumber, "UTF-8"));
			smsSenderURLQueryString.append("&textArea="
					+ URLEncoder.encode(textMessage, "UTF-8"));

			completeSenderURLString = EECConstants.WAY2_SMS_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private String callVianet(String userName, String password, long id,
			String mobileNumber, String textMessage) {
		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {
			smsSenderURLQueryString.append("username="
					+ URLEncoder.encode(userName, "UTF-8"));
			smsSenderURLQueryString.append("&password="
					+ URLEncoder.encode(password, "UTF-8"));
			smsSenderURLQueryString.append("&msgid="
					+ URLEncoder.encode(String.valueOf(id), "UTF-8"));
			smsSenderURLQueryString.append("&tel="
					+ URLEncoder.encode(EECConstants.COUNTRY_CODE
							+ mobileNumber, "UTF-8"));
			smsSenderURLQueryString.append("&msg="
					+ URLEncoder.encode(textMessage, "UTF-8"));

			completeSenderURLString = EECConstants.VIANET_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private String callSMSHorizon(String userName, String password,
			String mobileNumber, String textMessage) {
		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {
			smsSenderURLQueryString.append("user="
					+ URLEncoder.encode("Faisal", "UTF-8"));
			smsSenderURLQueryString.append("&apikey="
					+ URLEncoder.encode(EECConstants.SMS_HORIZON_API_VALUE,
							"UTF-8"));
			smsSenderURLQueryString.append("&mobile="
					+ URLEncoder.encode("8123397467", "UTF-8"));
			smsSenderURLQueryString.append("&message="
					+ URLEncoder.encode(textMessage, "UTF-8"));
			smsSenderURLQueryString.append("&senderid="
					+ URLEncoder.encode("MYTEXT", "UTF-8"));
			smsSenderURLQueryString.append("&type=" + "TXT");

			completeSenderURLString = EECConstants.SMS_HORIZON_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private static Log _log = LogFactoryUtil.getLog(StoreSettingsAction.class);
}
