/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.EcustomFieldValue;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing EcustomFieldValue in entity cache.
 *
 * @author Esquare
 * @see EcustomFieldValue
 * @generated
 */
public class EcustomFieldValueCacheModel implements CacheModel<EcustomFieldValue>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{customValueRecId=");
		sb.append(customValueRecId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", inventoryId=");
		sb.append(inventoryId);
		sb.append(", xml=");
		sb.append(xml);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EcustomFieldValue toEntityModel() {
		EcustomFieldValueImpl ecustomFieldValueImpl = new EcustomFieldValueImpl();

		ecustomFieldValueImpl.setCustomValueRecId(customValueRecId);
		ecustomFieldValueImpl.setCompanyId(companyId);
		ecustomFieldValueImpl.setInventoryId(inventoryId);

		if (xml == null) {
			ecustomFieldValueImpl.setXml(StringPool.BLANK);
		}
		else {
			ecustomFieldValueImpl.setXml(xml);
		}

		ecustomFieldValueImpl.resetOriginalValues();

		return ecustomFieldValueImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		customValueRecId = objectInput.readLong();
		companyId = objectInput.readLong();
		inventoryId = objectInput.readLong();
		xml = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(customValueRecId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(inventoryId);

		if (xml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(xml);
		}
	}

	public long customValueRecId;
	public long companyId;
	public long inventoryId;
	public String xml;
}