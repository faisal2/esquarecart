package com.esquare.ecommerce.model.bulkdataupload;

public class ErrorPageModel {

	private boolean isError;
	private String errorMessage;
	public boolean isError() {
		return isError;
	}
	public void setError(boolean isError) {
		this.isError = isError;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
}
