/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Order;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Order in entity cache.
 *
 * @author Esquare
 * @see Order
 * @generated
 */
public class OrderCacheModel implements CacheModel<Order>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(91);

		sb.append("{orderId=");
		sb.append(orderId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", number=");
		sb.append(number);
		sb.append(", tax=");
		sb.append(tax);
		sb.append(", shipping=");
		sb.append(shipping);
		sb.append(", altShipping=");
		sb.append(altShipping);
		sb.append(", requiresShipping=");
		sb.append(requiresShipping);
		sb.append(", insure=");
		sb.append(insure);
		sb.append(", insurance=");
		sb.append(insurance);
		sb.append(", couponCodes=");
		sb.append(couponCodes);
		sb.append(", couponVersion=");
		sb.append(couponVersion);
		sb.append(", couponDiscount=");
		sb.append(couponDiscount);
		sb.append(", walletAmount=");
		sb.append(walletAmount);
		sb.append(", billingFirstName=");
		sb.append(billingFirstName);
		sb.append(", billingLastName=");
		sb.append(billingLastName);
		sb.append(", billingEmailAddress=");
		sb.append(billingEmailAddress);
		sb.append(", billingCompany=");
		sb.append(billingCompany);
		sb.append(", billingStreet=");
		sb.append(billingStreet);
		sb.append(", billingCity=");
		sb.append(billingCity);
		sb.append(", billingState=");
		sb.append(billingState);
		sb.append(", billingZip=");
		sb.append(billingZip);
		sb.append(", billingCountry=");
		sb.append(billingCountry);
		sb.append(", billingPhone=");
		sb.append(billingPhone);
		sb.append(", shipToBilling=");
		sb.append(shipToBilling);
		sb.append(", shippingFirstName=");
		sb.append(shippingFirstName);
		sb.append(", shippingLastName=");
		sb.append(shippingLastName);
		sb.append(", shippingEmailAddress=");
		sb.append(shippingEmailAddress);
		sb.append(", shippingCompany=");
		sb.append(shippingCompany);
		sb.append(", shippingStreet=");
		sb.append(shippingStreet);
		sb.append(", shippingCity=");
		sb.append(shippingCity);
		sb.append(", shippingState=");
		sb.append(shippingState);
		sb.append(", shippingZip=");
		sb.append(shippingZip);
		sb.append(", shippingCountry=");
		sb.append(shippingCountry);
		sb.append(", shippingPhone=");
		sb.append(shippingPhone);
		sb.append(", comments=");
		sb.append(comments);
		sb.append(", sendOrderEmail=");
		sb.append(sendOrderEmail);
		sb.append(", paymentType=");
		sb.append(paymentType);
		sb.append(", paymentStatus=");
		sb.append(paymentStatus);
		sb.append(", status=");
		sb.append(status);
		sb.append(", history=");
		sb.append(history);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Order toEntityModel() {
		OrderImpl orderImpl = new OrderImpl();

		orderImpl.setOrderId(orderId);
		orderImpl.setGroupId(groupId);
		orderImpl.setCompanyId(companyId);
		orderImpl.setUserId(userId);

		if (userName == null) {
			orderImpl.setUserName(StringPool.BLANK);
		}
		else {
			orderImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			orderImpl.setCreateDate(null);
		}
		else {
			orderImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			orderImpl.setModifiedDate(null);
		}
		else {
			orderImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (number == null) {
			orderImpl.setNumber(StringPool.BLANK);
		}
		else {
			orderImpl.setNumber(number);
		}

		orderImpl.setTax(tax);
		orderImpl.setShipping(shipping);

		if (altShipping == null) {
			orderImpl.setAltShipping(StringPool.BLANK);
		}
		else {
			orderImpl.setAltShipping(altShipping);
		}

		orderImpl.setRequiresShipping(requiresShipping);
		orderImpl.setInsure(insure);
		orderImpl.setInsurance(insurance);

		if (couponCodes == null) {
			orderImpl.setCouponCodes(StringPool.BLANK);
		}
		else {
			orderImpl.setCouponCodes(couponCodes);
		}

		orderImpl.setCouponVersion(couponVersion);
		orderImpl.setCouponDiscount(couponDiscount);
		orderImpl.setWalletAmount(walletAmount);

		if (billingFirstName == null) {
			orderImpl.setBillingFirstName(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingFirstName(billingFirstName);
		}

		if (billingLastName == null) {
			orderImpl.setBillingLastName(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingLastName(billingLastName);
		}

		if (billingEmailAddress == null) {
			orderImpl.setBillingEmailAddress(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingEmailAddress(billingEmailAddress);
		}

		if (billingCompany == null) {
			orderImpl.setBillingCompany(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingCompany(billingCompany);
		}

		if (billingStreet == null) {
			orderImpl.setBillingStreet(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingStreet(billingStreet);
		}

		if (billingCity == null) {
			orderImpl.setBillingCity(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingCity(billingCity);
		}

		if (billingState == null) {
			orderImpl.setBillingState(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingState(billingState);
		}

		if (billingZip == null) {
			orderImpl.setBillingZip(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingZip(billingZip);
		}

		if (billingCountry == null) {
			orderImpl.setBillingCountry(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingCountry(billingCountry);
		}

		if (billingPhone == null) {
			orderImpl.setBillingPhone(StringPool.BLANK);
		}
		else {
			orderImpl.setBillingPhone(billingPhone);
		}

		orderImpl.setShipToBilling(shipToBilling);

		if (shippingFirstName == null) {
			orderImpl.setShippingFirstName(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingFirstName(shippingFirstName);
		}

		if (shippingLastName == null) {
			orderImpl.setShippingLastName(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingLastName(shippingLastName);
		}

		if (shippingEmailAddress == null) {
			orderImpl.setShippingEmailAddress(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingEmailAddress(shippingEmailAddress);
		}

		if (shippingCompany == null) {
			orderImpl.setShippingCompany(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingCompany(shippingCompany);
		}

		if (shippingStreet == null) {
			orderImpl.setShippingStreet(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingStreet(shippingStreet);
		}

		if (shippingCity == null) {
			orderImpl.setShippingCity(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingCity(shippingCity);
		}

		if (shippingState == null) {
			orderImpl.setShippingState(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingState(shippingState);
		}

		if (shippingZip == null) {
			orderImpl.setShippingZip(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingZip(shippingZip);
		}

		if (shippingCountry == null) {
			orderImpl.setShippingCountry(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingCountry(shippingCountry);
		}

		if (shippingPhone == null) {
			orderImpl.setShippingPhone(StringPool.BLANK);
		}
		else {
			orderImpl.setShippingPhone(shippingPhone);
		}

		if (comments == null) {
			orderImpl.setComments(StringPool.BLANK);
		}
		else {
			orderImpl.setComments(comments);
		}

		orderImpl.setSendOrderEmail(sendOrderEmail);

		if (paymentType == null) {
			orderImpl.setPaymentType(StringPool.BLANK);
		}
		else {
			orderImpl.setPaymentType(paymentType);
		}

		if (paymentStatus == null) {
			orderImpl.setPaymentStatus(StringPool.BLANK);
		}
		else {
			orderImpl.setPaymentStatus(paymentStatus);
		}

		if (status == null) {
			orderImpl.setStatus(StringPool.BLANK);
		}
		else {
			orderImpl.setStatus(status);
		}

		if (history == null) {
			orderImpl.setHistory(StringPool.BLANK);
		}
		else {
			orderImpl.setHistory(history);
		}

		orderImpl.resetOriginalValues();

		return orderImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		orderId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		number = objectInput.readUTF();
		tax = objectInput.readDouble();
		shipping = objectInput.readDouble();
		altShipping = objectInput.readUTF();
		requiresShipping = objectInput.readBoolean();
		insure = objectInput.readBoolean();
		insurance = objectInput.readDouble();
		couponCodes = objectInput.readUTF();
		couponVersion = objectInput.readDouble();
		couponDiscount = objectInput.readDouble();
		walletAmount = objectInput.readDouble();
		billingFirstName = objectInput.readUTF();
		billingLastName = objectInput.readUTF();
		billingEmailAddress = objectInput.readUTF();
		billingCompany = objectInput.readUTF();
		billingStreet = objectInput.readUTF();
		billingCity = objectInput.readUTF();
		billingState = objectInput.readUTF();
		billingZip = objectInput.readUTF();
		billingCountry = objectInput.readUTF();
		billingPhone = objectInput.readUTF();
		shipToBilling = objectInput.readBoolean();
		shippingFirstName = objectInput.readUTF();
		shippingLastName = objectInput.readUTF();
		shippingEmailAddress = objectInput.readUTF();
		shippingCompany = objectInput.readUTF();
		shippingStreet = objectInput.readUTF();
		shippingCity = objectInput.readUTF();
		shippingState = objectInput.readUTF();
		shippingZip = objectInput.readUTF();
		shippingCountry = objectInput.readUTF();
		shippingPhone = objectInput.readUTF();
		comments = objectInput.readUTF();
		sendOrderEmail = objectInput.readBoolean();
		paymentType = objectInput.readUTF();
		paymentStatus = objectInput.readUTF();
		status = objectInput.readUTF();
		history = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(orderId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (number == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(number);
		}

		objectOutput.writeDouble(tax);
		objectOutput.writeDouble(shipping);

		if (altShipping == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(altShipping);
		}

		objectOutput.writeBoolean(requiresShipping);
		objectOutput.writeBoolean(insure);
		objectOutput.writeDouble(insurance);

		if (couponCodes == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(couponCodes);
		}

		objectOutput.writeDouble(couponVersion);
		objectOutput.writeDouble(couponDiscount);
		objectOutput.writeDouble(walletAmount);

		if (billingFirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingFirstName);
		}

		if (billingLastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingLastName);
		}

		if (billingEmailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingEmailAddress);
		}

		if (billingCompany == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingCompany);
		}

		if (billingStreet == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingStreet);
		}

		if (billingCity == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingCity);
		}

		if (billingState == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingState);
		}

		if (billingZip == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingZip);
		}

		if (billingCountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingCountry);
		}

		if (billingPhone == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(billingPhone);
		}

		objectOutput.writeBoolean(shipToBilling);

		if (shippingFirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingFirstName);
		}

		if (shippingLastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingLastName);
		}

		if (shippingEmailAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingEmailAddress);
		}

		if (shippingCompany == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingCompany);
		}

		if (shippingStreet == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingStreet);
		}

		if (shippingCity == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingCity);
		}

		if (shippingState == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingState);
		}

		if (shippingZip == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingZip);
		}

		if (shippingCountry == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingCountry);
		}

		if (shippingPhone == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingPhone);
		}

		if (comments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comments);
		}

		objectOutput.writeBoolean(sendOrderEmail);

		if (paymentType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paymentType);
		}

		if (paymentStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paymentStatus);
		}

		if (status == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(status);
		}

		if (history == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(history);
		}
	}

	public long orderId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String number;
	public double tax;
	public double shipping;
	public String altShipping;
	public boolean requiresShipping;
	public boolean insure;
	public double insurance;
	public String couponCodes;
	public double couponVersion;
	public double couponDiscount;
	public double walletAmount;
	public String billingFirstName;
	public String billingLastName;
	public String billingEmailAddress;
	public String billingCompany;
	public String billingStreet;
	public String billingCity;
	public String billingState;
	public String billingZip;
	public String billingCountry;
	public String billingPhone;
	public boolean shipToBilling;
	public String shippingFirstName;
	public String shippingLastName;
	public String shippingEmailAddress;
	public String shippingCompany;
	public String shippingStreet;
	public String shippingCity;
	public String shippingState;
	public String shippingZip;
	public String shippingCountry;
	public String shippingPhone;
	public String comments;
	public boolean sendOrderEmail;
	public String paymentType;
	public String paymentStatus;
	public String status;
	public String history;
}