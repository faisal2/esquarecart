/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchWishListException;
import com.esquare.ecommerce.model.WishList;
import com.esquare.ecommerce.model.impl.WishListImpl;
import com.esquare.ecommerce.model.impl.WishListModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the wish list service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see WishListPersistence
 * @see WishListUtil
 * @generated
 */
public class WishListPersistenceImpl extends BasePersistenceImpl<WishList>
	implements WishListPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WishListUtil} to access the wish list persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WishListImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, WishListImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, WishListImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_C_U = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, WishListImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByC_U",
			new String[] { Long.class.getName(), Long.class.getName() },
			WishListModelImpl.COMPANYID_COLUMN_BITMASK |
			WishListModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_U = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_U",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the wish list where companyId = &#63; and userId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchWishListException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the matching wish list
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList findByC_U(long companyId, long userId)
		throws NoSuchWishListException, SystemException {
		WishList wishList = fetchByC_U(companyId, userId);

		if (wishList == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchWishListException(msg.toString());
		}

		return wishList;
	}

	/**
	 * Returns the wish list where companyId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the matching wish list, or <code>null</code> if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByC_U(long companyId, long userId)
		throws SystemException {
		return fetchByC_U(companyId, userId, true);
	}

	/**
	 * Returns the wish list where companyId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching wish list, or <code>null</code> if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByC_U(long companyId, long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_U,
					finderArgs, this);
		}

		if (result instanceof WishList) {
			WishList wishList = (WishList)result;

			if ((companyId != wishList.getCompanyId()) ||
					(userId != wishList.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_WISHLIST_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				List<WishList> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"WishListPersistenceImpl.fetchByC_U(long, long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					WishList wishList = list.get(0);

					result = wishList;

					cacheResult(wishList);

					if ((wishList.getCompanyId() != companyId) ||
							(wishList.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
							finderArgs, wishList);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (WishList)result;
		}
	}

	/**
	 * Removes the wish list where companyId = &#63; and userId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the wish list that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList removeByC_U(long companyId, long userId)
		throws NoSuchWishListException, SystemException {
		WishList wishList = findByC_U(companyId, userId);

		return remove(wishList);
	}

	/**
	 * Returns the number of wish lists where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the number of matching wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_U(long companyId, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_U;

		Object[] finderArgs = new Object[] { companyId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_WISHLIST_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_U_COMPANYID_2 = "wishList.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_U_USERID_2 = "wishList.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_G_U = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, WishListImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByG_U",
			new String[] { Long.class.getName(), Long.class.getName() },
			WishListModelImpl.GROUPID_COLUMN_BITMASK |
			WishListModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_G_U = new FinderPath(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_U",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the wish list where groupId = &#63; and userId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchWishListException} if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the matching wish list
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList findByG_U(long groupId, long userId)
		throws NoSuchWishListException, SystemException {
		WishList wishList = fetchByG_U(groupId, userId);

		if (wishList == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchWishListException(msg.toString());
		}

		return wishList;
	}

	/**
	 * Returns the wish list where groupId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the matching wish list, or <code>null</code> if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByG_U(long groupId, long userId)
		throws SystemException {
		return fetchByG_U(groupId, userId, true);
	}

	/**
	 * Returns the wish list where groupId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching wish list, or <code>null</code> if a matching wish list could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByG_U(long groupId, long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { groupId, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_G_U,
					finderArgs, this);
		}

		if (result instanceof WishList) {
			WishList wishList = (WishList)result;

			if ((groupId != wishList.getGroupId()) ||
					(userId != wishList.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_WISHLIST_WHERE);

			query.append(_FINDER_COLUMN_G_U_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				List<WishList> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"WishListPersistenceImpl.fetchByG_U(long, long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					WishList wishList = list.get(0);

					result = wishList;

					cacheResult(wishList);

					if ((wishList.getGroupId() != groupId) ||
							(wishList.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
							finderArgs, wishList);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (WishList)result;
		}
	}

	/**
	 * Removes the wish list where groupId = &#63; and userId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the wish list that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList removeByG_U(long groupId, long userId)
		throws NoSuchWishListException, SystemException {
		WishList wishList = findByG_U(groupId, userId);

		return remove(wishList);
	}

	/**
	 * Returns the number of wish lists where groupId = &#63; and userId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the number of matching wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_U(long groupId, long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_G_U;

		Object[] finderArgs = new Object[] { groupId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_WISHLIST_WHERE);

			query.append(_FINDER_COLUMN_G_U_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_U_GROUPID_2 = "wishList.groupId = ? AND ";
	private static final String _FINDER_COLUMN_G_U_USERID_2 = "wishList.userId = ?";

	public WishListPersistenceImpl() {
		setModelClass(WishList.class);
	}

	/**
	 * Caches the wish list in the entity cache if it is enabled.
	 *
	 * @param wishList the wish list
	 */
	@Override
	public void cacheResult(WishList wishList) {
		EntityCacheUtil.putResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListImpl.class, wishList.getPrimaryKey(), wishList);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
			new Object[] { wishList.getCompanyId(), wishList.getUserId() },
			wishList);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
			new Object[] { wishList.getGroupId(), wishList.getUserId() },
			wishList);

		wishList.resetOriginalValues();
	}

	/**
	 * Caches the wish lists in the entity cache if it is enabled.
	 *
	 * @param wishLists the wish lists
	 */
	@Override
	public void cacheResult(List<WishList> wishLists) {
		for (WishList wishList : wishLists) {
			if (EntityCacheUtil.getResult(
						WishListModelImpl.ENTITY_CACHE_ENABLED,
						WishListImpl.class, wishList.getPrimaryKey()) == null) {
				cacheResult(wishList);
			}
			else {
				wishList.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all wish lists.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WishListImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WishListImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the wish list.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(WishList wishList) {
		EntityCacheUtil.removeResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListImpl.class, wishList.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(wishList);
	}

	@Override
	public void clearCache(List<WishList> wishLists) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (WishList wishList : wishLists) {
			EntityCacheUtil.removeResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
				WishListImpl.class, wishList.getPrimaryKey());

			clearUniqueFindersCache(wishList);
		}
	}

	protected void cacheUniqueFindersCache(WishList wishList) {
		if (wishList.isNew()) {
			Object[] args = new Object[] {
					wishList.getCompanyId(), wishList.getUserId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_U, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U, args, wishList);

			args = new Object[] { wishList.getGroupId(), wishList.getUserId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_G_U, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U, args, wishList);
		}
		else {
			WishListModelImpl wishListModelImpl = (WishListModelImpl)wishList;

			if ((wishListModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wishList.getCompanyId(), wishList.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_U, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U, args,
					wishList);
			}

			if ((wishListModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_G_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						wishList.getGroupId(), wishList.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_G_U, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U, args,
					wishList);
			}
		}
	}

	protected void clearUniqueFindersCache(WishList wishList) {
		WishListModelImpl wishListModelImpl = (WishListModelImpl)wishList;

		Object[] args = new Object[] {
				wishList.getCompanyId(), wishList.getUserId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U, args);

		if ((wishListModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_U.getColumnBitmask()) != 0) {
			args = new Object[] {
					wishListModelImpl.getOriginalCompanyId(),
					wishListModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U, args);
		}

		args = new Object[] { wishList.getGroupId(), wishList.getUserId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U, args);

		if ((wishListModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_G_U.getColumnBitmask()) != 0) {
			args = new Object[] {
					wishListModelImpl.getOriginalGroupId(),
					wishListModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U, args);
		}
	}

	/**
	 * Creates a new wish list with the primary key. Does not add the wish list to the database.
	 *
	 * @param wishListId the primary key for the new wish list
	 * @return the new wish list
	 */
	@Override
	public WishList create(long wishListId) {
		WishList wishList = new WishListImpl();

		wishList.setNew(true);
		wishList.setPrimaryKey(wishListId);

		return wishList;
	}

	/**
	 * Removes the wish list with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param wishListId the primary key of the wish list
	 * @return the wish list that was removed
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList remove(long wishListId)
		throws NoSuchWishListException, SystemException {
		return remove((Serializable)wishListId);
	}

	/**
	 * Removes the wish list with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the wish list
	 * @return the wish list that was removed
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList remove(Serializable primaryKey)
		throws NoSuchWishListException, SystemException {
		Session session = null;

		try {
			session = openSession();

			WishList wishList = (WishList)session.get(WishListImpl.class,
					primaryKey);

			if (wishList == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWishListException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(wishList);
		}
		catch (NoSuchWishListException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected WishList removeImpl(WishList wishList) throws SystemException {
		wishList = toUnwrappedModel(wishList);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(wishList)) {
				wishList = (WishList)session.get(WishListImpl.class,
						wishList.getPrimaryKeyObj());
			}

			if (wishList != null) {
				session.delete(wishList);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (wishList != null) {
			clearCache(wishList);
		}

		return wishList;
	}

	@Override
	public WishList updateImpl(com.esquare.ecommerce.model.WishList wishList)
		throws SystemException {
		wishList = toUnwrappedModel(wishList);

		boolean isNew = wishList.isNew();

		Session session = null;

		try {
			session = openSession();

			if (wishList.isNew()) {
				session.save(wishList);

				wishList.setNew(false);
			}
			else {
				session.merge(wishList);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WishListModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
			WishListImpl.class, wishList.getPrimaryKey(), wishList);

		clearUniqueFindersCache(wishList);
		cacheUniqueFindersCache(wishList);

		return wishList;
	}

	protected WishList toUnwrappedModel(WishList wishList) {
		if (wishList instanceof WishListImpl) {
			return wishList;
		}

		WishListImpl wishListImpl = new WishListImpl();

		wishListImpl.setNew(wishList.isNew());
		wishListImpl.setPrimaryKey(wishList.getPrimaryKey());

		wishListImpl.setWishListId(wishList.getWishListId());
		wishListImpl.setGroupId(wishList.getGroupId());
		wishListImpl.setCompanyId(wishList.getCompanyId());
		wishListImpl.setUserId(wishList.getUserId());
		wishListImpl.setUserName(wishList.getUserName());
		wishListImpl.setCreateDate(wishList.getCreateDate());
		wishListImpl.setModifiedDate(wishList.getModifiedDate());
		wishListImpl.setProductInventoryIds(wishList.getProductInventoryIds());

		return wishListImpl;
	}

	/**
	 * Returns the wish list with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the wish list
	 * @return the wish list
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList findByPrimaryKey(Serializable primaryKey)
		throws NoSuchWishListException, SystemException {
		WishList wishList = fetchByPrimaryKey(primaryKey);

		if (wishList == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchWishListException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return wishList;
	}

	/**
	 * Returns the wish list with the primary key or throws a {@link com.esquare.ecommerce.NoSuchWishListException} if it could not be found.
	 *
	 * @param wishListId the primary key of the wish list
	 * @return the wish list
	 * @throws com.esquare.ecommerce.NoSuchWishListException if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList findByPrimaryKey(long wishListId)
		throws NoSuchWishListException, SystemException {
		return findByPrimaryKey((Serializable)wishListId);
	}

	/**
	 * Returns the wish list with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the wish list
	 * @return the wish list, or <code>null</code> if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		WishList wishList = (WishList)EntityCacheUtil.getResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
				WishListImpl.class, primaryKey);

		if (wishList == _nullWishList) {
			return null;
		}

		if (wishList == null) {
			Session session = null;

			try {
				session = openSession();

				wishList = (WishList)session.get(WishListImpl.class, primaryKey);

				if (wishList != null) {
					cacheResult(wishList);
				}
				else {
					EntityCacheUtil.putResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
						WishListImpl.class, primaryKey, _nullWishList);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(WishListModelImpl.ENTITY_CACHE_ENABLED,
					WishListImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return wishList;
	}

	/**
	 * Returns the wish list with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param wishListId the primary key of the wish list
	 * @return the wish list, or <code>null</code> if a wish list with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public WishList fetchByPrimaryKey(long wishListId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)wishListId);
	}

	/**
	 * Returns all the wish lists.
	 *
	 * @return the wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WishList> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the wish lists.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.WishListModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of wish lists
	 * @param end the upper bound of the range of wish lists (not inclusive)
	 * @return the range of wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WishList> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the wish lists.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.WishListModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of wish lists
	 * @param end the upper bound of the range of wish lists (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<WishList> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<WishList> list = (List<WishList>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WISHLIST);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WISHLIST;

				if (pagination) {
					sql = sql.concat(WishListModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<WishList>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<WishList>(list);
				}
				else {
					list = (List<WishList>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the wish lists from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (WishList wishList : findAll()) {
			remove(wishList);
		}
	}

	/**
	 * Returns the number of wish lists.
	 *
	 * @return the number of wish lists
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WISHLIST);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the wish list persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.WishList")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<WishList>> listenersList = new ArrayList<ModelListener<WishList>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<WishList>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WishListImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_WISHLIST = "SELECT wishList FROM WishList wishList";
	private static final String _SQL_SELECT_WISHLIST_WHERE = "SELECT wishList FROM WishList wishList WHERE ";
	private static final String _SQL_COUNT_WISHLIST = "SELECT COUNT(wishList) FROM WishList wishList";
	private static final String _SQL_COUNT_WISHLIST_WHERE = "SELECT COUNT(wishList) FROM WishList wishList WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "wishList.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WishList exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No WishList exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WishListPersistenceImpl.class);
	private static WishList _nullWishList = new WishListImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<WishList> toCacheModel() {
				return _nullWishListCacheModel;
			}
		};

	private static CacheModel<WishList> _nullWishListCacheModel = new CacheModel<WishList>() {
			@Override
			public WishList toEntityModel() {
				return _nullWishList;
			}
		};
}