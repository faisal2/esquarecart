/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchOrderException;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.impl.OrderImpl;
import com.esquare.ecommerce.model.impl.OrderModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the order service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see OrderPersistence
 * @see OrderUtil
 * @generated
 */
public class OrderPersistenceImpl extends BasePersistenceImpl<Order>
	implements OrderPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OrderUtil} to access the order persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OrderImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			OrderModelImpl.GROUPID_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the orders where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByGroupId(long groupId) throws SystemException {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByGroupId(long groupId, int start, int end)
		throws SystemException {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByGroupId(long groupId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((groupId != order.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByGroupId_First(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByGroupId_First(groupId, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByGroupId_First(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByGroupId_Last(long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByGroupId_Last(groupId, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByGroupId_Last(long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where groupId = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByGroupId_PrevAndNext(long orderId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, order, groupId,
					orderByComparator, true);

			array[1] = order;

			array[2] = getByGroupId_PrevAndNext(session, order, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByGroupId_PrevAndNext(Session session, Order order,
		long groupId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByGroupId(long groupId) throws SystemException {
		for (Order order : findByGroupId(groupId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByGroupId(long groupId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "order_.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_S",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_S",
			new String[] { Long.class.getName(), String.class.getName() },
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.STATUS_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_S",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the orders where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_S(long companyId, String status)
		throws SystemException {
		return findByC_S(companyId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_S(long companyId, String status, int start,
		int end) throws SystemException {
		return findByC_S(companyId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_S(long companyId, String status, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S;
			finderArgs = new Object[] { companyId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_S;
			finderArgs = new Object[] {
					companyId, status,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						!Validator.equals(status, order.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_C_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_C_S_STATUS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindStatus) {
					qPos.add(status);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByC_S_First(long companyId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByC_S_First(companyId, status, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByC_S_First(long companyId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByC_S(companyId, status, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByC_S_Last(long companyId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByC_S_Last(companyId, status, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByC_S_Last(long companyId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_S(companyId, status);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByC_S(companyId, status, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and status = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByC_S_PrevAndNext(long orderId, long companyId,
		String status, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByC_S_PrevAndNext(session, order, companyId, status,
					orderByComparator, true);

			array[1] = order;

			array[2] = getByC_S_PrevAndNext(session, order, companyId, status,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByC_S_PrevAndNext(Session session, Order order,
		long companyId, String status, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

		boolean bindStatus = false;

		if (status == null) {
			query.append(_FINDER_COLUMN_C_S_STATUS_1);
		}
		else if (status.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_S_STATUS_3);
		}
		else {
			bindStatus = true;

			query.append(_FINDER_COLUMN_C_S_STATUS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindStatus) {
			qPos.add(status);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and status = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_S(long companyId, String status)
		throws SystemException {
		for (Order order : findByC_S(companyId, status, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param status the status
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_S(long companyId, String status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_S;

		Object[] finderArgs = new Object[] { companyId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_C_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_C_S_STATUS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindStatus) {
					qPos.add(status);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_S_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_S_STATUS_1 = "order_.status IS NULL";
	private static final String _FINDER_COLUMN_C_S_STATUS_2 = "order_.status = ?";
	private static final String _FINDER_COLUMN_C_S_STATUS_3 = "(order_.status IS NULL OR order_.status = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.USERID_COLUMN_BITMASK |
			OrderModelImpl.STATUS_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the orders where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_U_S(long companyId, long userId, String status)
		throws SystemException {
		return findByC_U_S(companyId, userId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_U_S(long companyId, long userId, String status,
		int start, int end) throws SystemException {
		return findByC_U_S(companyId, userId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByC_U_S(long companyId, long userId, String status,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U_S;
			finderArgs = new Object[] { companyId, userId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_U_S;
			finderArgs = new Object[] {
					companyId, userId, status,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						(userId != order.getUserId()) ||
						!Validator.equals(status, order.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_C_U_S_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_S_USERID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_C_U_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_U_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_C_U_S_STATUS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindStatus) {
					qPos.add(status);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByC_U_S_First(long companyId, long userId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByC_U_S_First(companyId, userId, status,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByC_U_S_First(long companyId, long userId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByC_U_S(companyId, userId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByC_U_S_Last(long companyId, long userId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByC_U_S_Last(companyId, userId, status,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByC_U_S_Last(long companyId, long userId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_U_S(companyId, userId, status);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByC_U_S(companyId, userId, status, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByC_U_S_PrevAndNext(long orderId, long companyId,
		long userId, String status, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByC_U_S_PrevAndNext(session, order, companyId,
					userId, status, orderByComparator, true);

			array[1] = order;

			array[2] = getByC_U_S_PrevAndNext(session, order, companyId,
					userId, status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByC_U_S_PrevAndNext(Session session, Order order,
		long companyId, long userId, String status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_C_U_S_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_U_S_USERID_2);

		boolean bindStatus = false;

		if (status == null) {
			query.append(_FINDER_COLUMN_C_U_S_STATUS_1);
		}
		else if (status.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_C_U_S_STATUS_3);
		}
		else {
			bindStatus = true;

			query.append(_FINDER_COLUMN_C_U_S_STATUS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(userId);

		if (bindStatus) {
			qPos.add(status);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and userId = &#63; and status = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_U_S(long companyId, long userId, String status)
		throws SystemException {
		for (Order order : findByC_U_S(companyId, userId, status,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param status the status
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_U_S(long companyId, long userId, String status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_U_S;

		Object[] finderArgs = new Object[] { companyId, userId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_C_U_S_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_S_USERID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_C_U_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_U_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_C_U_S_STATUS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindStatus) {
					qPos.add(status);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_U_S_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_U_S_USERID_2 = "order_.userId = ? AND ";
	private static final String _FINDER_COLUMN_C_U_S_STATUS_1 = "order_.status IS NULL";
	private static final String _FINDER_COLUMN_C_U_S_STATUS_2 = "order_.status = ?";
	private static final String _FINDER_COLUMN_C_U_S_STATUS_3 = "(order_.status IS NULL OR order_.status = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_NUMBER = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByNumber",
			new String[] { String.class.getName() },
			OrderModelImpl.NUMBER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_NUMBER = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNumber",
			new String[] { String.class.getName() });

	/**
	 * Returns the order where number = &#63; or throws a {@link com.esquare.ecommerce.NoSuchOrderException} if it could not be found.
	 *
	 * @param number the number
	 * @return the matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByNumber(String number)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByNumber(number);

		if (order == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("number=");
			msg.append(number);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchOrderException(msg.toString());
		}

		return order;
	}

	/**
	 * Returns the order where number = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param number the number
	 * @return the matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByNumber(String number) throws SystemException {
		return fetchByNumber(number, true);
	}

	/**
	 * Returns the order where number = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param number the number
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByNumber(String number, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { number };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_NUMBER,
					finderArgs, this);
		}

		if (result instanceof Order) {
			Order order = (Order)result;

			if (!Validator.equals(number, order.getNumber())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_ORDER__WHERE);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_NUMBER_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NUMBER_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_NUMBER_NUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNumber) {
					qPos.add(number);
				}

				List<Order> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
						finderArgs, list);
				}
				else {
					Order order = list.get(0);

					result = order;

					cacheResult(order);

					if ((order.getNumber() == null) ||
							!order.getNumber().equals(number)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
							finderArgs, order);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NUMBER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Order)result;
		}
	}

	/**
	 * Removes the order where number = &#63; from the database.
	 *
	 * @param number the number
	 * @return the order that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order removeByNumber(String number)
		throws NoSuchOrderException, SystemException {
		Order order = findByNumber(number);

		return remove(order);
	}

	/**
	 * Returns the number of orders where number = &#63;.
	 *
	 * @param number the number
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByNumber(String number) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_NUMBER;

		Object[] finderArgs = new Object[] { number };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ORDER__WHERE);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_NUMBER_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_NUMBER_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_NUMBER_NUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNumber) {
					qPos.add(number);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NUMBER_NUMBER_1 = "order_.number IS NULL";
	private static final String _FINDER_COLUMN_NUMBER_NUMBER_2 = "order_.number = ?";
	private static final String _FINDER_COLUMN_NUMBER_NUMBER_3 = "(order_.number IS NULL OR order_.number = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_G_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByG_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByG_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			OrderModelImpl.GROUPID_COLUMN_BITMASK |
			OrderModelImpl.USERID_COLUMN_BITMASK |
			OrderModelImpl.STATUS_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_G_U_S = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_U_S",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the orders where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByG_U_S(long groupId, long userId, String status)
		throws SystemException {
		return findByG_U_S(groupId, userId, status, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByG_U_S(long groupId, long userId, String status,
		int start, int end) throws SystemException {
		return findByG_U_S(groupId, userId, status, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByG_U_S(long groupId, long userId, String status,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_U_S;
			finderArgs = new Object[] { groupId, userId, status };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_G_U_S;
			finderArgs = new Object[] {
					groupId, userId, status,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((groupId != order.getGroupId()) ||
						(userId != order.getUserId()) ||
						!Validator.equals(status, order.getStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_G_U_S_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_S_USERID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_G_U_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_U_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_G_U_S_STATUS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				if (bindStatus) {
					qPos.add(status);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByG_U_S_First(long groupId, long userId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByG_U_S_First(groupId, userId, status,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByG_U_S_First(long groupId, long userId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByG_U_S(groupId, userId, status, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByG_U_S_Last(long groupId, long userId, String status,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByG_U_S_Last(groupId, userId, status,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", status=");
		msg.append(status);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByG_U_S_Last(long groupId, long userId, String status,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByG_U_S(groupId, userId, status);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByG_U_S(groupId, userId, status, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByG_U_S_PrevAndNext(long orderId, long groupId,
		long userId, String status, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByG_U_S_PrevAndNext(session, order, groupId, userId,
					status, orderByComparator, true);

			array[1] = order;

			array[2] = getByG_U_S_PrevAndNext(session, order, groupId, userId,
					status, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByG_U_S_PrevAndNext(Session session, Order order,
		long groupId, long userId, String status,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_G_U_S_GROUPID_2);

		query.append(_FINDER_COLUMN_G_U_S_USERID_2);

		boolean bindStatus = false;

		if (status == null) {
			query.append(_FINDER_COLUMN_G_U_S_STATUS_1);
		}
		else if (status.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_G_U_S_STATUS_3);
		}
		else {
			bindStatus = true;

			query.append(_FINDER_COLUMN_G_U_S_STATUS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		qPos.add(userId);

		if (bindStatus) {
			qPos.add(status);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where groupId = &#63; and userId = &#63; and status = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByG_U_S(long groupId, long userId, String status)
		throws SystemException {
		for (Order order : findByG_U_S(groupId, userId, status,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where groupId = &#63; and userId = &#63; and status = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param status the status
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_U_S(long groupId, long userId, String status)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_G_U_S;

		Object[] finderArgs = new Object[] { groupId, userId, status };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_G_U_S_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_S_USERID_2);

			boolean bindStatus = false;

			if (status == null) {
				query.append(_FINDER_COLUMN_G_U_S_STATUS_1);
			}
			else if (status.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_G_U_S_STATUS_3);
			}
			else {
				bindStatus = true;

				query.append(_FINDER_COLUMN_G_U_S_STATUS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				if (bindStatus) {
					qPos.add(status);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_U_S_GROUPID_2 = "order_.groupId = ? AND ";
	private static final String _FINDER_COLUMN_G_U_S_USERID_2 = "order_.userId = ? AND ";
	private static final String _FINDER_COLUMN_G_U_S_STATUS_1 = "order_.status IS NULL";
	private static final String _FINDER_COLUMN_G_U_S_STATUS_2 = "order_.status = ?";
	private static final String _FINDER_COLUMN_G_U_S_STATUS_3 = "(order_.status IS NULL OR order_.status = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COUPONCODEVERSION =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCouponCodeVersion",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Double.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODEVERSION =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCouponCodeVersion",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Double.class.getName()
			},
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.COUPONCODES_COLUMN_BITMASK |
			OrderModelImpl.COUPONVERSION_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUPONCODEVERSION = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCouponCodeVersion",
			new String[] {
				Long.class.getName(), String.class.getName(),
				Double.class.getName()
			});

	/**
	 * Returns all the orders where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCodeVersion(long companyId,
		String couponCodes, double couponVersion) throws SystemException {
		return findByCouponCodeVersion(companyId, couponCodes, couponVersion,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCodeVersion(long companyId,
		String couponCodes, double couponVersion, int start, int end)
		throws SystemException {
		return findByCouponCodeVersion(companyId, couponCodes, couponVersion,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCodeVersion(long companyId,
		String couponCodes, double couponVersion, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODEVERSION;
			finderArgs = new Object[] { companyId, couponCodes, couponVersion };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COUPONCODEVERSION;
			finderArgs = new Object[] {
					companyId, couponCodes, couponVersion,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						!Validator.equals(couponCodes, order.getCouponCodes()) ||
						(couponVersion != order.getCouponVersion())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COMPANYID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONVERSION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				qPos.add(couponVersion);

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByCouponCodeVersion_First(long companyId,
		String couponCodes, double couponVersion,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByCouponCodeVersion_First(companyId, couponCodes,
				couponVersion, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(", couponVersion=");
		msg.append(couponVersion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByCouponCodeVersion_First(long companyId,
		String couponCodes, double couponVersion,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByCouponCodeVersion(companyId, couponCodes,
				couponVersion, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByCouponCodeVersion_Last(long companyId,
		String couponCodes, double couponVersion,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByCouponCodeVersion_Last(companyId, couponCodes,
				couponVersion, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(", couponVersion=");
		msg.append(couponVersion);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByCouponCodeVersion_Last(long companyId,
		String couponCodes, double couponVersion,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCouponCodeVersion(companyId, couponCodes,
				couponVersion);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByCouponCodeVersion(companyId, couponCodes,
				couponVersion, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByCouponCodeVersion_PrevAndNext(long orderId,
		long companyId, String couponCodes, double couponVersion,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByCouponCodeVersion_PrevAndNext(session, order,
					companyId, couponCodes, couponVersion, orderByComparator,
					true);

			array[1] = order;

			array[2] = getByCouponCodeVersion_PrevAndNext(session, order,
					companyId, couponCodes, couponVersion, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByCouponCodeVersion_PrevAndNext(Session session,
		Order order, long companyId, String couponCodes, double couponVersion,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_COUPONCODEVERSION_COMPANYID_2);

		boolean bindCouponCodes = false;

		if (couponCodes == null) {
			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_1);
		}
		else if (couponCodes.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_3);
		}
		else {
			bindCouponCodes = true;

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_2);
		}

		query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONVERSION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindCouponCodes) {
			qPos.add(couponCodes);
		}

		qPos.add(couponVersion);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCouponCodeVersion(long companyId, String couponCodes,
		double couponVersion) throws SystemException {
		for (Order order : findByCouponCodeVersion(companyId, couponCodes,
				couponVersion, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and couponCodes = &#63; and couponVersion = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param couponVersion the coupon version
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCouponCodeVersion(long companyId, String couponCodes,
		double couponVersion) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COUPONCODEVERSION;

		Object[] finderArgs = new Object[] { companyId, couponCodes, couponVersion };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COMPANYID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONVERSION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				qPos.add(couponVersion);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_1 = "order_.couponCodes IS NULL AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_2 = "order_.couponCodes = ? AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODES_3 = "(order_.couponCodes IS NULL OR order_.couponCodes = '') AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONVERSION_2 =
		"order_.couponVersion = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COUPONCODE =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCouponCode",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODE =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCouponCode",
			new String[] { Long.class.getName(), String.class.getName() },
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.COUPONCODES_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUPONCODE = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCouponCode",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the orders where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCode(long companyId, String couponCodes)
		throws SystemException {
		return findByCouponCode(companyId, couponCodes, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and couponCodes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCode(long companyId, String couponCodes,
		int start, int end) throws SystemException {
		return findByCouponCode(companyId, couponCodes, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and couponCodes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByCouponCode(long companyId, String couponCodes,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODE;
			finderArgs = new Object[] { companyId, couponCodes };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COUPONCODE;
			finderArgs = new Object[] {
					companyId, couponCodes,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						!Validator.equals(couponCodes, order.getCouponCodes())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_COUPONCODE_COMPANYID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByCouponCode_First(long companyId, String couponCodes,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByCouponCode_First(companyId, couponCodes,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByCouponCode_First(long companyId, String couponCodes,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByCouponCode(companyId, couponCodes, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByCouponCode_Last(long companyId, String couponCodes,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByCouponCode_Last(companyId, couponCodes,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByCouponCode_Last(long companyId, String couponCodes,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCouponCode(companyId, couponCodes);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByCouponCode(companyId, couponCodes, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByCouponCode_PrevAndNext(long orderId, long companyId,
		String couponCodes, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByCouponCode_PrevAndNext(session, order, companyId,
					couponCodes, orderByComparator, true);

			array[1] = order;

			array[2] = getByCouponCode_PrevAndNext(session, order, companyId,
					couponCodes, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByCouponCode_PrevAndNext(Session session, Order order,
		long companyId, String couponCodes,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_COUPONCODE_COMPANYID_2);

		boolean bindCouponCodes = false;

		if (couponCodes == null) {
			query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_1);
		}
		else if (couponCodes.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_3);
		}
		else {
			bindCouponCodes = true;

			query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindCouponCodes) {
			qPos.add(couponCodes);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and couponCodes = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCouponCode(long companyId, String couponCodes)
		throws SystemException {
		for (Order order : findByCouponCode(companyId, couponCodes,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param couponCodes the coupon codes
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCouponCode(long companyId, String couponCodes)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COUPONCODE;

		Object[] finderArgs = new Object[] { companyId, couponCodes };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_COUPONCODE_COMPANYID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODES_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COUPONCODE_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODES_1 = "order_.couponCodes IS NULL";
	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODES_2 = "order_.couponCodes = ?";
	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODES_3 = "(order_.couponCodes IS NULL OR order_.couponCodes = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERCOUPONCODE =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserCouponCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERCOUPONCODE =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserCouponCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.USERID_COLUMN_BITMASK |
			OrderModelImpl.COUPONCODES_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERCOUPONCODE = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserCouponCode",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the orders where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByUserCouponCode(long companyId, long userId,
		String couponCodes) throws SystemException {
		return findByUserCouponCode(companyId, userId, couponCodes,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByUserCouponCode(long companyId, long userId,
		String couponCodes, int start, int end) throws SystemException {
		return findByUserCouponCode(companyId, userId, couponCodes, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByUserCouponCode(long companyId, long userId,
		String couponCodes, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERCOUPONCODE;
			finderArgs = new Object[] { companyId, userId, couponCodes };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERCOUPONCODE;
			finderArgs = new Object[] {
					companyId, userId, couponCodes,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						(userId != order.getUserId()) ||
						!Validator.equals(couponCodes, order.getCouponCodes())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_USERCOUPONCODE_COMPANYID_2);

			query.append(_FINDER_COLUMN_USERCOUPONCODE_USERID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByUserCouponCode_First(long companyId, long userId,
		String couponCodes, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByUserCouponCode_First(companyId, userId,
				couponCodes, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByUserCouponCode_First(long companyId, long userId,
		String couponCodes, OrderByComparator orderByComparator)
		throws SystemException {
		List<Order> list = findByUserCouponCode(companyId, userId, couponCodes,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByUserCouponCode_Last(long companyId, long userId,
		String couponCodes, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByUserCouponCode_Last(companyId, userId,
				couponCodes, orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", couponCodes=");
		msg.append(couponCodes);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByUserCouponCode_Last(long companyId, long userId,
		String couponCodes, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByUserCouponCode(companyId, userId, couponCodes);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByUserCouponCode(companyId, userId, couponCodes,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByUserCouponCode_PrevAndNext(long orderId,
		long companyId, long userId, String couponCodes,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByUserCouponCode_PrevAndNext(session, order,
					companyId, userId, couponCodes, orderByComparator, true);

			array[1] = order;

			array[2] = getByUserCouponCode_PrevAndNext(session, order,
					companyId, userId, couponCodes, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByUserCouponCode_PrevAndNext(Session session,
		Order order, long companyId, long userId, String couponCodes,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_USERCOUPONCODE_COMPANYID_2);

		query.append(_FINDER_COLUMN_USERCOUPONCODE_USERID_2);

		boolean bindCouponCodes = false;

		if (couponCodes == null) {
			query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_1);
		}
		else if (couponCodes.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_3);
		}
		else {
			bindCouponCodes = true;

			query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(userId);

		if (bindCouponCodes) {
			qPos.add(couponCodes);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and userId = &#63; and couponCodes = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserCouponCode(long companyId, long userId,
		String couponCodes) throws SystemException {
		for (Order order : findByUserCouponCode(companyId, userId, couponCodes,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and userId = &#63; and couponCodes = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param couponCodes the coupon codes
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserCouponCode(long companyId, long userId,
		String couponCodes) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERCOUPONCODE;

		Object[] finderArgs = new Object[] { companyId, userId, couponCodes };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_USERCOUPONCODE_COMPANYID_2);

			query.append(_FINDER_COLUMN_USERCOUPONCODE_USERID_2);

			boolean bindCouponCodes = false;

			if (couponCodes == null) {
				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_1);
			}
			else if (couponCodes.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_3);
			}
			else {
				bindCouponCodes = true;

				query.append(_FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindCouponCodes) {
					qPos.add(couponCodes);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERCOUPONCODE_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_USERCOUPONCODE_USERID_2 = "order_.userId = ? AND ";
	private static final String _FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_1 = "order_.couponCodes IS NULL";
	private static final String _FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_2 = "order_.couponCodes = ?";
	private static final String _FINDER_COLUMN_USERCOUPONCODE_COUPONCODES_3 = "(order_.couponCodes IS NULL OR order_.couponCodes = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERNUMBER =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrderNumber",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBER =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrderNumber",
			new String[] { Long.class.getName(), String.class.getName() },
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.NUMBER_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDERNUMBER = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrderNumber",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the orders where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumber(long companyId, String number)
		throws SystemException {
		return findByOrderNumber(companyId, number, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and number = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumber(long companyId, String number,
		int start, int end) throws SystemException {
		return findByOrderNumber(companyId, number, start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and number = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumber(long companyId, String number,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBER;
			finderArgs = new Object[] { companyId, number };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERNUMBER;
			finderArgs = new Object[] {
					companyId, number,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						!Validator.equals(number, order.getNumber())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_ORDERNUMBER_COMPANYID_2);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindNumber) {
					qPos.add(number);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByOrderNumber_First(long companyId, String number,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByOrderNumber_First(companyId, number,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", number=");
		msg.append(number);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByOrderNumber_First(long companyId, String number,
		OrderByComparator orderByComparator) throws SystemException {
		List<Order> list = findByOrderNumber(companyId, number, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByOrderNumber_Last(long companyId, String number,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByOrderNumber_Last(companyId, number,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", number=");
		msg.append(number);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByOrderNumber_Last(long companyId, String number,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrderNumber(companyId, number);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByOrderNumber(companyId, number, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and number = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByOrderNumber_PrevAndNext(long orderId, long companyId,
		String number, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByOrderNumber_PrevAndNext(session, order, companyId,
					number, orderByComparator, true);

			array[1] = order;

			array[2] = getByOrderNumber_PrevAndNext(session, order, companyId,
					number, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByOrderNumber_PrevAndNext(Session session, Order order,
		long companyId, String number, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_ORDERNUMBER_COMPANYID_2);

		boolean bindNumber = false;

		if (number == null) {
			query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_1);
		}
		else if (number.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_3);
		}
		else {
			bindNumber = true;

			query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindNumber) {
			qPos.add(number);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and number = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrderNumber(long companyId, String number)
		throws SystemException {
		for (Order order : findByOrderNumber(companyId, number,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param number the number
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrderNumber(long companyId, String number)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDERNUMBER;

		Object[] finderArgs = new Object[] { companyId, number };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_ORDERNUMBER_COMPANYID_2);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_ORDERNUMBER_NUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindNumber) {
					qPos.add(number);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDERNUMBER_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_ORDERNUMBER_NUMBER_1 = "order_.number IS NULL";
	private static final String _FINDER_COLUMN_ORDERNUMBER_NUMBER_2 = "order_.number = ?";
	private static final String _FINDER_COLUMN_ORDERNUMBER_NUMBER_3 = "(order_.number IS NULL OR order_.number = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERNUMBERUSERID =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrderNumberUserId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBERUSERID =
		new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, OrderImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByOrderNumberUserId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			},
			OrderModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderModelImpl.USERID_COLUMN_BITMASK |
			OrderModelImpl.NUMBER_COLUMN_BITMASK |
			OrderModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDERNUMBERUSERID = new FinderPath(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByOrderNumberUserId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				String.class.getName()
			});

	/**
	 * Returns all the orders where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @return the matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumberUserId(long companyId, long userId,
		String number) throws SystemException {
		return findByOrderNumberUserId(companyId, userId, number,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumberUserId(long companyId, long userId,
		String number, int start, int end) throws SystemException {
		return findByOrderNumberUserId(companyId, userId, number, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the orders where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findByOrderNumberUserId(long companyId, long userId,
		String number, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBERUSERID;
			finderArgs = new Object[] { companyId, userId, number };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERNUMBERUSERID;
			finderArgs = new Object[] {
					companyId, userId, number,
					
					start, end, orderByComparator
				};
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Order order : list) {
				if ((companyId != order.getCompanyId()) ||
						(userId != order.getUserId()) ||
						!Validator.equals(number, order.getNumber())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_COMPANYID_2);

			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_USERID_2);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindNumber) {
					qPos.add(number);
				}

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByOrderNumberUserId_First(long companyId, long userId,
		String number, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByOrderNumberUserId_First(companyId, userId, number,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", number=");
		msg.append(number);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the first order in the ordered set where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByOrderNumberUserId_First(long companyId, long userId,
		String number, OrderByComparator orderByComparator)
		throws SystemException {
		List<Order> list = findByOrderNumberUserId(companyId, userId, number,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByOrderNumberUserId_Last(long companyId, long userId,
		String number, OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByOrderNumberUserId_Last(companyId, userId, number,
				orderByComparator);

		if (order != null) {
			return order;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(", number=");
		msg.append(number);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderException(msg.toString());
	}

	/**
	 * Returns the last order in the ordered set where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order, or <code>null</code> if a matching order could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByOrderNumberUserId_Last(long companyId, long userId,
		String number, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByOrderNumberUserId(companyId, userId, number);

		if (count == 0) {
			return null;
		}

		List<Order> list = findByOrderNumberUserId(companyId, userId, number,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the orders before and after the current order in the ordered set where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param orderId the primary key of the current order
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order[] findByOrderNumberUserId_PrevAndNext(long orderId,
		long companyId, long userId, String number,
		OrderByComparator orderByComparator)
		throws NoSuchOrderException, SystemException {
		Order order = findByPrimaryKey(orderId);

		Session session = null;

		try {
			session = openSession();

			Order[] array = new OrderImpl[3];

			array[0] = getByOrderNumberUserId_PrevAndNext(session, order,
					companyId, userId, number, orderByComparator, true);

			array[1] = order;

			array[2] = getByOrderNumberUserId_PrevAndNext(session, order,
					companyId, userId, number, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Order getByOrderNumberUserId_PrevAndNext(Session session,
		Order order, long companyId, long userId, String number,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDER__WHERE);

		query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_COMPANYID_2);

		query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_USERID_2);

		boolean bindNumber = false;

		if (number == null) {
			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_1);
		}
		else if (number.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_3);
		}
		else {
			bindNumber = true;

			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(userId);

		if (bindNumber) {
			qPos.add(number);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(order);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Order> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the orders where companyId = &#63; and userId = &#63; and number = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrderNumberUserId(long companyId, long userId,
		String number) throws SystemException {
		for (Order order : findByOrderNumberUserId(companyId, userId, number,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders where companyId = &#63; and userId = &#63; and number = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param number the number
	 * @return the number of matching orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrderNumberUserId(long companyId, long userId,
		String number) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDERNUMBERUSERID;

		Object[] finderArgs = new Object[] { companyId, userId, number };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_ORDER__WHERE);

			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_COMPANYID_2);

			query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_USERID_2);

			boolean bindNumber = false;

			if (number == null) {
				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_1);
			}
			else if (number.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_3);
			}
			else {
				bindNumber = true;

				query.append(_FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (bindNumber) {
					qPos.add(number);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDERNUMBERUSERID_COMPANYID_2 = "order_.companyId = ? AND ";
	private static final String _FINDER_COLUMN_ORDERNUMBERUSERID_USERID_2 = "order_.userId = ? AND ";
	private static final String _FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_1 = "order_.number IS NULL";
	private static final String _FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_2 = "order_.number = ?";
	private static final String _FINDER_COLUMN_ORDERNUMBERUSERID_NUMBER_3 = "(order_.number IS NULL OR order_.number = '')";

	public OrderPersistenceImpl() {
		setModelClass(Order.class);
	}

	/**
	 * Caches the order in the entity cache if it is enabled.
	 *
	 * @param order the order
	 */
	@Override
	public void cacheResult(Order order) {
		EntityCacheUtil.putResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderImpl.class, order.getPrimaryKey(), order);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER,
			new Object[] { order.getNumber() }, order);

		order.resetOriginalValues();
	}

	/**
	 * Caches the orders in the entity cache if it is enabled.
	 *
	 * @param orders the orders
	 */
	@Override
	public void cacheResult(List<Order> orders) {
		for (Order order : orders) {
			if (EntityCacheUtil.getResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
						OrderImpl.class, order.getPrimaryKey()) == null) {
				cacheResult(order);
			}
			else {
				order.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all orders.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OrderImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OrderImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the order.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Order order) {
		EntityCacheUtil.removeResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderImpl.class, order.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(order);
	}

	@Override
	public void clearCache(List<Order> orders) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Order order : orders) {
			EntityCacheUtil.removeResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
				OrderImpl.class, order.getPrimaryKey());

			clearUniqueFindersCache(order);
		}
	}

	protected void cacheUniqueFindersCache(Order order) {
		if (order.isNew()) {
			Object[] args = new Object[] { order.getNumber() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NUMBER, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER, args, order);
		}
		else {
			OrderModelImpl orderModelImpl = (OrderModelImpl)order;

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_NUMBER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { order.getNumber() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NUMBER, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NUMBER, args,
					order);
			}
		}
	}

	protected void clearUniqueFindersCache(Order order) {
		OrderModelImpl orderModelImpl = (OrderModelImpl)order;

		Object[] args = new Object[] { order.getNumber() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NUMBER, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NUMBER, args);

		if ((orderModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_NUMBER.getColumnBitmask()) != 0) {
			args = new Object[] { orderModelImpl.getOriginalNumber() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NUMBER, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NUMBER, args);
		}
	}

	/**
	 * Creates a new order with the primary key. Does not add the order to the database.
	 *
	 * @param orderId the primary key for the new order
	 * @return the new order
	 */
	@Override
	public Order create(long orderId) {
		Order order = new OrderImpl();

		order.setNew(true);
		order.setPrimaryKey(orderId);

		return order;
	}

	/**
	 * Removes the order with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param orderId the primary key of the order
	 * @return the order that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order remove(long orderId)
		throws NoSuchOrderException, SystemException {
		return remove((Serializable)orderId);
	}

	/**
	 * Removes the order with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the order
	 * @return the order that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order remove(Serializable primaryKey)
		throws NoSuchOrderException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Order order = (Order)session.get(OrderImpl.class, primaryKey);

			if (order == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOrderException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(order);
		}
		catch (NoSuchOrderException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Order removeImpl(Order order) throws SystemException {
		order = toUnwrappedModel(order);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(order)) {
				order = (Order)session.get(OrderImpl.class,
						order.getPrimaryKeyObj());
			}

			if (order != null) {
				session.delete(order);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (order != null) {
			clearCache(order);
		}

		return order;
	}

	@Override
	public Order updateImpl(com.esquare.ecommerce.model.Order order)
		throws SystemException {
		order = toUnwrappedModel(order);

		boolean isNew = order.isNew();

		OrderModelImpl orderModelImpl = (OrderModelImpl)order;

		Session session = null;

		try {
			session = openSession();

			if (order.isNew()) {
				session.save(order);

				order.setNew(false);
			}
			else {
				session.merge(order);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OrderModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { orderModelImpl.getOriginalGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { orderModelImpl.getGroupId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalUserId(),
						orderModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U_S,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getUserId(), orderModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U_S,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_U_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalGroupId(),
						orderModelImpl.getOriginalUserId(),
						orderModelImpl.getOriginalStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_U_S,
					args);

				args = new Object[] {
						orderModelImpl.getGroupId(), orderModelImpl.getUserId(),
						orderModelImpl.getStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_G_U_S,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODEVERSION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalCouponCodes(),
						orderModelImpl.getOriginalCouponVersion()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODEVERSION,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getCouponCodes(),
						orderModelImpl.getCouponVersion()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODEVERSION,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalCouponCodes()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODE,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getCouponCodes()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUPONCODE,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERCOUPONCODE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalUserId(),
						orderModelImpl.getOriginalCouponCodes()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERCOUPONCODE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERCOUPONCODE,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getUserId(),
						orderModelImpl.getCouponCodes()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERCOUPONCODE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERCOUPONCODE,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERNUMBER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBER,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERNUMBER,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBER,
					args);
			}

			if ((orderModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBERUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderModelImpl.getOriginalCompanyId(),
						orderModelImpl.getOriginalUserId(),
						orderModelImpl.getOriginalNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERNUMBERUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBERUSERID,
					args);

				args = new Object[] {
						orderModelImpl.getCompanyId(),
						orderModelImpl.getUserId(), orderModelImpl.getNumber()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERNUMBERUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERNUMBERUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
			OrderImpl.class, order.getPrimaryKey(), order);

		clearUniqueFindersCache(order);
		cacheUniqueFindersCache(order);

		return order;
	}

	protected Order toUnwrappedModel(Order order) {
		if (order instanceof OrderImpl) {
			return order;
		}

		OrderImpl orderImpl = new OrderImpl();

		orderImpl.setNew(order.isNew());
		orderImpl.setPrimaryKey(order.getPrimaryKey());

		orderImpl.setOrderId(order.getOrderId());
		orderImpl.setGroupId(order.getGroupId());
		orderImpl.setCompanyId(order.getCompanyId());
		orderImpl.setUserId(order.getUserId());
		orderImpl.setUserName(order.getUserName());
		orderImpl.setCreateDate(order.getCreateDate());
		orderImpl.setModifiedDate(order.getModifiedDate());
		orderImpl.setNumber(order.getNumber());
		orderImpl.setTax(order.getTax());
		orderImpl.setShipping(order.getShipping());
		orderImpl.setAltShipping(order.getAltShipping());
		orderImpl.setRequiresShipping(order.isRequiresShipping());
		orderImpl.setInsure(order.isInsure());
		orderImpl.setInsurance(order.getInsurance());
		orderImpl.setCouponCodes(order.getCouponCodes());
		orderImpl.setCouponVersion(order.getCouponVersion());
		orderImpl.setCouponDiscount(order.getCouponDiscount());
		orderImpl.setWalletAmount(order.getWalletAmount());
		orderImpl.setBillingFirstName(order.getBillingFirstName());
		orderImpl.setBillingLastName(order.getBillingLastName());
		orderImpl.setBillingEmailAddress(order.getBillingEmailAddress());
		orderImpl.setBillingCompany(order.getBillingCompany());
		orderImpl.setBillingStreet(order.getBillingStreet());
		orderImpl.setBillingCity(order.getBillingCity());
		orderImpl.setBillingState(order.getBillingState());
		orderImpl.setBillingZip(order.getBillingZip());
		orderImpl.setBillingCountry(order.getBillingCountry());
		orderImpl.setBillingPhone(order.getBillingPhone());
		orderImpl.setShipToBilling(order.isShipToBilling());
		orderImpl.setShippingFirstName(order.getShippingFirstName());
		orderImpl.setShippingLastName(order.getShippingLastName());
		orderImpl.setShippingEmailAddress(order.getShippingEmailAddress());
		orderImpl.setShippingCompany(order.getShippingCompany());
		orderImpl.setShippingStreet(order.getShippingStreet());
		orderImpl.setShippingCity(order.getShippingCity());
		orderImpl.setShippingState(order.getShippingState());
		orderImpl.setShippingZip(order.getShippingZip());
		orderImpl.setShippingCountry(order.getShippingCountry());
		orderImpl.setShippingPhone(order.getShippingPhone());
		orderImpl.setComments(order.getComments());
		orderImpl.setSendOrderEmail(order.isSendOrderEmail());
		orderImpl.setPaymentType(order.getPaymentType());
		orderImpl.setPaymentStatus(order.getPaymentStatus());
		orderImpl.setStatus(order.getStatus());
		orderImpl.setHistory(order.getHistory());

		return orderImpl;
	}

	/**
	 * Returns the order with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the order
	 * @return the order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOrderException, SystemException {
		Order order = fetchByPrimaryKey(primaryKey);

		if (order == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOrderException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return order;
	}

	/**
	 * Returns the order with the primary key or throws a {@link com.esquare.ecommerce.NoSuchOrderException} if it could not be found.
	 *
	 * @param orderId the primary key of the order
	 * @return the order
	 * @throws com.esquare.ecommerce.NoSuchOrderException if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order findByPrimaryKey(long orderId)
		throws NoSuchOrderException, SystemException {
		return findByPrimaryKey((Serializable)orderId);
	}

	/**
	 * Returns the order with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the order
	 * @return the order, or <code>null</code> if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Order order = (Order)EntityCacheUtil.getResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
				OrderImpl.class, primaryKey);

		if (order == _nullOrder) {
			return null;
		}

		if (order == null) {
			Session session = null;

			try {
				session = openSession();

				order = (Order)session.get(OrderImpl.class, primaryKey);

				if (order != null) {
					cacheResult(order);
				}
				else {
					EntityCacheUtil.putResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
						OrderImpl.class, primaryKey, _nullOrder);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OrderModelImpl.ENTITY_CACHE_ENABLED,
					OrderImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return order;
	}

	/**
	 * Returns the order with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param orderId the primary key of the order
	 * @return the order, or <code>null</code> if a order with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Order fetchByPrimaryKey(long orderId) throws SystemException {
		return fetchByPrimaryKey((Serializable)orderId);
	}

	/**
	 * Returns all the orders.
	 *
	 * @return the orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the orders.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @return the range of orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the orders.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of orders
	 * @param end the upper bound of the range of orders (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Order> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Order> list = (List<Order>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ORDER_);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ORDER_;

				if (pagination) {
					sql = sql.concat(OrderModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Order>(list);
				}
				else {
					list = (List<Order>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the orders from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Order order : findAll()) {
			remove(order);
		}
	}

	/**
	 * Returns the number of orders.
	 *
	 * @return the number of orders
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ORDER_);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the order persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.Order")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Order>> listenersList = new ArrayList<ModelListener<Order>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Order>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OrderImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ORDER_ = "SELECT order_ FROM Order order_";
	private static final String _SQL_SELECT_ORDER__WHERE = "SELECT order_ FROM Order order_ WHERE ";
	private static final String _SQL_COUNT_ORDER_ = "SELECT COUNT(order_) FROM Order order_";
	private static final String _SQL_COUNT_ORDER__WHERE = "SELECT COUNT(order_) FROM Order order_ WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "order_.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Order exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Order exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OrderPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"number"
			});
	private static Order _nullOrder = new OrderImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Order> toCacheModel() {
				return _nullOrderCacheModel;
			}
		};

	private static CacheModel<Order> _nullOrderCacheModel = new CacheModel<Order>() {
			@Override
			public Order toEntityModel() {
				return _nullOrder;
			}
		};
}