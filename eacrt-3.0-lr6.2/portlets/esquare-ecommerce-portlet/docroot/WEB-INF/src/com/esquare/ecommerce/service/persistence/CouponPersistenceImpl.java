/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchCouponException;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.impl.CouponImpl;
import com.esquare.ecommerce.model.impl.CouponModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the coupon service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see CouponPersistence
 * @see CouponUtil
 * @generated
 */
public class CouponPersistenceImpl extends BasePersistenceImpl<Coupon>
	implements CouponPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CouponUtil} to access the coupon persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CouponImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, CouponImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, CouponImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_COUPONCODE = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, CouponImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCouponCode",
			new String[] { String.class.getName(), Boolean.class.getName() },
			CouponModelImpl.COUPONCODE_COLUMN_BITMASK |
			CouponModelImpl.ARCHIVED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUPONCODE = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCouponCode",
			new String[] { String.class.getName(), Boolean.class.getName() });

	/**
	 * Returns the coupon where couponCode = &#63; and archived = &#63; or throws a {@link com.esquare.ecommerce.NoSuchCouponException} if it could not be found.
	 *
	 * @param couponCode the coupon code
	 * @param archived the archived
	 * @return the matching coupon
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon findByCouponCode(String couponCode, boolean archived)
		throws NoSuchCouponException, SystemException {
		Coupon coupon = fetchByCouponCode(couponCode, archived);

		if (coupon == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("couponCode=");
			msg.append(couponCode);

			msg.append(", archived=");
			msg.append(archived);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCouponException(msg.toString());
		}

		return coupon;
	}

	/**
	 * Returns the coupon where couponCode = &#63; and archived = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param couponCode the coupon code
	 * @param archived the archived
	 * @return the matching coupon, or <code>null</code> if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByCouponCode(String couponCode, boolean archived)
		throws SystemException {
		return fetchByCouponCode(couponCode, archived, true);
	}

	/**
	 * Returns the coupon where couponCode = &#63; and archived = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param couponCode the coupon code
	 * @param archived the archived
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching coupon, or <code>null</code> if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByCouponCode(String couponCode, boolean archived,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { couponCode, archived };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_COUPONCODE,
					finderArgs, this);
		}

		if (result instanceof Coupon) {
			Coupon coupon = (Coupon)result;

			if (!Validator.equals(couponCode, coupon.getCouponCode()) ||
					(archived != coupon.getArchived())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_COUPON_WHERE);

			boolean bindCouponCode = false;

			if (couponCode == null) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_1);
			}
			else if (couponCode.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_3);
			}
			else {
				bindCouponCode = true;

				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODE_ARCHIVED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCouponCode) {
					qPos.add(couponCode);
				}

				qPos.add(archived);

				List<Coupon> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODE,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CouponPersistenceImpl.fetchByCouponCode(String, boolean, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Coupon coupon = list.get(0);

					result = coupon;

					cacheResult(coupon);

					if ((coupon.getCouponCode() == null) ||
							!coupon.getCouponCode().equals(couponCode) ||
							(coupon.getArchived() != archived)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODE,
							finderArgs, coupon);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODE,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Coupon)result;
		}
	}

	/**
	 * Removes the coupon where couponCode = &#63; and archived = &#63; from the database.
	 *
	 * @param couponCode the coupon code
	 * @param archived the archived
	 * @return the coupon that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon removeByCouponCode(String couponCode, boolean archived)
		throws NoSuchCouponException, SystemException {
		Coupon coupon = findByCouponCode(couponCode, archived);

		return remove(coupon);
	}

	/**
	 * Returns the number of coupons where couponCode = &#63; and archived = &#63;.
	 *
	 * @param couponCode the coupon code
	 * @param archived the archived
	 * @return the number of matching coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCouponCode(String couponCode, boolean archived)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COUPONCODE;

		Object[] finderArgs = new Object[] { couponCode, archived };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COUPON_WHERE);

			boolean bindCouponCode = false;

			if (couponCode == null) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_1);
			}
			else if (couponCode.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_3);
			}
			else {
				bindCouponCode = true;

				query.append(_FINDER_COLUMN_COUPONCODE_COUPONCODE_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODE_ARCHIVED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCouponCode) {
					qPos.add(couponCode);
				}

				qPos.add(archived);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODE_1 = "coupon.couponCode IS NULL AND ";
	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODE_2 = "coupon.couponCode = ? AND ";
	private static final String _FINDER_COLUMN_COUPONCODE_COUPONCODE_3 = "(coupon.couponCode IS NULL OR coupon.couponCode = '') AND ";
	private static final String _FINDER_COLUMN_COUPONCODE_ARCHIVED_2 = "coupon.archived = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COUPONCODEVERSION = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, CouponImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCouponCodeVersion",
			new String[] { String.class.getName(), Double.class.getName() },
			CouponModelImpl.COUPONCODE_COLUMN_BITMASK |
			CouponModelImpl.VERSION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUPONCODEVERSION = new FinderPath(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCouponCodeVersion",
			new String[] { String.class.getName(), Double.class.getName() });

	/**
	 * Returns the coupon where couponCode = &#63; and version = &#63; or throws a {@link com.esquare.ecommerce.NoSuchCouponException} if it could not be found.
	 *
	 * @param couponCode the coupon code
	 * @param version the version
	 * @return the matching coupon
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon findByCouponCodeVersion(String couponCode, double version)
		throws NoSuchCouponException, SystemException {
		Coupon coupon = fetchByCouponCodeVersion(couponCode, version);

		if (coupon == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("couponCode=");
			msg.append(couponCode);

			msg.append(", version=");
			msg.append(version);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCouponException(msg.toString());
		}

		return coupon;
	}

	/**
	 * Returns the coupon where couponCode = &#63; and version = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param couponCode the coupon code
	 * @param version the version
	 * @return the matching coupon, or <code>null</code> if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByCouponCodeVersion(String couponCode, double version)
		throws SystemException {
		return fetchByCouponCodeVersion(couponCode, version, true);
	}

	/**
	 * Returns the coupon where couponCode = &#63; and version = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param couponCode the coupon code
	 * @param version the version
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching coupon, or <code>null</code> if a matching coupon could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByCouponCodeVersion(String couponCode, double version,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { couponCode, version };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
					finderArgs, this);
		}

		if (result instanceof Coupon) {
			Coupon coupon = (Coupon)result;

			if (!Validator.equals(couponCode, coupon.getCouponCode()) ||
					(version != coupon.getVersion())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_COUPON_WHERE);

			boolean bindCouponCode = false;

			if (couponCode == null) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_1);
			}
			else if (couponCode.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_3);
			}
			else {
				bindCouponCode = true;

				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_VERSION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCouponCode) {
					qPos.add(couponCode);
				}

				qPos.add(version);

				List<Coupon> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CouponPersistenceImpl.fetchByCouponCodeVersion(String, double, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Coupon coupon = list.get(0);

					result = coupon;

					cacheResult(coupon);

					if ((coupon.getCouponCode() == null) ||
							!coupon.getCouponCode().equals(couponCode) ||
							(coupon.getVersion() != version)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
							finderArgs, coupon);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Coupon)result;
		}
	}

	/**
	 * Removes the coupon where couponCode = &#63; and version = &#63; from the database.
	 *
	 * @param couponCode the coupon code
	 * @param version the version
	 * @return the coupon that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon removeByCouponCodeVersion(String couponCode, double version)
		throws NoSuchCouponException, SystemException {
		Coupon coupon = findByCouponCodeVersion(couponCode, version);

		return remove(coupon);
	}

	/**
	 * Returns the number of coupons where couponCode = &#63; and version = &#63;.
	 *
	 * @param couponCode the coupon code
	 * @param version the version
	 * @return the number of matching coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCouponCodeVersion(String couponCode, double version)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COUPONCODEVERSION;

		Object[] finderArgs = new Object[] { couponCode, version };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COUPON_WHERE);

			boolean bindCouponCode = false;

			if (couponCode == null) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_1);
			}
			else if (couponCode.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_3);
			}
			else {
				bindCouponCode = true;

				query.append(_FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_2);
			}

			query.append(_FINDER_COLUMN_COUPONCODEVERSION_VERSION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindCouponCode) {
					qPos.add(couponCode);
				}

				qPos.add(version);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_1 = "coupon.couponCode IS NULL AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_2 = "coupon.couponCode = ? AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_COUPONCODE_3 = "(coupon.couponCode IS NULL OR coupon.couponCode = '') AND ";
	private static final String _FINDER_COLUMN_COUPONCODEVERSION_VERSION_2 = "coupon.version = ?";

	public CouponPersistenceImpl() {
		setModelClass(Coupon.class);
	}

	/**
	 * Caches the coupon in the entity cache if it is enabled.
	 *
	 * @param coupon the coupon
	 */
	@Override
	public void cacheResult(Coupon coupon) {
		EntityCacheUtil.putResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponImpl.class, coupon.getPrimaryKey(), coupon);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODE,
			new Object[] { coupon.getCouponCode(), coupon.getArchived() },
			coupon);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
			new Object[] { coupon.getCouponCode(), coupon.getVersion() }, coupon);

		coupon.resetOriginalValues();
	}

	/**
	 * Caches the coupons in the entity cache if it is enabled.
	 *
	 * @param coupons the coupons
	 */
	@Override
	public void cacheResult(List<Coupon> coupons) {
		for (Coupon coupon : coupons) {
			if (EntityCacheUtil.getResult(
						CouponModelImpl.ENTITY_CACHE_ENABLED, CouponImpl.class,
						coupon.getPrimaryKey()) == null) {
				cacheResult(coupon);
			}
			else {
				coupon.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all coupons.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CouponImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CouponImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the coupon.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Coupon coupon) {
		EntityCacheUtil.removeResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponImpl.class, coupon.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(coupon);
	}

	@Override
	public void clearCache(List<Coupon> coupons) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Coupon coupon : coupons) {
			EntityCacheUtil.removeResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
				CouponImpl.class, coupon.getPrimaryKey());

			clearUniqueFindersCache(coupon);
		}
	}

	protected void cacheUniqueFindersCache(Coupon coupon) {
		if (coupon.isNew()) {
			Object[] args = new Object[] {
					coupon.getCouponCode(), coupon.getArchived()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COUPONCODE, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODE, args,
				coupon);

			args = new Object[] { coupon.getCouponCode(), coupon.getVersion() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
				args, coupon);
		}
		else {
			CouponModelImpl couponModelImpl = (CouponModelImpl)coupon;

			if ((couponModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COUPONCODE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coupon.getCouponCode(), coupon.getArchived()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COUPONCODE,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODE,
					args, coupon);
			}

			if ((couponModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COUPONCODEVERSION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						coupon.getCouponCode(), coupon.getVersion()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
					args, coupon);
			}
		}
	}

	protected void clearUniqueFindersCache(Coupon coupon) {
		CouponModelImpl couponModelImpl = (CouponModelImpl)coupon;

		Object[] args = new Object[] {
				coupon.getCouponCode(), coupon.getArchived()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODE, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODE, args);

		if ((couponModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COUPONCODE.getColumnBitmask()) != 0) {
			args = new Object[] {
					couponModelImpl.getOriginalCouponCode(),
					couponModelImpl.getOriginalArchived()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODE, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODE, args);
		}

		args = new Object[] { coupon.getCouponCode(), coupon.getVersion() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
			args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
			args);

		if ((couponModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COUPONCODEVERSION.getColumnBitmask()) != 0) {
			args = new Object[] {
					couponModelImpl.getOriginalCouponCode(),
					couponModelImpl.getOriginalVersion()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUPONCODEVERSION,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COUPONCODEVERSION,
				args);
		}
	}

	/**
	 * Creates a new coupon with the primary key. Does not add the coupon to the database.
	 *
	 * @param couponId the primary key for the new coupon
	 * @return the new coupon
	 */
	@Override
	public Coupon create(long couponId) {
		Coupon coupon = new CouponImpl();

		coupon.setNew(true);
		coupon.setPrimaryKey(couponId);

		return coupon;
	}

	/**
	 * Removes the coupon with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param couponId the primary key of the coupon
	 * @return the coupon that was removed
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon remove(long couponId)
		throws NoSuchCouponException, SystemException {
		return remove((Serializable)couponId);
	}

	/**
	 * Removes the coupon with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the coupon
	 * @return the coupon that was removed
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon remove(Serializable primaryKey)
		throws NoSuchCouponException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Coupon coupon = (Coupon)session.get(CouponImpl.class, primaryKey);

			if (coupon == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCouponException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(coupon);
		}
		catch (NoSuchCouponException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Coupon removeImpl(Coupon coupon) throws SystemException {
		coupon = toUnwrappedModel(coupon);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(coupon)) {
				coupon = (Coupon)session.get(CouponImpl.class,
						coupon.getPrimaryKeyObj());
			}

			if (coupon != null) {
				session.delete(coupon);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (coupon != null) {
			clearCache(coupon);
		}

		return coupon;
	}

	@Override
	public Coupon updateImpl(com.esquare.ecommerce.model.Coupon coupon)
		throws SystemException {
		coupon = toUnwrappedModel(coupon);

		boolean isNew = coupon.isNew();

		Session session = null;

		try {
			session = openSession();

			if (coupon.isNew()) {
				session.save(coupon);

				coupon.setNew(false);
			}
			else {
				session.merge(coupon);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CouponModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
			CouponImpl.class, coupon.getPrimaryKey(), coupon);

		clearUniqueFindersCache(coupon);
		cacheUniqueFindersCache(coupon);

		return coupon;
	}

	protected Coupon toUnwrappedModel(Coupon coupon) {
		if (coupon instanceof CouponImpl) {
			return coupon;
		}

		CouponImpl couponImpl = new CouponImpl();

		couponImpl.setNew(coupon.isNew());
		couponImpl.setPrimaryKey(coupon.getPrimaryKey());

		couponImpl.setCouponId(coupon.getCouponId());
		couponImpl.setGroupId(coupon.getGroupId());
		couponImpl.setCompanyId(coupon.getCompanyId());
		couponImpl.setUserId(coupon.getUserId());
		couponImpl.setVersion(coupon.getVersion());
		couponImpl.setUserName(coupon.getUserName());
		couponImpl.setCreateDate(coupon.getCreateDate());
		couponImpl.setModifiedDate(coupon.getModifiedDate());
		couponImpl.setCouponCode(coupon.getCouponCode());
		couponImpl.setCouponName(coupon.getCouponName());
		couponImpl.setDescription(coupon.getDescription());
		couponImpl.setCouponLimit(coupon.getCouponLimit());
		couponImpl.setUserLimit(coupon.getUserLimit());
		couponImpl.setDiscountType(coupon.getDiscountType());
		couponImpl.setDiscount(coupon.getDiscount());
		couponImpl.setMinOrder(coupon.getMinOrder());
		couponImpl.setDiscountLimit(coupon.getDiscountLimit());
		couponImpl.setLimitCatalogs(coupon.getLimitCatalogs());
		couponImpl.setLimitProducts(coupon.getLimitProducts());
		couponImpl.setStartDate(coupon.getStartDate());
		couponImpl.setEndDate(coupon.getEndDate());
		couponImpl.setActive(coupon.isActive());
		couponImpl.setArchived(coupon.isArchived());
		couponImpl.setNeverExpire(coupon.isNeverExpire());

		return couponImpl;
	}

	/**
	 * Returns the coupon with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the coupon
	 * @return the coupon
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCouponException, SystemException {
		Coupon coupon = fetchByPrimaryKey(primaryKey);

		if (coupon == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCouponException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return coupon;
	}

	/**
	 * Returns the coupon with the primary key or throws a {@link com.esquare.ecommerce.NoSuchCouponException} if it could not be found.
	 *
	 * @param couponId the primary key of the coupon
	 * @return the coupon
	 * @throws com.esquare.ecommerce.NoSuchCouponException if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon findByPrimaryKey(long couponId)
		throws NoSuchCouponException, SystemException {
		return findByPrimaryKey((Serializable)couponId);
	}

	/**
	 * Returns the coupon with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the coupon
	 * @return the coupon, or <code>null</code> if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Coupon coupon = (Coupon)EntityCacheUtil.getResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
				CouponImpl.class, primaryKey);

		if (coupon == _nullCoupon) {
			return null;
		}

		if (coupon == null) {
			Session session = null;

			try {
				session = openSession();

				coupon = (Coupon)session.get(CouponImpl.class, primaryKey);

				if (coupon != null) {
					cacheResult(coupon);
				}
				else {
					EntityCacheUtil.putResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
						CouponImpl.class, primaryKey, _nullCoupon);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CouponModelImpl.ENTITY_CACHE_ENABLED,
					CouponImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return coupon;
	}

	/**
	 * Returns the coupon with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param couponId the primary key of the coupon
	 * @return the coupon, or <code>null</code> if a coupon with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Coupon fetchByPrimaryKey(long couponId) throws SystemException {
		return fetchByPrimaryKey((Serializable)couponId);
	}

	/**
	 * Returns all the coupons.
	 *
	 * @return the coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Coupon> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the coupons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CouponModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coupons
	 * @param end the upper bound of the range of coupons (not inclusive)
	 * @return the range of coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Coupon> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the coupons.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CouponModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of coupons
	 * @param end the upper bound of the range of coupons (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Coupon> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Coupon> list = (List<Coupon>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_COUPON);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COUPON;

				if (pagination) {
					sql = sql.concat(CouponModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Coupon>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Coupon>(list);
				}
				else {
					list = (List<Coupon>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the coupons from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Coupon coupon : findAll()) {
			remove(coupon);
		}
	}

	/**
	 * Returns the number of coupons.
	 *
	 * @return the number of coupons
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COUPON);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the coupon persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.Coupon")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Coupon>> listenersList = new ArrayList<ModelListener<Coupon>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Coupon>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CouponImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_COUPON = "SELECT coupon FROM Coupon coupon";
	private static final String _SQL_SELECT_COUPON_WHERE = "SELECT coupon FROM Coupon coupon WHERE ";
	private static final String _SQL_COUNT_COUPON = "SELECT COUNT(coupon) FROM Coupon coupon";
	private static final String _SQL_COUNT_COUPON_WHERE = "SELECT COUNT(coupon) FROM Coupon coupon WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "coupon.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Coupon exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Coupon exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CouponPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"active"
			});
	private static Coupon _nullCoupon = new CouponImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Coupon> toCacheModel() {
				return _nullCouponCacheModel;
			}
		};

	private static CacheModel<Coupon> _nullCouponCacheModel = new CacheModel<Coupon>() {
			@Override
			public Coupon toEntityModel() {
				return _nullCoupon;
			}
		};
}