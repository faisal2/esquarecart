/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Cart;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Cart in entity cache.
 *
 * @author Esquare
 * @see Cart
 * @generated
 */
public class CartCacheModel implements CacheModel<Cart>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{cartId=");
		sb.append(cartId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", productInventoryIds=");
		sb.append(productInventoryIds);
		sb.append(", couponCodes=");
		sb.append(couponCodes);
		sb.append(", walletAmount=");
		sb.append(walletAmount);
		sb.append(", altShipping=");
		sb.append(altShipping);
		sb.append(", insure=");
		sb.append(insure);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Cart toEntityModel() {
		CartImpl cartImpl = new CartImpl();

		cartImpl.setCartId(cartId);
		cartImpl.setGroupId(groupId);
		cartImpl.setCompanyId(companyId);
		cartImpl.setUserId(userId);

		if (userName == null) {
			cartImpl.setUserName(StringPool.BLANK);
		}
		else {
			cartImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			cartImpl.setCreateDate(null);
		}
		else {
			cartImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			cartImpl.setModifiedDate(null);
		}
		else {
			cartImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (productInventoryIds == null) {
			cartImpl.setProductInventoryIds(StringPool.BLANK);
		}
		else {
			cartImpl.setProductInventoryIds(productInventoryIds);
		}

		if (couponCodes == null) {
			cartImpl.setCouponCodes(StringPool.BLANK);
		}
		else {
			cartImpl.setCouponCodes(couponCodes);
		}

		cartImpl.setWalletAmount(walletAmount);
		cartImpl.setAltShipping(altShipping);
		cartImpl.setInsure(insure);

		cartImpl.resetOriginalValues();

		return cartImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		cartId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		productInventoryIds = objectInput.readUTF();
		couponCodes = objectInput.readUTF();
		walletAmount = objectInput.readDouble();
		altShipping = objectInput.readInt();
		insure = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(cartId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (productInventoryIds == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(productInventoryIds);
		}

		if (couponCodes == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(couponCodes);
		}

		objectOutput.writeDouble(walletAmount);
		objectOutput.writeInt(altShipping);
		objectOutput.writeBoolean(insure);
	}

	public long cartId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String productInventoryIds;
	public String couponCodes;
	public double walletAmount;
	public int altShipping;
	public boolean insure;
}