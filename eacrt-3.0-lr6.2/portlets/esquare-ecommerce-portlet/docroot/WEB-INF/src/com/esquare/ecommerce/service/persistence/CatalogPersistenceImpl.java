/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchCatalogException;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.impl.CatalogImpl;
import com.esquare.ecommerce.model.impl.CatalogModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the catalog service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see CatalogPersistence
 * @see CatalogUtil
 * @generated
 */
public class CatalogPersistenceImpl extends BasePersistenceImpl<Catalog>
	implements CatalogPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CatalogUtil} to access the catalog persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CatalogImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			CatalogModelImpl.COMPANYID_COLUMN_BITMASK |
			CatalogModelImpl.PARENTCATALOGID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the catalogs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the catalogs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @return the range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByCompanyId(long companyId, int start, int end)
		throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogs where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByCompanyId(long companyId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<Catalog> list = (List<Catalog>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Catalog catalog : list) {
				if ((companyId != catalog.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CatalogModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Catalog>(list);
				}
				else {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByCompanyId_First(companyId, orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Catalog> list = findByCompanyId(companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByCompanyId_Last(companyId, orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<Catalog> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the catalogs before and after the current catalog in the ordered set where companyId = &#63;.
	 *
	 * @param catalogId the primary key of the current catalog
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog[] findByCompanyId_PrevAndNext(long catalogId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = findByPrimaryKey(catalogId);

		Session session = null;

		try {
			session = openSession();

			Catalog[] array = new CatalogImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, catalog, companyId,
					orderByComparator, true);

			array[1] = catalog;

			array[2] = getByCompanyId_PrevAndNext(session, catalog, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Catalog getByCompanyId_PrevAndNext(Session session,
		Catalog catalog, long companyId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CATALOG_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CatalogModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(catalog);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Catalog> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the catalogs where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (Catalog catalog : findByCompanyId(companyId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(catalog);
		}
	}

	/**
	 * Returns the number of catalogs where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "catalog.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_P = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_P",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_P = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_P",
			new String[] { Long.class.getName(), Long.class.getName() },
			CatalogModelImpl.COMPANYID_COLUMN_BITMASK |
			CatalogModelImpl.PARENTCATALOGID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_P = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_P",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the catalogs where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @return the matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByC_P(long companyId, long parentCatalogId)
		throws SystemException {
		return findByC_P(companyId, parentCatalogId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogs where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @return the range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByC_P(long companyId, long parentCatalogId,
		int start, int end) throws SystemException {
		return findByC_P(companyId, parentCatalogId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogs where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByC_P(long companyId, long parentCatalogId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_P;
			finderArgs = new Object[] { companyId, parentCatalogId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_P;
			finderArgs = new Object[] {
					companyId, parentCatalogId,
					
					start, end, orderByComparator
				};
		}

		List<Catalog> list = (List<Catalog>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Catalog catalog : list) {
				if ((companyId != catalog.getCompanyId()) ||
						(parentCatalogId != catalog.getParentCatalogId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_C_P_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_P_PARENTCATALOGID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CatalogModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(parentCatalogId);

				if (!pagination) {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Catalog>(list);
				}
				else {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByC_P_First(long companyId, long parentCatalogId,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByC_P_First(companyId, parentCatalogId,
				orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", parentCatalogId=");
		msg.append(parentCatalogId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByC_P_First(long companyId, long parentCatalogId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Catalog> list = findByC_P(companyId, parentCatalogId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByC_P_Last(long companyId, long parentCatalogId,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByC_P_Last(companyId, parentCatalogId,
				orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", parentCatalogId=");
		msg.append(parentCatalogId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByC_P_Last(long companyId, long parentCatalogId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_P(companyId, parentCatalogId);

		if (count == 0) {
			return null;
		}

		List<Catalog> list = findByC_P(companyId, parentCatalogId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the catalogs before and after the current catalog in the ordered set where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param catalogId the primary key of the current catalog
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog[] findByC_P_PrevAndNext(long catalogId, long companyId,
		long parentCatalogId, OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = findByPrimaryKey(catalogId);

		Session session = null;

		try {
			session = openSession();

			Catalog[] array = new CatalogImpl[3];

			array[0] = getByC_P_PrevAndNext(session, catalog, companyId,
					parentCatalogId, orderByComparator, true);

			array[1] = catalog;

			array[2] = getByC_P_PrevAndNext(session, catalog, companyId,
					parentCatalogId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Catalog getByC_P_PrevAndNext(Session session, Catalog catalog,
		long companyId, long parentCatalogId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CATALOG_WHERE);

		query.append(_FINDER_COLUMN_C_P_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_P_PARENTCATALOGID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CatalogModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(parentCatalogId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(catalog);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Catalog> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the catalogs where companyId = &#63; and parentCatalogId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_P(long companyId, long parentCatalogId)
		throws SystemException {
		for (Catalog catalog : findByC_P(companyId, parentCatalogId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(catalog);
		}
	}

	/**
	 * Returns the number of catalogs where companyId = &#63; and parentCatalogId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param parentCatalogId the parent catalog ID
	 * @return the number of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_P(long companyId, long parentCatalogId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_P;

		Object[] finderArgs = new Object[] { companyId, parentCatalogId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_C_P_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_P_PARENTCATALOGID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(parentCatalogId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_P_COMPANYID_2 = "catalog.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_P_PARENTCATALOGID_2 = "catalog.parentCatalogId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SHOWNAVIGATION =
		new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByShowNavigation",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SHOWNAVIGATION =
		new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, CatalogImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByShowNavigation",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			CatalogModelImpl.COMPANYID_COLUMN_BITMASK |
			CatalogModelImpl.SHOWNAVIGATION_COLUMN_BITMASK |
			CatalogModelImpl.PARENTCATALOGID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SHOWNAVIGATION = new FinderPath(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByShowNavigation",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the catalogs where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @return the matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByShowNavigation(long companyId,
		boolean showNavigation) throws SystemException {
		return findByShowNavigation(companyId, showNavigation,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogs where companyId = &#63; and showNavigation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @return the range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByShowNavigation(long companyId,
		boolean showNavigation, int start, int end) throws SystemException {
		return findByShowNavigation(companyId, showNavigation, start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogs where companyId = &#63; and showNavigation = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findByShowNavigation(long companyId,
		boolean showNavigation, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SHOWNAVIGATION;
			finderArgs = new Object[] { companyId, showNavigation };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SHOWNAVIGATION;
			finderArgs = new Object[] {
					companyId, showNavigation,
					
					start, end, orderByComparator
				};
		}

		List<Catalog> list = (List<Catalog>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Catalog catalog : list) {
				if ((companyId != catalog.getCompanyId()) ||
						(showNavigation != catalog.getShowNavigation())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_SHOWNAVIGATION_COMPANYID_2);

			query.append(_FINDER_COLUMN_SHOWNAVIGATION_SHOWNAVIGATION_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CatalogModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(showNavigation);

				if (!pagination) {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Catalog>(list);
				}
				else {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByShowNavigation_First(long companyId,
		boolean showNavigation, OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByShowNavigation_First(companyId,
				showNavigation, orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", showNavigation=");
		msg.append(showNavigation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the first catalog in the ordered set where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByShowNavigation_First(long companyId,
		boolean showNavigation, OrderByComparator orderByComparator)
		throws SystemException {
		List<Catalog> list = findByShowNavigation(companyId, showNavigation, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByShowNavigation_Last(long companyId,
		boolean showNavigation, OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByShowNavigation_Last(companyId, showNavigation,
				orderByComparator);

		if (catalog != null) {
			return catalog;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", showNavigation=");
		msg.append(showNavigation);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogException(msg.toString());
	}

	/**
	 * Returns the last catalog in the ordered set where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalog, or <code>null</code> if a matching catalog could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByShowNavigation_Last(long companyId,
		boolean showNavigation, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByShowNavigation(companyId, showNavigation);

		if (count == 0) {
			return null;
		}

		List<Catalog> list = findByShowNavigation(companyId, showNavigation,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the catalogs before and after the current catalog in the ordered set where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param catalogId the primary key of the current catalog
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog[] findByShowNavigation_PrevAndNext(long catalogId,
		long companyId, boolean showNavigation,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = findByPrimaryKey(catalogId);

		Session session = null;

		try {
			session = openSession();

			Catalog[] array = new CatalogImpl[3];

			array[0] = getByShowNavigation_PrevAndNext(session, catalog,
					companyId, showNavigation, orderByComparator, true);

			array[1] = catalog;

			array[2] = getByShowNavigation_PrevAndNext(session, catalog,
					companyId, showNavigation, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Catalog getByShowNavigation_PrevAndNext(Session session,
		Catalog catalog, long companyId, boolean showNavigation,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CATALOG_WHERE);

		query.append(_FINDER_COLUMN_SHOWNAVIGATION_COMPANYID_2);

		query.append(_FINDER_COLUMN_SHOWNAVIGATION_SHOWNAVIGATION_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CatalogModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(showNavigation);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(catalog);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Catalog> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the catalogs where companyId = &#63; and showNavigation = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByShowNavigation(long companyId, boolean showNavigation)
		throws SystemException {
		for (Catalog catalog : findByShowNavigation(companyId, showNavigation,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(catalog);
		}
	}

	/**
	 * Returns the number of catalogs where companyId = &#63; and showNavigation = &#63;.
	 *
	 * @param companyId the company ID
	 * @param showNavigation the show navigation
	 * @return the number of matching catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByShowNavigation(long companyId, boolean showNavigation)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SHOWNAVIGATION;

		Object[] finderArgs = new Object[] { companyId, showNavigation };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CATALOG_WHERE);

			query.append(_FINDER_COLUMN_SHOWNAVIGATION_COMPANYID_2);

			query.append(_FINDER_COLUMN_SHOWNAVIGATION_SHOWNAVIGATION_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(showNavigation);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SHOWNAVIGATION_COMPANYID_2 = "catalog.companyId = ? AND ";
	private static final String _FINDER_COLUMN_SHOWNAVIGATION_SHOWNAVIGATION_2 = "catalog.showNavigation = ?";

	public CatalogPersistenceImpl() {
		setModelClass(Catalog.class);
	}

	/**
	 * Caches the catalog in the entity cache if it is enabled.
	 *
	 * @param catalog the catalog
	 */
	@Override
	public void cacheResult(Catalog catalog) {
		EntityCacheUtil.putResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogImpl.class, catalog.getPrimaryKey(), catalog);

		catalog.resetOriginalValues();
	}

	/**
	 * Caches the catalogs in the entity cache if it is enabled.
	 *
	 * @param catalogs the catalogs
	 */
	@Override
	public void cacheResult(List<Catalog> catalogs) {
		for (Catalog catalog : catalogs) {
			if (EntityCacheUtil.getResult(
						CatalogModelImpl.ENTITY_CACHE_ENABLED,
						CatalogImpl.class, catalog.getPrimaryKey()) == null) {
				cacheResult(catalog);
			}
			else {
				catalog.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all catalogs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CatalogImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CatalogImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the catalog.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Catalog catalog) {
		EntityCacheUtil.removeResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogImpl.class, catalog.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Catalog> catalogs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Catalog catalog : catalogs) {
			EntityCacheUtil.removeResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
				CatalogImpl.class, catalog.getPrimaryKey());
		}
	}

	/**
	 * Creates a new catalog with the primary key. Does not add the catalog to the database.
	 *
	 * @param catalogId the primary key for the new catalog
	 * @return the new catalog
	 */
	@Override
	public Catalog create(long catalogId) {
		Catalog catalog = new CatalogImpl();

		catalog.setNew(true);
		catalog.setPrimaryKey(catalogId);

		return catalog;
	}

	/**
	 * Removes the catalog with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param catalogId the primary key of the catalog
	 * @return the catalog that was removed
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog remove(long catalogId)
		throws NoSuchCatalogException, SystemException {
		return remove((Serializable)catalogId);
	}

	/**
	 * Removes the catalog with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the catalog
	 * @return the catalog that was removed
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog remove(Serializable primaryKey)
		throws NoSuchCatalogException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Catalog catalog = (Catalog)session.get(CatalogImpl.class, primaryKey);

			if (catalog == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCatalogException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(catalog);
		}
		catch (NoSuchCatalogException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Catalog removeImpl(Catalog catalog) throws SystemException {
		catalog = toUnwrappedModel(catalog);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(catalog)) {
				catalog = (Catalog)session.get(CatalogImpl.class,
						catalog.getPrimaryKeyObj());
			}

			if (catalog != null) {
				session.delete(catalog);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (catalog != null) {
			clearCache(catalog);
		}

		return catalog;
	}

	@Override
	public Catalog updateImpl(com.esquare.ecommerce.model.Catalog catalog)
		throws SystemException {
		catalog = toUnwrappedModel(catalog);

		boolean isNew = catalog.isNew();

		CatalogModelImpl catalogModelImpl = (CatalogModelImpl)catalog;

		Session session = null;

		try {
			session = openSession();

			if (catalog.isNew()) {
				session.save(catalog);

				catalog.setNew(false);
			}
			else {
				session.merge(catalog);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CatalogModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((catalogModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						catalogModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { catalogModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((catalogModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_P.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						catalogModelImpl.getOriginalCompanyId(),
						catalogModelImpl.getOriginalParentCatalogId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_P, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_P,
					args);

				args = new Object[] {
						catalogModelImpl.getCompanyId(),
						catalogModelImpl.getParentCatalogId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_P, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_P,
					args);
			}

			if ((catalogModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SHOWNAVIGATION.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						catalogModelImpl.getOriginalCompanyId(),
						catalogModelImpl.getOriginalShowNavigation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SHOWNAVIGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SHOWNAVIGATION,
					args);

				args = new Object[] {
						catalogModelImpl.getCompanyId(),
						catalogModelImpl.getShowNavigation()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SHOWNAVIGATION,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SHOWNAVIGATION,
					args);
			}
		}

		EntityCacheUtil.putResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
			CatalogImpl.class, catalog.getPrimaryKey(), catalog);

		return catalog;
	}

	protected Catalog toUnwrappedModel(Catalog catalog) {
		if (catalog instanceof CatalogImpl) {
			return catalog;
		}

		CatalogImpl catalogImpl = new CatalogImpl();

		catalogImpl.setNew(catalog.isNew());
		catalogImpl.setPrimaryKey(catalog.getPrimaryKey());

		catalogImpl.setCatalogId(catalog.getCatalogId());
		catalogImpl.setGroupId(catalog.getGroupId());
		catalogImpl.setCompanyId(catalog.getCompanyId());
		catalogImpl.setUserId(catalog.getUserId());
		catalogImpl.setCreateDate(catalog.getCreateDate());
		catalogImpl.setModifiedDate(catalog.getModifiedDate());
		catalogImpl.setParentCatalogId(catalog.getParentCatalogId());
		catalogImpl.setName(catalog.getName());
		catalogImpl.setDescription(catalog.getDescription());
		catalogImpl.setFolderId(catalog.getFolderId());
		catalogImpl.setShowNavigation(catalog.isShowNavigation());

		return catalogImpl;
	}

	/**
	 * Returns the catalog with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the catalog
	 * @return the catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCatalogException, SystemException {
		Catalog catalog = fetchByPrimaryKey(primaryKey);

		if (catalog == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCatalogException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return catalog;
	}

	/**
	 * Returns the catalog with the primary key or throws a {@link com.esquare.ecommerce.NoSuchCatalogException} if it could not be found.
	 *
	 * @param catalogId the primary key of the catalog
	 * @return the catalog
	 * @throws com.esquare.ecommerce.NoSuchCatalogException if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog findByPrimaryKey(long catalogId)
		throws NoSuchCatalogException, SystemException {
		return findByPrimaryKey((Serializable)catalogId);
	}

	/**
	 * Returns the catalog with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the catalog
	 * @return the catalog, or <code>null</code> if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Catalog catalog = (Catalog)EntityCacheUtil.getResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
				CatalogImpl.class, primaryKey);

		if (catalog == _nullCatalog) {
			return null;
		}

		if (catalog == null) {
			Session session = null;

			try {
				session = openSession();

				catalog = (Catalog)session.get(CatalogImpl.class, primaryKey);

				if (catalog != null) {
					cacheResult(catalog);
				}
				else {
					EntityCacheUtil.putResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
						CatalogImpl.class, primaryKey, _nullCatalog);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CatalogModelImpl.ENTITY_CACHE_ENABLED,
					CatalogImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return catalog;
	}

	/**
	 * Returns the catalog with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param catalogId the primary key of the catalog
	 * @return the catalog, or <code>null</code> if a catalog with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalog fetchByPrimaryKey(long catalogId) throws SystemException {
		return fetchByPrimaryKey((Serializable)catalogId);
	}

	/**
	 * Returns all the catalogs.
	 *
	 * @return the catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @return the range of catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CatalogModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogs
	 * @param end the upper bound of the range of catalogs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Catalog> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Catalog> list = (List<Catalog>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CATALOG);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CATALOG;

				if (pagination) {
					sql = sql.concat(CatalogModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Catalog>(list);
				}
				else {
					list = (List<Catalog>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the catalogs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Catalog catalog : findAll()) {
			remove(catalog);
		}
	}

	/**
	 * Returns the number of catalogs.
	 *
	 * @return the number of catalogs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CATALOG);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the catalog persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.Catalog")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Catalog>> listenersList = new ArrayList<ModelListener<Catalog>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Catalog>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CatalogImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CATALOG = "SELECT catalog FROM Catalog catalog";
	private static final String _SQL_SELECT_CATALOG_WHERE = "SELECT catalog FROM Catalog catalog WHERE ";
	private static final String _SQL_COUNT_CATALOG = "SELECT COUNT(catalog) FROM Catalog catalog";
	private static final String _SQL_COUNT_CATALOG_WHERE = "SELECT COUNT(catalog) FROM Catalog catalog WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "catalog.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Catalog exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Catalog exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CatalogPersistenceImpl.class);
	private static Catalog _nullCatalog = new CatalogImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Catalog> toCacheModel() {
				return _nullCatalogCacheModel;
			}
		};

	private static CacheModel<Catalog> _nullCatalogCacheModel = new CacheModel<Catalog>() {
			@Override
			public Catalog toEntityModel() {
				return _nullCatalog;
			}
		};
}