/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.NoSuchProductImagesException;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.service.base.ProductImagesLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.ProductImagesUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the product images local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.ProductImagesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.ProductImagesLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.ProductImagesLocalServiceUtil
 */
public class ProductImagesLocalServiceImpl
	extends ProductImagesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.esquare.ecommerce.service.ProductImagesLocalServiceUtil} to access the product images local service.
	 */
	public List<ProductImages> findByInventoryId(long inventoryId)
			throws SystemException
			{
			List<ProductImages> productImages = null;
				
				productImages = productImagesPersistence.findByInventoryId(inventoryId);
				
				return productImages;
			}
}