/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.NoSuchShippingRatesException;
import com.esquare.ecommerce.model.ShippingRates;
import com.esquare.ecommerce.service.base.ShippingRatesLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.ShippingRatesFinderUtil;
import com.esquare.ecommerce.service.persistence.ShippingRatesUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the shipping rates local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.ShippingRatesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.ShippingRatesLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.ShippingRatesLocalServiceUtil
 */
public class ShippingRatesLocalServiceImpl extends
		ShippingRatesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * com.esquare.ecommerce.service.ShippingRatesLocalServiceUtil} to access
	 * the shipping rates local service.
	 */
	public List<ShippingRates> getShippingRatesDetails(long companyId)
			throws SystemException {

		return ShippingRatesFinderUtil.findByCompanyId(companyId);
	}

	public List<ShippingRates> getShippingSettingRecord(long companyId,
			String countryId, String shippingType, double value)
			throws SystemException {

		return ShippingRatesFinderUtil.getShippingSettingRecord(companyId,
				countryId, shippingType, value);
	}

	public List<ShippingRates> findByCountry(long companyId, String countryId)
			throws SystemException {

		return ShippingRatesUtil.findByCountry(companyId, countryId);
	}

	public ShippingRates findByRanges(long companyId, String countryId,
			String shippingType, double rangeFrom, double rangeTo) {
		ShippingRates shippingRates = null;
		try {
			shippingRates = ShippingRatesUtil.findByRanges(companyId,
					countryId, shippingType, rangeFrom, rangeTo);
		} catch (SystemException e) {
		} catch (NoSuchShippingRatesException e) {
		}
		return shippingRates;
	}

	public boolean findShippingRanges(long companyId, String countryId,
			long recId, String shippingType, double rangeFrom, double rangeTo)
			throws SystemException {

		return ShippingRatesFinderUtil.findShippingRanges(companyId, countryId,
				recId, shippingType, rangeFrom, rangeTo);
	}
}