package com.esquare.ecommerce.service.persistence;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.impl.ProductInventoryImpl;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ProductInventoryFinderImpl extends BasePersistenceImpl implements
		ProductInventoryFinder {
	@SuppressWarnings("unchecked")
	public List<ProductInventory> getProductList(String catalogIds,
			int viewCount) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_PRODUCTLIST);
			sql = sql.replace("[$CATALOG_IDS$]", catalogIds);
			if (viewCount > 0)
				sql = sql.concat("limit" + StringPool.SPACE
						+ String.valueOf(viewCount));
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);

		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List<ProductInventory>) query.list();
	}

	public int getStockCount(long companyId, int quantity) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_STOCK_COUNT);
			if (quantity > 0) {
				sql = sql.concat("AND (EEC_ProductInventory.quantity > 0)");
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("QUANTITY", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(companyId);
			qPos.add(quantity);

			Iterator<Long> itr = query.list().iterator();
			if (itr.hasNext()) {
				Long count = itr.next();
				if (count != null) {
					return count.intValue();
				}
			}

		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public double getPriceRangeByCatalogId(String catalogIds, String orderType) {

		Session session = null;
		SQLQuery query = null;
		try {
			if (Validator.isNotNull(catalogIds)) {
				String sql = CustomSQLUtil.get(GET_PRICEBY_CATALOGID);
				sql = sql.replace("[$CATALOG_IDS$]", catalogIds);
				sql = sql.replace("[$ORDER_TYPE$]", orderType);
				session = openSession();
				query = session.createSQLQuery(sql);
				// query.addScalar("PRICE", Type.DOUBLE);
				QueryPos qPos = QueryPos.getInstance(query);
				Iterator<Double> itr = query.list().iterator();
				if (itr.hasNext()) {
					double price = itr.next();
					if (price > 0) {
						return price;
					}
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return 0.0;
	}

	public void getCatalogIdsbyCreateDate(long companyId,
			LinkedHashMap<String, String> childCatalogMap) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_CATALOGIDSBY_CREATEDATE);
			sql = sql.replace("[$MONTH_DIFF$]",
					String.valueOf(PortletPropsValues.NEW_LAUNCH_MONTH_DIFF));
			sql = sql.replace("[$COMPANYID$]", String.valueOf(companyId));
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("NAME", Type.STRING);
			query.addScalar("CATALOG_ID", Type.LONG);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				if (!childCatalogMap.containsKey((String) array[0])) {
					childCatalogMap.put((String) array[0],
							String.valueOf(array[1]));
				} else {
					childCatalogMap.put(
							(String) array[0],
							childCatalogMap.get((String) array[0])
									+ StringPool.COMMA
									+ String.valueOf(array[1]));
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
	}

	public void getCatalogIdsbyDiscount(long companyId,
			LinkedHashMap<String, String> childCatalogMap) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_CATALOGIDSBY_DISCOUNT);
			sql = sql.replace("[$COMPANYID$]", String.valueOf(companyId));
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("NAME", Type.STRING);
			query.addScalar("CATALOG_ID", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				if (!childCatalogMap.containsKey((String) array[0])) {
					childCatalogMap.put((String) array[0],
							String.valueOf(array[1]));
				} else {
					childCatalogMap.put(
							(String) array[0],
							childCatalogMap.get((String) array[0])
									+ StringPool.COMMA
									+ String.valueOf(array[1]));
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
	}

	public List<ProductInventory> getProductListByPricePange(long companyId,
			long curProductId, String searchName, String catalogIds,
			String priceRanges, String sortByName, String cmd) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_PRODUCTLISTBY_PRICE);
			sql = sql.replace("[$CATALOG_IDS$]", catalogIds);
			if (Validator.isNotNull(cmd)) {
				if (cmd.equals(EECConstants.NEWLAUNCH)) {
					sql = sql
							.concat("AND (a.createDate BETWEEN NOW() - INTERVAL [$MONTH_DIFF$] MONTH AND NOW())"
									+ StringPool.SPACE);
					sql = sql.replace("[$MONTH_DIFF$]", String
							.valueOf(PortletPropsValues.NEW_LAUNCH_MONTH_DIFF));
					sql = sql.replace("[$COMPANYID$]",
							String.valueOf(companyId));
				} else if (cmd.equals(EECConstants.HOTDEAL)) {
					sql = sql.concat("AND a.discount>0" + StringPool.SPACE);
				} else if (cmd.equals(EECConstants.SEARCH)) {
					sql = sql.concat("AND b.name like " + StringPool.QUOTE
							+ StringPool.PERCENT + searchName
							+ StringPool.PERCENT + StringPool.QUOTE
							+ StringPool.SPACE);
				} else if (cmd.equals(EECConstants.SIMILAR_PRODUCTS)) {

					double price = 0l;
					double priceRangeOffset = 0l;
					try {
						price = ProductInventoryLocalServiceUtil
								.getProductInventory(curProductId).getPrice();
						priceRangeOffset = Double
								.parseDouble(EECUtil
										.getPreference(
												companyId,
												EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT));
					} catch (NumberFormatException exception) {
						priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
					} catch (NullPointerException exception) {
						priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
					} catch (SystemException e) {
						_log.info(e.getClass());
					} catch (PortalException e) {
						_log.info(e.getClass());
					}
					double priceRangeOffsetValue = (price * (priceRangeOffset / 100));
					sql = sql.concat(" AND a.price BETWEEN "
							+ (price - priceRangeOffsetValue) + " AND "
							+ (price + priceRangeOffsetValue)
							+ StringPool.SPACE);

				} else if (cmd.equals(EECConstants.RELATED_PRODUCT)) {
					sql = sql.concat(" AND a.limitProducts LIKE "
							+ StringPool.QUOTE + StringPool.PERCENT
							+ curProductId + StringPool.PERCENT
							+ StringPool.QUOTE + StringPool.SPACE);
				}
			}
			sql = sql.concat("GROUP BY a.productDetailsId" + StringPool.SPACE);
			if (Validator.isNotNull(priceRanges)) {
				String[] rangearray = priceRanges.split(StringPool.AT);
				if (rangearray.length > 1) {
					sql = sql.concat("HAVING(");
					for (int i = 0; i < rangearray.length; i++) {
						sql = sql.concat("newprice BETWEEN [$PRICE_RANGE$] OR"
								+ StringPool.SPACE);
						sql = sql.replace("[$PRICE_RANGE$]", rangearray[i]);
						if (i == rangearray.length - 1) {
							sql = sql.substring(0, sql.toString().length() - 3);
							sql = sql.concat(")");
						}
					}
				} else {
					sql = sql
							.concat("HAVING (newprice BETWEEN [$PRICE_RANGE$])"
									+ StringPool.SPACE);
					sql = sql.replace("[$PRICE_RANGE$]", rangearray[0]);
				}
			}
			if (Validator.isNotNull(sortByName)) {
				if (sortByName.equals("AtoZ"))
					sql = sql.concat("ORDER BY b.name");
				else if (sortByName.equals("ZtoA"))
					sql = sql.concat("ORDER BY b.name DESC");
				else if (sortByName.equals("LowtoHigh"))
					sql = sql.concat("ORDER BY newprice");
				else
					sql = sql.concat("ORDER BY newprice DESC");
			} else {
				sql = sql.concat("ORDER BY b.name");
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);

		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public List<ProductInventory> getRelatedProductList(long catalogId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_RELATEDPRODUCTLIST);
			sql = sql.replace("[$CATALOG_ID$]", String.valueOf(catalogId));
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);

		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public List<ProductInventory> getShowMoreProductList(long companyId,
			long curProductId, String searchName, String catalogIds,
			String priceRanges, String sortByName, String cmd, int limit) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_PRODUCTLISTBY_PRICE);
			sql = sql.replace("[$CATALOG_IDS$]", catalogIds);
			if (Validator.isNotNull(cmd)) {
				if (cmd.equals(EECConstants.NEWLAUNCH)) {
					sql = sql
							.concat("AND (a.createDate BETWEEN NOW() - INTERVAL [$MONTH_DIFF$] MONTH AND NOW())"
									+ StringPool.SPACE);
					sql = sql.replace("[$MONTH_DIFF$]", String
							.valueOf(PortletPropsValues.NEW_LAUNCH_MONTH_DIFF));
					sql = sql.replace("[$COMPANYID$]",
							String.valueOf(companyId));
				} else if (cmd.equals(EECConstants.HOTDEAL)) {
					sql = sql.concat("AND a.discount>0" + StringPool.SPACE);
				} else if (cmd.equals(EECConstants.SEARCH)) {
					sql = sql.concat("AND b.name like " + StringPool.QUOTE
							+ StringPool.PERCENT + searchName
							+ StringPool.PERCENT + StringPool.QUOTE
							+ StringPool.SPACE);
				} else if (cmd.equals(EECConstants.SIMILAR_PRODUCTS)) {

					double price = 0l;
					double priceRangeOffset = 0l;
					try {
						price = ProductInventoryLocalServiceUtil
								.getProductInventory(curProductId).getPrice();
						priceRangeOffset = Double
								.parseDouble(EECUtil
										.getPreference(
												companyId,
												EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT));
					} catch (NumberFormatException exception) {
						priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
					} catch (NullPointerException exception) {
						priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
					} catch (SystemException e) {
						_log.info(e.getClass());
					} catch (PortalException e) {
						_log.info(e.getClass());
					}
					double priceRangeOffsetValue = (price * (priceRangeOffset / 100));
					sql = sql.concat(" AND a.price BETWEEN "
							+ (price - priceRangeOffsetValue) + " AND "
							+ (price + priceRangeOffsetValue)
							+ StringPool.SPACE);

				} else if (cmd.equals(EECConstants.RELATED_PRODUCT)) {
					sql = sql.concat(" AND a.limitProducts LIKE "
							+ StringPool.QUOTE + StringPool.PERCENT
							+ curProductId + StringPool.PERCENT
							+ StringPool.QUOTE + StringPool.SPACE);
				}
			}
			sql = sql.concat("GROUP BY a.productDetailsId" + StringPool.SPACE);
			if (Validator.isNotNull(priceRanges)) {
				String[] rangearray = priceRanges.split(StringPool.AT);
				if (rangearray.length > 1) {
					sql = sql.concat("HAVING(");
					for (int i = 0; i < rangearray.length; i++) {
						sql = sql.concat("newprice BETWEEN [$PRICE_RANGE$] OR"
								+ StringPool.SPACE);
						sql = sql.replace("[$PRICE_RANGE$]", rangearray[i]);
						if (i == rangearray.length - 1) {
							sql = sql.substring(0, sql.toString().length() - 3);
							sql = sql.concat(")");
						}
					}
				} else {
					sql = sql
							.concat("HAVING (newprice BETWEEN [$PRICE_RANGE$])"
									+ StringPool.SPACE);
					sql = sql.replace("[$PRICE_RANGE$]", rangearray[0]);
				}
			}
			if (Validator.isNotNull(sortByName)) {
				if (sortByName.equals("AtoZ"))
					sql = sql.concat("ORDER BY b.name" + StringPool.SPACE);
				else if (sortByName.equals("ZtoA"))
					sql = sql.concat("ORDER BY b.name DESC" + StringPool.SPACE);
				else if (sortByName.equals("LowtoHigh"))
					sql = sql.concat("ORDER BY newprice" + StringPool.SPACE);
				else
					sql = sql.concat("ORDER BY newprice DESC"
							+ StringPool.SPACE);
			} else {
				sql = sql.concat("ORDER BY b.name" + StringPool.SPACE);
			}
			session = openSession();
			sql = sql
					.concat("limit" + StringPool.SPACE + String.valueOf(limit));
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public void getCatalogIdsbySearch(long companyId, String productName,
			LinkedHashMap<String, String> childCatalogMap) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_CATALOGIDSBY_SEARCH);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("NAME", Type.STRING);
			query.addScalar("CATALOG_ID", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(StringPool.PERCENT + productName + StringPool.PERCENT);
			qPos.add(companyId);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				if (!childCatalogMap.containsKey((String) array[0])) {
					childCatalogMap.put((String) array[0],
							String.valueOf(array[1]));
				} else {
					childCatalogMap.put(
							(String) array[0],
							childCatalogMap.get((String) array[0])
									+ StringPool.COMMA
									+ String.valueOf(array[1]));
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
	}

	public void getCatalogIdsbySimilarProduct(long companyId, double minPrice,
			double maxPrice, LinkedHashMap<String, String> childCatalogMap) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_CATALOGIDSBY_SIMILAR_PRODUCT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("NAME", Type.STRING);
			query.addScalar("CATALOG_ID", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(minPrice);
			qPos.add(maxPrice);
			qPos.add(companyId);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				if (!childCatalogMap.containsKey((String) array[0])) {
					childCatalogMap.put((String) array[0],
							String.valueOf(array[1]));
				} else {
					childCatalogMap.put(
							(String) array[0],
							childCatalogMap.get((String) array[0])
									+ StringPool.COMMA
									+ String.valueOf(array[1]));
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
	}

	@SuppressWarnings("unchecked")
	public List<ProductInventory> getSimilarProduct(long companyId,long curInventoryId,
			double minPrice, double maxPrice, boolean visibility,
			boolean descOrder, int limit) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_SIMILAR_PRODUCTS);
			if (descOrder) {
				sql = StringUtil.replace(sql, "[$ORDER_TYPE$]", ORDER_BY_DESC);
			} else {
				sql = StringUtil.replace(sql, "[$ORDER_TYPE$]", ORDER_BY_ASC);
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(minPrice);
			qPos.add(maxPrice);
			qPos.add(companyId);
			qPos.add(visibility);
			qPos.add(curInventoryId);
			qPos.add(limit);
		} catch (ORMException e) {
			e.printStackTrace();
		} finally {
			closeSession(session);
		}
		return (List<ProductInventory>) query.list();
	}

	public void getCatalogIdsbyRelatedProduct(long companyId,
			long productInventoryId,
			LinkedHashMap<String, String> childCatalogMap) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_CATALOGIDSBY_RELATED_PRODUCT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("NAME", Type.STRING);
			query.addScalar("CATALOG_ID", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(StringPool.PERCENT + productInventoryId
					+ StringPool.PERCENT);
			qPos.add(companyId);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				if (!childCatalogMap.containsKey((String) array[0])) {
					childCatalogMap.put((String) array[0],
							String.valueOf(array[1]));
				} else {
					childCatalogMap.put(
							(String) array[0],
							childCatalogMap.get((String) array[0])
									+ StringPool.COMMA
									+ String.valueOf(array[1]));
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ProductInventory> getRelatedProduct(long companyId,String limitProducts, boolean visibility,
			boolean descOrder, int limit) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_RELATED_PRODUCTS);
			if (descOrder) {
				sql = StringUtil.replace(sql, "[$ORDER_TYPE$]", ORDER_BY_DESC);
			} else {
				sql = StringUtil.replace(sql, "[$ORDER_TYPE$]", ORDER_BY_ASC);
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addEntity("ProductInventory", ProductInventoryImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(StringPool.PERCENT+limitProducts+StringPool.PERCENT);
			qPos.add(companyId);
			qPos.add(visibility);
			qPos.add(limit);
		} catch (ORMException e) {
			e.printStackTrace();
		} finally {
			closeSession(session);
		}
		return (List<ProductInventory>) query.list();
	}

	private static Log _log = LogFactoryUtil
			.getLog(ProductDetailsFinderImpl.class);
	public static String GET_PRODUCTLIST = "getProductList";
	public static String GET_STOCK_COUNT = "getStockCount";
	public static String GET_PRICEBY_CATALOGID = "getPriceRangeByCatalogId";
	public static String GET_PRODUCTLISTBY_PRICE = "getProductListByPricePange";
	public static String GET_CATALOGIDSBY_CREATEDATE = "getCatalogIdsbyCreateDate";
	public static String GET_CATALOGIDSBY_DISCOUNT = "getCatalogIdsbyDiscount";
	public static String GET_RELATEDPRODUCTLIST = "getRelatedProductList";
	public static String GET_SHOWMORE_PRODUCTLIST = "getShowMoreProductList";
	public static String GET_CATALOGIDSBY_SEARCH = "getCatalogIdsbySearch";
	public static String GET_CATALOGIDSBY_SIMILAR_PRODUCT = "getCatalogIdsbySimilarProduct";
	public static String GET_SIMILAR_PRODUCTS = "getSimilarProducts";
	public static String GET_CATALOGIDSBY_RELATED_PRODUCT = "getCatalogIdsbyRelatedProduct";
	public static String GET_RELATED_PRODUCTS = "getRelatedProducts";

}