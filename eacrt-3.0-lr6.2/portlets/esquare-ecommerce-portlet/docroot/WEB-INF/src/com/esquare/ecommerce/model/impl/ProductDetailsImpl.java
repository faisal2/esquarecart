/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;

/**
 * The extended model implementation for the ProductDetails service. Represents a row in the &quot;EEC_ProductDetails&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.model.ProductDetails} interface.
 * </p>
 *
 * @author Esquare
 */
public class ProductDetailsImpl extends ProductDetailsBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a product details model instance should use the {@link com.esquare.ecommerce.model.ProductDetails} interface instead.
	 */
	public ProductDetailsImpl() {
	}
	public Catalog getCatalog() {
		Catalog catalog = null;

		if (getCatalogId() > 0) {
			try {
				catalog = CatalogLocalServiceUtil.getCatalog(
						getCatalogId());
			}
			catch (Exception e) {
				catalog = new CatalogImpl();

				catalog.setCompanyId(getCompanyId());

			}
		}
		else {
			catalog = new CatalogImpl();

			catalog.setCompanyId(getCompanyId());
		}

		return catalog;
	}
}