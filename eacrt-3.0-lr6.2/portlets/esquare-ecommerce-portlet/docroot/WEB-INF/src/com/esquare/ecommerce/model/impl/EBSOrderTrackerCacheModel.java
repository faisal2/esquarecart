/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.EBSOrderTracker;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EBSOrderTracker in entity cache.
 *
 * @author Esquare
 * @see EBSOrderTracker
 * @generated
 */
public class EBSOrderTrackerCacheModel implements CacheModel<EBSOrderTracker>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{ebsOrderTrackerId=");
		sb.append(ebsOrderTrackerId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", responseCode=");
		sb.append(responseCode);
		sb.append(", responseMessage=");
		sb.append(responseMessage);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", paymentId=");
		sb.append(paymentId);
		sb.append(", paymentMethod=");
		sb.append(paymentMethod);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", isFlagged=");
		sb.append(isFlagged);
		sb.append(", transactionId=");
		sb.append(transactionId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EBSOrderTracker toEntityModel() {
		EBSOrderTrackerImpl ebsOrderTrackerImpl = new EBSOrderTrackerImpl();

		ebsOrderTrackerImpl.setEbsOrderTrackerId(ebsOrderTrackerId);
		ebsOrderTrackerImpl.setCompanyId(companyId);
		ebsOrderTrackerImpl.setOrderId(orderId);
		ebsOrderTrackerImpl.setUserId(userId);
		ebsOrderTrackerImpl.setResponseCode(responseCode);

		if (responseMessage == null) {
			ebsOrderTrackerImpl.setResponseMessage(StringPool.BLANK);
		}
		else {
			ebsOrderTrackerImpl.setResponseMessage(responseMessage);
		}

		if (createDate == Long.MIN_VALUE) {
			ebsOrderTrackerImpl.setCreateDate(null);
		}
		else {
			ebsOrderTrackerImpl.setCreateDate(new Date(createDate));
		}

		ebsOrderTrackerImpl.setPaymentId(paymentId);
		ebsOrderTrackerImpl.setPaymentMethod(paymentMethod);

		if (amount == null) {
			ebsOrderTrackerImpl.setAmount(StringPool.BLANK);
		}
		else {
			ebsOrderTrackerImpl.setAmount(amount);
		}

		if (isFlagged == null) {
			ebsOrderTrackerImpl.setIsFlagged(StringPool.BLANK);
		}
		else {
			ebsOrderTrackerImpl.setIsFlagged(isFlagged);
		}

		ebsOrderTrackerImpl.setTransactionId(transactionId);

		ebsOrderTrackerImpl.resetOriginalValues();

		return ebsOrderTrackerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ebsOrderTrackerId = objectInput.readLong();
		companyId = objectInput.readLong();
		orderId = objectInput.readLong();
		userId = objectInput.readLong();
		responseCode = objectInput.readLong();
		responseMessage = objectInput.readUTF();
		createDate = objectInput.readLong();
		paymentId = objectInput.readLong();
		paymentMethod = objectInput.readLong();
		amount = objectInput.readUTF();
		isFlagged = objectInput.readUTF();
		transactionId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ebsOrderTrackerId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(orderId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(responseCode);

		if (responseMessage == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(responseMessage);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(paymentId);
		objectOutput.writeLong(paymentMethod);

		if (amount == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(amount);
		}

		if (isFlagged == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isFlagged);
		}

		objectOutput.writeLong(transactionId);
	}

	public long ebsOrderTrackerId;
	public long companyId;
	public long orderId;
	public long userId;
	public long responseCode;
	public String responseMessage;
	public long createDate;
	public long paymentId;
	public long paymentMethod;
	public String amount;
	public String isFlagged;
	public long transactionId;
}