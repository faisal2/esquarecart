/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchCartException;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.impl.CartImpl;
import com.esquare.ecommerce.model.impl.CartModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cart service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see CartPersistence
 * @see CartUtil
 * @generated
 */
public class CartPersistenceImpl extends BasePersistenceImpl<Cart>
	implements CartPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CartUtil} to access the cart persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CartImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, CartImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, CartImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_C_U = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, CartImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByC_U",
			new String[] { Long.class.getName(), Long.class.getName() },
			CartModelImpl.COMPANYID_COLUMN_BITMASK |
			CartModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_U = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_U",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the cart where companyId = &#63; and userId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchCartException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the matching cart
	 * @throws com.esquare.ecommerce.NoSuchCartException if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart findByC_U(long companyId, long userId)
		throws NoSuchCartException, SystemException {
		Cart cart = fetchByC_U(companyId, userId);

		if (cart == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCartException(msg.toString());
		}

		return cart;
	}

	/**
	 * Returns the cart where companyId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the matching cart, or <code>null</code> if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByC_U(long companyId, long userId)
		throws SystemException {
		return fetchByC_U(companyId, userId, true);
	}

	/**
	 * Returns the cart where companyId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cart, or <code>null</code> if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByC_U(long companyId, long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_U,
					finderArgs, this);
		}

		if (result instanceof Cart) {
			Cart cart = (Cart)result;

			if ((companyId != cart.getCompanyId()) ||
					(userId != cart.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CART_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				List<Cart> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"CartPersistenceImpl.fetchByC_U(long, long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					Cart cart = list.get(0);

					result = cart;

					cacheResult(cart);

					if ((cart.getCompanyId() != companyId) ||
							(cart.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
							finderArgs, cart);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Cart)result;
		}
	}

	/**
	 * Removes the cart where companyId = &#63; and userId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the cart that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart removeByC_U(long companyId, long userId)
		throws NoSuchCartException, SystemException {
		Cart cart = findByC_U(companyId, userId);

		return remove(cart);
	}

	/**
	 * Returns the number of carts where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the number of matching carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_U(long companyId, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_U;

		Object[] finderArgs = new Object[] { companyId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CART_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_U_COMPANYID_2 = "cart.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_U_USERID_2 = "cart.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_G_U = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, CartImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByG_U",
			new String[] { Long.class.getName(), Long.class.getName() },
			CartModelImpl.GROUPID_COLUMN_BITMASK |
			CartModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_G_U = new FinderPath(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByG_U",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the cart where groupId = &#63; and userId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchCartException} if it could not be found.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the matching cart
	 * @throws com.esquare.ecommerce.NoSuchCartException if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart findByG_U(long groupId, long userId)
		throws NoSuchCartException, SystemException {
		Cart cart = fetchByG_U(groupId, userId);

		if (cart == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("groupId=");
			msg.append(groupId);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCartException(msg.toString());
		}

		return cart;
	}

	/**
	 * Returns the cart where groupId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the matching cart, or <code>null</code> if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByG_U(long groupId, long userId) throws SystemException {
		return fetchByG_U(groupId, userId, true);
	}

	/**
	 * Returns the cart where groupId = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching cart, or <code>null</code> if a matching cart could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByG_U(long groupId, long userId, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { groupId, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_G_U,
					finderArgs, this);
		}

		if (result instanceof Cart) {
			Cart cart = (Cart)result;

			if ((groupId != cart.getGroupId()) || (userId != cart.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CART_WHERE);

			query.append(_FINDER_COLUMN_G_U_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				List<Cart> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
						finderArgs, list);
				}
				else {
					Cart cart = list.get(0);

					result = cart;

					cacheResult(cart);

					if ((cart.getGroupId() != groupId) ||
							(cart.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
							finderArgs, cart);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Cart)result;
		}
	}

	/**
	 * Removes the cart where groupId = &#63; and userId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the cart that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart removeByG_U(long groupId, long userId)
		throws NoSuchCartException, SystemException {
		Cart cart = findByG_U(groupId, userId);

		return remove(cart);
	}

	/**
	 * Returns the number of carts where groupId = &#63; and userId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param userId the user ID
	 * @return the number of matching carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByG_U(long groupId, long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_G_U;

		Object[] finderArgs = new Object[] { groupId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CART_WHERE);

			query.append(_FINDER_COLUMN_G_U_GROUPID_2);

			query.append(_FINDER_COLUMN_G_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_G_U_GROUPID_2 = "cart.groupId = ? AND ";
	private static final String _FINDER_COLUMN_G_U_USERID_2 = "cart.userId = ?";

	public CartPersistenceImpl() {
		setModelClass(Cart.class);
	}

	/**
	 * Caches the cart in the entity cache if it is enabled.
	 *
	 * @param cart the cart
	 */
	@Override
	public void cacheResult(Cart cart) {
		EntityCacheUtil.putResult(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartImpl.class, cart.getPrimaryKey(), cart);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U,
			new Object[] { cart.getCompanyId(), cart.getUserId() }, cart);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U,
			new Object[] { cart.getGroupId(), cart.getUserId() }, cart);

		cart.resetOriginalValues();
	}

	/**
	 * Caches the carts in the entity cache if it is enabled.
	 *
	 * @param carts the carts
	 */
	@Override
	public void cacheResult(List<Cart> carts) {
		for (Cart cart : carts) {
			if (EntityCacheUtil.getResult(CartModelImpl.ENTITY_CACHE_ENABLED,
						CartImpl.class, cart.getPrimaryKey()) == null) {
				cacheResult(cart);
			}
			else {
				cart.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all carts.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CartImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CartImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cart.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Cart cart) {
		EntityCacheUtil.removeResult(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartImpl.class, cart.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(cart);
	}

	@Override
	public void clearCache(List<Cart> carts) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Cart cart : carts) {
			EntityCacheUtil.removeResult(CartModelImpl.ENTITY_CACHE_ENABLED,
				CartImpl.class, cart.getPrimaryKey());

			clearUniqueFindersCache(cart);
		}
	}

	protected void cacheUniqueFindersCache(Cart cart) {
		if (cart.isNew()) {
			Object[] args = new Object[] { cart.getCompanyId(), cart.getUserId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_U, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U, args, cart);

			args = new Object[] { cart.getGroupId(), cart.getUserId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_G_U, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U, args, cart);
		}
		else {
			CartModelImpl cartModelImpl = (CartModelImpl)cart;

			if ((cartModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						cart.getCompanyId(), cart.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_U, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_U, args, cart);
			}

			if ((cartModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_G_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { cart.getGroupId(), cart.getUserId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_G_U, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_G_U, args, cart);
			}
		}
	}

	protected void clearUniqueFindersCache(Cart cart) {
		CartModelImpl cartModelImpl = (CartModelImpl)cart;

		Object[] args = new Object[] { cart.getCompanyId(), cart.getUserId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U, args);

		if ((cartModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_U.getColumnBitmask()) != 0) {
			args = new Object[] {
					cartModelImpl.getOriginalCompanyId(),
					cartModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_U, args);
		}

		args = new Object[] { cart.getGroupId(), cart.getUserId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U, args);

		if ((cartModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_G_U.getColumnBitmask()) != 0) {
			args = new Object[] {
					cartModelImpl.getOriginalGroupId(),
					cartModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_G_U, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_G_U, args);
		}
	}

	/**
	 * Creates a new cart with the primary key. Does not add the cart to the database.
	 *
	 * @param cartId the primary key for the new cart
	 * @return the new cart
	 */
	@Override
	public Cart create(long cartId) {
		Cart cart = new CartImpl();

		cart.setNew(true);
		cart.setPrimaryKey(cartId);

		return cart;
	}

	/**
	 * Removes the cart with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param cartId the primary key of the cart
	 * @return the cart that was removed
	 * @throws com.esquare.ecommerce.NoSuchCartException if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart remove(long cartId) throws NoSuchCartException, SystemException {
		return remove((Serializable)cartId);
	}

	/**
	 * Removes the cart with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cart
	 * @return the cart that was removed
	 * @throws com.esquare.ecommerce.NoSuchCartException if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart remove(Serializable primaryKey)
		throws NoSuchCartException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Cart cart = (Cart)session.get(CartImpl.class, primaryKey);

			if (cart == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCartException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(cart);
		}
		catch (NoSuchCartException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Cart removeImpl(Cart cart) throws SystemException {
		cart = toUnwrappedModel(cart);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(cart)) {
				cart = (Cart)session.get(CartImpl.class, cart.getPrimaryKeyObj());
			}

			if (cart != null) {
				session.delete(cart);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (cart != null) {
			clearCache(cart);
		}

		return cart;
	}

	@Override
	public Cart updateImpl(com.esquare.ecommerce.model.Cart cart)
		throws SystemException {
		cart = toUnwrappedModel(cart);

		boolean isNew = cart.isNew();

		Session session = null;

		try {
			session = openSession();

			if (cart.isNew()) {
				session.save(cart);

				cart.setNew(false);
			}
			else {
				session.merge(cart);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CartModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CartModelImpl.ENTITY_CACHE_ENABLED,
			CartImpl.class, cart.getPrimaryKey(), cart);

		clearUniqueFindersCache(cart);
		cacheUniqueFindersCache(cart);

		return cart;
	}

	protected Cart toUnwrappedModel(Cart cart) {
		if (cart instanceof CartImpl) {
			return cart;
		}

		CartImpl cartImpl = new CartImpl();

		cartImpl.setNew(cart.isNew());
		cartImpl.setPrimaryKey(cart.getPrimaryKey());

		cartImpl.setCartId(cart.getCartId());
		cartImpl.setGroupId(cart.getGroupId());
		cartImpl.setCompanyId(cart.getCompanyId());
		cartImpl.setUserId(cart.getUserId());
		cartImpl.setUserName(cart.getUserName());
		cartImpl.setCreateDate(cart.getCreateDate());
		cartImpl.setModifiedDate(cart.getModifiedDate());
		cartImpl.setProductInventoryIds(cart.getProductInventoryIds());
		cartImpl.setCouponCodes(cart.getCouponCodes());
		cartImpl.setWalletAmount(cart.getWalletAmount());
		cartImpl.setAltShipping(cart.getAltShipping());
		cartImpl.setInsure(cart.isInsure());

		return cartImpl;
	}

	/**
	 * Returns the cart with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cart
	 * @return the cart
	 * @throws com.esquare.ecommerce.NoSuchCartException if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCartException, SystemException {
		Cart cart = fetchByPrimaryKey(primaryKey);

		if (cart == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCartException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return cart;
	}

	/**
	 * Returns the cart with the primary key or throws a {@link com.esquare.ecommerce.NoSuchCartException} if it could not be found.
	 *
	 * @param cartId the primary key of the cart
	 * @return the cart
	 * @throws com.esquare.ecommerce.NoSuchCartException if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart findByPrimaryKey(long cartId)
		throws NoSuchCartException, SystemException {
		return findByPrimaryKey((Serializable)cartId);
	}

	/**
	 * Returns the cart with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cart
	 * @return the cart, or <code>null</code> if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Cart cart = (Cart)EntityCacheUtil.getResult(CartModelImpl.ENTITY_CACHE_ENABLED,
				CartImpl.class, primaryKey);

		if (cart == _nullCart) {
			return null;
		}

		if (cart == null) {
			Session session = null;

			try {
				session = openSession();

				cart = (Cart)session.get(CartImpl.class, primaryKey);

				if (cart != null) {
					cacheResult(cart);
				}
				else {
					EntityCacheUtil.putResult(CartModelImpl.ENTITY_CACHE_ENABLED,
						CartImpl.class, primaryKey, _nullCart);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CartModelImpl.ENTITY_CACHE_ENABLED,
					CartImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return cart;
	}

	/**
	 * Returns the cart with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param cartId the primary key of the cart
	 * @return the cart, or <code>null</code> if a cart with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Cart fetchByPrimaryKey(long cartId) throws SystemException {
		return fetchByPrimaryKey((Serializable)cartId);
	}

	/**
	 * Returns all the carts.
	 *
	 * @return the carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cart> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the carts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of carts
	 * @param end the upper bound of the range of carts (not inclusive)
	 * @return the range of carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cart> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the carts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CartModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of carts
	 * @param end the upper bound of the range of carts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Cart> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Cart> list = (List<Cart>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CART);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CART;

				if (pagination) {
					sql = sql.concat(CartModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Cart>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Cart>(list);
				}
				else {
					list = (List<Cart>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the carts from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Cart cart : findAll()) {
			remove(cart);
		}
	}

	/**
	 * Returns the number of carts.
	 *
	 * @return the number of carts
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CART);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the cart persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.Cart")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Cart>> listenersList = new ArrayList<ModelListener<Cart>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Cart>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CartImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CART = "SELECT cart FROM Cart cart";
	private static final String _SQL_SELECT_CART_WHERE = "SELECT cart FROM Cart cart WHERE ";
	private static final String _SQL_COUNT_CART = "SELECT COUNT(cart) FROM Cart cart";
	private static final String _SQL_COUNT_CART_WHERE = "SELECT COUNT(cart) FROM Cart cart WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "cart.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Cart exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Cart exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CartPersistenceImpl.class);
	private static Cart _nullCart = new CartImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Cart> toCacheModel() {
				return _nullCartCacheModel;
			}
		};

	private static CacheModel<Cart> _nullCartCacheModel = new CacheModel<Cart>() {
			@Override
			public Cart toEntityModel() {
				return _nullCart;
			}
		};
}