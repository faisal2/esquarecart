/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

/**
 * The extended model implementation for the TaxesSetting service. Represents a row in the &quot;EEC_TaxesSetting&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.model.TaxesSetting} interface.
 * </p>
 *
 * @author Esquare
 */
public class TaxesSettingImpl extends TaxesSettingBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a taxes setting model instance should use the {@link com.esquare.ecommerce.model.TaxesSetting} interface instead.
	 */
	public TaxesSettingImpl() {
	}
}