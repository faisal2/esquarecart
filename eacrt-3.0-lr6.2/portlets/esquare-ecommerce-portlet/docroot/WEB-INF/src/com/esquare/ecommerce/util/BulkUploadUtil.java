package com.esquare.ecommerce.util;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.esquare.ecommerce.NoSuchCatalogException;
import com.esquare.ecommerce.NoSuchProductImagesException;
import com.esquare.ecommerce.dashboard.util.DashboardUtil;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.EcustomField;
import com.esquare.ecommerce.model.EcustomFieldModel;
import com.esquare.ecommerce.model.EcustomFieldValue;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.ShippingRegion;
import com.esquare.ecommerce.model.bulkdataupload.ErrorPageModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadCatalogModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadCustomModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadInventoryModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadProductModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadTitleModel;
import com.esquare.ecommerce.model.impl.CatalogImpl;
import com.esquare.ecommerce.model.impl.EcustomFieldImpl;
import com.esquare.ecommerce.model.impl.EcustomFieldValueImpl;
import com.esquare.ecommerce.model.impl.ProductDetailsImpl;
import com.esquare.ecommerce.model.impl.ProductImagesImpl;
import com.esquare.ecommerce.model.impl.ProductInventoryImpl;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldValueLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductImagesLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

public class BulkUploadUtil {
	public static long parentCatalogId = 0;
	
	private static long parentfoldeId = 0;
	
	private static int lineNumber = 0;
	
	public static List<UploadCatalogModel> bulkUpload(File file, long companyId, ErrorPageModel errorPage) throws FileNotFoundException {
		  
		FileInputStream inputStream;
		Workbook workbook;
		List<UploadCatalogModel> catalogModels = new ArrayList<UploadCatalogModel>();
		try {
			inputStream = new FileInputStream(file);
			workbook = new  XSSFWorkbook(inputStream);
			Sheet nSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = nSheet.iterator();
			UploadCatalogModel uploadCatalogModel = null;
			Iterator<Cell> cellIterator = null;
			Row nextRow = null;
			while (iterator.hasNext()) {
				
				nextRow = iterator.next();
				lineNumber = nextRow.getRowNum();
				uploadCatalogModel = new UploadCatalogModel();
				if (nextRow.getCell(0).toString().equals("Header") && nextRow.getCell(1).toString().equalsIgnoreCase("Catalog")) {
					nextRow = iterator.next();
					lineNumber = nextRow.getRowNum();
					cellIterator = nextRow.cellIterator();
					cellIterator.next();
					List<String> catStrings = new ArrayList<String>();
					while (cellIterator.hasNext()) {
						catStrings.add(cellIterator.next().toString());	
					}
					uploadCatalogModel.setCatalogs(catStrings);
					nextRow = iterator.next();
					lineNumber = nextRow.getRowNum();
					cellIterator = nextRow.cellIterator();
					cellIterator.next();
					uploadCatalogModel.setShowNavigation((cellIterator.next().toString()).equalsIgnoreCase("Yes")? true: false);
					nextRow = iterator.next();
					lineNumber = nextRow.getRowNum();
					
				}
				if (nextRow.getCell(0).toString().equals("Header") && nextRow.getCell(1).toString().equalsIgnoreCase("Product")) {
					List<UploadProductModel> listUploadProductModel = new ArrayList<UploadProductModel>();
					nextRow = iterator.next();
					lineNumber = nextRow.getRowNum();
					UploadProductModel model = null;
					UploadInventoryModel inventoryModel = null;
					
					while(iterator.hasNext() && ((nextRow = iterator.next()).getCell(0)!=null) ){
						
						lineNumber = nextRow.getRowNum();
						model = new UploadProductModel();
						inventoryModel = new UploadInventoryModel();
						
						inventoryModel.setSKU(nextRow.getCell(0).getStringCellValue());
						model.setName(nextRow.getCell(1).getStringCellValue());
						model.setDescription(nextRow.getCell(2).getStringCellValue());
						inventoryModel.setImageURL(nextRow.getCell(3).getStringCellValue());
						inventoryModel.setPrice((long)nextRow.getCell(4).getNumericCellValue());
						inventoryModel.setDiscount((double)nextRow.getCell(5).getNumericCellValue());
						inventoryModel.setQuantity((long)(nextRow.getCell(6).getNumericCellValue()));
						inventoryModel.setTaxable(nextRow.getCell(7).getStringCellValue().equalsIgnoreCase("Yes")?true:false);
						inventoryModel.setFreeShipping(nextRow.getCell(8).getStringCellValue().equalsIgnoreCase("Yes")?true:false);
						inventoryModel.setWeightage((nextRow.getCell(9)!=null?nextRow.getCell(9).getNumericCellValue():0.0));
						inventoryModel.setOutOfStockPurchase(nextRow.getCell(10).getStringCellValue().equalsIgnoreCase("Yes")?true:false);
						model.setVisibility(nextRow.getCell(11).getStringCellValue().equalsIgnoreCase("Yes")?true:false);
						model.setRelatedProducts((nextRow.getCell(12)!=null)?nextRow.getCell(12).getStringCellValue():"");
						model.setUploadInventoryModel(inventoryModel);
						listUploadProductModel.add(model);
					}
					uploadCatalogModel.setUploadProductModel(listUploadProductModel);
				}
					catalogModels.add(uploadCatalogModel);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}catch(Exception e){
			 errorPage.setError(true);
			 errorPage.setErrorMessage(String.valueOf(e.getMessage()+" in Line number :"+(lineNumber+1)));
		}	
		
		return catalogModels;	
	}
	
	

	public static void uplodaProduct(List<UploadCatalogModel> listUploadCatalogModel, ThemeDisplay themeDisplay, ServiceContext context, HashMap<String ,File> map, ErrorPageModel errorPage) throws SystemException, NoSuchProductImagesException, NoSuchCatalogException {
		
		for (UploadCatalogModel uploadCatalogModel : listUploadCatalogModel) {
			parentCatalogId = 0;
			parentfoldeId = 0;		
			creatCatalog(uploadCatalogModel, themeDisplay, context, errorPage);
			addProducts(uploadCatalogModel, themeDisplay, context, map, errorPage);
		}
	}
	
	private static boolean creatCatalog(UploadCatalogModel uploadCatalogModel, ThemeDisplay themeDisplay, ServiceContext context, ErrorPageModel errorPage) throws NoSuchCatalogException {
		boolean isExceed = false;
		for (String catalogName : uploadCatalogModel.getCatalogs()) {
			try {
				Catalog ct = CatalogLocalServiceUtil.findByNameCompanyIdParentId(catalogName, themeDisplay.getCompanyId(), parentCatalogId);
				if(Validator.isNull(ct)){
					isExceed = productCatalogValidation(true, themeDisplay.getCompanyId(), errorPage);
					if (isExceed) {
						break;
					}
					Catalog catal = new CatalogImpl();
					catal.setCatalogId(CounterLocalServiceUtil.increment());
					catal.setName(catalogName);
					catal.setCompanyId(themeDisplay.getCompanyId());
					catal.setGroupId(themeDisplay.getScopeGroupId());
					catal.setUserId(themeDisplay.getUserId());
					catal.setCreateDate(new Date());
					catal.setModifiedDate(new Date());
					catal.setShowNavigation(uploadCatalogModel.getShowNavigation());
					catal.setParentCatalogId(parentCatalogId);
					
					try {
						createFolder(catalogName, themeDisplay, context);
					} catch (PortalException e) {
						e.printStackTrace();
					}
					catal.setFolderId(parentfoldeId);
					
					CatalogLocalServiceUtil.addCatalog(catal);
					parentCatalogId = catal.getCatalogId();
				} else {
					parentCatalogId = ct.getCatalogId();
					parentfoldeId = ct.getFolderId();
				}
			} catch (SystemException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	 public static List<ProductDetails> addProducts(UploadCatalogModel catalogModel, ThemeDisplay themeDisplay, ServiceContext serviceContext, HashMap<String ,File> map, ErrorPageModel errorPage) throws SystemException, NoSuchProductImagesException{
		 List<ProductDetails> products = new ArrayList<ProductDetails>();
		 ProductInventory productInventory = null;
		 long count = 0l;
		 long productInventryId = 0l;
		 boolean isExceed = false;
		 for(UploadProductModel product :catalogModel.getUploadProductModel()){
			 ProductDetails pro = new ProductDetailsImpl();
			 productInventory = ProductInventoryLocalServiceUtil.fetchBySKU(themeDisplay.getCompanyId(), product.getUploadInventoryModel().getSKU());
			 pro.setCatalogId(parentCatalogId);
			 pro.setName(product.getName());
			 pro.setDescription(product.getDescription());
			 pro.setRelatedProductIds(product.getRelatedProducts());
			 pro.setCompanyId(themeDisplay.getCompanyId());
			 pro.setUserId(themeDisplay.getUserId());
			 pro.setCreateDate(new Date());
			 pro.setModifiedDate(new Date());
			 pro.setVisibility(product.isVisibility());
			 if (productInventory!=null) {
				 count = productInventory.getProductDetailsId();
				 productInventryId = productInventory.getInventoryId();
				 pro.setProductDetailsId(count);
				 pro = ProductDetailsLocalServiceUtil.updateProductDetails(pro);
			 } else {
				 count = CounterLocalServiceUtil.increment(ProductDetails.class.getName());
				 pro.setProductDetailsId(count);
				 isExceed = productCatalogValidation(false, themeDisplay.getCompanyId(), errorPage);
				 if (isExceed) {
					 break;
				 }
				 pro = ProductDetailsLocalServiceUtil.addProductDetails(pro);
			 }
			 products.add(pro);
			 
			 addInventories(productInventryId, product.getUploadInventoryModel(), pro.getProductDetailsId(), themeDisplay.getCompanyId(), product.isVisibility(), themeDisplay, serviceContext, map, errorPage);
		 }
		return products;
	 }
	 
	 public static boolean productCatalogValidation(boolean isCatalog, long companyId, ErrorPageModel errorPage){
			
			Instance instance = null;
			try {
				instance = InstanceLocalServiceUtil.getInstance(companyId);
			} catch (PortalException e) {
				e.printStackTrace();
			} catch (SystemException e) {
				e.printStackTrace();
			}
			if (isCatalog) {
				int catCount = 0;
				int catalogPackageCount = Integer.valueOf(DashboardUtil.getPackageValue(instance.getPackageType(), EECConstants.CATALOG_COUNT));
				try {
					catCount = CatalogLocalServiceUtil.getCatalogsCount(companyId, 0);
				} catch (SystemException e1) {
					e1.printStackTrace();
				}
				if (catCount >= catalogPackageCount) {
					errorPage.setError(true);
					errorPage.setErrorMessage("Your Current "+ instance.getPackageType() +" "+ PortletPropsValues.CATALOG_ERROR_MESSAGE+catalogPackageCount +" For catalog Please Upgrade Your Package !");
					return true;
				}
			} else {
				int proCount = 0;
				int productPackageCount = Integer.valueOf(DashboardUtil.getPackageValue(instance.getPackageType(), EECConstants.PRODUCT_COUNT));
				try {
					proCount = ProductDetailsLocalServiceUtil.getProductDetailsesCount();
				} catch (SystemException e) {
					e.printStackTrace();
				}
				if (proCount >= productPackageCount) {
					errorPage.setError(true);
					errorPage.setErrorMessage("Your current "+ instance.getPackageType() +" "+PortletPropsValues.PRODUCT_ERROR_MESSAGE+ productPackageCount +" For Product");
					return true;
				}
			}
			
			return false;
		}
	 public static void createProductImage(String imageURL,long inventoryId, ThemeDisplay themeDisplay, ServiceContext serviceContext,HashMap<String ,File> map, ErrorPageModel errorPage) throws NoSuchProductImagesException{
			if (!imageURL.isEmpty()) {
				File imageFile = map.get(imageURL);
				if (Validator.isNotNull(imageFile) && imageFile.getName() != null) {
					String imageFileName = imageFile.getName() + "_" + inventoryId;
					InputStream inputStream;
					long count = 0l;
					ProductImages productimage = null;
					try {
						List<ProductImages> productImages = ProductImagesLocalServiceUtil
								.findByInventoryId(inventoryId);
						productimage = !productImages.isEmpty() ? productImages
								.get(0) : null;
					} catch (SystemException e1) {
						e1.printStackTrace();
					}
	
					try {
						inputStream = new FileInputStream(imageFile);
						FileEntry dlFileEntry = null;
						if (productimage != null && productimage.getImageId() > 0) {
							dlFileEntry = DLAppServiceUtil.updateFileEntry(
									productimage.getImageId(), imageFileName,
									MimeTypesUtil.getContentType(imageFile),
									imageFileName, imageFileName, StringPool.BLANK,
									false, inputStream, imageFile.length(),
									serviceContext);
							count = productimage.getRecId();
						} else {
							dlFileEntry = DLAppServiceUtil
									.addFileEntry(
											themeDisplay.getScopeGroupId(),
											parentfoldeId,
											imageFileName,
											MimeTypesUtil.getContentType(imageFile),
											imageFileName, imageFileName,
											StringPool.BLANK, inputStream,
											imageFile.length(), serviceContext);
							count = CounterLocalServiceUtil
									.increment(ProductImages.class.getName());
	
							ProductImages productImages = new ProductImagesImpl();
							productImages.setRecId(count);
							productImages.setInventoryId(inventoryId);
							productImages.setImageId(dlFileEntry.getFileEntryId());
							ProductImagesLocalServiceUtil
									.addProductImages(productImages);
						}
					} catch (FileNotFoundException e) {
					} catch (PortalException e) {
					} catch (SystemException e) {
					}
				} else {
					errorPage.setError(true);
					errorPage.setErrorMessage("Image URL Is Not Matching With Excel Record");
				}
		 }
	 }
	 
	 public static List<ProductInventory> addInventories(long inventoryId, UploadInventoryModel inventory,long productDetailsId, long companyId, boolean visibility, ThemeDisplay themeDisplay, ServiceContext serviceContext, HashMap<String ,File> map, ErrorPageModel errorPage) throws SystemException, NoSuchProductImagesException{
		 List<ProductInventory> inventories = new ArrayList<ProductInventory>();
			 ProductInventory in = new ProductInventoryImpl();
			 
			 in.setProductDetailsId(productDetailsId);
			 in.setSku(inventory.getSKU());
			 in.setFreeShipping(inventory.isFreeShipping());
			 in.setOutOfStockPurchase(inventory.isOutOfStockPurchase());
			 in.setTaxes(inventory.isTaxable());
			 in.setPrice(inventory.getPrice());
			 in.setDiscount(inventory.getDiscount());
			 in.setQuantity(inventory.getQuantity());
			 in.setWeight(inventory.getWeightage());
			 in.setCompanyId(companyId);
			 in.setCreateDate(new Date());
			 in.setModifiedDate(new Date());
			 in.setVisibility(true);
			 if (inventoryId<=0) {
				 inventoryId = CounterLocalServiceUtil.increment(ProductInventory.class.getName());
				 in.setInventoryId(inventoryId);
				 in = ProductInventoryLocalServiceUtil.addProductInventory(in);
			 } else {
				 in.setInventoryId(inventoryId);
				 in = ProductInventoryLocalServiceUtil.updateProductInventory(in);
			 }
			 inventories.add(in);
			 
			createProductImage(inventory.getImageURL(), in.getInventoryId(), themeDisplay, serviceContext, map, errorPage); 
		 return inventories;
	 }


	private static void createFolder(String folderName, ThemeDisplay themeDisplay, ServiceContext context) throws PortalException, SystemException {
		
		StringBundler stringBundler = new StringBundler();
		stringBundler.append(CharPool.OPEN_BRACKET).append(CharPool.AMPERSAND)
				.append(CharPool.APOSTROPHE).append(CharPool.AT)
				.append(CharPool.BACK_SLASH).append(CharPool.CLOSE_CURLY_BRACE)
				.append(CharPool.COLON).append(CharPool.COMMA)
				.append(CharPool.EQUAL).append(CharPool.GREATER_THAN)
				.append(CharPool.FORWARD_SLASH).append(CharPool.LESS_THAN)
				.append(CharPool.OPEN_CURLY_BRACE)
				.append(CharPool.PERCENT).append(CharPool.PIPE)
				.append(CharPool.PLUS).append(CharPool.POUND)
				.append(CharPool.QUESTION).append(CharPool.QUOTE)
				.append(CharPool.SEMICOLON)
				.append(CharPool.SLASH).append(CharPool.STAR)
				.append(CharPool.TILDE).append(CharPool.CLOSE_BRACKET);
		String name = folderName.replaceAll(stringBundler.toString(), "_");

		if (parentfoldeId <= 0) {
			Folder catalogFolder = null;
			try {
				catalogFolder = DLAppServiceUtil.getFolder(
						themeDisplay.getScopeGroupId(), 0L,
						PortletPropsValues.ECOMMERCE_CATALOGS);
			} catch (Exception e) {
				catalogFolder = DLAppServiceUtil.addFolder(
						themeDisplay.getScopeGroupId(), 0l,
						PortletPropsValues.ECOMMERCE_CATALOGS,
						PortletPropsValues.ECOMMERCE_CATALOGS, context);
			}
			Folder parentFolder = null;
			Company company = null;
			try {
				company = CompanyLocalServiceUtil.getCompany(themeDisplay
						.getCompanyId());
				parentFolder = DLAppServiceUtil.getFolder(
						themeDisplay.getScopeGroupId(),
						catalogFolder.getFolderId(), company.getWebId());
			} catch (Exception e) {
				parentFolder = DLAppServiceUtil.addFolder(
						themeDisplay.getScopeGroupId(),
						catalogFolder.getFolderId(), company.getWebId(),
						company.getWebId(), context);
			}
			parentfoldeId = parentFolder.getFolderId();
		} 
		Folder folder = DLAppServiceUtil.addFolder(themeDisplay.getScopeGroupId(), parentfoldeId, name, name, context);
		parentfoldeId = folder.getFolderId();
	}
	
	//Reading Sheet two for Custom fields
	public static List<UploadCustomModel> bulkCustomFeildUpload(File file) throws IOException{
		List<UploadCustomModel> customModels = new ArrayList<UploadCustomModel>();
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workBook = new  XSSFWorkbook(inputStream);
		Sheet nSheet = workBook.getSheetAt(1);
		Iterator<Row> rowIterator = nSheet.iterator();
		UploadCustomModel uploadCustomModels = null;
		Row nextRow = null;
		
		while(rowIterator.hasNext()){
			uploadCustomModels = new UploadCustomModel();
			nextRow = rowIterator.next();
			if(nextRow.getCell(0)!=null && nextRow.getCell(0).toString().equals("SKU No")){
					uploadCustomModels.setSkuNo(nextRow.getCell(1).toString());
				nextRow = rowIterator.next();
				if (nextRow.getCell(0).toString().equals("Custom Field Name")) {
					uploadCustomModels.setCustomFieldName(nextRow.getCell(1).toString());
				}
				nextRow = rowIterator.next();
				if (nextRow.getCell(0).toString().equals("Title") && nextRow.getCell(1).toString().equalsIgnoreCase("Description")) {
					
					List<UploadTitleModel> uploadTitleModelList = new ArrayList<UploadTitleModel>();
					UploadTitleModel uploadTitleModel = null;
					while(rowIterator.hasNext() && ((nextRow = rowIterator.next()).getCell(0)!=null)){
						uploadTitleModel = new UploadTitleModel();
						uploadTitleModel.setTitle(nextRow.getCell(0).toString());
						uploadTitleModel.setDescription(nextRow.getCell(1).toString());
						uploadTitleModelList.add(uploadTitleModel);
					}
					uploadCustomModels.setUploadTitleModel(uploadTitleModelList);
				}
			}
				customModels.add(uploadCustomModels);
		}
		return customModels;
	}
	
	public static void uploadCustomField(List<UploadCustomModel> listUploadCutomModel, long companyId) throws SystemException {
		for (UploadCustomModel uploadCustomModel : listUploadCutomModel) {
			if(uploadCustomModel==null || uploadCustomModel.getUploadTitleModel()==null) {
				continue;
			}
			EcustomField eCustomField = null;
			try {
				eCustomField = EcustomFieldLocalServiceUtil.findByTitle(companyId, uploadCustomModel.getCustomFieldName());
			} catch (Exception e) {
				e.printStackTrace();
			}
			long recId = 0l; 
			EcustomField entityImpl = new EcustomFieldImpl();
			entityImpl.setTitle(uploadCustomModel.getCustomFieldName());
			entityImpl.setCompanyId(companyId);
			String xml = StringPool.BLANK;
			
			// Add/Update Custom Field Table
			
			if (eCustomField!=null) {
				entityImpl.setCustomFieldRecId(eCustomField.getCustomFieldRecId());
				xml = getXmlContent(uploadCustomModel.getUploadTitleModel(), eCustomField.getXml());
				entityImpl.setXml(xml);
				eCustomField = EcustomFieldLocalServiceUtil.updateEcustomField(entityImpl);
			} else {
				recId = CounterLocalServiceUtil.increment(EcustomField.class.getName());
				entityImpl.setCustomFieldRecId(recId);
				xml = getXmlContent(uploadCustomModel.getUploadTitleModel(), xml);
				entityImpl.setXml(xml);
				eCustomField = EcustomFieldLocalServiceUtil.addEcustomField(entityImpl);
			}
			
			// Add/Update Custom Field Value Table.
			ProductInventory invontory = ProductInventoryLocalServiceUtil.fetchBySKU(companyId, uploadCustomModel.getSkuNo());
			
			if (Validator.isNull(invontory)) {
				continue;
			}
			EcustomFieldValue customValue = EcustomFieldValueLocalServiceUtil.findByInventoryId(companyId, invontory.getInventoryId());
			
			String formFieldsIndexes = StringPool.BLANK;
			StringBuffer sb_KeyName = new StringBuffer();
			StringBuffer sb_KeyValue = new StringBuffer();
			formFieldsIndexes = setXML(uploadCustomModel.getUploadTitleModel(), "", sb_KeyName, sb_KeyValue, formFieldsIndexes);
			String[] fileds = formFieldsIndexes.split(",");
			formFieldsIndexes = formFieldsIndexes + ",";
			String xmlContent = StringPool.BLANK;
			try {
				 xmlContent = EECUtil.addXmlContent(StringPool.BLANK,
						PortletPropsValues.CUSTOM_FIELD_TAG,
						PortletPropsValues.ID_ATTRIBUTE, fileds, formFieldsIndexes.toString()
								.split(","), sb_KeyValue.toString().split(","));
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
			
			if (Validator.isNull(customValue)) {
				 customValue = new EcustomFieldValueImpl();
				 customValue.setCustomValueRecId(CounterLocalServiceUtil.increment(EcustomFieldValue.class.getName()));
				 customValue.setInventoryId(invontory.getInventoryId());
				 customValue.setCompanyId(companyId);
				 customValue.setXml(xmlContent);
				 EcustomFieldValueLocalServiceUtil.addEcustomFieldValue(customValue);
			} else {
				customValue.setXml(xmlContent);
				EcustomFieldValueLocalServiceUtil.updateEcustomFieldValue(customValue);
			}
			
			invontory.setCustomFieldRecId(((EcustomFieldModel) eCustomField).getCustomFieldRecId());
			ProductInventoryLocalServiceUtil.updateProductInventory(invontory);
		}
	}

	private static String getXmlContent(List<UploadTitleModel> uploadTitleModel, String xml) {
		String xmlContent = StringPool.BLANK;
		String formFieldsIndexes = StringPool.BLANK;
		
		StringBuffer sb_KeyName = new StringBuffer();
		StringBuffer sb_KeyType = new StringBuffer();
		try {
			if (!xml.isEmpty()) {
				List<ShippingRegion> shipRegionlist = EECUtil.readXMLContent(xml, PortletPropsValues.CUSTOM_FIELD_TAG);
				updatedUploadTitleModel(uploadTitleModel, shipRegionlist);
			}
			
			formFieldsIndexes = setXML(uploadTitleModel, "text", sb_KeyName, sb_KeyType, formFieldsIndexes);
			
			String[] fileds = formFieldsIndexes.split(",");
			xmlContent = EECUtil.addXmlContent(StringPool.BLANK,
					PortletPropsValues.CUSTOM_FIELD_TAG,
					PortletPropsValues.ID_ATTRIBUTE, fileds, sb_KeyName.toString()
							.split(","), sb_KeyType.toString().split(","));
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	    return xmlContent;
	}
	
	private static List<UploadTitleModel> updatedUploadTitleModel(List<UploadTitleModel> uploadTitleModel, List<ShippingRegion> shipRegionlist) {
		HashMap<String, UploadTitleModel> map = new HashMap<String, UploadTitleModel>();
		for (UploadTitleModel title: uploadTitleModel) {
			map.put(title.getTitle(), title);
		}
		
		UploadTitleModel model = null;
		for (ShippingRegion obj : shipRegionlist) {
			model = map.get(obj.getName());
			if (Validator.isNull(model)) {
				model = new UploadTitleModel();
				model.setTitle(obj.getName());
				uploadTitleModel.add(model);
			}
		}
		return uploadTitleModel;
	}
	
	private static String setXML(List<UploadTitleModel> uploadTitleModel, String fieldType, StringBuffer key, StringBuffer type, String formFieldsIndexes) {
		UploadTitleModel title = null;
		for (int i =0; i<uploadTitleModel.size(); i++) {
			title = uploadTitleModel.get(i);
			key.append(title.getTitle());
			key.append(",");
			if (fieldType.equals(StringPool.BLANK)) {
				type.append(title.getDescription());
			} else {
				type.append(fieldType);
			}
			type.append(",");
			formFieldsIndexes += (i+1)+((i==(uploadTitleModel.size()-1)) ? "":",");
		}
		return formFieldsIndexes;
	}
}		