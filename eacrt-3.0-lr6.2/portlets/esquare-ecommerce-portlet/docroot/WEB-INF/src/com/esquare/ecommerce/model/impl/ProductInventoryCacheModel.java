/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.ProductInventory;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProductInventory in entity cache.
 *
 * @author Esquare
 * @see ProductInventory
 * @generated
 */
public class ProductInventoryCacheModel implements CacheModel<ProductInventory>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{inventoryId=");
		sb.append(inventoryId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", productDetailsId=");
		sb.append(productDetailsId);
		sb.append(", customFieldRecId=");
		sb.append(customFieldRecId);
		sb.append(", sku=");
		sb.append(sku);
		sb.append(", price=");
		sb.append(price);
		sb.append(", discount=");
		sb.append(discount);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", barcode=");
		sb.append(barcode);
		sb.append(", taxes=");
		sb.append(taxes);
		sb.append(", freeShipping=");
		sb.append(freeShipping);
		sb.append(", weight=");
		sb.append(weight);
		sb.append(", shippingFormula=");
		sb.append(shippingFormula);
		sb.append(", outOfStockPurchase=");
		sb.append(outOfStockPurchase);
		sb.append(", similarProducts=");
		sb.append(similarProducts);
		sb.append(", limitCatalogs=");
		sb.append(limitCatalogs);
		sb.append(", limitProducts=");
		sb.append(limitProducts);
		sb.append(", visibility=");
		sb.append(visibility);
		sb.append(", column1=");
		sb.append(column1);
		sb.append(", column2=");
		sb.append(column2);
		sb.append(", column3=");
		sb.append(column3);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProductInventory toEntityModel() {
		ProductInventoryImpl productInventoryImpl = new ProductInventoryImpl();

		productInventoryImpl.setInventoryId(inventoryId);
		productInventoryImpl.setCompanyId(companyId);

		if (createDate == Long.MIN_VALUE) {
			productInventoryImpl.setCreateDate(null);
		}
		else {
			productInventoryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			productInventoryImpl.setModifiedDate(null);
		}
		else {
			productInventoryImpl.setModifiedDate(new Date(modifiedDate));
		}

		productInventoryImpl.setProductDetailsId(productDetailsId);
		productInventoryImpl.setCustomFieldRecId(customFieldRecId);

		if (sku == null) {
			productInventoryImpl.setSku(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setSku(sku);
		}

		productInventoryImpl.setPrice(price);
		productInventoryImpl.setDiscount(discount);
		productInventoryImpl.setQuantity(quantity);

		if (barcode == null) {
			productInventoryImpl.setBarcode(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setBarcode(barcode);
		}

		productInventoryImpl.setTaxes(taxes);
		productInventoryImpl.setFreeShipping(freeShipping);
		productInventoryImpl.setWeight(weight);

		if (shippingFormula == null) {
			productInventoryImpl.setShippingFormula(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setShippingFormula(shippingFormula);
		}

		productInventoryImpl.setOutOfStockPurchase(outOfStockPurchase);
		productInventoryImpl.setSimilarProducts(similarProducts);

		if (limitCatalogs == null) {
			productInventoryImpl.setLimitCatalogs(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setLimitCatalogs(limitCatalogs);
		}

		if (limitProducts == null) {
			productInventoryImpl.setLimitProducts(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setLimitProducts(limitProducts);
		}

		productInventoryImpl.setVisibility(visibility);

		if (column1 == null) {
			productInventoryImpl.setColumn1(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setColumn1(column1);
		}

		if (column2 == null) {
			productInventoryImpl.setColumn2(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setColumn2(column2);
		}

		if (column3 == null) {
			productInventoryImpl.setColumn3(StringPool.BLANK);
		}
		else {
			productInventoryImpl.setColumn3(column3);
		}

		productInventoryImpl.resetOriginalValues();

		return productInventoryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		inventoryId = objectInput.readLong();
		companyId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		productDetailsId = objectInput.readLong();
		customFieldRecId = objectInput.readLong();
		sku = objectInput.readUTF();
		price = objectInput.readDouble();
		discount = objectInput.readDouble();
		quantity = objectInput.readLong();
		barcode = objectInput.readUTF();
		taxes = objectInput.readBoolean();
		freeShipping = objectInput.readBoolean();
		weight = objectInput.readDouble();
		shippingFormula = objectInput.readUTF();
		outOfStockPurchase = objectInput.readBoolean();
		similarProducts = objectInput.readBoolean();
		limitCatalogs = objectInput.readUTF();
		limitProducts = objectInput.readUTF();
		visibility = objectInput.readBoolean();
		column1 = objectInput.readUTF();
		column2 = objectInput.readUTF();
		column3 = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(inventoryId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(productDetailsId);
		objectOutput.writeLong(customFieldRecId);

		if (sku == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sku);
		}

		objectOutput.writeDouble(price);
		objectOutput.writeDouble(discount);
		objectOutput.writeLong(quantity);

		if (barcode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(barcode);
		}

		objectOutput.writeBoolean(taxes);
		objectOutput.writeBoolean(freeShipping);
		objectOutput.writeDouble(weight);

		if (shippingFormula == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingFormula);
		}

		objectOutput.writeBoolean(outOfStockPurchase);
		objectOutput.writeBoolean(similarProducts);

		if (limitCatalogs == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(limitCatalogs);
		}

		if (limitProducts == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(limitProducts);
		}

		objectOutput.writeBoolean(visibility);

		if (column1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(column1);
		}

		if (column2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(column2);
		}

		if (column3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(column3);
		}
	}

	public long inventoryId;
	public long companyId;
	public long createDate;
	public long modifiedDate;
	public long productDetailsId;
	public long customFieldRecId;
	public String sku;
	public double price;
	public double discount;
	public long quantity;
	public String barcode;
	public boolean taxes;
	public boolean freeShipping;
	public double weight;
	public String shippingFormula;
	public boolean outOfStockPurchase;
	public boolean similarProducts;
	public String limitCatalogs;
	public String limitProducts;
	public boolean visibility;
	public String column1;
	public String column2;
	public String column3;
}