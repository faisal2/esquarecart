package com.esquare.ecommerce.contentdisplay.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.dynamicdatamapping.model.DDMTemplate;
import com.liferay.portlet.dynamicdatamapping.model.DDMTemplateConstants;
import com.liferay.portlet.dynamicdatamapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ContentDisplayAction extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		try {
			updateContent(actionRequest);
		} catch (Exception e) {
			_log.info(e.getClass());
		}

		super.processAction(actionRequest, actionResponse);
	}

	private void updateContent(ActionRequest actionRequest)
			throws PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		Group group = GroupLocalServiceUtil.getGroup(
				themeDisplay.getCompanyId(), EECConstants.SHOP_ADMINNAME);
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		String editorContent = ParamUtil.getString(actionRequest,
				"editorContent");
		String templateId = ParamUtil.getString(actionRequest, "templateId");
		Map<Locale, String> nameMap = new HashMap<Locale, String>();
		nameMap.put(themeDisplay.getLocale(),
				ParamUtil.getString(actionRequest, "name"));
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				DDMTemplate.class.getName(), actionRequest);
		if (cmd.equals(EECConstants.ADD)) {

			// Add template
			
//			check here

			DDMTemplateLocalServiceUtil.addTemplate(themeDisplay.getUserId(),
					group.getGroupId(), group.getClassNameId(),
					group.getClassPK(), nameMap, null,
					DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
					DDMTemplateConstants.TEMPLATE_MODE_CREATE, "vm",
					editorContent, serviceContext);
		} else {

			// Update template

			DDMTemplateLocalServiceUtil.updateTemplate(
					Long.valueOf(templateId), group.getClassPK(), nameMap,
					null, DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY,
					DDMTemplateConstants.TEMPLATE_MODE_EDIT, "vm",
					editorContent, true, false, null, null, serviceContext);
		}
	}

	private static Log _log = LogFactoryUtil.getLog(ContentDisplayAction.class);
}
