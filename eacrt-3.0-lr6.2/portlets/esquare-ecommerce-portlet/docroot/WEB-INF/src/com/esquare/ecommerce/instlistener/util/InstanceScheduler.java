package com.esquare.ecommerce.instlistener.util;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.mail.internet.InternetAddress;

import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.userview.util.UserViewUtil;
import com.esquare.ecommerce.util.DNSSettings;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class InstanceScheduler implements MessageListener {

	public String fromName = PortletPropsValues.ADMIN_EMAIL_FROM_NAME;

	public String fromAddress = PortletPropsValues.ADMIN_EMAIL_FROM_ADDRESS;

	public long groupId = PortletPropsValues.GROUP_ID;

	public void receive(Message arg0) throws MessageListenerException {

		callScheduler(PortletPropsValues.PACKAGE_TYPE_FREE);
		callScheduler(StringPool.BLANK);

		/*
		 * callScheduler(PortletPropsValues.PACKAGE_TYPE_BASIC); int start = 15;
		 * int end = -10; int intervel = 2; for (int i = start; i > end; i--) {
		 * i = i - intervel + 1; }
		 */

	}

	public void callScheduler(String packageType) {

		String beforAlertBody = PortletPropsValues.BEFORE_EXPIRE_ALERT_BODY;
		String afterAlertBody = PortletPropsValues.AFTER_EXPIRE_ALERT_BODY;
		String expireAlertBody = PortletPropsValues.EXPIRE_ALERT_BODY;
		String deleteAlertBody = PortletPropsValues.DELETE_ALERT_BODY;

		String beforAlertSubject = PortletPropsValues.BEFORE_EXPIRE_ALERT_SUBJECT;
		String afterAlertSubject = PortletPropsValues.AFTER_EXPIRE_ALERT_SUBJECT;
		String expireAlertSubject = PortletPropsValues.EXPIRE_ALERT_SUBJECT;
		String deleteAlertSubject = PortletPropsValues.DELETE_ALERT_SUBJECT;

		int start, end, intervel;

		if (packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_FREE)) {
			start = EECConstants.FREE_BEGIN_ALERT;
			end = -EECConstants.FREE_STORE_CLOSED_ALERT;
			intervel = EECConstants.FREE_ALERT_INTERVEL;
		} else {
			start = EECConstants.PACKAGE_BEGIN_ALERT;
			end = -EECConstants.PACKAGE_STORE_CLOSED_ALERT;
			intervel = EECConstants.PACKAGE_ALERT_INTERVEL;
		}

		beforAlertBody = UserViewUtil.getTemplate(groupId, beforAlertBody);
		afterAlertBody = UserViewUtil.getTemplate(groupId, afterAlertBody);
		expireAlertBody = UserViewUtil.getTemplate(groupId, expireAlertBody);
		deleteAlertBody = UserViewUtil.getTemplate(groupId, deleteAlertBody);

		String subject = StringPool.BLANK;
		String bodyTemplate = StringPool.BLANK;

		for (int i = start; i > end; i--) {

			if (i == 0)
				continue;

			if (i > 0) {
				// Before expire template
				bodyTemplate = beforAlertBody;
				subject = beforAlertSubject;
			} else {
				// After expire template
				bodyTemplate = afterAlertBody;
				subject = afterAlertSubject;
			}

			sendAlertMessage(i, packageType, subject, bodyTemplate);

			i = i - intervel + 1;
		}

		try {
			expireStore(packageType, expireAlertBody, expireAlertSubject);
			// deleteStore(packageType, deleteAlertBody, deleteAlertSubject);
		} catch (UnsupportedEncodingException e) {
			_log.info(e.getClass());
		}
	}

	public void expireStore(String packageType, String bodyTemplate,
			String subject) throws UnsupportedEncodingException {

		int dayCount = 0;

		try {
			List<Instance> instance = InstanceLocalServiceUtil
					.getExpPackageList(dayCount, packageType);

			for (Instance inst : instance) {
				Company company = CompanyLocalServiceUtil.getCompany(inst
						.getCompanyId());
				company.setActive(false);
				CompanyLocalServiceUtil.updateCompany(company);
				sendMail(inst.getInstanceUserId(), subject, bodyTemplate);
			}

		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
	}

	public void deleteStore(String packageType, String bodyTemplate,
			String subject) throws UnsupportedEncodingException {
		int dayCount = -EECConstants.PACKAGE_STORE_CLOSED_ALERT - 1;

		if (packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_FREE)) {
			dayCount = -EECConstants.FREE_STORE_CLOSED_ALERT - 1;
		}

		try {
			List<Instance> instance = InstanceLocalServiceUtil
					.getExpPackageList(dayCount, packageType);
			for (Instance inst : instance) {

				Company company = CompanyLocalServiceUtil.getCompany(inst
						.getCompanyId());
				if (PortletPropsValues.CURRENT_WORKING_ENVIRONMENT
						.equals("Production")) {
					DNSSettings.deleteDomain(company.getWebId());
				}
				CompanyLocalServiceUtil.deleteCompany(company);
				InstanceLocalServiceUtil.deleteInstance(inst.getCompanyId());
				sendMail(inst.getInstanceUserId(), subject, bodyTemplate);
			}

		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
	}

	public void sendAlertMessage(int dayCount, String packageType,
			String subject, String bodyTemplate) {
		try {
			List<Instance> instance = InstanceLocalServiceUtil
					.getExpPackageList(dayCount, packageType);
			for (Instance inst : instance) {
				sendMail(inst.getInstanceUserId(), subject, bodyTemplate);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}
	}

	public void sendMail(long userId, String subject, String bodyTemplate)
			throws PortalException, SystemException,
			UnsupportedEncodingException {

		User user = UserLocalServiceUtil.getUser(userId);
		String[] oldSubs = { "[$STORENAME$]", "[$EXP_DATE$]" };
		String[] newSubs = { user.getFirstName(), user.getEmailAddress() };
		String msg = StringUtil.replace(bodyTemplate, oldSubs, newSubs);
		InternetAddress from = new InternetAddress(fromAddress, fromName);
		InternetAddress to = new InternetAddress(user.getEmailAddress(),
				user.getFirstName());
		MailMessage message = new MailMessage(from, to, subject, msg, true);
		MailServiceUtil.sendEmail(message);

	}

	private static Log _log = LogFactoryUtil.getLog(InstanceScheduler.class);
}