/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.EcartPackageBilling;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EcartPackageBilling in entity cache.
 *
 * @author Esquare
 * @see EcartPackageBilling
 * @generated
 */
public class EcartPackageBillingCacheModel implements CacheModel<EcartPackageBilling>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{billId=");
		sb.append(billId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", billingDate=");
		sb.append(billingDate);
		sb.append(", packageType=");
		sb.append(packageType);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", transactionId=");
		sb.append(transactionId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EcartPackageBilling toEntityModel() {
		EcartPackageBillingImpl ecartPackageBillingImpl = new EcartPackageBillingImpl();

		ecartPackageBillingImpl.setBillId(billId);
		ecartPackageBillingImpl.setCompanyId(companyId);
		ecartPackageBillingImpl.setUserId(userId);

		if (billingDate == Long.MIN_VALUE) {
			ecartPackageBillingImpl.setBillingDate(null);
		}
		else {
			ecartPackageBillingImpl.setBillingDate(new Date(billingDate));
		}

		if (packageType == null) {
			ecartPackageBillingImpl.setPackageType(StringPool.BLANK);
		}
		else {
			ecartPackageBillingImpl.setPackageType(packageType);
		}

		ecartPackageBillingImpl.setAmount(amount);
		ecartPackageBillingImpl.setTransactionId(transactionId);

		ecartPackageBillingImpl.resetOriginalValues();

		return ecartPackageBillingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		billId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		billingDate = objectInput.readLong();
		packageType = objectInput.readUTF();
		amount = objectInput.readDouble();
		transactionId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(billId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(billingDate);

		if (packageType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(packageType);
		}

		objectOutput.writeDouble(amount);
		objectOutput.writeLong(transactionId);
	}

	public long billId;
	public long companyId;
	public long userId;
	public long billingDate;
	public String packageType;
	public double amount;
	public long transactionId;
}