/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.ProductImages;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ProductImages in entity cache.
 *
 * @author Esquare
 * @see ProductImages
 * @generated
 */
public class ProductImagesCacheModel implements CacheModel<ProductImages>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{recId=");
		sb.append(recId);
		sb.append(", inventoryId=");
		sb.append(inventoryId);
		sb.append(", imageId=");
		sb.append(imageId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProductImages toEntityModel() {
		ProductImagesImpl productImagesImpl = new ProductImagesImpl();

		productImagesImpl.setRecId(recId);
		productImagesImpl.setInventoryId(inventoryId);
		productImagesImpl.setImageId(imageId);

		productImagesImpl.resetOriginalValues();

		return productImagesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recId = objectInput.readLong();
		inventoryId = objectInput.readLong();
		imageId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recId);
		objectOutput.writeLong(inventoryId);
		objectOutput.writeLong(imageId);
	}

	public long recId;
	public long inventoryId;
	public long imageId;
}