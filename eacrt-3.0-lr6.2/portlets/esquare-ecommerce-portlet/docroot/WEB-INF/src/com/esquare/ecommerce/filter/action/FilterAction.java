package com.esquare.ecommerce.filter.action;

import javax.portlet.Event;
import javax.portlet.PortletSession;

import com.liferay.util.bridges.mvc.MVCPortlet;

public class FilterAction extends MVCPortlet{
	public void processEvent(javax.portlet.EventRequest request, javax.portlet.EventResponse response) 
            throws javax.portlet.PortletException, java.io.IOException {
	Event event = request.getEvent();
	String cmd = (String) event.getValue();
	String[] values = cmd.split(",");
	PortletSession pSession = request.getPortletSession();
	if(values.length>0){
		pSession.setAttribute("FILTER_EVENT_CMD", values[1], PortletSession.PORTLET_SCOPE);
	}
}
}