/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import java.util.Map;

import com.esquare.ecommerce.NoSuchCouponException;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.service.CartLocalServiceUtil;
import com.esquare.ecommerce.service.CouponLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * The extended model implementation for the Cart service. Represents a row in
 * the &quot;EEC_Cart&quot; database table, with each column mapped to a
 * property of this class.
 * 
 * <p>
 * Helper methods and all application logic should be put in this class.
 * Whenever methods are added, rerun ServiceBuilder to copy their definitions
 * into the {@link com.esquare.ecommerce.model.Cart} interface.
 * </p>
 * 
 * @author Esquare
 */
public class CartImpl extends CartBaseImpl {
	
	public CartImpl() {
	}

	public void addProductInventoryId(long productInventoryId) {
		setProductInventoryIds(StringUtil.add(getProductInventoryIds(), String.valueOf(productInventoryId),
				StringPool.COMMA, true));
	}

	public Map<CartItem, Integer> getProductinventories() throws SystemException {
		return CartLocalServiceUtil
				.getProductInventories(getCompanyId(), getProductInventoryIds());
	}

	public Coupon getCoupon() throws PortalException, SystemException {
		Coupon coupon = null;
		if (Validator.isNotNull(getCouponCodes())) {
			String couponCode = StringUtil.split(getCouponCodes())[0];

			try {
				coupon = CouponLocalServiceUtil.getCoupon(couponCode);
			} catch (NoSuchCouponException nsce) {
			}
		}

		return coupon;
	}

	public int getProductsSize() {
		return StringUtil.split(getProductInventoryIds()).length;
	}
}