/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchOrderRefundTrackerException;
import com.esquare.ecommerce.model.OrderRefundTracker;
import com.esquare.ecommerce.model.impl.OrderRefundTrackerImpl;
import com.esquare.ecommerce.model.impl.OrderRefundTrackerModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the order refund tracker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see OrderRefundTrackerPersistence
 * @see OrderRefundTrackerUtil
 * @generated
 */
public class OrderRefundTrackerPersistenceImpl extends BasePersistenceImpl<OrderRefundTracker>
	implements OrderRefundTrackerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OrderRefundTrackerUtil} to access the order refund tracker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OrderRefundTrackerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED,
			OrderRefundTrackerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED,
			OrderRefundTrackerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_S = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED,
			OrderRefundTrackerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_S",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED,
			OrderRefundTrackerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_S",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			OrderRefundTrackerModelImpl.COMPANYID_COLUMN_BITMASK |
			OrderRefundTrackerModelImpl.PAYMENTSTATUS_COLUMN_BITMASK |
			OrderRefundTrackerModelImpl.MODIFIEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_S = new FinderPath(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_S",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the order refund trackers where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @return the matching order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findByC_S(long companyId,
		boolean paymentStatus) throws SystemException {
		return findByC_S(companyId, paymentStatus, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order refund trackers where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderRefundTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param start the lower bound of the range of order refund trackers
	 * @param end the upper bound of the range of order refund trackers (not inclusive)
	 * @return the range of matching order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findByC_S(long companyId,
		boolean paymentStatus, int start, int end) throws SystemException {
		return findByC_S(companyId, paymentStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the order refund trackers where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderRefundTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param start the lower bound of the range of order refund trackers
	 * @param end the upper bound of the range of order refund trackers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findByC_S(long companyId,
		boolean paymentStatus, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S;
			finderArgs = new Object[] { companyId, paymentStatus };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_S;
			finderArgs = new Object[] {
					companyId, paymentStatus,
					
					start, end, orderByComparator
				};
		}

		List<OrderRefundTracker> list = (List<OrderRefundTracker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrderRefundTracker orderRefundTracker : list) {
				if ((companyId != orderRefundTracker.getCompanyId()) ||
						(paymentStatus != orderRefundTracker.getPaymentStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDERREFUNDTRACKER_WHERE);

			query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_S_PAYMENTSTATUS_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderRefundTrackerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(paymentStatus);

				if (!pagination) {
					list = (List<OrderRefundTracker>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderRefundTracker>(list);
				}
				else {
					list = (List<OrderRefundTracker>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order refund tracker in the ordered set where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order refund tracker
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a matching order refund tracker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker findByC_S_First(long companyId,
		boolean paymentStatus, OrderByComparator orderByComparator)
		throws NoSuchOrderRefundTrackerException, SystemException {
		OrderRefundTracker orderRefundTracker = fetchByC_S_First(companyId,
				paymentStatus, orderByComparator);

		if (orderRefundTracker != null) {
			return orderRefundTracker;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", paymentStatus=");
		msg.append(paymentStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderRefundTrackerException(msg.toString());
	}

	/**
	 * Returns the first order refund tracker in the ordered set where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order refund tracker, or <code>null</code> if a matching order refund tracker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker fetchByC_S_First(long companyId,
		boolean paymentStatus, OrderByComparator orderByComparator)
		throws SystemException {
		List<OrderRefundTracker> list = findByC_S(companyId, paymentStatus, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order refund tracker in the ordered set where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order refund tracker
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a matching order refund tracker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker findByC_S_Last(long companyId,
		boolean paymentStatus, OrderByComparator orderByComparator)
		throws NoSuchOrderRefundTrackerException, SystemException {
		OrderRefundTracker orderRefundTracker = fetchByC_S_Last(companyId,
				paymentStatus, orderByComparator);

		if (orderRefundTracker != null) {
			return orderRefundTracker;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", paymentStatus=");
		msg.append(paymentStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderRefundTrackerException(msg.toString());
	}

	/**
	 * Returns the last order refund tracker in the ordered set where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order refund tracker, or <code>null</code> if a matching order refund tracker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker fetchByC_S_Last(long companyId,
		boolean paymentStatus, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByC_S(companyId, paymentStatus);

		if (count == 0) {
			return null;
		}

		List<OrderRefundTracker> list = findByC_S(companyId, paymentStatus,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the order refund trackers before and after the current order refund tracker in the ordered set where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param refundTrackerId the primary key of the current order refund tracker
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order refund tracker
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker[] findByC_S_PrevAndNext(long refundTrackerId,
		long companyId, boolean paymentStatus,
		OrderByComparator orderByComparator)
		throws NoSuchOrderRefundTrackerException, SystemException {
		OrderRefundTracker orderRefundTracker = findByPrimaryKey(refundTrackerId);

		Session session = null;

		try {
			session = openSession();

			OrderRefundTracker[] array = new OrderRefundTrackerImpl[3];

			array[0] = getByC_S_PrevAndNext(session, orderRefundTracker,
					companyId, paymentStatus, orderByComparator, true);

			array[1] = orderRefundTracker;

			array[2] = getByC_S_PrevAndNext(session, orderRefundTracker,
					companyId, paymentStatus, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrderRefundTracker getByC_S_PrevAndNext(Session session,
		OrderRefundTracker orderRefundTracker, long companyId,
		boolean paymentStatus, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDERREFUNDTRACKER_WHERE);

		query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_S_PAYMENTSTATUS_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderRefundTrackerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(paymentStatus);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(orderRefundTracker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrderRefundTracker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the order refund trackers where companyId = &#63; and paymentStatus = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_S(long companyId, boolean paymentStatus)
		throws SystemException {
		for (OrderRefundTracker orderRefundTracker : findByC_S(companyId,
				paymentStatus, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(orderRefundTracker);
		}
	}

	/**
	 * Returns the number of order refund trackers where companyId = &#63; and paymentStatus = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentStatus the payment status
	 * @return the number of matching order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_S(long companyId, boolean paymentStatus)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_S;

		Object[] finderArgs = new Object[] { companyId, paymentStatus };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDERREFUNDTRACKER_WHERE);

			query.append(_FINDER_COLUMN_C_S_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_S_PAYMENTSTATUS_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(paymentStatus);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_S_COMPANYID_2 = "orderRefundTracker.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_S_PAYMENTSTATUS_2 = "orderRefundTracker.paymentStatus = ?";

	public OrderRefundTrackerPersistenceImpl() {
		setModelClass(OrderRefundTracker.class);
	}

	/**
	 * Caches the order refund tracker in the entity cache if it is enabled.
	 *
	 * @param orderRefundTracker the order refund tracker
	 */
	@Override
	public void cacheResult(OrderRefundTracker orderRefundTracker) {
		EntityCacheUtil.putResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerImpl.class, orderRefundTracker.getPrimaryKey(),
			orderRefundTracker);

		orderRefundTracker.resetOriginalValues();
	}

	/**
	 * Caches the order refund trackers in the entity cache if it is enabled.
	 *
	 * @param orderRefundTrackers the order refund trackers
	 */
	@Override
	public void cacheResult(List<OrderRefundTracker> orderRefundTrackers) {
		for (OrderRefundTracker orderRefundTracker : orderRefundTrackers) {
			if (EntityCacheUtil.getResult(
						OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
						OrderRefundTrackerImpl.class,
						orderRefundTracker.getPrimaryKey()) == null) {
				cacheResult(orderRefundTracker);
			}
			else {
				orderRefundTracker.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all order refund trackers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OrderRefundTrackerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OrderRefundTrackerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the order refund tracker.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(OrderRefundTracker orderRefundTracker) {
		EntityCacheUtil.removeResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerImpl.class, orderRefundTracker.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<OrderRefundTracker> orderRefundTrackers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (OrderRefundTracker orderRefundTracker : orderRefundTrackers) {
			EntityCacheUtil.removeResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
				OrderRefundTrackerImpl.class, orderRefundTracker.getPrimaryKey());
		}
	}

	/**
	 * Creates a new order refund tracker with the primary key. Does not add the order refund tracker to the database.
	 *
	 * @param refundTrackerId the primary key for the new order refund tracker
	 * @return the new order refund tracker
	 */
	@Override
	public OrderRefundTracker create(long refundTrackerId) {
		OrderRefundTracker orderRefundTracker = new OrderRefundTrackerImpl();

		orderRefundTracker.setNew(true);
		orderRefundTracker.setPrimaryKey(refundTrackerId);

		return orderRefundTracker;
	}

	/**
	 * Removes the order refund tracker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param refundTrackerId the primary key of the order refund tracker
	 * @return the order refund tracker that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker remove(long refundTrackerId)
		throws NoSuchOrderRefundTrackerException, SystemException {
		return remove((Serializable)refundTrackerId);
	}

	/**
	 * Removes the order refund tracker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the order refund tracker
	 * @return the order refund tracker that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker remove(Serializable primaryKey)
		throws NoSuchOrderRefundTrackerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			OrderRefundTracker orderRefundTracker = (OrderRefundTracker)session.get(OrderRefundTrackerImpl.class,
					primaryKey);

			if (orderRefundTracker == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOrderRefundTrackerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(orderRefundTracker);
		}
		catch (NoSuchOrderRefundTrackerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected OrderRefundTracker removeImpl(
		OrderRefundTracker orderRefundTracker) throws SystemException {
		orderRefundTracker = toUnwrappedModel(orderRefundTracker);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(orderRefundTracker)) {
				orderRefundTracker = (OrderRefundTracker)session.get(OrderRefundTrackerImpl.class,
						orderRefundTracker.getPrimaryKeyObj());
			}

			if (orderRefundTracker != null) {
				session.delete(orderRefundTracker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (orderRefundTracker != null) {
			clearCache(orderRefundTracker);
		}

		return orderRefundTracker;
	}

	@Override
	public OrderRefundTracker updateImpl(
		com.esquare.ecommerce.model.OrderRefundTracker orderRefundTracker)
		throws SystemException {
		orderRefundTracker = toUnwrappedModel(orderRefundTracker);

		boolean isNew = orderRefundTracker.isNew();

		OrderRefundTrackerModelImpl orderRefundTrackerModelImpl = (OrderRefundTrackerModelImpl)orderRefundTracker;

		Session session = null;

		try {
			session = openSession();

			if (orderRefundTracker.isNew()) {
				session.save(orderRefundTracker);

				orderRefundTracker.setNew(false);
			}
			else {
				session.merge(orderRefundTracker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OrderRefundTrackerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((orderRefundTrackerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderRefundTrackerModelImpl.getOriginalCompanyId(),
						orderRefundTrackerModelImpl.getOriginalPaymentStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S,
					args);

				args = new Object[] {
						orderRefundTrackerModelImpl.getCompanyId(),
						orderRefundTrackerModelImpl.getPaymentStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_S,
					args);
			}
		}

		EntityCacheUtil.putResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
			OrderRefundTrackerImpl.class, orderRefundTracker.getPrimaryKey(),
			orderRefundTracker);

		return orderRefundTracker;
	}

	protected OrderRefundTracker toUnwrappedModel(
		OrderRefundTracker orderRefundTracker) {
		if (orderRefundTracker instanceof OrderRefundTrackerImpl) {
			return orderRefundTracker;
		}

		OrderRefundTrackerImpl orderRefundTrackerImpl = new OrderRefundTrackerImpl();

		orderRefundTrackerImpl.setNew(orderRefundTracker.isNew());
		orderRefundTrackerImpl.setPrimaryKey(orderRefundTracker.getPrimaryKey());

		orderRefundTrackerImpl.setRefundTrackerId(orderRefundTracker.getRefundTrackerId());
		orderRefundTrackerImpl.setCompanyId(orderRefundTracker.getCompanyId());
		orderRefundTrackerImpl.setUserId(orderRefundTracker.getUserId());
		orderRefundTrackerImpl.setReferenceNumber(orderRefundTracker.getReferenceNumber());
		orderRefundTrackerImpl.setAmountClaimed(orderRefundTracker.getAmountClaimed());
		orderRefundTrackerImpl.setPaymentStatus(orderRefundTracker.isPaymentStatus());
		orderRefundTrackerImpl.setPaymentMode(orderRefundTracker.getPaymentMode());
		orderRefundTrackerImpl.setCreateDate(orderRefundTracker.getCreateDate());
		orderRefundTrackerImpl.setModifiedDate(orderRefundTracker.getModifiedDate());
		orderRefundTrackerImpl.setExpectedDeliveryDate(orderRefundTracker.getExpectedDeliveryDate());
		orderRefundTrackerImpl.setComments(orderRefundTracker.getComments());

		return orderRefundTrackerImpl;
	}

	/**
	 * Returns the order refund tracker with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the order refund tracker
	 * @return the order refund tracker
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOrderRefundTrackerException, SystemException {
		OrderRefundTracker orderRefundTracker = fetchByPrimaryKey(primaryKey);

		if (orderRefundTracker == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOrderRefundTrackerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return orderRefundTracker;
	}

	/**
	 * Returns the order refund tracker with the primary key or throws a {@link com.esquare.ecommerce.NoSuchOrderRefundTrackerException} if it could not be found.
	 *
	 * @param refundTrackerId the primary key of the order refund tracker
	 * @return the order refund tracker
	 * @throws com.esquare.ecommerce.NoSuchOrderRefundTrackerException if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker findByPrimaryKey(long refundTrackerId)
		throws NoSuchOrderRefundTrackerException, SystemException {
		return findByPrimaryKey((Serializable)refundTrackerId);
	}

	/**
	 * Returns the order refund tracker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the order refund tracker
	 * @return the order refund tracker, or <code>null</code> if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		OrderRefundTracker orderRefundTracker = (OrderRefundTracker)EntityCacheUtil.getResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
				OrderRefundTrackerImpl.class, primaryKey);

		if (orderRefundTracker == _nullOrderRefundTracker) {
			return null;
		}

		if (orderRefundTracker == null) {
			Session session = null;

			try {
				session = openSession();

				orderRefundTracker = (OrderRefundTracker)session.get(OrderRefundTrackerImpl.class,
						primaryKey);

				if (orderRefundTracker != null) {
					cacheResult(orderRefundTracker);
				}
				else {
					EntityCacheUtil.putResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
						OrderRefundTrackerImpl.class, primaryKey,
						_nullOrderRefundTracker);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OrderRefundTrackerModelImpl.ENTITY_CACHE_ENABLED,
					OrderRefundTrackerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return orderRefundTracker;
	}

	/**
	 * Returns the order refund tracker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param refundTrackerId the primary key of the order refund tracker
	 * @return the order refund tracker, or <code>null</code> if a order refund tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderRefundTracker fetchByPrimaryKey(long refundTrackerId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)refundTrackerId);
	}

	/**
	 * Returns all the order refund trackers.
	 *
	 * @return the order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order refund trackers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderRefundTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of order refund trackers
	 * @param end the upper bound of the range of order refund trackers (not inclusive)
	 * @return the range of order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the order refund trackers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderRefundTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of order refund trackers
	 * @param end the upper bound of the range of order refund trackers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderRefundTracker> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<OrderRefundTracker> list = (List<OrderRefundTracker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ORDERREFUNDTRACKER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ORDERREFUNDTRACKER;

				if (pagination) {
					sql = sql.concat(OrderRefundTrackerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<OrderRefundTracker>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderRefundTracker>(list);
				}
				else {
					list = (List<OrderRefundTracker>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the order refund trackers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (OrderRefundTracker orderRefundTracker : findAll()) {
			remove(orderRefundTracker);
		}
	}

	/**
	 * Returns the number of order refund trackers.
	 *
	 * @return the number of order refund trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ORDERREFUNDTRACKER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the order refund tracker persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.OrderRefundTracker")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<OrderRefundTracker>> listenersList = new ArrayList<ModelListener<OrderRefundTracker>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<OrderRefundTracker>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OrderRefundTrackerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ORDERREFUNDTRACKER = "SELECT orderRefundTracker FROM OrderRefundTracker orderRefundTracker";
	private static final String _SQL_SELECT_ORDERREFUNDTRACKER_WHERE = "SELECT orderRefundTracker FROM OrderRefundTracker orderRefundTracker WHERE ";
	private static final String _SQL_COUNT_ORDERREFUNDTRACKER = "SELECT COUNT(orderRefundTracker) FROM OrderRefundTracker orderRefundTracker";
	private static final String _SQL_COUNT_ORDERREFUNDTRACKER_WHERE = "SELECT COUNT(orderRefundTracker) FROM OrderRefundTracker orderRefundTracker WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "orderRefundTracker.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No OrderRefundTracker exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No OrderRefundTracker exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OrderRefundTrackerPersistenceImpl.class);
	private static OrderRefundTracker _nullOrderRefundTracker = new OrderRefundTrackerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<OrderRefundTracker> toCacheModel() {
				return _nullOrderRefundTrackerCacheModel;
			}
		};

	private static CacheModel<OrderRefundTracker> _nullOrderRefundTrackerCacheModel =
		new CacheModel<OrderRefundTracker>() {
			@Override
			public OrderRefundTracker toEntityModel() {
				return _nullOrderRefundTracker;
			}
		};
}