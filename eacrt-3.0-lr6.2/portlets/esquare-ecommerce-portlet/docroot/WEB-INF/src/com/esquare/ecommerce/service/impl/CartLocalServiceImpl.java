/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.esquare.ecommerce.NoSuchCartException;
import com.esquare.ecommerce.WalletLimitException;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.CartItemImpl;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.base.CartLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.CartUtil;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.User;

/**
 * The implementation of the cart local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.CartLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CartLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.CartLocalServiceUtil
 */
public class CartLocalServiceImpl extends CartLocalServiceBaseImpl {

	public Cart findByC_U(long companyId, long userId)
			throws NoSuchCartException, SystemException {
		return CartUtil.findByC_U(companyId, userId);
	}

	public Cart getCart(long userId, long companyId) throws PortalException,
			SystemException {

		return cartPersistence.findByC_U(companyId, userId);
	}

	public Map<CartItem, Integer> getProductInventories(long companyId,
			String productInventoryIds) throws SystemException {
		Map<CartItem, Integer> productInventories = new TreeMap<CartItem, Integer>();

		String[] productInventoryIdsArray = StringUtil
				.split(productInventoryIds);

		for (int i = 0; i < productInventoryIdsArray.length; i++) {
			long productInventoryId = EECUtil
					.getProductInventoryId(productInventoryIdsArray[i]);

			ProductInventory productInventory = productInventoryPersistence
					.fetchByPrimaryKey(productInventoryId);
			if (productInventory != null) {
				ProductDetails productDetails = productDetailsPersistence
						.fetchByPrimaryKey(productInventory
								.getProductDetailsId());
				Catalog catalog = productDetails.getCatalog();

				if (catalog.getCompanyId() == companyId) {
					CartItem cartItem = new CartItemImpl(productInventory);

					Integer count = productInventories.get(cartItem);

					if (count == null) {
						count = new Integer(1);
					} else {
						count = new Integer(count.intValue() + 1);
					}

					productInventories.put(cartItem, count);
				}
			}
		}

		return productInventories;
	}

	public Cart updateCart(long userId, long companyId, long groupId,
			String productInventoryIds, String couponCodes, int altShipping,
			boolean insure) throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);
		Date now = new Date();

		Cart cart = null;

		if (user.isDefaultUser()) {
			cart = cartPersistence.create(0);

			cart.setGroupId(groupId);
			cart.setCompanyId(user.getCompanyId());
			cart.setUserId(userId);
			cart.setUserName(user.getFullName());
			cart.setCreateDate(now);
		} else {
			cart = cartPersistence.fetchByC_U(companyId, userId);
			if (cart == null) {
				long cartId = counterLocalService.increment();

				cart = cartPersistence.create(cartId);
				cart.setGroupId(groupId);
				cart.setCompanyId(user.getCompanyId());
				cart.setUserId(userId);
				cart.setUserName(user.getFullName());
				cart.setCreateDate(now);
			}
		}
		cart.setModifiedDate(now);
		cart.setProductInventoryIds(checkProductInventoryIds(companyId,
				productInventoryIds));
		cart.setCouponCodes(couponCodes);
		cart.setAltShipping(altShipping);
		cart.setInsure(insure);

		if (!user.isDefaultUser()) {
			cartPersistence.update(cart);
		}

		return cart;
	}

	public Cart updateWalletAmount(long cartId, long userId,
			double walletAmount, double grandTotal) throws SystemException,
			PortalException {

		double myWalletAmount = EECUtil.myWalletAmount(userId);
		if (myWalletAmount < walletAmount) {
			throw new WalletLimitException();
		}
		Cart cart = null;
		try {
			cart = cartPersistence.fetchByPrimaryKey(cartId);
			cart.setWalletAmount((walletAmount < grandTotal || walletAmount == 0) ? walletAmount
					: grandTotal);
			cartPersistence.updateImpl(cart);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		return cart;
	}

	protected String checkProductInventoryIds(long companyId,
			String productInventoryIds) {
		String[] productInventoryIdsArray = StringUtil
				.split(productInventoryIds);

		for (int i = 0; i < productInventoryIdsArray.length; i++) {
			long productInventoryId = EECUtil
					.getProductInventoryId(productInventoryIdsArray[i]);

			ProductInventory productInventory = null;
			ProductDetails productDetails = null;

			try {
				productInventory = productInventoryPersistence
						.fetchByPrimaryKey(productInventoryId);
				productDetails = productDetailsPersistence
						.fetchByPrimaryKey(productInventory
								.getProductDetailsId());

				Catalog catalog = productDetails.getCatalog();

				if (catalog.getCompanyId() != companyId) {
					productDetails = null;
				}
			} catch (Exception e) {
				_log.info(e.getClass());
			}

			if (productDetails == null) {
				productInventoryIds = StringUtil.remove(productInventoryIds,
						productInventoryIdsArray[i]);
			}
		}

		return productInventoryIds;
	}

	private static Log _log = LogFactoryUtil.getLog(CartLocalServiceImpl.class);
}