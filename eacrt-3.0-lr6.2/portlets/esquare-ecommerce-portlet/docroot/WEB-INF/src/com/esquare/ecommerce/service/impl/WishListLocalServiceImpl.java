/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.WishList;
import com.esquare.ecommerce.model.WishListItem;
import com.esquare.ecommerce.model.WishListItemImpl;
import com.esquare.ecommerce.service.base.WishListLocalServiceBaseImpl;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.User;

/**
 * The implementation of the wish list local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.WishListLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.WishListLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.WishListLocalServiceUtil
 */
public class WishListLocalServiceImpl extends WishListLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * com.esquare.ecommerce.service.WishListLocalServiceUtil} to access the
	 * wish list local service.
	 */
	public WishList getWishList(long userId, long companyId)
			throws PortalException, SystemException {

		return wishListPersistence.findByC_U(companyId, userId);
	}

	public Map<WishListItem, Integer> getProductInventories(long companyId,
			String productInventoryIds) throws SystemException {

		Map<WishListItem, Integer> productInventories = new TreeMap<WishListItem, Integer>();

		String[] productIdsArray = StringUtil.split(productInventoryIds);

		for (int i = 0; i < productIdsArray.length; i++) {
			long productInventoryId = EECUtil
					.getProductInventoryId(productIdsArray[i]);

			ProductInventory productInventory = productInventoryPersistence
					.fetchByPrimaryKey(productInventoryId);
			ProductDetails productDetails = productDetailsPersistence
					.fetchByPrimaryKey(productInventory.getProductDetailsId());

			if (productInventory != null) {
				Catalog catalog = productDetails.getCatalog();

				if (catalog.getCompanyId() == companyId) {
					WishListItem wishListItem = new WishListItemImpl(
							productInventory);

					Integer count = productInventories.get(wishListItem);

					if (count == null) {
						count = new Integer(1);
					} else {
						count = new Integer(count.intValue() + 1);
					}

					productInventories.put(wishListItem, count);
				}
			}
		}

		return productInventories;
	}

	public WishList updateWishList(long userId, long companyId, long groupId,
			String productInventoryIds) throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);
		Date now = new Date();

		WishList wishList = null;

		if (user.isDefaultUser()) {
			wishList = wishListPersistence.create(0);

			wishList.setGroupId(groupId);
			wishList.setCompanyId(user.getCompanyId());
			wishList.setUserId(userId);
			wishList.setUserName(user.getFullName());
			wishList.setCreateDate(now);
		} else {
			wishList = wishListPersistence.fetchByC_U(companyId, userId);

			if (wishList == null) {
				long wishListId = counterLocalService.increment();

				wishList = wishListPersistence.create(wishListId);
				wishList.setGroupId(groupId);
				wishList.setCompanyId(user.getCompanyId());
				wishList.setUserId(userId);
				wishList.setUserName(user.getFullName());
				wishList.setCreateDate(now);
			}
		}

		wishList.setModifiedDate(now);
		wishList.setProductInventoryIds(checkProductInventoryIds(companyId,
				productInventoryIds));

		if (!user.isDefaultUser()) {
			wishListPersistence.update(wishList);
		}

		return wishList;
	}

	protected String checkProductInventoryIds(long companyId,
			String productInventoryIds) {
		String[] productInventoryIdsArray = StringUtil
				.split(productInventoryIds);

		for (int i = 0; i < productInventoryIdsArray.length; i++) {
			long productInventoryId = EECUtil
					.getProductInventoryId(productInventoryIdsArray[i]);

			ProductInventory productInventory = null;
			ProductDetails productDetails = null;

			try {
				productInventory = productInventoryPersistence
						.fetchByPrimaryKey(productInventoryId);
				productDetails = productDetailsPersistence
						.fetchByPrimaryKey(productInventory
								.getProductDetailsId());

				Catalog catalog = productDetails.getCatalog();

				if (catalog.getCompanyId() != companyId) {
					productDetails = null;
				}
			} catch (Exception e) {
			}

			if (productDetails == null) {
				productInventoryIds = StringUtil.remove(productInventoryIds,
						productInventoryIdsArray[i]);
			}
		}

		return productInventoryIds;
	}
}