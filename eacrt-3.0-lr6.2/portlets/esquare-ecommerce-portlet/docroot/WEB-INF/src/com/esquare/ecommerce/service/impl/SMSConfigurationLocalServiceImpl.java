/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.model.SMSConfiguration;
import com.esquare.ecommerce.service.base.SMSConfigurationLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.SMSConfigurationPersistence;
import com.esquare.ecommerce.service.persistence.SMSConfigurationUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the s m s configuration local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.SMSConfigurationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.SMSConfigurationLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.SMSConfigurationLocalServiceUtil
 */
public class SMSConfigurationLocalServiceImpl
	extends SMSConfigurationLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.esquare.ecommerce.service.SMSConfigurationLocalServiceUtil} to access the s m s configuration local service.
	 */
	
	public List<SMSConfiguration> findByCompanyId(long companyId) throws SystemException { 
		return SMSConfigurationUtil.findByCompanyId(companyId);
	}
	

}