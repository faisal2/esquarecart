package com.esquare.ecommerce.compare.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.Cookie;

import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CompareAction extends MVCPortlet {
	@Override
	public void serveResource(ResourceRequest p_request,
			ResourceResponse p_response) throws IOException, PortletException {
		super.serveResource(p_request, p_response);
		String compareProductInventoryId = null;
		Cookie cookie = EECUtil.getCookie(p_request, "compare-inventory-ids");
		if (Validator.isNotNull(cookie))
			compareProductInventoryId = cookie.getValue();
		String selProductInventoryId = p_request
				.getParameter("selProductInventoryId");
		StringBuffer sb = new StringBuffer();
		ProductInventory productInventory = null;
		try {
			productInventory = ProductInventoryLocalServiceUtil
					.getProductInventory(Long.valueOf(selProductInventoryId));
		} catch (NumberFormatException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		if (Validator.isNull(compareProductInventoryId)) {
			sb.append(selProductInventoryId);
			sb.append(StringPool.COMMA);
			compareProductInventoryId = sb.toString();
			Cookie compare_custom_field_id = new Cookie(
					"compare-custom-field-id", String.valueOf(productInventory
							.getCustomFieldRecId()));
			p_response.addProperty(compare_custom_field_id);
		} else if (compareProductInventoryId.split(StringPool.COMMA).length < 4) {
			compareProductInventoryId = addSelectedProduct(
					compareProductInventoryId, selProductInventoryId);
		}
		Cookie compare_inventory_ids = new Cookie("compare-inventory-ids",
				compareProductInventoryId.toString());
		p_response.addProperty(compare_inventory_ids);
	}

	public String deleteSelectedProduct(String aJAXCALL_InventoryId,
			String selProductInventoryId) {

		if (aJAXCALL_InventoryId.contains(selProductInventoryId)) {
			String[] inventoryIds = aJAXCALL_InventoryId.split(",");
			List<String> list = new ArrayList<String>(
					Arrays.asList(inventoryIds));
			list.remove(selProductInventoryId);
			inventoryIds = list.toArray(new String[inventoryIds.length]);
			selProductInventoryId = StringPool.BLANK;
			for (String inventoryId : inventoryIds) {
				if (Validator.isNotNull(inventoryId)) {
					selProductInventoryId += inventoryId + StringPool.COMMA;
				}
			}
		}
		return selProductInventoryId;
	}

	public String addSelectedProduct(String compareProductInventoryId,
			String selProductInventoryId) {
		Set<String> compareInventoryIDSet = new LinkedHashSet<String>(
				Arrays.asList(compareProductInventoryId.split(StringPool.COMMA)));
		compareInventoryIDSet.add(selProductInventoryId);
		StringBundler compareInventoryId = new StringBundler();
		for (String hashSet : compareInventoryIDSet) {
			compareInventoryId.append(hashSet);
			compareInventoryId.append(StringPool.COMMA);
		}
		return compareInventoryId.toString();
	}

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		String productInventoryIds = ParamUtil.getString(actionRequest,
				"productInventoryIds");
		String selProductInventoryId = ParamUtil.getString(actionRequest,
				"selectedInventoryId");
		if (cmd.equals(EECConstants.DELETE)) {
			Cookie compare_inventory_ids = null;
			String productInventoryId = StringPool.BLANK;
			if (selProductInventoryId.equalsIgnoreCase(StringPool.BLANK)) {
				deleteCookies(actionRequest, actionResponse,
						"compare-inventory-ids");
				deleteCookies(actionRequest, actionResponse,
						"compare-custom-field-id");
			} else {
				productInventoryId = deleteSelectedProduct(productInventoryIds,
						selProductInventoryId);
				if (Validator.isNull(productInventoryId)) {
					deleteCookies(actionRequest, actionResponse,
							"compare-custom-field-id");
				}
				compare_inventory_ids = new Cookie("compare-inventory-ids",
						productInventoryId);
				actionResponse.addProperty(compare_inventory_ids);
			}
		}

	}

	private void deleteCookies(ActionRequest actionRequest,
			ActionResponse actionResponse, String cookieName) {
		Cookie cookie = EECUtil.getCookie(actionRequest, cookieName);
		if (Validator.isNotNull(cookie)) {
			cookie.setMaxAge(0);
			actionResponse.addProperty(cookie);
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CompareAction.class);
}