/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchEcustomFieldException;
import com.esquare.ecommerce.model.EcustomField;
import com.esquare.ecommerce.model.impl.EcustomFieldImpl;
import com.esquare.ecommerce.model.impl.EcustomFieldModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the ecustom field service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see EcustomFieldPersistence
 * @see EcustomFieldUtil
 * @generated
 */
public class EcustomFieldPersistenceImpl extends BasePersistenceImpl<EcustomField>
	implements EcustomFieldPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EcustomFieldUtil} to access the ecustom field persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EcustomFieldImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, EcustomFieldImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, EcustomFieldImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, EcustomFieldImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, EcustomFieldImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			EcustomFieldModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the ecustom fields where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ecustom fields where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ecustom fields
	 * @param end the upper bound of the range of ecustom fields (not inclusive)
	 * @return the range of matching ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findByCompanyId(long companyId, int start, int end)
		throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecustom fields where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ecustom fields
	 * @param end the upper bound of the range of ecustom fields (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findByCompanyId(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<EcustomField> list = (List<EcustomField>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EcustomField ecustomField : list) {
				if ((companyId != ecustomField.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ECUSTOMFIELD_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EcustomFieldModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<EcustomField>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcustomField>(list);
				}
				else {
					list = (List<EcustomField>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ecustom field in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (ecustomField != null) {
			return ecustomField;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcustomFieldException(msg.toString());
	}

	/**
	 * Returns the first ecustom field in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecustom field, or <code>null</code> if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EcustomField> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ecustom field in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (ecustomField != null) {
			return ecustomField;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcustomFieldException(msg.toString());
	}

	/**
	 * Returns the last ecustom field in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecustom field, or <code>null</code> if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<EcustomField> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ecustom fields before and after the current ecustom field in the ordered set where companyId = &#63;.
	 *
	 * @param customFieldRecId the primary key of the current ecustom field
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField[] findByCompanyId_PrevAndNext(long customFieldRecId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = findByPrimaryKey(customFieldRecId);

		Session session = null;

		try {
			session = openSession();

			EcustomField[] array = new EcustomFieldImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, ecustomField,
					companyId, orderByComparator, true);

			array[1] = ecustomField;

			array[2] = getByCompanyId_PrevAndNext(session, ecustomField,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EcustomField getByCompanyId_PrevAndNext(Session session,
		EcustomField ecustomField, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ECUSTOMFIELD_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EcustomFieldModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ecustomField);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EcustomField> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ecustom fields where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (EcustomField ecustomField : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ecustomField);
		}
	}

	/**
	 * Returns the number of ecustom fields where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ECUSTOMFIELD_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "ecustomField.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_TITLE = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, EcustomFieldImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByTitle",
			new String[] { Long.class.getName(), String.class.getName() },
			EcustomFieldModelImpl.COMPANYID_COLUMN_BITMASK |
			EcustomFieldModelImpl.TITLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TITLE = new FinderPath(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTitle",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the ecustom field where companyId = &#63; and title = &#63; or throws a {@link com.esquare.ecommerce.NoSuchEcustomFieldException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param title the title
	 * @return the matching ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField findByTitle(long companyId, String title)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = fetchByTitle(companyId, title);

		if (ecustomField == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", title=");
			msg.append(title);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchEcustomFieldException(msg.toString());
		}

		return ecustomField;
	}

	/**
	 * Returns the ecustom field where companyId = &#63; and title = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param title the title
	 * @return the matching ecustom field, or <code>null</code> if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByTitle(long companyId, String title)
		throws SystemException {
		return fetchByTitle(companyId, title, true);
	}

	/**
	 * Returns the ecustom field where companyId = &#63; and title = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param title the title
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching ecustom field, or <code>null</code> if a matching ecustom field could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByTitle(long companyId, String title,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, title };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_TITLE,
					finderArgs, this);
		}

		if (result instanceof EcustomField) {
			EcustomField ecustomField = (EcustomField)result;

			if ((companyId != ecustomField.getCompanyId()) ||
					!Validator.equals(title, ecustomField.getTitle())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_ECUSTOMFIELD_WHERE);

			query.append(_FINDER_COLUMN_TITLE_COMPANYID_2);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindTitle) {
					qPos.add(title);
				}

				List<EcustomField> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TITLE,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"EcustomFieldPersistenceImpl.fetchByTitle(long, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					EcustomField ecustomField = list.get(0);

					result = ecustomField;

					cacheResult(ecustomField);

					if ((ecustomField.getCompanyId() != companyId) ||
							(ecustomField.getTitle() == null) ||
							!ecustomField.getTitle().equals(title)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TITLE,
							finderArgs, ecustomField);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TITLE,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EcustomField)result;
		}
	}

	/**
	 * Removes the ecustom field where companyId = &#63; and title = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param title the title
	 * @return the ecustom field that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField removeByTitle(long companyId, String title)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = findByTitle(companyId, title);

		return remove(ecustomField);
	}

	/**
	 * Returns the number of ecustom fields where companyId = &#63; and title = &#63;.
	 *
	 * @param companyId the company ID
	 * @param title the title
	 * @return the number of matching ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByTitle(long companyId, String title)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TITLE;

		Object[] finderArgs = new Object[] { companyId, title };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ECUSTOMFIELD_WHERE);

			query.append(_FINDER_COLUMN_TITLE_COMPANYID_2);

			boolean bindTitle = false;

			if (title == null) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_1);
			}
			else if (title.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_TITLE_TITLE_3);
			}
			else {
				bindTitle = true;

				query.append(_FINDER_COLUMN_TITLE_TITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindTitle) {
					qPos.add(title);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TITLE_COMPANYID_2 = "ecustomField.companyId = ? AND ";
	private static final String _FINDER_COLUMN_TITLE_TITLE_1 = "ecustomField.title IS NULL";
	private static final String _FINDER_COLUMN_TITLE_TITLE_2 = "ecustomField.title = ?";
	private static final String _FINDER_COLUMN_TITLE_TITLE_3 = "(ecustomField.title IS NULL OR ecustomField.title = '')";

	public EcustomFieldPersistenceImpl() {
		setModelClass(EcustomField.class);
	}

	/**
	 * Caches the ecustom field in the entity cache if it is enabled.
	 *
	 * @param ecustomField the ecustom field
	 */
	@Override
	public void cacheResult(EcustomField ecustomField) {
		EntityCacheUtil.putResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldImpl.class, ecustomField.getPrimaryKey(), ecustomField);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TITLE,
			new Object[] { ecustomField.getCompanyId(), ecustomField.getTitle() },
			ecustomField);

		ecustomField.resetOriginalValues();
	}

	/**
	 * Caches the ecustom fields in the entity cache if it is enabled.
	 *
	 * @param ecustomFields the ecustom fields
	 */
	@Override
	public void cacheResult(List<EcustomField> ecustomFields) {
		for (EcustomField ecustomField : ecustomFields) {
			if (EntityCacheUtil.getResult(
						EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
						EcustomFieldImpl.class, ecustomField.getPrimaryKey()) == null) {
				cacheResult(ecustomField);
			}
			else {
				ecustomField.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ecustom fields.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EcustomFieldImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EcustomFieldImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ecustom field.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EcustomField ecustomField) {
		EntityCacheUtil.removeResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldImpl.class, ecustomField.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(ecustomField);
	}

	@Override
	public void clearCache(List<EcustomField> ecustomFields) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EcustomField ecustomField : ecustomFields) {
			EntityCacheUtil.removeResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
				EcustomFieldImpl.class, ecustomField.getPrimaryKey());

			clearUniqueFindersCache(ecustomField);
		}
	}

	protected void cacheUniqueFindersCache(EcustomField ecustomField) {
		if (ecustomField.isNew()) {
			Object[] args = new Object[] {
					ecustomField.getCompanyId(), ecustomField.getTitle()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TITLE, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TITLE, args,
				ecustomField);
		}
		else {
			EcustomFieldModelImpl ecustomFieldModelImpl = (EcustomFieldModelImpl)ecustomField;

			if ((ecustomFieldModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_TITLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ecustomField.getCompanyId(), ecustomField.getTitle()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TITLE, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TITLE, args,
					ecustomField);
			}
		}
	}

	protected void clearUniqueFindersCache(EcustomField ecustomField) {
		EcustomFieldModelImpl ecustomFieldModelImpl = (EcustomFieldModelImpl)ecustomField;

		Object[] args = new Object[] {
				ecustomField.getCompanyId(), ecustomField.getTitle()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TITLE, args);

		if ((ecustomFieldModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_TITLE.getColumnBitmask()) != 0) {
			args = new Object[] {
					ecustomFieldModelImpl.getOriginalCompanyId(),
					ecustomFieldModelImpl.getOriginalTitle()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TITLE, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TITLE, args);
		}
	}

	/**
	 * Creates a new ecustom field with the primary key. Does not add the ecustom field to the database.
	 *
	 * @param customFieldRecId the primary key for the new ecustom field
	 * @return the new ecustom field
	 */
	@Override
	public EcustomField create(long customFieldRecId) {
		EcustomField ecustomField = new EcustomFieldImpl();

		ecustomField.setNew(true);
		ecustomField.setPrimaryKey(customFieldRecId);

		return ecustomField;
	}

	/**
	 * Removes the ecustom field with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param customFieldRecId the primary key of the ecustom field
	 * @return the ecustom field that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField remove(long customFieldRecId)
		throws NoSuchEcustomFieldException, SystemException {
		return remove((Serializable)customFieldRecId);
	}

	/**
	 * Removes the ecustom field with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ecustom field
	 * @return the ecustom field that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField remove(Serializable primaryKey)
		throws NoSuchEcustomFieldException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EcustomField ecustomField = (EcustomField)session.get(EcustomFieldImpl.class,
					primaryKey);

			if (ecustomField == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEcustomFieldException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ecustomField);
		}
		catch (NoSuchEcustomFieldException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EcustomField removeImpl(EcustomField ecustomField)
		throws SystemException {
		ecustomField = toUnwrappedModel(ecustomField);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ecustomField)) {
				ecustomField = (EcustomField)session.get(EcustomFieldImpl.class,
						ecustomField.getPrimaryKeyObj());
			}

			if (ecustomField != null) {
				session.delete(ecustomField);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ecustomField != null) {
			clearCache(ecustomField);
		}

		return ecustomField;
	}

	@Override
	public EcustomField updateImpl(
		com.esquare.ecommerce.model.EcustomField ecustomField)
		throws SystemException {
		ecustomField = toUnwrappedModel(ecustomField);

		boolean isNew = ecustomField.isNew();

		EcustomFieldModelImpl ecustomFieldModelImpl = (EcustomFieldModelImpl)ecustomField;

		Session session = null;

		try {
			session = openSession();

			if (ecustomField.isNew()) {
				session.save(ecustomField);

				ecustomField.setNew(false);
			}
			else {
				session.merge(ecustomField);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EcustomFieldModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ecustomFieldModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ecustomFieldModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { ecustomFieldModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		EntityCacheUtil.putResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldImpl.class, ecustomField.getPrimaryKey(), ecustomField);

		clearUniqueFindersCache(ecustomField);
		cacheUniqueFindersCache(ecustomField);

		return ecustomField;
	}

	protected EcustomField toUnwrappedModel(EcustomField ecustomField) {
		if (ecustomField instanceof EcustomFieldImpl) {
			return ecustomField;
		}

		EcustomFieldImpl ecustomFieldImpl = new EcustomFieldImpl();

		ecustomFieldImpl.setNew(ecustomField.isNew());
		ecustomFieldImpl.setPrimaryKey(ecustomField.getPrimaryKey());

		ecustomFieldImpl.setCustomFieldRecId(ecustomField.getCustomFieldRecId());
		ecustomFieldImpl.setCompanyId(ecustomField.getCompanyId());
		ecustomFieldImpl.setTitle(ecustomField.getTitle());
		ecustomFieldImpl.setXml(ecustomField.getXml());

		return ecustomFieldImpl;
	}

	/**
	 * Returns the ecustom field with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecustom field
	 * @return the ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEcustomFieldException, SystemException {
		EcustomField ecustomField = fetchByPrimaryKey(primaryKey);

		if (ecustomField == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEcustomFieldException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ecustomField;
	}

	/**
	 * Returns the ecustom field with the primary key or throws a {@link com.esquare.ecommerce.NoSuchEcustomFieldException} if it could not be found.
	 *
	 * @param customFieldRecId the primary key of the ecustom field
	 * @return the ecustom field
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldException if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField findByPrimaryKey(long customFieldRecId)
		throws NoSuchEcustomFieldException, SystemException {
		return findByPrimaryKey((Serializable)customFieldRecId);
	}

	/**
	 * Returns the ecustom field with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecustom field
	 * @return the ecustom field, or <code>null</code> if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EcustomField ecustomField = (EcustomField)EntityCacheUtil.getResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
				EcustomFieldImpl.class, primaryKey);

		if (ecustomField == _nullEcustomField) {
			return null;
		}

		if (ecustomField == null) {
			Session session = null;

			try {
				session = openSession();

				ecustomField = (EcustomField)session.get(EcustomFieldImpl.class,
						primaryKey);

				if (ecustomField != null) {
					cacheResult(ecustomField);
				}
				else {
					EntityCacheUtil.putResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
						EcustomFieldImpl.class, primaryKey, _nullEcustomField);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EcustomFieldModelImpl.ENTITY_CACHE_ENABLED,
					EcustomFieldImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ecustomField;
	}

	/**
	 * Returns the ecustom field with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param customFieldRecId the primary key of the ecustom field
	 * @return the ecustom field, or <code>null</code> if a ecustom field with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomField fetchByPrimaryKey(long customFieldRecId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)customFieldRecId);
	}

	/**
	 * Returns all the ecustom fields.
	 *
	 * @return the ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ecustom fields.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecustom fields
	 * @param end the upper bound of the range of ecustom fields (not inclusive)
	 * @return the range of ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecustom fields.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecustom fields
	 * @param end the upper bound of the range of ecustom fields (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomField> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EcustomField> list = (List<EcustomField>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ECUSTOMFIELD);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ECUSTOMFIELD;

				if (pagination) {
					sql = sql.concat(EcustomFieldModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EcustomField>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcustomField>(list);
				}
				else {
					list = (List<EcustomField>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ecustom fields from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EcustomField ecustomField : findAll()) {
			remove(ecustomField);
		}
	}

	/**
	 * Returns the number of ecustom fields.
	 *
	 * @return the number of ecustom fields
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ECUSTOMFIELD);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the ecustom field persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.EcustomField")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EcustomField>> listenersList = new ArrayList<ModelListener<EcustomField>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EcustomField>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EcustomFieldImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ECUSTOMFIELD = "SELECT ecustomField FROM EcustomField ecustomField";
	private static final String _SQL_SELECT_ECUSTOMFIELD_WHERE = "SELECT ecustomField FROM EcustomField ecustomField WHERE ";
	private static final String _SQL_COUNT_ECUSTOMFIELD = "SELECT COUNT(ecustomField) FROM EcustomField ecustomField";
	private static final String _SQL_COUNT_ECUSTOMFIELD_WHERE = "SELECT COUNT(ecustomField) FROM EcustomField ecustomField WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ecustomField.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EcustomField exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EcustomField exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EcustomFieldPersistenceImpl.class);
	private static EcustomField _nullEcustomField = new EcustomFieldImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EcustomField> toCacheModel() {
				return _nullEcustomFieldCacheModel;
			}
		};

	private static CacheModel<EcustomField> _nullEcustomFieldCacheModel = new CacheModel<EcustomField>() {
			@Override
			public EcustomField toEntityModel() {
				return _nullEcustomField;
			}
		};
}