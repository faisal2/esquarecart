/**
 * Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.displayproduct.action;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
/**
 * @author Jorge Ferrer
 * @author Alberto Montero
 * @author Julio Camarero
 * @author Brian Wing Shun Chan
 */

public class ConfigurationActionImpl extends DefaultConfigurationAction {

	public void processAction(
			PortletConfig portletConfig, ActionRequest actionRequest,
			ActionResponse actionResponse)
		throws Exception {
		String cmd = ParamUtil.getString(actionRequest, "cmd");
		
		
		if (cmd.equals("ajaxCall")) {
			ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			try {
				String parentCatalogId = ParamUtil.getString(actionRequest, "parentCatalogId");
			
				int count =CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), Long.parseLong(parentCatalogId));
			    List<Catalog> subChildResults = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId(), Long.parseLong(parentCatalogId),0,count);
			    actionRequest.setAttribute("SUB_CATEGORY_RESULT", subChildResults);
			} catch (Exception e) {
				_log.info(e.getClass());
			} 
		} else {
			String type = ParamUtil.getString(actionRequest, "type");
			String viewCount = ParamUtil.getString(actionRequest, "viewCount");
			String parentCatalogId = ParamUtil.getString(actionRequest, "parentCatalogId");
			String priceRange = ParamUtil.getString(actionRequest, "priceRange");
			String portletResource = ParamUtil.getString(actionRequest, "portletResource");

			PortletPreferences preferences =PortletPreferencesFactoryUtil.getPortletSetup(actionRequest, portletResource);
			
			preferences.setValue("viewCount", viewCount);
			preferences.setValue("type", type);
			preferences.setValue("parentCatalogId", parentCatalogId);
			preferences.setValue("priceRange", priceRange);
			


			preferences.store();
			SessionMessages.add(
					actionRequest, portletConfig.getPortletName() + ".doConfigure");
		}
		
	}
	
	public String render(
			PortletConfig portletConfig, RenderRequest renderRequest,
			RenderResponse renderResponse)
		throws Exception {

			return "/html/eec/common/display_product/configuration.jsp";
	}
	private static Log _log=LogFactoryUtil.getLog(ConfigurationActionImpl.class);
}