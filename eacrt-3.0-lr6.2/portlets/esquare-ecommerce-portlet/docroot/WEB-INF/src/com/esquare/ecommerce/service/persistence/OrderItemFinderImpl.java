package com.esquare.ecommerce.service.persistence;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.impl.OrderImpl;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class OrderItemFinderImpl extends BasePersistenceImpl<OrderItem>
		implements OrderItemFinder {

	public static String GET_PURCHASED_PRODUCT_COUNT = "getPurchasedProductCount";
	public static String GET_ORDER_ITEM_STATUS_COUNT = "getOrderItemStatusCount";
	public static String GET_ORDER_ITEM_STATUS_CANCELLED_COUNT = "getOrderItemStatusCancelledCount";
	public static String GET_TOTAL_AMOUNT_ORDER = "getTotalAmountOrder";
	public static String GET_LASTWEEK_MONTH_ORDER_AMOUNT_DAYWISE = "getLastWeekMonthOrderAmountDaywise";
	public static String GET_LASTWEEK_MONTH_ORDER_AMOUNT = "getLastWeekMonthOrderAmount";
	public static String GET_TODAY_ORDER_AMOUNT = "getTodayOrderAmount";
	public static String GET_TODAY_PURCHASED_ORDER_ITEM = "getTodayPurchasedOrderItem";
	public static String GET_PURCHASED_LASTWEEK_MONTH_PRODUCT_COUNT = "getPurchasedLastweekMonthProductCount";
	public static String GET_YESTERDAY_ORDER_AMOUNT = "getYesterdayOrderAmount";
	public static String GET_ORDER_ITEM_STATUS_LIST = "getOrderItemStatusList";

	public int getPurchasedProductCount(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_PURCHASED_PRODUCT_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("PURCHASED_PRODUCT_COUNT", Type.INTEGER);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());

		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public int getOrderItemStatusCount(String status, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = null;
			if (status.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)) {
				sql = CustomSQLUtil.get(GET_ORDER_ITEM_STATUS_CANCELLED_COUNT);
			} else {
				sql = CustomSQLUtil.get(GET_ORDER_ITEM_STATUS_COUNT);
			}
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("ORDER_ITEM_STATUS_COUNT", Type.INTEGER);
			QueryPos qPos = QueryPos.getInstance(query);
			if (!status.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED))
				qPos.add(status);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());

		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public long getTotalAmountOrder(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_TOTAL_AMOUNT_ORDER);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("TOTAL_AMOUNT_ORDER", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Long.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public Map<String, String> getLastWeekMonthOrderAmountDaywise(int day,
			String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		Map<String, String> getLastWeekMonthOrderAmountDaywise = new LinkedHashMap<String, String>();
		try {
			String sql = CustomSQLUtil
					.get(GET_LASTWEEK_MONTH_ORDER_AMOUNT_DAYWISE);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("THE_DATE", Type.STRING);
			query.addScalar("SUM_OF_ORDER", Type.STRING);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(day);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				String theDate = (String) array[0];
				String sumofOrder = (String) array[1];
				getLastWeekMonthOrderAmountDaywise.put(theDate, sumofOrder);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return getLastWeekMonthOrderAmountDaywise;
	}

	public Double getLastWeekMonthOrderAmount(int day, String isNotStatus,
			long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_LASTWEEK_MONTH_ORDER_AMOUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("SUM_OF_ORDER", Type.DOUBLE);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(day);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Double.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0d;
	}

	public long getTodayOrderAmount(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_TODAY_ORDER_AMOUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("TODAY_ORDER_AMOUNT", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Long.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public int getTodayPurchasedOrderItem(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_TODAY_PURCHASED_ORDER_ITEM);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("TODAY_PURCHASED_ORDER_ITEM", Type.INTEGER);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public int getPurchasedLastweekMonthProductCount(int day,
			String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil
					.get(GET_PURCHASED_LASTWEEK_MONTH_PRODUCT_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("WEEK_MONTH_PURCHASED_PRODUCT_COUNT", Type.INTEGER);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(day);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public long getYesterdayOrderAmount(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_YESTERDAY_ORDER_AMOUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("YESTERDAY_ORDER_AMOUNT", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Long.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	@SuppressWarnings("unchecked")
	public List<Order> getOrderItemStatusList(long companyId,
			String orderItemStatus) {
		Session session = null;
		SQLQuery query = null;
		String customQuery = GET_ORDER_ITEM_STATUS_LIST;
		try {
			String sql = CustomSQLUtil.get(customQuery);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addEntity("Order", OrderImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(companyId);
			qPos.add(orderItemStatus);
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List<Order>) query.list();
	}
	private static Log _log = LogFactoryUtil.getLog(OrderItemFinderImpl.class);
}