package com.esquare.ecommerce.model.bulkdataupload;

import java.util.List;

public class UploadCustomModel {

	private String skuNo;
	private String customFieldName;
	private List<UploadTitleModel> uploadTitleModel;
	public String getSkuNo() {
		return skuNo;
	}
	public void setSkuNo(String string) {
		this.skuNo = string;
	}
	public String getCustomFieldName() {
		return customFieldName;
	}
	public void setCustomFieldName(String string) {
		this.customFieldName = string;
	}
	public List<UploadTitleModel> getUploadTitleModel() {
		return uploadTitleModel;
	}
	public void setUploadTitleModel(List<UploadTitleModel> uploadTitleModel) {
		this.uploadTitleModel = uploadTitleModel;
	}
	
}
