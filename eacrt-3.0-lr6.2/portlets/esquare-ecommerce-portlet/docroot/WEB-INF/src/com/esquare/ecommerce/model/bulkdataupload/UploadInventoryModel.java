package com.esquare.ecommerce.model.bulkdataupload;

public class UploadInventoryModel {
	
	private String SKU;
	private long price;
	private double discount;
	private long quantity;
	private boolean Taxable;
	private boolean freeShipping;
	private double weightage;
	private boolean outOfStockPurchase;
	private String imageURL;
	public String getSKU() {
		return SKU;
	}
	public void setSKU(String sKU) {
		SKU = sKU;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double d) {
		this.discount = d;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public boolean isTaxable() {
		return Taxable;
	}
	public void setTaxable(boolean taxable) {
		Taxable = taxable;
	}
	public boolean isFreeShipping() {
		return freeShipping;
	}
	public void setFreeShipping(boolean freeShipping) {
		this.freeShipping = freeShipping;
	}
	public double getWeightage() {
		return weightage;
	}
	public void setWeightage(double weightage) {
		this.weightage = weightage;
	}
	public boolean isOutOfStockPurchase() {
		return outOfStockPurchase;
	}
	public void setOutOfStockPurchase(boolean outOfStockPurchase) {
		this.outOfStockPurchase = outOfStockPurchase;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

}
