/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.PaymentUserDetail;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing PaymentUserDetail in entity cache.
 *
 * @author Esquare
 * @see PaymentUserDetail
 * @generated
 */
public class PaymentUserDetailCacheModel implements CacheModel<PaymentUserDetail>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{recId=");
		sb.append(recId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", email=");
		sb.append(email);
		sb.append(", name=");
		sb.append(name);
		sb.append(", phoneNo=");
		sb.append(phoneNo);
		sb.append(", pinCode=");
		sb.append(pinCode);
		sb.append(", address=");
		sb.append(address);
		sb.append(", country=");
		sb.append(country);
		sb.append(", state=");
		sb.append(state);
		sb.append(", city=");
		sb.append(city);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PaymentUserDetail toEntityModel() {
		PaymentUserDetailImpl paymentUserDetailImpl = new PaymentUserDetailImpl();

		paymentUserDetailImpl.setRecId(recId);
		paymentUserDetailImpl.setCompanyId(companyId);
		paymentUserDetailImpl.setUserId(userId);

		if (email == null) {
			paymentUserDetailImpl.setEmail(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setEmail(email);
		}

		if (name == null) {
			paymentUserDetailImpl.setName(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setName(name);
		}

		paymentUserDetailImpl.setPhoneNo(phoneNo);
		paymentUserDetailImpl.setPinCode(pinCode);

		if (address == null) {
			paymentUserDetailImpl.setAddress(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setAddress(address);
		}

		if (country == null) {
			paymentUserDetailImpl.setCountry(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setCountry(country);
		}

		if (state == null) {
			paymentUserDetailImpl.setState(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setState(state);
		}

		if (city == null) {
			paymentUserDetailImpl.setCity(StringPool.BLANK);
		}
		else {
			paymentUserDetailImpl.setCity(city);
		}

		paymentUserDetailImpl.resetOriginalValues();

		return paymentUserDetailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		email = objectInput.readUTF();
		name = objectInput.readUTF();
		phoneNo = objectInput.readLong();
		pinCode = objectInput.readLong();
		address = objectInput.readUTF();
		country = objectInput.readUTF();
		state = objectInput.readUTF();
		city = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeLong(phoneNo);
		objectOutput.writeLong(pinCode);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}

		if (country == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(country);
		}

		if (state == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(state);
		}

		if (city == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(city);
		}
	}

	public long recId;
	public long companyId;
	public long userId;
	public String email;
	public String name;
	public long phoneNo;
	public long pinCode;
	public String address;
	public String country;
	public String state;
	public String city;
}