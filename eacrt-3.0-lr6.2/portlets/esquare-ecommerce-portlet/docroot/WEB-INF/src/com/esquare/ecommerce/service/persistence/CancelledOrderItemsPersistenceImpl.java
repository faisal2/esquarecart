/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchCancelledOrderItemsException;
import com.esquare.ecommerce.model.CancelledOrderItems;
import com.esquare.ecommerce.model.impl.CancelledOrderItemsImpl;
import com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cancelled order items service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see CancelledOrderItemsPersistence
 * @see CancelledOrderItemsUtil
 * @generated
 */
public class CancelledOrderItemsPersistenceImpl extends BasePersistenceImpl<CancelledOrderItems>
	implements CancelledOrderItemsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CancelledOrderItemsUtil} to access the cancelled order items persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CancelledOrderItemsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_U = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_U",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_U",
			new String[] { Long.class.getName(), Long.class.getName() },
			CancelledOrderItemsModelImpl.COMPANYID_COLUMN_BITMASK |
			CancelledOrderItemsModelImpl.USERID_COLUMN_BITMASK |
			CancelledOrderItemsModelImpl.CANCELLEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_U = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_U",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the cancelled order itemses where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC_U(long companyId, long userId)
		throws SystemException {
		return findByC_U(companyId, userId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cancelled order itemses where companyId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @return the range of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC_U(long companyId, long userId,
		int start, int end) throws SystemException {
		return findByC_U(companyId, userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the cancelled order itemses where companyId = &#63; and userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC_U(long companyId, long userId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U;
			finderArgs = new Object[] { companyId, userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_U;
			finderArgs = new Object[] {
					companyId, userId,
					
					start, end, orderByComparator
				};
		}

		List<CancelledOrderItems> list = (List<CancelledOrderItems>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CancelledOrderItems cancelledOrderItems : list) {
				if ((companyId != cancelledOrderItems.getCompanyId()) ||
						(userId != cancelledOrderItems.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CANCELLEDORDERITEMS_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CancelledOrderItemsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CancelledOrderItems>(list);
				}
				else {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first cancelled order items in the ordered set where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByC_U_First(long companyId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = fetchByC_U_First(companyId,
				userId, orderByComparator);

		if (cancelledOrderItems != null) {
			return cancelledOrderItems;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCancelledOrderItemsException(msg.toString());
	}

	/**
	 * Returns the first cancelled order items in the ordered set where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cancelled order items, or <code>null</code> if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByC_U_First(long companyId, long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CancelledOrderItems> list = findByC_U(companyId, userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last cancelled order items in the ordered set where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByC_U_Last(long companyId, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = fetchByC_U_Last(companyId,
				userId, orderByComparator);

		if (cancelledOrderItems != null) {
			return cancelledOrderItems;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCancelledOrderItemsException(msg.toString());
	}

	/**
	 * Returns the last cancelled order items in the ordered set where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cancelled order items, or <code>null</code> if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByC_U_Last(long companyId, long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_U(companyId, userId);

		if (count == 0) {
			return null;
		}

		List<CancelledOrderItems> list = findByC_U(companyId, userId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the cancelled order itemses before and after the current cancelled order items in the ordered set where companyId = &#63; and userId = &#63;.
	 *
	 * @param orderItemId the primary key of the current cancelled order items
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems[] findByC_U_PrevAndNext(long orderItemId,
		long companyId, long userId, OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = findByPrimaryKey(orderItemId);

		Session session = null;

		try {
			session = openSession();

			CancelledOrderItems[] array = new CancelledOrderItemsImpl[3];

			array[0] = getByC_U_PrevAndNext(session, cancelledOrderItems,
					companyId, userId, orderByComparator, true);

			array[1] = cancelledOrderItems;

			array[2] = getByC_U_PrevAndNext(session, cancelledOrderItems,
					companyId, userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CancelledOrderItems getByC_U_PrevAndNext(Session session,
		CancelledOrderItems cancelledOrderItems, long companyId, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CANCELLEDORDERITEMS_WHERE);

		query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_U_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CancelledOrderItemsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(cancelledOrderItems);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CancelledOrderItems> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the cancelled order itemses where companyId = &#63; and userId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_U(long companyId, long userId)
		throws SystemException {
		for (CancelledOrderItems cancelledOrderItems : findByC_U(companyId,
				userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(cancelledOrderItems);
		}
	}

	/**
	 * Returns the number of cancelled order itemses where companyId = &#63; and userId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param userId the user ID
	 * @return the number of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_U(long companyId, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_U;

		Object[] finderArgs = new Object[] { companyId, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CANCELLEDORDERITEMS_WHERE);

			query.append(_FINDER_COLUMN_C_U_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_U_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_U_COMPANYID_2 = "cancelledOrderItems.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_U_USERID_2 = "cancelledOrderItems.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED,
			CancelledOrderItemsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC",
			new String[] { Long.class.getName() },
			CancelledOrderItemsModelImpl.COMPANYID_COLUMN_BITMASK |
			CancelledOrderItemsModelImpl.CANCELLEDDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C = new FinderPath(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the cancelled order itemses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC(long companyId)
		throws SystemException {
		return findByC(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cancelled order itemses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @return the range of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC(long companyId, int start, int end)
		throws SystemException {
		return findByC(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the cancelled order itemses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findByC(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<CancelledOrderItems> list = (List<CancelledOrderItems>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CancelledOrderItems cancelledOrderItems : list) {
				if ((companyId != cancelledOrderItems.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CANCELLEDORDERITEMS_WHERE);

			query.append(_FINDER_COLUMN_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CancelledOrderItemsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CancelledOrderItems>(list);
				}
				else {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first cancelled order items in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByC_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = fetchByC_First(companyId,
				orderByComparator);

		if (cancelledOrderItems != null) {
			return cancelledOrderItems;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCancelledOrderItemsException(msg.toString());
	}

	/**
	 * Returns the first cancelled order items in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching cancelled order items, or <code>null</code> if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByC_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CancelledOrderItems> list = findByC(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last cancelled order items in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByC_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = fetchByC_Last(companyId,
				orderByComparator);

		if (cancelledOrderItems != null) {
			return cancelledOrderItems;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCancelledOrderItemsException(msg.toString());
	}

	/**
	 * Returns the last cancelled order items in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching cancelled order items, or <code>null</code> if a matching cancelled order items could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByC_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC(companyId);

		if (count == 0) {
			return null;
		}

		List<CancelledOrderItems> list = findByC(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the cancelled order itemses before and after the current cancelled order items in the ordered set where companyId = &#63;.
	 *
	 * @param orderItemId the primary key of the current cancelled order items
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems[] findByC_PrevAndNext(long orderItemId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = findByPrimaryKey(orderItemId);

		Session session = null;

		try {
			session = openSession();

			CancelledOrderItems[] array = new CancelledOrderItemsImpl[3];

			array[0] = getByC_PrevAndNext(session, cancelledOrderItems,
					companyId, orderByComparator, true);

			array[1] = cancelledOrderItems;

			array[2] = getByC_PrevAndNext(session, cancelledOrderItems,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CancelledOrderItems getByC_PrevAndNext(Session session,
		CancelledOrderItems cancelledOrderItems, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CANCELLEDORDERITEMS_WHERE);

		query.append(_FINDER_COLUMN_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CancelledOrderItemsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(cancelledOrderItems);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CancelledOrderItems> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the cancelled order itemses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC(long companyId) throws SystemException {
		for (CancelledOrderItems cancelledOrderItems : findByC(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(cancelledOrderItems);
		}
	}

	/**
	 * Returns the number of cancelled order itemses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CANCELLEDORDERITEMS_WHERE);

			query.append(_FINDER_COLUMN_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_COMPANYID_2 = "cancelledOrderItems.companyId = ?";

	public CancelledOrderItemsPersistenceImpl() {
		setModelClass(CancelledOrderItems.class);
	}

	/**
	 * Caches the cancelled order items in the entity cache if it is enabled.
	 *
	 * @param cancelledOrderItems the cancelled order items
	 */
	@Override
	public void cacheResult(CancelledOrderItems cancelledOrderItems) {
		EntityCacheUtil.putResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsImpl.class, cancelledOrderItems.getPrimaryKey(),
			cancelledOrderItems);

		cancelledOrderItems.resetOriginalValues();
	}

	/**
	 * Caches the cancelled order itemses in the entity cache if it is enabled.
	 *
	 * @param cancelledOrderItemses the cancelled order itemses
	 */
	@Override
	public void cacheResult(List<CancelledOrderItems> cancelledOrderItemses) {
		for (CancelledOrderItems cancelledOrderItems : cancelledOrderItemses) {
			if (EntityCacheUtil.getResult(
						CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
						CancelledOrderItemsImpl.class,
						cancelledOrderItems.getPrimaryKey()) == null) {
				cacheResult(cancelledOrderItems);
			}
			else {
				cancelledOrderItems.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all cancelled order itemses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CancelledOrderItemsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CancelledOrderItemsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the cancelled order items.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CancelledOrderItems cancelledOrderItems) {
		EntityCacheUtil.removeResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsImpl.class, cancelledOrderItems.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CancelledOrderItems> cancelledOrderItemses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CancelledOrderItems cancelledOrderItems : cancelledOrderItemses) {
			EntityCacheUtil.removeResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
				CancelledOrderItemsImpl.class,
				cancelledOrderItems.getPrimaryKey());
		}
	}

	/**
	 * Creates a new cancelled order items with the primary key. Does not add the cancelled order items to the database.
	 *
	 * @param orderItemId the primary key for the new cancelled order items
	 * @return the new cancelled order items
	 */
	@Override
	public CancelledOrderItems create(long orderItemId) {
		CancelledOrderItems cancelledOrderItems = new CancelledOrderItemsImpl();

		cancelledOrderItems.setNew(true);
		cancelledOrderItems.setPrimaryKey(orderItemId);

		return cancelledOrderItems;
	}

	/**
	 * Removes the cancelled order items with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param orderItemId the primary key of the cancelled order items
	 * @return the cancelled order items that was removed
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems remove(long orderItemId)
		throws NoSuchCancelledOrderItemsException, SystemException {
		return remove((Serializable)orderItemId);
	}

	/**
	 * Removes the cancelled order items with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the cancelled order items
	 * @return the cancelled order items that was removed
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems remove(Serializable primaryKey)
		throws NoSuchCancelledOrderItemsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CancelledOrderItems cancelledOrderItems = (CancelledOrderItems)session.get(CancelledOrderItemsImpl.class,
					primaryKey);

			if (cancelledOrderItems == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCancelledOrderItemsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(cancelledOrderItems);
		}
		catch (NoSuchCancelledOrderItemsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CancelledOrderItems removeImpl(
		CancelledOrderItems cancelledOrderItems) throws SystemException {
		cancelledOrderItems = toUnwrappedModel(cancelledOrderItems);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(cancelledOrderItems)) {
				cancelledOrderItems = (CancelledOrderItems)session.get(CancelledOrderItemsImpl.class,
						cancelledOrderItems.getPrimaryKeyObj());
			}

			if (cancelledOrderItems != null) {
				session.delete(cancelledOrderItems);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (cancelledOrderItems != null) {
			clearCache(cancelledOrderItems);
		}

		return cancelledOrderItems;
	}

	@Override
	public CancelledOrderItems updateImpl(
		com.esquare.ecommerce.model.CancelledOrderItems cancelledOrderItems)
		throws SystemException {
		cancelledOrderItems = toUnwrappedModel(cancelledOrderItems);

		boolean isNew = cancelledOrderItems.isNew();

		CancelledOrderItemsModelImpl cancelledOrderItemsModelImpl = (CancelledOrderItemsModelImpl)cancelledOrderItems;

		Session session = null;

		try {
			session = openSession();

			if (cancelledOrderItems.isNew()) {
				session.save(cancelledOrderItems);

				cancelledOrderItems.setNew(false);
			}
			else {
				session.merge(cancelledOrderItems);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CancelledOrderItemsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((cancelledOrderItemsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						cancelledOrderItemsModelImpl.getOriginalCompanyId(),
						cancelledOrderItemsModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U,
					args);

				args = new Object[] {
						cancelledOrderItemsModelImpl.getCompanyId(),
						cancelledOrderItemsModelImpl.getUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_U, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_U,
					args);
			}

			if ((cancelledOrderItemsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						cancelledOrderItemsModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C,
					args);

				args = new Object[] { cancelledOrderItemsModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C,
					args);
			}
		}

		EntityCacheUtil.putResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
			CancelledOrderItemsImpl.class, cancelledOrderItems.getPrimaryKey(),
			cancelledOrderItems);

		return cancelledOrderItems;
	}

	protected CancelledOrderItems toUnwrappedModel(
		CancelledOrderItems cancelledOrderItems) {
		if (cancelledOrderItems instanceof CancelledOrderItemsImpl) {
			return cancelledOrderItems;
		}

		CancelledOrderItemsImpl cancelledOrderItemsImpl = new CancelledOrderItemsImpl();

		cancelledOrderItemsImpl.setNew(cancelledOrderItems.isNew());
		cancelledOrderItemsImpl.setPrimaryKey(cancelledOrderItems.getPrimaryKey());

		cancelledOrderItemsImpl.setOrderItemId(cancelledOrderItems.getOrderItemId());
		cancelledOrderItemsImpl.setCompanyId(cancelledOrderItems.getCompanyId());
		cancelledOrderItemsImpl.setUserId(cancelledOrderItems.getUserId());
		cancelledOrderItemsImpl.setOrderId(cancelledOrderItems.getOrderId());
		cancelledOrderItemsImpl.setPrice(cancelledOrderItems.getPrice());
		cancelledOrderItemsImpl.setQuantity(cancelledOrderItems.getQuantity());
		cancelledOrderItemsImpl.setCancelledDate(cancelledOrderItems.getCancelledDate());

		return cancelledOrderItemsImpl;
	}

	/**
	 * Returns the cancelled order items with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the cancelled order items
	 * @return the cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCancelledOrderItemsException, SystemException {
		CancelledOrderItems cancelledOrderItems = fetchByPrimaryKey(primaryKey);

		if (cancelledOrderItems == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCancelledOrderItemsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return cancelledOrderItems;
	}

	/**
	 * Returns the cancelled order items with the primary key or throws a {@link com.esquare.ecommerce.NoSuchCancelledOrderItemsException} if it could not be found.
	 *
	 * @param orderItemId the primary key of the cancelled order items
	 * @return the cancelled order items
	 * @throws com.esquare.ecommerce.NoSuchCancelledOrderItemsException if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems findByPrimaryKey(long orderItemId)
		throws NoSuchCancelledOrderItemsException, SystemException {
		return findByPrimaryKey((Serializable)orderItemId);
	}

	/**
	 * Returns the cancelled order items with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the cancelled order items
	 * @return the cancelled order items, or <code>null</code> if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CancelledOrderItems cancelledOrderItems = (CancelledOrderItems)EntityCacheUtil.getResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
				CancelledOrderItemsImpl.class, primaryKey);

		if (cancelledOrderItems == _nullCancelledOrderItems) {
			return null;
		}

		if (cancelledOrderItems == null) {
			Session session = null;

			try {
				session = openSession();

				cancelledOrderItems = (CancelledOrderItems)session.get(CancelledOrderItemsImpl.class,
						primaryKey);

				if (cancelledOrderItems != null) {
					cacheResult(cancelledOrderItems);
				}
				else {
					EntityCacheUtil.putResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
						CancelledOrderItemsImpl.class, primaryKey,
						_nullCancelledOrderItems);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CancelledOrderItemsModelImpl.ENTITY_CACHE_ENABLED,
					CancelledOrderItemsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return cancelledOrderItems;
	}

	/**
	 * Returns the cancelled order items with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param orderItemId the primary key of the cancelled order items
	 * @return the cancelled order items, or <code>null</code> if a cancelled order items with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CancelledOrderItems fetchByPrimaryKey(long orderItemId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)orderItemId);
	}

	/**
	 * Returns all the cancelled order itemses.
	 *
	 * @return the cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the cancelled order itemses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @return the range of cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the cancelled order itemses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.CancelledOrderItemsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of cancelled order itemses
	 * @param end the upper bound of the range of cancelled order itemses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CancelledOrderItems> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CancelledOrderItems> list = (List<CancelledOrderItems>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CANCELLEDORDERITEMS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CANCELLEDORDERITEMS;

				if (pagination) {
					sql = sql.concat(CancelledOrderItemsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CancelledOrderItems>(list);
				}
				else {
					list = (List<CancelledOrderItems>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the cancelled order itemses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CancelledOrderItems cancelledOrderItems : findAll()) {
			remove(cancelledOrderItems);
		}
	}

	/**
	 * Returns the number of cancelled order itemses.
	 *
	 * @return the number of cancelled order itemses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CANCELLEDORDERITEMS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the cancelled order items persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.CancelledOrderItems")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CancelledOrderItems>> listenersList = new ArrayList<ModelListener<CancelledOrderItems>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CancelledOrderItems>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CancelledOrderItemsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CANCELLEDORDERITEMS = "SELECT cancelledOrderItems FROM CancelledOrderItems cancelledOrderItems";
	private static final String _SQL_SELECT_CANCELLEDORDERITEMS_WHERE = "SELECT cancelledOrderItems FROM CancelledOrderItems cancelledOrderItems WHERE ";
	private static final String _SQL_COUNT_CANCELLEDORDERITEMS = "SELECT COUNT(cancelledOrderItems) FROM CancelledOrderItems cancelledOrderItems";
	private static final String _SQL_COUNT_CANCELLEDORDERITEMS_WHERE = "SELECT COUNT(cancelledOrderItems) FROM CancelledOrderItems cancelledOrderItems WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "cancelledOrderItems.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CancelledOrderItems exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CancelledOrderItems exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CancelledOrderItemsPersistenceImpl.class);
	private static CancelledOrderItems _nullCancelledOrderItems = new CancelledOrderItemsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CancelledOrderItems> toCacheModel() {
				return _nullCancelledOrderItemsCacheModel;
			}
		};

	private static CacheModel<CancelledOrderItems> _nullCancelledOrderItemsCacheModel =
		new CacheModel<CancelledOrderItems>() {
			@Override
			public CancelledOrderItems toEntityModel() {
				return _nullCancelledOrderItems;
			}
		};
}