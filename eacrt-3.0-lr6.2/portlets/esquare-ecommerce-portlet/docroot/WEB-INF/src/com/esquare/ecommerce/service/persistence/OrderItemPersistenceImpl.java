/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchOrderItemException;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.impl.OrderItemImpl;
import com.esquare.ecommerce.model.impl.OrderItemModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the order item service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see OrderItemPersistence
 * @see OrderItemUtil
 * @generated
 */
public class OrderItemPersistenceImpl extends BasePersistenceImpl<OrderItem>
	implements OrderItemPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link OrderItemUtil} to access the order item persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = OrderItemImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERID = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOrderId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID =
		new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOrderId",
			new String[] { Long.class.getName() },
			OrderItemModelImpl.ORDERID_COLUMN_BITMASK |
			OrderItemModelImpl.NAME_COLUMN_BITMASK |
			OrderItemModelImpl.DESCRIPTION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDERID = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrderId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the order items where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByOrderId(long orderId)
		throws SystemException {
		return findByOrderId(orderId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order items where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @return the range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByOrderId(long orderId, int start, int end)
		throws SystemException {
		return findByOrderId(orderId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the order items where orderId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByOrderId(long orderId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID;
			finderArgs = new Object[] { orderId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERID;
			finderArgs = new Object[] { orderId, start, end, orderByComparator };
		}

		List<OrderItem> list = (List<OrderItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrderItem orderItem : list) {
				if ((orderId != orderItem.getOrderId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (!pagination) {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderItem>(list);
				}
				else {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByOrderId_First(long orderId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByOrderId_First(orderId, orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByOrderId_First(long orderId,
		OrderByComparator orderByComparator) throws SystemException {
		List<OrderItem> list = findByOrderId(orderId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByOrderId_Last(long orderId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByOrderId_Last(orderId, orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByOrderId_Last(long orderId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByOrderId(orderId);

		if (count == 0) {
			return null;
		}

		List<OrderItem> list = findByOrderId(orderId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the order items before and after the current order item in the ordered set where orderId = &#63;.
	 *
	 * @param orderItemId the primary key of the current order item
	 * @param orderId the order ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem[] findByOrderId_PrevAndNext(long orderItemId,
		long orderId, OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = findByPrimaryKey(orderItemId);

		Session session = null;

		try {
			session = openSession();

			OrderItem[] array = new OrderItemImpl[3];

			array[0] = getByOrderId_PrevAndNext(session, orderItem, orderId,
					orderByComparator, true);

			array[1] = orderItem;

			array[2] = getByOrderId_PrevAndNext(session, orderItem, orderId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrderItem getByOrderId_PrevAndNext(Session session,
		OrderItem orderItem, long orderId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDERITEM_WHERE);

		query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(orderId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(orderItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrderItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the order items where orderId = &#63; from the database.
	 *
	 * @param orderId the order ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByOrderId(long orderId) throws SystemException {
		for (OrderItem orderItem : findByOrderId(orderId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(orderItem);
		}
	}

	/**
	 * Returns the number of order items where orderId = &#63;.
	 *
	 * @param orderId the order ID
	 * @return the number of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByOrderId(long orderId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDERID;

		Object[] finderArgs = new Object[] { orderId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERID_ORDERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDERID_ORDERID_2 = "orderItem.orderId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_O_I_S = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByO_I_S",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_O_I_S = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByO_I_S",
			new String[] { Long.class.getName(), String.class.getName() },
			OrderItemModelImpl.ORDERID_COLUMN_BITMASK |
			OrderItemModelImpl.ORDERITEMSTATUS_COLUMN_BITMASK |
			OrderItemModelImpl.NAME_COLUMN_BITMASK |
			OrderItemModelImpl.DESCRIPTION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_O_I_S = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByO_I_S",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the order items where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @return the matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByO_I_S(long orderId, String orderItemStatus)
		throws SystemException {
		return findByO_I_S(orderId, orderItemStatus, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order items where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @return the range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByO_I_S(long orderId, String orderItemStatus,
		int start, int end) throws SystemException {
		return findByO_I_S(orderId, orderItemStatus, start, end, null);
	}

	/**
	 * Returns an ordered range of all the order items where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByO_I_S(long orderId, String orderItemStatus,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_O_I_S;
			finderArgs = new Object[] { orderId, orderItemStatus };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_O_I_S;
			finderArgs = new Object[] {
					orderId, orderItemStatus,
					
					start, end, orderByComparator
				};
		}

		List<OrderItem> list = (List<OrderItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrderItem orderItem : list) {
				if ((orderId != orderItem.getOrderId()) ||
						!Validator.equals(orderItemStatus,
							orderItem.getOrderItemStatus())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_O_I_S_ORDERID_2);

			boolean bindOrderItemStatus = false;

			if (orderItemStatus == null) {
				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_1);
			}
			else if (orderItemStatus.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_3);
			}
			else {
				bindOrderItemStatus = true;

				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (bindOrderItemStatus) {
					qPos.add(orderItemStatus);
				}

				if (!pagination) {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderItem>(list);
				}
				else {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByO_I_S_First(long orderId, String orderItemStatus,
		OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByO_I_S_First(orderId, orderItemStatus,
				orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(", orderItemStatus=");
		msg.append(orderItemStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByO_I_S_First(long orderId, String orderItemStatus,
		OrderByComparator orderByComparator) throws SystemException {
		List<OrderItem> list = findByO_I_S(orderId, orderItemStatus, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByO_I_S_Last(long orderId, String orderItemStatus,
		OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByO_I_S_Last(orderId, orderItemStatus,
				orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(", orderItemStatus=");
		msg.append(orderItemStatus);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByO_I_S_Last(long orderId, String orderItemStatus,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByO_I_S(orderId, orderItemStatus);

		if (count == 0) {
			return null;
		}

		List<OrderItem> list = findByO_I_S(orderId, orderItemStatus, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the order items before and after the current order item in the ordered set where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderItemId the primary key of the current order item
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem[] findByO_I_S_PrevAndNext(long orderItemId, long orderId,
		String orderItemStatus, OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = findByPrimaryKey(orderItemId);

		Session session = null;

		try {
			session = openSession();

			OrderItem[] array = new OrderItemImpl[3];

			array[0] = getByO_I_S_PrevAndNext(session, orderItem, orderId,
					orderItemStatus, orderByComparator, true);

			array[1] = orderItem;

			array[2] = getByO_I_S_PrevAndNext(session, orderItem, orderId,
					orderItemStatus, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrderItem getByO_I_S_PrevAndNext(Session session,
		OrderItem orderItem, long orderId, String orderItemStatus,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDERITEM_WHERE);

		query.append(_FINDER_COLUMN_O_I_S_ORDERID_2);

		boolean bindOrderItemStatus = false;

		if (orderItemStatus == null) {
			query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_1);
		}
		else if (orderItemStatus.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_3);
		}
		else {
			bindOrderItemStatus = true;

			query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(orderId);

		if (bindOrderItemStatus) {
			qPos.add(orderItemStatus);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(orderItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrderItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the order items where orderId = &#63; and orderItemStatus = &#63; from the database.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByO_I_S(long orderId, String orderItemStatus)
		throws SystemException {
		for (OrderItem orderItem : findByO_I_S(orderId, orderItemStatus,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(orderItem);
		}
	}

	/**
	 * Returns the number of order items where orderId = &#63; and orderItemStatus = &#63;.
	 *
	 * @param orderId the order ID
	 * @param orderItemStatus the order item status
	 * @return the number of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByO_I_S(long orderId, String orderItemStatus)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_O_I_S;

		Object[] finderArgs = new Object[] { orderId, orderItemStatus };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_O_I_S_ORDERID_2);

			boolean bindOrderItemStatus = false;

			if (orderItemStatus == null) {
				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_1);
			}
			else if (orderItemStatus.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_3);
			}
			else {
				bindOrderItemStatus = true;

				query.append(_FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (bindOrderItemStatus) {
					qPos.add(orderItemStatus);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_O_I_S_ORDERID_2 = "orderItem.orderId = ? AND ";
	private static final String _FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_1 = "orderItem.orderItemStatus IS NULL";
	private static final String _FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_2 = "orderItem.orderItemStatus = ?";
	private static final String _FINDER_COLUMN_O_I_S_ORDERITEMSTATUS_3 = "(orderItem.orderItemStatus IS NULL OR orderItem.orderItemStatus = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERITEMID =
		new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByorderItemId",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERITEMID =
		new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, OrderItemImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByorderItemId",
			new String[] { Long.class.getName(), String.class.getName() },
			OrderItemModelImpl.ORDERID_COLUMN_BITMASK |
			OrderItemModelImpl.PRODUCTINVENTORYID_COLUMN_BITMASK |
			OrderItemModelImpl.NAME_COLUMN_BITMASK |
			OrderItemModelImpl.DESCRIPTION_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ORDERITEMID = new FinderPath(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByorderItemId",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the order items where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @return the matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByorderItemId(long orderId,
		String productInventoryId) throws SystemException {
		return findByorderItemId(orderId, productInventoryId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order items where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @return the range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByorderItemId(long orderId,
		String productInventoryId, int start, int end)
		throws SystemException {
		return findByorderItemId(orderId, productInventoryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the order items where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findByorderItemId(long orderId,
		String productInventoryId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERITEMID;
			finderArgs = new Object[] { orderId, productInventoryId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ORDERITEMID;
			finderArgs = new Object[] {
					orderId, productInventoryId,
					
					start, end, orderByComparator
				};
		}

		List<OrderItem> list = (List<OrderItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (OrderItem orderItem : list) {
				if ((orderId != orderItem.getOrderId()) ||
						!Validator.equals(productInventoryId,
							orderItem.getProductInventoryId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERITEMID_ORDERID_2);

			boolean bindProductInventoryId = false;

			if (productInventoryId == null) {
				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_1);
			}
			else if (productInventoryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_3);
			}
			else {
				bindProductInventoryId = true;

				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(OrderItemModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (bindProductInventoryId) {
					qPos.add(productInventoryId);
				}

				if (!pagination) {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderItem>(list);
				}
				else {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByorderItemId_First(long orderId,
		String productInventoryId, OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByorderItemId_First(orderId,
				productInventoryId, orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(", productInventoryId=");
		msg.append(productInventoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the first order item in the ordered set where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByorderItemId_First(long orderId,
		String productInventoryId, OrderByComparator orderByComparator)
		throws SystemException {
		List<OrderItem> list = findByorderItemId(orderId, productInventoryId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByorderItemId_Last(long orderId,
		String productInventoryId, OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByorderItemId_Last(orderId,
				productInventoryId, orderByComparator);

		if (orderItem != null) {
			return orderItem;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("orderId=");
		msg.append(orderId);

		msg.append(", productInventoryId=");
		msg.append(productInventoryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchOrderItemException(msg.toString());
	}

	/**
	 * Returns the last order item in the ordered set where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching order item, or <code>null</code> if a matching order item could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByorderItemId_Last(long orderId,
		String productInventoryId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByorderItemId(orderId, productInventoryId);

		if (count == 0) {
			return null;
		}

		List<OrderItem> list = findByorderItemId(orderId, productInventoryId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the order items before and after the current order item in the ordered set where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderItemId the primary key of the current order item
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem[] findByorderItemId_PrevAndNext(long orderItemId,
		long orderId, String productInventoryId,
		OrderByComparator orderByComparator)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = findByPrimaryKey(orderItemId);

		Session session = null;

		try {
			session = openSession();

			OrderItem[] array = new OrderItemImpl[3];

			array[0] = getByorderItemId_PrevAndNext(session, orderItem,
					orderId, productInventoryId, orderByComparator, true);

			array[1] = orderItem;

			array[2] = getByorderItemId_PrevAndNext(session, orderItem,
					orderId, productInventoryId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected OrderItem getByorderItemId_PrevAndNext(Session session,
		OrderItem orderItem, long orderId, String productInventoryId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ORDERITEM_WHERE);

		query.append(_FINDER_COLUMN_ORDERITEMID_ORDERID_2);

		boolean bindProductInventoryId = false;

		if (productInventoryId == null) {
			query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_1);
		}
		else if (productInventoryId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_3);
		}
		else {
			bindProductInventoryId = true;

			query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(OrderItemModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(orderId);

		if (bindProductInventoryId) {
			qPos.add(productInventoryId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(orderItem);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<OrderItem> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the order items where orderId = &#63; and productInventoryId = &#63; from the database.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByorderItemId(long orderId, String productInventoryId)
		throws SystemException {
		for (OrderItem orderItem : findByorderItemId(orderId,
				productInventoryId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(orderItem);
		}
	}

	/**
	 * Returns the number of order items where orderId = &#63; and productInventoryId = &#63;.
	 *
	 * @param orderId the order ID
	 * @param productInventoryId the product inventory ID
	 * @return the number of matching order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByorderItemId(long orderId, String productInventoryId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ORDERITEMID;

		Object[] finderArgs = new Object[] { orderId, productInventoryId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ORDERITEM_WHERE);

			query.append(_FINDER_COLUMN_ORDERITEMID_ORDERID_2);

			boolean bindProductInventoryId = false;

			if (productInventoryId == null) {
				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_1);
			}
			else if (productInventoryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_3);
			}
			else {
				bindProductInventoryId = true;

				query.append(_FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(orderId);

				if (bindProductInventoryId) {
					qPos.add(productInventoryId);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ORDERITEMID_ORDERID_2 = "orderItem.orderId = ? AND ";
	private static final String _FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_1 = "orderItem.productInventoryId IS NULL";
	private static final String _FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_2 = "orderItem.productInventoryId = ?";
	private static final String _FINDER_COLUMN_ORDERITEMID_PRODUCTINVENTORYID_3 = "(orderItem.productInventoryId IS NULL OR orderItem.productInventoryId = '')";

	public OrderItemPersistenceImpl() {
		setModelClass(OrderItem.class);
	}

	/**
	 * Caches the order item in the entity cache if it is enabled.
	 *
	 * @param orderItem the order item
	 */
	@Override
	public void cacheResult(OrderItem orderItem) {
		EntityCacheUtil.putResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemImpl.class, orderItem.getPrimaryKey(), orderItem);

		orderItem.resetOriginalValues();
	}

	/**
	 * Caches the order items in the entity cache if it is enabled.
	 *
	 * @param orderItems the order items
	 */
	@Override
	public void cacheResult(List<OrderItem> orderItems) {
		for (OrderItem orderItem : orderItems) {
			if (EntityCacheUtil.getResult(
						OrderItemModelImpl.ENTITY_CACHE_ENABLED,
						OrderItemImpl.class, orderItem.getPrimaryKey()) == null) {
				cacheResult(orderItem);
			}
			else {
				orderItem.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all order items.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(OrderItemImpl.class.getName());
		}

		EntityCacheUtil.clearCache(OrderItemImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the order item.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(OrderItem orderItem) {
		EntityCacheUtil.removeResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemImpl.class, orderItem.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<OrderItem> orderItems) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (OrderItem orderItem : orderItems) {
			EntityCacheUtil.removeResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
				OrderItemImpl.class, orderItem.getPrimaryKey());
		}
	}

	/**
	 * Creates a new order item with the primary key. Does not add the order item to the database.
	 *
	 * @param orderItemId the primary key for the new order item
	 * @return the new order item
	 */
	@Override
	public OrderItem create(long orderItemId) {
		OrderItem orderItem = new OrderItemImpl();

		orderItem.setNew(true);
		orderItem.setPrimaryKey(orderItemId);

		return orderItem;
	}

	/**
	 * Removes the order item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param orderItemId the primary key of the order item
	 * @return the order item that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem remove(long orderItemId)
		throws NoSuchOrderItemException, SystemException {
		return remove((Serializable)orderItemId);
	}

	/**
	 * Removes the order item with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the order item
	 * @return the order item that was removed
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem remove(Serializable primaryKey)
		throws NoSuchOrderItemException, SystemException {
		Session session = null;

		try {
			session = openSession();

			OrderItem orderItem = (OrderItem)session.get(OrderItemImpl.class,
					primaryKey);

			if (orderItem == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchOrderItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(orderItem);
		}
		catch (NoSuchOrderItemException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected OrderItem removeImpl(OrderItem orderItem)
		throws SystemException {
		orderItem = toUnwrappedModel(orderItem);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(orderItem)) {
				orderItem = (OrderItem)session.get(OrderItemImpl.class,
						orderItem.getPrimaryKeyObj());
			}

			if (orderItem != null) {
				session.delete(orderItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (orderItem != null) {
			clearCache(orderItem);
		}

		return orderItem;
	}

	@Override
	public OrderItem updateImpl(com.esquare.ecommerce.model.OrderItem orderItem)
		throws SystemException {
		orderItem = toUnwrappedModel(orderItem);

		boolean isNew = orderItem.isNew();

		OrderItemModelImpl orderItemModelImpl = (OrderItemModelImpl)orderItem;

		Session session = null;

		try {
			session = openSession();

			if (orderItem.isNew()) {
				session.save(orderItem);

				orderItem.setNew(false);
			}
			else {
				session.merge(orderItem);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !OrderItemModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((orderItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderItemModelImpl.getOriginalOrderId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID,
					args);

				args = new Object[] { orderItemModelImpl.getOrderId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERID,
					args);
			}

			if ((orderItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_O_I_S.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderItemModelImpl.getOriginalOrderId(),
						orderItemModelImpl.getOriginalOrderItemStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_O_I_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_O_I_S,
					args);

				args = new Object[] {
						orderItemModelImpl.getOrderId(),
						orderItemModelImpl.getOrderItemStatus()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_O_I_S, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_O_I_S,
					args);
			}

			if ((orderItemModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERITEMID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						orderItemModelImpl.getOriginalOrderId(),
						orderItemModelImpl.getOriginalProductInventoryId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERITEMID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERITEMID,
					args);

				args = new Object[] {
						orderItemModelImpl.getOrderId(),
						orderItemModelImpl.getProductInventoryId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORDERITEMID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ORDERITEMID,
					args);
			}
		}

		EntityCacheUtil.putResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
			OrderItemImpl.class, orderItem.getPrimaryKey(), orderItem);

		return orderItem;
	}

	protected OrderItem toUnwrappedModel(OrderItem orderItem) {
		if (orderItem instanceof OrderItemImpl) {
			return orderItem;
		}

		OrderItemImpl orderItemImpl = new OrderItemImpl();

		orderItemImpl.setNew(orderItem.isNew());
		orderItemImpl.setPrimaryKey(orderItem.getPrimaryKey());

		orderItemImpl.setOrderItemId(orderItem.getOrderItemId());
		orderItemImpl.setOrderId(orderItem.getOrderId());
		orderItemImpl.setCompanyId(orderItem.getCompanyId());
		orderItemImpl.setProductInventoryId(orderItem.getProductInventoryId());
		orderItemImpl.setSku(orderItem.getSku());
		orderItemImpl.setName(orderItem.getName());
		orderItemImpl.setDescription(orderItem.getDescription());
		orderItemImpl.setProperties(orderItem.getProperties());
		orderItemImpl.setPrice(orderItem.getPrice());
		orderItemImpl.setTax(orderItem.getTax());
		orderItemImpl.setShippingPrice(orderItem.getShippingPrice());
		orderItemImpl.setQuantity(orderItem.getQuantity());
		orderItemImpl.setShippingTrackerId(orderItem.getShippingTrackerId());
		orderItemImpl.setOrderItemStatus(orderItem.getOrderItemStatus());
		orderItemImpl.setCreatedDate(orderItem.getCreatedDate());
		orderItemImpl.setDeliveredDate(orderItem.getDeliveredDate());
		orderItemImpl.setModifiedDate(orderItem.getModifiedDate());
		orderItemImpl.setExpectedDeliveryDate(orderItem.getExpectedDeliveryDate());
		orderItemImpl.setCancelByUser(orderItem.isCancelByUser());
		orderItemImpl.setCanceledByUserId(orderItem.getCanceledByUserId());

		return orderItemImpl;
	}

	/**
	 * Returns the order item with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the order item
	 * @return the order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByPrimaryKey(Serializable primaryKey)
		throws NoSuchOrderItemException, SystemException {
		OrderItem orderItem = fetchByPrimaryKey(primaryKey);

		if (orderItem == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchOrderItemException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return orderItem;
	}

	/**
	 * Returns the order item with the primary key or throws a {@link com.esquare.ecommerce.NoSuchOrderItemException} if it could not be found.
	 *
	 * @param orderItemId the primary key of the order item
	 * @return the order item
	 * @throws com.esquare.ecommerce.NoSuchOrderItemException if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem findByPrimaryKey(long orderItemId)
		throws NoSuchOrderItemException, SystemException {
		return findByPrimaryKey((Serializable)orderItemId);
	}

	/**
	 * Returns the order item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the order item
	 * @return the order item, or <code>null</code> if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		OrderItem orderItem = (OrderItem)EntityCacheUtil.getResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
				OrderItemImpl.class, primaryKey);

		if (orderItem == _nullOrderItem) {
			return null;
		}

		if (orderItem == null) {
			Session session = null;

			try {
				session = openSession();

				orderItem = (OrderItem)session.get(OrderItemImpl.class,
						primaryKey);

				if (orderItem != null) {
					cacheResult(orderItem);
				}
				else {
					EntityCacheUtil.putResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
						OrderItemImpl.class, primaryKey, _nullOrderItem);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(OrderItemModelImpl.ENTITY_CACHE_ENABLED,
					OrderItemImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return orderItem;
	}

	/**
	 * Returns the order item with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param orderItemId the primary key of the order item
	 * @return the order item, or <code>null</code> if a order item with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public OrderItem fetchByPrimaryKey(long orderItemId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)orderItemId);
	}

	/**
	 * Returns all the order items.
	 *
	 * @return the order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the order items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @return the range of order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the order items.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.OrderItemModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of order items
	 * @param end the upper bound of the range of order items (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<OrderItem> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<OrderItem> list = (List<OrderItem>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ORDERITEM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ORDERITEM;

				if (pagination) {
					sql = sql.concat(OrderItemModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<OrderItem>(list);
				}
				else {
					list = (List<OrderItem>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the order items from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (OrderItem orderItem : findAll()) {
			remove(orderItem);
		}
	}

	/**
	 * Returns the number of order items.
	 *
	 * @return the number of order items
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ORDERITEM);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the order item persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.OrderItem")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<OrderItem>> listenersList = new ArrayList<ModelListener<OrderItem>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<OrderItem>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(OrderItemImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ORDERITEM = "SELECT orderItem FROM OrderItem orderItem";
	private static final String _SQL_SELECT_ORDERITEM_WHERE = "SELECT orderItem FROM OrderItem orderItem WHERE ";
	private static final String _SQL_COUNT_ORDERITEM = "SELECT COUNT(orderItem) FROM OrderItem orderItem";
	private static final String _SQL_COUNT_ORDERITEM_WHERE = "SELECT COUNT(orderItem) FROM OrderItem orderItem WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "orderItem.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No OrderItem exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No OrderItem exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(OrderItemPersistenceImpl.class);
	private static OrderItem _nullOrderItem = new OrderItemImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<OrderItem> toCacheModel() {
				return _nullOrderItemCacheModel;
			}
		};

	private static CacheModel<OrderItem> _nullOrderItemCacheModel = new CacheModel<OrderItem>() {
			@Override
			public OrderItem toEntityModel() {
				return _nullOrderItem;
			}
		};
}