/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchShippingRatesException;
import com.esquare.ecommerce.model.ShippingRates;
import com.esquare.ecommerce.model.impl.ShippingRatesImpl;
import com.esquare.ecommerce.model.impl.ShippingRatesModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the shipping rates service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see ShippingRatesPersistence
 * @see ShippingRatesUtil
 * @generated
 */
public class ShippingRatesPersistenceImpl extends BasePersistenceImpl<ShippingRates>
	implements ShippingRatesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ShippingRatesUtil} to access the shipping rates persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ShippingRatesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyId", new String[] { Long.class.getName() },
			ShippingRatesModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the shipping rateses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the shipping rateses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @return the range of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCompanyId(long companyId, int start,
		int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the shipping rateses where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCompanyId(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ShippingRates> list = (List<ShippingRates>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ShippingRates shippingRates : list) {
				if ((companyId != shippingRates.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ShippingRatesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ShippingRates>(list);
				}
				else {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first shipping rates in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (shippingRates != null) {
			return shippingRates;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchShippingRatesException(msg.toString());
	}

	/**
	 * Returns the first shipping rates in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ShippingRates> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last shipping rates in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (shippingRates != null) {
			return shippingRates;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchShippingRatesException(msg.toString());
	}

	/**
	 * Returns the last shipping rates in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ShippingRates> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the shipping rateses before and after the current shipping rates in the ordered set where companyId = &#63;.
	 *
	 * @param recId the primary key of the current shipping rates
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates[] findByCompanyId_PrevAndNext(long recId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = findByPrimaryKey(recId);

		Session session = null;

		try {
			session = openSession();

			ShippingRates[] array = new ShippingRatesImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, shippingRates,
					companyId, orderByComparator, true);

			array[1] = shippingRates;

			array[2] = getByCompanyId_PrevAndNext(session, shippingRates,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ShippingRates getByCompanyId_PrevAndNext(Session session,
		ShippingRates shippingRates, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SHIPPINGRATES_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ShippingRatesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(shippingRates);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ShippingRates> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the shipping rateses where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (ShippingRates shippingRates : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(shippingRates);
		}
	}

	/**
	 * Returns the number of shipping rateses where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "shippingRates.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COUNTRY = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCountry",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUNTRY =
		new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCountry",
			new String[] { Long.class.getName(), String.class.getName() },
			ShippingRatesModelImpl.COMPANYID_COLUMN_BITMASK |
			ShippingRatesModelImpl.COUNTRYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COUNTRY = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCountry",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the shipping rateses where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @return the matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCountry(long companyId, String countryId)
		throws SystemException {
		return findByCountry(companyId, countryId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the shipping rateses where companyId = &#63; and countryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @return the range of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCountry(long companyId, String countryId,
		int start, int end) throws SystemException {
		return findByCountry(companyId, countryId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the shipping rateses where companyId = &#63; and countryId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findByCountry(long companyId, String countryId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUNTRY;
			finderArgs = new Object[] { companyId, countryId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COUNTRY;
			finderArgs = new Object[] {
					companyId, countryId,
					
					start, end, orderByComparator
				};
		}

		List<ShippingRates> list = (List<ShippingRates>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ShippingRates shippingRates : list) {
				if ((companyId != shippingRates.getCompanyId()) ||
						!Validator.equals(countryId,
							shippingRates.getCountryId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_COUNTRY_COMPANYID_2);

			boolean bindCountryId = false;

			if (countryId == null) {
				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_1);
			}
			else if (countryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_3);
			}
			else {
				bindCountryId = true;

				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ShippingRatesModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCountryId) {
					qPos.add(countryId);
				}

				if (!pagination) {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ShippingRates>(list);
				}
				else {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first shipping rates in the ordered set where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByCountry_First(long companyId, String countryId,
		OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByCountry_First(companyId,
				countryId, orderByComparator);

		if (shippingRates != null) {
			return shippingRates;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", countryId=");
		msg.append(countryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchShippingRatesException(msg.toString());
	}

	/**
	 * Returns the first shipping rates in the ordered set where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByCountry_First(long companyId, String countryId,
		OrderByComparator orderByComparator) throws SystemException {
		List<ShippingRates> list = findByCountry(companyId, countryId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last shipping rates in the ordered set where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByCountry_Last(long companyId, String countryId,
		OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByCountry_Last(companyId, countryId,
				orderByComparator);

		if (shippingRates != null) {
			return shippingRates;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", countryId=");
		msg.append(countryId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchShippingRatesException(msg.toString());
	}

	/**
	 * Returns the last shipping rates in the ordered set where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByCountry_Last(long companyId, String countryId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCountry(companyId, countryId);

		if (count == 0) {
			return null;
		}

		List<ShippingRates> list = findByCountry(companyId, countryId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the shipping rateses before and after the current shipping rates in the ordered set where companyId = &#63; and countryId = &#63;.
	 *
	 * @param recId the primary key of the current shipping rates
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates[] findByCountry_PrevAndNext(long recId,
		long companyId, String countryId, OrderByComparator orderByComparator)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = findByPrimaryKey(recId);

		Session session = null;

		try {
			session = openSession();

			ShippingRates[] array = new ShippingRatesImpl[3];

			array[0] = getByCountry_PrevAndNext(session, shippingRates,
					companyId, countryId, orderByComparator, true);

			array[1] = shippingRates;

			array[2] = getByCountry_PrevAndNext(session, shippingRates,
					companyId, countryId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ShippingRates getByCountry_PrevAndNext(Session session,
		ShippingRates shippingRates, long companyId, String countryId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SHIPPINGRATES_WHERE);

		query.append(_FINDER_COLUMN_COUNTRY_COMPANYID_2);

		boolean bindCountryId = false;

		if (countryId == null) {
			query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_1);
		}
		else if (countryId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_3);
		}
		else {
			bindCountryId = true;

			query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ShippingRatesModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindCountryId) {
			qPos.add(countryId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(shippingRates);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ShippingRates> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the shipping rateses where companyId = &#63; and countryId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCountry(long companyId, String countryId)
		throws SystemException {
		for (ShippingRates shippingRates : findByCountry(companyId, countryId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(shippingRates);
		}
	}

	/**
	 * Returns the number of shipping rateses where companyId = &#63; and countryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @return the number of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCountry(long companyId, String countryId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COUNTRY;

		Object[] finderArgs = new Object[] { companyId, countryId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_COUNTRY_COMPANYID_2);

			boolean bindCountryId = false;

			if (countryId == null) {
				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_1);
			}
			else if (countryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_3);
			}
			else {
				bindCountryId = true;

				query.append(_FINDER_COLUMN_COUNTRY_COUNTRYID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCountryId) {
					qPos.add(countryId);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COUNTRY_COMPANYID_2 = "shippingRates.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COUNTRY_COUNTRYID_1 = "shippingRates.countryId IS NULL";
	private static final String _FINDER_COLUMN_COUNTRY_COUNTRYID_2 = "shippingRates.countryId = ?";
	private static final String _FINDER_COLUMN_COUNTRY_COUNTRYID_3 = "(shippingRates.countryId IS NULL OR shippingRates.countryId = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_RANGES = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED,
			ShippingRatesImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByRanges",
			new String[] {
				Long.class.getName(), String.class.getName(),
				String.class.getName(), Double.class.getName(),
				Double.class.getName()
			},
			ShippingRatesModelImpl.COMPANYID_COLUMN_BITMASK |
			ShippingRatesModelImpl.COUNTRYID_COLUMN_BITMASK |
			ShippingRatesModelImpl.SHIPPINGTYPE_COLUMN_BITMASK |
			ShippingRatesModelImpl.RANGEFROM_COLUMN_BITMASK |
			ShippingRatesModelImpl.RANGETO_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_RANGES = new FinderPath(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRanges",
			new String[] {
				Long.class.getName(), String.class.getName(),
				String.class.getName(), Double.class.getName(),
				Double.class.getName()
			});

	/**
	 * Returns the shipping rates where companyId = &#63; and countryId = &#63; and shippingType = &#63; and rangeFrom = &#63; and rangeTo = &#63; or throws a {@link com.esquare.ecommerce.NoSuchShippingRatesException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param shippingType the shipping type
	 * @param rangeFrom the range from
	 * @param rangeTo the range to
	 * @return the matching shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByRanges(long companyId, String countryId,
		String shippingType, double rangeFrom, double rangeTo)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByRanges(companyId, countryId,
				shippingType, rangeFrom, rangeTo);

		if (shippingRates == null) {
			StringBundler msg = new StringBundler(12);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", countryId=");
			msg.append(countryId);

			msg.append(", shippingType=");
			msg.append(shippingType);

			msg.append(", rangeFrom=");
			msg.append(rangeFrom);

			msg.append(", rangeTo=");
			msg.append(rangeTo);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchShippingRatesException(msg.toString());
		}

		return shippingRates;
	}

	/**
	 * Returns the shipping rates where companyId = &#63; and countryId = &#63; and shippingType = &#63; and rangeFrom = &#63; and rangeTo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param shippingType the shipping type
	 * @param rangeFrom the range from
	 * @param rangeTo the range to
	 * @return the matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByRanges(long companyId, String countryId,
		String shippingType, double rangeFrom, double rangeTo)
		throws SystemException {
		return fetchByRanges(companyId, countryId, shippingType, rangeFrom,
			rangeTo, true);
	}

	/**
	 * Returns the shipping rates where companyId = &#63; and countryId = &#63; and shippingType = &#63; and rangeFrom = &#63; and rangeTo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param shippingType the shipping type
	 * @param rangeFrom the range from
	 * @param rangeTo the range to
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching shipping rates, or <code>null</code> if a matching shipping rates could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByRanges(long companyId, String countryId,
		String shippingType, double rangeFrom, double rangeTo,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] {
				companyId, countryId, shippingType, rangeFrom, rangeTo
			};

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_RANGES,
					finderArgs, this);
		}

		if (result instanceof ShippingRates) {
			ShippingRates shippingRates = (ShippingRates)result;

			if ((companyId != shippingRates.getCompanyId()) ||
					!Validator.equals(countryId, shippingRates.getCountryId()) ||
					!Validator.equals(shippingType,
						shippingRates.getShippingType()) ||
					(rangeFrom != shippingRates.getRangeFrom()) ||
					(rangeTo != shippingRates.getRangeTo())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(7);

			query.append(_SQL_SELECT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_RANGES_COMPANYID_2);

			boolean bindCountryId = false;

			if (countryId == null) {
				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_1);
			}
			else if (countryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_3);
			}
			else {
				bindCountryId = true;

				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_2);
			}

			boolean bindShippingType = false;

			if (shippingType == null) {
				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_1);
			}
			else if (shippingType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_3);
			}
			else {
				bindShippingType = true;

				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_2);
			}

			query.append(_FINDER_COLUMN_RANGES_RANGEFROM_2);

			query.append(_FINDER_COLUMN_RANGES_RANGETO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCountryId) {
					qPos.add(countryId);
				}

				if (bindShippingType) {
					qPos.add(shippingType);
				}

				qPos.add(rangeFrom);

				qPos.add(rangeTo);

				List<ShippingRates> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RANGES,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ShippingRatesPersistenceImpl.fetchByRanges(long, String, String, double, double, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ShippingRates shippingRates = list.get(0);

					result = shippingRates;

					cacheResult(shippingRates);

					if ((shippingRates.getCompanyId() != companyId) ||
							(shippingRates.getCountryId() == null) ||
							!shippingRates.getCountryId().equals(countryId) ||
							(shippingRates.getShippingType() == null) ||
							!shippingRates.getShippingType().equals(shippingType) ||
							(shippingRates.getRangeFrom() != rangeFrom) ||
							(shippingRates.getRangeTo() != rangeTo)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RANGES,
							finderArgs, shippingRates);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RANGES,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ShippingRates)result;
		}
	}

	/**
	 * Removes the shipping rates where companyId = &#63; and countryId = &#63; and shippingType = &#63; and rangeFrom = &#63; and rangeTo = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param shippingType the shipping type
	 * @param rangeFrom the range from
	 * @param rangeTo the range to
	 * @return the shipping rates that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates removeByRanges(long companyId, String countryId,
		String shippingType, double rangeFrom, double rangeTo)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = findByRanges(companyId, countryId,
				shippingType, rangeFrom, rangeTo);

		return remove(shippingRates);
	}

	/**
	 * Returns the number of shipping rateses where companyId = &#63; and countryId = &#63; and shippingType = &#63; and rangeFrom = &#63; and rangeTo = &#63;.
	 *
	 * @param companyId the company ID
	 * @param countryId the country ID
	 * @param shippingType the shipping type
	 * @param rangeFrom the range from
	 * @param rangeTo the range to
	 * @return the number of matching shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByRanges(long companyId, String countryId,
		String shippingType, double rangeFrom, double rangeTo)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_RANGES;

		Object[] finderArgs = new Object[] {
				companyId, countryId, shippingType, rangeFrom, rangeTo
			};

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(6);

			query.append(_SQL_COUNT_SHIPPINGRATES_WHERE);

			query.append(_FINDER_COLUMN_RANGES_COMPANYID_2);

			boolean bindCountryId = false;

			if (countryId == null) {
				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_1);
			}
			else if (countryId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_3);
			}
			else {
				bindCountryId = true;

				query.append(_FINDER_COLUMN_RANGES_COUNTRYID_2);
			}

			boolean bindShippingType = false;

			if (shippingType == null) {
				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_1);
			}
			else if (shippingType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_3);
			}
			else {
				bindShippingType = true;

				query.append(_FINDER_COLUMN_RANGES_SHIPPINGTYPE_2);
			}

			query.append(_FINDER_COLUMN_RANGES_RANGEFROM_2);

			query.append(_FINDER_COLUMN_RANGES_RANGETO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindCountryId) {
					qPos.add(countryId);
				}

				if (bindShippingType) {
					qPos.add(shippingType);
				}

				qPos.add(rangeFrom);

				qPos.add(rangeTo);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_RANGES_COMPANYID_2 = "shippingRates.companyId = ? AND ";
	private static final String _FINDER_COLUMN_RANGES_COUNTRYID_1 = "shippingRates.countryId IS NULL AND ";
	private static final String _FINDER_COLUMN_RANGES_COUNTRYID_2 = "shippingRates.countryId = ? AND ";
	private static final String _FINDER_COLUMN_RANGES_COUNTRYID_3 = "(shippingRates.countryId IS NULL OR shippingRates.countryId = '') AND ";
	private static final String _FINDER_COLUMN_RANGES_SHIPPINGTYPE_1 = "shippingRates.shippingType IS NULL AND ";
	private static final String _FINDER_COLUMN_RANGES_SHIPPINGTYPE_2 = "shippingRates.shippingType = ? AND ";
	private static final String _FINDER_COLUMN_RANGES_SHIPPINGTYPE_3 = "(shippingRates.shippingType IS NULL OR shippingRates.shippingType = '') AND ";
	private static final String _FINDER_COLUMN_RANGES_RANGEFROM_2 = "shippingRates.rangeFrom = ? AND ";
	private static final String _FINDER_COLUMN_RANGES_RANGETO_2 = "shippingRates.rangeTo = ?";

	public ShippingRatesPersistenceImpl() {
		setModelClass(ShippingRates.class);
	}

	/**
	 * Caches the shipping rates in the entity cache if it is enabled.
	 *
	 * @param shippingRates the shipping rates
	 */
	@Override
	public void cacheResult(ShippingRates shippingRates) {
		EntityCacheUtil.putResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesImpl.class, shippingRates.getPrimaryKey(),
			shippingRates);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RANGES,
			new Object[] {
				shippingRates.getCompanyId(), shippingRates.getCountryId(),
				shippingRates.getShippingType(), shippingRates.getRangeFrom(),
				shippingRates.getRangeTo()
			}, shippingRates);

		shippingRates.resetOriginalValues();
	}

	/**
	 * Caches the shipping rateses in the entity cache if it is enabled.
	 *
	 * @param shippingRateses the shipping rateses
	 */
	@Override
	public void cacheResult(List<ShippingRates> shippingRateses) {
		for (ShippingRates shippingRates : shippingRateses) {
			if (EntityCacheUtil.getResult(
						ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
						ShippingRatesImpl.class, shippingRates.getPrimaryKey()) == null) {
				cacheResult(shippingRates);
			}
			else {
				shippingRates.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all shipping rateses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ShippingRatesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ShippingRatesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the shipping rates.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ShippingRates shippingRates) {
		EntityCacheUtil.removeResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesImpl.class, shippingRates.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(shippingRates);
	}

	@Override
	public void clearCache(List<ShippingRates> shippingRateses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ShippingRates shippingRates : shippingRateses) {
			EntityCacheUtil.removeResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
				ShippingRatesImpl.class, shippingRates.getPrimaryKey());

			clearUniqueFindersCache(shippingRates);
		}
	}

	protected void cacheUniqueFindersCache(ShippingRates shippingRates) {
		if (shippingRates.isNew()) {
			Object[] args = new Object[] {
					shippingRates.getCompanyId(), shippingRates.getCountryId(),
					shippingRates.getShippingType(),
					shippingRates.getRangeFrom(), shippingRates.getRangeTo()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_RANGES, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RANGES, args,
				shippingRates);
		}
		else {
			ShippingRatesModelImpl shippingRatesModelImpl = (ShippingRatesModelImpl)shippingRates;

			if ((shippingRatesModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_RANGES.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						shippingRates.getCompanyId(),
						shippingRates.getCountryId(),
						shippingRates.getShippingType(),
						shippingRates.getRangeFrom(), shippingRates.getRangeTo()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_RANGES, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_RANGES, args,
					shippingRates);
			}
		}
	}

	protected void clearUniqueFindersCache(ShippingRates shippingRates) {
		ShippingRatesModelImpl shippingRatesModelImpl = (ShippingRatesModelImpl)shippingRates;

		Object[] args = new Object[] {
				shippingRates.getCompanyId(), shippingRates.getCountryId(),
				shippingRates.getShippingType(), shippingRates.getRangeFrom(),
				shippingRates.getRangeTo()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RANGES, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RANGES, args);

		if ((shippingRatesModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_RANGES.getColumnBitmask()) != 0) {
			args = new Object[] {
					shippingRatesModelImpl.getOriginalCompanyId(),
					shippingRatesModelImpl.getOriginalCountryId(),
					shippingRatesModelImpl.getOriginalShippingType(),
					shippingRatesModelImpl.getOriginalRangeFrom(),
					shippingRatesModelImpl.getOriginalRangeTo()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RANGES, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_RANGES, args);
		}
	}

	/**
	 * Creates a new shipping rates with the primary key. Does not add the shipping rates to the database.
	 *
	 * @param recId the primary key for the new shipping rates
	 * @return the new shipping rates
	 */
	@Override
	public ShippingRates create(long recId) {
		ShippingRates shippingRates = new ShippingRatesImpl();

		shippingRates.setNew(true);
		shippingRates.setPrimaryKey(recId);

		return shippingRates;
	}

	/**
	 * Removes the shipping rates with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param recId the primary key of the shipping rates
	 * @return the shipping rates that was removed
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates remove(long recId)
		throws NoSuchShippingRatesException, SystemException {
		return remove((Serializable)recId);
	}

	/**
	 * Removes the shipping rates with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the shipping rates
	 * @return the shipping rates that was removed
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates remove(Serializable primaryKey)
		throws NoSuchShippingRatesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ShippingRates shippingRates = (ShippingRates)session.get(ShippingRatesImpl.class,
					primaryKey);

			if (shippingRates == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchShippingRatesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(shippingRates);
		}
		catch (NoSuchShippingRatesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ShippingRates removeImpl(ShippingRates shippingRates)
		throws SystemException {
		shippingRates = toUnwrappedModel(shippingRates);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(shippingRates)) {
				shippingRates = (ShippingRates)session.get(ShippingRatesImpl.class,
						shippingRates.getPrimaryKeyObj());
			}

			if (shippingRates != null) {
				session.delete(shippingRates);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (shippingRates != null) {
			clearCache(shippingRates);
		}

		return shippingRates;
	}

	@Override
	public ShippingRates updateImpl(
		com.esquare.ecommerce.model.ShippingRates shippingRates)
		throws SystemException {
		shippingRates = toUnwrappedModel(shippingRates);

		boolean isNew = shippingRates.isNew();

		ShippingRatesModelImpl shippingRatesModelImpl = (ShippingRatesModelImpl)shippingRates;

		Session session = null;

		try {
			session = openSession();

			if (shippingRates.isNew()) {
				session.save(shippingRates);

				shippingRates.setNew(false);
			}
			else {
				session.merge(shippingRates);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ShippingRatesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((shippingRatesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						shippingRatesModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { shippingRatesModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((shippingRatesModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUNTRY.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						shippingRatesModelImpl.getOriginalCompanyId(),
						shippingRatesModelImpl.getOriginalCountryId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUNTRY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUNTRY,
					args);

				args = new Object[] {
						shippingRatesModelImpl.getCompanyId(),
						shippingRatesModelImpl.getCountryId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COUNTRY, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COUNTRY,
					args);
			}
		}

		EntityCacheUtil.putResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
			ShippingRatesImpl.class, shippingRates.getPrimaryKey(),
			shippingRates);

		clearUniqueFindersCache(shippingRates);
		cacheUniqueFindersCache(shippingRates);

		return shippingRates;
	}

	protected ShippingRates toUnwrappedModel(ShippingRates shippingRates) {
		if (shippingRates instanceof ShippingRatesImpl) {
			return shippingRates;
		}

		ShippingRatesImpl shippingRatesImpl = new ShippingRatesImpl();

		shippingRatesImpl.setNew(shippingRates.isNew());
		shippingRatesImpl.setPrimaryKey(shippingRates.getPrimaryKey());

		shippingRatesImpl.setRecId(shippingRates.getRecId());
		shippingRatesImpl.setCompanyId(shippingRates.getCompanyId());
		shippingRatesImpl.setCountryId(shippingRates.getCountryId());
		shippingRatesImpl.setName(shippingRates.getName());
		shippingRatesImpl.setShippingType(shippingRates.getShippingType());
		shippingRatesImpl.setRangeFrom(shippingRates.getRangeFrom());
		shippingRatesImpl.setRangeTo(shippingRates.getRangeTo());
		shippingRatesImpl.setPrice(shippingRates.getPrice());
		shippingRatesImpl.setXml(shippingRates.getXml());

		return shippingRatesImpl;
	}

	/**
	 * Returns the shipping rates with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the shipping rates
	 * @return the shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByPrimaryKey(Serializable primaryKey)
		throws NoSuchShippingRatesException, SystemException {
		ShippingRates shippingRates = fetchByPrimaryKey(primaryKey);

		if (shippingRates == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchShippingRatesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return shippingRates;
	}

	/**
	 * Returns the shipping rates with the primary key or throws a {@link com.esquare.ecommerce.NoSuchShippingRatesException} if it could not be found.
	 *
	 * @param recId the primary key of the shipping rates
	 * @return the shipping rates
	 * @throws com.esquare.ecommerce.NoSuchShippingRatesException if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates findByPrimaryKey(long recId)
		throws NoSuchShippingRatesException, SystemException {
		return findByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns the shipping rates with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the shipping rates
	 * @return the shipping rates, or <code>null</code> if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ShippingRates shippingRates = (ShippingRates)EntityCacheUtil.getResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
				ShippingRatesImpl.class, primaryKey);

		if (shippingRates == _nullShippingRates) {
			return null;
		}

		if (shippingRates == null) {
			Session session = null;

			try {
				session = openSession();

				shippingRates = (ShippingRates)session.get(ShippingRatesImpl.class,
						primaryKey);

				if (shippingRates != null) {
					cacheResult(shippingRates);
				}
				else {
					EntityCacheUtil.putResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
						ShippingRatesImpl.class, primaryKey, _nullShippingRates);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ShippingRatesModelImpl.ENTITY_CACHE_ENABLED,
					ShippingRatesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return shippingRates;
	}

	/**
	 * Returns the shipping rates with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param recId the primary key of the shipping rates
	 * @return the shipping rates, or <code>null</code> if a shipping rates with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ShippingRates fetchByPrimaryKey(long recId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns all the shipping rateses.
	 *
	 * @return the shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the shipping rateses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @return the range of shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the shipping rateses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ShippingRatesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of shipping rateses
	 * @param end the upper bound of the range of shipping rateses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ShippingRates> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ShippingRates> list = (List<ShippingRates>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_SHIPPINGRATES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SHIPPINGRATES;

				if (pagination) {
					sql = sql.concat(ShippingRatesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ShippingRates>(list);
				}
				else {
					list = (List<ShippingRates>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the shipping rateses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ShippingRates shippingRates : findAll()) {
			remove(shippingRates);
		}
	}

	/**
	 * Returns the number of shipping rateses.
	 *
	 * @return the number of shipping rateses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SHIPPINGRATES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the shipping rates persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.ShippingRates")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ShippingRates>> listenersList = new ArrayList<ModelListener<ShippingRates>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ShippingRates>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ShippingRatesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_SHIPPINGRATES = "SELECT shippingRates FROM ShippingRates shippingRates";
	private static final String _SQL_SELECT_SHIPPINGRATES_WHERE = "SELECT shippingRates FROM ShippingRates shippingRates WHERE ";
	private static final String _SQL_COUNT_SHIPPINGRATES = "SELECT COUNT(shippingRates) FROM ShippingRates shippingRates";
	private static final String _SQL_COUNT_SHIPPINGRATES_WHERE = "SELECT COUNT(shippingRates) FROM ShippingRates shippingRates WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "shippingRates.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ShippingRates exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ShippingRates exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ShippingRatesPersistenceImpl.class);
	private static ShippingRates _nullShippingRates = new ShippingRatesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ShippingRates> toCacheModel() {
				return _nullShippingRatesCacheModel;
			}
		};

	private static CacheModel<ShippingRates> _nullShippingRatesCacheModel = new CacheModel<ShippingRates>() {
			@Override
			public ShippingRates toEntityModel() {
				return _nullShippingRates;
			}
		};
}