/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.EcartEbsPayment;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EcartEbsPayment in entity cache.
 *
 * @author Esquare
 * @see EcartEbsPayment
 * @generated
 */
public class EcartEbsPaymentCacheModel implements CacheModel<EcartEbsPayment>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{ebsTrackId=");
		sb.append(ebsTrackId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", ecartAutoId=");
		sb.append(ecartAutoId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", responseCode=");
		sb.append(responseCode);
		sb.append(", responseMessage=");
		sb.append(responseMessage);
		sb.append(", paidDate=");
		sb.append(paidDate);
		sb.append(", paymentId=");
		sb.append(paymentId);
		sb.append(", paymentMethod=");
		sb.append(paymentMethod);
		sb.append(", amount=");
		sb.append(amount);
		sb.append(", isFlagged=");
		sb.append(isFlagged);
		sb.append(", transactionId=");
		sb.append(transactionId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EcartEbsPayment toEntityModel() {
		EcartEbsPaymentImpl ecartEbsPaymentImpl = new EcartEbsPaymentImpl();

		ecartEbsPaymentImpl.setEbsTrackId(ebsTrackId);
		ecartEbsPaymentImpl.setCompanyId(companyId);

		if (ecartAutoId == null) {
			ecartEbsPaymentImpl.setEcartAutoId(StringPool.BLANK);
		}
		else {
			ecartEbsPaymentImpl.setEcartAutoId(ecartAutoId);
		}

		ecartEbsPaymentImpl.setUserId(userId);
		ecartEbsPaymentImpl.setResponseCode(responseCode);

		if (responseMessage == null) {
			ecartEbsPaymentImpl.setResponseMessage(StringPool.BLANK);
		}
		else {
			ecartEbsPaymentImpl.setResponseMessage(responseMessage);
		}

		if (paidDate == Long.MIN_VALUE) {
			ecartEbsPaymentImpl.setPaidDate(null);
		}
		else {
			ecartEbsPaymentImpl.setPaidDate(new Date(paidDate));
		}

		ecartEbsPaymentImpl.setPaymentId(paymentId);
		ecartEbsPaymentImpl.setPaymentMethod(paymentMethod);

		if (amount == null) {
			ecartEbsPaymentImpl.setAmount(StringPool.BLANK);
		}
		else {
			ecartEbsPaymentImpl.setAmount(amount);
		}

		if (isFlagged == null) {
			ecartEbsPaymentImpl.setIsFlagged(StringPool.BLANK);
		}
		else {
			ecartEbsPaymentImpl.setIsFlagged(isFlagged);
		}

		ecartEbsPaymentImpl.setTransactionId(transactionId);

		ecartEbsPaymentImpl.resetOriginalValues();

		return ecartEbsPaymentImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ebsTrackId = objectInput.readLong();
		companyId = objectInput.readLong();
		ecartAutoId = objectInput.readUTF();
		userId = objectInput.readLong();
		responseCode = objectInput.readLong();
		responseMessage = objectInput.readUTF();
		paidDate = objectInput.readLong();
		paymentId = objectInput.readLong();
		paymentMethod = objectInput.readLong();
		amount = objectInput.readUTF();
		isFlagged = objectInput.readUTF();
		transactionId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ebsTrackId);
		objectOutput.writeLong(companyId);

		if (ecartAutoId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ecartAutoId);
		}

		objectOutput.writeLong(userId);
		objectOutput.writeLong(responseCode);

		if (responseMessage == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(responseMessage);
		}

		objectOutput.writeLong(paidDate);
		objectOutput.writeLong(paymentId);
		objectOutput.writeLong(paymentMethod);

		if (amount == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(amount);
		}

		if (isFlagged == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(isFlagged);
		}

		objectOutput.writeLong(transactionId);
	}

	public long ebsTrackId;
	public long companyId;
	public String ecartAutoId;
	public long userId;
	public long responseCode;
	public String responseMessage;
	public long paidDate;
	public long paymentId;
	public long paymentMethod;
	public String amount;
	public String isFlagged;
	public long transactionId;
}