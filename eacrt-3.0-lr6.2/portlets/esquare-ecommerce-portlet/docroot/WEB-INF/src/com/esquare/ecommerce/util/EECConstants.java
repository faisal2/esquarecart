package com.esquare.ecommerce.util;

import java.text.SimpleDateFormat;

import com.liferay.portal.kernel.util.Constants;

public interface EECConstants extends Constants {

	public static final String ECART_WEBID = "esquarecart.com";

	public static final String ADMIN_EMAIL_TO_NAME = "emailToName";

	public static final String ADMIN_EMAIL_TO_ADDRESS = "emailToAddress";

	public static final String SHOP_ADMINNAME = "admin";

	public static final String GUEST = "Guest";

	public static final String LOGIN = "login";

	public static final String DOWNLOAD = "download";

	public static final int DEFAULT_USER_COUNT = 2;

	public static long DEFAULT_PARENT_CATALOG_ID = 0;

	public static final String TWITTER = "twitter";

	public static final String LINKEDIN = "linkedIn";

	public static final String FORGOT_PASSWORD_RENDER = "forgot_password_render";

	public static final String FORGOT_PASSWORD_ACTION = "forgot_password_action";

	public static final String CAPTCHA = "captcha";

	public static final boolean STATUS_ACTIVE_DEFAULT = true;

	public static final boolean STATUS_INACTIVE = false;

	public static final String TWITTER_ID_LOGIN = "TWITTER_ID_LOGIN";

	public static final String TWITTER_LOGIN_PENDING = "TWITTER_LOGIN_PENDING";

	public static final String LINKED_IN_LOGIN = "LINKED_IN_LOGIN";

	public static final String CATALOG = "CATALOG";

	public static final String CONFIRMATION_MAIL = "confirmation-mail";

	public static final String STATUS_MAIL = "status-mail";

	public static final String PRODUCT = "PRODUCT";

	public static final boolean PRODUCT_VISIBLITY_TRUE = true;

	public static final boolean PRODUCT_VISIBLITY_FALSE = false;

	public static final String AND_OPERATOR = "andOperator";

	public static final String DISCOUNT_TYPE = "discountType";

	public static final String ADMIN_SEARCH = "admin-search";

	public static final String USER_SEARCH = "user-search";

	public static final String ACTIVE = "active";

	public static final String COUPON_CODE = "couponCode";

	public static final String COUPON_NAME = "couponName";

	public static final String SEARCH = "search";

	public static final String SEARCH_LIST = "search_list";
	
	public static final String COMPARE_PORTLET_NAME = "compare_WAR_esquareecommerceportlet";
	
	public static final String COMPARE_PAGE_FRIENDLY_URL = "/compare";

	public static final String CART_PORTLET_NAME = "cart_WAR_esquareecommerceportlet";

	public static final String CART_PAGE_FRIENDLY_URL = "/cart";

	public static final String LOGIN_PORTLET_NAME = "login_WAR_esquareecommerceportlet";

	public static final String LOGIN_PAGE_FRIENDLY_URL = "/login";

	public static final String USER_VIEW_PORTLET_NAME = "userView_WAR_esquareecommerceportlet";

	public static final String PRODUCT_DETAILS_PAGE_FRIENDLY_URL = "/productdetails";

	public static final String PRDODUCTS_PAGE_FRIENDLY_URL = "/products";

	public static final String WISHLIST_PORTLET_NAME = "wishList_WAR_esquareecommerceportlet";

	public static final String WISHLIST_PAGE_FRIENDLY_URL = "/wishlist";

	public static final String CART = "cart";

	public static final String CART_UPDATE = "cart_update";

	public static final String NEWLAUNCH = "latestItem";

	public static final String HOTDEAL = "hotDeals";

	public static final String BESTSELLER = "others";

	public static final String SIMILAR_PRODUCTS = "similarProducts";

	public static final String SIMILAR_PRODUCT_OFFSET_LIMIT = "similar_product_offset_limit";

	public static final String OTHERS = "login";

	public static final String PAYMENT_TYPE = "payment_type";

	public static final String PAYPAL = "paypal";

	public static final String REDIRECTURL = "redirecturl";

	public static final String MOVE_TO_SHIPPING = "move-to-shipping";

	public static final String RESENDMAIL = "re-sendmail";

	public static final String EBS = "ebs";
	
	public static final String CCAVENUE = "ccavenue";

	public static final String COD = "cod";

	public static final String WALLET = "wallet";

	public static final String CHECK = "check";

	public static final String ONLINE = "online";

	public static final String OTHER = "other";

	public static final String DELIVERED = "delivered";

	public static final String CMD1 = "cmd1";

	public static final String CC_AVENUE = "ccavenue";

	public static final String DISCOUNT_TYPE_ACTUAL = "actual";

	public static final String DISCOUNT_TYPE_FREE_SHIPPING = "free-shipping";

	public static final String DISCOUNT_TYPE_PERCENTAGE = "percentage";

	public static final String DISCOUNT_TYPE_TAX_FREE = "tax-free";

	public static final String STATUS_CHECKOUT = "CHECKOUT";

	public static final String STATUS_COMPLETED = "completed";

	public static final String STATUS_DENIED = "denied";

	public static final String STATUS_LATEST = "STATUS_LATEST";

	public static final String STATUS_PENDING = "pending";

	public static final String STATUS_REFUNDED = "refunded";
	
	public static final String CHECKOUT = "checkout";

	public static final String SHOPPING_ORDER = "SHOPPING_ORDER";

	public static final String EBS_RESPONSE = "ebs-response";
	
	public static final String CCAVENUE_RESPONSE = "ccavenue-response";

	//
	public static final String COMPLETED = "Completed";
	
	public static final String PAYMENT_SUCCESSFUL = "successful";

	public static final String PAYMENT_PENDING = "pending";

	public static final String ORDER_STATUS_PROCESSING = "PROCESSING";

	public static final String ORDER_STATUS_ONSHIPPING = "ONSHIPPING";

	public static final String ORDER_STATUS_DELIVERED = "DELIVERED";

	public static final String ORDER_STATUS_CANCELLED = "CANCELLED";

	public static final String ORDER_STATUS_RETURN = "RETURN";

	//

	public static final String[] STATUSES = { "checkout", "completed",
			"denied", "pending", "refunded" };

	public static final String[] DISCOUNT_TYPES = { DISCOUNT_TYPE_PERCENTAGE,
			DISCOUNT_TYPE_ACTUAL, DISCOUNT_TYPE_FREE_SHIPPING,
			DISCOUNT_TYPE_TAX_FREE };

	public String[] discountLimit = { "All Orders", "Specific Catalog",
			"Specific Product" };

	public String[] otherPaymentType = { EBS, CC_AVENUE };

	public static final String AUTOCOMPLETE = "AUTOCOMPLETE";

	public static final String AUTOCOMPLETE_SEARCH = "AUTOCOMPLETE_SEARCH";

	public static final String DISCOUNT = "discount";

	public static final String MAX_USER = "MU";

	public static final String PRODUCT_COUNT = "PC";

	public static final String CATALOG_COUNT = "CC";

	public static final String SIMILAR_PRODUCT = "SP";

	public static final String RELATED_PRODUCT = "RP";

	public static final String COMPARE_PORTLET = "CP";

	public static final String DOMAIN_SETTINGS = "DS";

	public static final String OUTOFSTOCK = "Out of Stock";

	public static final String INSTOCK = "In Stock";

	public static final String MONTH = "MONTH";

	public static final String WISHLIST_UPDATE = "wishlist_update";

	public static final String STYLE_DISPLAY_BLOCK = "display:block";
	
	public static final String STYLE_DISPLAY_INLINE_BLOCK = "display:inline-block";

	public static final String STYLE_DISPLAY_NONE = "display:none";

	public static final String USAGE = "usage";

	public static final String ORDER_COUNT = "orderCount";

	public static final String GENERAL = "General";

	public static final String SHIPPING = "Shipping";

	public static final String TAXES = "Taxes";

	public static final String EMAIL = "Email";

	public static final String PACKAGE_ALERT = "Package Alert";

	public static final String SOCIAL_NETWORK = "Social Network";

	public static final String GOOGLE_ANALYTICS = "GoogleAnalytics";

	public static final String ADD_COUNTRY = "ADD_COUNTRY";

	public static final String SHIPPING_SETTING = "SHIPPING_SETTING";

	public static final String CMD_SETTING = "CMD_SETTING";

	public static final String ADD_EBS = "addEbs";

	public static final String PAY_EBS = "payEbs";

	public static final String BASIC = "Basic";

	public static final String STANDARD = "Standard";

	public static final String PREMIUM = "Premium";

	public static final String ENTERPRISE = "Enterprise";

	public static final String TAXES_SETTING = "TAXES_SETTING";

	public static final String GENERAL_ADMIN_SETTINGS = "GENERAL_ADMIN_SETTINGS";

	public static final String LESS_STOCK = "25";

	public static final String[] MAX_LICENCE_DAYS = { "15", "30", "5", "10" };

	public static final String[] FREE_LICENCE_EXP_DAYS = { "20", "15", "30", "" };

	public static final int FREE_BEGIN_ALERT = 5;

	public static final int FREE_EXP_DATE_VALUE = 15;

	public static final int FREE_STORE_CLOSED_ALERT = 180;

	public static final int FREE_ALERT_INTERVEL = 1;

	public static final int PACKAGE_BEGIN_ALERT = 7;

	public static final int PACKAGE_EXP_DATE_VALUE = 15;

	public static final int PACKAGE_STORE_CLOSED_ALERT = 365;

	public static final int PACKAGE_ALERT_INTERVEL = 3;

	public static final int DEFAULT_COMPANY_ID = 10155;

	public static final SimpleDateFormat sdf = new SimpleDateFormat(
			"yyyy-MM-dd hh:mm:ss");

	public static final String CUSTOMER_CLAIM = "Customer Claim";

	public static final String PAID_LIST = "Paid List";

	public static final String CLAIM_REQUEST = "My Claim Request";

	public static final String WALLET_TRACKER = "My Wallet Tracker";

	public static final String WALLET_USED_AMOUNT = "Wallet Used Amount";

	public static final String PRODUCT_CUSTOM_FIELD_SEPERATOR = "_&_";
		
	public static final String WISHLIST_UPDATE_ATTRIBUTE = "wishlist_update_attribute";
	
	public static final String BULK_UPLOAD = "bulk_upload";
	
	public static final String PAYPAL_URL = "https://www.sandbox.paypal.com/cgi-bin/webscr?";
	
	public static final String SMS = "SMS";
	
	public static final String COUNTRY_CODE = "91";
	
	public static final String VIANET_HOST_URL = "http://smsc.vianett.no/V3/CPA/MT/MT.ashx";
	
	public static final String SMS_HORIZON_HOST_URL = "http://smshorizon.co.in/api/sendsms.php";
	
	public static final String SMS_HORIZON_API_VALUE = "xOCRdfSo74wXalSOmDYJ";
	
	public static final String BULK_SMS_HOST_URL = "http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp";
	
	public static final String WAY2_SMS_HOST_URL = "http://site23.way2sms.com/FirstServletsms";
	
	public static final String SMS_HHORIZON = "SMS_Horizon";
	
	public static final String VIANET = "Vianet";
	
	public static final String BULKSMS = "Bulksms";
	
	public static final String PRODUCTION = "Production";
	
	public static final String PREFIXURL = "http://";
	
	public static final String INR = "INR";
	
	public static final String USD ="USD";
}