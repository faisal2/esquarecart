/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchPaymentConfigurationException;
import com.esquare.ecommerce.model.PaymentConfiguration;
import com.esquare.ecommerce.model.impl.PaymentConfigurationImpl;
import com.esquare.ecommerce.model.impl.PaymentConfigurationModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the payment configuration service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see PaymentConfigurationPersistence
 * @see PaymentConfigurationUtil
 * @generated
 */
public class PaymentConfigurationPersistenceImpl extends BasePersistenceImpl<PaymentConfiguration>
	implements PaymentConfigurationPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PaymentConfigurationUtil} to access the payment configuration persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PaymentConfigurationImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			PaymentConfigurationModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the payment configurations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the payment configurations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentConfigurationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of payment configurations
	 * @param end the upper bound of the range of payment configurations (not inclusive)
	 * @return the range of matching payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findByCompanyId(long companyId,
		int start, int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the payment configurations where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentConfigurationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of payment configurations
	 * @param end the upper bound of the range of payment configurations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findByCompanyId(long companyId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<PaymentConfiguration> list = (List<PaymentConfiguration>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (PaymentConfiguration paymentConfiguration : list) {
				if ((companyId != paymentConfiguration.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PAYMENTCONFIGURATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PaymentConfigurationModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<PaymentConfiguration>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PaymentConfiguration>(list);
				}
				else {
					list = (List<PaymentConfiguration>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first payment configuration in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (paymentConfiguration != null) {
			return paymentConfiguration;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPaymentConfigurationException(msg.toString());
	}

	/**
	 * Returns the first payment configuration in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching payment configuration, or <code>null</code> if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<PaymentConfiguration> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last payment configuration in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (paymentConfiguration != null) {
			return paymentConfiguration;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPaymentConfigurationException(msg.toString());
	}

	/**
	 * Returns the last payment configuration in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching payment configuration, or <code>null</code> if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<PaymentConfiguration> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the payment configurations before and after the current payment configuration in the ordered set where companyId = &#63;.
	 *
	 * @param id the primary key of the current payment configuration
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration[] findByCompanyId_PrevAndNext(long id,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			PaymentConfiguration[] array = new PaymentConfigurationImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session,
					paymentConfiguration, companyId, orderByComparator, true);

			array[1] = paymentConfiguration;

			array[2] = getByCompanyId_PrevAndNext(session,
					paymentConfiguration, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PaymentConfiguration getByCompanyId_PrevAndNext(Session session,
		PaymentConfiguration paymentConfiguration, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PAYMENTCONFIGURATION_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PaymentConfigurationModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(paymentConfiguration);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PaymentConfiguration> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the payment configurations where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (PaymentConfiguration paymentConfiguration : findByCompanyId(
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(paymentConfiguration);
		}
	}

	/**
	 * Returns the number of payment configurations where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PAYMENTCONFIGURATION_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "paymentConfiguration.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_C_PGT = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED,
			PaymentConfigurationImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByC_PGT",
			new String[] { Long.class.getName(), String.class.getName() },
			PaymentConfigurationModelImpl.COMPANYID_COLUMN_BITMASK |
			PaymentConfigurationModelImpl.PAYMENTGATEWAYTYPE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_PGT = new FinderPath(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_PGT",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the payment configuration where companyId = &#63; and paymentGatewayType = &#63; or throws a {@link com.esquare.ecommerce.NoSuchPaymentConfigurationException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param paymentGatewayType the payment gateway type
	 * @return the matching payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration findByC_PGT(long companyId,
		String paymentGatewayType)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = fetchByC_PGT(companyId,
				paymentGatewayType);

		if (paymentConfiguration == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", paymentGatewayType=");
			msg.append(paymentGatewayType);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPaymentConfigurationException(msg.toString());
		}

		return paymentConfiguration;
	}

	/**
	 * Returns the payment configuration where companyId = &#63; and paymentGatewayType = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param paymentGatewayType the payment gateway type
	 * @return the matching payment configuration, or <code>null</code> if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByC_PGT(long companyId,
		String paymentGatewayType) throws SystemException {
		return fetchByC_PGT(companyId, paymentGatewayType, true);
	}

	/**
	 * Returns the payment configuration where companyId = &#63; and paymentGatewayType = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param paymentGatewayType the payment gateway type
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching payment configuration, or <code>null</code> if a matching payment configuration could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByC_PGT(long companyId,
		String paymentGatewayType, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { companyId, paymentGatewayType };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_C_PGT,
					finderArgs, this);
		}

		if (result instanceof PaymentConfiguration) {
			PaymentConfiguration paymentConfiguration = (PaymentConfiguration)result;

			if ((companyId != paymentConfiguration.getCompanyId()) ||
					!Validator.equals(paymentGatewayType,
						paymentConfiguration.getPaymentGatewayType())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PAYMENTCONFIGURATION_WHERE);

			query.append(_FINDER_COLUMN_C_PGT_COMPANYID_2);

			boolean bindPaymentGatewayType = false;

			if (paymentGatewayType == null) {
				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_1);
			}
			else if (paymentGatewayType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_3);
			}
			else {
				bindPaymentGatewayType = true;

				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindPaymentGatewayType) {
					qPos.add(paymentGatewayType);
				}

				List<PaymentConfiguration> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_PGT,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PaymentConfigurationPersistenceImpl.fetchByC_PGT(long, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PaymentConfiguration paymentConfiguration = list.get(0);

					result = paymentConfiguration;

					cacheResult(paymentConfiguration);

					if ((paymentConfiguration.getCompanyId() != companyId) ||
							(paymentConfiguration.getPaymentGatewayType() == null) ||
							!paymentConfiguration.getPaymentGatewayType()
													 .equals(paymentGatewayType)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_PGT,
							finderArgs, paymentConfiguration);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_PGT,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PaymentConfiguration)result;
		}
	}

	/**
	 * Removes the payment configuration where companyId = &#63; and paymentGatewayType = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param paymentGatewayType the payment gateway type
	 * @return the payment configuration that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration removeByC_PGT(long companyId,
		String paymentGatewayType)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = findByC_PGT(companyId,
				paymentGatewayType);

		return remove(paymentConfiguration);
	}

	/**
	 * Returns the number of payment configurations where companyId = &#63; and paymentGatewayType = &#63;.
	 *
	 * @param companyId the company ID
	 * @param paymentGatewayType the payment gateway type
	 * @return the number of matching payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_PGT(long companyId, String paymentGatewayType)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_PGT;

		Object[] finderArgs = new Object[] { companyId, paymentGatewayType };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PAYMENTCONFIGURATION_WHERE);

			query.append(_FINDER_COLUMN_C_PGT_COMPANYID_2);

			boolean bindPaymentGatewayType = false;

			if (paymentGatewayType == null) {
				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_1);
			}
			else if (paymentGatewayType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_3);
			}
			else {
				bindPaymentGatewayType = true;

				query.append(_FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindPaymentGatewayType) {
					qPos.add(paymentGatewayType);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_PGT_COMPANYID_2 = "paymentConfiguration.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_1 = "paymentConfiguration.paymentGatewayType IS NULL";
	private static final String _FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_2 = "paymentConfiguration.paymentGatewayType = ?";
	private static final String _FINDER_COLUMN_C_PGT_PAYMENTGATEWAYTYPE_3 = "(paymentConfiguration.paymentGatewayType IS NULL OR paymentConfiguration.paymentGatewayType = '')";

	public PaymentConfigurationPersistenceImpl() {
		setModelClass(PaymentConfiguration.class);
	}

	/**
	 * Caches the payment configuration in the entity cache if it is enabled.
	 *
	 * @param paymentConfiguration the payment configuration
	 */
	@Override
	public void cacheResult(PaymentConfiguration paymentConfiguration) {
		EntityCacheUtil.putResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			paymentConfiguration.getPrimaryKey(), paymentConfiguration);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_PGT,
			new Object[] {
				paymentConfiguration.getCompanyId(),
				paymentConfiguration.getPaymentGatewayType()
			}, paymentConfiguration);

		paymentConfiguration.resetOriginalValues();
	}

	/**
	 * Caches the payment configurations in the entity cache if it is enabled.
	 *
	 * @param paymentConfigurations the payment configurations
	 */
	@Override
	public void cacheResult(List<PaymentConfiguration> paymentConfigurations) {
		for (PaymentConfiguration paymentConfiguration : paymentConfigurations) {
			if (EntityCacheUtil.getResult(
						PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
						PaymentConfigurationImpl.class,
						paymentConfiguration.getPrimaryKey()) == null) {
				cacheResult(paymentConfiguration);
			}
			else {
				paymentConfiguration.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all payment configurations.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PaymentConfigurationImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PaymentConfigurationImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the payment configuration.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PaymentConfiguration paymentConfiguration) {
		EntityCacheUtil.removeResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationImpl.class, paymentConfiguration.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(paymentConfiguration);
	}

	@Override
	public void clearCache(List<PaymentConfiguration> paymentConfigurations) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PaymentConfiguration paymentConfiguration : paymentConfigurations) {
			EntityCacheUtil.removeResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
				PaymentConfigurationImpl.class,
				paymentConfiguration.getPrimaryKey());

			clearUniqueFindersCache(paymentConfiguration);
		}
	}

	protected void cacheUniqueFindersCache(
		PaymentConfiguration paymentConfiguration) {
		if (paymentConfiguration.isNew()) {
			Object[] args = new Object[] {
					paymentConfiguration.getCompanyId(),
					paymentConfiguration.getPaymentGatewayType()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_PGT, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_PGT, args,
				paymentConfiguration);
		}
		else {
			PaymentConfigurationModelImpl paymentConfigurationModelImpl = (PaymentConfigurationModelImpl)paymentConfiguration;

			if ((paymentConfigurationModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_C_PGT.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						paymentConfiguration.getCompanyId(),
						paymentConfiguration.getPaymentGatewayType()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_PGT, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_C_PGT, args,
					paymentConfiguration);
			}
		}
	}

	protected void clearUniqueFindersCache(
		PaymentConfiguration paymentConfiguration) {
		PaymentConfigurationModelImpl paymentConfigurationModelImpl = (PaymentConfigurationModelImpl)paymentConfiguration;

		Object[] args = new Object[] {
				paymentConfiguration.getCompanyId(),
				paymentConfiguration.getPaymentGatewayType()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_PGT, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_PGT, args);

		if ((paymentConfigurationModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_C_PGT.getColumnBitmask()) != 0) {
			args = new Object[] {
					paymentConfigurationModelImpl.getOriginalCompanyId(),
					paymentConfigurationModelImpl.getOriginalPaymentGatewayType()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_PGT, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_C_PGT, args);
		}
	}

	/**
	 * Creates a new payment configuration with the primary key. Does not add the payment configuration to the database.
	 *
	 * @param id the primary key for the new payment configuration
	 * @return the new payment configuration
	 */
	@Override
	public PaymentConfiguration create(long id) {
		PaymentConfiguration paymentConfiguration = new PaymentConfigurationImpl();

		paymentConfiguration.setNew(true);
		paymentConfiguration.setPrimaryKey(id);

		return paymentConfiguration;
	}

	/**
	 * Removes the payment configuration with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the payment configuration
	 * @return the payment configuration that was removed
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration remove(long id)
		throws NoSuchPaymentConfigurationException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the payment configuration with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the payment configuration
	 * @return the payment configuration that was removed
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration remove(Serializable primaryKey)
		throws NoSuchPaymentConfigurationException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PaymentConfiguration paymentConfiguration = (PaymentConfiguration)session.get(PaymentConfigurationImpl.class,
					primaryKey);

			if (paymentConfiguration == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPaymentConfigurationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(paymentConfiguration);
		}
		catch (NoSuchPaymentConfigurationException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PaymentConfiguration removeImpl(
		PaymentConfiguration paymentConfiguration) throws SystemException {
		paymentConfiguration = toUnwrappedModel(paymentConfiguration);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(paymentConfiguration)) {
				paymentConfiguration = (PaymentConfiguration)session.get(PaymentConfigurationImpl.class,
						paymentConfiguration.getPrimaryKeyObj());
			}

			if (paymentConfiguration != null) {
				session.delete(paymentConfiguration);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (paymentConfiguration != null) {
			clearCache(paymentConfiguration);
		}

		return paymentConfiguration;
	}

	@Override
	public PaymentConfiguration updateImpl(
		com.esquare.ecommerce.model.PaymentConfiguration paymentConfiguration)
		throws SystemException {
		paymentConfiguration = toUnwrappedModel(paymentConfiguration);

		boolean isNew = paymentConfiguration.isNew();

		PaymentConfigurationModelImpl paymentConfigurationModelImpl = (PaymentConfigurationModelImpl)paymentConfiguration;

		Session session = null;

		try {
			session = openSession();

			if (paymentConfiguration.isNew()) {
				session.save(paymentConfiguration);

				paymentConfiguration.setNew(false);
			}
			else {
				session.merge(paymentConfiguration);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PaymentConfigurationModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((paymentConfigurationModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						paymentConfigurationModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { paymentConfigurationModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		EntityCacheUtil.putResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
			PaymentConfigurationImpl.class,
			paymentConfiguration.getPrimaryKey(), paymentConfiguration);

		clearUniqueFindersCache(paymentConfiguration);
		cacheUniqueFindersCache(paymentConfiguration);

		return paymentConfiguration;
	}

	protected PaymentConfiguration toUnwrappedModel(
		PaymentConfiguration paymentConfiguration) {
		if (paymentConfiguration instanceof PaymentConfigurationImpl) {
			return paymentConfiguration;
		}

		PaymentConfigurationImpl paymentConfigurationImpl = new PaymentConfigurationImpl();

		paymentConfigurationImpl.setNew(paymentConfiguration.isNew());
		paymentConfigurationImpl.setPrimaryKey(paymentConfiguration.getPrimaryKey());

		paymentConfigurationImpl.setId(paymentConfiguration.getId());
		paymentConfigurationImpl.setCompanyId(paymentConfiguration.getCompanyId());
		paymentConfigurationImpl.setUserId(paymentConfiguration.getUserId());
		paymentConfigurationImpl.setPaymentGatewayType(paymentConfiguration.getPaymentGatewayType());
		paymentConfigurationImpl.setMerchantId(paymentConfiguration.getMerchantId());
		paymentConfigurationImpl.setSecretKey(paymentConfiguration.getSecretKey());
		paymentConfigurationImpl.setStatus(paymentConfiguration.isStatus());
		paymentConfigurationImpl.setCreateDate(paymentConfiguration.getCreateDate());
		paymentConfigurationImpl.setModifiedDate(paymentConfiguration.getModifiedDate());

		return paymentConfigurationImpl;
	}

	/**
	 * Returns the payment configuration with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment configuration
	 * @return the payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPaymentConfigurationException, SystemException {
		PaymentConfiguration paymentConfiguration = fetchByPrimaryKey(primaryKey);

		if (paymentConfiguration == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPaymentConfigurationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return paymentConfiguration;
	}

	/**
	 * Returns the payment configuration with the primary key or throws a {@link com.esquare.ecommerce.NoSuchPaymentConfigurationException} if it could not be found.
	 *
	 * @param id the primary key of the payment configuration
	 * @return the payment configuration
	 * @throws com.esquare.ecommerce.NoSuchPaymentConfigurationException if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration findByPrimaryKey(long id)
		throws NoSuchPaymentConfigurationException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the payment configuration with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment configuration
	 * @return the payment configuration, or <code>null</code> if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PaymentConfiguration paymentConfiguration = (PaymentConfiguration)EntityCacheUtil.getResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
				PaymentConfigurationImpl.class, primaryKey);

		if (paymentConfiguration == _nullPaymentConfiguration) {
			return null;
		}

		if (paymentConfiguration == null) {
			Session session = null;

			try {
				session = openSession();

				paymentConfiguration = (PaymentConfiguration)session.get(PaymentConfigurationImpl.class,
						primaryKey);

				if (paymentConfiguration != null) {
					cacheResult(paymentConfiguration);
				}
				else {
					EntityCacheUtil.putResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
						PaymentConfigurationImpl.class, primaryKey,
						_nullPaymentConfiguration);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PaymentConfigurationModelImpl.ENTITY_CACHE_ENABLED,
					PaymentConfigurationImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return paymentConfiguration;
	}

	/**
	 * Returns the payment configuration with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the payment configuration
	 * @return the payment configuration, or <code>null</code> if a payment configuration with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentConfiguration fetchByPrimaryKey(long id)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the payment configurations.
	 *
	 * @return the payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the payment configurations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentConfigurationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of payment configurations
	 * @param end the upper bound of the range of payment configurations (not inclusive)
	 * @return the range of payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the payment configurations.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentConfigurationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of payment configurations
	 * @param end the upper bound of the range of payment configurations (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentConfiguration> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PaymentConfiguration> list = (List<PaymentConfiguration>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PAYMENTCONFIGURATION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PAYMENTCONFIGURATION;

				if (pagination) {
					sql = sql.concat(PaymentConfigurationModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PaymentConfiguration>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PaymentConfiguration>(list);
				}
				else {
					list = (List<PaymentConfiguration>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the payment configurations from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PaymentConfiguration paymentConfiguration : findAll()) {
			remove(paymentConfiguration);
		}
	}

	/**
	 * Returns the number of payment configurations.
	 *
	 * @return the number of payment configurations
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PAYMENTCONFIGURATION);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the payment configuration persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.PaymentConfiguration")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PaymentConfiguration>> listenersList = new ArrayList<ModelListener<PaymentConfiguration>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PaymentConfiguration>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PaymentConfigurationImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PAYMENTCONFIGURATION = "SELECT paymentConfiguration FROM PaymentConfiguration paymentConfiguration";
	private static final String _SQL_SELECT_PAYMENTCONFIGURATION_WHERE = "SELECT paymentConfiguration FROM PaymentConfiguration paymentConfiguration WHERE ";
	private static final String _SQL_COUNT_PAYMENTCONFIGURATION = "SELECT COUNT(paymentConfiguration) FROM PaymentConfiguration paymentConfiguration";
	private static final String _SQL_COUNT_PAYMENTCONFIGURATION_WHERE = "SELECT COUNT(paymentConfiguration) FROM PaymentConfiguration paymentConfiguration WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "paymentConfiguration.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PaymentConfiguration exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PaymentConfiguration exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PaymentConfigurationPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static PaymentConfiguration _nullPaymentConfiguration = new PaymentConfigurationImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PaymentConfiguration> toCacheModel() {
				return _nullPaymentConfigurationCacheModel;
			}
		};

	private static CacheModel<PaymentConfiguration> _nullPaymentConfigurationCacheModel =
		new CacheModel<PaymentConfiguration>() {
			@Override
			public PaymentConfiguration toEntityModel() {
				return _nullPaymentConfiguration;
			}
		};
}