package com.esquare.ecommerce.ecartebs.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import com.esquare.ecommerce.dashboard.util.DashboardUtil;
import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.EcartEbsPayment;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.model.impl.EcartEbsPaymentImpl;
import com.esquare.ecommerce.model.impl.PaymentUserDetailImpl;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.EcartEbsPaymentLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.InstanceUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class EcartEbsAction extends MVCPortlet {

	public String VIRTUAL_HOST = PortletPropsValues.INSTANCE_VIRTUAL_HOST;

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		resourceResponse.setContentType("text/javascript");
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		try {
			String instName = resourceRequest.getParameter("param1")
					.replace(VIRTUAL_HOST, StringPool.BLANK)
					.replaceAll("\\.", "-")
					+ "." + EECConstants.ECART_WEBID;
			Company company = CompanyLocalServiceUtil
					.getCompanyByWebId(instName);
			ArrayList<String> strList = new ArrayList<String>();
			strList.add(company.getWebId());
			strList.add(String.valueOf(company.getCompanyId()));
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject.toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		}
	}

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		
		System.out.println("Saleem");

		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		System.out.println("cmd>>>>"+cmd);
		
		

		String newStoreName = ParamUtil
				.getString(actionRequest, "newStoreName");
		String emailAddress = ParamUtil.getString(actionRequest, "email");
		String password = ParamUtil.getString(actionRequest, "password");
		String packageType = ParamUtil.getString(actionRequest, "packageType");
		String ecartAutoId = ParamUtil.getString(actionRequest, "ecartAutoId");
		PortletSession ps = actionRequest.getPortletSession();

		if (Validator.isNotNull(cmd)
				&& cmd.equalsIgnoreCase(EECConstants.ADD_EBS)) {
			System.out.println("Not null add Mohamed");
			HashMap<String, String> insMap = new HashMap<String, String>();
			insMap.put("packageType", packageType);
			insMap.put("email", emailAddress);
			insMap.put("password", password);
			insMap.put("ecartAutoId", ecartAutoId);
			insMap.put("webId", newStoreName);
			ps.setAttribute("PACKAGE_DETAILS", insMap);
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/ecart_ebs/detail.jsp");
		} else if (Validator.isNotNull(cmd)
				&& cmd.equalsIgnoreCase(EECConstants.PAY_EBS)) {
			System.out.println("pay ebs");
			// getEBSResponse(actionRequest);
			EECUtil.getEBSResponse(actionRequest);
			HashMap<String, String> map = (HashMap<String, String>) actionRequest
					.getAttribute("EBSResponse");
			long responseCode = Long.parseLong(map.get("ResponseCode"));
			String isFlagged = map.get("IsFlagged");
			String responseMessage = map.get("ResponseMessage");
			String dateCreated = map.get("DateCreated");
			String amount = map.get("Amount").toString();
			long transactionId = Long.parseLong(map.get("TransactionID"));
			String orderId = map.get("MerchantRefNo").toString();
			long paymentMethod = Long
					.parseLong(map.get("PaymentMethod").trim());
			long paymentId = Long.parseLong(map.get("PaymentID"));
			String BillingCity = map.get("BillingCity");
			String BillingCountry = map.get("BillingCountry");
			String BillingEmail = map.get("BillingEmail");
			String BillingName = map.get("BillingName");
			String BillingPhone = map.get("BillingPhone");
			String BillingPostalCode = map.get("BillingPostalCode");
			String DeliveryState = map.get("DeliveryState");

			if (responseCode == 0) {
				if (isFlagged.equalsIgnoreCase("NO")) {
					HashMap<String, String> instanceMap = (HashMap<String, String>) ps
							.getAttribute("PACKAGE_DETAILS");
					String webId = instanceMap.get("webId");
					emailAddress = instanceMap.get("email");
					packageType = instanceMap.get("packageType");
					password = instanceMap.get("password");
					ps.removeAttribute("PACKAGE_DETAILS");

					String virtualHost = InstanceUtil.createInstance(
							actionRequest, webId, emailAddress, password,
							packageType);

					Company company = null;
					Instance instance = null;
					try {
						company = CompanyLocalServiceUtil
								.fetchCompanyByVirtualHost(virtualHost);

						instance = InstanceLocalServiceUtil.getInstance(company
								.getCompanyId());
						instance.setPackageType(packageType);
						InstanceLocalServiceUtil.updateInstance(instance);

						updateCartPermission(company.getCompanyId(),
								packageType, amount);
						QName qName = new QName("http://esquareinfo.com",
								"instanceinfo", "x");
						actionResponse.setEvent(qName, virtualHost);

						// EcartEbsPayment
						EcartEbsPayment ecartEbsPayment = new EcartEbsPaymentImpl();
						long ebsTrackId = CounterLocalServiceUtil
								.increment("EcartEbsPayment.class");
						ecartEbsPayment.setEbsTrackId(ebsTrackId);
						ecartEbsPayment.setCompanyId(company.getCompanyId());
						ecartEbsPayment.setEcartAutoId(ecartAutoId);
						ecartEbsPayment.setUserId(instance.getInstanceUserId());
						ecartEbsPayment.setResponseCode(responseCode);
						ecartEbsPayment.setResponseMessage(responseMessage);
						ecartEbsPayment.setPaidDate(EECUtil.stringToDate(
								dateCreated, "yyyy-MM-dd hh:mm:ss"));
						ecartEbsPayment.setPaymentId(paymentId);
						ecartEbsPayment.setPaymentMethod(paymentMethod);
						ecartEbsPayment.setAmount(amount);
						ecartEbsPayment.setIsFlagged(isFlagged);
						ecartEbsPayment.setTransactionId(transactionId);
						EcartEbsPaymentLocalServiceUtil
								.addEcartEbsPayment(ecartEbsPayment);

						// adding EbsUser details
						PaymentUserDetail paymentUserDetail = new PaymentUserDetailImpl();
						long recId = CounterLocalServiceUtil
								.increment("PaymentUserDetail.class");
						paymentUserDetail.setRecId(recId);
						paymentUserDetail.setCompanyId(company.getCompanyId());
						paymentUserDetail.setUserId(instance
								.getInstanceUserId());
						paymentUserDetail.setEmail(BillingEmail);
						paymentUserDetail.setName(BillingName);
						paymentUserDetail.setPhoneNo(Long
								.parseLong(BillingPhone));
						paymentUserDetail.setPinCode(Long
								.parseLong(BillingPostalCode));
						paymentUserDetail.setCity(BillingCity);
						paymentUserDetail.setCountry(BillingCountry);
						paymentUserDetail.setState(DeliveryState);
						PaymentUserDetailLocalServiceUtil
								.addPaymentUserDetail(paymentUserDetail);

						actionResponse.sendRedirect("/success");
					} catch (Exception e) {
						_log.info(e.getClass());
					}

				} else if (isFlagged.equalsIgnoreCase("YES")) {
					SessionMessages.add(actionRequest,
							EECConstants.PAYMENT_PENDING);
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/ecart_ebs/view.jsp");
				}
			} else {
				SessionMessages.add(actionRequest, "error-in-create");
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/ecart_ebs/view.jsp");
			}
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String title = ParamUtil.getString(renderRequest, "title");
		super.doView(renderRequest, renderResponse);
	}

	protected void updateCartPermission(long companyId, String packageType,
			String amount) {

		try {
			List<CartPermission> cartPermissions = CartPermissionLocalServiceUtil
					.getCartPermissionDetail(companyId);

			for (CartPermission cartPermission : cartPermissions) {
				if (cartPermission.getPermissionKey()
						.equals(EECConstants.MONTH))
					continue;
				String value = DashboardUtil.getPackageValue(packageType,
						cartPermission.getPermissionKey());
				cartPermission.setValue(value);
				CartPermissionLocalServiceUtil
						.updateCartPermission(cartPermission);
			}

			for (CartPermission cartPermissionMth : cartPermissions) {
				if (cartPermissionMth.getPermissionKey().equals(
						EECConstants.MONTH)) {
					cartPermissionMth.setValue(amount);
					CartPermissionLocalServiceUtil
							.updateCartPermission(cartPermissionMth);
				}
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	/*
	 * protected void getEBSResponse(ActionRequest actionRequest) throws
	 * IOException { String DR = ParamUtil.getString(actionRequest, "DR");
	 * String key = PortletPropsValues.EBS_SECRET_KEY; StringBuffer data1 = new
	 * StringBuffer().append(DR); for (int i = 0; i < data1.length(); i++) { if
	 * (data1.charAt(i) == ' ') data1.setCharAt(i, '+'); } Base64 base64 = new
	 * Base64(); byte[] data = base64.decode(data1.toString()); RC4 rc4 = new
	 * RC4(key); byte[] result = rc4.rc4(data);
	 * 
	 * ByteArrayInputStream byteIn = new ByteArrayInputStream(result, 0,
	 * result.length); BufferedReader dataIn = new BufferedReader( new
	 * InputStreamReader(byteIn)); String recvString1 = ""; String recvString =
	 * ""; recvString1 = dataIn.readLine(); int i = 0; while (recvString1 !=
	 * null) { i++; if (i > 705) break; recvString += recvString1 + "\n";
	 * recvString1 = dataIn.readLine(); } recvString = recvString.replace("=&",
	 * "=--&"); StringTokenizer st = new StringTokenizer(recvString, "=&");
	 * String field, value; HashMap<String, String> ebsMap = new HashMap<String,
	 * String>(); while (st.hasMoreTokens()) { field = st.nextToken(); value =
	 * st.nextToken(); ebsMap.put(field, value); }
	 * actionRequest.setAttribute("EBSResponse", ebsMap); }
	 */
private static Log _log=LogFactoryUtil.getLog(EcartEbsAction.class);
}
