/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.service.base.CatalogServiceBaseImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the catalog remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.CatalogService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CatalogServiceBaseImpl
 * @see com.esquare.ecommerce.service.CatalogServiceUtil
 */
public class CatalogServiceImpl extends CatalogServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.esquare.ecommerce.service.CatalogServiceUtil} to access the catalog remote service.
	 */
	public Catalog addCatalog(
			long parentCatalogId, String name, String description,boolean showNavigation,long folderId,
			ServiceContext serviceContext)
		throws PortalException, SystemException {

		return catalogLocalService.addCatalog(
			getUserId(), parentCatalogId, name, description, showNavigation, folderId, serviceContext);
	}
	public Catalog updateCatalog(
			long catalogId, long parentCatalogId, String name,
			String description,boolean showNavigation,long folderId,boolean mergeWithParentCatalog,
			ServiceContext serviceContext)
		throws PortalException, SystemException {
		
		return catalogLocalService.updateCatalog(getUserId(),
				catalogId, parentCatalogId, name, description, showNavigation, folderId,
				mergeWithParentCatalog, serviceContext);
	}
	public Catalog deleteCatalog(long catalogId)
			throws PortalException, SystemException {

			return catalogLocalService.deleteCatalog(catalogId);
		}

}