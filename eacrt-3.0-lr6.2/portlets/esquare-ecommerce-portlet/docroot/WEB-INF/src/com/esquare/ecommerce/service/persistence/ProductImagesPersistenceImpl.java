/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchProductImagesException;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.impl.ProductImagesImpl;
import com.esquare.ecommerce.model.impl.ProductImagesModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the product images service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see ProductImagesPersistence
 * @see ProductImagesUtil
 * @generated
 */
public class ProductImagesPersistenceImpl extends BasePersistenceImpl<ProductImages>
	implements ProductImagesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductImagesUtil} to access the product images persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductImagesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesModelImpl.FINDER_CACHE_ENABLED,
			ProductImagesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesModelImpl.FINDER_CACHE_ENABLED,
			ProductImagesImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_INVENTORYID = new FinderPath(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesModelImpl.FINDER_CACHE_ENABLED,
			ProductImagesImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByInventoryId", new String[] { Long.class.getName() },
			ProductImagesModelImpl.INVENTORYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_INVENTORYID = new FinderPath(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByInventoryId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the product images where inventoryId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchProductImagesException} if it could not be found.
	 *
	 * @param inventoryId the inventory ID
	 * @return the matching product images
	 * @throws com.esquare.ecommerce.NoSuchProductImagesException if a matching product images could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages findByInventoryId(long inventoryId)
		throws NoSuchProductImagesException, SystemException {
		ProductImages productImages = fetchByInventoryId(inventoryId);

		if (productImages == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("inventoryId=");
			msg.append(inventoryId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductImagesException(msg.toString());
		}

		return productImages;
	}

	/**
	 * Returns the product images where inventoryId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param inventoryId the inventory ID
	 * @return the matching product images, or <code>null</code> if a matching product images could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages fetchByInventoryId(long inventoryId)
		throws SystemException {
		return fetchByInventoryId(inventoryId, true);
	}

	/**
	 * Returns the product images where inventoryId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param inventoryId the inventory ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching product images, or <code>null</code> if a matching product images could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages fetchByInventoryId(long inventoryId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { inventoryId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					finderArgs, this);
		}

		if (result instanceof ProductImages) {
			ProductImages productImages = (ProductImages)result;

			if ((inventoryId != productImages.getInventoryId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PRODUCTIMAGES_WHERE);

			query.append(_FINDER_COLUMN_INVENTORYID_INVENTORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(inventoryId);

				List<ProductImages> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ProductImagesPersistenceImpl.fetchByInventoryId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ProductImages productImages = list.get(0);

					result = productImages;

					cacheResult(productImages);

					if ((productImages.getInventoryId() != inventoryId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
							finderArgs, productImages);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProductImages)result;
		}
	}

	/**
	 * Removes the product images where inventoryId = &#63; from the database.
	 *
	 * @param inventoryId the inventory ID
	 * @return the product images that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages removeByInventoryId(long inventoryId)
		throws NoSuchProductImagesException, SystemException {
		ProductImages productImages = findByInventoryId(inventoryId);

		return remove(productImages);
	}

	/**
	 * Returns the number of product imageses where inventoryId = &#63;.
	 *
	 * @param inventoryId the inventory ID
	 * @return the number of matching product imageses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByInventoryId(long inventoryId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_INVENTORYID;

		Object[] finderArgs = new Object[] { inventoryId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTIMAGES_WHERE);

			query.append(_FINDER_COLUMN_INVENTORYID_INVENTORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(inventoryId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INVENTORYID_INVENTORYID_2 = "productImages.inventoryId = ?";

	public ProductImagesPersistenceImpl() {
		setModelClass(ProductImages.class);
	}

	/**
	 * Caches the product images in the entity cache if it is enabled.
	 *
	 * @param productImages the product images
	 */
	@Override
	public void cacheResult(ProductImages productImages) {
		EntityCacheUtil.putResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesImpl.class, productImages.getPrimaryKey(),
			productImages);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
			new Object[] { productImages.getInventoryId() }, productImages);

		productImages.resetOriginalValues();
	}

	/**
	 * Caches the product imageses in the entity cache if it is enabled.
	 *
	 * @param productImageses the product imageses
	 */
	@Override
	public void cacheResult(List<ProductImages> productImageses) {
		for (ProductImages productImages : productImageses) {
			if (EntityCacheUtil.getResult(
						ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
						ProductImagesImpl.class, productImages.getPrimaryKey()) == null) {
				cacheResult(productImages);
			}
			else {
				productImages.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all product imageses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductImagesImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductImagesImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the product images.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProductImages productImages) {
		EntityCacheUtil.removeResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesImpl.class, productImages.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(productImages);
	}

	@Override
	public void clearCache(List<ProductImages> productImageses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProductImages productImages : productImageses) {
			EntityCacheUtil.removeResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
				ProductImagesImpl.class, productImages.getPrimaryKey());

			clearUniqueFindersCache(productImages);
		}
	}

	protected void cacheUniqueFindersCache(ProductImages productImages) {
		if (productImages.isNew()) {
			Object[] args = new Object[] { productImages.getInventoryId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_INVENTORYID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID, args,
				productImages);
		}
		else {
			ProductImagesModelImpl productImagesModelImpl = (ProductImagesModelImpl)productImages;

			if ((productImagesModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_INVENTORYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { productImages.getInventoryId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_INVENTORYID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					args, productImages);
			}
		}
	}

	protected void clearUniqueFindersCache(ProductImages productImages) {
		ProductImagesModelImpl productImagesModelImpl = (ProductImagesModelImpl)productImages;

		Object[] args = new Object[] { productImages.getInventoryId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVENTORYID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID, args);

		if ((productImagesModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_INVENTORYID.getColumnBitmask()) != 0) {
			args = new Object[] { productImagesModelImpl.getOriginalInventoryId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVENTORYID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID, args);
		}
	}

	/**
	 * Creates a new product images with the primary key. Does not add the product images to the database.
	 *
	 * @param recId the primary key for the new product images
	 * @return the new product images
	 */
	@Override
	public ProductImages create(long recId) {
		ProductImages productImages = new ProductImagesImpl();

		productImages.setNew(true);
		productImages.setPrimaryKey(recId);

		return productImages;
	}

	/**
	 * Removes the product images with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param recId the primary key of the product images
	 * @return the product images that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductImagesException if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages remove(long recId)
		throws NoSuchProductImagesException, SystemException {
		return remove((Serializable)recId);
	}

	/**
	 * Removes the product images with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the product images
	 * @return the product images that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductImagesException if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages remove(Serializable primaryKey)
		throws NoSuchProductImagesException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ProductImages productImages = (ProductImages)session.get(ProductImagesImpl.class,
					primaryKey);

			if (productImages == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductImagesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(productImages);
		}
		catch (NoSuchProductImagesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProductImages removeImpl(ProductImages productImages)
		throws SystemException {
		productImages = toUnwrappedModel(productImages);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(productImages)) {
				productImages = (ProductImages)session.get(ProductImagesImpl.class,
						productImages.getPrimaryKeyObj());
			}

			if (productImages != null) {
				session.delete(productImages);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (productImages != null) {
			clearCache(productImages);
		}

		return productImages;
	}

	@Override
	public ProductImages updateImpl(
		com.esquare.ecommerce.model.ProductImages productImages)
		throws SystemException {
		productImages = toUnwrappedModel(productImages);

		boolean isNew = productImages.isNew();

		Session session = null;

		try {
			session = openSession();

			if (productImages.isNew()) {
				session.save(productImages);

				productImages.setNew(false);
			}
			else {
				session.merge(productImages);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductImagesModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
			ProductImagesImpl.class, productImages.getPrimaryKey(),
			productImages);

		clearUniqueFindersCache(productImages);
		cacheUniqueFindersCache(productImages);

		return productImages;
	}

	protected ProductImages toUnwrappedModel(ProductImages productImages) {
		if (productImages instanceof ProductImagesImpl) {
			return productImages;
		}

		ProductImagesImpl productImagesImpl = new ProductImagesImpl();

		productImagesImpl.setNew(productImages.isNew());
		productImagesImpl.setPrimaryKey(productImages.getPrimaryKey());

		productImagesImpl.setRecId(productImages.getRecId());
		productImagesImpl.setInventoryId(productImages.getInventoryId());
		productImagesImpl.setImageId(productImages.getImageId());

		return productImagesImpl;
	}

	/**
	 * Returns the product images with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the product images
	 * @return the product images
	 * @throws com.esquare.ecommerce.NoSuchProductImagesException if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductImagesException, SystemException {
		ProductImages productImages = fetchByPrimaryKey(primaryKey);

		if (productImages == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductImagesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return productImages;
	}

	/**
	 * Returns the product images with the primary key or throws a {@link com.esquare.ecommerce.NoSuchProductImagesException} if it could not be found.
	 *
	 * @param recId the primary key of the product images
	 * @return the product images
	 * @throws com.esquare.ecommerce.NoSuchProductImagesException if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages findByPrimaryKey(long recId)
		throws NoSuchProductImagesException, SystemException {
		return findByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns the product images with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the product images
	 * @return the product images, or <code>null</code> if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ProductImages productImages = (ProductImages)EntityCacheUtil.getResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
				ProductImagesImpl.class, primaryKey);

		if (productImages == _nullProductImages) {
			return null;
		}

		if (productImages == null) {
			Session session = null;

			try {
				session = openSession();

				productImages = (ProductImages)session.get(ProductImagesImpl.class,
						primaryKey);

				if (productImages != null) {
					cacheResult(productImages);
				}
				else {
					EntityCacheUtil.putResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
						ProductImagesImpl.class, primaryKey, _nullProductImages);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ProductImagesModelImpl.ENTITY_CACHE_ENABLED,
					ProductImagesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return productImages;
	}

	/**
	 * Returns the product images with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param recId the primary key of the product images
	 * @return the product images, or <code>null</code> if a product images with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductImages fetchByPrimaryKey(long recId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns all the product imageses.
	 *
	 * @return the product imageses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductImages> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product imageses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductImagesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product imageses
	 * @param end the upper bound of the range of product imageses (not inclusive)
	 * @return the range of product imageses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductImages> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the product imageses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductImagesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product imageses
	 * @param end the upper bound of the range of product imageses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of product imageses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductImages> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProductImages> list = (List<ProductImages>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTIMAGES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTIMAGES;

				if (pagination) {
					sql = sql.concat(ProductImagesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProductImages>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductImages>(list);
				}
				else {
					list = (List<ProductImages>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the product imageses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ProductImages productImages : findAll()) {
			remove(productImages);
		}
	}

	/**
	 * Returns the number of product imageses.
	 *
	 * @return the number of product imageses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTIMAGES);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the product images persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.ProductImages")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ProductImages>> listenersList = new ArrayList<ModelListener<ProductImages>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ProductImages>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductImagesImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRODUCTIMAGES = "SELECT productImages FROM ProductImages productImages";
	private static final String _SQL_SELECT_PRODUCTIMAGES_WHERE = "SELECT productImages FROM ProductImages productImages WHERE ";
	private static final String _SQL_COUNT_PRODUCTIMAGES = "SELECT COUNT(productImages) FROM ProductImages productImages";
	private static final String _SQL_COUNT_PRODUCTIMAGES_WHERE = "SELECT COUNT(productImages) FROM ProductImages productImages WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "productImages.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProductImages exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProductImages exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductImagesPersistenceImpl.class);
	private static ProductImages _nullProductImages = new ProductImagesImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ProductImages> toCacheModel() {
				return _nullProductImagesCacheModel;
			}
		};

	private static CacheModel<ProductImages> _nullProductImagesCacheModel = new CacheModel<ProductImages>() {
			@Override
			public ProductImages toEntityModel() {
				return _nullProductImages;
			}
		};
}