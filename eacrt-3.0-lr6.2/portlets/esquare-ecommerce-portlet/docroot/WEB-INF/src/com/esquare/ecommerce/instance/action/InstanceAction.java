package com.esquare.ecommerce.instance.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.InstanceUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class InstanceAction extends MVCPortlet {

	public String VIRTUAL_HOST = PortletPropsValues.INSTANCE_VIRTUAL_HOST;

	public void editInstance(ActionRequest actionRequest,
			ActionResponse actionResponse) {

		try {
			/*checkCaptcha(actionRequest);*/
			createInstance(actionRequest, actionResponse);
			actionResponse.sendRedirect("/success");
		} catch (Exception e) {
			if (e instanceof IOException) {
				_log.info(e.getClass());
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/instance/list.jsp");
				
			}
			else if (e instanceof CaptchaTextException
					|| e instanceof CaptchaMaxChallengesException) {
				SessionErrors.add(actionRequest, e.getClass(), e);
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/instance/create_instance.jsp");
			}
			
		} 
	}

	private static void createInstance(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		String webId = ParamUtil.getString(actionRequest,
				PortletPropsValues.INSTANCE_NAME);
		String emailAddress = ParamUtil.getString(actionRequest,
				PortletPropsValues.INSTANCE_EMAILID);
		String password = ParamUtil.getString(actionRequest,
				PortletPropsValues.INSTANCE_PASSWORD);
		
		System.out.println("instance name >>"+webId);
		System.out.println("emailAddress>>>"+emailAddress);
		System.out.println("passw0rd>>>"+password);

		String packageType = ParamUtil.getString(actionRequest, "packageType");

		String virtualHost = InstanceUtil.createInstance(actionRequest, webId,
				emailAddress, password, packageType);

		actionRequest.setAttribute(PortletPropsValues.INSTANCE_EMAILID,
				emailAddress);
		actionRequest.setAttribute(PortletPropsValues.INSTANCE_PASSWORD,
				password);
		actionRequest.setAttribute("webId", virtualHost);

		// IPC Event Call
		QName qName = new QName("http://esquareinfo.com", "instanceinfo", "x");
		actionResponse.setEvent(qName, virtualHost);
	}

	/*protected void checkCaptcha(ActionRequest actionRequest)
			throws CaptchaException {
		CaptchaUtil.check(actionRequest);
	}
*/
	/*@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String cmd = ParamUtil.getString(resourceRequest, EECConstants.CMD);
		if (cmd.equals(EECConstants.CAPTCHA)) {
			try {
				CaptchaUtil.serveImage(resourceRequest, resourceResponse);
			} catch (Exception e) {
				_log.info(e, e);
			}
		} else {
			resourceResponse.setContentType("text/javascript");
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			try {
				String instName = resourceRequest.getParameter("param1")
						.replace(VIRTUAL_HOST, StringPool.BLANK)
						.replaceAll("\\.", "-")
						+ "." + EECConstants.ECART_WEBID;
				Company company = CompanyLocalServiceUtil
						.getCompanyByWebId(instName);
				ArrayList<String> strList = new ArrayList<String>();
				strList.add(company.getWebId());
				strList.add(String.valueOf(company.getCompanyId()));

				jsonObject.put("retVal2", strList.toString());
				PrintWriter writer = resourceResponse.getWriter();
				writer.write(jsonObject.toString());
			} catch (SystemException e) {
				_log.info(e.getClass());
			} catch (PortalException e) {
				_log.info(e.getClass());
			}
		}
	}*/

	private static Log _log = LogFactoryUtil.getLog(InstanceAction.class);
}
