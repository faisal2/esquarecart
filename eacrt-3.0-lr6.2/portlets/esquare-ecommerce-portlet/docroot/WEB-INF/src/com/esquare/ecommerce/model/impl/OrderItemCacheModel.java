/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.OrderItem;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing OrderItem in entity cache.
 *
 * @author Esquare
 * @see OrderItem
 * @generated
 */
public class OrderItemCacheModel implements CacheModel<OrderItem>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(41);

		sb.append("{orderItemId=");
		sb.append(orderItemId);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", productInventoryId=");
		sb.append(productInventoryId);
		sb.append(", sku=");
		sb.append(sku);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", properties=");
		sb.append(properties);
		sb.append(", price=");
		sb.append(price);
		sb.append(", tax=");
		sb.append(tax);
		sb.append(", shippingPrice=");
		sb.append(shippingPrice);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", shippingTrackerId=");
		sb.append(shippingTrackerId);
		sb.append(", orderItemStatus=");
		sb.append(orderItemStatus);
		sb.append(", createdDate=");
		sb.append(createdDate);
		sb.append(", deliveredDate=");
		sb.append(deliveredDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", expectedDeliveryDate=");
		sb.append(expectedDeliveryDate);
		sb.append(", cancelByUser=");
		sb.append(cancelByUser);
		sb.append(", canceledByUserId=");
		sb.append(canceledByUserId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public OrderItem toEntityModel() {
		OrderItemImpl orderItemImpl = new OrderItemImpl();

		orderItemImpl.setOrderItemId(orderItemId);
		orderItemImpl.setOrderId(orderId);
		orderItemImpl.setCompanyId(companyId);

		if (productInventoryId == null) {
			orderItemImpl.setProductInventoryId(StringPool.BLANK);
		}
		else {
			orderItemImpl.setProductInventoryId(productInventoryId);
		}

		if (sku == null) {
			orderItemImpl.setSku(StringPool.BLANK);
		}
		else {
			orderItemImpl.setSku(sku);
		}

		if (name == null) {
			orderItemImpl.setName(StringPool.BLANK);
		}
		else {
			orderItemImpl.setName(name);
		}

		if (description == null) {
			orderItemImpl.setDescription(StringPool.BLANK);
		}
		else {
			orderItemImpl.setDescription(description);
		}

		if (properties == null) {
			orderItemImpl.setProperties(StringPool.BLANK);
		}
		else {
			orderItemImpl.setProperties(properties);
		}

		orderItemImpl.setPrice(price);
		orderItemImpl.setTax(tax);
		orderItemImpl.setShippingPrice(shippingPrice);
		orderItemImpl.setQuantity(quantity);

		if (shippingTrackerId == null) {
			orderItemImpl.setShippingTrackerId(StringPool.BLANK);
		}
		else {
			orderItemImpl.setShippingTrackerId(shippingTrackerId);
		}

		if (orderItemStatus == null) {
			orderItemImpl.setOrderItemStatus(StringPool.BLANK);
		}
		else {
			orderItemImpl.setOrderItemStatus(orderItemStatus);
		}

		if (createdDate == Long.MIN_VALUE) {
			orderItemImpl.setCreatedDate(null);
		}
		else {
			orderItemImpl.setCreatedDate(new Date(createdDate));
		}

		if (deliveredDate == Long.MIN_VALUE) {
			orderItemImpl.setDeliveredDate(null);
		}
		else {
			orderItemImpl.setDeliveredDate(new Date(deliveredDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			orderItemImpl.setModifiedDate(null);
		}
		else {
			orderItemImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (expectedDeliveryDate == Long.MIN_VALUE) {
			orderItemImpl.setExpectedDeliveryDate(null);
		}
		else {
			orderItemImpl.setExpectedDeliveryDate(new Date(expectedDeliveryDate));
		}

		orderItemImpl.setCancelByUser(cancelByUser);
		orderItemImpl.setCanceledByUserId(canceledByUserId);

		orderItemImpl.resetOriginalValues();

		return orderItemImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		orderItemId = objectInput.readLong();
		orderId = objectInput.readLong();
		companyId = objectInput.readLong();
		productInventoryId = objectInput.readUTF();
		sku = objectInput.readUTF();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
		properties = objectInput.readUTF();
		price = objectInput.readDouble();
		tax = objectInput.readDouble();
		shippingPrice = objectInput.readDouble();
		quantity = objectInput.readInt();
		shippingTrackerId = objectInput.readUTF();
		orderItemStatus = objectInput.readUTF();
		createdDate = objectInput.readLong();
		deliveredDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		expectedDeliveryDate = objectInput.readLong();
		cancelByUser = objectInput.readBoolean();
		canceledByUserId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(orderItemId);
		objectOutput.writeLong(orderId);
		objectOutput.writeLong(companyId);

		if (productInventoryId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(productInventoryId);
		}

		if (sku == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(sku);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (properties == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(properties);
		}

		objectOutput.writeDouble(price);
		objectOutput.writeDouble(tax);
		objectOutput.writeDouble(shippingPrice);
		objectOutput.writeInt(quantity);

		if (shippingTrackerId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingTrackerId);
		}

		if (orderItemStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(orderItemStatus);
		}

		objectOutput.writeLong(createdDate);
		objectOutput.writeLong(deliveredDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(expectedDeliveryDate);
		objectOutput.writeBoolean(cancelByUser);
		objectOutput.writeLong(canceledByUserId);
	}

	public long orderItemId;
	public long orderId;
	public long companyId;
	public String productInventoryId;
	public String sku;
	public String name;
	public String description;
	public String properties;
	public double price;
	public double tax;
	public double shippingPrice;
	public int quantity;
	public String shippingTrackerId;
	public String orderItemStatus;
	public long createdDate;
	public long deliveredDate;
	public long modifiedDate;
	public long expectedDeliveryDate;
	public boolean cancelByUser;
	public long canceledByUserId;
}