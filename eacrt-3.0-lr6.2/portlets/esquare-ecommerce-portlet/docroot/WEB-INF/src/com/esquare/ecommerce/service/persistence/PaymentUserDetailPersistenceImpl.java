/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchPaymentUserDetailException;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.model.impl.PaymentUserDetailImpl;
import com.esquare.ecommerce.model.impl.PaymentUserDetailModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the payment user detail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see PaymentUserDetailPersistence
 * @see PaymentUserDetailUtil
 * @generated
 */
public class PaymentUserDetailPersistenceImpl extends BasePersistenceImpl<PaymentUserDetail>
	implements PaymentUserDetailPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PaymentUserDetailUtil} to access the payment user detail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PaymentUserDetailImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED,
			PaymentUserDetailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED,
			PaymentUserDetailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_USERID = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED,
			PaymentUserDetailImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUserId", new String[] { Long.class.getName() },
			PaymentUserDetailModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the payment user detail where userId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchPaymentUserDetailException} if it could not be found.
	 *
	 * @param userId the user ID
	 * @return the matching payment user detail
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail findByUserId(long userId)
		throws NoSuchPaymentUserDetailException, SystemException {
		PaymentUserDetail paymentUserDetail = fetchByUserId(userId);

		if (paymentUserDetail == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPaymentUserDetailException(msg.toString());
		}

		return paymentUserDetail;
	}

	/**
	 * Returns the payment user detail where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param userId the user ID
	 * @return the matching payment user detail, or <code>null</code> if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByUserId(long userId)
		throws SystemException {
		return fetchByUserId(userId, true);
	}

	/**
	 * Returns the payment user detail where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching payment user detail, or <code>null</code> if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByUserId(long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_USERID,
					finderArgs, this);
		}

		if (result instanceof PaymentUserDetail) {
			PaymentUserDetail paymentUserDetail = (PaymentUserDetail)result;

			if ((userId != paymentUserDetail.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PAYMENTUSERDETAIL_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				List<PaymentUserDetail> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PaymentUserDetailPersistenceImpl.fetchByUserId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PaymentUserDetail paymentUserDetail = list.get(0);

					result = paymentUserDetail;

					cacheResult(paymentUserDetail);

					if ((paymentUserDetail.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
							finderArgs, paymentUserDetail);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PaymentUserDetail)result;
		}
	}

	/**
	 * Removes the payment user detail where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @return the payment user detail that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail removeByUserId(long userId)
		throws NoSuchPaymentUserDetailException, SystemException {
		PaymentUserDetail paymentUserDetail = findByUserId(userId);

		return remove(paymentUserDetail);
	}

	/**
	 * Returns the number of payment user details where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PAYMENTUSERDETAIL_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "paymentUserDetail.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COMPANYID = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED,
			PaymentUserDetailImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCompanyId", new String[] { Long.class.getName() },
			PaymentUserDetailModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the payment user detail where companyId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchPaymentUserDetailException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @return the matching payment user detail
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail findByCompanyId(long companyId)
		throws NoSuchPaymentUserDetailException, SystemException {
		PaymentUserDetail paymentUserDetail = fetchByCompanyId(companyId);

		if (paymentUserDetail == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchPaymentUserDetailException(msg.toString());
		}

		return paymentUserDetail;
	}

	/**
	 * Returns the payment user detail where companyId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @return the matching payment user detail, or <code>null</code> if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByCompanyId(long companyId)
		throws SystemException {
		return fetchByCompanyId(companyId, true);
	}

	/**
	 * Returns the payment user detail where companyId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching payment user detail, or <code>null</code> if a matching payment user detail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByCompanyId(long companyId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_COMPANYID,
					finderArgs, this);
		}

		if (result instanceof PaymentUserDetail) {
			PaymentUserDetail paymentUserDetail = (PaymentUserDetail)result;

			if ((companyId != paymentUserDetail.getCompanyId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PAYMENTUSERDETAIL_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				List<PaymentUserDetail> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COMPANYID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"PaymentUserDetailPersistenceImpl.fetchByCompanyId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					PaymentUserDetail paymentUserDetail = list.get(0);

					result = paymentUserDetail;

					cacheResult(paymentUserDetail);

					if ((paymentUserDetail.getCompanyId() != companyId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COMPANYID,
							finderArgs, paymentUserDetail);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COMPANYID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PaymentUserDetail)result;
		}
	}

	/**
	 * Removes the payment user detail where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @return the payment user detail that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail removeByCompanyId(long companyId)
		throws NoSuchPaymentUserDetailException, SystemException {
		PaymentUserDetail paymentUserDetail = findByCompanyId(companyId);

		return remove(paymentUserDetail);
	}

	/**
	 * Returns the number of payment user details where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PAYMENTUSERDETAIL_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "paymentUserDetail.companyId = ?";

	public PaymentUserDetailPersistenceImpl() {
		setModelClass(PaymentUserDetail.class);
	}

	/**
	 * Caches the payment user detail in the entity cache if it is enabled.
	 *
	 * @param paymentUserDetail the payment user detail
	 */
	@Override
	public void cacheResult(PaymentUserDetail paymentUserDetail) {
		EntityCacheUtil.putResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailImpl.class, paymentUserDetail.getPrimaryKey(),
			paymentUserDetail);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID,
			new Object[] { paymentUserDetail.getUserId() }, paymentUserDetail);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COMPANYID,
			new Object[] { paymentUserDetail.getCompanyId() }, paymentUserDetail);

		paymentUserDetail.resetOriginalValues();
	}

	/**
	 * Caches the payment user details in the entity cache if it is enabled.
	 *
	 * @param paymentUserDetails the payment user details
	 */
	@Override
	public void cacheResult(List<PaymentUserDetail> paymentUserDetails) {
		for (PaymentUserDetail paymentUserDetail : paymentUserDetails) {
			if (EntityCacheUtil.getResult(
						PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
						PaymentUserDetailImpl.class,
						paymentUserDetail.getPrimaryKey()) == null) {
				cacheResult(paymentUserDetail);
			}
			else {
				paymentUserDetail.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all payment user details.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(PaymentUserDetailImpl.class.getName());
		}

		EntityCacheUtil.clearCache(PaymentUserDetailImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the payment user detail.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PaymentUserDetail paymentUserDetail) {
		EntityCacheUtil.removeResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailImpl.class, paymentUserDetail.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(paymentUserDetail);
	}

	@Override
	public void clearCache(List<PaymentUserDetail> paymentUserDetails) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PaymentUserDetail paymentUserDetail : paymentUserDetails) {
			EntityCacheUtil.removeResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
				PaymentUserDetailImpl.class, paymentUserDetail.getPrimaryKey());

			clearUniqueFindersCache(paymentUserDetail);
		}
	}

	protected void cacheUniqueFindersCache(PaymentUserDetail paymentUserDetail) {
		if (paymentUserDetail.isNew()) {
			Object[] args = new Object[] { paymentUserDetail.getUserId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID, args,
				paymentUserDetail);

			args = new Object[] { paymentUserDetail.getCompanyId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COMPANYID, args,
				paymentUserDetail);
		}
		else {
			PaymentUserDetailModelImpl paymentUserDetailModelImpl = (PaymentUserDetailModelImpl)paymentUserDetail;

			if ((paymentUserDetailModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { paymentUserDetail.getUserId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USERID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USERID, args,
					paymentUserDetail);
			}

			if ((paymentUserDetailModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { paymentUserDetail.getCompanyId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_COMPANYID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_COMPANYID, args,
					paymentUserDetail);
			}
		}
	}

	protected void clearUniqueFindersCache(PaymentUserDetail paymentUserDetail) {
		PaymentUserDetailModelImpl paymentUserDetailModelImpl = (PaymentUserDetailModelImpl)paymentUserDetail;

		Object[] args = new Object[] { paymentUserDetail.getUserId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID, args);

		if ((paymentUserDetailModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_USERID.getColumnBitmask()) != 0) {
			args = new Object[] { paymentUserDetailModelImpl.getOriginalUserId() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USERID, args);
		}

		args = new Object[] { paymentUserDetail.getCompanyId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COMPANYID, args);

		if ((paymentUserDetailModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COMPANYID.getColumnBitmask()) != 0) {
			args = new Object[] {
					paymentUserDetailModelImpl.getOriginalCompanyId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_COMPANYID, args);
		}
	}

	/**
	 * Creates a new payment user detail with the primary key. Does not add the payment user detail to the database.
	 *
	 * @param recId the primary key for the new payment user detail
	 * @return the new payment user detail
	 */
	@Override
	public PaymentUserDetail create(long recId) {
		PaymentUserDetail paymentUserDetail = new PaymentUserDetailImpl();

		paymentUserDetail.setNew(true);
		paymentUserDetail.setPrimaryKey(recId);

		return paymentUserDetail;
	}

	/**
	 * Removes the payment user detail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param recId the primary key of the payment user detail
	 * @return the payment user detail that was removed
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail remove(long recId)
		throws NoSuchPaymentUserDetailException, SystemException {
		return remove((Serializable)recId);
	}

	/**
	 * Removes the payment user detail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the payment user detail
	 * @return the payment user detail that was removed
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail remove(Serializable primaryKey)
		throws NoSuchPaymentUserDetailException, SystemException {
		Session session = null;

		try {
			session = openSession();

			PaymentUserDetail paymentUserDetail = (PaymentUserDetail)session.get(PaymentUserDetailImpl.class,
					primaryKey);

			if (paymentUserDetail == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPaymentUserDetailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(paymentUserDetail);
		}
		catch (NoSuchPaymentUserDetailException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PaymentUserDetail removeImpl(PaymentUserDetail paymentUserDetail)
		throws SystemException {
		paymentUserDetail = toUnwrappedModel(paymentUserDetail);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(paymentUserDetail)) {
				paymentUserDetail = (PaymentUserDetail)session.get(PaymentUserDetailImpl.class,
						paymentUserDetail.getPrimaryKeyObj());
			}

			if (paymentUserDetail != null) {
				session.delete(paymentUserDetail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (paymentUserDetail != null) {
			clearCache(paymentUserDetail);
		}

		return paymentUserDetail;
	}

	@Override
	public PaymentUserDetail updateImpl(
		com.esquare.ecommerce.model.PaymentUserDetail paymentUserDetail)
		throws SystemException {
		paymentUserDetail = toUnwrappedModel(paymentUserDetail);

		boolean isNew = paymentUserDetail.isNew();

		Session session = null;

		try {
			session = openSession();

			if (paymentUserDetail.isNew()) {
				session.save(paymentUserDetail);

				paymentUserDetail.setNew(false);
			}
			else {
				session.merge(paymentUserDetail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PaymentUserDetailModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
			PaymentUserDetailImpl.class, paymentUserDetail.getPrimaryKey(),
			paymentUserDetail);

		clearUniqueFindersCache(paymentUserDetail);
		cacheUniqueFindersCache(paymentUserDetail);

		return paymentUserDetail;
	}

	protected PaymentUserDetail toUnwrappedModel(
		PaymentUserDetail paymentUserDetail) {
		if (paymentUserDetail instanceof PaymentUserDetailImpl) {
			return paymentUserDetail;
		}

		PaymentUserDetailImpl paymentUserDetailImpl = new PaymentUserDetailImpl();

		paymentUserDetailImpl.setNew(paymentUserDetail.isNew());
		paymentUserDetailImpl.setPrimaryKey(paymentUserDetail.getPrimaryKey());

		paymentUserDetailImpl.setRecId(paymentUserDetail.getRecId());
		paymentUserDetailImpl.setCompanyId(paymentUserDetail.getCompanyId());
		paymentUserDetailImpl.setUserId(paymentUserDetail.getUserId());
		paymentUserDetailImpl.setEmail(paymentUserDetail.getEmail());
		paymentUserDetailImpl.setName(paymentUserDetail.getName());
		paymentUserDetailImpl.setPhoneNo(paymentUserDetail.getPhoneNo());
		paymentUserDetailImpl.setPinCode(paymentUserDetail.getPinCode());
		paymentUserDetailImpl.setAddress(paymentUserDetail.getAddress());
		paymentUserDetailImpl.setCountry(paymentUserDetail.getCountry());
		paymentUserDetailImpl.setState(paymentUserDetail.getState());
		paymentUserDetailImpl.setCity(paymentUserDetail.getCity());

		return paymentUserDetailImpl;
	}

	/**
	 * Returns the payment user detail with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment user detail
	 * @return the payment user detail
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPaymentUserDetailException, SystemException {
		PaymentUserDetail paymentUserDetail = fetchByPrimaryKey(primaryKey);

		if (paymentUserDetail == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPaymentUserDetailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return paymentUserDetail;
	}

	/**
	 * Returns the payment user detail with the primary key or throws a {@link com.esquare.ecommerce.NoSuchPaymentUserDetailException} if it could not be found.
	 *
	 * @param recId the primary key of the payment user detail
	 * @return the payment user detail
	 * @throws com.esquare.ecommerce.NoSuchPaymentUserDetailException if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail findByPrimaryKey(long recId)
		throws NoSuchPaymentUserDetailException, SystemException {
		return findByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns the payment user detail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the payment user detail
	 * @return the payment user detail, or <code>null</code> if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		PaymentUserDetail paymentUserDetail = (PaymentUserDetail)EntityCacheUtil.getResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
				PaymentUserDetailImpl.class, primaryKey);

		if (paymentUserDetail == _nullPaymentUserDetail) {
			return null;
		}

		if (paymentUserDetail == null) {
			Session session = null;

			try {
				session = openSession();

				paymentUserDetail = (PaymentUserDetail)session.get(PaymentUserDetailImpl.class,
						primaryKey);

				if (paymentUserDetail != null) {
					cacheResult(paymentUserDetail);
				}
				else {
					EntityCacheUtil.putResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
						PaymentUserDetailImpl.class, primaryKey,
						_nullPaymentUserDetail);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(PaymentUserDetailModelImpl.ENTITY_CACHE_ENABLED,
					PaymentUserDetailImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return paymentUserDetail;
	}

	/**
	 * Returns the payment user detail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param recId the primary key of the payment user detail
	 * @return the payment user detail, or <code>null</code> if a payment user detail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public PaymentUserDetail fetchByPrimaryKey(long recId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns all the payment user details.
	 *
	 * @return the payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentUserDetail> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the payment user details.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentUserDetailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of payment user details
	 * @param end the upper bound of the range of payment user details (not inclusive)
	 * @return the range of payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentUserDetail> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the payment user details.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.PaymentUserDetailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of payment user details
	 * @param end the upper bound of the range of payment user details (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<PaymentUserDetail> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PaymentUserDetail> list = (List<PaymentUserDetail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PAYMENTUSERDETAIL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PAYMENTUSERDETAIL;

				if (pagination) {
					sql = sql.concat(PaymentUserDetailModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PaymentUserDetail>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<PaymentUserDetail>(list);
				}
				else {
					list = (List<PaymentUserDetail>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the payment user details from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (PaymentUserDetail paymentUserDetail : findAll()) {
			remove(paymentUserDetail);
		}
	}

	/**
	 * Returns the number of payment user details.
	 *
	 * @return the number of payment user details
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PAYMENTUSERDETAIL);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the payment user detail persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.PaymentUserDetail")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<PaymentUserDetail>> listenersList = new ArrayList<ModelListener<PaymentUserDetail>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<PaymentUserDetail>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(PaymentUserDetailImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PAYMENTUSERDETAIL = "SELECT paymentUserDetail FROM PaymentUserDetail paymentUserDetail";
	private static final String _SQL_SELECT_PAYMENTUSERDETAIL_WHERE = "SELECT paymentUserDetail FROM PaymentUserDetail paymentUserDetail WHERE ";
	private static final String _SQL_COUNT_PAYMENTUSERDETAIL = "SELECT COUNT(paymentUserDetail) FROM PaymentUserDetail paymentUserDetail";
	private static final String _SQL_COUNT_PAYMENTUSERDETAIL_WHERE = "SELECT COUNT(paymentUserDetail) FROM PaymentUserDetail paymentUserDetail WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "paymentUserDetail.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PaymentUserDetail exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PaymentUserDetail exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(PaymentUserDetailPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"state"
			});
	private static PaymentUserDetail _nullPaymentUserDetail = new PaymentUserDetailImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<PaymentUserDetail> toCacheModel() {
				return _nullPaymentUserDetailCacheModel;
			}
		};

	private static CacheModel<PaymentUserDetail> _nullPaymentUserDetailCacheModel =
		new CacheModel<PaymentUserDetail>() {
			@Override
			public PaymentUserDetail toEntityModel() {
				return _nullPaymentUserDetail;
			}
		};
}