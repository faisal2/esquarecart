/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.CouponCodeException;
import com.esquare.ecommerce.CouponDateException;
import com.esquare.ecommerce.CouponDescriptionException;
import com.esquare.ecommerce.CouponDiscountException;
import com.esquare.ecommerce.CouponMinimumOrderException;
import com.esquare.ecommerce.CouponNameException;
import com.esquare.ecommerce.DuplicateCouponCodeException;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.service.CouponLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.base.CouponLocalServiceBaseImpl;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.MathUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the coupon local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.CouponLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CouponLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.CouponLocalServiceUtil
 */
public class CouponLocalServiceImpl extends CouponLocalServiceBaseImpl {
	public Coupon addCoupon(long userId,String couponCode,
			String couponName, String description, Date startDate,
			Date endDate, boolean neverExpire, boolean active, int couponLimit,
			int userLimit, double minOrder, String discountType,
			double discount, String discountLimit, String limitCatalogs,
			String limitProduct, ServiceContext serviceContext)
			throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);
		long groupId = serviceContext.getScopeGroupId();

		couponCode = couponCode.trim().toUpperCase();

		if (neverExpire) {
			endDate = null;
		}

		if ((endDate != null) && startDate.after(endDate)) {
			throw new CouponDateException();
		}

		Date now = new Date();

		validate(user.getCompanyId(), groupId, couponCode, couponName,
				description, limitCatalogs, limitProduct, minOrder, discount);

		long couponId = counterLocalService.increment();

		Coupon coupon = couponPersistence.create(couponId);

		coupon.setGroupId(groupId);
		coupon.setCompanyId(user.getCompanyId());
		coupon.setUserId(user.getUserId());
		coupon.setVersion(1.0);
		coupon.setUserName(user.getFullName());
		coupon.setCreateDate(now);
		coupon.setCouponCode(couponCode);
		coupon.setCouponName(couponName);
		coupon.setDescription(description);
		coupon.setCouponLimit(couponLimit);
		coupon.setUserLimit(userLimit);
		coupon.setStartDate(startDate);
		coupon.setEndDate(endDate);
		coupon.setActive(active);
		coupon.setMinOrder(minOrder);
		coupon.setDiscount(discount);
		coupon.setDiscountType(discountType);
		coupon.setDiscountLimit(discountLimit);
		coupon.setLimitCatalogs(limitCatalogs);
		coupon.setLimitProducts(limitProduct);
		coupon.setNeverExpire(neverExpire);
		coupon.setArchived(false);

		couponPersistence.update(coupon);

		return coupon;
	}

	protected void validate(long companyId, long groupId, String couponCode,
			String couponName, String description, String limitCatalog,
			String limitProduct, double minOrder, double discount)
			throws PortalException, SystemException {
		if (Validator.isNull(couponCode) || Validator.isNumber(couponCode)
				|| (couponCode.indexOf(CharPool.SPACE) != -1)) {

			throw new CouponCodeException();
		}
		if (couponPersistence.fetchByCouponCode(couponCode,false) != null) {
			throw new DuplicateCouponCodeException();
		}
		validate(companyId, groupId, couponName, description, limitCatalog,
				limitProduct, minOrder, discount);
	}

	protected void validate(long companyId, long groupId, String name,
			String description, String limitCatalog, String limitProduct,
			double minOrder, double discount) throws PortalException,
			SystemException {

		if (Validator.isNull(name)) {
			throw new CouponNameException();
		} else if (Validator.isNull(description)) {
			throw new CouponDescriptionException();
		}

		if (minOrder < 0) {
			throw new CouponMinimumOrderException();
		}

		if (discount < 0) {
			throw new CouponDiscountException();
		}
	}

	public Coupon updateCoupon(long userId, long couponId,
			String couponName, String description, Date startDate,
			Date endDate, boolean neverExpire, boolean active, int couponLimit,
			int userLimit, double minOrder, String discountType,
			double discount, String discountLimit, String limitCatalogs,
			String limitProduct, ServiceContext serviceContext)
			throws PortalException, SystemException {
		Coupon coupon = couponPersistence.fetchByPrimaryKey(couponId);
		User user = userPersistence.findByPrimaryKey(userId);

		if (neverExpire) {
			endDate = null;
		}

		if ((endDate != null) && startDate.after(endDate)) {
			throw new CouponDateException();
		}

		Date now = new Date();

		validate(coupon.getCompanyId(), coupon.getGroupId(), couponName,
				description, limitCatalogs, limitProduct, minOrder, discount);
		int couponUsedCount = 0; 
		couponUsedCount = orderLocalService.getCouponVersionUsedCount(serviceContext.getCompanyId(),
				coupon.getCouponCode(),coupon.getVersion());
		if(couponUsedCount>0){
			coupon.setArchived(true);
			coupon.setActive(false);
			couponPersistence.update(coupon);
			
			String couponCode=coupon.getCouponCode();
			
			double newVersion = MathUtil.format(coupon.getVersion() + 0.1, 1, 1);

			long id = counterLocalService.increment();

			coupon = couponPersistence.create(id);
			coupon.setGroupId(serviceContext.getScopeGroupId());
			coupon.setCompanyId(serviceContext.getCompanyId());
			coupon.setUserId(userId);
			coupon.setVersion(newVersion);
			coupon.setCreateDate(new Date());
			coupon.setArchived(false);
			coupon.setUserName(user.getFullName());
			coupon.setCouponCode(couponCode);
		}
		else {
			coupon.setModifiedDate(now);	
		}
		coupon.setCouponName(couponName);
		coupon.setDescription(description);
		coupon.setCouponLimit(couponLimit);
		coupon.setUserLimit(userLimit);
		coupon.setStartDate(startDate);
		coupon.setEndDate(endDate);
		coupon.setActive(active);
		coupon.setMinOrder(minOrder);
		coupon.setDiscount(discount);
		coupon.setDiscountType(discountType);
		coupon.setDiscountLimit(discountLimit);
		coupon.setLimitCatalogs(limitCatalogs);
		coupon.setLimitProducts(limitProduct);
		coupon.setNeverExpire(neverExpire);

		couponPersistence.update(coupon);

		return coupon;
	}

	public Coupon changeCouponStatus(long couponId, boolean status)
			throws SystemException {
		Coupon coupon = couponPersistence.fetchByPrimaryKey(couponId);
		coupon.setActive(status);
		couponPersistence.update(coupon);
		return coupon;
	}

	@SuppressWarnings("unchecked")
	public List<Coupon> searchCoupon(long groupId, long companyId,
			String couponCode, String couponName, String discountType,
			boolean isAndOperator, boolean active) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
				.forClass(Coupon.class);
		Criterion criterion1 = null;
		Criterion criterion2 = null;
		Criterion criterion3 = null;
		criterion1 = RestrictionsFactoryUtil.like("groupId", groupId);
		criterion1 = RestrictionsFactoryUtil.and(criterion1,
				RestrictionsFactoryUtil.like("companyId", companyId));
		criterion1 = RestrictionsFactoryUtil.and(criterion1,
				RestrictionsFactoryUtil.like("archived", false));
		criterion2 = criterion1;
		if (isAndOperator) {
			criterion2 = RestrictionsFactoryUtil.and(criterion2,
					RestrictionsFactoryUtil.like("active", active));
			if (Validator.isNotNull(couponCode))
				criterion2 = RestrictionsFactoryUtil.and(criterion2,
						RestrictionsFactoryUtil.like("couponCode", couponCode));
			if (Validator.isNotNull(couponName))
				criterion2 = RestrictionsFactoryUtil.and(criterion2,
						RestrictionsFactoryUtil.like("couponName", couponName));
			if (Validator.isNotNull(discountType))
				criterion2 = RestrictionsFactoryUtil.and(criterion2,
						RestrictionsFactoryUtil.like("discountType",
								discountType));
			dynamicQuery.add(criterion2);
		} else {
			criterion3 = RestrictionsFactoryUtil.like("active", active);
			if (Validator.isNotNull(couponCode))
				criterion3 = RestrictionsFactoryUtil.or(criterion3,
						RestrictionsFactoryUtil.like("couponCode", couponCode));
			if (Validator.isNotNull(couponName))
				criterion2 = RestrictionsFactoryUtil.or(criterion3,
						RestrictionsFactoryUtil.like("couponName", couponName));
			if (Validator.isNotNull(discountType))
				criterion3 = RestrictionsFactoryUtil.or(criterion3,
						RestrictionsFactoryUtil.like("discountType",
								discountType));
			criterion3 = RestrictionsFactoryUtil.and(criterion1, criterion3);
			dynamicQuery.add(criterion3);
		}

		return CouponLocalServiceUtil.dynamicQuery(dynamicQuery);
	}

	public Coupon getCoupon(String couponCode) throws PortalException,
			SystemException {

		couponCode = couponCode.trim().toUpperCase();

		return couponPersistence.findByCouponCode(couponCode,false);
	}
}