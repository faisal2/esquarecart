/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchEBSOrderTrackerException;
import com.esquare.ecommerce.model.EBSOrderTracker;
import com.esquare.ecommerce.model.impl.EBSOrderTrackerImpl;
import com.esquare.ecommerce.model.impl.EBSOrderTrackerModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the e b s order tracker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see EBSOrderTrackerPersistence
 * @see EBSOrderTrackerUtil
 * @generated
 */
public class EBSOrderTrackerPersistenceImpl extends BasePersistenceImpl<EBSOrderTracker>
	implements EBSOrderTrackerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EBSOrderTrackerUtil} to access the e b s order tracker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EBSOrderTrackerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerModelImpl.FINDER_CACHE_ENABLED,
			EBSOrderTrackerImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerModelImpl.FINDER_CACHE_ENABLED,
			EBSOrderTrackerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public EBSOrderTrackerPersistenceImpl() {
		setModelClass(EBSOrderTracker.class);
	}

	/**
	 * Caches the e b s order tracker in the entity cache if it is enabled.
	 *
	 * @param ebsOrderTracker the e b s order tracker
	 */
	@Override
	public void cacheResult(EBSOrderTracker ebsOrderTracker) {
		EntityCacheUtil.putResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerImpl.class, ebsOrderTracker.getPrimaryKey(),
			ebsOrderTracker);

		ebsOrderTracker.resetOriginalValues();
	}

	/**
	 * Caches the e b s order trackers in the entity cache if it is enabled.
	 *
	 * @param ebsOrderTrackers the e b s order trackers
	 */
	@Override
	public void cacheResult(List<EBSOrderTracker> ebsOrderTrackers) {
		for (EBSOrderTracker ebsOrderTracker : ebsOrderTrackers) {
			if (EntityCacheUtil.getResult(
						EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
						EBSOrderTrackerImpl.class,
						ebsOrderTracker.getPrimaryKey()) == null) {
				cacheResult(ebsOrderTracker);
			}
			else {
				ebsOrderTracker.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all e b s order trackers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EBSOrderTrackerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EBSOrderTrackerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the e b s order tracker.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EBSOrderTracker ebsOrderTracker) {
		EntityCacheUtil.removeResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerImpl.class, ebsOrderTracker.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EBSOrderTracker> ebsOrderTrackers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EBSOrderTracker ebsOrderTracker : ebsOrderTrackers) {
			EntityCacheUtil.removeResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
				EBSOrderTrackerImpl.class, ebsOrderTracker.getPrimaryKey());
		}
	}

	/**
	 * Creates a new e b s order tracker with the primary key. Does not add the e b s order tracker to the database.
	 *
	 * @param ebsOrderTrackerId the primary key for the new e b s order tracker
	 * @return the new e b s order tracker
	 */
	@Override
	public EBSOrderTracker create(long ebsOrderTrackerId) {
		EBSOrderTracker ebsOrderTracker = new EBSOrderTrackerImpl();

		ebsOrderTracker.setNew(true);
		ebsOrderTracker.setPrimaryKey(ebsOrderTrackerId);

		return ebsOrderTracker;
	}

	/**
	 * Removes the e b s order tracker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ebsOrderTrackerId the primary key of the e b s order tracker
	 * @return the e b s order tracker that was removed
	 * @throws com.esquare.ecommerce.NoSuchEBSOrderTrackerException if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker remove(long ebsOrderTrackerId)
		throws NoSuchEBSOrderTrackerException, SystemException {
		return remove((Serializable)ebsOrderTrackerId);
	}

	/**
	 * Removes the e b s order tracker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the e b s order tracker
	 * @return the e b s order tracker that was removed
	 * @throws com.esquare.ecommerce.NoSuchEBSOrderTrackerException if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker remove(Serializable primaryKey)
		throws NoSuchEBSOrderTrackerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EBSOrderTracker ebsOrderTracker = (EBSOrderTracker)session.get(EBSOrderTrackerImpl.class,
					primaryKey);

			if (ebsOrderTracker == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEBSOrderTrackerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ebsOrderTracker);
		}
		catch (NoSuchEBSOrderTrackerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EBSOrderTracker removeImpl(EBSOrderTracker ebsOrderTracker)
		throws SystemException {
		ebsOrderTracker = toUnwrappedModel(ebsOrderTracker);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ebsOrderTracker)) {
				ebsOrderTracker = (EBSOrderTracker)session.get(EBSOrderTrackerImpl.class,
						ebsOrderTracker.getPrimaryKeyObj());
			}

			if (ebsOrderTracker != null) {
				session.delete(ebsOrderTracker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ebsOrderTracker != null) {
			clearCache(ebsOrderTracker);
		}

		return ebsOrderTracker;
	}

	@Override
	public EBSOrderTracker updateImpl(
		com.esquare.ecommerce.model.EBSOrderTracker ebsOrderTracker)
		throws SystemException {
		ebsOrderTracker = toUnwrappedModel(ebsOrderTracker);

		boolean isNew = ebsOrderTracker.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ebsOrderTracker.isNew()) {
				session.save(ebsOrderTracker);

				ebsOrderTracker.setNew(false);
			}
			else {
				session.merge(ebsOrderTracker);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
			EBSOrderTrackerImpl.class, ebsOrderTracker.getPrimaryKey(),
			ebsOrderTracker);

		return ebsOrderTracker;
	}

	protected EBSOrderTracker toUnwrappedModel(EBSOrderTracker ebsOrderTracker) {
		if (ebsOrderTracker instanceof EBSOrderTrackerImpl) {
			return ebsOrderTracker;
		}

		EBSOrderTrackerImpl ebsOrderTrackerImpl = new EBSOrderTrackerImpl();

		ebsOrderTrackerImpl.setNew(ebsOrderTracker.isNew());
		ebsOrderTrackerImpl.setPrimaryKey(ebsOrderTracker.getPrimaryKey());

		ebsOrderTrackerImpl.setEbsOrderTrackerId(ebsOrderTracker.getEbsOrderTrackerId());
		ebsOrderTrackerImpl.setCompanyId(ebsOrderTracker.getCompanyId());
		ebsOrderTrackerImpl.setOrderId(ebsOrderTracker.getOrderId());
		ebsOrderTrackerImpl.setUserId(ebsOrderTracker.getUserId());
		ebsOrderTrackerImpl.setResponseCode(ebsOrderTracker.getResponseCode());
		ebsOrderTrackerImpl.setResponseMessage(ebsOrderTracker.getResponseMessage());
		ebsOrderTrackerImpl.setCreateDate(ebsOrderTracker.getCreateDate());
		ebsOrderTrackerImpl.setPaymentId(ebsOrderTracker.getPaymentId());
		ebsOrderTrackerImpl.setPaymentMethod(ebsOrderTracker.getPaymentMethod());
		ebsOrderTrackerImpl.setAmount(ebsOrderTracker.getAmount());
		ebsOrderTrackerImpl.setIsFlagged(ebsOrderTracker.getIsFlagged());
		ebsOrderTrackerImpl.setTransactionId(ebsOrderTracker.getTransactionId());

		return ebsOrderTrackerImpl;
	}

	/**
	 * Returns the e b s order tracker with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the e b s order tracker
	 * @return the e b s order tracker
	 * @throws com.esquare.ecommerce.NoSuchEBSOrderTrackerException if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEBSOrderTrackerException, SystemException {
		EBSOrderTracker ebsOrderTracker = fetchByPrimaryKey(primaryKey);

		if (ebsOrderTracker == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEBSOrderTrackerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ebsOrderTracker;
	}

	/**
	 * Returns the e b s order tracker with the primary key or throws a {@link com.esquare.ecommerce.NoSuchEBSOrderTrackerException} if it could not be found.
	 *
	 * @param ebsOrderTrackerId the primary key of the e b s order tracker
	 * @return the e b s order tracker
	 * @throws com.esquare.ecommerce.NoSuchEBSOrderTrackerException if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker findByPrimaryKey(long ebsOrderTrackerId)
		throws NoSuchEBSOrderTrackerException, SystemException {
		return findByPrimaryKey((Serializable)ebsOrderTrackerId);
	}

	/**
	 * Returns the e b s order tracker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the e b s order tracker
	 * @return the e b s order tracker, or <code>null</code> if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EBSOrderTracker ebsOrderTracker = (EBSOrderTracker)EntityCacheUtil.getResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
				EBSOrderTrackerImpl.class, primaryKey);

		if (ebsOrderTracker == _nullEBSOrderTracker) {
			return null;
		}

		if (ebsOrderTracker == null) {
			Session session = null;

			try {
				session = openSession();

				ebsOrderTracker = (EBSOrderTracker)session.get(EBSOrderTrackerImpl.class,
						primaryKey);

				if (ebsOrderTracker != null) {
					cacheResult(ebsOrderTracker);
				}
				else {
					EntityCacheUtil.putResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
						EBSOrderTrackerImpl.class, primaryKey,
						_nullEBSOrderTracker);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EBSOrderTrackerModelImpl.ENTITY_CACHE_ENABLED,
					EBSOrderTrackerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ebsOrderTracker;
	}

	/**
	 * Returns the e b s order tracker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ebsOrderTrackerId the primary key of the e b s order tracker
	 * @return the e b s order tracker, or <code>null</code> if a e b s order tracker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EBSOrderTracker fetchByPrimaryKey(long ebsOrderTrackerId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ebsOrderTrackerId);
	}

	/**
	 * Returns all the e b s order trackers.
	 *
	 * @return the e b s order trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EBSOrderTracker> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the e b s order trackers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EBSOrderTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of e b s order trackers
	 * @param end the upper bound of the range of e b s order trackers (not inclusive)
	 * @return the range of e b s order trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EBSOrderTracker> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the e b s order trackers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EBSOrderTrackerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of e b s order trackers
	 * @param end the upper bound of the range of e b s order trackers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of e b s order trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EBSOrderTracker> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EBSOrderTracker> list = (List<EBSOrderTracker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EBSORDERTRACKER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EBSORDERTRACKER;

				if (pagination) {
					sql = sql.concat(EBSOrderTrackerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EBSOrderTracker>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EBSOrderTracker>(list);
				}
				else {
					list = (List<EBSOrderTracker>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the e b s order trackers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EBSOrderTracker ebsOrderTracker : findAll()) {
			remove(ebsOrderTracker);
		}
	}

	/**
	 * Returns the number of e b s order trackers.
	 *
	 * @return the number of e b s order trackers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EBSORDERTRACKER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the e b s order tracker persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.EBSOrderTracker")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EBSOrderTracker>> listenersList = new ArrayList<ModelListener<EBSOrderTracker>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EBSOrderTracker>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EBSOrderTrackerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EBSORDERTRACKER = "SELECT ebsOrderTracker FROM EBSOrderTracker ebsOrderTracker";
	private static final String _SQL_COUNT_EBSORDERTRACKER = "SELECT COUNT(ebsOrderTracker) FROM EBSOrderTracker ebsOrderTracker";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ebsOrderTracker.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EBSOrderTracker exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EBSOrderTrackerPersistenceImpl.class);
	private static EBSOrderTracker _nullEBSOrderTracker = new EBSOrderTrackerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EBSOrderTracker> toCacheModel() {
				return _nullEBSOrderTrackerCacheModel;
			}
		};

	private static CacheModel<EBSOrderTracker> _nullEBSOrderTrackerCacheModel = new CacheModel<EBSOrderTracker>() {
			@Override
			public EBSOrderTracker toEntityModel() {
				return _nullEBSOrderTracker;
			}
		};
}