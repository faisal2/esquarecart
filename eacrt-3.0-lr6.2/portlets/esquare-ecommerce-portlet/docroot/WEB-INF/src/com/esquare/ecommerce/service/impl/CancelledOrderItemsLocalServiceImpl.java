/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.CancelledOrderItems;
import com.esquare.ecommerce.service.base.CancelledOrderItemsLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the cancelled order items local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.CancelledOrderItemsLocalService}
 * interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CancelledOrderItemsLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.CancelledOrderItemsLocalServiceUtil
 */
public class CancelledOrderItemsLocalServiceImpl extends
		CancelledOrderItemsLocalServiceBaseImpl {
	public CancelledOrderItems updateCancelledOrderItems(long orderId,
			long orderItemId, double price, int quantity,
			ServiceContext serviceContext) throws SystemException {

		CancelledOrderItems cancelledOrderItems = cancelledOrderItemsPersistence
				.fetchByPrimaryKey(orderItemId);
		if (Validator.isNull(cancelledOrderItems))
			cancelledOrderItems = cancelledOrderItemsPersistence
					.create(orderItemId);
		cancelledOrderItems.setCompanyId(serviceContext.getCompanyId());
		cancelledOrderItems.setUserId(serviceContext.getUserId());
		cancelledOrderItems.setPrice(price);
		cancelledOrderItems.setOrderId(orderId);
		cancelledOrderItems.setQuantity(cancelledOrderItems.getQuantity()
				+ quantity);
		cancelledOrderItems.setCancelledDate(new Date());

		cancelledOrderItemsPersistence.update(cancelledOrderItems);
		return cancelledOrderItems;
	}

	public List<CancelledOrderItems> getByCompanyId(long companyId)
			throws SystemException {
		return cancelledOrderItemsPersistence.findByC(companyId);
	}
}