package com.esquare.ecommerce.service.persistence;

import java.util.List;

import com.esquare.ecommerce.model.EcartPackageBilling;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.impl.EcartPackageBillingImpl;
import com.esquare.ecommerce.model.impl.InstanceImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class EcartPackageBillingFinderImpl extends BasePersistenceImpl
		implements EcartPackageBillingFinder {
	public List<EcartPackageBilling> getPackageBillingList(int month, int year,
			long companyId) {
		Session session = null;
		SQLQuery query = null;
		String customQuery = GET_PACKAGE_BILLING;
		try {
			String sql = CustomSQLUtil.get(customQuery);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addEntity("EcartPackageBilling",
					EcartPackageBillingImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(month);
			qPos.add(year);
			qPos.add(companyId);
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public List<Integer> getPackageBillingMonth(int year, long companyId) {
		Session session = null;
		SQLQuery query = null;
		String customQuery = GET_PACKAGE_BILLING_MONTH;
		try {
			String sql = CustomSQLUtil.get(customQuery);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("monthValues", Type.INTEGER);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(year);
			qPos.add(companyId);
		} catch (Exception e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public List<Instance> getPackageBillingList(String notInPackageType) {
		Session session = null;
		SQLQuery query = null;
		String customQuery = GET_PACKAGE_BILLING_LIST;

		try {
			String sql = CustomSQLUtil.get(customQuery);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addEntity("Instance", InstanceImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(notInPackageType);
		} catch (ORMException e) {
			_log.info(e.getClass());
		}
		return (List) query.list();
	}

	public static String GET_PACKAGE_BILLING = "getPackageBilling";
	public static String GET_PACKAGE_BILLING_LIST = "getPackageBillingList";
	public static String GET_PACKAGE_BILLING_MONTH = "getPackageBillingMonth";
	private static Log _log = LogFactoryUtil
			.getLog(EcartPackageBillingFinderImpl.class);
}