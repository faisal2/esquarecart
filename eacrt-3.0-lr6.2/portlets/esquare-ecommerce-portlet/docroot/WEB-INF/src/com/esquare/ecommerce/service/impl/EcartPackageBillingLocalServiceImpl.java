/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.EcartPackageBilling;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.service.base.EcartPackageBillingLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.EcartPackageBillingFinderUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * The implementation of the ecart package billing local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.EcartPackageBillingLocalService}
 * interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.EcartPackageBillingLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.EcartPackageBillingLocalServiceUtil
 */
public class EcartPackageBillingLocalServiceImpl extends
		EcartPackageBillingLocalServiceBaseImpl {
	public EcartPackageBilling addEcartPackageBilling(long companyId,
			long userId, Date billingDate, String packageType, double amount)
			throws SystemException {

		long billId = counterLocalService.increment();

		EcartPackageBilling ecartPackageBilling = ecartPackageBillingPersistence
				.create(billId);

		ecartPackageBilling.setCompanyId(companyId);
		ecartPackageBilling.setUserId(userId);
		ecartPackageBilling.setBillingDate(billingDate);
		ecartPackageBilling.setPackageType(packageType);
		ecartPackageBilling.setAmount(amount);

		ecartPackageBillingPersistence.update(ecartPackageBilling);

		return ecartPackageBilling;
	}

	public EcartPackageBilling addTranscationId(long billId, long transactionId) {

		EcartPackageBilling ecartPackageBilling = null;
		try {
			ecartPackageBilling = ecartPackageBillingPersistence
					.fetchByPrimaryKey(billId);
			ecartPackageBilling.setTransactionId(transactionId);
			ecartPackageBillingPersistence.update(ecartPackageBilling);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}

		return ecartPackageBilling;
	}

	public List<EcartPackageBilling> getPackageBillingList(int month, int year,
			long companyId) {
		return EcartPackageBillingFinderUtil.getPackageBillingList(month, year,
				companyId);
	}

	public List<EcartPackageBilling> findByCompanyId(long companyId)
			throws SystemException {
		return ecartPackageBillingPersistence.findByCompanyId(companyId);
	}

	public List<EcartPackageBilling> findByCompanyTransactionId(long companyId,
			long transactionId) throws SystemException {
		return ecartPackageBillingPersistence.findByC_T_Id(companyId,
				transactionId);
	}

	public List<Integer> getPackageBillingMonth(int year, long companyId) {
		return EcartPackageBillingFinderUtil.getPackageBillingMonth(year,
				companyId);
	}

	public List<Instance> getPackageBillingList(
			String notInPackageType) {
		return EcartPackageBillingFinderUtil.getPackageBillingList(notInPackageType);
	}
	private static Log _log = LogFactoryUtil.getLog(EcartPackageBillingLocalServiceImpl.class);
}