/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.PaymentConfiguration;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PaymentConfiguration in entity cache.
 *
 * @author Esquare
 * @see PaymentConfiguration
 * @generated
 */
public class PaymentConfigurationCacheModel implements CacheModel<PaymentConfiguration>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{id=");
		sb.append(id);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", paymentGatewayType=");
		sb.append(paymentGatewayType);
		sb.append(", merchantId=");
		sb.append(merchantId);
		sb.append(", secretKey=");
		sb.append(secretKey);
		sb.append(", status=");
		sb.append(status);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PaymentConfiguration toEntityModel() {
		PaymentConfigurationImpl paymentConfigurationImpl = new PaymentConfigurationImpl();

		paymentConfigurationImpl.setId(id);
		paymentConfigurationImpl.setCompanyId(companyId);
		paymentConfigurationImpl.setUserId(userId);

		if (paymentGatewayType == null) {
			paymentConfigurationImpl.setPaymentGatewayType(StringPool.BLANK);
		}
		else {
			paymentConfigurationImpl.setPaymentGatewayType(paymentGatewayType);
		}

		if (merchantId == null) {
			paymentConfigurationImpl.setMerchantId(StringPool.BLANK);
		}
		else {
			paymentConfigurationImpl.setMerchantId(merchantId);
		}

		if (secretKey == null) {
			paymentConfigurationImpl.setSecretKey(StringPool.BLANK);
		}
		else {
			paymentConfigurationImpl.setSecretKey(secretKey);
		}

		paymentConfigurationImpl.setStatus(status);

		if (createDate == Long.MIN_VALUE) {
			paymentConfigurationImpl.setCreateDate(null);
		}
		else {
			paymentConfigurationImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			paymentConfigurationImpl.setModifiedDate(null);
		}
		else {
			paymentConfigurationImpl.setModifiedDate(new Date(modifiedDate));
		}

		paymentConfigurationImpl.resetOriginalValues();

		return paymentConfigurationImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		paymentGatewayType = objectInput.readUTF();
		merchantId = objectInput.readUTF();
		secretKey = objectInput.readUTF();
		status = objectInput.readBoolean();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (paymentGatewayType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paymentGatewayType);
		}

		if (merchantId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(merchantId);
		}

		if (secretKey == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(secretKey);
		}

		objectOutput.writeBoolean(status);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
	}

	public long id;
	public long companyId;
	public long userId;
	public String paymentGatewayType;
	public String merchantId;
	public String secretKey;
	public boolean status;
	public long createDate;
	public long modifiedDate;
}