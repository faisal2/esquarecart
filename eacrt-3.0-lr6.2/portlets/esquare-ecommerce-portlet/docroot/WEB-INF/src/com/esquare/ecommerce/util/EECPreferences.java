///**
// * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
// *
// * This library is free software; you can redistribute it and/or modify it under
// * the terms of the GNU Lesser General Public License as published by the Free
// * Software Foundation; either version 2.1 of the License, or (at your option)
// * any later version.
// *
// * This library is distributed in the hope that it will be useful, but WITHOUT
// * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
// * details.
// */
//
//package com.esquare.ecommerce.util;
//
//import java.util.Currency;
//import java.util.Locale;
//import java.util.Set;
//import java.util.TreeSet;
//
//import javax.portlet.PortletPreferences;
//
//import com.liferay.portal.kernel.exception.SystemException;
//import com.liferay.portal.kernel.util.GetterUtil;
//import com.liferay.portal.kernel.util.StringPool;
//import com.liferay.portal.kernel.util.Validator;
//import com.liferay.portal.service.PortletPreferencesLocalServiceUtil;
//import com.liferay.portal.util.PortalUtil;
//import com.liferay.portal.util.PortletKeys;
//import com.liferay.util.ContentUtil;
//
///**
// * @author Mohammad azharuddin & Brian Wing Shun Chan
// */
//public class EECPreferences {
//
//
//
//	public static final String[] CURRENCY_IDS;
//
//	static {
//		String[] ids = null;
//
//		try {
//			Set<String> set = new TreeSet<String>();
//
//			Locale[] locales = Locale.getAvailableLocales();
//
//			for (int i = 0; i < locales.length; i++) {
//				Locale locale = locales[i];
//
//				if (locale.getCountry().length() == 2) {
//					Currency currency = Currency.getInstance(locale);
//
//					String currencyId = currency.getCurrencyCode();
//
//					set.add(currencyId);
//				}
//			}
//
//			ids = set.toArray(new String[set.size()]);
//		}
//		catch (Exception e) {
//			ids = new String[] {"USD", "CAD", "EUR", "GBP", "JPY"};
//		}
//		finally {
//			CURRENCY_IDS = ids;
//		}
//	}
//
//
//
//	public static EECPreferences getInstance(long companyId, long groupId)
//		throws SystemException {
//
//		return new EECPreferences(companyId, groupId);
//	}
//
////	public String[][] getAlternativeShipping() {
////		String value = _portletPreferences.getValue(
////			"alternativeShipping", null);
////
////		if (value == null) {
////			return new String[0][0];
////		}
////		else {
////			String[] array = StringUtil.split(
////				"alternativeShipping", "[$_ARRAY_$]");
////
////			String[][] alternativeShipping = new String[array.length][0];
////
////			for (int i = 0; i < array.length; i++) {
////				alternativeShipping[i] = StringUtil.split(array[i]);
////			}
////
////			return alternativeShipping;
////		}
////	}
////
////	public String getAlternativeShippingName(int altShipping) {
////		String altShippingName = StringPool.BLANK;
////
////		try {
////			altShippingName = getAlternativeShipping()[0][altShipping];
////		}
////		catch (Exception e) {
////		}
////
////		return altShippingName;
////	}
//
//	
//
//	public String getCurrencyId() {
//		return _portletPreferences.getValue("currencyId", "USD");
//	}
//
//	public String getEmailFromAddress(long companyId) throws SystemException {
//		return PortalUtil.getEmailFromAddress(
//			_portletPreferences, companyId,PortletPropsValues.EMAIL_FROM_ADDRESS);
//	}
//
//	public String getEmailFromName(long companyId) throws SystemException {
//		return PortalUtil.getEmailFromAddress(
//			_portletPreferences, companyId,PortletPropsValues.EMAIL_FROM_NAME);
//	}
//
//	public String getEmailOrderConfirmationBody() {
//		String emailOrderConfirmationBody = _portletPreferences.getValue(
//			"emailOrderConfirmationBody", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderConfirmationBody)) {
//			return emailOrderConfirmationBody;
//		}
//		else {
//			return ContentUtil.get(PortletPropsValues.EMAIL_ORDER_CONFIRMATION_BODY);
//		}
//	}
//
//	public boolean getEmailOrderConfirmationEnabled() {
//		String emailOrderConfirmationEnabled = _portletPreferences.getValue(
//			"emailOrderConfirmationEnabled", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderConfirmationEnabled)) {
//			return GetterUtil.getBoolean(emailOrderConfirmationEnabled);
//		}
//		else {
//			return PortletPropsValues.EMAIL_ORDER_CONFIRMATION_ENABLED;
//		}
//	}
//
//	public String getEmailOrderConfirmationSubject() {
//		String emailOrderConfirmationSubject = _portletPreferences.getValue(
//			"emailOrderConfirmationSubject", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderConfirmationSubject)) {
//			return emailOrderConfirmationSubject;
//		}
//		else {
//			return ContentUtil.get(PortletPropsValues.EMAIL_ORDER_CONFIRMATION_SUBJECT);
//		}
//	}
//
//	public String getEmailOrderShippingBody() {
//		String emailOrderShippingBody = _portletPreferences.getValue(
//			"emailOrderShippingBody", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderShippingBody)) {
//			return emailOrderShippingBody;
//		}
//		else {
//			return ContentUtil.get(PortletPropsValues.EMAIL_ORDER_SHIPPING_BODY);
//		}
//	}
//
//	public boolean getEmailOrderShippingEnabled() {
//		String emailOrderShippingEnabled = _portletPreferences.getValue(
//			"emailOrderShippingEnabled", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderShippingEnabled)) {
//			return GetterUtil.getBoolean(emailOrderShippingEnabled);
//		}
//		else {
//			return PortletPropsValues.EMAIL_SHIPPING_CONFIRMATION_ENABLED;
//		}
//	}
//
//	public String getEmailOrderShippingSubject() {
//		String emailOrderShippingSubject = _portletPreferences.getValue(
//			"emailOrderShippingSubject", StringPool.BLANK);
//
//		if (Validator.isNotNull(emailOrderShippingSubject)) {
//			return emailOrderShippingSubject;
//		}
//		else {
//			return ContentUtil.get(PortletPropsValues.EMAIL_ORDER_SHIPPING_SUBJECT);
//		}
//	}
//
////	public String[] getInsurance() {
////		String value = _portletPreferences.getValue("insurance", null);
////
////		if (value == null) {
////			return new String[5];
////		}
////		else {
////			return StringUtil.split(value);
////		}
////	}
////
////	public String getInsuranceFormula() {
////		return _portletPreferences.getValue("insuranceFormula", "flat");
////	}
////
////	public double getMinOrder() {
////		return GetterUtil.getDouble(_portletPreferences.getValue(
////			"minOrder", StringPool.BLANK));
////	}
////
////	public String getPayPalEmailAddress() {
////		return _portletPreferences.getValue(
////			"paypalEmailAddress", StringPool.BLANK);
////	}
////
////	public String[] getShipping() {
////		String value = _portletPreferences.getValue("shipping", null);
////
////		if (value == null) {
////			return new String[5];
////		}
////		else {
////			return StringUtil.split(value);
////		}
////	}
////
////	public String getShippingFormula() {
////		return _portletPreferences.getValue("shippingFormula", "flat");
////	}
////
////	public double getTaxRate() {
////		return GetterUtil.getDouble(
////			_portletPreferences.getValue("taxRate", StringPool.BLANK));
////	}
////
////	public String getTaxState() {
////		return _portletPreferences.getValue("taxState", "CA");
////	}
////
////	public void setAlternativeShipping(String[][] alternativeShipping)
////		throws ReadOnlyException {
////
////		if (alternativeShipping.length == 0) {
////			_portletPreferences.setValue(
////				"alternativeShipping", StringPool.BLANK);
////		}
////
////		StringBundler sb = new StringBundler(
////			alternativeShipping.length * 2 - 1);
////
////		for (int i = 0; i < alternativeShipping.length; i++) {
////			sb.append(StringUtil.merge(alternativeShipping[i]));
////
////			if ((i + 1) < alternativeShipping.length) {
////				sb.append("[$_ARRAY_$]");
////			}
////		}
////
////		_portletPreferences.setValue("alternativeShipping", sb.toString());
////	}
////
////	public void setCcTypes(String[] ccTypes) throws ReadOnlyException {
////		if (ccTypes.length == 0) {
////			_portletPreferences.setValue("ccTypes", CC_NONE);
////		}
////		else {
////			_portletPreferences.setValue("ccTypes", StringUtil.merge(ccTypes));
////		}
////	}
////
////	public void setCurrencyId(String currencyId) throws ReadOnlyException {
////		_portletPreferences.setValue("currencyId", currencyId);
////	}
////
////	public void setEmailFromAddress(String emailFromAddress)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue("emailFromAddress", emailFromAddress);
////	}
////
////	public void setEmailFromName(String emailFromName)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue("emailFromName", emailFromName);
////	}
////
////	public void setEmailOrderConfirmationBody(String emailOrderConfirmationBody)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderConfirmationBody", emailOrderConfirmationBody);
////	}
////
////	public void setEmailOrderConfirmationEnabled(
////			boolean emailOrderConfirmationEnabled)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderConfirmationEnabled",
////			String.valueOf(emailOrderConfirmationEnabled));
////	}
////
////	public void setEmailOrderConfirmationSubject(
////			String emailOrderConfirmationSubject)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderConfirmationSubject", emailOrderConfirmationSubject);
////	}
////
////	public void setEmailOrderShippingBody(String emailOrderShippingBody)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderShippingBody", emailOrderShippingBody);
////	}
////
////	public void setEmailOrderShippingEnabled(boolean emailOrderShippingEnabled)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderShippingEnabled",
////			String.valueOf(emailOrderShippingEnabled));
////	}
////
////	public void setEmailOrderShippingSubject(String emailOrderShippingSubject)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue(
////			"emailOrderShippingSubject", emailOrderShippingSubject);
////	}
////
////	public void setInsurance(String[] insurance) throws ReadOnlyException {
////		_portletPreferences.setValue("insurance", StringUtil.merge(insurance));
////	}
////
////	public void setInsuranceFormula(String insuranceFormula)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue("insuranceFormula", insuranceFormula);
////	}
////
////	public void setMinOrder(double minOrder) throws ReadOnlyException {
////		_portletPreferences.setValue("minOrder", String.valueOf(minOrder));
////	}
////
////	public void setPayPalEmailAddress(String payPalEmailAddress)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue("paypalEmailAddress", payPalEmailAddress);
////	}
////
////	public void setShipping(String[] shipping) throws ReadOnlyException {
////		_portletPreferences.setValue("shipping", StringUtil.merge(shipping));
////	}
////
////	public void setShippingFormula(String shippingFormula)
////		throws ReadOnlyException {
////
////		_portletPreferences.setValue("shippingFormula", shippingFormula);
////	}
////
////	public void setTaxRate(double taxRate) throws ReadOnlyException {
////		_portletPreferences.setValue("taxRate", String.valueOf(taxRate));
////	}
////
////	public void setTaxState(String taxState) throws ReadOnlyException {
////		_portletPreferences.setValue("taxState", taxState);
////	}
////
////	public void store() throws IOException, ValidatorException {
////		_portletPreferences.store();
////	}
////
////	public boolean useAlternativeShipping() {
////		String[][] alternativeShipping = getAlternativeShipping();
////
////		try {
////			for (int i = 0; i < 10; i++) {
////				if (Validator.isNotNull(alternativeShipping[0][i]) &&
////					Validator.isNotNull(alternativeShipping[1][i])) {
////
////					return true;
////				}
////			}
////		}
////		catch (Exception e) {
////		}
////
////		return false;
////	}
////
////	public boolean usePayPal() {
////		return Validator.isNotNull(getPayPalEmailAddress());
////	}
//
//	protected EECPreferences(long companyId, long groupId)
//		throws SystemException {
//
//		long ownerId = groupId;
//		int ownerType = PortletKeys.PREFS_OWNER_TYPE_GROUP;
//		long plid = PortletKeys.PREFS_PLID_SHARED;
//		
//		//pass the portlet id
//		String portletId = PortletKeys.SHOPPING;
//
//		_portletPreferences = PortletPreferencesLocalServiceUtil.getPreferences(
//			companyId, ownerId, ownerType, plid, portletId);
//	}
//
//	private PortletPreferences _portletPreferences;
//
//}