/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchTemplateManagerException;
import com.esquare.ecommerce.model.TemplateManager;
import com.esquare.ecommerce.model.impl.TemplateManagerImpl;
import com.esquare.ecommerce.model.impl.TemplateManagerModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the template manager service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see TemplateManagerPersistence
 * @see TemplateManagerUtil
 * @generated
 */
public class TemplateManagerPersistenceImpl extends BasePersistenceImpl<TemplateManager>
	implements TemplateManagerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TemplateManagerUtil} to access the template manager persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TemplateManagerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED,
			TemplateManagerImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED,
			TemplateManagerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PACKAGETYPE =
		new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED,
			TemplateManagerImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByPackageType",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PACKAGETYPE =
		new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED,
			TemplateManagerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPackageType",
			new String[] { String.class.getName() },
			TemplateManagerModelImpl.PACKAGETYPE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PACKAGETYPE = new FinderPath(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPackageType",
			new String[] { String.class.getName() });

	/**
	 * Returns all the template managers where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @return the matching template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findByPackageType(String packageType)
		throws SystemException {
		return findByPackageType(packageType, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the template managers where packageType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.TemplateManagerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param packageType the package type
	 * @param start the lower bound of the range of template managers
	 * @param end the upper bound of the range of template managers (not inclusive)
	 * @return the range of matching template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findByPackageType(String packageType,
		int start, int end) throws SystemException {
		return findByPackageType(packageType, start, end, null);
	}

	/**
	 * Returns an ordered range of all the template managers where packageType = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.TemplateManagerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param packageType the package type
	 * @param start the lower bound of the range of template managers
	 * @param end the upper bound of the range of template managers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findByPackageType(String packageType,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PACKAGETYPE;
			finderArgs = new Object[] { packageType };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PACKAGETYPE;
			finderArgs = new Object[] { packageType, start, end, orderByComparator };
		}

		List<TemplateManager> list = (List<TemplateManager>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (TemplateManager templateManager : list) {
				if (!Validator.equals(packageType,
							templateManager.getPackageType())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TEMPLATEMANAGER_WHERE);

			boolean bindPackageType = false;

			if (packageType == null) {
				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_1);
			}
			else if (packageType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_3);
			}
			else {
				bindPackageType = true;

				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TemplateManagerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPackageType) {
					qPos.add(packageType);
				}

				if (!pagination) {
					list = (List<TemplateManager>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TemplateManager>(list);
				}
				else {
					list = (List<TemplateManager>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first template manager in the ordered set where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching template manager
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a matching template manager could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager findByPackageType_First(String packageType,
		OrderByComparator orderByComparator)
		throws NoSuchTemplateManagerException, SystemException {
		TemplateManager templateManager = fetchByPackageType_First(packageType,
				orderByComparator);

		if (templateManager != null) {
			return templateManager;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("packageType=");
		msg.append(packageType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTemplateManagerException(msg.toString());
	}

	/**
	 * Returns the first template manager in the ordered set where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching template manager, or <code>null</code> if a matching template manager could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager fetchByPackageType_First(String packageType,
		OrderByComparator orderByComparator) throws SystemException {
		List<TemplateManager> list = findByPackageType(packageType, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last template manager in the ordered set where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching template manager
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a matching template manager could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager findByPackageType_Last(String packageType,
		OrderByComparator orderByComparator)
		throws NoSuchTemplateManagerException, SystemException {
		TemplateManager templateManager = fetchByPackageType_Last(packageType,
				orderByComparator);

		if (templateManager != null) {
			return templateManager;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("packageType=");
		msg.append(packageType);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTemplateManagerException(msg.toString());
	}

	/**
	 * Returns the last template manager in the ordered set where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching template manager, or <code>null</code> if a matching template manager could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager fetchByPackageType_Last(String packageType,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByPackageType(packageType);

		if (count == 0) {
			return null;
		}

		List<TemplateManager> list = findByPackageType(packageType, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the template managers before and after the current template manager in the ordered set where packageType = &#63;.
	 *
	 * @param recId the primary key of the current template manager
	 * @param packageType the package type
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next template manager
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager[] findByPackageType_PrevAndNext(long recId,
		String packageType, OrderByComparator orderByComparator)
		throws NoSuchTemplateManagerException, SystemException {
		TemplateManager templateManager = findByPrimaryKey(recId);

		Session session = null;

		try {
			session = openSession();

			TemplateManager[] array = new TemplateManagerImpl[3];

			array[0] = getByPackageType_PrevAndNext(session, templateManager,
					packageType, orderByComparator, true);

			array[1] = templateManager;

			array[2] = getByPackageType_PrevAndNext(session, templateManager,
					packageType, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TemplateManager getByPackageType_PrevAndNext(Session session,
		TemplateManager templateManager, String packageType,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TEMPLATEMANAGER_WHERE);

		boolean bindPackageType = false;

		if (packageType == null) {
			query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_1);
		}
		else if (packageType.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_3);
		}
		else {
			bindPackageType = true;

			query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TemplateManagerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindPackageType) {
			qPos.add(packageType);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(templateManager);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TemplateManager> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the template managers where packageType = &#63; from the database.
	 *
	 * @param packageType the package type
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByPackageType(String packageType)
		throws SystemException {
		for (TemplateManager templateManager : findByPackageType(packageType,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(templateManager);
		}
	}

	/**
	 * Returns the number of template managers where packageType = &#63;.
	 *
	 * @param packageType the package type
	 * @return the number of matching template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByPackageType(String packageType) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PACKAGETYPE;

		Object[] finderArgs = new Object[] { packageType };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TEMPLATEMANAGER_WHERE);

			boolean bindPackageType = false;

			if (packageType == null) {
				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_1);
			}
			else if (packageType.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_3);
			}
			else {
				bindPackageType = true;

				query.append(_FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindPackageType) {
					qPos.add(packageType);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_1 = "templateManager.packageType IS NULL";
	private static final String _FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_2 = "templateManager.packageType = ?";
	private static final String _FINDER_COLUMN_PACKAGETYPE_PACKAGETYPE_3 = "(templateManager.packageType IS NULL OR templateManager.packageType = '')";

	public TemplateManagerPersistenceImpl() {
		setModelClass(TemplateManager.class);
	}

	/**
	 * Caches the template manager in the entity cache if it is enabled.
	 *
	 * @param templateManager the template manager
	 */
	@Override
	public void cacheResult(TemplateManager templateManager) {
		EntityCacheUtil.putResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerImpl.class, templateManager.getPrimaryKey(),
			templateManager);

		templateManager.resetOriginalValues();
	}

	/**
	 * Caches the template managers in the entity cache if it is enabled.
	 *
	 * @param templateManagers the template managers
	 */
	@Override
	public void cacheResult(List<TemplateManager> templateManagers) {
		for (TemplateManager templateManager : templateManagers) {
			if (EntityCacheUtil.getResult(
						TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
						TemplateManagerImpl.class,
						templateManager.getPrimaryKey()) == null) {
				cacheResult(templateManager);
			}
			else {
				templateManager.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all template managers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(TemplateManagerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(TemplateManagerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the template manager.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(TemplateManager templateManager) {
		EntityCacheUtil.removeResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerImpl.class, templateManager.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<TemplateManager> templateManagers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TemplateManager templateManager : templateManagers) {
			EntityCacheUtil.removeResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
				TemplateManagerImpl.class, templateManager.getPrimaryKey());
		}
	}

	/**
	 * Creates a new template manager with the primary key. Does not add the template manager to the database.
	 *
	 * @param recId the primary key for the new template manager
	 * @return the new template manager
	 */
	@Override
	public TemplateManager create(long recId) {
		TemplateManager templateManager = new TemplateManagerImpl();

		templateManager.setNew(true);
		templateManager.setPrimaryKey(recId);

		return templateManager;
	}

	/**
	 * Removes the template manager with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param recId the primary key of the template manager
	 * @return the template manager that was removed
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager remove(long recId)
		throws NoSuchTemplateManagerException, SystemException {
		return remove((Serializable)recId);
	}

	/**
	 * Removes the template manager with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the template manager
	 * @return the template manager that was removed
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager remove(Serializable primaryKey)
		throws NoSuchTemplateManagerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			TemplateManager templateManager = (TemplateManager)session.get(TemplateManagerImpl.class,
					primaryKey);

			if (templateManager == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTemplateManagerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(templateManager);
		}
		catch (NoSuchTemplateManagerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TemplateManager removeImpl(TemplateManager templateManager)
		throws SystemException {
		templateManager = toUnwrappedModel(templateManager);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(templateManager)) {
				templateManager = (TemplateManager)session.get(TemplateManagerImpl.class,
						templateManager.getPrimaryKeyObj());
			}

			if (templateManager != null) {
				session.delete(templateManager);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (templateManager != null) {
			clearCache(templateManager);
		}

		return templateManager;
	}

	@Override
	public TemplateManager updateImpl(
		com.esquare.ecommerce.model.TemplateManager templateManager)
		throws SystemException {
		templateManager = toUnwrappedModel(templateManager);

		boolean isNew = templateManager.isNew();

		TemplateManagerModelImpl templateManagerModelImpl = (TemplateManagerModelImpl)templateManager;

		Session session = null;

		try {
			session = openSession();

			if (templateManager.isNew()) {
				session.save(templateManager);

				templateManager.setNew(false);
			}
			else {
				session.merge(templateManager);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !TemplateManagerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((templateManagerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PACKAGETYPE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						templateManagerModelImpl.getOriginalPackageType()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PACKAGETYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PACKAGETYPE,
					args);

				args = new Object[] { templateManagerModelImpl.getPackageType() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PACKAGETYPE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PACKAGETYPE,
					args);
			}
		}

		EntityCacheUtil.putResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
			TemplateManagerImpl.class, templateManager.getPrimaryKey(),
			templateManager);

		return templateManager;
	}

	protected TemplateManager toUnwrappedModel(TemplateManager templateManager) {
		if (templateManager instanceof TemplateManagerImpl) {
			return templateManager;
		}

		TemplateManagerImpl templateManagerImpl = new TemplateManagerImpl();

		templateManagerImpl.setNew(templateManager.isNew());
		templateManagerImpl.setPrimaryKey(templateManager.getPrimaryKey());

		templateManagerImpl.setRecId(templateManager.getRecId());
		templateManagerImpl.setTitle(templateManager.getTitle());
		templateManagerImpl.setCssId(templateManager.getCssId());
		templateManagerImpl.setLarName(templateManager.getLarName());
		templateManagerImpl.setTemplateDesc(templateManager.getTemplateDesc());
		templateManagerImpl.setFolderId(templateManager.getFolderId());
		templateManagerImpl.setPackageType(templateManager.getPackageType());
		templateManagerImpl.setModifiedDate(templateManager.getModifiedDate());

		return templateManagerImpl;
	}

	/**
	 * Returns the template manager with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the template manager
	 * @return the template manager
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager findByPrimaryKey(Serializable primaryKey)
		throws NoSuchTemplateManagerException, SystemException {
		TemplateManager templateManager = fetchByPrimaryKey(primaryKey);

		if (templateManager == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTemplateManagerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return templateManager;
	}

	/**
	 * Returns the template manager with the primary key or throws a {@link com.esquare.ecommerce.NoSuchTemplateManagerException} if it could not be found.
	 *
	 * @param recId the primary key of the template manager
	 * @return the template manager
	 * @throws com.esquare.ecommerce.NoSuchTemplateManagerException if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager findByPrimaryKey(long recId)
		throws NoSuchTemplateManagerException, SystemException {
		return findByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns the template manager with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the template manager
	 * @return the template manager, or <code>null</code> if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		TemplateManager templateManager = (TemplateManager)EntityCacheUtil.getResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
				TemplateManagerImpl.class, primaryKey);

		if (templateManager == _nullTemplateManager) {
			return null;
		}

		if (templateManager == null) {
			Session session = null;

			try {
				session = openSession();

				templateManager = (TemplateManager)session.get(TemplateManagerImpl.class,
						primaryKey);

				if (templateManager != null) {
					cacheResult(templateManager);
				}
				else {
					EntityCacheUtil.putResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
						TemplateManagerImpl.class, primaryKey,
						_nullTemplateManager);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(TemplateManagerModelImpl.ENTITY_CACHE_ENABLED,
					TemplateManagerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return templateManager;
	}

	/**
	 * Returns the template manager with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param recId the primary key of the template manager
	 * @return the template manager, or <code>null</code> if a template manager with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public TemplateManager fetchByPrimaryKey(long recId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)recId);
	}

	/**
	 * Returns all the template managers.
	 *
	 * @return the template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the template managers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.TemplateManagerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of template managers
	 * @param end the upper bound of the range of template managers (not inclusive)
	 * @return the range of template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the template managers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.TemplateManagerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of template managers
	 * @param end the upper bound of the range of template managers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<TemplateManager> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TemplateManager> list = (List<TemplateManager>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_TEMPLATEMANAGER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TEMPLATEMANAGER;

				if (pagination) {
					sql = sql.concat(TemplateManagerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TemplateManager>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<TemplateManager>(list);
				}
				else {
					list = (List<TemplateManager>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the template managers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (TemplateManager templateManager : findAll()) {
			remove(templateManager);
		}
	}

	/**
	 * Returns the number of template managers.
	 *
	 * @return the number of template managers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TEMPLATEMANAGER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the template manager persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.TemplateManager")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<TemplateManager>> listenersList = new ArrayList<ModelListener<TemplateManager>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<TemplateManager>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(TemplateManagerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_TEMPLATEMANAGER = "SELECT templateManager FROM TemplateManager templateManager";
	private static final String _SQL_SELECT_TEMPLATEMANAGER_WHERE = "SELECT templateManager FROM TemplateManager templateManager WHERE ";
	private static final String _SQL_COUNT_TEMPLATEMANAGER = "SELECT COUNT(templateManager) FROM TemplateManager templateManager";
	private static final String _SQL_COUNT_TEMPLATEMANAGER_WHERE = "SELECT COUNT(templateManager) FROM TemplateManager templateManager WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "templateManager.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TemplateManager exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TemplateManager exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(TemplateManagerPersistenceImpl.class);
	private static TemplateManager _nullTemplateManager = new TemplateManagerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<TemplateManager> toCacheModel() {
				return _nullTemplateManagerCacheModel;
			}
		};

	private static CacheModel<TemplateManager> _nullTemplateManagerCacheModel = new CacheModel<TemplateManager>() {
			@Override
			public TemplateManager toEntityModel() {
				return _nullTemplateManager;
			}
		};
}