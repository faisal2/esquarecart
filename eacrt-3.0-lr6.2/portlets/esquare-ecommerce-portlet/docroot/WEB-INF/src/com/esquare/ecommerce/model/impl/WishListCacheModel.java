/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.WishList;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing WishList in entity cache.
 *
 * @author Esquare
 * @see WishList
 * @generated
 */
public class WishListCacheModel implements CacheModel<WishList>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{wishListId=");
		sb.append(wishListId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", productInventoryIds=");
		sb.append(productInventoryIds);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public WishList toEntityModel() {
		WishListImpl wishListImpl = new WishListImpl();

		wishListImpl.setWishListId(wishListId);
		wishListImpl.setGroupId(groupId);
		wishListImpl.setCompanyId(companyId);
		wishListImpl.setUserId(userId);

		if (userName == null) {
			wishListImpl.setUserName(StringPool.BLANK);
		}
		else {
			wishListImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			wishListImpl.setCreateDate(null);
		}
		else {
			wishListImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			wishListImpl.setModifiedDate(null);
		}
		else {
			wishListImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (productInventoryIds == null) {
			wishListImpl.setProductInventoryIds(StringPool.BLANK);
		}
		else {
			wishListImpl.setProductInventoryIds(productInventoryIds);
		}

		wishListImpl.resetOriginalValues();

		return wishListImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		wishListId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		productInventoryIds = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(wishListId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (productInventoryIds == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(productInventoryIds);
		}
	}

	public long wishListId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String productInventoryIds;
}