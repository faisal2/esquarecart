/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;

import com.esquare.ecommerce.model.EBSOrderTracker;
import com.esquare.ecommerce.service.base.EBSOrderTrackerLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * The implementation of the e b s order tracker local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.EBSOrderTrackerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.EBSOrderTrackerLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.EBSOrderTrackerLocalServiceUtil
 */
public class EBSOrderTrackerLocalServiceImpl
	extends EBSOrderTrackerLocalServiceBaseImpl {
	
	public void addEBSOrderTracker(long companyId,long orderId, long userId,long responseCode,
			String responseMessage, Date createDate, long paymentId,
			long paymentMethod, String amount, String isFlagged,
			long transactionId)  {
		long ebsOrderTrackerId = 0;
		try {
			ebsOrderTrackerId = counterLocalService.increment();
		} catch (SystemException e) {
		}
		
		EBSOrderTracker ebsOrderTracker = ebsOrderTrackerPersistence.create(ebsOrderTrackerId);
		
		ebsOrderTracker.setCompanyId(companyId);
		ebsOrderTracker.setOrderId(orderId);
		ebsOrderTracker.setUserId(userId);
		ebsOrderTracker.setResponseCode(responseCode);
		ebsOrderTracker.setResponseMessage(responseMessage);
		ebsOrderTracker.setCreateDate(createDate);
		ebsOrderTracker.setPaymentId(paymentId);
		ebsOrderTracker.setPaymentMethod(paymentMethod);
		ebsOrderTracker.setAmount(amount);
		ebsOrderTracker.setIsFlagged(isFlagged);
		ebsOrderTracker.setTransactionId(transactionId);
		
			try {
				ebsOrderTrackerPersistence.update(ebsOrderTracker);
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
	}
	private static Log _log = LogFactoryUtil.getLog(EBSOrderTrackerLocalServiceImpl.class);
}