package com.esquare.ecommerce.myprofile.action;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.portal.PhoneNumberException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.ListType;
import com.liferay.portal.model.Phone;
import com.liferay.portal.model.User;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class MyprofileAction extends MVCPortlet {
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		UploadPortletRequest uploadRequest = PortalUtil
				.getUploadPortletRequest(actionRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay) uploadRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = themeDisplay.getUserId();
		String firstName = ParamUtil.getString(uploadRequest, "firstName");
		String lastName = ParamUtil.getString(uploadRequest, "lastName");
		String street = ParamUtil.getString(uploadRequest, "street");
		String zip = ParamUtil.getString(uploadRequest, "zip");
		long countryId = ParamUtil.getLong(uploadRequest, "countryId");
		String city = ParamUtil.getString(uploadRequest, "city");

		String mobile = ParamUtil.getString(uploadRequest, "mobile");
		String phoneNumber = ParamUtil.getString(uploadRequest, "phone");

		String mobiles[] = new String[2];
		mobiles[0] = mobile;
		if (Validator.isNotNull(phoneNumber))
			mobiles[1] = phoneNumber;

		long stateId = ParamUtil.getLong(uploadRequest, "regionId");

		String emailAddress = ParamUtil
				.getString(uploadRequest, "emailAddress");
		boolean male = ParamUtil.getBoolean(uploadRequest, "male", true);
		String skypeSn = ParamUtil.getString(uploadRequest, "skypeSn");
		InputStream inputStream = uploadRequest.getFileAsStream("myImageFile");

		int birthdayMonth = ParamUtil
				.getInteger(uploadRequest, "birthdayMonth");
		int birthdayDay = ParamUtil.getInteger(uploadRequest, "birthdayDay");
		int birthdayYear = ParamUtil.getInteger(uploadRequest, "birthdayYear");
		Calendar birthday = CalendarFactoryUtil.getCalendar();
		birthday.set(Calendar.MONTH, birthdayMonth);
		birthday.set(Calendar.DATE, birthdayDay);
		birthday.set(Calendar.YEAR, birthdayYear);

		String password1 = uploadRequest.getParameter("password1");
		String password2 = uploadRequest.getParameter("password2");

		User user = themeDisplay.getUser();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmailAddress(emailAddress);

		if (user != null) {
			try {
				Contact selContact = user.getContact();
				selContact.setMale(male);
				selContact.setSkypeSn(skypeSn);
				selContact.setBirthday(birthday.getTime());
				ContactLocalServiceUtil.updateContact(selContact);

			} catch (PortalException e) {
				_log.info(e.getClass());
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
		}

		List<Address> address;
		List<Phone> phones;
		try {
			address = AddressLocalServiceUtil.getAddresses(themeDisplay
					.getCompanyId(), "com.liferay.portal.model.Contact",
					themeDisplay.getContact().getContactId());

			if (!address.isEmpty() && address != null) {
				address.get(0).setStreet1(street);
				address.get(0).setZip(zip);
				address.get(0).setCity(city);
				address.get(0).setCountryId(countryId);
				address.get(0).setRegionId(stateId);
				AddressLocalServiceUtil.updateAddress(address.get(0));
			} else {
				List<ListType> listTypeAddress = ListTypeServiceUtil
						.getListTypes("com.liferay.portal.model.Contact.address");
				ListType addressType = listTypeAddress.get(0);
				AddressLocalServiceUtil.addAddress(userId,
						Contact.class.getName(), user.getContactId(), street,
						StringPool.BLANK, StringPool.BLANK, city, zip, stateId,
						countryId, addressType.getListTypeId(), false, true);

			}
			phones = PhoneLocalServiceUtil.getPhones(themeDisplay
					.getCompanyId(), "com.liferay.portal.model.Contact",
					themeDisplay.getContact().getContactId());
			List<ListType> listTypePhone = ListTypeServiceUtil
					.getListTypes("com.liferay.portal.model.Contact.phone");
			ListType phoneType = null;
			boolean update = true;
			for (int i = 0; i < mobiles.length; i++) {
				if (phones.size() == 0
						|| (mobiles.length > phones.size() && i == 1))
					update = false;
				if (i == 0) {
					phoneType = listTypePhone.get(0);
				} else if (i == 1) {
					phoneType = listTypePhone.get(2);
				}
				if (update) {
					Phone phone = phones.get(i);
					phone.setNumber(mobiles[i]);
					PhoneLocalServiceUtil.updatePhone(phone);
				} else {
					try {
						PhoneLocalServiceUtil.addPhone(userId,
								Contact.class.getName(), user.getContactId(),
								mobiles[i], StringPool.BLANK,
								phoneType.getListTypeId(), false);
					} catch (PhoneNumberException phoneNumberException) {
					}
				}
			}

			UserLocalServiceUtil.updateUser(user);
			if (Validator.isNotNull(password1)
					&& Validator.isNotNull(password2)
					&& password1.equals(password2)) {
				UserLocalServiceUtil.updatePassword(user.getUserId(),
						password1, password2, false);
			}
			if (inputStream != null) {
				updatePortrait(inputStream, userId);
			}

		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (Exception e) {
			_log.info(e.getClass());
		}

		super.processAction(actionRequest, actionResponse);
	}

	protected void updatePortrait(InputStream inputStream, long userId)
			throws PortalException, SystemException, IOException {
		if (inputStream == null) {
			throw new UploadException();
		}
		byte[] bytes = FileUtil.getBytes(inputStream);
		UserLocalServiceUtil.updatePortrait(userId, bytes);
	}

	private static Log _log = LogFactoryUtil.getLog(MyprofileAction.class);
}