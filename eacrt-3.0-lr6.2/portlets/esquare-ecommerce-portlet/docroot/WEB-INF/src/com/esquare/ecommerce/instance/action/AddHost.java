package com.esquare.ecommerce.instance.action;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;

public class AddHost {
 
    public void addAHost(String hostName){
		    	
		String fileName=null;
    	String osName = System.getProperty("os.name").toLowerCase();
    	if(osName.equalsIgnoreCase("linux")){
    		fileName = "/etc/hosts";
    	}
    	else if(osName.contains("windows")){
    		
    	    fileName="C:/Windows/System32/drivers/etc/hosts";	
    	}
    	
        FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(fileName,true);
		} catch (IOException e) {
			_log.info(e.getClass());
		}
           
        BufferedWriter bufferedWriter =
                new BufferedWriter(fileWriter);

            try {
            	
            	bufferedWriter.newLine();
				bufferedWriter.write("127.0.0.1 ");
				bufferedWriter.write(hostName);
	            bufferedWriter.newLine();

	            // Always close files.
	            bufferedWriter.close();
			} catch (IOException e) {
				_log.info(e.getClass());
			}
            
    
	}
    
    public void updateDomain(String oldDomainName , String newDomainName) throws IOException{
    	String fileName=null;
    	String osName = System.getProperty("os.name").toLowerCase();
    	if(osName.equalsIgnoreCase("linux")){
    		fileName = "/etc/hosts";
    	}
    	else if(osName.contains("windows")){
    	    fileName="C:/Windows/System32/drivers/etc/hosts";	
    	}
    	File f = new File(fileName);
        String content = FileUtil.read(fileName);
        FileUtil.write(f, content.replaceAll(oldDomainName, newDomainName));
    }
    private static Log _log=LogFactoryUtil.getLog(AddHost.class);
}