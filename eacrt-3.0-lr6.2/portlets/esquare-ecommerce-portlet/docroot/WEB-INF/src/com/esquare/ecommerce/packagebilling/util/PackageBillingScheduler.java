package com.esquare.ecommerce.packagebilling.util;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.EcartPackageBillingLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;

public class PackageBillingScheduler implements MessageListener {

	public void receive(Message message) throws MessageListenerException {
		try {
			List<Instance> instance = EcartPackageBillingLocalServiceUtil
					.getPackageBillingList(PortletPropsValues.PACKAGE_TYPE_FREE);
			CartPermission cartPermission = null;
			for (Instance instance2 : instance) {
				cartPermission = CartPermissionLocalServiceUtil
						.getCartPermissionCI_PK(instance2.getCompanyId(),
								EECConstants.MONTH);
				Double amount = 0d;
				amount += cartPermission.getPrice();
				EcartPackageBillingLocalServiceUtil.addEcartPackageBilling(
						instance2.getCompanyId(),
						instance2.getInstanceUserId(), new Date(),
						instance2.getPackageType(), amount);
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	private static Log _log = LogFactoryUtil
			.getLog(PackageBillingScheduler.class);
}
