package com.esquare.ecommerce.service.persistence;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.esquare.ecommerce.model.Order;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class OrderFinderImpl extends BasePersistenceImpl<Order> implements
		OrderFinder {

	public static String GET_TODAY_ORDER_COUNT = "getTodayOrderCount";
	public static String GET_LAST_NO_OF_DAYS_ORDER_COUNT = "getLastNoOfDaysOrderCount";
	public static String GET_LAST_WEEK_EACH_DAY_COUNT = "getLastWeekEachDayCount";
	public static String GET_TODAY_PURCHASED_USER_COUNT = "getTodayPurchasedUserCount";

	public int getTodayOrderCount(String isNotStatus, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_TODAY_ORDER_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("TODAY_ORDER_COUNT_VALUE", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public int getLastNoOfDaysOrderCount(int day, String isNotStatus,
			long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_LAST_NO_OF_DAYS_ORDER_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("LAST_DAYS_ORDER_COUNT_VALUE", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(day);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}

	public Map<String, String> getLastWeekEachDayCount(String isNotStatus,
			long companyId) {
		Session session = null;
		SQLQuery query = null;
		Map<String, String> getWeekEachDayCount = new LinkedHashMap<String, String>();
		try {
			String sql = CustomSQLUtil.get(GET_LAST_WEEK_EACH_DAY_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("THE_DATE", Type.STRING);
			query.addScalar("COUNT_VALUE", Type.STRING);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(isNotStatus);
			qPos.add(companyId);
			Iterator<Object[]> itr = query.list().iterator();
			while (itr.hasNext()) {
				Object[] array = itr.next();
				String theDate = (String) array[0];
				String countValue = (String) array[1];
				getWeekEachDayCount.put(theDate, countValue);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return getWeekEachDayCount;
	}

	public int getTodayPurchasedUserCount(String status, long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_TODAY_PURCHASED_USER_COUNT);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.addScalar("TODAY_PURCHASED_USER_COUNT", Type.LONG);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(status);
			qPos.add(companyId);
			if (Validator.isNotNull(query.uniqueResult()))
				return Integer.valueOf(query.uniqueResult().toString());

		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return 0;
	}
private static Log _log = LogFactoryUtil.getLog(OrderFinderImpl.class);
}