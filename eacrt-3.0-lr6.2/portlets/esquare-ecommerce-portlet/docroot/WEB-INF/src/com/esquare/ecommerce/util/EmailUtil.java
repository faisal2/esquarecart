package com.esquare.ecommerce.util;

import java.io.IOException;
import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.PortletPreferences;

import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;

public class EmailUtil {

	public static void sendMail(ActionRequest actionRequest, long orderId,
			String cmd, String orderMailContent, String subject)
			throws IOException, SystemException, PortalException {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		Long companyId = themeDisplay.getCompanyId();
		Long userId = themeDisplay.getUserId();
		String tabs = ParamUtil.getString(actionRequest, "tabs");
		String emailFromName = PortletPropsValues.ADMIN_EMAIL_FROM_NAME;
		String senderMailAddress = PortletPropsValues.ADMIN_EMAIL_FROM_ADDRESS;
		String emailAddress = null;

		if (cmd.equals(EECConstants.CHECKOUT)) {
			subject = ContentUtil.get(PortletProps.get(cmd
					+ ".emailConfirmation.subject"));
			orderMailContent = getItemDetails(actionRequest, orderId,
					companyId, cmd);
		}

		Order order = OrderLocalServiceUtil.getOrder(orderId);

		if (Validator.isNotNull(order)) {

			// long orderId = order.getOrderId();
			String trackNumber = order.getNumber();
			emailAddress = order.getBillingEmailAddress();
			orderMailContent = getItemDetails(actionRequest, orderId,
					companyId, cmd);

			String shippingAddress = order.getShippingFirstName() + " "
					+ order.getShippingLastName() + "<br>"
					+ order.getShippingEmailAddress() + "<br>"
					+ order.getShippingStreet() + "<br>"
					+ order.getShippingCity() + "<br>"
					+ order.getShippingState() + "<br>"
					+ order.getShippingZip() + "<br>"
					+ order.getShippingCountry();

			String phone = order.getShippingPhone();

			String[] oldStub = new String[] { "[$TO_NAME$]",
					"[$ORDER_NUMBER$]", "[$FROM_NAME$]", "[$FROM_ADDRESS$]",
					"[$PORTAL_URL$]", "[$STORE_ID$]", "[$STORE_DOMAIN$]",
					"[$ORDER_NUMBER$]", "[$ORDER_DETAILS_TABLE$]",
					"[$SHIIPING_ADDRESS$]", "[$PHONE_NUMBER$]" };

			String[] newStub = new String[] { order.getBillingFirstName(),
					trackNumber, emailFromName, senderMailAddress,
					PortletPropsValues.DEFAULT_PORTAL_URL,
					String.valueOf(companyId),
					themeDisplay.getCompany().getWebId(),
					Long.toString(orderId), orderMailContent, shippingAddress,
					phone };

			subject = StringUtil.replace(subject, oldStub, newStub);
			orderMailContent = StringUtil.replace(orderMailContent, oldStub,
					newStub);

			try {
				MailMessage mailMessage = new MailMessage();
				mailMessage.setBody(orderMailContent);
				mailMessage.setHTMLFormat(true);
				mailMessage.setSubject(subject);
				mailMessage.setFrom(new InternetAddress(senderMailAddress));
				mailMessage.setTo(new InternetAddress(emailAddress));
				MailServiceUtil.sendEmail(mailMessage);
				SessionMessages.add(actionRequest.getPortletSession(),
						"mail-send-success");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static String getItemDetails(ActionRequest actionRequest,
			long orderId, long companyId, String cmd) {

		StringBuffer sb = new StringBuffer();
		String orderMailContent = StringPool.BLANK;
		try {
			List<OrderItem> orderItems = OrderItemLocalServiceUtil
					.getOrderItems(orderId);
			sb.append(getTemplate(actionRequest, companyId, cmd
					+ ".details.table.header"));

			for (OrderItem orderItem1 : orderItems) {
				orderMailContent = getTemplate(actionRequest, companyId, cmd
						+ ".details.table.content");
				String[] oldSubs = { "[$Name$]", "[$Quantity$]",
						"[$Expected Delivery Date$]", "[$Price$]",
						"[$Sub Total$]", };
				String[] newSubs = {
						orderItem1.getName(),
						String.valueOf(orderItem1.getQuantity()),
						String.valueOf(orderItem1.getExpectedDeliveryDate()),
						String.valueOf(orderItem1.getPrice()),
						String.valueOf(orderItem1.getQuantity()
								* orderItem1.getPrice()) };
				sb.append(StringUtil
						.replace(orderMailContent, oldSubs, newSubs));
			}
			sb.append(getTemplate(actionRequest, companyId, cmd
					+ ".details.table.footer"));
		} catch (PortalException e) {
		} catch (SystemException e) {
		}
		return sb.toString();
	}

	public static String getTemplate(ActionRequest actionRequest,
			long companyId, String template) throws PortalException,
			SystemException {
		PortletPreferences preferences = PrefsPropsUtil
				.getPreferences(companyId);
		String emailBody = PrefsParamUtil.getString(preferences, actionRequest,
				template);
		if (Validator.isNotNull(emailBody)) {
			return emailBody;
		} else {
			return ContentUtil.get(PortletProps.get(template));
		}
	}

	private static Log _log = LogFactoryUtil.getLog(EmailUtil.class);
}
