package com.esquare.ecommerce.model.bulkdataupload;

public class UploadProductModel {
	
	private String name;
	private String Description;
	private boolean visibility;
	private String relatedProducts;
	private UploadInventoryModel uploadInventoryModel;
	
	public UploadInventoryModel getUploadInventoryModel() {
		return uploadInventoryModel;
	}
	public void setUploadInventoryModel(UploadInventoryModel uploadInventoryModel) {
		this.uploadInventoryModel = uploadInventoryModel;
	}
	public String getRelatedProducts() {
		return relatedProducts;
	}
	public void setRelatedProducts(String relatedProducts) {
		this.relatedProducts = relatedProducts;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
		public boolean isVisibility() {
		return visibility;
	}
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
		
}
