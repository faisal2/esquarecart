package com.esquare.ecommerce.instlistener.action;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

//import org.apache.jasper.tagplugins.jstl.core.Param;

import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class InstanceListenerAction extends MVCPortlet {

	@javax.portlet.ProcessEvent(qname = "{http://esquareinfo.com}instanceinfo")
	public void handleProcessempinfoEvent(javax.portlet.EventRequest request,
			javax.portlet.EventResponse response)
			throws javax.portlet.PortletException, java.io.IOException {
		javax.portlet.Event event = request.getEvent();
		String value = (String) event.getValue();
		response.setRenderParameter("InstanceInfo", value);
		PortletSession session = request.getPortletSession();
		session.setAttribute("InstanceInfo", value,
				PortletSession.APPLICATION_SCOPE);
	}

	public void loginURL(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		
		String webId = "http://" + ParamUtil.getString(actionRequest, "webId");
		
		if (!PortletPropsValues.CURRENT_WORKING_ENVIRONMENT
				.equals("Production")) {
			webId = webId + ":8080";
		}
		actionResponse.sendRedirect(webId);
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
}
