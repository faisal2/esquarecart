/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.CartPermission;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CartPermission in entity cache.
 *
 * @author Esquare
 * @see CartPermission
 * @generated
 */
public class CartPermissionCacheModel implements CacheModel<CartPermission>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{permissionId=");
		sb.append(permissionId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", permissionKey=");
		sb.append(permissionKey);
		sb.append(", Value=");
		sb.append(Value);
		sb.append(", price=");
		sb.append(price);
		sb.append(", unlimitedPck=");
		sb.append(unlimitedPck);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CartPermission toEntityModel() {
		CartPermissionImpl cartPermissionImpl = new CartPermissionImpl();

		cartPermissionImpl.setPermissionId(permissionId);
		cartPermissionImpl.setCompanyId(companyId);

		if (permissionKey == null) {
			cartPermissionImpl.setPermissionKey(StringPool.BLANK);
		}
		else {
			cartPermissionImpl.setPermissionKey(permissionKey);
		}

		if (Value == null) {
			cartPermissionImpl.setValue(StringPool.BLANK);
		}
		else {
			cartPermissionImpl.setValue(Value);
		}

		cartPermissionImpl.setPrice(price);
		cartPermissionImpl.setUnlimitedPck(unlimitedPck);

		cartPermissionImpl.resetOriginalValues();

		return cartPermissionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		permissionId = objectInput.readLong();
		companyId = objectInput.readLong();
		permissionKey = objectInput.readUTF();
		Value = objectInput.readUTF();
		price = objectInput.readInt();
		unlimitedPck = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(permissionId);
		objectOutput.writeLong(companyId);

		if (permissionKey == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(permissionKey);
		}

		if (Value == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Value);
		}

		objectOutput.writeInt(price);
		objectOutput.writeBoolean(unlimitedPck);
	}

	public long permissionId;
	public long companyId;
	public String permissionKey;
	public String Value;
	public int price;
	public boolean unlimitedPck;
}