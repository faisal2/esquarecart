/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchEcartEbsPaymentException;
import com.esquare.ecommerce.model.EcartEbsPayment;
import com.esquare.ecommerce.model.impl.EcartEbsPaymentImpl;
import com.esquare.ecommerce.model.impl.EcartEbsPaymentModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the ecart ebs payment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see EcartEbsPaymentPersistence
 * @see EcartEbsPaymentUtil
 * @generated
 */
public class EcartEbsPaymentPersistenceImpl extends BasePersistenceImpl<EcartEbsPayment>
	implements EcartEbsPaymentPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EcartEbsPaymentUtil} to access the ecart ebs payment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EcartEbsPaymentImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentModelImpl.FINDER_CACHE_ENABLED,
			EcartEbsPaymentImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentModelImpl.FINDER_CACHE_ENABLED,
			EcartEbsPaymentImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public EcartEbsPaymentPersistenceImpl() {
		setModelClass(EcartEbsPayment.class);
	}

	/**
	 * Caches the ecart ebs payment in the entity cache if it is enabled.
	 *
	 * @param ecartEbsPayment the ecart ebs payment
	 */
	@Override
	public void cacheResult(EcartEbsPayment ecartEbsPayment) {
		EntityCacheUtil.putResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentImpl.class, ecartEbsPayment.getPrimaryKey(),
			ecartEbsPayment);

		ecartEbsPayment.resetOriginalValues();
	}

	/**
	 * Caches the ecart ebs payments in the entity cache if it is enabled.
	 *
	 * @param ecartEbsPayments the ecart ebs payments
	 */
	@Override
	public void cacheResult(List<EcartEbsPayment> ecartEbsPayments) {
		for (EcartEbsPayment ecartEbsPayment : ecartEbsPayments) {
			if (EntityCacheUtil.getResult(
						EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
						EcartEbsPaymentImpl.class,
						ecartEbsPayment.getPrimaryKey()) == null) {
				cacheResult(ecartEbsPayment);
			}
			else {
				ecartEbsPayment.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ecart ebs payments.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EcartEbsPaymentImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EcartEbsPaymentImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ecart ebs payment.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EcartEbsPayment ecartEbsPayment) {
		EntityCacheUtil.removeResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentImpl.class, ecartEbsPayment.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EcartEbsPayment> ecartEbsPayments) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EcartEbsPayment ecartEbsPayment : ecartEbsPayments) {
			EntityCacheUtil.removeResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
				EcartEbsPaymentImpl.class, ecartEbsPayment.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ecart ebs payment with the primary key. Does not add the ecart ebs payment to the database.
	 *
	 * @param ebsTrackId the primary key for the new ecart ebs payment
	 * @return the new ecart ebs payment
	 */
	@Override
	public EcartEbsPayment create(long ebsTrackId) {
		EcartEbsPayment ecartEbsPayment = new EcartEbsPaymentImpl();

		ecartEbsPayment.setNew(true);
		ecartEbsPayment.setPrimaryKey(ebsTrackId);

		return ecartEbsPayment;
	}

	/**
	 * Removes the ecart ebs payment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ebsTrackId the primary key of the ecart ebs payment
	 * @return the ecart ebs payment that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcartEbsPaymentException if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment remove(long ebsTrackId)
		throws NoSuchEcartEbsPaymentException, SystemException {
		return remove((Serializable)ebsTrackId);
	}

	/**
	 * Removes the ecart ebs payment with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ecart ebs payment
	 * @return the ecart ebs payment that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcartEbsPaymentException if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment remove(Serializable primaryKey)
		throws NoSuchEcartEbsPaymentException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EcartEbsPayment ecartEbsPayment = (EcartEbsPayment)session.get(EcartEbsPaymentImpl.class,
					primaryKey);

			if (ecartEbsPayment == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEcartEbsPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ecartEbsPayment);
		}
		catch (NoSuchEcartEbsPaymentException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EcartEbsPayment removeImpl(EcartEbsPayment ecartEbsPayment)
		throws SystemException {
		ecartEbsPayment = toUnwrappedModel(ecartEbsPayment);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ecartEbsPayment)) {
				ecartEbsPayment = (EcartEbsPayment)session.get(EcartEbsPaymentImpl.class,
						ecartEbsPayment.getPrimaryKeyObj());
			}

			if (ecartEbsPayment != null) {
				session.delete(ecartEbsPayment);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ecartEbsPayment != null) {
			clearCache(ecartEbsPayment);
		}

		return ecartEbsPayment;
	}

	@Override
	public EcartEbsPayment updateImpl(
		com.esquare.ecommerce.model.EcartEbsPayment ecartEbsPayment)
		throws SystemException {
		ecartEbsPayment = toUnwrappedModel(ecartEbsPayment);

		boolean isNew = ecartEbsPayment.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ecartEbsPayment.isNew()) {
				session.save(ecartEbsPayment);

				ecartEbsPayment.setNew(false);
			}
			else {
				session.merge(ecartEbsPayment);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
			EcartEbsPaymentImpl.class, ecartEbsPayment.getPrimaryKey(),
			ecartEbsPayment);

		return ecartEbsPayment;
	}

	protected EcartEbsPayment toUnwrappedModel(EcartEbsPayment ecartEbsPayment) {
		if (ecartEbsPayment instanceof EcartEbsPaymentImpl) {
			return ecartEbsPayment;
		}

		EcartEbsPaymentImpl ecartEbsPaymentImpl = new EcartEbsPaymentImpl();

		ecartEbsPaymentImpl.setNew(ecartEbsPayment.isNew());
		ecartEbsPaymentImpl.setPrimaryKey(ecartEbsPayment.getPrimaryKey());

		ecartEbsPaymentImpl.setEbsTrackId(ecartEbsPayment.getEbsTrackId());
		ecartEbsPaymentImpl.setCompanyId(ecartEbsPayment.getCompanyId());
		ecartEbsPaymentImpl.setEcartAutoId(ecartEbsPayment.getEcartAutoId());
		ecartEbsPaymentImpl.setUserId(ecartEbsPayment.getUserId());
		ecartEbsPaymentImpl.setResponseCode(ecartEbsPayment.getResponseCode());
		ecartEbsPaymentImpl.setResponseMessage(ecartEbsPayment.getResponseMessage());
		ecartEbsPaymentImpl.setPaidDate(ecartEbsPayment.getPaidDate());
		ecartEbsPaymentImpl.setPaymentId(ecartEbsPayment.getPaymentId());
		ecartEbsPaymentImpl.setPaymentMethod(ecartEbsPayment.getPaymentMethod());
		ecartEbsPaymentImpl.setAmount(ecartEbsPayment.getAmount());
		ecartEbsPaymentImpl.setIsFlagged(ecartEbsPayment.getIsFlagged());
		ecartEbsPaymentImpl.setTransactionId(ecartEbsPayment.getTransactionId());

		return ecartEbsPaymentImpl;
	}

	/**
	 * Returns the ecart ebs payment with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecart ebs payment
	 * @return the ecart ebs payment
	 * @throws com.esquare.ecommerce.NoSuchEcartEbsPaymentException if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEcartEbsPaymentException, SystemException {
		EcartEbsPayment ecartEbsPayment = fetchByPrimaryKey(primaryKey);

		if (ecartEbsPayment == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEcartEbsPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ecartEbsPayment;
	}

	/**
	 * Returns the ecart ebs payment with the primary key or throws a {@link com.esquare.ecommerce.NoSuchEcartEbsPaymentException} if it could not be found.
	 *
	 * @param ebsTrackId the primary key of the ecart ebs payment
	 * @return the ecart ebs payment
	 * @throws com.esquare.ecommerce.NoSuchEcartEbsPaymentException if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment findByPrimaryKey(long ebsTrackId)
		throws NoSuchEcartEbsPaymentException, SystemException {
		return findByPrimaryKey((Serializable)ebsTrackId);
	}

	/**
	 * Returns the ecart ebs payment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecart ebs payment
	 * @return the ecart ebs payment, or <code>null</code> if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EcartEbsPayment ecartEbsPayment = (EcartEbsPayment)EntityCacheUtil.getResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
				EcartEbsPaymentImpl.class, primaryKey);

		if (ecartEbsPayment == _nullEcartEbsPayment) {
			return null;
		}

		if (ecartEbsPayment == null) {
			Session session = null;

			try {
				session = openSession();

				ecartEbsPayment = (EcartEbsPayment)session.get(EcartEbsPaymentImpl.class,
						primaryKey);

				if (ecartEbsPayment != null) {
					cacheResult(ecartEbsPayment);
				}
				else {
					EntityCacheUtil.putResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
						EcartEbsPaymentImpl.class, primaryKey,
						_nullEcartEbsPayment);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EcartEbsPaymentModelImpl.ENTITY_CACHE_ENABLED,
					EcartEbsPaymentImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ecartEbsPayment;
	}

	/**
	 * Returns the ecart ebs payment with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ebsTrackId the primary key of the ecart ebs payment
	 * @return the ecart ebs payment, or <code>null</code> if a ecart ebs payment with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartEbsPayment fetchByPrimaryKey(long ebsTrackId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)ebsTrackId);
	}

	/**
	 * Returns all the ecart ebs payments.
	 *
	 * @return the ecart ebs payments
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartEbsPayment> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ecart ebs payments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartEbsPaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecart ebs payments
	 * @param end the upper bound of the range of ecart ebs payments (not inclusive)
	 * @return the range of ecart ebs payments
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartEbsPayment> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecart ebs payments.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartEbsPaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecart ebs payments
	 * @param end the upper bound of the range of ecart ebs payments (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ecart ebs payments
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartEbsPayment> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EcartEbsPayment> list = (List<EcartEbsPayment>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ECARTEBSPAYMENT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ECARTEBSPAYMENT;

				if (pagination) {
					sql = sql.concat(EcartEbsPaymentModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EcartEbsPayment>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcartEbsPayment>(list);
				}
				else {
					list = (List<EcartEbsPayment>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ecart ebs payments from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EcartEbsPayment ecartEbsPayment : findAll()) {
			remove(ecartEbsPayment);
		}
	}

	/**
	 * Returns the number of ecart ebs payments.
	 *
	 * @return the number of ecart ebs payments
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ECARTEBSPAYMENT);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the ecart ebs payment persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.EcartEbsPayment")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EcartEbsPayment>> listenersList = new ArrayList<ModelListener<EcartEbsPayment>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EcartEbsPayment>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EcartEbsPaymentImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ECARTEBSPAYMENT = "SELECT ecartEbsPayment FROM EcartEbsPayment ecartEbsPayment";
	private static final String _SQL_COUNT_ECARTEBSPAYMENT = "SELECT COUNT(ecartEbsPayment) FROM EcartEbsPayment ecartEbsPayment";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ecartEbsPayment.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EcartEbsPayment exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EcartEbsPaymentPersistenceImpl.class);
	private static EcartEbsPayment _nullEcartEbsPayment = new EcartEbsPaymentImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EcartEbsPayment> toCacheModel() {
				return _nullEcartEbsPaymentCacheModel;
			}
		};

	private static CacheModel<EcartEbsPayment> _nullEcartEbsPaymentCacheModel = new CacheModel<EcartEbsPayment>() {
			@Override
			public EcartEbsPayment toEntityModel() {
				return _nullEcartEbsPayment;
			}
		};
}