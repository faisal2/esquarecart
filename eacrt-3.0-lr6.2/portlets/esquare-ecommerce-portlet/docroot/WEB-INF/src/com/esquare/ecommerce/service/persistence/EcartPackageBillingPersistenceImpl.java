/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchEcartPackageBillingException;
import com.esquare.ecommerce.model.EcartPackageBilling;
import com.esquare.ecommerce.model.impl.EcartPackageBillingImpl;
import com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the ecart package billing service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see EcartPackageBillingPersistence
 * @see EcartPackageBillingUtil
 * @generated
 */
public class EcartPackageBillingPersistenceImpl extends BasePersistenceImpl<EcartPackageBilling>
	implements EcartPackageBillingPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EcartPackageBillingUtil} to access the ecart package billing persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EcartPackageBillingImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			EcartPackageBillingModelImpl.COMPANYID_COLUMN_BITMASK |
			EcartPackageBillingModelImpl.BILLINGDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the ecart package billings where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByCompanyId(long companyId)
		throws SystemException {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the ecart package billings where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @return the range of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByCompanyId(long companyId, int start,
		int end) throws SystemException {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecart package billings where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByCompanyId(long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<EcartPackageBilling> list = (List<EcartPackageBilling>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EcartPackageBilling ecartPackageBilling : list) {
				if ((companyId != ecartPackageBilling.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_ECARTPACKAGEBILLING_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EcartPackageBillingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcartPackageBilling>(list);
				}
				else {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ecart package billing in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByCompanyId_First(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (ecartPackageBilling != null) {
			return ecartPackageBilling;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcartPackageBillingException(msg.toString());
	}

	/**
	 * Returns the first ecart package billing in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecart package billing, or <code>null</code> if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByCompanyId_First(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<EcartPackageBilling> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ecart package billing in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (ecartPackageBilling != null) {
			return ecartPackageBilling;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcartPackageBillingException(msg.toString());
	}

	/**
	 * Returns the last ecart package billing in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecart package billing, or <code>null</code> if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByCompanyId_Last(long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<EcartPackageBilling> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ecart package billings before and after the current ecart package billing in the ordered set where companyId = &#63;.
	 *
	 * @param billId the primary key of the current ecart package billing
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling[] findByCompanyId_PrevAndNext(long billId,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = findByPrimaryKey(billId);

		Session session = null;

		try {
			session = openSession();

			EcartPackageBilling[] array = new EcartPackageBillingImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, ecartPackageBilling,
					companyId, orderByComparator, true);

			array[1] = ecartPackageBilling;

			array[2] = getByCompanyId_PrevAndNext(session, ecartPackageBilling,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EcartPackageBilling getByCompanyId_PrevAndNext(Session session,
		EcartPackageBilling ecartPackageBilling, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ECARTPACKAGEBILLING_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EcartPackageBillingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ecartPackageBilling);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EcartPackageBilling> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ecart package billings where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCompanyId(long companyId) throws SystemException {
		for (EcartPackageBilling ecartPackageBilling : findByCompanyId(
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ecartPackageBilling);
		}
	}

	/**
	 * Returns the number of ecart package billings where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCompanyId(long companyId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_ECARTPACKAGEBILLING_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "ecartPackageBilling.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_T_ID = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_T_Id",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_T_ID =
		new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED,
			EcartPackageBillingImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_T_Id",
			new String[] { Long.class.getName(), Long.class.getName() },
			EcartPackageBillingModelImpl.COMPANYID_COLUMN_BITMASK |
			EcartPackageBillingModelImpl.TRANSACTIONID_COLUMN_BITMASK |
			EcartPackageBillingModelImpl.BILLINGDATE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_T_ID = new FinderPath(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_T_Id",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the ecart package billings where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @return the matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByC_T_Id(long companyId,
		long transactionId) throws SystemException {
		return findByC_T_Id(companyId, transactionId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ecart package billings where companyId = &#63; and transactionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @return the range of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByC_T_Id(long companyId,
		long transactionId, int start, int end) throws SystemException {
		return findByC_T_Id(companyId, transactionId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecart package billings where companyId = &#63; and transactionId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findByC_T_Id(long companyId,
		long transactionId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_T_ID;
			finderArgs = new Object[] { companyId, transactionId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_T_ID;
			finderArgs = new Object[] {
					companyId, transactionId,
					
					start, end, orderByComparator
				};
		}

		List<EcartPackageBilling> list = (List<EcartPackageBilling>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EcartPackageBilling ecartPackageBilling : list) {
				if ((companyId != ecartPackageBilling.getCompanyId()) ||
						(transactionId != ecartPackageBilling.getTransactionId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_ECARTPACKAGEBILLING_WHERE);

			query.append(_FINDER_COLUMN_C_T_ID_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_T_ID_TRANSACTIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EcartPackageBillingModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(transactionId);

				if (!pagination) {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcartPackageBilling>(list);
				}
				else {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first ecart package billing in the ordered set where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByC_T_Id_First(long companyId,
		long transactionId, OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = fetchByC_T_Id_First(companyId,
				transactionId, orderByComparator);

		if (ecartPackageBilling != null) {
			return ecartPackageBilling;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", transactionId=");
		msg.append(transactionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcartPackageBillingException(msg.toString());
	}

	/**
	 * Returns the first ecart package billing in the ordered set where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching ecart package billing, or <code>null</code> if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByC_T_Id_First(long companyId,
		long transactionId, OrderByComparator orderByComparator)
		throws SystemException {
		List<EcartPackageBilling> list = findByC_T_Id(companyId, transactionId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last ecart package billing in the ordered set where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByC_T_Id_Last(long companyId,
		long transactionId, OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = fetchByC_T_Id_Last(companyId,
				transactionId, orderByComparator);

		if (ecartPackageBilling != null) {
			return ecartPackageBilling;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", transactionId=");
		msg.append(transactionId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEcartPackageBillingException(msg.toString());
	}

	/**
	 * Returns the last ecart package billing in the ordered set where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching ecart package billing, or <code>null</code> if a matching ecart package billing could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByC_T_Id_Last(long companyId,
		long transactionId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByC_T_Id(companyId, transactionId);

		if (count == 0) {
			return null;
		}

		List<EcartPackageBilling> list = findByC_T_Id(companyId, transactionId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the ecart package billings before and after the current ecart package billing in the ordered set where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param billId the primary key of the current ecart package billing
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling[] findByC_T_Id_PrevAndNext(long billId,
		long companyId, long transactionId, OrderByComparator orderByComparator)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = findByPrimaryKey(billId);

		Session session = null;

		try {
			session = openSession();

			EcartPackageBilling[] array = new EcartPackageBillingImpl[3];

			array[0] = getByC_T_Id_PrevAndNext(session, ecartPackageBilling,
					companyId, transactionId, orderByComparator, true);

			array[1] = ecartPackageBilling;

			array[2] = getByC_T_Id_PrevAndNext(session, ecartPackageBilling,
					companyId, transactionId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EcartPackageBilling getByC_T_Id_PrevAndNext(Session session,
		EcartPackageBilling ecartPackageBilling, long companyId,
		long transactionId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_ECARTPACKAGEBILLING_WHERE);

		query.append(_FINDER_COLUMN_C_T_ID_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_T_ID_TRANSACTIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EcartPackageBillingModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(transactionId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ecartPackageBilling);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EcartPackageBilling> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the ecart package billings where companyId = &#63; and transactionId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByC_T_Id(long companyId, long transactionId)
		throws SystemException {
		for (EcartPackageBilling ecartPackageBilling : findByC_T_Id(companyId,
				transactionId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ecartPackageBilling);
		}
	}

	/**
	 * Returns the number of ecart package billings where companyId = &#63; and transactionId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param transactionId the transaction ID
	 * @return the number of matching ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByC_T_Id(long companyId, long transactionId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_C_T_ID;

		Object[] finderArgs = new Object[] { companyId, transactionId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ECARTPACKAGEBILLING_WHERE);

			query.append(_FINDER_COLUMN_C_T_ID_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_T_ID_TRANSACTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(transactionId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_C_T_ID_COMPANYID_2 = "ecartPackageBilling.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_T_ID_TRANSACTIONID_2 = "ecartPackageBilling.transactionId = ?";

	public EcartPackageBillingPersistenceImpl() {
		setModelClass(EcartPackageBilling.class);
	}

	/**
	 * Caches the ecart package billing in the entity cache if it is enabled.
	 *
	 * @param ecartPackageBilling the ecart package billing
	 */
	@Override
	public void cacheResult(EcartPackageBilling ecartPackageBilling) {
		EntityCacheUtil.putResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingImpl.class, ecartPackageBilling.getPrimaryKey(),
			ecartPackageBilling);

		ecartPackageBilling.resetOriginalValues();
	}

	/**
	 * Caches the ecart package billings in the entity cache if it is enabled.
	 *
	 * @param ecartPackageBillings the ecart package billings
	 */
	@Override
	public void cacheResult(List<EcartPackageBilling> ecartPackageBillings) {
		for (EcartPackageBilling ecartPackageBilling : ecartPackageBillings) {
			if (EntityCacheUtil.getResult(
						EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
						EcartPackageBillingImpl.class,
						ecartPackageBilling.getPrimaryKey()) == null) {
				cacheResult(ecartPackageBilling);
			}
			else {
				ecartPackageBilling.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ecart package billings.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EcartPackageBillingImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EcartPackageBillingImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ecart package billing.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EcartPackageBilling ecartPackageBilling) {
		EntityCacheUtil.removeResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingImpl.class, ecartPackageBilling.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EcartPackageBilling> ecartPackageBillings) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EcartPackageBilling ecartPackageBilling : ecartPackageBillings) {
			EntityCacheUtil.removeResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
				EcartPackageBillingImpl.class,
				ecartPackageBilling.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ecart package billing with the primary key. Does not add the ecart package billing to the database.
	 *
	 * @param billId the primary key for the new ecart package billing
	 * @return the new ecart package billing
	 */
	@Override
	public EcartPackageBilling create(long billId) {
		EcartPackageBilling ecartPackageBilling = new EcartPackageBillingImpl();

		ecartPackageBilling.setNew(true);
		ecartPackageBilling.setPrimaryKey(billId);

		return ecartPackageBilling;
	}

	/**
	 * Removes the ecart package billing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param billId the primary key of the ecart package billing
	 * @return the ecart package billing that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling remove(long billId)
		throws NoSuchEcartPackageBillingException, SystemException {
		return remove((Serializable)billId);
	}

	/**
	 * Removes the ecart package billing with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ecart package billing
	 * @return the ecart package billing that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling remove(Serializable primaryKey)
		throws NoSuchEcartPackageBillingException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EcartPackageBilling ecartPackageBilling = (EcartPackageBilling)session.get(EcartPackageBillingImpl.class,
					primaryKey);

			if (ecartPackageBilling == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEcartPackageBillingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ecartPackageBilling);
		}
		catch (NoSuchEcartPackageBillingException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EcartPackageBilling removeImpl(
		EcartPackageBilling ecartPackageBilling) throws SystemException {
		ecartPackageBilling = toUnwrappedModel(ecartPackageBilling);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ecartPackageBilling)) {
				ecartPackageBilling = (EcartPackageBilling)session.get(EcartPackageBillingImpl.class,
						ecartPackageBilling.getPrimaryKeyObj());
			}

			if (ecartPackageBilling != null) {
				session.delete(ecartPackageBilling);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ecartPackageBilling != null) {
			clearCache(ecartPackageBilling);
		}

		return ecartPackageBilling;
	}

	@Override
	public EcartPackageBilling updateImpl(
		com.esquare.ecommerce.model.EcartPackageBilling ecartPackageBilling)
		throws SystemException {
		ecartPackageBilling = toUnwrappedModel(ecartPackageBilling);

		boolean isNew = ecartPackageBilling.isNew();

		EcartPackageBillingModelImpl ecartPackageBillingModelImpl = (EcartPackageBillingModelImpl)ecartPackageBilling;

		Session session = null;

		try {
			session = openSession();

			if (ecartPackageBilling.isNew()) {
				session.save(ecartPackageBilling);

				ecartPackageBilling.setNew(false);
			}
			else {
				session.merge(ecartPackageBilling);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EcartPackageBillingModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((ecartPackageBillingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ecartPackageBillingModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { ecartPackageBillingModelImpl.getCompanyId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_COMPANYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((ecartPackageBillingModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_T_ID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ecartPackageBillingModelImpl.getOriginalCompanyId(),
						ecartPackageBillingModelImpl.getOriginalTransactionId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_T_ID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_T_ID,
					args);

				args = new Object[] {
						ecartPackageBillingModelImpl.getCompanyId(),
						ecartPackageBillingModelImpl.getTransactionId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_T_ID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_T_ID,
					args);
			}
		}

		EntityCacheUtil.putResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
			EcartPackageBillingImpl.class, ecartPackageBilling.getPrimaryKey(),
			ecartPackageBilling);

		return ecartPackageBilling;
	}

	protected EcartPackageBilling toUnwrappedModel(
		EcartPackageBilling ecartPackageBilling) {
		if (ecartPackageBilling instanceof EcartPackageBillingImpl) {
			return ecartPackageBilling;
		}

		EcartPackageBillingImpl ecartPackageBillingImpl = new EcartPackageBillingImpl();

		ecartPackageBillingImpl.setNew(ecartPackageBilling.isNew());
		ecartPackageBillingImpl.setPrimaryKey(ecartPackageBilling.getPrimaryKey());

		ecartPackageBillingImpl.setBillId(ecartPackageBilling.getBillId());
		ecartPackageBillingImpl.setCompanyId(ecartPackageBilling.getCompanyId());
		ecartPackageBillingImpl.setUserId(ecartPackageBilling.getUserId());
		ecartPackageBillingImpl.setBillingDate(ecartPackageBilling.getBillingDate());
		ecartPackageBillingImpl.setPackageType(ecartPackageBilling.getPackageType());
		ecartPackageBillingImpl.setAmount(ecartPackageBilling.getAmount());
		ecartPackageBillingImpl.setTransactionId(ecartPackageBilling.getTransactionId());

		return ecartPackageBillingImpl;
	}

	/**
	 * Returns the ecart package billing with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecart package billing
	 * @return the ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEcartPackageBillingException, SystemException {
		EcartPackageBilling ecartPackageBilling = fetchByPrimaryKey(primaryKey);

		if (ecartPackageBilling == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEcartPackageBillingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ecartPackageBilling;
	}

	/**
	 * Returns the ecart package billing with the primary key or throws a {@link com.esquare.ecommerce.NoSuchEcartPackageBillingException} if it could not be found.
	 *
	 * @param billId the primary key of the ecart package billing
	 * @return the ecart package billing
	 * @throws com.esquare.ecommerce.NoSuchEcartPackageBillingException if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling findByPrimaryKey(long billId)
		throws NoSuchEcartPackageBillingException, SystemException {
		return findByPrimaryKey((Serializable)billId);
	}

	/**
	 * Returns the ecart package billing with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecart package billing
	 * @return the ecart package billing, or <code>null</code> if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EcartPackageBilling ecartPackageBilling = (EcartPackageBilling)EntityCacheUtil.getResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
				EcartPackageBillingImpl.class, primaryKey);

		if (ecartPackageBilling == _nullEcartPackageBilling) {
			return null;
		}

		if (ecartPackageBilling == null) {
			Session session = null;

			try {
				session = openSession();

				ecartPackageBilling = (EcartPackageBilling)session.get(EcartPackageBillingImpl.class,
						primaryKey);

				if (ecartPackageBilling != null) {
					cacheResult(ecartPackageBilling);
				}
				else {
					EntityCacheUtil.putResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
						EcartPackageBillingImpl.class, primaryKey,
						_nullEcartPackageBilling);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EcartPackageBillingModelImpl.ENTITY_CACHE_ENABLED,
					EcartPackageBillingImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ecartPackageBilling;
	}

	/**
	 * Returns the ecart package billing with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param billId the primary key of the ecart package billing
	 * @return the ecart package billing, or <code>null</code> if a ecart package billing with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcartPackageBilling fetchByPrimaryKey(long billId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)billId);
	}

	/**
	 * Returns all the ecart package billings.
	 *
	 * @return the ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ecart package billings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @return the range of ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecart package billings.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcartPackageBillingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecart package billings
	 * @param end the upper bound of the range of ecart package billings (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcartPackageBilling> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EcartPackageBilling> list = (List<EcartPackageBilling>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ECARTPACKAGEBILLING);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ECARTPACKAGEBILLING;

				if (pagination) {
					sql = sql.concat(EcartPackageBillingModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcartPackageBilling>(list);
				}
				else {
					list = (List<EcartPackageBilling>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ecart package billings from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EcartPackageBilling ecartPackageBilling : findAll()) {
			remove(ecartPackageBilling);
		}
	}

	/**
	 * Returns the number of ecart package billings.
	 *
	 * @return the number of ecart package billings
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ECARTPACKAGEBILLING);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the ecart package billing persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.EcartPackageBilling")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EcartPackageBilling>> listenersList = new ArrayList<ModelListener<EcartPackageBilling>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EcartPackageBilling>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EcartPackageBillingImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ECARTPACKAGEBILLING = "SELECT ecartPackageBilling FROM EcartPackageBilling ecartPackageBilling";
	private static final String _SQL_SELECT_ECARTPACKAGEBILLING_WHERE = "SELECT ecartPackageBilling FROM EcartPackageBilling ecartPackageBilling WHERE ";
	private static final String _SQL_COUNT_ECARTPACKAGEBILLING = "SELECT COUNT(ecartPackageBilling) FROM EcartPackageBilling ecartPackageBilling";
	private static final String _SQL_COUNT_ECARTPACKAGEBILLING_WHERE = "SELECT COUNT(ecartPackageBilling) FROM EcartPackageBilling ecartPackageBilling WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ecartPackageBilling.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EcartPackageBilling exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EcartPackageBilling exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EcartPackageBillingPersistenceImpl.class);
	private static EcartPackageBilling _nullEcartPackageBilling = new EcartPackageBillingImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EcartPackageBilling> toCacheModel() {
				return _nullEcartPackageBillingCacheModel;
			}
		};

	private static CacheModel<EcartPackageBilling> _nullEcartPackageBillingCacheModel =
		new CacheModel<EcartPackageBilling>() {
			@Override
			public EcartPackageBilling toEntityModel() {
				return _nullEcartPackageBilling;
			}
		};
}