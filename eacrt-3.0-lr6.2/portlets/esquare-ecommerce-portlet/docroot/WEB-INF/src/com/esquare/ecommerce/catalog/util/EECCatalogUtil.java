package com.esquare.ecommerce.catalog.util;

import java.util.List;

import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.UnicodeFormatter;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

public class EECCatalogUtil {
	public static String getSubCatalogList(long groupId, long catalogId,
			PortletURL editURL, PortletURL addCatalogURL, PortletURL deleteURL,
			String ctxPath) throws SystemException {
		StringBuffer sb = new StringBuffer();
		sb.append("<div style='display:block;' id='rowId");
		sb.append(catalogId);
		sb.append("'>");
		getList(groupId, catalogId, editURL, addCatalogURL, deleteURL, ctxPath,
				sb);
		sb.append("</div>");

		return sb.toString();
	}

	private static void getList(long groupId, long catalogId,
			PortletURL editURL, PortletURL addCatalogURL, PortletURL deleteURL,
			String ctxPath, StringBuffer sb) throws SystemException {

		int count = CatalogLocalServiceUtil
				.getCatalogsCount(groupId, catalogId);
		if (count > 0) {
			List list = CatalogLocalServiceUtil.getCatalogs(groupId, catalogId,
					0, count);
			sb.append("<ul>");
			for (int i = 0; i < list.size(); i++) {
				Catalog catalog = (Catalog) list.get(i);
				catalog = catalog.toEscapedModel();
				// editURL.setParameter("catalogId",
				// String.valueOf(catalog.getCatalogId()));
				sb.append("<li class='child'><img src='");
				sb.append(ctxPath);
				sb.append("/images/eec/common/catalog/liback.png' style='padding-left: 12px;padding-top: 9px;'/>");
				sb.append("<div  onmouseover='showActionDiv(\"action");
				sb.append(catalog.getCatalogId());
				sb.append("\");' onmouseout='hideActionDiv(\"action");
				sb.append(catalog.getCatalogId());
				sb.append("\");' class='subs1'>");
				sb.append("<a href='' class='list1'>");
				sb.append(catalog.getName());
				sb.append("</a>");
				sb.append("<div id='action");
				sb.append(catalog.getCatalogId());
				sb.append("' style='float: right;display: none;margin-top: -15px;'><a href='#' style='font-weight:normal;padding-left:7px;' onclick='editCatalog(");
				sb.append(catalog.getCatalogId());
				sb.append(");'><img src='");
				sb.append(ctxPath);
				sb.append("/images/eec/common/catalog/edit.png' title='Edit' /></a> <a href='#' style='font-weight:normal;;padding-left:7px;' onclick='addCatalog(");
				sb.append(catalog.getCatalogId());
				sb.append(");' style='font-weight:normal;'><img src='");
				sb.append(ctxPath);
				sb.append("/images/eec/common/catalog/plus.png' title='Add' /></a> <a href='#'  onclick='confirmDel(");
				sb.append(catalog.getCatalogId());
				sb.append(");' style='font-weight:normal;;padding-left:7px;'><img src='");
				sb.append(ctxPath);
				sb.append("/images/eec/common/catalog/delete.png' title='Delete' /></a></div></div>");

				if (catalog.getParentCatalogId() != 0) {
					getList(groupId, catalog.getCatalogId(), editURL,
							addCatalogURL, deleteURL, ctxPath, sb);
				}
				sb.append("</li>");
			}
			sb.append("</ul>");
		}
	}

	public static void getCatalog(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortalException,
			SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long catalogId = ParamUtil.getLong(renderRequest, "catalogId");

		Catalog catalog = null;

		if ((catalogId > 0)
				&& (catalogId != EECConstants.DEFAULT_PARENT_CATALOG_ID)) {

			catalog = CatalogLocalServiceUtil.getCatalog(catalogId);
		} else {
			/*
			 * ECCPermission.check( themeDisplay.getPermissionChecker(),
			 * themeDisplay.getScopeGroupId(), ActionKeys.VIEW);
			 */
		}

		renderRequest.setAttribute("CATALOG", catalog);
	}

	public static String getSubCatalogPopupList(long groupId, long catalogId,
			String nameSpace) throws SystemException, PortalException {
		StringBuffer sb = new StringBuffer();
		sb.append("<div style='display:block;' id='rowId");
		sb.append(catalogId);
		sb.append("'>");
		getPopupList(groupId, catalogId, nameSpace, sb);
		sb.append("</div>");
		return sb.toString();
	}

	public static void getPopupList(long groupId, long catalogId,
			String nameSpace, StringBuffer sb) throws SystemException,
			PortalException {
		int count = CatalogLocalServiceUtil
				.getCatalogsCount(groupId, catalogId);
		if (count > 0) {
			List list = CatalogLocalServiceUtil.getCatalogs(groupId, catalogId,
					0, count);
			sb.append("<ul id='sitemap'>");
			for (int i = 0; i < list.size(); i++) {
				Catalog catalog = (Catalog) list.get(i);
				catalog = catalog.toEscapedModel();
				sb.append("<li style='background:#F2F2F2;'>");
				sb.append("<div>");
				sb.append("<a href='' onclick='");
				sb.append("return CloseMySelf(\"");
				sb.append(catalog.getCatalogId());
				sb.append("\",\"");
				sb.append(UnicodeFormatter.toString(catalog.getName()));
				sb.append("\"); window.close();");
				sb.append("'>");
				sb.append(catalog.getName());
				sb.append("</a><br/>");
				// if(catalog.getParentCatalogId() != 0)
				// getProductList(groupId,catalog.getCatalogId(),sb);
				sb.append("</div>");
				if (catalog.getParentCatalogId() != 0) {
					getPopupList(groupId, catalog.getCatalogId(), nameSpace, sb);
				}
				sb.append("</li>");
			}
			sb.append("</ul>");
		}
	}

}
