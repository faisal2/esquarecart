package com.esquare.ecommerce.userview.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import com.esquare.ecommerce.NoSuchProductInventoryException;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class UserViewAction extends MVCPortlet {

	public void processEvent(javax.portlet.EventRequest request,
			javax.portlet.EventResponse response)
			throws javax.portlet.PortletException, java.io.IOException {
		Event event = request.getEvent();
		String value = (String) event.getValue();
		String[] values = value.split(StringPool.COMMA);
		PortletSession pSession = request.getPortletSession();
		if (values.length > 0) {
			pSession.setAttribute("EVENT_PRODUCTID", values[0],
					PortletSession.PORTLET_SCOPE);
			pSession.setAttribute("EVENT_CMD", values[1],
					PortletSession.PORTLET_SCOPE);
		}
	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		PortletSession pSession = renderRequest.getPortletSession();
		String eventCmd = (String) pSession.getAttribute("EVENT_CMD",
				PortletSession.PORTLET_SCOPE);
		String tabs = ParamUtil.getString(renderRequest, "tabs1");
		if (Validator.isNotNull(tabs) && tabs.equals("ListView")
				|| tabs.equals("GridView")) {
			viewTemplate = "/html/eec/common/userview/view.jsp";
		} else if (Validator.isNotNull(eventCmd)
				&& eventCmd.equals("detailsView")) {
			viewTemplate = "/html/eec/common/userview/detail_view.jsp";
		} else {
			viewTemplate = "/html/eec/common/userview/view.jsp";
		}
		// pSession.removeAttribute("EVENT_CMD",pSession.PORTLET_SCOPE);
		super.doView(renderRequest, renderResponse);
	}

	public void navigationProcess(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, SystemException {
		String parentCatalogId = ParamUtil.getString(actionRequest,
				"parentCatalogId");
		String productName = ParamUtil.getString(actionRequest, "productName");
		String cmd = ParamUtil.getString(actionRequest, "cmd");
		String CurProductId = ParamUtil
				.getString(actionRequest, "CurProductId");
		PortletSession psession = actionRequest.getPortletSession();
		psession.setAttribute("parentCatalogId", parentCatalogId,
				PortletSession.APPLICATION_SCOPE);
		psession.setAttribute("productName", productName,
				PortletSession.APPLICATION_SCOPE);
		psession.setAttribute("CMD", cmd, PortletSession.PORTLET_SCOPE);
		QName qName = new QName("http://liferay.com", "productId", "x");
		cmd = StringPool.BLANK + StringPool.COMMA + cmd;
		actionResponse.setEvent(qName, cmd);
		psession.setAttribute("CurProductId", CurProductId,
				PortletSession.APPLICATION_SCOPE);
		psession.removeAttribute("SORT_BY_NAME&PRICE",PortletSession.PORTLET_SCOPE);
		psession.removeAttribute("CATALOGIDS", PortletSession.PORTLET_SCOPE);
		psession.removeAttribute("CHKBOX_PRICERANGE",
				PortletSession.PORTLET_SCOPE);
		psession.removeAttribute("SLIDER_PRICERANGE",
				PortletSession.PORTLET_SCOPE);
		actionResponse.setRenderParameter("jspPage",
				"/html/eec/common/userview/view.jsp");

	}

	@Override
	public void serveResource(ResourceRequest p_request,
			ResourceResponse p_response) throws PortletException, IOException {
		String cmd = p_request.getParameter(EECConstants.CMD1);
		PortletSession pSession = p_request.getPortletSession();
		if (cmd.equals(EECConstants.WISHLIST_UPDATE)) {
			long productInventoryId = ParamUtil.getLong(p_request,
					"curProductId");
			EECUtil.updateWishList(p_request, productInventoryId);
		} else if (cmd.equals("BRAND")) {

			String brandCatalogId = (String) pSession.getAttribute(
					"BRAND_CATALOGID", PortletSession.PORTLET_SCOPE);
			String curcatalogId = ParamUtil
					.getString(p_request, "curCatalogId");
			String catalogIds = StringPool.BLANK;
			StringBuffer sb = new StringBuffer();
			if (Validator.isNull(brandCatalogId)) {
				sb.append(curcatalogId);
				sb.append(StringPool.COMMA);
				catalogIds = sb.toString().substring(0,
						sb.toString().length() - 1);
			} else {
				catalogIds = deleteSelectedBrand(brandCatalogId, curcatalogId);
			}
			pSession.setAttribute("CATALOGIDS", String.valueOf(catalogIds),
					PortletSession.PORTLET_SCOPE);
		} else if (cmd.equals("SORTBYNAME&PRICE")) {
			String sortByNameandPrice = ParamUtil.getString(p_request,
					"sortByNameandPrice");
			pSession.setAttribute("SORT_BY_NAME&PRICE", sortByNameandPrice,
					PortletSession.PORTLET_SCOPE);
		} else if (cmd.equals("CMD_CHKBOX_PRICE")) {
			String range = p_request.getParameter("priceRange");
			String multiplePriceRange = (String) pSession.getAttribute(
					"MULTIPLE_PRICE_RANGES", PortletSession.PORTLET_SCOPE);
			StringBuffer buffer = new StringBuffer();
			String priceRanges = StringPool.BLANK;
			if (Validator.isNull(multiplePriceRange)) {
				buffer.append(String.valueOf(range));
				buffer.append(StringPool.COMMA);
				priceRanges = buffer.toString().substring(0,
						buffer.toString().length() - 1);
			} else {
				priceRanges = deleteSelectedRange(multiplePriceRange,
						String.valueOf(range));
			}
			pSession.setAttribute("CHKBOX_PRICERANGE", priceRanges,
					PortletSession.PORTLET_SCOPE);
		} else if (cmd.equals("CMD_SLIDER_PRICE")) {
			String range = p_request.getParameter("priceRange");
			pSession.setAttribute("SLIDER_PRICERANGE", range,
					PortletSession.PORTLET_SCOPE);
		}
	}

	private String deleteSelectedRange(String multiplePriceRange,
			String currange) {
		if (multiplePriceRange.contains(currange)) {
			String[] inventoryIds = multiplePriceRange.split(StringPool.AT);
			List<String> list = new ArrayList<String>(
					Arrays.asList(inventoryIds));
			list.remove(currange);
			inventoryIds = list.toArray(new String[inventoryIds.length]);
			currange = StringPool.BLANK;
			for (String inventoryId : inventoryIds) {
				if (Validator.isNotNull(inventoryId)) {
					currange += inventoryId + StringPool.AT;
				}
			}
			if (currange.length() > 0)
				currange = currange.substring(0, currange.length() - 1);
		} else {
			currange += StringPool.AT + multiplePriceRange;
		}
		return currange;
	}

	private String deleteSelectedBrand(String brandCatalogId,
			String curcatalogId) {
		if (brandCatalogId.contains(curcatalogId)) {
			// for old logic have a look at version 302
			curcatalogId = brandCatalogId.replace(curcatalogId, "");

			if (Validator.isNotNull(curcatalogId)
					&& curcatalogId.substring(0, 1).contains(",")) {
				curcatalogId = curcatalogId.substring(1);
			}
			if (Validator.isNotNull(curcatalogId)
					&& curcatalogId.substring(curcatalogId.length() - 1)
							.contains(",")) {
				curcatalogId = curcatalogId.substring(0,
						curcatalogId.length() - 1);
			}
			curcatalogId = curcatalogId.replaceAll("[,]{2,}", ",");
		} else {
			curcatalogId += StringPool.COMMA + brandCatalogId;
		}
		return curcatalogId;
		// return curcatalogId.toString().substring(0,
		// curcatalogId.toString().length()-1);
	}

	public void redirectToDetailsPage(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		String curProductId = actionRequest.getParameter("curProductId");
		QName qName = new QName("http://liferay.com", "productId", "x");
		actionResponse.setEvent(qName, curProductId + "," + "detailsView");
		/*
		 * PortletURL redirectURL = EECUtil.getPortletURL(actionRequest,
		 * EECConstants.USER_VIEW_PORTLET_NAME, themeDisplay.getPlid(),
		 * ActionRequest.RENDER_PHASE,
		 * "/html/eec/common/userview/detail_view.jsp", null,
		 * LiferayWindowState.NORMAL, LiferayPortletMode.VIEW);
		 * redirectURL.setParameter("curProductId", curProductId);
		 * actionResponse.sendRedirect(redirectURL.toString());
		 */
	}

	public void searchProduct(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, SystemException,
			NoSuchProductInventoryException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		PortletSession psession = actionRequest.getPortletSession();
		String productName = ParamUtil.getString(actionRequest, "productName");
		if (Validator.isNotNull(productName)) {
			ProductDetails productDetails = ProductDetailsLocalServiceUtil
					.fetchByName(themeDisplay.getCompanyId(), productName);
			if (Validator.isNotNull(productDetails)) {
				ProductInventory inventory = ProductInventoryLocalServiceUtil
						.findByProductDetailsId(productDetails
								.getProductDetailsId());
				psession.setAttribute("EVENT_PRODUCTID",
						String.valueOf(inventory.getInventoryId()),
						PortletSession.PORTLET_SCOPE);
				psession.setAttribute("EVENT_CMD", "detailsView",
						PortletSession.PORTLET_SCOPE);
			}
		}
	}

	private static Log _log = LogFactoryUtil.getLog(UserViewAction.class);
}
