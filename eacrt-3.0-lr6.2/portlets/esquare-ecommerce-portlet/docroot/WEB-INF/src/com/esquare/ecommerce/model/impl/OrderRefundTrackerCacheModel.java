/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.OrderRefundTracker;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing OrderRefundTracker in entity cache.
 *
 * @author Esquare
 * @see OrderRefundTracker
 * @generated
 */
public class OrderRefundTrackerCacheModel implements CacheModel<OrderRefundTracker>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{refundTrackerId=");
		sb.append(refundTrackerId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", referenceNumber=");
		sb.append(referenceNumber);
		sb.append(", amountClaimed=");
		sb.append(amountClaimed);
		sb.append(", paymentStatus=");
		sb.append(paymentStatus);
		sb.append(", paymentMode=");
		sb.append(paymentMode);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", expectedDeliveryDate=");
		sb.append(expectedDeliveryDate);
		sb.append(", comments=");
		sb.append(comments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public OrderRefundTracker toEntityModel() {
		OrderRefundTrackerImpl orderRefundTrackerImpl = new OrderRefundTrackerImpl();

		orderRefundTrackerImpl.setRefundTrackerId(refundTrackerId);
		orderRefundTrackerImpl.setCompanyId(companyId);
		orderRefundTrackerImpl.setUserId(userId);
		orderRefundTrackerImpl.setReferenceNumber(referenceNumber);
		orderRefundTrackerImpl.setAmountClaimed(amountClaimed);
		orderRefundTrackerImpl.setPaymentStatus(paymentStatus);

		if (paymentMode == null) {
			orderRefundTrackerImpl.setPaymentMode(StringPool.BLANK);
		}
		else {
			orderRefundTrackerImpl.setPaymentMode(paymentMode);
		}

		if (createDate == Long.MIN_VALUE) {
			orderRefundTrackerImpl.setCreateDate(null);
		}
		else {
			orderRefundTrackerImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			orderRefundTrackerImpl.setModifiedDate(null);
		}
		else {
			orderRefundTrackerImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (expectedDeliveryDate == Long.MIN_VALUE) {
			orderRefundTrackerImpl.setExpectedDeliveryDate(null);
		}
		else {
			orderRefundTrackerImpl.setExpectedDeliveryDate(new Date(
					expectedDeliveryDate));
		}

		if (comments == null) {
			orderRefundTrackerImpl.setComments(StringPool.BLANK);
		}
		else {
			orderRefundTrackerImpl.setComments(comments);
		}

		orderRefundTrackerImpl.resetOriginalValues();

		return orderRefundTrackerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		refundTrackerId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		referenceNumber = objectInput.readLong();
		amountClaimed = objectInput.readDouble();
		paymentStatus = objectInput.readBoolean();
		paymentMode = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		expectedDeliveryDate = objectInput.readLong();
		comments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(refundTrackerId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(referenceNumber);
		objectOutput.writeDouble(amountClaimed);
		objectOutput.writeBoolean(paymentStatus);

		if (paymentMode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(paymentMode);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(expectedDeliveryDate);

		if (comments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comments);
		}
	}

	public long refundTrackerId;
	public long companyId;
	public long userId;
	public long referenceNumber;
	public double amountClaimed;
	public boolean paymentStatus;
	public String paymentMode;
	public long createDate;
	public long modifiedDate;
	public long expectedDeliveryDate;
	public String comments;
}