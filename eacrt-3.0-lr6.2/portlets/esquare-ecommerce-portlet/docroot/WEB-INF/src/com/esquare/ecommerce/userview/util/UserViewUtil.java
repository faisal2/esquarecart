package com.esquare.ecommerce.userview.util;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.servlet.http.Cookie;

import com.esquare.ecommerce.NoSuchProductInventoryException;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.EcustomField;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.ShippingRegion;
import com.esquare.ecommerce.model.WishListItem;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductImagesLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMTemplate;
import com.liferay.portlet.dynamicdatamapping.service.DDMTemplateLocalServiceUtil;

public class UserViewUtil {
	private static Date last_createdDate = null;
	static int total = 0;

	public static String getProductsContent(long groupId, long companyId,
			String pathThemeImages, int viewCount, String type,
			long parentCatalogId, PortletURL detailURL, PortletURL cartViewURL,
			PortletURL userViewURL, String ctxPath, String className,
			long productId, PortletRequest portletRequest)
			throws NumberFormatException, PortalException, SystemException {
		StringBuffer sb = new StringBuffer();
		if (viewCount > 0) {
			sb.append(getHeader(groupId, type, parentCatalogId, userViewURL,
					ctxPath, EECConstants.STYLE_DISPLAY_BLOCK));
		}
		sb.append(getBody(groupId, companyId, pathThemeImages, viewCount, type,
				parentCatalogId, detailURL, cartViewURL, userViewURL, ctxPath,
				className, null, productId, StringPool.BLANK, portletRequest));
		if (viewCount > 0) {
			sb.append(getFooter(groupId, type));
		}
		return sb.toString();

	}

	public static String getHeaderTitleName(String type, long parentCatalogId,
			PortletURL userViewURL) throws NumberFormatException,
			PortalException, SystemException {

		String titleName = StringPool.BLANK;
		long catalogId = 0;
		if (type.equals(EECConstants.NEWLAUNCH)) {
			titleName = "NEW LAUNCH";
			userViewURL.setParameter(Constants.CMD, type);
		} else if (type.equals(EECConstants.HOTDEAL)) {
			titleName = "SPECIAL OFFER";
			userViewURL.setParameter(Constants.CMD, type);
		} else if (type.equals(EECConstants.BESTSELLER)) {
			titleName = "BEST SELLER";
			userViewURL.setParameter(Constants.CMD, type);
		} else if (type.equals(EECConstants.SIMILAR_PRODUCTS)) {
			titleName = "SIMILAR PRODUCTS";
			userViewURL.setParameter(Constants.CMD, type);
		} else if (type.equals(EECConstants.RELATED_PRODUCT)) {
			titleName = "RELATED PRODUCTS";
			userViewURL.setParameter(Constants.CMD, type);
		} else if (type.equals(PortletPropsValues.WISHLIST_TITLE_NAME)) {
			titleName = PortletPropsValues.WISHLIST_TITLE_NAME;
		} else if (type.equals(PortletPropsValues.ORDER_TRACKER_TITLE_NAME)) {
			titleName = PortletPropsValues.ORDER_TRACKER_TITLE_NAME;
		} else if (type.equals(PortletPropsValues.WALLET_USED_AMOUNT)) {
			titleName = PortletPropsValues.WALLET_USED_AMOUNT;
		} else if (type.equals(PortletPropsValues.MY_WALLET_TRACKER)) {
			titleName = PortletPropsValues.MY_WALLET_TRACKER;
		} else {
			if (parentCatalogId != 0) {
				String[] types = type.split(",");
				if (types.length == 1) {
					titleName = CatalogLocalServiceUtil.getCatalog(
							Long.parseLong(type)).getName();
					catalogId = CatalogLocalServiceUtil.getCatalog(
							Long.parseLong(type)).getCatalogId();
					// userViewURL.setParameter("catalogId",String.valueOf(catalogId));
					userViewURL.setParameter("parentCatalogId",
							String.valueOf(catalogId));
				} else {
					Catalog catalog = CatalogLocalServiceUtil
							.getCatalog(parentCatalogId);
					titleName = catalog.getName();
					userViewURL.setParameter("parentCatalogId",
							String.valueOf(catalog.getCatalogId()));
				}
			}
		}
		return titleName;

	}

	public static String getHeader(long groupId, String type,
			long parentCatalogId, PortletURL userViewURL, String ctxPath,
			String enableViewall) throws NumberFormatException,
			PortalException, SystemException {

		String templateHeader = getTemplate(groupId,
				PortletPropsValues.DEFAULT_HEADER_TEMPLATE);
		String[] oldSubs = { "[$HEDER_TITLE$]", "[VIEW_ALL_URL]",
				"[$CTXPATH$]", "[$VIEWALL_BLOCK_NONE$]" };
		String[] newSubs = {
				getHeaderTitleName(type, parentCatalogId, userViewURL),
				userViewURL.toString(), ctxPath, enableViewall };
		return StringUtil.replace(templateHeader, oldSubs, newSubs).toString();
	}

	public static String getBody(long groupId, long companyId,
			String pathThemeImages, int viewCount, String type,
			long parentCatalogId, PortletURL detailURL, PortletURL cartViewURL,
			PortletURL userViewURL, String ctxPath, String className,
			List<ProductInventory> filterContentList, long productId,
			String wishListProductIds, PortletRequest portletRequest)
			throws NoSuchProductInventoryException, SystemException {

		String bodyContent = StringPool.BLANK;
		String templateName = StringPool.BLANK;
		List<ProductInventory> productInventories = null;
		if (className.equals(PortletPropsValues.EEC_HOME_GRIDCLASS)) {
			templateName = PortletPropsValues.HOMEPAGE_GRID_TEMPLATE;
		} else if (className.equals(PortletPropsValues.EEC_GRIDCLASS)) {
			templateName = PortletPropsValues.DEFAULT_GRID_TEMPLATE;
		} else if (className.equals(PortletPropsValues.EEC_LISTCLASS)) {
			templateName = PortletPropsValues.DEFAULT_LIST_TEMPLATE;
		} else {
			templateName = PortletPropsValues.DEFAULT_DETAIL_TEMPLATE;
		}
		if (Validator.isNotNull(filterContentList)) {
			productInventories = filterContentList;
		} else {
			productInventories = (List<ProductInventory>) getProductsList(
					companyId, parentCatalogId, viewCount, type, productId);
		}
		StringBuffer sb = new StringBuffer();
		bodyContent = getTemplate(groupId, templateName);
		if (viewCount > 0) {
			for (int i = 0; i < viewCount; i++) {
				if (i < productInventories.size()
						&& productId != productInventories.get(i)
								.getProductDetailsId()) {
					sb.append(replacedContent(companyId, bodyContent, type,
							productInventories.get(i), pathThemeImages,
							detailURL, cartViewURL, userViewURL, ctxPath,
							className, StringPool.BLANK, wishListProductIds,
							portletRequest));
				} else {
					sb.append("<li class=");
					sb.append(className);
					sb.append(" id='hompage-gridlist' >");
					sb.append("<table>");
					sb.append("<tr><td'><div> <img src=\"");
					sb.append(pathThemeImages);
					sb.append("/eec/no_image.gif\" id='table1'/></div></td></tr></table></li>");
				}
			}
		} else if (productInventories.size() > 0 && viewCount == 0) {
			for (int i = 0; i < productInventories.size(); i++) {
				if (productId != productInventories.get(i)
						.getProductDetailsId())
					sb.append(replacedContent(companyId, bodyContent, type,
							productInventories.get(i), pathThemeImages,
							detailURL, cartViewURL, userViewURL, ctxPath,
							className, StringPool.BLANK, wishListProductIds,
							portletRequest));
			}
		}
		/*
		 * if(viewCount > 0){
		 * 
		 * bodyContent = getTemplate(groupId, templateName); for(int i =0; i <
		 * viewCount; i++) {
		 * 
		 * if(i < productInventories.size() &&
		 * productId!=productInventories.get(i).getProductDetailsId()) {
		 * sb.append(replacedContent(bodyContent,type,
		 * productInventories.get(i),pathThemeImages,detailURL, userViewURL,
		 * ctxPath,className,StringPool.BLANK)); } else { sb.append(
		 * "<div style='width:246px;float:right;margin-top:-14px;'> <img src=\""
		 * +pathThemeImages+
		 * "/eec/no_image.gif\" style='border:1px solid #D3D3D3;' /></div>"); }
		 * } }
		 */else {
			sb.append("<span style='color: #EC4D25;font-size: 14px;font-weight: bold;'>There are No Products...</span>");
		}

		return sb.toString();
	}

	public static String replacedContent(long companyId, String bodyContent,
			String type, ProductInventory productInventory,
			String pathThemeImages, PortletURL detailURL,
			PortletURL cartViewURL, PortletURL userViewURL, String ctxPath,
			String className, String namespace, String wishListProductIds,
			PortletRequest portletRequest) {
		try {
			// ProductInventory productInventory =
			// ProductInventoryLocalServiceUtil.findByProductDetailsId(product.getProductDetailsId());
			List<ProductImages> productImages = ProductImagesLocalServiceUtil
					.findByInventoryId(productInventory.getInventoryId());
			FileEntry dlFileEntry = DLAppServiceUtil.getFileEntry(productImages.get(0)
					.getImageId());
			String imageURL = "/documents/" + dlFileEntry.getGroupId() + "/"
					+ dlFileEntry.getFolderId() + "/" + dlFileEntry.getTitle()
					+ "/" + dlFileEntry.getUuid();
			ProductDetails productDetail = ProductDetailsLocalServiceUtil
					.getProductDetails(productInventory.getProductDetailsId());

			String isTaxes = (productInventory.getTaxes()) ? StringPool.BLANK
					: PortletPropsValues.DETAILSVIEW_TAXES_CONTENT;
			String isFreeShipping = (productInventory.getFreeShipping()) ? PortletPropsValues.DETAILSVIEW_FREESHIPPING_CONTENT
					: StringPool.BLANK;

			double discountPrice = productInventory.getPrice()
					* (productInventory.getDiscount() / 100);
			double discountpercentage = (productInventory.getDiscount() / 100);
			double listOrginalRates = productInventory.getPrice()
					- discountPrice;
			detailURL.setParameter("curProductId",
					String.valueOf(productInventory.getInventoryId()));
			if (Validator.isNotNull(cartViewURL)) {
				cartViewURL.setParameter("productInventoryIds",
						String.valueOf(productInventory.getInventoryId()));
			}
			String BuythisButtonURL = "javascript:addToCartPage("
					+ String.valueOf(productInventory.getInventoryId()) + ")";
			String iconURL = StringPool.BLANK;
			String isStrikrPrice = EECConstants.STYLE_DISPLAY_NONE;

			if (Validator.isNotNull(type)) {
				detailURL.setParameter("type", type);
			}
			isStrikrPrice = (productInventory.getDiscount() > 0) ? EECConstants.STYLE_DISPLAY_INLINE_BLOCK
					: EECConstants.STYLE_DISPLAY_NONE;
			if (isStrikrPrice.equals(EECConstants.STYLE_DISPLAY_NONE)) {

			}
			iconURL = getIconImageURL(type, productInventory, pathThemeImages);
			String discount = (iconURL.contains(EECConstants.DISCOUNT)) ? String
					.valueOf(productInventory.getDiscount()) + "%"
					: StringPool.BLANK;
			String stockStatus = (EECUtil.isOutOfStock(productInventory)) ? EECConstants.INSTOCK
					: EECConstants.OUTOFSTOCK;
			String isOutofStockpurchase = (!EECUtil.isInStock(productInventory)) ? EECConstants.STYLE_DISPLAY_NONE
					: EECConstants.STYLE_DISPLAY_BLOCK;
			String addtoCompare = StringPool.BLANK;
			/*
			 * if (Validator.isNull(type) &&
			 * Validator.isNotNull(productInventory .getCustomFieldRecId())) {
			 * addtoCompare = "<input type='checkbox' id=\"comparechkbox_" +
			 * productInventory.getInventoryId() +
			 * "\" name='comparechkbox' onclick=\"javascript:addProducToCompare("
			 * + String.valueOf(productInventory.getInventoryId()) + "," +
			 * String.valueOf(productInventory.getCustomFieldRecId()) +
			 * ");\" /> Add To Compare"; }
			 */

			String name = StringPool.BLANK;
			String viewmore = "<span id='popup'> <span id='view-more'> View more</span> <div id='element_to_pop_up'> <a class='b-close'>x</a>"
					+ String.valueOf(productDetail.getDescription())
					+ "</div></span>";

			String description = StringPool.BLANK;

			name = productDetail.getName();
			description = productDetail.getDescription();
			String listDescription = productDetail.getDescription();
			String detailsDescribtion = productDetail.getDescription();

			int nameLength = name.length();
			int length = description.length();
			if (className.equals("DETAILVIEW_CONTENT")) {
				
					detailsDescribtion = detailsDescribtion;
					
				
			} else if (className.equals(PortletPropsValues.EEC_LISTCLASS)) {
				if (nameLength > 65) {
					nameLength = 65;
					name = name.substring(0, nameLength) + "...";
				}
				if (length > 150) {
					length = 150;
					listDescription = listDescription.substring(0, length)
							+ "...";
				}
			} else if (className.equals(PortletPropsValues.EEC_WISH_LIST_CLASS)) {
				if (nameLength > 30) {
					nameLength = 30;
					name = name.substring(0, nameLength) + "...";
				}
				if (length > 65) {
					length = 65;
					listDescription = listDescription.substring(0, length - 1)
							+ "...";
				}
			} else if (className.equals(PortletPropsValues.EEC_GRIDCLASS)) {
				if (nameLength > 25) {
					nameLength = 25;
					name = name.substring(0, nameLength) + "...";
				}

				if (length > 16) {
					length = 16;
					description = description.substring(0, length) + "...";
				}

			} else if (className.equals(PortletPropsValues.EEC_HOME_GRIDCLASS)) {
				if (nameLength > 25) {
					nameLength = 25;
					name = name.substring(0, nameLength) + "...";
				}

				if (length > 16) {
					length = 16;
					description = description.substring(0, length) + "...";
				}
			} else {
				if (nameLength > 30) {
					nameLength = 30;
					name = name.substring(0, nameLength) + "...";
				}

				if (length > 60) {
					length = 60;
					description = description.substring(0, length) + ".......";
				}
			}

			/*
			 * if(EECUtil.isInStock(preoductDetail)){
			 * stockStatus=EECConstants.INSTOCK; }else{
			 * stockStatus=EECConstants.OUTOFSTOCK; }
			 */

			NumberFormat currencyFormat = EECUtil.getCurrencyFormat(companyId);

			String autoGenIds = String.valueOf(productInventory
					.getInventoryId());
			String addtoCompareURL = "javascript:addProducToCompare("
					+ String.valueOf(productInventory.getInventoryId()) + ","
					+ String.valueOf(productInventory.getCustomFieldRecId())
					+ ")";

			String addedtoWishList = EECConstants.STYLE_DISPLAY_NONE;
			String addtoMywishList = EECConstants.STYLE_DISPLAY_BLOCK;
			String addToWishlistURL = StringPool.BLANK;

			// ProductInventory productInventory =
			// ProductInventoryLocalServiceUtil.findByProductDetailsId(productId);

			addToWishlistURL = "javascript:wishListLogin(" + autoGenIds + ")";
			if (Validator.isNotNull(wishListProductIds)) {

				String[] wishlistProductId = wishListProductIds
						.split(StringPool.COMMA);
				for (int i = 0; i < wishlistProductId.length; i++) {
					if (wishlistProductId[i].equals(String.valueOf(autoGenIds))) {
						addedtoWishList = EECConstants.STYLE_DISPLAY_BLOCK;
						addtoMywishList = EECConstants.STYLE_DISPLAY_NONE;
						addToWishlistURL = "javascript:addToWishListPage("
								+ StringPool.BLANK + ")";
						break;
					}
				}

			}
			String addToCompareDivStyle = EECConstants.STYLE_DISPLAY_NONE;
			String addedToCompareDivStyle = EECConstants.STYLE_DISPLAY_NONE;
			String compareInventoryId = null;
			Cookie cookie = EECUtil.getCookie(portletRequest,
					"compare-inventory-ids");
			if (Validator.isNotNull(cookie))
				compareInventoryId = cookie.getValue();
			if (Validator.isNotNull(compareInventoryId)) {
				Set<String> compareInventoryIDSet = new HashSet<String>(
						Arrays.asList(compareInventoryId
								.split(StringPool.COMMA)));
				if (compareInventoryIDSet.contains(String
						.valueOf(productInventory.getInventoryId()))) {
					addedToCompareDivStyle = EECConstants.STYLE_DISPLAY_BLOCK;
				} else {
					addToCompareDivStyle = EECConstants.STYLE_DISPLAY_BLOCK;
				}

			} else {
				addToCompareDivStyle = EECConstants.STYLE_DISPLAY_BLOCK;
			}
			boolean isInStock=EECUtil.isInStock(productInventory);

			String[] oldSubs = { "[$ICON_URL$]", "[$IMAGE_URL$]",
					"[$PRODUCT_NAME$]", "[$PRODUCTNAME_TOOLTIP$]",
					"[$DESCRIPTION$]", "[$LIST_DESCRIPTION$]",
					"[$DETAILSDESCRIPTION$]", "[$PRICE$]",
					"[$ORIGINAL_PRICE$]", "[$QUANTITY$]", "[$DISCOUNT$]",
					"[$DISCOUNT_PERCENTAGE$]", "[$DISCOUNT_PRICE$]", "[$SKU$]",
					"[$ISTAXES$]", "[$ISFREE_SHIPPING$]",
					"[$PRODUCT_DEAILS_ID$]", "[$DETAIL_URL$]",
					"[$CARTVIEW_URL$]", "[$USER_VIEW_URL$]", "[$CTXPATH$]",
					"[$LIST_GRID_STYLE_CLASS$]", "[$PATHTHEMEIMAGES$]",
					"[$BUYNOW$]", "[$STOCK_STATUS$]",
					"[$ISOUTSTOCK_PURCHASE$]", "[$STRIKE_PRICE_STYLE$]",
					"[$ADDPRODUCT_TO_COMPARE$]", "[$AUTOGENIDS$]",
					"[$WISHLIST_URL$]", "[$ADDED_WISHLIST$]",
					"[$ADD_MYWISHLIST$]", "[$ADDTOCOMPARE_URL$]",
					"[$ADD_TO_COMPARE_STYLE$]", "[$ADDED_TO_COMPARE_STYLE$]",
					"[$ADDTOCOMPARE_DIV_ID$]", "[$ADDEDTOCOMPARE_DIV_ID$]",
					"[$WISHLIST_ADD_TO_CART_STYLE$]", "[$LIST_ADD_TO_CART$]","[$LIST_OUT_OF_STOCK$]" };
			String[] newSubs = {
					iconURL,
					imageURL,
					name,
					String.valueOf(productDetail.getName()),
					description,
					listDescription,
					detailsDescribtion,
					String.valueOf(currencyFormat.format(productInventory
							.getPrice())),
					String.valueOf(currencyFormat.format(listOrginalRates)),
					String.valueOf(productInventory.getQuantity()),
					discount,
					String.valueOf(discountpercentage),
					String.valueOf(currencyFormat.format(discountPrice)),
					String.valueOf(productInventory.getSku()),
					isTaxes,
					isFreeShipping,
					String.valueOf(productInventory.getProductDetailsId()),
					detailURL.toString(),
					Validator.isNotNull(cartViewURL)
							&& isInStock ? cartViewURL
							.toString() : "#\" onclick=\"return false",
					userViewURL.toString(),
					ctxPath,
					className,
					pathThemeImages,
					BuythisButtonURL,
					stockStatus,
					isOutofStockpurchase,
					isStrikrPrice,
					addtoCompare,
					autoGenIds,
					addToWishlistURL,
					addedtoWishList,
					addtoMywishList,
					addtoCompareURL,
					addToCompareDivStyle,
					addedToCompareDivStyle,
					PortletPropsValues.DETAILS_ADD_TO_COMPARE_DIV_ID,
					PortletPropsValues.DETAILS_ADDED_TO_COMPARE_DIV_ID,
					(isInStock) ? EECConstants.STYLE_DISPLAY_BLOCK
							: EECConstants.STYLE_DISPLAY_NONE,
					(isInStock) ? EECConstants.STYLE_DISPLAY_BLOCK
							: EECConstants.STYLE_DISPLAY_NONE,
							(!isInStock) ? EECConstants.STYLE_DISPLAY_BLOCK
									: EECConstants.STYLE_DISPLAY_NONE };
			bodyContent = StringUtil.replace(bodyContent, oldSubs, newSubs);
		} catch (PortalException e) {
			_log.info(e.getClass());
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
			_log.info(e.getClass());
		}
		return bodyContent;
	}

	private static String getIconImageURL(String type,
			ProductInventory productInventory, String pathThemeImages) {
		String iconURL = StringPool.BLANK;
		if (Validator.isNotNull(type) && type.equals(EECConstants.NEWLAUNCH)) {
			iconURL = PortletPropsValues.EEC_NEWLAUNCH_CLASS;
		} else if (Validator.isNotNull(type)
				&& type.equals(EECConstants.HOTDEAL)) {
			iconURL = PortletPropsValues.EEC_DISCOUNT_CLASS;
		} else if (Validator.isNotNull(type)
				&& type.equals(EECConstants.BESTSELLER)) {
			iconURL = PortletPropsValues.EEC_BESTSELLER_CLASS;
		} else {
			if (productInventory.getDiscount() > 0.0) {
				iconURL = PortletPropsValues.EEC_DISCOUNT_CLASS;
			} else if (Validator.isNotNull(last_createdDate)
					&& productInventory.getCreateDate().after(last_createdDate)) {
				iconURL = PortletPropsValues.EEC_NEWLAUNCH_CLASS;
			} else {
				iconURL = StringPool.BLANK;
			}
		}
		// iconURL =StringPool.BLANK;
		return iconURL;
	}

	@SuppressWarnings("unchecked")
	public static List<ProductInventory> getProductsList(long companyId,
			long parentCatalogId, int viewCount, String type, long productId)
			throws NoSuchProductInventoryException, SystemException {
		String sortBy = StringPool.BLANK;
		String subcatalogIds = StringPool.BLANK;
		boolean isOther = false;
		boolean isSmilar = false;
		boolean isRelated = false;
		StringBuffer sb = new StringBuffer();
		if (Validator.isNotNull(type)) {
			if (type.equals(EECConstants.NEWLAUNCH)) {
				sortBy = "createDate";
			} else if (type.equals(EECConstants.HOTDEAL)) {
				sortBy = EECConstants.DISCOUNT;
			} else if (type.equals(EECConstants.BESTSELLER)) {
				// Best seller logic
			} else if (type.equals(EECConstants.SIMILAR_PRODUCTS)) {
				isSmilar = true;
			} else if (type.equals(EECConstants.RELATED_PRODUCT)) {
				isRelated = true;
			} else {
				isOther = true;
				String[] catalogIds = type.split(",");
				for (String catalogId : catalogIds)
					subcatalogIds = getChildCatalogIds(companyId,
							Long.parseLong(catalogId), sb);
			}
		} else {
			isOther = true;
			subcatalogIds = getChildCatalogIds(companyId, parentCatalogId, sb);
		}
		List<ProductInventory> newList = null;
		// Projection projection =
		// ProjectionFactoryUtil.groupProperty("productDetailsId");

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
				.forClass(ProductInventory.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("visibility", true));
		// dynamicQuery.setProjection(projection);

		if (sortBy.equals("createDate")) {
			try {
				dynamicQuery.addOrder(OrderFactoryUtil.desc(sortBy));
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Calendar calendar = Calendar.getInstance();
				// calendar.setTime(productInventory.getCreateDate());
				calendar.setTime(new Date());
				calendar.add(Calendar.MONTH,
						-PortletPropsValues.NEW_LAUNCH_MONTH_DIFF);
				last_createdDate = df.parse(df.format(calendar.getTime()));
				dynamicQuery.add(RestrictionsFactoryUtil.between("createDate",
						last_createdDate, new Date()));
			} catch (Exception e) {
				_log.info(e.getClass());
			}
		} else if (sortBy.equals(EECConstants.DISCOUNT)) {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(sortBy));
			dynamicQuery.add(RestrictionsFactoryUtil.gt("discount", 0.0));
			dynamicQuery.addOrder(OrderFactoryUtil.desc("createDate"));
		} else if (isSmilar) {
			dynamicQuery
					.add(RestrictionsFactoryUtil.between("price", min, max));
		} else if (isRelated) {
			dynamicQuery.add(RestrictionsFactoryUtil.like("limitProducts", "%"
					+ productId + "%"));
		}

		try {

			if (isOther) {
				newList = ProductInventoryLocalServiceUtil.getProductList(
						subcatalogIds, viewCount);
			} else if (viewCount == 0) {
				newList = ProductInventoryLocalServiceUtil.dynamicQuery(
						dynamicQuery, 0, ProductInventoryLocalServiceUtil
								.getProductInventoriesCount());
			} else {
				newList = ProductInventoryLocalServiceUtil.dynamicQuery(
						dynamicQuery, 0, viewCount);
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		return newList;
	}

	public static String getChildCatalogIds(long companyId,
			long parentCatalogId, StringBuffer catalogIds) {

		List<Catalog> catalogList = new ArrayList<Catalog>();
		try {
			catalogList = CatalogLocalServiceUtil.getCatalogs(companyId,
					parentCatalogId);
			catalogIds.append(parentCatalogId + StringPool.COMMA);
			if (catalogList.size() != 0) {
				for (Catalog catalog : catalogList) {
					if (catalog.getParentCatalogId() > 0) {
						getChildCatalogIds(companyId, catalog.getCatalogId(),
								catalogIds);
					} else {
						catalogIds.append(catalog.getCatalogId()
								+ StringPool.COMMA);
					}
				}
			} /*
			 * else { catalogIds.append(parentCatalogId + StringPool.COMMA); }
			 */
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		return catalogIds.toString().substring(0,
				catalogIds.toString().length() - 1);
	}

	public static String getFooter(long groupId, String type) {

		return getTemplate(groupId, PortletPropsValues.DEFAULT_FOOTER_TEMPLATE);

	}

	public static String getTemplate(long groupId, String templateName) {

		String tempContent = StringPool.BLANK;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
					.forClass(DDMTemplate.class);
			dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
			dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%"
					+ templateName + "%"));
			@SuppressWarnings("unchecked")
			List<DDMTemplate> templateList = (List<DDMTemplate>) DDMTemplateLocalServiceUtil
					.dynamicQuery(dynamicQuery);

			if (Validator.isNotNull(templateList)) {
				tempContent = templateList.get(0).getScript();
			}

		} catch (Exception e) {
		}
		return tempContent;
	}

	public static String getDetailviewTemplateContent(long groupId,
			long companyId, long productInventoryId, String type,
			String pathThemeImages, PortletURL detailURL,
			PortletURL cartViewURL, PortletURL wishlistURL, String ctxPath,
			String templateName, String wishListProductIds, String shopNowURL,
			PortletRequest portletRequest) throws SystemException,
			PortalException {
		StringBuffer sb = new StringBuffer();
		String bodyContent = StringPool.BLANK;
		String addedtoWishList = EECConstants.STYLE_DISPLAY_NONE;
		String addtoMywishList = EECConstants.STYLE_DISPLAY_BLOCK;
		String addToWishlistURL = StringPool.BLANK;

		// ProductInventory productInventory =
		// ProductInventoryLocalServiceUtil.findByProductDetailsId(productId);
		ProductInventory productInventory = ProductInventoryLocalServiceUtil
				.getProductInventory(productInventoryId);
		if (Validator.isNotNull(productInventory))
			bodyContent = getTemplate(groupId, templateName);
		addToWishlistURL = "javascript:wishListLogin("
				+ String.valueOf(productInventoryId) + ")";
		if (Validator.isNotNull(wishListProductIds)) {
			String[] wishlistProductId = wishListProductIds
					.split(StringPool.COMMA);
			for (int i = 0; i < wishlistProductId.length; i++) {
				if (wishlistProductId[i].equals(String
						.valueOf(productInventoryId))) {
					addedtoWishList = EECConstants.STYLE_DISPLAY_BLOCK;
					addtoMywishList = EECConstants.STYLE_DISPLAY_NONE;
					addToWishlistURL = "javascript:addToWishListPage("
							+ StringPool.BLANK + ")";
					break;
				}
			}

		}

		String addtoCompareURL = "javascript:addProducToCompare("
				+ String.valueOf(productInventory.getInventoryId()) + ","
				+ String.valueOf(productInventory.getCustomFieldRecId()) + ")";
		String businessdays_min = EECUtil.getPreference(companyId,
				"businessdays_min");
		String businessdays_max = EECUtil.getPreference(companyId,
				"businessdays_max");
		businessdays_min = (Validator.isNotNull(businessdays_min)) ? businessdays_min
				: String.valueOf(PortletPropsValues.DELIVER_BUSINESSDAYS_MIN);
		businessdays_max = (Validator.isNotNull(businessdays_max)) ? businessdays_max
				: String.valueOf(PortletPropsValues.DELIVER_BUSINESSDAYS_MAX);
		String[] oldSubs = { "[$WISHLIST_URL$]", "[$ADDED_WISHLIST$]",
				"[$ADD_MYWISHLIST$]", "[$ADDTOCOMPARE_URL$]",
				"[$ADD_TO_COMPARE_STYLE$]", "[$ADDED_TO_COMPARE_STYLE$]",
				"[$ADDTOCOMPARE_DIV_ID$]", "[$ADDEDTOCOMPARE_DIV_ID$]",
				"[$DELIVERED_MIN_DAYS$]", "[$DELIVERED_MAX_DAYS$]", "[$SHOPNOW_URL$]" };
		String[] newSubs = { addToWishlistURL, addedtoWishList,
				addtoMywishList, addtoCompareURL,
				EECConstants.STYLE_DISPLAY_BLOCK,
				EECConstants.STYLE_DISPLAY_NONE,
				PortletPropsValues.DETAILS_ADD_TO_COMPARE_DIV_ID,
				PortletPropsValues.DETAILS_ADDED_TO_COMPARE_DIV_ID,
				businessdays_min, businessdays_max, shopNowURL };

		sb.append(StringUtil.replace(
				replacedContent(companyId, bodyContent, type, productInventory,
						pathThemeImages, detailURL, null, cartViewURL, ctxPath,
						"DETAILVIEW_CONTENT", StringPool.BLANK,
						wishListProductIds, portletRequest), oldSubs, newSubs));
		// start of trick used to resolve the issue
		// if there is no define feild for a product there was a alignment issue
		// so i added a table to avoid alignment problem when there is no define
		// field content.....the css is totally messed up.(initially not
		// developed by me) so i used this trick email:azar@esquareinfo.com

		if (Validator.isNull(getDefineFieldContent(groupId, companyId,
				productInventory))) {
			sb.append("<table></table>");
		} else {
			sb.append(getDefineFieldContent(groupId, companyId,
					productInventory));
		}
		// end of trick
		return sb.toString();

	}

	public static String getCartTemplateContent(long groupId, long companyId,
			String pathThemeImages, Map<CartItem, Integer> productInventories,
			PortletURL renderURL, PortletURL actionURL, String ctxPath,
			String namespace, boolean isSignedIN, String shopNowURL,
			PortletRequest portletRequest) throws SystemException,
			PortalException {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		StringBuffer sb = new StringBuffer();
		try {
			String couponDisplay = EECConstants.STYLE_DISPLAY_NONE;
			String header = getTemplate(groupId,
					PortletPropsValues.CART_HEADER_TEMPLATE);
			String bodyContent = getTemplate(groupId,
					PortletPropsValues.CART_CONTENT_TEMPLATE);
			String footer = getTemplate(groupId,
					PortletPropsValues.CART_FOOTER_TEMPLATE);
			Cart cart = EECUtil.getCart(portletRequest);
			Set results = productInventories.entrySet();
			Iterator itr = results.iterator();
			couponDisplay = (productInventories.size() > 0) ? EECConstants.STYLE_DISPLAY_BLOCK
					: EECConstants.STYLE_DISPLAY_NONE;
			sb.append(header);
			NumberFormat currencyFormat = EECUtil.getCurrencyFormat(companyId);
			for (int i = 0; itr.hasNext(); i++) {
				Map.Entry entry = (Map.Entry) itr.next();
				CartItem cartItem = (CartItem) entry.getKey();
				Integer count = (Integer) entry.getValue();

				ProductInventory productInventory = cartItem
						.getProductInventory();
				boolean osp = productInventory.getOutOfStockPurchase();
				String content = replacedContent(companyId, bodyContent, null,
						productInventory, pathThemeImages, renderURL, null,
						actionURL, ctxPath, StringPool.BLANK, namespace,
						StringPool.BLANK, portletRequest);
				String change = "<a class='eeccart-change' id=\""
						+ productInventory.getInventoryId()
						+ "\" href=\"javascript:changeQuantity("
						+ productInventory.getInventoryId() + "," + i + ","
						+ productInventory.getQuantity() + "," + osp
						+ ");\">Change</a> ";
				String quantity = "<input  type='text' maxlength='4' class='eeccart-changebox' id=\"input_"
						+ productInventory.getInventoryId()
						+ "\" name=\"productInventory_"
						+ productInventory.getInventoryId()
						+ "_"
						+ i
						+ "_count\" value=" + count + " size='2' readonly /> ";
				double listorginalrates = productInventory.getPrice()
						- (productInventory.getPrice() * (productInventory
								.getDiscount() / 100));
				double subtotal = listorginalrates * count;
				String closebutttonURL = namespace + "deleteSelectedCart("
						+ productInventory.getInventoryId() + ");";

				String[] oldSubs = { "[$CHANGE$]", "[$CART_QUANTITY$]",
						"[$SUB_TOTAL$]", "[$CLOSE_BUTTON$]" };
				String[] newSubs = { change, quantity,
						String.valueOf(currencyFormat.format(subtotal)),
						closebutttonURL };
				sb.append(StringUtil.replace(content, oldSubs, newSubs));
				total = total + count;
			}

			if (results.size() > 0) {
				String[] oldSubs = { "[$EMPTY_CART$]", "[$UPDATE_CART$]",
						"[$IS_LOGIN$]", "[$CHECKOUT$]", "[$PATHTHEMEIMAGES$]",
						"[$SHOPNOW_URL$]", "[$COUPON_DISPLAY$]",
						"[$COUPONCODES$]", "[$DELETE_COUPON_URL$]",
						"[$UPDATE_COUPON$]" };
				String[] newSubs = {
						namespace + "emptyCart()",
						namespace + "updateCart()",
						isSignedIN ? LanguageUtil.get(themeDisplay.getLocale(),
								"checkout") : LanguageUtil.get(
								themeDisplay.getLocale(), "login-to-checkout"),
						namespace + "checkOut()", pathThemeImages, shopNowURL,
						couponDisplay, cart.getCouponCodes(),
						namespace + "deleteCoupon()",
						namespace + "updateCoupon()" };
				sb.append(StringUtil.replace(footer, oldSubs, newSubs));
			} else {
				sb.append("<div id='eecart-emptymsg'>"
						+ PortletPropsValues.CART_EMPTY_MESSAGE + "</div>");
				sb.append("<input type='button' id='continueshoping' value='Continue Shopping' onclick=\"location.href="
						+ "'" + shopNowURL + "'" + "\" />");
			}
		} catch (Exception e) {
		}

		return sb.toString();
	}

	public static String getDefineFieldContent(long groupId, long companyId,
			ProductInventory productInventory) throws SystemException,
			PortalException {
		StringBuffer sb = new StringBuffer();
		String customFieldValue = StringPool.BLANK;
		String header = getTemplate(groupId,
				PortletPropsValues.DEFINE_FIELDS_HEADER_TEMPLATE);
		String content = getTemplate(groupId,
				PortletPropsValues.DEFINE_FIELDS_TEMPLATE);
		String footer = getTemplate(groupId,
				PortletPropsValues.DEFINE_FIELDS_FOOTER_TEMPLATE);
		try {
			if (productInventory.getCustomFieldRecId() > 0) {
				EcustomField custFieldobj = EcustomFieldLocalServiceUtil
						.getEcustomField(productInventory.getCustomFieldRecId());
				List<ShippingRegion> ecustomFieldlist = EECUtil.readXMLContent(
						custFieldobj.getXml(),
						PortletPropsValues.CUSTOM_FIELD_TAG);
				if (ecustomFieldlist.size() > 0)
					sb.append(header);
				for (ShippingRegion customField : ecustomFieldlist) {
					customFieldValue = EECUtil.getCustomFieldsValue(companyId,
							customField, productInventory);
					String[] oldSubs = { "[$KEY_NAME$]", "[$KEY_VALUE$]" };
					String[] newSubs = {
							customField.getName(),
							(Validator.isNotNull(customFieldValue)) ? customFieldValue
									: PortletPropsValues.DEFINE_FIELDS_NOTAVAILABLE };
					sb.append(StringUtil.replace(content, oldSubs, newSubs));
				}
				sb.append(footer);
			}
		} catch (Exception e) {
		}
		return sb.toString();
	}

	public static String getProductsFilterContent(
			List<ProductInventory> filterContentList, long groupId,
			long companyId, String pathThemeImages, int viewCount, String type,
			long parentCatalogId, PortletURL detailURL, PortletURL cartViewURL,
			PortletURL userViewURL, String ctxPath, String className,
			String wishListProductIds, PortletRequest portletRequest)
			throws NumberFormatException, PortalException, SystemException {
		StringBuffer sb = new StringBuffer();
		if (viewCount > 0) {
			sb.append(getHeader(groupId, type, parentCatalogId, userViewURL,
					ctxPath, EECConstants.STYLE_DISPLAY_BLOCK));
		}
		sb.append(getBody(groupId, companyId, pathThemeImages, viewCount, type,
				parentCatalogId, detailURL, cartViewURL, userViewURL, ctxPath,
				className, filterContentList, 0, wishListProductIds,
				portletRequest));
		if (viewCount > 0) {
			sb.append(getFooter(groupId, type));
		}
		return sb.toString();

	}

	public static String getOrderSummaryTemplateContent(long groupId,
			PortletRequest portletRequest) {
		StringBuffer sb = new StringBuffer();
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
					.getAttribute(WebKeys.THEME_DISPLAY);
			/*
			 * NumberFormat numberFormat = NumberFormat.getInstance(themeDisplay
			 * .getLocale());
			 */

			NumberFormat currencyFormat = EECUtil
					.getCurrencyFormat(themeDisplay.getCompanyId());
			Order order = OrderLocalServiceUtil.getLatestOrder(
					themeDisplay.getUserId(), themeDisplay.getCompanyId());
			String bodyContent = getTemplate(groupId,
					PortletPropsValues.ORDERSUMMARY_CONTENT_TEMPLATE);
			Cart cart = EECUtil.getCart(portletRequest);
			Coupon coupon = cart.getCoupon();
			Map productInventories = cart.getProductinventories();
			double grandTotal = EECUtil.calculateTotal(
					themeDisplay.getCompanyId(), themeDisplay.getUserId(),
					productInventories, order.getShippingCountry(),
					order.getShippingState(), coupon, cart.getAltShipping(),
					cart.isInsure());

			String[] oldSubs = { "[$PRODUCT_COUNT$]", "[$SUB_TOTAL$]",
					"[$COUPON_DISCOUNT$]", "[$TAX_PRICE$]",
					"[$SHIPPING_PRICE$]", "[$GRAND_TOTAL$]",
					"[$WALLET_AMOUNT$]", "[$AMOUNT_PAYABLE$]" };
			String[] newSubs = {
					String.valueOf(cart.getProductsSize()),
					String.valueOf(currencyFormat.format(EECUtil
							.calculateActualSubtotal(productInventories))),
					String.valueOf(currencyFormat.format(EECUtil
							.calculateCouponDiscount(
									themeDisplay.getCompanyId(),
									themeDisplay.getUserId(),
									productInventories, coupon,
									order.getShippingCountry(),
									order.getShippingState()))),
					String.valueOf(currencyFormat.format(EECUtil.calculateTax(
							themeDisplay.getCompanyId(), productInventories,
							order.getShippingCountry(),
							order.getShippingState(), themeDisplay.getUserId(), coupon))),
					String.valueOf(currencyFormat.format(EECUtil
							.calculateShippingPrice(
									themeDisplay.getCompanyId(),
									productInventories,
									order.getShippingCountry(),
									order.getShippingState()))),
					String.valueOf(currencyFormat.format(grandTotal)),
					String.valueOf(currencyFormat.format(cart.getWalletAmount())),
					String.valueOf(currencyFormat.format(EECUtil.amountPayable(
							grandTotal, cart.getWalletAmount()))) };
			sb.append(StringUtil.replace(bodyContent, oldSubs, newSubs));
		} catch (Exception e) {
		}
		return sb.toString();
	}

	public static String getRelatedProductsContent(long groupId,
			long companyId, long productId, String type,
			String pathThemeImages, PortletURL detailURL,
			PortletURL userViewURL, String ctxPath,
			PortletRequest portletRequest) throws SystemException,
			NumberFormatException, PortalException {
		StringBuffer sb = new StringBuffer();
		String bodyContent = getTemplate(groupId,
				PortletPropsValues.ECAR_RELATED_AND_SIMILAR_PRODUCT_TTEMPLATE);
		List<ProductInventory> productInventories = ProductInventoryLocalServiceUtil
				.getRelatedProduct(companyId, String.valueOf(productId), true,
						true, 4);
		detailURL.setParameter("cmd", "detailsView");
		userViewURL.setParameter("CurProductId", String.valueOf(productId));
		if (productInventories.size() > 0){
			sb.append(getHeader(groupId, EECConstants.RELATED_PRODUCT, 0,
					userViewURL, ctxPath, EECConstants.STYLE_DISPLAY_BLOCK));
		}
		for (int i = 0; i < productInventories.size(); i++) {
			sb.append(replacedContent(companyId, bodyContent, type,
					productInventories.get(i), pathThemeImages, detailURL,
					null, userViewURL, ctxPath,
					PortletPropsValues.EEC_HOME_GRIDCLASS, StringPool.BLANK,
					StringPool.BLANK, portletRequest));
		}
		return sb.toString();
	}

	public static String getSimilarProductsContent(long groupId,
			long companyId, long productInventoryId, String type,
			String pathThemeImages, PortletURL detailURL,
			PortletURL userViewURL, String ctxPath, int priceRange,
			PortletRequest portletRequest) throws SystemException,
			NumberFormatException, PortalException {
		StringBuffer sb = new StringBuffer();
		String bodyContent = getTemplate(groupId,
				PortletPropsValues.ECAR_RELATED_AND_SIMILAR_PRODUCT_TTEMPLATE);
		// ProductInventory
		// pInventory=ProductInventoryLocalServiceUtil.findByProductDetailsId(productId);
		ProductInventory pInventory = ProductInventoryLocalServiceUtil
				.getProductInventory(productInventoryId);
		// long pInventoryId=pInventory.getInventoryId();
		double price = pInventory.getPrice() * (((double) priceRange) / 100);
		min = pInventory.getPrice() - price;
		max = pInventory.getPrice() + price;
		List<ProductInventory> productInventories = ProductInventoryLocalServiceUtil
				.getSimilarProduct(companyId, productInventoryId, min, max,
						true, true, 4);
		detailURL.setParameter("cmd", "detailsView");
		userViewURL.setParameter("CurProductId",
				String.valueOf(productInventoryId));
		if (productInventories.size() > 0){
			sb.append(getHeader(groupId, EECConstants.SIMILAR_PRODUCTS, 0,
					userViewURL, ctxPath, EECConstants.STYLE_DISPLAY_BLOCK));
		}
		for (int i = 0; i < productInventories.size(); i++) {
			sb.append(replacedContent(companyId, bodyContent, type,
					productInventories.get(i), pathThemeImages, detailURL,
					null, userViewURL, ctxPath,
					PortletPropsValues.EEC_HOME_GRIDCLASS, StringPool.BLANK,
					StringPool.BLANK, portletRequest));
		}
		return sb.toString();
	}

	public static String getWishListTemplateContent(long groupId,
			long companyId, String pathThemeImages,
			Map<WishListItem, Integer> productInventories,
			PortletURL renderURL, PortletURL actionURL, String ctxPath,
			String namespace, boolean isSignedIN, String shopNowURL,
			PortletRequest portletRequest) throws SystemException,
			PortalException {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append(getHeader(groupId,
					PortletPropsValues.WISHLIST_TITLE_NAME, 0, actionURL,
					ctxPath, EECConstants.STYLE_DISPLAY_NONE));
			if (!productInventories.isEmpty()) {
				String header = getTemplate(groupId,
						PortletPropsValues.WISHLIST_HEADER_TEMPLATE);
				sb.append(header);
			}
			String bodyContent = getTemplate(groupId,
					PortletPropsValues.WISHLIST_CONTENT_TEMP);

			Iterator itr = productInventories.entrySet().iterator();
			while (itr.hasNext()) {
				Map.Entry entry = (Map.Entry) itr.next();
				WishListItem wishListItem = (WishListItem) entry.getKey();
				ProductInventory productInventory = wishListItem
						.getProductInventory();
				String closebutttonURL = "javascript:" + namespace
						+ "deleteSelectedWishList("
						+ productInventory.getInventoryId() + ");";

				String detailsbutttonURL = "javascript:" + namespace
						+ "addToDetailsPage("
						+ productInventory.getInventoryId() + ");";
				String[] oldSubs = { "[$DETAILSPAGE_URL$]", "[$CLOSE_BUTTON$]" };
				String[] newSubs = { detailsbutttonURL, closebutttonURL };

				sb.append(StringUtil.replace(
						replacedContent(companyId, bodyContent, null,
								productInventory, pathThemeImages, renderURL,
								null, actionURL, ctxPath,
								PortletPropsValues.EEC_WISH_LIST_CLASS,
								namespace, StringPool.BLANK, portletRequest),
						oldSubs, newSubs));

			}
			if (productInventories.isEmpty()) {
				sb.append("<div id='eecart-emptymsg'>"
						+ PortletPropsValues.WISHLIST_EMPTY_MESSAGE + "</div>");
				sb.append("<input type='button' id='continueshoping' value='Continue Shopping' onclick=\"location.href="
						+ "'" + shopNowURL + "'" + "\" />");
			}
		} catch (Exception e) {
		}

		return sb.toString();
	}

	public static String getCompareProductsContent(String bodyContent,
			long groupId, long companyId, long productInventoryId,
			String pathThemeImages, PortletURL detailURL,
			PortletURL userViewURL, String ctxPath, String namespace,
			StringBuffer sb, PortletRequest portletRequest)
			throws SystemException, PortalException {
		ProductInventory pInventory = ProductInventoryLocalServiceUtil
				.getProductInventory(productInventoryId);
		String content = replacedContent(companyId, bodyContent, null,
				pInventory, pathThemeImages, detailURL, null, userViewURL,
				ctxPath, PortletPropsValues.EEC_HOME_GRIDCLASS,
				StringPool.BLANK, StringPool.BLANK, portletRequest);
		String closebutttonURL = namespace + "deleteSelectedProduct("
				+ pInventory.getInventoryId() + ");";
		sb.append(StringUtil.replace(content, "[$CLOSE_BUTTON$]",
				closebutttonURL));
		return sb.toString();

	}

	public static void getChildCatalogMap(long companyId, long parentCatalogId,
			LinkedHashMap<String, String> childCatalogMap)
			throws SystemException {
		Catalog catalog = null;
		try {
			catalog = CatalogLocalServiceUtil.getCatalog(parentCatalogId);
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
		if (ProductDetailsLocalServiceUtil.getProductDetailsList(companyId,
				parentCatalogId).size() > 0) {
			if (!childCatalogMap.containsKey(catalog.getName())) {
				childCatalogMap.put(catalog.getName(),
						String.valueOf(catalog.getCatalogId()));
			} else {
				childCatalogMap.put(catalog.getName(),
						childCatalogMap.get(catalog.getName())
								+ StringPool.COMMA + catalog.getCatalogId());
			}
		}

		List<Catalog> catalogs = CatalogLocalServiceUtil.getCatalogs(companyId,
				parentCatalogId);

		for (Catalog curCatalog : catalogs) {
			getChildCatalogMap(companyId, curCatalog.getCatalogId(),
					childCatalogMap);

		}
	}

	static double min = 0.0;
	static double max = 0.0;
	private static Log _log = LogFactoryUtil.getLog(UserViewUtil.class);
}