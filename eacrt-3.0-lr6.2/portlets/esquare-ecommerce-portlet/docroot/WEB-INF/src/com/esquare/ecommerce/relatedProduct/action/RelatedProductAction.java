package com.esquare.ecommerce.relatedProduct.action;

import javax.portlet.Event;
import javax.portlet.PortletSession;

import com.liferay.util.bridges.mvc.MVCPortlet;

public class RelatedProductAction extends MVCPortlet {
	public void processEvent(javax.portlet.EventRequest request,
			javax.portlet.EventResponse response)
			throws javax.portlet.PortletException, java.io.IOException {

		Event event = request.getEvent();
		String value = (String) event.getValue();
		String[] values = value.split(",");
		if (values.length > 0) {
			response.setRenderParameter("curProductId", values[0]);
			PortletSession pSession = request.getPortletSession();
			pSession.setAttribute("EVENT_PRODUCTID", values[0],
					PortletSession.PORTLET_SCOPE);
		}
	}
}