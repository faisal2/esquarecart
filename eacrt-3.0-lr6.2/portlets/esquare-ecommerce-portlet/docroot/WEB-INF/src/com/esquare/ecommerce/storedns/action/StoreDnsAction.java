package com.esquare.ecommerce.storedns.action;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.esquare.ecommerce.NoSuchPaymentUserDetailException;
import com.esquare.ecommerce.dashboard.util.DashboardUtil;
import com.esquare.ecommerce.instance.action.AddHost;
import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.EcartEbsPayment;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.model.impl.EcartEbsPaymentImpl;
import com.esquare.ecommerce.model.impl.PaymentUserDetailImpl;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.EcartEbsPaymentLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil;
import com.esquare.ecommerce.util.DNSSettings;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Account;
import com.liferay.portal.model.VirtualHost;
import com.liferay.portal.service.AccountLocalServiceUtil;
import com.liferay.portal.service.VirtualHostLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class TemplateManager
 */
public class StoreDnsAction extends MVCPortlet {
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = ParamUtil.getString(actionRequest, "virtualHost");
		String cmd1 = ParamUtil.getString(actionRequest, EECConstants.CMD);
		if (Validator.isNotNull(cmd)) {
			String hostname = ParamUtil.getString(actionRequest, "hostName");
			try {

				if (PortletPropsValues.CURRENT_WORKING_ENVIRONMENT
						.equals("Production")) {
					DNSSettings.deleteDomain(hostname);
				} else {
					AddHost addHost = new AddHost();
					addHost.updateDomain(hostname, cmd);
				}

				VirtualHost vitualHost = VirtualHostLocalServiceUtil
						.getVirtualHost(hostname);
				vitualHost.setHostname(cmd);
				VirtualHostLocalServiceUtil.updateVirtualHost(vitualHost);

				Account account = AccountLocalServiceUtil.getAccount(
						themeDisplay.getCompanyId(), themeDisplay.getAccount()
								.getAccountId());
				account.setName(cmd);
				AccountLocalServiceUtil.updateAccount(account);
			} catch (Exception e) {
				_log.info(e.getClass());
			}
		}
		if (Validator.isNotNull(cmd1)
				&& cmd1.equalsIgnoreCase(EECConstants.PAY_EBS)) {
			// getEBSResponse(actionRequest);
			EECUtil.getEBSResponse(actionRequest);
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) actionRequest
					.getAttribute("EBSResponse");
			long responseCode = Long.parseLong(map.get("ResponseCode"));
			String isFlagged = map.get("IsFlagged");
			String packageType = map.get("DeliveryName");
			if (responseCode == 0) {
				if (isFlagged.equalsIgnoreCase("NO")) {

					Instance instance = null;
					// int pckMonth = 1;
					try {
						instance = InstanceLocalServiceUtil
								.getInstance(themeDisplay.getCompanyId());
						Calendar expDate = Calendar.getInstance();
						expDate.setTime(instance.getExpiryDate());
						expDate.add(Calendar.MONTH, 1);
						instance.setExpiryDate(expDate.getTime());
						instance.setPackageType(packageType);

						InstanceLocalServiceUtil.updateInstance(instance);
					} catch (Exception e) {
						_log.info(e.getClass());
					}
					updateEcartEbs(map, themeDisplay.getCompanyId(),
							instance.getInstanceUserId());

					updateEBSUserDetails(map, themeDisplay.getCompanyId(),
							instance.getInstanceUserId());

					updateCartPermission(packageType,
							themeDisplay.getCompanyId());

					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/storedns/view.jsp");

				} else if (isFlagged.equalsIgnoreCase("YES")) {
					SessionMessages.add(actionRequest,
							EECConstants.PAYMENT_PENDING);
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/storedns/view.jsp");
				}
			} else {
				SessionMessages.add(actionRequest, "error-in-create");
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/storedns/view.jsp");
			}
		}
	}

	public void updateCartPermission(String packageType, long companyId) {
		String[] permissionKeys = PortletPropsValues.PACKAGE_PERMISSION_KEY;
		for (String permissionKey : permissionKeys) {
			CartPermission cartPermission = CartPermissionLocalServiceUtil
					.getCartPermissionCI_PK(companyId, permissionKey);
			try {
				if (cartPermission == null) {
					CartPermissionLocalServiceUtil.createCartPermission(
							companyId, permissionKey,
							DashboardUtil.getPackageValue(packageType,
									permissionKey), 0, false);
				} else {
					cartPermission.setValue(DashboardUtil.getPackageValue(
							packageType, permissionKey));
					if (cartPermission.getPrice() > 0) {
						cartPermission.setPrice(DashboardUtil.getPackagePrice(
								packageType, permissionKey));
					}
					CartPermissionLocalServiceUtil
							.updateCartPermission(cartPermission);
				}
			} catch (Exception e) {
			}
		}
	}

	public void updateEBSUserDetails(HashMap<String, String> map,
			long companyId, long instanceUserrId) {

		String BillingCity = map.get("BillingCity");
		String BillingCountry = map.get("BillingCountry");
		String BillingEmail = map.get("BillingEmail");
		String BillingName = map.get("BillingName");
		String BillingPhone = map.get("BillingPhone");
		String BillingPostalCode = map.get("BillingPostalCode");
		String BillingState = map.get("BillingState");
		String BillingAddress = map.get("BillingAddress");

		PaymentUserDetail ebsUserDetail = new PaymentUserDetailImpl();
		try {
			ebsUserDetail = PaymentUserDetailLocalServiceUtil
					.findByUserId(instanceUserrId);
		} catch (NoSuchPaymentUserDetailException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}

		ebsUserDetail.setCompanyId(companyId);
		ebsUserDetail.setUserId(instanceUserrId);
		ebsUserDetail.setEmail(BillingEmail);
		ebsUserDetail.setName(BillingName);
		ebsUserDetail.setPhoneNo(Long.parseLong(BillingPhone));
		ebsUserDetail.setPinCode(Long.parseLong(BillingPostalCode));
		ebsUserDetail.setCity(BillingCity);
		ebsUserDetail.setCountry(BillingCountry);
		ebsUserDetail.setState(BillingState);
		ebsUserDetail.setAddress(BillingAddress);
		try {
			if (Validator.isNotNull(ebsUserDetail.getPrimaryKey())) {
				PaymentUserDetailLocalServiceUtil
						.updatePaymentUserDetail(ebsUserDetail);
			} else {
				long recId = CounterLocalServiceUtil
						.increment("PaymentUserDetail.class");
				ebsUserDetail.setRecId(recId);
				PaymentUserDetailLocalServiceUtil
						.addPaymentUserDetail(ebsUserDetail);
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	public void updateEcartEbs(HashMap<String, String> map, long companyId,
			long instanceUserrId) {
		// EcartEbsPayment
		String responseMessage = map.get("ResponseMessage");
		String dateCreated = map.get("DateCreated");
		String amount = map.get("Amount").toString();
		long transactionId = Long.parseLong(map.get("TransactionID"));
		String orderId = map.get("MerchantRefNo").toString();
		long paymentMethod = Long.parseLong(map.get("PaymentMethod").trim());
		long paymentId = Long.parseLong(map.get("PaymentID"));
		long responseCode = Long.parseLong(map.get("ResponseCode"));
		String isFlagged = map.get("IsFlagged");

		EcartEbsPayment ecartEbsPayment = new EcartEbsPaymentImpl();
		ecartEbsPayment.setCompanyId(companyId);
		ecartEbsPayment.setEcartAutoId(String.valueOf(instanceUserrId));
		ecartEbsPayment.setUserId(instanceUserrId);
		ecartEbsPayment.setResponseCode(responseCode);
		ecartEbsPayment.setResponseMessage(responseMessage);
		ecartEbsPayment.setPaidDate(EECUtil.stringToDate(dateCreated,
				"yyyy-MM-dd hh:mm:ss"));
		ecartEbsPayment.setPaymentId(paymentId);
		ecartEbsPayment.setPaymentMethod(paymentMethod);
		ecartEbsPayment.setAmount(amount);
		ecartEbsPayment.setIsFlagged(isFlagged);
		ecartEbsPayment.setTransactionId(transactionId);
		try {
			long ebsTrackId = CounterLocalServiceUtil
					.increment("EcartEbsPayment.class");
			ecartEbsPayment.setEbsTrackId(ebsTrackId);
			EcartEbsPaymentLocalServiceUtil.addEcartEbsPayment(ecartEbsPayment);
		} catch (SystemException e) {
		}

	}

	/*
	 * protected void getEBSResponse(ActionRequest actionRequest) throws
	 * IOException { String DR = ParamUtil.getString(actionRequest, "DR");
	 * String key = PortletPropsValues.EBS_SECRET_KEY; StringBuffer data1 = new
	 * StringBuffer().append(DR); for (int i = 0; i < data1.length(); i++) { if
	 * (data1.charAt(i) == ' ') data1.setCharAt(i, '+'); } Base64 base64 = new
	 * Base64(); byte[] data = base64.decode(data1.toString()); RC4 rc4 = new
	 * RC4(key); byte[] result = rc4.rc4(data);
	 * 
	 * ByteArrayInputStream byteIn = new ByteArrayInputStream(result, 0,
	 * result.length); BufferedReader dataIn = new BufferedReader( new
	 * InputStreamReader(byteIn)); String recvString1 = ""; String recvString =
	 * ""; recvString1 = dataIn.readLine(); int i = 0; while (recvString1 !=
	 * null) { i++; if (i > 705) break; recvString += recvString1 + "\n";
	 * recvString1 = dataIn.readLine(); } recvString = recvString.replace("=&",
	 * "=--&"); StringTokenizer st = new StringTokenizer(recvString, "=&");
	 * String field, value; HashMap<String, String> ebsMap = new HashMap<String,
	 * String>(); while (st.hasMoreTokens()) { field = st.nextToken(); value =
	 * st.nextToken(); ebsMap.put(field, value); }
	 * actionRequest.setAttribute("EBSResponse", ebsMap); }
	 */
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(renderRequest, Constants.CMD);
		if (Validator.isNotNull(cmd) && cmd.equalsIgnoreCase(Constants.EDIT)) {
			String domainName = ParamUtil
					.getString(renderRequest, "domainName");
			String packageType = ParamUtil.getString(renderRequest,
					"selectedPck");
			renderRequest.setAttribute("packageType", packageType);
			renderRequest.setAttribute("domainName", domainName);
			viewTemplate = "/html/eec/common/storedns/detail.jsp";
		} else {
			viewTemplate = "/html/eec/common/storedns/view.jsp";
		}
		super.doView(renderRequest, renderResponse);

	}

	private static Log _log = LogFactoryUtil.getLog(StoreDnsAction.class);

}
