package com.esquare.ecommerce.util;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.util.portlet.PortletProps;

public class SocialNetworkConnectUtil {
	public static boolean isAppEnabled(long companyId, String prefix)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(
				companyId,
				prefix + ".connect.auth.enabled",
				GetterUtil.getBoolean(PortletProps.get(prefix
						+ ".connect.auth.enabled")));
	}

	public static String getAppId(long companyId, String prefix)
			throws SystemException
			 {
		return PrefsPropsUtil.getString(companyId, prefix + ".connect.app.id",
				PortletProps.get(prefix + ".connect.app.id"));
	}

	public static String getAppSecret(long companyId, String prefix)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId, prefix
				+ ".connect.app.secret",
				PortletProps.get(prefix + ".connect.app.secret"));
	}

	public static boolean isTwitterEnabled(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				"twitter.connect.auth.enabled",
				PortletPropsValues.TWITTER_CONNECT_AUTH_ENABLED);
	}

	public static String getTwitterAppId(long companyId) throws SystemException {
		return PrefsPropsUtil.getString(companyId, "twitter.connect.app.id",
				PortletPropsValues.TWITTER_CONNECT_APP_ID);
	}

	public static String getTwitterAppSecret(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId,
				"twitter.connect.app.secret",
				PortletPropsValues.TWITTER_CONNECT_APP_SECRET);
	}

	public static String getTwitterAuthURL()  {
		return PortletPropsValues.TWITTER_CONNECT_OAUTH_AUTH_URL;
	}

	public static String getTwitterVerifyCredentialURL() {
		return PortletPropsValues.TWITTER_CONNECT_OAUTH_AUTH_VERIFY_CREDENTIALS_URL;
	}

	public static boolean isLinkedInEnabled(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getBoolean(companyId,
				"linkedIn.connect.auth.enabled",
				PortletPropsValues.LINKEDIN_CONNECT_AUTH_ENABLED);
	}

	public static String getLinkedInAppId(long companyId)
			throws SystemException {
		return PrefsPropsUtil.getString(companyId, "linkedIn.connect.app.id",
				PortletPropsValues.LINKEDIN_CONNECT_APP_ID);
	}

	public static String getLinkedInAppSecret(long companyId) throws SystemException
			 {
		return PrefsPropsUtil.getString(companyId,
				"linkedIn.connect.app.secret",
				PortletPropsValues.LINKEDIN_CONNECT_APP_SECRET);
	}

	public static boolean isLinkedInProfileViewEnabled()  {
		return PortletPropsValues.LINKEDIN_CONNECT_PROFILE_VIEW_ENABLED;
	}

	public static boolean isLinkedInAutoLoginEnabled()  {
		return PortletPropsValues.LINKEDIN_CONNECT_AUTO_LOGIN_ENABLED;
	}
}
