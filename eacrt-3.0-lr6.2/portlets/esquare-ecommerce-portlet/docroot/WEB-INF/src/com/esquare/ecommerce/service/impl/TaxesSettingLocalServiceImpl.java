/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.model.TaxesSetting;
import com.esquare.ecommerce.service.base.TaxesSettingLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.TaxesSettingUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the taxes setting local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.TaxesSettingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.TaxesSettingLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.TaxesSettingLocalServiceUtil
 */
public class TaxesSettingLocalServiceImpl
	extends TaxesSettingLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.esquare.ecommerce.service.TaxesSettingLocalServiceUtil} to access the taxes setting local service.
	 */public List<TaxesSetting> getTaxesSettingDetails(long companyId) throws SystemException{
			
			return TaxesSettingUtil.findByCompanyId(companyId);
		}
	 	public TaxesSetting getTaxesRate(long companyId,String countryId) throws SystemException{
			
			return TaxesSettingUtil.fetchByTaxesRate(companyId, countryId);
		}
}