/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.esquare.ecommerce.NoSuchOrderItemException;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.base.OrderItemLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.OrderItemFinderUtil;
import com.esquare.ecommerce.service.persistence.OrderItemUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * The implementation of the order item local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.OrderItemLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.OrderItemLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.OrderItemLocalServiceUtil
 */
public class OrderItemLocalServiceImpl extends OrderItemLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * com.esquare.ecommerce.service.OrderItemLocalServiceUtil} to access the
	 * order item local service.
	 */
	public List<OrderItem> getOrderItems(long orderId) throws SystemException {

		return orderItemPersistence.findByOrderId(orderId);
	}

	public List<OrderItem> findByOrderItemStatus(long orderId,
			String orderItemStatus) throws SystemException {
		return orderItemPersistence.findByO_I_S(orderId, orderItemStatus);
	}
	public List<OrderItem> findByOrderItemId(long orderId,
			String productInventoryId) throws SystemException {
		return OrderItemUtil.findByorderItemId(orderId, productInventoryId);
	}
	public List<OrderItem> getOrderItemPrice(long orderId,
			String productInventoryId) throws SystemException {
		return OrderItemUtil.findByorderItemId(orderId, productInventoryId);
	}
	public int getPurchasedProductCount(String isNotStatus, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getPurchasedProductCount(isNotStatus,
				companyId);
	}

	public long getTotalAmountOrder(String isNotStatus, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getTotalAmountOrder(isNotStatus, companyId);
	}

	public Map<String, String> getLastWeekMonthOrderAmountDaywise(int day,
			String isNotStatus, long companyId) throws SystemException {
		return OrderItemFinderUtil.getLastWeekMonthOrderAmountDaywise(day,
				isNotStatus, companyId);
	}

	public Double getLastWeekMonthOrderAmount(int day, String isNotStatus,
			long companyId) throws SystemException {
		return OrderItemFinderUtil.getLastWeekMonthOrderAmount(day,
				isNotStatus, companyId);
	}

	public long getTodayOrderAmount(String isNotStatus, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getTodayOrderAmount(isNotStatus, companyId);
	}

	public int getTodayPurchasedOrderItem(String isNotStatus, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getTodayPurchasedOrderItem(isNotStatus,
				companyId);
	}

	public int getPurchasedLastweekMonthProductCount(int day,
			String isNotStatus, long companyId) throws SystemException {
		return OrderItemFinderUtil.getPurchasedLastweekMonthProductCount(day,
				isNotStatus, companyId);
	}

	public long getYesterdayOrderAmount(String isNotStatus, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getYesterdayOrderAmount(isNotStatus,
				companyId);
	}

	public void updateOrderItemStatusByOrder(long orderId,
			String orderItemStatus, String shippingTrackerId,
			Date expectedDeliveryDate, Date deliveredDate,
			long canceledByUserId, boolean cancelByUser) throws SystemException {
		List<OrderItem> orderItems = orderItemPersistence
				.findByOrderId(orderId);
		for (OrderItem orderItem : orderItems) {
			updateOrderItemStatus(orderItem, orderItemStatus,
					shippingTrackerId, expectedDeliveryDate, deliveredDate,
					canceledByUserId, orderItem.getQuantity(), cancelByUser);
		}
	}

	public OrderItem updateOrderItemStatus(long orderItemId,
			String orderItemStatus, String shippingTrackerId,
			Date expectedDeliveryDate, Date deliveredDate,
			long canceledByUserId, int quantity, boolean cancelByUser)
			throws SystemException, NoSuchOrderItemException {
		OrderItem orderItem = orderItemPersistence
				.findByPrimaryKey(orderItemId);
		return updateOrderItemStatus(orderItem, orderItemStatus,
				shippingTrackerId, expectedDeliveryDate, deliveredDate,
				canceledByUserId, quantity, cancelByUser);
	}

	public OrderItem updateOrderItemStatus(OrderItem orderItem,
			String orderItemStatus, String shippingTrackerId,
			Date expectedDeliveryDate, Date deliveredDate,
			long canceledByUserId, int quantity, boolean cancelByUser)
			throws SystemException {
		orderItem.setModifiedDate(new Date());
		if (Validator.isNotNull(orderItemStatus))
			orderItem.setOrderItemStatus(orderItemStatus);
		if (Validator.isNotNull(shippingTrackerId))
			orderItem.setShippingTrackerId(shippingTrackerId);
		if (Validator.isNotNull(expectedDeliveryDate))
			orderItem.setExpectedDeliveryDate(expectedDeliveryDate);
		if (Validator.isNotNull(deliveredDate))
			orderItem.setDeliveredDate(deliveredDate);
		if (Validator.isNotNull(canceledByUserId))
			orderItem.setCanceledByUserId(canceledByUserId);
		if (cancelByUser)
			orderItem.setCancelByUser(cancelByUser);
		orderItem.setQuantity(quantity);
		orderItemPersistence.update(orderItem);
		return orderItem;
	}

	public int getOrderItemStatusCount(String status, long companyId)
			throws SystemException {
		return OrderItemFinderUtil.getOrderItemStatusCount(status,
				companyId);
	}
	
}