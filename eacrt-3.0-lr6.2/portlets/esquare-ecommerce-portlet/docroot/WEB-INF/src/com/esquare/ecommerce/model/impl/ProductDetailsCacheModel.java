/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.ProductDetails;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProductDetails in entity cache.
 *
 * @author Esquare
 * @see ProductDetails
 * @generated
 */
public class ProductDetailsCacheModel implements CacheModel<ProductDetails>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{productDetailsId=");
		sb.append(productDetailsId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", catalogId=");
		sb.append(catalogId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", visibility=");
		sb.append(visibility);
		sb.append(", relatedProductIds=");
		sb.append(relatedProductIds);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProductDetails toEntityModel() {
		ProductDetailsImpl productDetailsImpl = new ProductDetailsImpl();

		productDetailsImpl.setProductDetailsId(productDetailsId);
		productDetailsImpl.setCompanyId(companyId);
		productDetailsImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			productDetailsImpl.setCreateDate(null);
		}
		else {
			productDetailsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			productDetailsImpl.setModifiedDate(null);
		}
		else {
			productDetailsImpl.setModifiedDate(new Date(modifiedDate));
		}

		productDetailsImpl.setCatalogId(catalogId);

		if (name == null) {
			productDetailsImpl.setName(StringPool.BLANK);
		}
		else {
			productDetailsImpl.setName(name);
		}

		if (description == null) {
			productDetailsImpl.setDescription(StringPool.BLANK);
		}
		else {
			productDetailsImpl.setDescription(description);
		}

		productDetailsImpl.setVisibility(visibility);

		if (relatedProductIds == null) {
			productDetailsImpl.setRelatedProductIds(StringPool.BLANK);
		}
		else {
			productDetailsImpl.setRelatedProductIds(relatedProductIds);
		}

		productDetailsImpl.resetOriginalValues();

		return productDetailsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		productDetailsId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		catalogId = objectInput.readLong();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
		visibility = objectInput.readBoolean();
		relatedProductIds = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(productDetailsId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(catalogId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeBoolean(visibility);

		if (relatedProductIds == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(relatedProductIds);
		}
	}

	public long productDetailsId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long catalogId;
	public String name;
	public String description;
	public boolean visibility;
	public String relatedProductIds;
}