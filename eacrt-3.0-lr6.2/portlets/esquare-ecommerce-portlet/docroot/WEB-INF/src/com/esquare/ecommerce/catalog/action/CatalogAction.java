package com.esquare.ecommerce.catalog.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.esquare.ecommerce.DuplicateCatalogNameException;
import com.esquare.ecommerce.NoSuchCatalogException;
import com.esquare.ecommerce.catalog.util.EECCatalogUtil;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.CatalogServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductImagesLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.NoSuchRepositoryEntryException;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.DuplicateFolderNameException;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ecommerceCategoryAction
 */
public class CatalogAction extends MVCPortlet {

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(renderRequest, Constants.CMD);
		if (Validator.equals(cmd, "customField")) {

			viewTemplate = "/html/eec/common/catalog/common_edit.jsp";

		} else {

			viewTemplate = "/html/eec/common/catalog/view_catalog.jsp";
		}
		try {
			EECCatalogUtil.getCatalog(renderRequest, renderResponse);
		} catch (NumberFormatException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		super.doView(renderRequest, renderResponse);
	}

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		String cmd = ParamUtil.getString(actionRequest, Constants.CMD);
		try {
			if (cmd.equals(Constants.UPDATE)) {
				updateCatalog(actionRequest, actionResponse);
			} else if (cmd.equals(Constants.DELETE)) {
				deleteCatalog(actionRequest, actionResponse);
			} else if (cmd.equals(Constants.SEARCH)) {
				searchCatalog(actionRequest, actionResponse);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
		}
	}

	public void updateCatalog(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		long catalogId = ParamUtil.getLong(actionRequest, "catalogId", 0l);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long parentCatalogId = ParamUtil.getLong(actionRequest,
				"parentCatalogId");
		String catalogName = ParamUtil.getString(actionRequest, "name");
		StringBundler stringBundler = new StringBundler();
		stringBundler.append(CharPool.OPEN_BRACKET).append(CharPool.AMPERSAND)
				.append(CharPool.APOSTROPHE).append(CharPool.AT)
				.append(CharPool.BACK_SLASH).append(CharPool.CLOSE_CURLY_BRACE)
				.append(CharPool.COLON).append(CharPool.COMMA)
				.append(CharPool.EQUAL).append(CharPool.GREATER_THAN)
				.append(CharPool.FORWARD_SLASH).append(CharPool.LESS_THAN)
				.append(CharPool.OPEN_CURLY_BRACE)
				.append(CharPool.PERCENT).append(CharPool.PIPE)
				.append(CharPool.PLUS).append(CharPool.POUND)
				.append(CharPool.QUESTION).append(CharPool.QUOTE)
				.append(CharPool.SEMICOLON)
				.append(CharPool.SLASH).append(CharPool.STAR)
				.append(CharPool.TILDE).append(CharPool.CLOSE_BRACKET);
		String name = catalogName.replaceAll(stringBundler.toString(), "_");
		String description = ParamUtil.getString(actionRequest, "description");
		boolean showNavigation = ParamUtil.getBoolean(actionRequest,
				"showNavigation");
		boolean mergeWithParentCategory = ParamUtil.getBoolean(actionRequest,
				"mergeWithParentCategory");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Catalog.class.getName(), actionRequest);

		Folder catalogFolder = null;
		ServiceContext context = ServiceContextFactory.getInstance(
				DLFolder.class.getName(), actionRequest);
		context.setAddGroupPermissions(true);
		context.setAddGuestPermissions(true);
		try {
			catalogFolder = DLAppServiceUtil.getFolder(
					themeDisplay.getScopeGroupId(), 0L,
					PortletPropsValues.ECOMMERCE_CATALOGS);
		} catch (Exception e) {
			_log.info(e.getClass());
			catalogFolder = DLAppServiceUtil.addFolder(
					themeDisplay.getScopeGroupId(), 0l,
					PortletPropsValues.ECOMMERCE_CATALOGS,
					PortletPropsValues.ECOMMERCE_CATALOGS, context);
		}
		Folder parentFolder = null;
		Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompany(themeDisplay
					.getCompanyId());
			parentFolder = DLAppServiceUtil.getFolder(
					themeDisplay.getScopeGroupId(),
					catalogFolder.getFolderId(), company.getWebId());
		} catch (Exception e) {
			parentFolder = DLAppServiceUtil.addFolder(
					themeDisplay.getScopeGroupId(),
					catalogFolder.getFolderId(), company.getWebId(),
					company.getWebId(), context);
		}
		if (catalogId <= 0) {

			// Add catalog
			try {
				Folder folder = null;
				if (parentCatalogId <= 0) {
					folder = DLAppServiceUtil.addFolder(
							parentFolder.getRepositoryId(),
							parentFolder.getFolderId(), name, name, context);
				} else {
					Catalog parentCalatlog = CatalogLocalServiceUtil
							.getCatalog(parentCatalogId);
					if (parentCalatlog.getParentCatalogId() <= 0) {
						folder = DLAppServiceUtil.addFolder(
								parentFolder.getRepositoryId(),
								parentCalatlog.getFolderId(), name, name,
								context);
					} else {
						Catalog calatlog = CatalogLocalServiceUtil
								.getCatalog(parentCalatlog.getCatalogId());
						folder = DLAppServiceUtil.addFolder(
								parentFolder.getRepositoryId(),
								calatlog.getFolderId(), name, name, context);
					}

				}
				Catalog catalog = CatalogServiceUtil.addCatalog(
						parentCatalogId, catalogName, description,
						showNavigation, folder.getFolderId(), serviceContext);
				// updateCustomField(actionRequest,catalog.getCatalogId());
			} catch (Exception e) {
				if (e instanceof NoSuchCatalogException
						|| e instanceof PrincipalException
						|| e instanceof DuplicateCatalogNameException
						|| e instanceof DuplicateFolderNameException) {
					_log.info(e.getClass());
					SessionErrors.add(actionRequest, e.getClass().getName());
					actionResponse
							.setRenderParameter("jspPage",
									"/html/eec/common/catalog/common_edit.jsp?cmd_add=ADD_CATALOG&cmd=customField");
				}
			}

		} else {
			// Update catalog

			try {
				Catalog calatlog = CatalogLocalServiceUtil
						.getCatalog(catalogId);
				DLAppServiceUtil.updateFolder(calatlog.getFolderId(), name,
						description, serviceContext);
				CatalogServiceUtil.updateCatalog(catalogId, parentCatalogId,
						catalogName, description, showNavigation,
						calatlog.getFolderId(), mergeWithParentCategory,
						serviceContext);
			} catch (Exception e) {
			}
		}

	}

	private void searchCatalog(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		String name = ParamUtil.getString(actionRequest, "keywords");
		if (Validator.isNotNull(name)) {
			try {
				DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
						Catalog.class, PortalClassLoaderUtil.getClassLoader());
				if (Validator.isNotNull(name))
					dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%"
							+ name + "%"));
				List<Catalog> searchCatalogList = CatalogLocalServiceUtil
						.dynamicQuery(dynamicQuery);
				PortletSession session = actionRequest.getPortletSession();
				session.setAttribute("SEARCH_CATALOG_LIST", searchCatalogList);
			} catch (Exception e) {
			}
		} else {
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/catalog/view_catalog.jsp");
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		resourceResponse.setContentType("text/javascript");
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		try {
			String catalogName = resourceRequest.getParameter("catalogName");
			long companyId = ParamUtil.getLong(resourceRequest, "companyId");
			long parentCatalogId = ParamUtil.getLong(resourceRequest,
					"parentCatalogId");
			boolean iscatalogName = false;
			List<Catalog> catalogs = CatalogLocalServiceUtil.getCatalogs(
					companyId, parentCatalogId);
			if (Validator.isNotNull(catalogs)) {
				for (Catalog catalog : catalogs) {
					if (catalog.getName().equalsIgnoreCase(catalogName)) {
						iscatalogName = true;
					}
				}
			}
			jsonObject.put("retVal", iscatalogName);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject.toString());

		} catch (SystemException e) {
		}

	}

	private void deleteCatalog(ActionRequest actionRequest,
			ActionResponse actionResponse) throws NumberFormatException,
			PortalException, SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long catalogId = ParamUtil.getLong(actionRequest, "catalogId", 0l);
		if (Validator.isNotNull(catalogId)) {
			deleteCatalog(themeDisplay.getCompanyId(), catalogId);
		}
	}

	public void deleteCatalog(long companyId, long catalogId)
			throws SystemException {

		// for logic written by nallakannan check svn version 170
		List<Catalog> catalogs = CatalogLocalServiceUtil.getCatalogs(companyId,
				catalogId);

		Catalog catalog1 = null;
		try {
			catalog1 = CatalogLocalServiceUtil.getCatalog(catalogId);
			CatalogLocalServiceUtil.deleteSingleCatalog(catalogId);
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
		try {
			DLAppServiceUtil.deleteFolder(catalog1.getFolderId());
		} catch (NoSuchRepositoryEntryException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}

		deleteProduct(companyId, catalog1.getCatalogId());
		for (Catalog catalog : catalogs) {
			deleteCatalog(companyId, catalog.getCatalogId());
		}
	}

	private void deleteProduct(long companyId, long catalogId)
			throws SystemException {
		List<ProductDetails> productDetailsList = ProductDetailsLocalServiceUtil
				.getProductDetailsList(companyId, catalogId);
		for (ProductDetails productDetails : productDetailsList) {
			try {
				ProductInventory pInventory = ProductInventoryLocalServiceUtil
						.findByProductDetailsId(productDetails
								.getProductDetailsId());
				List<ProductImages> pImages = ProductImagesLocalServiceUtil.findByInventoryId(pInventory.getInventoryId());
				for (ProductImages productimage: pImages) {
					ProductImagesLocalServiceUtil.deleteProductImages(productimage.getRecId());
				}
				
				ProductInventoryLocalServiceUtil.deleteProductInventory(pInventory.getInventoryId());
				ProductDetailsLocalServiceUtil
						.deleteProductDetails(productDetails
								.getProductDetailsId());
			} catch (Exception e) {
				_log.info(e.getClass());
			}
		}

	}

	private static Log _log = LogFactoryUtil.getLog(CatalogAction.class);
}
