package com.esquare.ecommerce.ordertracker.action;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;

import com.esquare.ecommerce.NoSuchOrderException;
import com.esquare.ecommerce.NoSuchOrderItemException;
import com.esquare.ecommerce.WalletLimitException;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.CartItemImpl;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.CancelledOrderItemsLocalServiceUtil;
import com.esquare.ecommerce.service.CouponLocalServiceUtil;
import com.esquare.ecommerce.service.MyWalletLocalServiceUtil;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.EmailUtil;
import com.esquare.ecommerce.util.SMSUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.ContentUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferay.util.portlet.PortletProps;
import java.util.Arrays;
import java.text.SimpleDateFormat;

public class OrderTrackerAction extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = ParamUtil.getString(actionRequest, "cmd", null);
		String curStatus = ParamUtil
				.getString(actionRequest, "curStatus", null);
		String redirectURL = ParamUtil.getString(actionRequest,
				EECConstants.REDIRECTURL, null);
		String emailType = ParamUtil
				.getString(actionRequest, "emailType", null);
		String location = ParamUtil.getString(actionRequest, "location", null);
		String comment = ParamUtil.getString(actionRequest, "comment", null);
		String reason = ParamUtil.getString(actionRequest, "reason", null);
		String orderItemIds = ParamUtil.getString(actionRequest,
				"orderItemIds", null);
		long orderId = ParamUtil.getLong(actionRequest, "orderId", 0l);
		double totalOrderAmountBeforeUpdate = 0;
		String shippingTrackerId = ParamUtil.getString(actionRequest,
				"shippingTrackerId", null);
		Order order = null;
		Coupon coupon = null;
		ServiceContext serviceContext = null;
		try {
			serviceContext = ServiceContextFactory.getInstance(
					Order.class.getName(), actionRequest);
		} catch (PortalException e1) {
			_log.error(e1.getMessage());
		} catch (SystemException e1) {
			_log.error(e1.getMessage());
		}

		if (cmd.equals(EECConstants.ADMIN_SEARCH)
				|| cmd.equals(EECConstants.USER_SEARCH)) {
			searchOrders(actionRequest, actionResponse);
		} else if (cmd.equals(EECConstants.RESENDMAIL)) {
			sendEmail(actionRequest, orderId, themeDisplay.getCompanyId(), cmd,
					StringPool.BLANK, StringPool.BLANK, serviceContext);
			actionResponse.sendRedirect(redirectURL);
		} else {
			try {
				if (orderId > 0) {
					order = OrderLocalServiceUtil.getOrder(orderId);
				} else {
					OrderItem orderItem = OrderItemLocalServiceUtil
							.getOrderItem(Long.valueOf(orderItemIds));
					order = OrderLocalServiceUtil.getOrder(orderItem
							.getOrderId());
					orderId = order.getOrderId();
				}
				totalOrderAmountBeforeUpdate = EECUtil.calculateTotal(order);
				serviceContext = ServiceContextFactory.getInstance(
						Order.class.getName(), actionRequest);
				coupon = CouponLocalServiceUtil.getCoupon(order
						.getCouponCodes());
			} catch (PortalException e) {
				_log.info(e.getClass());
			} catch (SystemException e) {
				_log.info(e.getClass());
			} catch (NumberFormatException e) {
				_log.info(e.getClass());
			}

			if (cmd.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)) {

				try {

					double remainingOrderActualSubtotal = remainingOrderActualSubtotal(
							order, coupon, actionRequest);
					MyWalletLocalServiceUtil.updateMyWallet(order
							.getCompanyId(), order.getUserId(), orderId,
							totalOrderAmountBeforeUpdate,
							remainingOrderActualSubtotal,
							EECUtil.getCanceledOrderItems(order.getCompanyId(),
									actionRequest), order.getShippingCountry(),
							order.getShippingState());
					
				} catch (Exception e) {
					_log.info(e.getClass());
					if (e instanceof WalletLimitException) {
						StringBundler couponApplicableItems = new StringBundler();
						getCouponApplicableItems(order, coupon,
								couponApplicableItems, actionRequest);
						SessionErrors.add(actionRequest, e.getClass());
						SessionMessages.add(actionRequest,
								"couponApplicableItems",
								couponApplicableItems.toString());
						SessionMessages.add(actionRequest, "minimumOrder",
								String.valueOf(coupon.getMinOrder()));
						if (Validator.isNotNull(redirectURL))
							actionResponse.sendRedirect(redirectURL);
						return;
					}
				}
			}
			Date date = null;
			if(cmd.equalsIgnoreCase(EECConstants.ORDER_STATUS_ONSHIPPING)){
				String dateString = ParamUtil.getString(actionRequest,"shippingDate");
				
				String timeString = ParamUtil.getString(actionRequest,"shippingTime");
				
				String dateTimeString = dateString+" "+timeString;
				System.out.println(dateTimeString+":::::dateTimeString:::::");
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	            try{
	            date = formatter.parse(dateTimeString);
	            }catch(Exception e){
	            	
	            }
	           
			}else{
				date = getDate(actionRequest, 0l);
			}
			String history = order.getHistory();

			history = EECUtil.updateXmlContent(history, cmd, date, location,
					orderItemIds, shippingTrackerId, comment, reason,
					actionRequest);
			boolean cancelledByUser = (order.getUserId() == serviceContext
					.getUserId()) ? true : false;
			changeStatus(orderId, shippingTrackerId, cmd, history,
					cancelledByUser, actionRequest, serviceContext);

			sendEmail(actionRequest, orderId, themeDisplay.getCompanyId(), cmd,
					curStatus, cmd, serviceContext);
			actionResponse.setRenderParameter("tabs1", cmd);
		}

		super.processAction(actionRequest, actionResponse);
	}

	public double remainingOrderActualSubtotal(Order order, Coupon coupon,
			ActionRequest actionRequest) throws PortalException,
			SystemException, DocumentException {

		List<OrderItem> totalOrderItems = OrderItemLocalServiceUtil
				.getOrderItems(order.getOrderId());
		Map<CartItem, Integer> totalOrderItemsMap = new HashMap<CartItem, Integer>();
		for (OrderItem orderItem : totalOrderItems) {
			totalOrderItemsMap.put(
					new CartItemImpl(ProductInventoryLocalServiceUtil
							.getProductInventory(Long.valueOf(orderItem
									.getProductInventoryId()))), orderItem
							.getQuantity());
		}
		Map<CartItem, Integer> cancelledOrderItemsMap = EECUtil
				.getCanceledOrderItems(order.getCompanyId(), actionRequest);
		double remainingOrderActualSubtotal = EECUtil
				.calculateActualSubtotal(EECUtil.getCouponApplicableItems(
						coupon, totalOrderItemsMap))
				- EECUtil.calculateActualSubtotal(EECUtil
						.getCouponApplicableItems(coupon,
								cancelledOrderItemsMap));

		return remainingOrderActualSubtotal;
	}

	protected void sendEmail(ActionRequest actionRequest, long orderId,
			long companyId, String cmd, String oldStatus, String newStatus,
			ServiceContext serviceContext) {
		try {
			String orderItemDetails = EmailUtil.getItemDetails(actionRequest,
					orderId, companyId, cmd);
			String subject = ContentUtil.get(PortletProps.get(cmd
					+ ".emailConfirmation.subject"));

			EmailUtil.sendMail(actionRequest, orderId, cmd, orderItemDetails,
					subject);
			SMSUtil.sendSMS(actionRequest, orderId, cmd);

		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PortletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void changeStatus(long orderId, String shippingTrackerId,
			String cmd, String history, boolean cancelByUser,
			ActionRequest actionRequest, ServiceContext serviceContext) {
		try {
			long[] orderItemIds = StringUtil.split(
					ParamUtil.getString(actionRequest, "itemIds"), 0L);
			System.out.println("orderItemIds::::::::"+Arrays.toString(orderItemIds));
			Date deliveredDate = null;
			long canceledByUserId = 0l;
			Date expectedDeliveryDate = null;
			if (cmd.equals(EECConstants.ORDER_STATUS_DELIVERED))
				deliveredDate = new Date();
			if (cmd.equals(EECConstants.ORDER_STATUS_CANCELLED))
				canceledByUserId = serviceContext.getUserId();
			for (int i = 0; i < orderItemIds.length; i++) {
				int quantityToBeCancelled = 0;
				String newStatus = cmd;
				OrderItem orderItem = OrderItemLocalServiceUtil
						.getOrderItem(orderItemIds[i]);
				int quantity = orderItem.getQuantity();
				expectedDeliveryDate = getDate(actionRequest, orderItemIds[i]);
				if (cmd.equals(EECConstants.ORDER_STATUS_CANCELLED)) {
					quantityToBeCancelled = ParamUtil.getInteger(actionRequest,
							"quantity" + orderItemIds[i], 0);
					quantity = quantity - quantityToBeCancelled;
					updateStock(
							Long.valueOf(orderItem.getProductInventoryId()),
							quantityToBeCancelled);
					if (quantity > 0)
						newStatus = null;
				}
				OrderItemLocalServiceUtil.updateOrderItemStatus(
						orderItemIds[i], newStatus, shippingTrackerId,
						expectedDeliveryDate, deliveredDate, canceledByUserId,
						quantity, cancelByUser);
				OrderLocalServiceUtil.updateOrder(orderId, null, null, history);
				if (cmd.equals(EECConstants.ORDER_STATUS_CANCELLED))
					CancelledOrderItemsLocalServiceUtil
							.updateCancelledOrderItems(orderItem.getOrderId(),
									orderItemIds[i], orderItem.getPrice(),
									quantityToBeCancelled, serviceContext);

			}
		} catch (NoSuchOrderException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (NoSuchOrderItemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}

	}

	public Date getDate(ActionRequest actionRequest, long orderItemId) {
		ThemeDisplay themedisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		int month = ParamUtil.getInteger(actionRequest, (orderItemId > 0) ? "m"
				+ orderItemId : "m");
		int day = ParamUtil.getInteger(actionRequest, (orderItemId > 0) ? "d"
				+ orderItemId : "d");
		int year = ParamUtil.getInteger(actionRequest, (orderItemId > 0) ? "y"
				+ orderItemId : "y");
		int hour = ParamUtil.getInteger(actionRequest,
				(orderItemId > 0) ? "hour" + orderItemId : "hour");
		int minute = ParamUtil.getInteger(actionRequest,
				(orderItemId > 0) ? "min" + orderItemId : "min");
		int amPm = ParamUtil.getInteger(actionRequest,
				(orderItemId > 0) ? "ampm" + orderItemId : "ampm");
		if (amPm == Calendar.PM) {
			hour += 12;
		}
		try {
			return PortalUtil.getDate(month, day, year, hour, minute,
					themedisplay.getUser().getTimeZone(),
					(Class<? extends PortalException>) null);
		} catch (PortalException pe) {
			return null;
		}
	}

	private void searchOrders(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		long companyId = themeDisplay.getCompanyId();
		String orderNumber = ParamUtil.getString(actionRequest, "orderNumber");
		List<Order> orderList = null;
		if (cmd.equals(EECConstants.ADMIN_SEARCH)) {

			try {
				orderList = OrderLocalServiceUtil.findByOrderNumber(companyId,
						orderNumber);
			} catch (Exception e) {
			}
		} else if (cmd.equals(EECConstants.USER_SEARCH)) {
			try {
				orderList = OrderLocalServiceUtil.findByOrderNumberUserId(
						companyId, themeDisplay.getUserId(), orderNumber);
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
		}
		try {
			PortletURL portletURL = EECUtil.getPortletURL(
					actionRequest,
					actionResponse.getNamespace().substring(1,
							actionResponse.getNamespace().length() - 1),
					themeDisplay.getPlid(), ActionRequest.RENDER_PHASE,
					"/html/eec/common/ordertracker/view_order.jsp", null,
					LiferayWindowState.MAXIMIZED, LiferayPortletMode.VIEW);
			portletURL.setParameter("orderId",
					String.valueOf(orderList.get(0).getOrderId()));
			actionResponse.sendRedirect(portletURL.toString());

		} catch (IndexOutOfBoundsException exception) {
			SessionErrors.add(actionRequest, "NoRecord");
		} catch (IOException e) {
			SessionErrors.add(actionRequest, "NoRecord");
		}
	}

	public void updateStock(long productInventoryId, int quantity)
			throws PortalException, NumberFormatException, SystemException {
		ProductInventory inventory = ProductInventoryLocalServiceUtil
				.fetchProductInventory(productInventoryId);
		long stockQuantity = (inventory.getQuantity() + quantity);
		inventory.setQuantity(stockQuantity);
		ProductInventoryLocalServiceUtil.updateProductInventory(inventory);
	}

	public void getCouponApplicableItems(Order order, Coupon coupon,
			StringBundler couponApplicableItems, ActionRequest actionRequest) {
		try {
			Map<CartItem, Integer> cancelledOrderItemsMap = EECUtil
					.getCanceledOrderItems(order.getCompanyId(), actionRequest);
			cancelledOrderItemsMap = EECUtil.getCouponApplicableItems(coupon,
					cancelledOrderItemsMap);
			Iterator<Map.Entry<CartItem, Integer>> itr = cancelledOrderItemsMap
					.entrySet().iterator();

			while (itr.hasNext()) {
				Map.Entry<CartItem, Integer> entry = itr.next();
				CartItem cartItem = entry.getKey();
				ProductInventory productInventory = cartItem
						.getProductInventory();
				ProductDetails productDetails = ProductDetailsLocalServiceUtil
						.getProduct(productInventory.getProductDetailsId());
				couponApplicableItems.append(productDetails.getName());
				couponApplicableItems.append(StringPool.COMMA);

			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(OrderTrackerAction.class);
}
