package com.esquare.ecommerce.product.action;

import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.esquare.ecommerce.DuplicateProductNameException;
import com.esquare.ecommerce.NoSuchCatalogException;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.model.EcustomField;
import com.esquare.ecommerce.model.EcustomFieldValue;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductImages;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.ShippingRegion;
import com.esquare.ecommerce.model.impl.EcustomFieldValueImpl;
import com.esquare.ecommerce.model.impl.ProductDetailsImpl;
import com.esquare.ecommerce.model.impl.ProductImagesImpl;
import com.esquare.ecommerce.model.impl.ProductInventoryImpl;
import com.esquare.ecommerce.product.util.ECCProductDetailsUtil;
import com.esquare.ecommerce.service.CatalogLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil;
import com.esquare.ecommerce.service.EcustomFieldValueLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.ProductImagesLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.util.BulkUploadUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.esquare.ecommerce.model.bulkdataupload.ErrorPageModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadCatalogModel;
import com.esquare.ecommerce.model.bulkdataupload.UploadCustomModel;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ProductAction extends MVCPortlet {
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(renderRequest, EECConstants.CMD);
		long parentCatalogId = 0;
		;
		long catalogId = ParamUtil.getLong(renderRequest, "catalogId");
		PortletSession pSession = renderRequest.getPortletSession();
		String selestedCatalogId = (String) pSession.getAttribute("CATALOG_ID");
		if (Validator.isNotNull(cmd) || Validator.equals(cmd, EECConstants.ADD)
				|| Validator.equals(cmd, EECConstants.EDIT)
				|| Validator.equals(cmd, "defineField")
				|| Validator.equals(cmd, EECConstants.BULK_UPLOAD)) {
			if (catalogId > 0) {
				parentCatalogId = catalogId;
			} else if (Validator.isNotNull(selestedCatalogId)) {
				parentCatalogId = Long.parseLong(selestedCatalogId);
			}
			if (parentCatalogId > 0) {
				List<EcustomField> list = null;
				try {
					// list =
					// EcustomFieldLocalServiceUtil.findByCatalogId(parentCatalogId);
				} catch (Exception e) {
					_log.info(e.getClass());
				}
				pSession.setAttribute("DEFINE_FIELD_LIST", list);
			} else {
				pSession.setAttribute("DEFINE_FIELD_LIST", null);
			}

			if (Validator.equals(cmd, EECConstants.BULK_UPLOAD)) {
				viewTemplate = "/html/eec/common/product/bulk_product.jsp";
			} else {
				viewTemplate = "/html/eec/common/product/edit_product.jsp";
			}

		} else {
			viewTemplate = "/html/eec/common/product/view_product.jsp";
		}
		try {
			ECCProductDetailsUtil.getProductDetail(renderRequest,
					renderResponse);
		} catch (NumberFormatException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		super.doView(renderRequest, renderResponse);
	}

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		try {
			if (cmd.equals(EECConstants.ADD)) {
				updateProduct(actionRequest, actionResponse,themeDisplay);
			} else if (cmd.equals(EECConstants.DELETE)) {
				deleteProduct(actionRequest, actionResponse,themeDisplay);
			} else if (cmd.equals(EECConstants.SEARCH)) {
				searchProduct(actionRequest, actionResponse,themeDisplay);
			} else if (cmd.equals("stockAction")) {
				stockAction(actionRequest, actionResponse,themeDisplay);
			} else if (cmd.equals(EECConstants.BULK_UPLOAD)) {
				getBulkUploadFromExcel(actionRequest, actionResponse,themeDisplay);
			}
		} catch (Exception e) {
			 e.printStackTrace();
			_log.info(e.getClass());
		}
	}

	private void searchProduct(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay) {
	
		String productName = ParamUtil.getString(actionRequest, "productName");
		List<ProductDetails> searchProductList = null;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
					ProductDetails.class,
					PortalClassLoaderUtil.getClassLoader());
			if (Validator.isNotNull(productName)) {
				dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId",
						themeDisplay.getCompanyId()));
				dynamicQuery.add(RestrictionsFactoryUtil.like("name", "%"
						+ productName + "%"));
				searchProductList = ProductDetailsLocalServiceUtil
						.dynamicQuery(dynamicQuery);
			}
			actionRequest
					.setAttribute("SEARCH_PRODUCT_LIST", searchProductList);
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/product/view_product.jsp");
		} catch (Exception e) {
			_log.info(e.getClass());
		}
	}

	private void updateProduct(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay2) throws Exception {
		UploadPortletRequest uploadRequest = PortalUtil
				.getUploadPortletRequest(actionRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		long productDetailsId = ParamUtil.getLong(uploadRequest,
				"productDetailsId");
		long catalogId = ParamUtil.getLong(uploadRequest, "catalogId");
		String name = ParamUtil.getString(uploadRequest, "name");
		String description = ParamUtil.getString(uploadRequest, "description");
		String sku = ParamUtil.getString(uploadRequest, "sku");
		double price = ParamUtil.getDouble(uploadRequest, "price");
		double discount = ParamUtil.getDouble(uploadRequest, "discount");
		long quantity = ParamUtil.getLong(uploadRequest, "quantity");
		boolean taxes = ParamUtil.getBoolean(uploadRequest, "taxes");
		boolean freeShipping = ParamUtil.getBoolean(uploadRequest,
				"freeShipping");
		double weight = ParamUtil.getDouble(uploadRequest, "weight");
		boolean outOfStockPurchase = ParamUtil.getBoolean(uploadRequest,
				"outOfStockPurchase");
		boolean visibility = ParamUtil.getBoolean(uploadRequest, "visibility");

		boolean similarProducts = ParamUtil.getBoolean(uploadRequest,
				"similarProducts");
		String customFieldRecId = ParamUtil.getString(uploadRequest,
				"customFieldRecId");
		String customValueRecId = ParamUtil.getString(uploadRequest,
				"customValueRecId");

		/*
		 * String limitCatalogs = ParamUtil.getString(uploadRequest,
		 * "limitCatalogs"); limitCatalogs=getSubString(limitCatalogs);
		 */

		String limitProducts = ParamUtil.getString(uploadRequest,
				"limitProducts");
		// limitProducts=getSubString(limitProducts);
		File imageFile = uploadRequest.getFile("imageFile");
		String imageFileName = uploadRequest.getFileName("imageFile");
		InputStream inputStream = null;
		inputStream = uploadRequest.getFileAsStream("imageFile");
		ProductDetails productDetails = new ProductDetailsImpl();
		productDetails.setCatalogId(catalogId);
		productDetails.setCompanyId(themeDisplay.getCompanyId());
		productDetails.setUserId(themeDisplay.getUserId());
		productDetails.setName(name);
		productDetails.setDescription(description);
		productDetails.setModifiedDate(new Date());
		productDetails.setVisibility(visibility);

		ProductInventory productInventory = new ProductInventoryImpl();
		productInventory.setCompanyId(themeDisplay.getCompanyId());
		productInventory.setSku(sku);
		productInventory.setPrice(price);
		productInventory.setDiscount(discount);
		productInventory.setQuantity(quantity);
		productInventory.setTaxes(taxes);
		productInventory.setFreeShipping(freeShipping);
		productInventory.setWeight(weight);
		productInventory.setOutOfStockPurchase(outOfStockPurchase);
		productInventory.setSimilarProducts(similarProducts);
		productInventory.setVisibility(visibility);
		// productInventory.setLimitCatalogs(limitCatalogs);
		productInventory.setLimitProducts(limitProducts);
		productInventory.setModifiedDate(new Date());
		if (Validator.isNotNull(customFieldRecId))
			productInventory.setCustomFieldRecId(Long
					.parseLong(customFieldRecId));

		ProductImages productImages = new ProductImagesImpl();
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				DLFileEntry.class.getName(), actionRequest);
		Catalog catalog = CatalogLocalServiceUtil.getCatalog(catalogId);
		if (productDetailsId <= 0) {
			try {
				// add process

				// Product Details
				productDetails.setProductDetailsId(CounterLocalServiceUtil
						.increment(this.getClass().getName()));
				productDetails.setCreateDate(new Date());

				// validate(themeDisplay.getCompanyId(),name,catalogId);
				ProductDetails pDetails = ProductDetailsLocalServiceUtil
						.addProductDetails(productDetails);

				// Product Inventory Details
				productInventory.setInventoryId(CounterLocalServiceUtil
						.increment(this.getClass().getName()));
				productInventory.setProductDetailsId(pDetails
						.getProductDetailsId());
				productInventory.setCreateDate(new Date());
				ProductInventory inventory = ProductInventoryLocalServiceUtil
						.addProductInventory(productInventory);
				// Product Custom Fields Details

				if (Validator.isNotNull(customFieldRecId)) {
					EcustomField ecustomField = EcustomFieldLocalServiceUtil
							.getEcustomField(Long.parseLong(customFieldRecId));
					if (Validator.isNotNull(ecustomField)) {
						List<ShippingRegion> shipRegionlist = EECUtil
								.readXMLContent(ecustomField.getXml(),
										PortletPropsValues.CUSTOM_FIELD_TAG);
						EcustomFieldValue cfValue = new EcustomFieldValueImpl();
						long recId = CounterLocalServiceUtil
								.increment("EcustomFieldValue.class");
						cfValue.setCustomValueRecId(recId);
						cfValue.setCompanyId(themeDisplay.getCompanyId());
						cfValue.setInventoryId(inventory.getInventoryId());

						String xml = StringPool.BLANK;
						StringBuffer sb_fieldIds = new StringBuffer();
						StringBuffer sb_KeyName = new StringBuffer();
						StringBuffer sb_keyValue = new StringBuffer();
						for (int i = 0; i < shipRegionlist.size(); i++) {
							sb_fieldIds.append(shipRegionlist.get(i).getId());
							sb_fieldIds
									.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
							// sb_KeyName.append(shipRegionlist.get(i).getName());
							sb_KeyName.append(shipRegionlist.get(i).getId());
							sb_KeyName
									.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
							String keyValue = ParamUtil.getString(
									uploadRequest, shipRegionlist.get(i)
											.getName());
							if (Validator.isNotNull(keyValue))
								sb_keyValue.append(keyValue);
							else
								sb_keyValue
										.append(PortletPropsValues.DEFINE_FIELDS_NOTAVAILABLE);
							sb_keyValue
									.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
						}
						xml = EECUtil
								.addXmlContent(
										StringPool.BLANK,
										PortletPropsValues.CUSTOM_FIELD_TAG,
										PortletPropsValues.ID_ATTRIBUTE,
										sb_fieldIds
												.toString()
												.split(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR),
										sb_fieldIds
												.toString()
												.split(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR),
										sb_keyValue
												.toString()
												.split(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR));
						cfValue.setXml(xml);
						EcustomFieldValueLocalServiceUtil
								.addEcustomFieldValue(cfValue);
					}

				}

				// Product Image Details
				productImages.setRecId(CounterLocalServiceUtil.increment(this
						.getClass().getName()));
				productImages.setInventoryId(inventory.getInventoryId());

				imageFileName = imageFileName + productImages.getRecId();
				FileEntry dlFileEntry = DLAppServiceUtil.addFileEntry(
						themeDisplay.getScopeGroupId(), catalog.getFolderId(),
						imageFileName, MimeTypesUtil.getContentType(imageFile),
						imageFileName, description, StringPool.BLANK,
						inputStream, imageFile.length(), serviceContext);
				productImages.setImageId(dlFileEntry.getFileEntryId());
				ProductImagesLocalServiceUtil.addProductImages(productImages);
			} catch (Exception e) {
				if (e instanceof DuplicateProductNameException) {
					SessionErrors.add(actionRequest, e.getClass().getName());
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/product/edit_product.jsp");
				} else if (e instanceof NoSuchCatalogException) {
					SessionErrors.add(actionRequest, e.getClass().getName());
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/product/edit_product.jsp");
				} else {
					_log.info(e.getClass());
				}
			}

		} else {
			// Update process
			// Product Details
			ProductDetails pDetails = ProductDetailsLocalServiceUtil
					.getProductDetails(productDetailsId);
			productDetails.setProductDetailsId(productDetailsId);
			productDetails.setCreateDate(pDetails.getCreateDate());
			ProductDetailsLocalServiceUtil.updateProductDetails(productDetails);

			// Product Inventory Details
			ProductInventory pInventory = ProductInventoryLocalServiceUtil
					.findByProductDetailsId(productDetailsId);
			productInventory.setInventoryId(pInventory.getInventoryId());
			productInventory.setProductDetailsId(pInventory
					.getProductDetailsId());
			productInventory.setCreateDate(pInventory.getCreateDate());
			ProductInventoryLocalServiceUtil
					.updateProductInventory(productInventory);

			// Product Custom Fields Details

			if (Validator.isNotNull(customFieldRecId)
					|| Validator.isNotNull(customValueRecId)) {
				EcustomField ecustomField = EcustomFieldLocalServiceUtil
						.getEcustomField(Long.parseLong(customFieldRecId));
				EcustomFieldValue cfValue = null;
				try {
					cfValue = EcustomFieldValueLocalServiceUtil
							.getEcustomFieldValue(Long
									.parseLong(customValueRecId));
				} catch (Exception e) {
				}

				String xml = StringPool.BLANK;
				StringBuffer sb_fieldIds = new StringBuffer();
				StringBuffer sb_KeyName = new StringBuffer();
				StringBuffer sb_keyValue = new StringBuffer();

				List<ShippingRegion> shipRegionlist = EECUtil.readXMLContent(
						ecustomField.getXml(),
						PortletPropsValues.CUSTOM_FIELD_TAG);
				for (int i = 0; i < shipRegionlist.size(); i++) {
					sb_fieldIds.append(shipRegionlist.get(i).getId());
					sb_fieldIds
							.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
					// sb_KeyName.append(shipRegionlist.get(i).getName());
					sb_KeyName.append(shipRegionlist.get(i).getId());
					sb_KeyName
							.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
					String keyValue = ParamUtil.getString(uploadRequest,
							shipRegionlist.get(i).getName());
					if (Validator.isNotNull(keyValue))
						sb_keyValue.append(keyValue);
					else
						sb_keyValue
								.append(PortletPropsValues.DEFINE_FIELDS_NOTAVAILABLE);
					sb_keyValue
							.append(EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR);
				}
				xml = EECUtil.addXmlContent(
						StringPool.BLANK,
						PortletPropsValues.CUSTOM_FIELD_TAG,
						PortletPropsValues.ID_ATTRIBUTE,
						sb_fieldIds.toString().split(
								EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR),
						sb_fieldIds.toString().split(
								EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR),
						sb_keyValue.toString().split(
								EECConstants.PRODUCT_CUSTOM_FIELD_SEPERATOR));
				if (Validator.isNotNull(cfValue)) {
					cfValue.setCustomValueRecId(cfValue.getCustomValueRecId());
					cfValue.setCompanyId(themeDisplay.getCompanyId());
					cfValue.setInventoryId(pInventory.getInventoryId());

					cfValue.setXml(xml);
					EcustomFieldValueLocalServiceUtil
							.updateEcustomFieldValue(cfValue);
				} else {
					cfValue = new EcustomFieldValueImpl();
					long recId = CounterLocalServiceUtil
							.increment("EcustomFieldValue.class");
					cfValue.setCustomValueRecId(recId);
					cfValue.setCompanyId(themeDisplay.getCompanyId());
					cfValue.setInventoryId(pInventory.getInventoryId());
					cfValue.setXml(xml);
					EcustomFieldValueLocalServiceUtil
							.addEcustomFieldValue(cfValue);
				}

			}

			// Product Image Details
			ProductImages pImages = ProductImagesLocalServiceUtil
					.findByInventoryId(pInventory.getInventoryId()).get(0);
			productImages.setRecId(pImages.getRecId());
			productImages.setInventoryId(pImages.getInventoryId());
			FileEntry dlFileEntry = null;
			if (Validator.isNotNull(imageFileName)) {
				imageFileName = imageFileName
						+ String.valueOf(pImages.getRecId());
				dlFileEntry = DLAppServiceUtil.updateFileEntry(
						pImages.getImageId(), imageFileName,
						MimeTypesUtil.getContentType(imageFile), imageFileName,
						imageFileName, StringPool.BLANK, false, inputStream,
						imageFile.length(), serviceContext);
				productImages.setImageId(dlFileEntry.getFileEntryId());
			} else {
				dlFileEntry = DLAppServiceUtil.getFileEntry(pImages
						.getImageId());
				productImages.setImageId(dlFileEntry.getFileEntryId());
			}
			ProductImagesLocalServiceUtil.updateProductImages(productImages);
			if (catalogId != pDetails.getCatalogId()) {
				DLAppServiceUtil.moveFileEntry(dlFileEntry.getFileEntryId(),
						catalog.getFolderId(), serviceContext);
			}

		}
		String portletName = (String) actionRequest
				.getAttribute(WebKeys.PORTLET_ID);
		PortletURL redirectURL = PortletURLFactoryUtil
				.create(PortalUtil.getHttpServletRequest(actionRequest),
						portletName, themeDisplay.getLayout().getPlid(),
						PortletRequest.RENDER_PHASE);
		try {
			actionResponse.sendRedirect(redirectURL.toString());
		} catch (IOException e) {
			_log.info(e.getClass());
		}
		CacheRegistryUtil.clear(EcustomFieldValueImpl.class.getName());
	}

	private void deleteProduct(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay) {
		long[] deleteProductIds = StringUtil.split(
		ParamUtil.getString(actionRequest, "deleteProductIds"), 0L);
		if (Validator.isNotNull(deleteProductIds)) {
			for (int i = 0; i < deleteProductIds.length; i++) {
				try {
					ProductInventory pInventory = ProductInventoryLocalServiceUtil
							.findByProductDetailsId(deleteProductIds[i]);
					
					List<ProductImages> productImagesList = ProductImagesLocalServiceUtil.findByInventoryId(pInventory.getInventoryId());
					if(Validator.isNotNull(productImagesList) && productImagesList.size() > 0) {
						ProductImages pImages = productImagesList.get(0);
						DLAppServiceUtil.deleteFileEntry(pImages.getImageId());
						ProductImagesLocalServiceUtil.deleteProductImages(pImages.getRecId());
					}
					ProductInventoryLocalServiceUtil
							.deleteProductInventory(pInventory.getInventoryId());
					ProductDetailsLocalServiceUtil.deleteProductDetails(deleteProductIds[i]);
				} catch (Exception e) {
				}
			}
		} else {
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/product/view_product.jsp");
		}

	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request
				.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = request.getParameter(EECConstants.AUTOCOMPLETE);
		String cmdSearch = request
				.getParameter(EECConstants.AUTOCOMPLETE_SEARCH);
		long companyId = ParamUtil.getLong(request, "companyId");
		PortletSession pSession = request.getPortletSession();
		response.setContentType("text/javascript");
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
		if (cmd.equals("CUSTOMFIELD")) {
			String ecustomFieldName = request.getParameter("ecustomFieldName");
			EcustomField ecustomField = null;
			try {
				ecustomField = EcustomFieldLocalServiceUtil.findByTitle(
						companyId, ecustomFieldName);
			} catch (Exception e) {
				_log.info(e.getClass());
			}
			pSession.setAttribute("CUSTOM_FIELD_OBJECT", ecustomField,
					PortletSession.PORTLET_SCOPE);
		} else if (cmd.equals("SKU")) {
			try {
				long productInventoryId = ParamUtil.getLong(request,
						"productInventoryId");
				String sku = request.getParameter("sku");
				boolean isSKU = false;
				ProductInventory productInventory = ProductInventoryLocalServiceUtil
						.fetchBySKU(companyId, sku);
				if (productInventoryId < 0) {
					if (productInventory != null) {
						isSKU = true;
					}
				} else {
					if (productInventory != null
							&& productInventory.getInventoryId() != productInventoryId) {
						isSKU = true;
					}
				}
				jsonObject.put("retVal", isSKU);
				PrintWriter writer = response.getWriter();
				writer.write(jsonObject.toString());

			} catch (Exception e) {
				_log.info(e.getClass());
			}

		} else if (cmd.equals("PRODUCT_NAME")) {
			try {
				long productDetailsId = ParamUtil.getLong(request,
						"productDetailsId");
				String productName = request.getParameter("productName");
				boolean isName = false;
				ProductDetails productDetails = ProductDetailsLocalServiceUtil
						.fetchByName(companyId, productName);
				if (productDetailsId < 0) {
					if (productDetails != null) {
						isName = true;
					}
				} else {
					if (productDetails != null
							&& productDetails.getProductDetailsId() != productDetailsId) {
						isName = true;
					}
				}
				jsonObject.put("retVal", isName);
				PrintWriter writer = response.getWriter();
				writer.write(jsonObject.toString());
			} catch (Exception e) {
				_log.info(e.getClass());
			}

		} else {
			String query = request.getParameter("q");
			JSONObject json = JSONFactoryUtil.createJSONObject();
			JSONArray results = null;
			if (cmd.equals(EECConstants.PRODUCT)) {
				results = productList(themeDisplay.getCompanyId(), query,
						cmdSearch);
			}/*
			 * else if (cmd.equals(EECConstants.CATALOG)) { results =
			 * cataLogList(companyId, query); }
			 */
			json.put("response", results);
			PrintWriter writer = response.getWriter();
			writer.println(json.toString());
		}
	}

	private JSONArray productList(long companyId, String query, String cmdSearch) {

		JSONArray results = JSONFactoryUtil.createJSONArray();
		List<ProductDetails> filteredEntries = null;
		try {
			filteredEntries = getFilteredProductDetails(companyId, query);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		for (ProductDetails entry : filteredEntries) {
			JSONObject listEntry = JSONFactoryUtil.createJSONObject();
			if ((Validator.isNotNull(cmdSearch))
					&& (cmdSearch.equals(EECConstants.PRODUCT))) {
				listEntry.put("name", entry.getName());
			} else {
				listEntry.put("name",
						entry.getName() + " (" + entry.getProductDetailsId()
								+ ")");
			}
			results.put(listEntry);
		}
		return results;
	}

	@SuppressWarnings("unchecked")
	private List<ProductDetails> getFilteredProductDetails(long companyId,
			String query) throws SystemException {
		List<ProductDetails> ProductDetailsList = null;
		DynamicQuery dynamicQuery = null;
		Criterion criterion = null;
		ProductDetailsList = ProductDetailsLocalServiceUtil
				.getProductDetailList(companyId);
		if (query == null || "*".equals(query) || query.contains("*")) {
			return ProductDetailsList;
		}
		dynamicQuery = DynamicQueryFactoryUtil.forClass(ProductDetails.class);
		criterion = RestrictionsFactoryUtil.like("companyId", companyId);
		criterion = RestrictionsFactoryUtil.and(criterion,
				RestrictionsFactoryUtil
						.like("name", query + StringPool.PERCENT));
		dynamicQuery.add(criterion);
		ProductDetailsList = (List<ProductDetails>) ProductDetailsLocalServiceUtil
				.dynamicQuery(dynamicQuery);
		return ProductDetailsList;
	}

	public void stockAction(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay) throws Exception {
		String outofStock = ParamUtil.getString(actionRequest, "outofStock");
		String lessStock = ParamUtil.getString(actionRequest, "lessStock");
		List<ProductDetails> productDetailList = null;
		if (Validator.isNotNull(outofStock)) {
			int outofStocks = Integer.parseInt(outofStock);
			try {
				productDetailList = ProductDetailsLocalServiceUtil
						.getOutofStockLessStock(
								PortalUtil.getCompanyId(actionRequest),
								outofStocks);

			} catch (SystemException e) {
				_log.info(e.getClass());
			}

		} else if (Validator.isNotNull(lessStock)) {
			int lesStock = Integer.parseInt(lessStock);
			try {
				productDetailList = ProductDetailsLocalServiceUtil
						.getOutofStockLessStock(
								PortalUtil.getCompanyId(actionRequest),
								lesStock);

			} catch (SystemException e) {
				_log.info(e.getClass());
			}

		}
		actionRequest.setAttribute("SEARCH_PRODUCT_LIST", productDetailList);
		actionResponse.setRenderParameter("jspPage",
				"/html/eec/common/product/view_product.jsp");

	}

	public void getBulkUploadFromExcel(ActionRequest actionRequest,
			ActionResponse actionResponse, ThemeDisplay themeDisplay) throws Exception {
		
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
			File file = uploadRequest.getFile("filename");
			File[] imageFiles = uploadRequest.getFiles("imageFiles");
			String filesName = ParamUtil.getString(uploadRequest,"fileNames");
			String[] fileNames = filesName.split(",");
			HashMap<String ,File> map = new HashMap<String,File>();
			for(int i=0; i<fileNames.length;i++){
					map.put(fileNames[i], imageFiles[i]);
		} 
		
		ErrorPageModel errorPage = new ErrorPageModel();
		
		List<UploadCatalogModel> listUploadCatalogModel = BulkUploadUtil.bulkUpload(file, themeDisplay.getCompanyId(), errorPage);
		
		List<UploadCustomModel> listUploadCutomModel = BulkUploadUtil.bulkCustomFeildUpload(file);
		
		ServiceContext context = null;
		try {
			context = ServiceContextFactory.getInstance(DLFolder.class.getName(), actionRequest);
			context.setAddGroupPermissions(true);
			context.setAddGuestPermissions(true);
		} catch (PortalException e1) {
			e1.printStackTrace();
		}
		if(!errorPage.isError()){
			BulkUploadUtil.uplodaProduct(listUploadCatalogModel, themeDisplay, context, map, errorPage);
			BulkUploadUtil.uploadCustomField(listUploadCutomModel, themeDisplay.getCompanyId());
			SessionErrors.add(actionRequest, "XLS_ERROR", errorPage.getErrorMessage());
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + 
					 SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		} else {
			SessionErrors.add(actionRequest, "XLS_ERROR", errorPage.getErrorMessage());
			SessionMessages.add(actionRequest, PortalUtil.getPortletId(actionRequest) + 
					 SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
			actionResponse.setRenderParameter("jspPage","/html/eec/common/product/errorPage.jsp");
		}
		
	}
	
	

	private static Log _log = LogFactoryUtil.getLog(ProductAction.class);
}
