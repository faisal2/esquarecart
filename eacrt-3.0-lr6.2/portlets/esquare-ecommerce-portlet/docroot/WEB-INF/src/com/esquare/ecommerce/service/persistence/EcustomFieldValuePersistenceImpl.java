/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchEcustomFieldValueException;
import com.esquare.ecommerce.model.EcustomFieldValue;
import com.esquare.ecommerce.model.impl.EcustomFieldValueImpl;
import com.esquare.ecommerce.model.impl.EcustomFieldValueModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the ecustom field value service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see EcustomFieldValuePersistence
 * @see EcustomFieldValueUtil
 * @generated
 */
public class EcustomFieldValuePersistenceImpl extends BasePersistenceImpl<EcustomFieldValue>
	implements EcustomFieldValuePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EcustomFieldValueUtil} to access the ecustom field value persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EcustomFieldValueImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueModelImpl.FINDER_CACHE_ENABLED,
			EcustomFieldValueImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueModelImpl.FINDER_CACHE_ENABLED,
			EcustomFieldValueImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_INVENTORYID = new FinderPath(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueModelImpl.FINDER_CACHE_ENABLED,
			EcustomFieldValueImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByInventoryId",
			new String[] { Long.class.getName(), Long.class.getName() },
			EcustomFieldValueModelImpl.COMPANYID_COLUMN_BITMASK |
			EcustomFieldValueModelImpl.INVENTORYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_INVENTORYID = new FinderPath(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByInventoryId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the ecustom field value where companyId = &#63; and inventoryId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchEcustomFieldValueException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param inventoryId the inventory ID
	 * @return the matching ecustom field value
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldValueException if a matching ecustom field value could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue findByInventoryId(long companyId, long inventoryId)
		throws NoSuchEcustomFieldValueException, SystemException {
		EcustomFieldValue ecustomFieldValue = fetchByInventoryId(companyId,
				inventoryId);

		if (ecustomFieldValue == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", inventoryId=");
			msg.append(inventoryId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchEcustomFieldValueException(msg.toString());
		}

		return ecustomFieldValue;
	}

	/**
	 * Returns the ecustom field value where companyId = &#63; and inventoryId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param inventoryId the inventory ID
	 * @return the matching ecustom field value, or <code>null</code> if a matching ecustom field value could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue fetchByInventoryId(long companyId, long inventoryId)
		throws SystemException {
		return fetchByInventoryId(companyId, inventoryId, true);
	}

	/**
	 * Returns the ecustom field value where companyId = &#63; and inventoryId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param inventoryId the inventory ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching ecustom field value, or <code>null</code> if a matching ecustom field value could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue fetchByInventoryId(long companyId,
		long inventoryId, boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, inventoryId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					finderArgs, this);
		}

		if (result instanceof EcustomFieldValue) {
			EcustomFieldValue ecustomFieldValue = (EcustomFieldValue)result;

			if ((companyId != ecustomFieldValue.getCompanyId()) ||
					(inventoryId != ecustomFieldValue.getInventoryId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_ECUSTOMFIELDVALUE_WHERE);

			query.append(_FINDER_COLUMN_INVENTORYID_COMPANYID_2);

			query.append(_FINDER_COLUMN_INVENTORYID_INVENTORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(inventoryId);

				List<EcustomFieldValue> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"EcustomFieldValuePersistenceImpl.fetchByInventoryId(long, long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					EcustomFieldValue ecustomFieldValue = list.get(0);

					result = ecustomFieldValue;

					cacheResult(ecustomFieldValue);

					if ((ecustomFieldValue.getCompanyId() != companyId) ||
							(ecustomFieldValue.getInventoryId() != inventoryId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
							finderArgs, ecustomFieldValue);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EcustomFieldValue)result;
		}
	}

	/**
	 * Removes the ecustom field value where companyId = &#63; and inventoryId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param inventoryId the inventory ID
	 * @return the ecustom field value that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue removeByInventoryId(long companyId,
		long inventoryId)
		throws NoSuchEcustomFieldValueException, SystemException {
		EcustomFieldValue ecustomFieldValue = findByInventoryId(companyId,
				inventoryId);

		return remove(ecustomFieldValue);
	}

	/**
	 * Returns the number of ecustom field values where companyId = &#63; and inventoryId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param inventoryId the inventory ID
	 * @return the number of matching ecustom field values
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByInventoryId(long companyId, long inventoryId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_INVENTORYID;

		Object[] finderArgs = new Object[] { companyId, inventoryId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_ECUSTOMFIELDVALUE_WHERE);

			query.append(_FINDER_COLUMN_INVENTORYID_COMPANYID_2);

			query.append(_FINDER_COLUMN_INVENTORYID_INVENTORYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(inventoryId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INVENTORYID_COMPANYID_2 = "ecustomFieldValue.companyId = ? AND ";
	private static final String _FINDER_COLUMN_INVENTORYID_INVENTORYID_2 = "ecustomFieldValue.inventoryId = ?";

	public EcustomFieldValuePersistenceImpl() {
		setModelClass(EcustomFieldValue.class);
	}

	/**
	 * Caches the ecustom field value in the entity cache if it is enabled.
	 *
	 * @param ecustomFieldValue the ecustom field value
	 */
	@Override
	public void cacheResult(EcustomFieldValue ecustomFieldValue) {
		EntityCacheUtil.putResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueImpl.class, ecustomFieldValue.getPrimaryKey(),
			ecustomFieldValue);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
			new Object[] {
				ecustomFieldValue.getCompanyId(),
				ecustomFieldValue.getInventoryId()
			}, ecustomFieldValue);

		ecustomFieldValue.resetOriginalValues();
	}

	/**
	 * Caches the ecustom field values in the entity cache if it is enabled.
	 *
	 * @param ecustomFieldValues the ecustom field values
	 */
	@Override
	public void cacheResult(List<EcustomFieldValue> ecustomFieldValues) {
		for (EcustomFieldValue ecustomFieldValue : ecustomFieldValues) {
			if (EntityCacheUtil.getResult(
						EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
						EcustomFieldValueImpl.class,
						ecustomFieldValue.getPrimaryKey()) == null) {
				cacheResult(ecustomFieldValue);
			}
			else {
				ecustomFieldValue.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ecustom field values.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EcustomFieldValueImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EcustomFieldValueImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ecustom field value.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EcustomFieldValue ecustomFieldValue) {
		EntityCacheUtil.removeResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueImpl.class, ecustomFieldValue.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(ecustomFieldValue);
	}

	@Override
	public void clearCache(List<EcustomFieldValue> ecustomFieldValues) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EcustomFieldValue ecustomFieldValue : ecustomFieldValues) {
			EntityCacheUtil.removeResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
				EcustomFieldValueImpl.class, ecustomFieldValue.getPrimaryKey());

			clearUniqueFindersCache(ecustomFieldValue);
		}
	}

	protected void cacheUniqueFindersCache(EcustomFieldValue ecustomFieldValue) {
		if (ecustomFieldValue.isNew()) {
			Object[] args = new Object[] {
					ecustomFieldValue.getCompanyId(),
					ecustomFieldValue.getInventoryId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_INVENTORYID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID, args,
				ecustomFieldValue);
		}
		else {
			EcustomFieldValueModelImpl ecustomFieldValueModelImpl = (EcustomFieldValueModelImpl)ecustomFieldValue;

			if ((ecustomFieldValueModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_INVENTORYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ecustomFieldValue.getCompanyId(),
						ecustomFieldValue.getInventoryId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_INVENTORYID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_INVENTORYID,
					args, ecustomFieldValue);
			}
		}
	}

	protected void clearUniqueFindersCache(EcustomFieldValue ecustomFieldValue) {
		EcustomFieldValueModelImpl ecustomFieldValueModelImpl = (EcustomFieldValueModelImpl)ecustomFieldValue;

		Object[] args = new Object[] {
				ecustomFieldValue.getCompanyId(),
				ecustomFieldValue.getInventoryId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVENTORYID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID, args);

		if ((ecustomFieldValueModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_INVENTORYID.getColumnBitmask()) != 0) {
			args = new Object[] {
					ecustomFieldValueModelImpl.getOriginalCompanyId(),
					ecustomFieldValueModelImpl.getOriginalInventoryId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVENTORYID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_INVENTORYID, args);
		}
	}

	/**
	 * Creates a new ecustom field value with the primary key. Does not add the ecustom field value to the database.
	 *
	 * @param customValueRecId the primary key for the new ecustom field value
	 * @return the new ecustom field value
	 */
	@Override
	public EcustomFieldValue create(long customValueRecId) {
		EcustomFieldValue ecustomFieldValue = new EcustomFieldValueImpl();

		ecustomFieldValue.setNew(true);
		ecustomFieldValue.setPrimaryKey(customValueRecId);

		return ecustomFieldValue;
	}

	/**
	 * Removes the ecustom field value with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param customValueRecId the primary key of the ecustom field value
	 * @return the ecustom field value that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldValueException if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue remove(long customValueRecId)
		throws NoSuchEcustomFieldValueException, SystemException {
		return remove((Serializable)customValueRecId);
	}

	/**
	 * Removes the ecustom field value with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ecustom field value
	 * @return the ecustom field value that was removed
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldValueException if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue remove(Serializable primaryKey)
		throws NoSuchEcustomFieldValueException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EcustomFieldValue ecustomFieldValue = (EcustomFieldValue)session.get(EcustomFieldValueImpl.class,
					primaryKey);

			if (ecustomFieldValue == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEcustomFieldValueException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ecustomFieldValue);
		}
		catch (NoSuchEcustomFieldValueException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EcustomFieldValue removeImpl(EcustomFieldValue ecustomFieldValue)
		throws SystemException {
		ecustomFieldValue = toUnwrappedModel(ecustomFieldValue);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ecustomFieldValue)) {
				ecustomFieldValue = (EcustomFieldValue)session.get(EcustomFieldValueImpl.class,
						ecustomFieldValue.getPrimaryKeyObj());
			}

			if (ecustomFieldValue != null) {
				session.delete(ecustomFieldValue);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ecustomFieldValue != null) {
			clearCache(ecustomFieldValue);
		}

		return ecustomFieldValue;
	}

	@Override
	public EcustomFieldValue updateImpl(
		com.esquare.ecommerce.model.EcustomFieldValue ecustomFieldValue)
		throws SystemException {
		ecustomFieldValue = toUnwrappedModel(ecustomFieldValue);

		boolean isNew = ecustomFieldValue.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ecustomFieldValue.isNew()) {
				session.save(ecustomFieldValue);

				ecustomFieldValue.setNew(false);
			}
			else {
				session.merge(ecustomFieldValue);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EcustomFieldValueModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
			EcustomFieldValueImpl.class, ecustomFieldValue.getPrimaryKey(),
			ecustomFieldValue);

		clearUniqueFindersCache(ecustomFieldValue);
		cacheUniqueFindersCache(ecustomFieldValue);

		return ecustomFieldValue;
	}

	protected EcustomFieldValue toUnwrappedModel(
		EcustomFieldValue ecustomFieldValue) {
		if (ecustomFieldValue instanceof EcustomFieldValueImpl) {
			return ecustomFieldValue;
		}

		EcustomFieldValueImpl ecustomFieldValueImpl = new EcustomFieldValueImpl();

		ecustomFieldValueImpl.setNew(ecustomFieldValue.isNew());
		ecustomFieldValueImpl.setPrimaryKey(ecustomFieldValue.getPrimaryKey());

		ecustomFieldValueImpl.setCustomValueRecId(ecustomFieldValue.getCustomValueRecId());
		ecustomFieldValueImpl.setCompanyId(ecustomFieldValue.getCompanyId());
		ecustomFieldValueImpl.setInventoryId(ecustomFieldValue.getInventoryId());
		ecustomFieldValueImpl.setXml(ecustomFieldValue.getXml());

		return ecustomFieldValueImpl;
	}

	/**
	 * Returns the ecustom field value with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecustom field value
	 * @return the ecustom field value
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldValueException if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEcustomFieldValueException, SystemException {
		EcustomFieldValue ecustomFieldValue = fetchByPrimaryKey(primaryKey);

		if (ecustomFieldValue == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEcustomFieldValueException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ecustomFieldValue;
	}

	/**
	 * Returns the ecustom field value with the primary key or throws a {@link com.esquare.ecommerce.NoSuchEcustomFieldValueException} if it could not be found.
	 *
	 * @param customValueRecId the primary key of the ecustom field value
	 * @return the ecustom field value
	 * @throws com.esquare.ecommerce.NoSuchEcustomFieldValueException if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue findByPrimaryKey(long customValueRecId)
		throws NoSuchEcustomFieldValueException, SystemException {
		return findByPrimaryKey((Serializable)customValueRecId);
	}

	/**
	 * Returns the ecustom field value with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ecustom field value
	 * @return the ecustom field value, or <code>null</code> if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EcustomFieldValue ecustomFieldValue = (EcustomFieldValue)EntityCacheUtil.getResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
				EcustomFieldValueImpl.class, primaryKey);

		if (ecustomFieldValue == _nullEcustomFieldValue) {
			return null;
		}

		if (ecustomFieldValue == null) {
			Session session = null;

			try {
				session = openSession();

				ecustomFieldValue = (EcustomFieldValue)session.get(EcustomFieldValueImpl.class,
						primaryKey);

				if (ecustomFieldValue != null) {
					cacheResult(ecustomFieldValue);
				}
				else {
					EntityCacheUtil.putResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
						EcustomFieldValueImpl.class, primaryKey,
						_nullEcustomFieldValue);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EcustomFieldValueModelImpl.ENTITY_CACHE_ENABLED,
					EcustomFieldValueImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ecustomFieldValue;
	}

	/**
	 * Returns the ecustom field value with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param customValueRecId the primary key of the ecustom field value
	 * @return the ecustom field value, or <code>null</code> if a ecustom field value with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EcustomFieldValue fetchByPrimaryKey(long customValueRecId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)customValueRecId);
	}

	/**
	 * Returns all the ecustom field values.
	 *
	 * @return the ecustom field values
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomFieldValue> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ecustom field values.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldValueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecustom field values
	 * @param end the upper bound of the range of ecustom field values (not inclusive)
	 * @return the range of ecustom field values
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomFieldValue> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ecustom field values.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.EcustomFieldValueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ecustom field values
	 * @param end the upper bound of the range of ecustom field values (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ecustom field values
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EcustomFieldValue> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EcustomFieldValue> list = (List<EcustomFieldValue>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_ECUSTOMFIELDVALUE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ECUSTOMFIELDVALUE;

				if (pagination) {
					sql = sql.concat(EcustomFieldValueModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EcustomFieldValue>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EcustomFieldValue>(list);
				}
				else {
					list = (List<EcustomFieldValue>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ecustom field values from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EcustomFieldValue ecustomFieldValue : findAll()) {
			remove(ecustomFieldValue);
		}
	}

	/**
	 * Returns the number of ecustom field values.
	 *
	 * @return the number of ecustom field values
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ECUSTOMFIELDVALUE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the ecustom field value persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.EcustomFieldValue")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EcustomFieldValue>> listenersList = new ArrayList<ModelListener<EcustomFieldValue>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EcustomFieldValue>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EcustomFieldValueImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_ECUSTOMFIELDVALUE = "SELECT ecustomFieldValue FROM EcustomFieldValue ecustomFieldValue";
	private static final String _SQL_SELECT_ECUSTOMFIELDVALUE_WHERE = "SELECT ecustomFieldValue FROM EcustomFieldValue ecustomFieldValue WHERE ";
	private static final String _SQL_COUNT_ECUSTOMFIELDVALUE = "SELECT COUNT(ecustomFieldValue) FROM EcustomFieldValue ecustomFieldValue";
	private static final String _SQL_COUNT_ECUSTOMFIELDVALUE_WHERE = "SELECT COUNT(ecustomFieldValue) FROM EcustomFieldValue ecustomFieldValue WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ecustomFieldValue.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EcustomFieldValue exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EcustomFieldValue exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EcustomFieldValuePersistenceImpl.class);
	private static EcustomFieldValue _nullEcustomFieldValue = new EcustomFieldValueImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EcustomFieldValue> toCacheModel() {
				return _nullEcustomFieldValueCacheModel;
			}
		};

	private static CacheModel<EcustomFieldValue> _nullEcustomFieldValueCacheModel =
		new CacheModel<EcustomFieldValue>() {
			@Override
			public EcustomFieldValue toEntityModel() {
				return _nullEcustomFieldValue;
			}
		};
}