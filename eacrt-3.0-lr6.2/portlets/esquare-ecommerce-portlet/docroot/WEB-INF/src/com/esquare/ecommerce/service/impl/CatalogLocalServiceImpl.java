/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.NoSuchCatalogException;
import com.esquare.ecommerce.model.Catalog;
import com.esquare.ecommerce.service.base.CatalogLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.CatalogUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the catalog local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.CatalogLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CatalogLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.CatalogLocalServiceUtil
 */
public class CatalogLocalServiceImpl extends CatalogLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * com.esquare.ecommerce.service.CatalogLocalServiceUtil} to access the
	 * catalog local service.
	 */
	public List<Catalog> getCatalogs(long companyId, long parentCatalogId,
			int start, int end) throws SystemException {

		return catalogPersistence.findByC_P(companyId, parentCatalogId, start,
				end);
	}

	public int getCatalogsCount(long companyId, long parentCatalogId)
			throws SystemException {

		return catalogPersistence.countByC_P(companyId, parentCatalogId);
	}

	public void getSubcatalogIds(List<Long> catalogIds, long companyId,
			long catalogId) throws SystemException {

		List<Catalog> catalogs = catalogPersistence.findByC_P(companyId,
				catalogId);

		for (Catalog catalog : catalogs) {
			catalogIds.add(catalog.getCatalogId());

			getSubcatalogIds(catalogIds, catalog.getGroupId(),
					catalog.getCatalogId());
		}
	}

	public Catalog addCatalog(long userId, long parentCatalogId, String name,
			String description, boolean showNavigation, long folderId,
			ServiceContext serviceContext) throws PortalException,
			SystemException {

		// Catalog

		User user = userPersistence.findByPrimaryKey(userId);
		long groupId = serviceContext.getScopeGroupId();
		parentCatalogId = getParentCatalogId(groupId, parentCatalogId);
		Date now = new Date();

		long catalogId = counterLocalService.increment();

		Catalog catalog = catalogPersistence.create(catalogId);
		catalog.setGroupId(groupId);
		catalog.setCompanyId(user.getCompanyId());
		catalog.setUserId(user.getUserId());
		catalog.setCreateDate(now);
		catalog.setModifiedDate(now);
		catalog.setParentCatalogId(parentCatalogId);
		catalog.setName(name);
		catalog.setDescription(description);
		catalog.setShowNavigation(showNavigation);
		catalog.setFolderId(folderId);
		catalogPersistence.update(catalog);

		return catalog;
	}

	protected long getParentCatalogId(long groupId, long parentCatalogId)
			throws SystemException {

		if (parentCatalogId != EECConstants.DEFAULT_PARENT_CATALOG_ID) {

			Catalog parentCatalog = catalogPersistence
					.fetchByPrimaryKey(parentCatalogId);

			if ((parentCatalog == null)
					|| (groupId != parentCatalog.getGroupId())) {

				parentCatalogId = EECConstants.DEFAULT_PARENT_CATALOG_ID;
			}
		}

		return parentCatalogId;
	}

	/*
	 * protected void validate(long companyId,String name) throws
	 * PortalException, SystemException { if
	 * (catalogPersistence.fetchByName(companyId,name) != null) { throw new
	 * DuplicateCatalogNameException(); } }
	 */

	public Catalog updateCatalog(long userId, long catalogId,
			long parentCatalogId, String name, String description,
			boolean showNavigation, long folderId,
			boolean mergeWithParentCatalog, ServiceContext serviceContext)
			throws PortalException, SystemException {
		// Merge categories
		// User user = userPersistence.findByPrimaryKey(userId);
		Catalog catalog = catalogPersistence.findByPrimaryKey(catalogId);

		parentCatalogId = getParentCatalogId(catalog, parentCatalogId);
		if (mergeWithParentCatalog && (catalogId != parentCatalogId)
				&& (parentCatalogId != EECConstants.DEFAULT_PARENT_CATALOG_ID)) {
			mergeCategories(catalog, parentCatalogId);

			return catalog;
		}

		// Category
		catalog.setModifiedDate(new Date());
		catalog.setParentCatalogId(parentCatalogId);
		catalog.setName(name);
		catalog.setShowNavigation(showNavigation);
		catalog.setDescription(description);
		catalog.setFolderId(folderId);

		catalogPersistence.update(catalog);

		return catalog;
	}

	protected long getParentCatalogId(Catalog catalog, long parentCatalogId)
			throws SystemException {

		if (parentCatalogId == EECConstants.DEFAULT_PARENT_CATALOG_ID) {

			return parentCatalogId;
		}

		if (catalog.getCatalogId() == parentCatalogId) {
			return catalog.getParentCatalogId();
		} else {
			Catalog parentCatalog = catalogPersistence
					.fetchByPrimaryKey(parentCatalogId);

			if ((parentCatalog == null)
					|| (catalog.getGroupId() != parentCatalog.getGroupId())) {

				return catalog.getParentCatalogId();
			}

			List<Long> subcategoryIds = new ArrayList<Long>();

			getSubcatalogIds(subcategoryIds, catalog.getGroupId(),
					catalog.getCatalogId());

			if (subcategoryIds.contains(parentCatalogId)) {
				return catalog.getParentCatalogId();
			}

			return parentCatalogId;
		}
	}

	protected void mergeCategories(Catalog fromCatalog, long toCatalogId)
			throws PortalException, SystemException {

		List<Catalog> catalogs = catalogPersistence.findByC_P(
				fromCatalog.getCompanyId(), fromCatalog.getCatalogId());

		for (Catalog catalog : catalogs) {
			mergeCategories(catalog, toCatalogId);
		}
		deleteCatalog(fromCatalog);
	}

	public Catalog deleteCatalog(long catalogId) throws PortalException,
			SystemException {

		Catalog catalog = catalogPersistence.findByPrimaryKey(catalogId);

		Catalog curCatalog = deleteCatalog(catalog);
		return curCatalog;
	}

	public Catalog deleteCatalog(Catalog catalog) throws SystemException {

		List<Catalog> catalogs = catalogPersistence.findByC_P(
				catalog.getCompanyId(), catalog.getCatalogId());

		for (Catalog curCatalog : catalogs) {
			deleteCatalog(curCatalog);
		}

		Catalog curCatalog = catalogPersistence.remove(catalog);
		return curCatalog;
	}

	public List<Catalog> getCatalogs(long companyId) throws SystemException {
		return CatalogUtil.findByCompanyId(companyId);
	}

	public List<Catalog> getCatalogs(long companyId, long parentCatalogId)
			throws SystemException {
		return CatalogUtil.findByC_P(companyId, parentCatalogId);
	}

	public List<Catalog> getShowNavigations(long companyId,
			boolean showNavigation) throws SystemException {
		return catalogPersistence.findByShowNavigation(companyId,
				showNavigation);
	}

	public Catalog deleteSingleCatalog(long catalogId)
			throws NoSuchCatalogException, SystemException {
		return catalogPersistence.remove(catalogId);
	}
	
	public Catalog findByNameCompanyIdParentId(String name,long companyId, long parentCatalogId) {
		Catalog catalog = null;
		try {
			catalog = catalogPersistence.findByNameCompanyIdParentId(name, companyId, parentCatalogId);
		} catch (NoSuchCatalogException e) {
			//e.printStackTrace();
		} catch (SystemException e) {
			//e.printStackTrace();
		}
		return catalog;
	}
}