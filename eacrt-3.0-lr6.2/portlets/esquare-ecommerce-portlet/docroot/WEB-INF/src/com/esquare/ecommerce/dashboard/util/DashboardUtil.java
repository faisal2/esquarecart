package com.esquare.ecommerce.dashboard.util;

import com.esquare.ecommerce.util.PortletPropsValues;

public class DashboardUtil {

	public static String getPackageValue(String packageType, String type) {
		String[] values = getPackageValue(packageType);
		return values[getCount(type) - 1];
	}
	
	public static Integer getPackagePrice(String packageType, String type) {
		String[] values = getPackagePrice(packageType);
		if(com.liferay.portal.kernel.util.Validator.isNotNull(values)){
		return Integer.valueOf(values[getCount(type) - 1]);
		}
          return 0;
	}
	
	public static String[] getPackageValue(String packageType) {
		String[] values = null;
		if (packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_FREE)) {
			values = PortletPropsValues.FREE_PACKAGE_PERMISSION_VALUE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_BASIC)) {
			values = PortletPropsValues.BASIC_PACKAGE_PERMISSION_VALUE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_STANDARD)) {
			values = PortletPropsValues.STANDARD_PACKAGE_PERMISSION_VALUE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_PREMIUM)) {
			values = PortletPropsValues.PREMIUM_PACKAGE_PERMISSION_VALUE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_ENTERPRISE)) {
			values = PortletPropsValues.ENTERPRISE_PACKAGE_PERMISSION_VALUE;
		}
		return values;
	}
	
	public static String[] getPackagePrice(String packageType){
		String[] price = null;
		if (packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_BASIC)) {
			price = PortletPropsValues.BASIC_PACKAGE_PERMISSION_PRICE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_STANDARD)) {
			price = PortletPropsValues.STANDARD_PACKAGE_PERMISSION_PRICE;
		} else if (packageType
				.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_PREMIUM)) {
			price = PortletPropsValues.PREMIUM_PACKAGE_PERMISSION_PRICE;
		} 
		return price;
	}

	public static int getCount(String type) {

		String[] names = PortletPropsValues.PACKAGE_PERMISSION_KEY;
		int count = 0;

		for (String name : names) {
			count++;
			if (name.equals(type)) {
				break;
			}
		}
		return count;

	}

}