package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.esquare.ecommerce.BillingCityException;
import com.esquare.ecommerce.BillingCountryException;
import com.esquare.ecommerce.BillingEmailAddressException;
import com.esquare.ecommerce.BillingFirstNameException;
import com.esquare.ecommerce.BillingLastNameException;
import com.esquare.ecommerce.BillingPhoneException;
import com.esquare.ecommerce.BillingStateException;
import com.esquare.ecommerce.BillingStreetException;
import com.esquare.ecommerce.BillingZipException;
import com.esquare.ecommerce.NoSuchOrderException;
import com.esquare.ecommerce.ShippingCityException;
import com.esquare.ecommerce.ShippingCountryException;
import com.esquare.ecommerce.ShippingEmailAddressException;
import com.esquare.ecommerce.ShippingFirstNameException;
import com.esquare.ecommerce.ShippingLastNameException;
import com.esquare.ecommerce.ShippingPhoneException;
import com.esquare.ecommerce.ShippingStateException;
import com.esquare.ecommerce.ShippingStreetException;
import com.esquare.ecommerce.ShippingZipException;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.esquare.ecommerce.service.base.OrderLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.OrderFinderUtil;
import com.esquare.ecommerce.service.persistence.OrderItemFinderUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortletKeys;
import com.liferay.portal.util.SubscriptionSender;
import com.liferay.portlet.shopping.CartMinOrderException;
import com.liferay.util.PwdGenerator;

/**
 * The implementation of the order local service.
 * 
 * @author mohammad azharuddin
 *         <p>
 *         All custom service methods should be put in this class. Whenever
 *         methods are added, rerun ServiceBuilder to copy their definitions
 *         into the {@link com.esquare.ecommerce.service.OrderLocalService}
 *         interface.
 * 
 *         <p>
 *         This is a local service. Methods of this service will not have
 *         security checks based on the propagated JAAS credentials because this
 *         service can only be accessed from within the same VM.
 *         </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.OrderLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.OrderLocalServiceUtil
 */
public class OrderLocalServiceImpl extends OrderLocalServiceBaseImpl {

	public Order addLatestOrder(long userId, long companyId)
			throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);
		Date now = new Date();

		String number = getNumber();

		Order order = null;

		long orderId = counterLocalService.increment();

		List<Order> pastOrders = orderPersistence.findByC_U_S(companyId,
				userId, EECConstants.STATUS_CHECKOUT, 0, 1);

		if (pastOrders.size() > 0) {
			Order pastOrder = pastOrders.get(0);

			order = orderPersistence.create(orderId);

			order.setBillingCompany(pastOrder.getBillingCompany());
			order.setBillingStreet(pastOrder.getBillingStreet());
			order.setBillingCity(pastOrder.getBillingCity());
			order.setBillingState(pastOrder.getBillingState());
			order.setBillingZip(pastOrder.getBillingZip());
			order.setBillingCountry(pastOrder.getBillingCountry());
			order.setBillingPhone(pastOrder.getBillingPhone());
			order.setShipToBilling(pastOrder.isShipToBilling());
			order.setShippingCompany(pastOrder.getShippingCompany());
			order.setShippingStreet(pastOrder.getShippingStreet());
			order.setShippingCity(pastOrder.getShippingCity());
			order.setShippingState(pastOrder.getShippingState());
			order.setShippingZip(pastOrder.getShippingZip());
			order.setShippingCountry(pastOrder.getShippingCountry());
			order.setShippingPhone(pastOrder.getShippingPhone());
			order.setBillingFirstName(pastOrder.getBillingFirstName());
			order.setBillingLastName(pastOrder.getBillingLastName());
			order.setShippingFirstName(pastOrder.getShippingFirstName());
			order.setShippingLastName(pastOrder.getShippingLastName());
		} else {
			order = orderPersistence.create(orderId);
			order.setBillingFirstName(user.getFirstName());
			order.setBillingLastName(user.getLastName());
			order.setShippingFirstName(user.getFirstName());
			order.setShippingLastName(user.getLastName());
		}

		order.setGroupId(companyId);
		order.setCompanyId(user.getCompanyId());
		order.setUserId(user.getUserId());
		order.setUserName(user.getFullName());
		order.setCreateDate(now);
		order.setModifiedDate(now);
		order.setNumber(number);
		order.setBillingEmailAddress(user.getEmailAddress());
		order.setShippingEmailAddress(user.getEmailAddress());
		order.setStatus(EECConstants.STATUS_LATEST);
		order.setSendOrderEmail(true);
		orderPersistence.update(order);

		return order;
	}

	public Order getLatestOrder(long userId, long companyId)
			throws PortalException, SystemException {

		List<Order> orders = orderPersistence.findByC_U_S(companyId, userId,
				EECConstants.STATUS_LATEST, 0, 1);

		Order order = null;
		if (orders.size() == 1) {

			order = orders.get(0);
		} else {
			order = orderLocalService.addLatestOrder(userId, companyId);
		}

		return order;
	}

	protected String getNumber() throws SystemException {
		String number = PwdGenerator.getPassword(PwdGenerator.KEY1
				+ PwdGenerator.KEY2, 12);

		try {
			orderPersistence.findByNumber(number);

			return getNumber();
		} catch (NoSuchOrderException nsoe) {
			return number;
		}
	}

	public Order getOrder(long orderId) throws PortalException, SystemException {
		return orderPersistence.findByPrimaryKey(orderId);
	}

	public List<Order> findByCompanyId(long companyId, String status)
			throws SystemException {
		return orderPersistence.findByC_S(companyId, status);
	}

	public List<Order> findByUserId(long companyId, long userId, String status)
			throws SystemException {
		return orderPersistence.findByC_U_S(companyId, userId, status);
	}

	public Order updateLatestOrder(long userId, long companyId,
			String billingFirstName, String billingLastName,
			String billingEmailAddress, String billingCompany,
			String billingStreet, String billingCity, String billingState,
			String billingZip, String billingCountry, String billingPhone,
			boolean shipToBilling, String shippingFirstName,
			String shippingLastName, String shippingEmailAddress,
			String shippingCompany, String shippingStreet, String shippingCity,
			String shippingState, String shippingZip, String shippingCountry,
			String shippingPhone, String comments) throws PortalException,
			SystemException {

		Order order = getLatestOrder(userId, companyId);

		return updateOrder(order.getOrderId(), billingFirstName,
				billingLastName, billingEmailAddress, billingCompany,
				billingStreet, billingCity, billingState, billingZip,
				billingCountry, billingPhone, shipToBilling, shippingFirstName,
				shippingLastName, shippingEmailAddress, shippingCompany,
				shippingStreet, shippingCity, shippingState, shippingZip,
				shippingCountry, shippingPhone, comments);
	}

	public Order updateOrder(long orderId, String billingFirstName,
			String billingLastName, String billingEmailAddress,
			String billingCompany, String billingStreet, String billingCity,
			String billingState, String billingZip, String billingCountry,
			String billingPhone, boolean shipToBilling,
			String shippingFirstName, String shippingLastName,
			String shippingEmailAddress, String shippingCompany,
			String shippingStreet, String shippingCity, String shippingState,
			String shippingZip, String shippingCountry, String shippingPhone,
			String comments) throws PortalException, SystemException {

		Order order = orderPersistence.findByPrimaryKey(orderId);

		validate(billingFirstName, billingLastName, billingEmailAddress,
				billingStreet, billingCity, billingState, billingZip,
				billingCountry, billingPhone, shipToBilling, shippingFirstName,
				shippingLastName, shippingEmailAddress, shippingStreet,
				shippingCity, shippingState, shippingZip, shippingCountry,
				shippingPhone);

		order.setModifiedDate(new Date());
		order.setBillingFirstName(billingFirstName);
		order.setBillingLastName(billingLastName);
		order.setBillingEmailAddress(billingEmailAddress);
		order.setBillingCompany(billingCompany);
		order.setBillingStreet(billingStreet);
		order.setBillingCity(billingCity);
		order.setBillingState(billingState);
		order.setBillingZip(billingZip);
		order.setBillingCountry(billingCountry);
		order.setBillingPhone(billingPhone);
		order.setShipToBilling(shipToBilling);

		if (shipToBilling) {
			order.setShippingFirstName(billingFirstName);
			order.setShippingLastName(billingLastName);
			order.setShippingEmailAddress(billingEmailAddress);
			order.setShippingCompany(billingCompany);
			order.setShippingStreet(billingStreet);
			order.setShippingCity(billingCity);
			order.setShippingState(billingState);
			order.setShippingZip(billingZip);
			order.setShippingCountry(billingCountry);
			order.setShippingPhone(billingPhone);
		} else {
			order.setShippingFirstName(shippingFirstName);
			order.setShippingLastName(shippingLastName);
			order.setShippingEmailAddress(shippingEmailAddress);
			order.setShippingCompany(shippingCompany);
			order.setShippingStreet(shippingStreet);
			order.setShippingCity(shippingCity);
			order.setShippingState(shippingState);
			order.setShippingZip(shippingZip);
			order.setShippingCountry(shippingCountry);
			order.setShippingPhone(shippingPhone);
		}

		order.setComments(comments);

		orderPersistence.update(order);

		return order;
	}

	public void sendEmail(long orderId, String emailType, String oldStatus,
			String newStatus, String orderItemDetails,
			ServiceContext serviceContext) throws PortalException,
			SystemException {

		Order order = orderPersistence.findByPrimaryKey(orderId);
		sendEmail(order, emailType, oldStatus, newStatus, orderItemDetails,
				serviceContext);
	}

	public void sendEmail(Order order, String emailType, String oldStatus,
			String newStatus, String orderItemDetails,
			ServiceContext serviceContext) throws PortalException,
			SystemException {
		HttpServletRequest request = serviceContext.getRequest();
		long companyId = serviceContext.getCompanyId();

		User user = userPersistence.findByPrimaryKey(order.getUserId());

		String billingAddress = order.getBillingFirstName() + " "
				+ order.getBillingLastName() + "<br>"
				+ order.getBillingEmailAddress() + "<br>"
				+ order.getBillingStreet() + "<br>" + order.getBillingCity()
				+ "<br>" + order.getBillingState() + "<br>"
				+ order.getBillingZip() + "<br>" + order.getBillingCountry()
				+ "<br>" + order.getBillingPhone() + "<br>";

		String shippingAddress = order.getShippingFirstName() + " "
				+ order.getShippingLastName() + "<br>"
				+ order.getShippingEmailAddress() + "<br>"
				+ order.getShippingStreet() + "<br>" + order.getShippingCity()
				+ "<br>" + order.getShippingState() + "<br>"
				+ order.getShippingZip() + "<br>" + order.getShippingCountry()
				+ "<br>" + order.getShippingPhone() + "<br>";

		double total = EECUtil.calculateTotal(order);
		String toName = null;
		String fromName = null;
		String fromAddress = null;
		String toAddress = null;
		String subject = null;
		String body = null;

		fromName = EECUtil.getEmailName(request, companyId,
				PropsKeys.ADMIN_EMAIL_FROM_NAME);
		fromAddress = EECUtil.getEmailAddress(request, companyId,
				PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);
		toName = user.getFullName();
		toAddress = user.getEmailAddress();
		if (Validator.isNotNull(emailType)) {
			if (emailType.equals(EECConstants.CONFIRMATION_MAIL)) {
				subject = EECUtil.getEmailContent(request, companyId,
						"Order.emailConfirmation.subject");
				body = EECUtil.getEmailContent(request, companyId,
						"Order.emailConfirmation.body");
			}
		} else {
			if (newStatus.equals(EECConstants.ORDER_STATUS_ONSHIPPING)) {
				subject = EECUtil.getEmailContent(request, companyId,
						"Shipping.emailConfirmation.subject");
				body = EECUtil.getEmailContent(request, companyId,
						"Shipping.emailConfirmation.body");
			} else if (newStatus.equals(EECConstants.ORDER_STATUS_CANCELLED)) {
				subject = EECUtil.getEmailContent(request, companyId,
						"Cancel.emailConfirmation.subject");
				body = EECUtil.getEmailContent(request, companyId,
						"Cancel.emailConfirmation.body");
			} else if (newStatus.equals(EECConstants.ORDER_STATUS_RETURN)) {
				subject = EECUtil.getEmailContent(request, companyId,
						"Return.emailConfirmation.subject");
				body = EECUtil.getEmailContent(request, companyId,
						"Return.emailConfirmation.body");
			} else if (newStatus.equals(EECConstants.ORDER_STATUS_DELIVERED)) {
				subject = EECUtil.getEmailContent(request, companyId,
						"Delivered.emailConfirmation.subject");
				body = EECUtil.getEmailContent(request, companyId,
						"Delivered.emailConfirmation.body");
			}
		}
		body = StringUtil.replace(body, "[$ORDER_DETAILS_TABLE$]",
				orderItemDetails);
		SubscriptionSender subscriptionSender = new SubscriptionSender();

		subscriptionSender.setBody(body);
		subscriptionSender.setCompanyId(order.getCompanyId());
		subscriptionSender.setContextAttributes("[$OLD_STATUS$]", oldStatus,
				"[$NEW_STATUS$]", newStatus, "[$ORDER_BILLING_ADDRESS$]",
				billingAddress, "[$ORDER_NUMBER$]", order.getNumber(),
				"[$ORDER_SHIPPING_ADDRESS$]", shippingAddress,
				"[$ORDER_TOTAL$]", total);
		subscriptionSender.setFrom(fromAddress, fromName);
		subscriptionSender.setHtmlFormat(true);
		subscriptionSender.setMailId("shopping_order", order.getOrderId());
		subscriptionSender.setPortletId(PortletKeys.SHOPPING);
		subscriptionSender.setScopeGroupId(order.getGroupId());
		subscriptionSender.setServiceContext(serviceContext);
		subscriptionSender.setSubject(subject);
		subscriptionSender.setUserId(order.getUserId());

		subscriptionSender.addRuntimeSubscribers(toAddress, toName);

		subscriptionSender.flushNotificationsAsync();
		if (Validator.isNotNull(emailType)
				&& emailType.equals(EECConstants.CONFIRMATION_MAIL)
				&& order.isSendOrderEmail()) {
			order.setSendOrderEmail(false);

			orderPersistence.update(order);
		}

	}

	public Order saveLatestOrder(long companyId, long userId, Cart cart)
			throws PortalException, SystemException {

		Map<CartItem, Integer> productInventories = cart
				.getProductinventories();
		Date now = new Date();

		Order order = getLatestOrder(cart.getUserId(), cart.getCompanyId());

		order.setCreateDate(now);
		order.setModifiedDate(now);
		order.setStatus(EECConstants.STATUS_CHECKOUT);
		orderPersistence.update(order);

		boolean requiresShipping = false;

		Coupon coupon = couponPersistence.fetchByCouponCode(
				cart.getCouponCodes(), false);

		Iterator<Map.Entry<CartItem, Integer>> itr = productInventories
				.entrySet().iterator();

		while (itr.hasNext()) {
			Map.Entry<CartItem, Integer> entry = itr.next();

			CartItem cartItem = entry.getKey();
			Integer count = entry.getValue();

			ProductInventory productInventory = cartItem.getProductInventory();
			ProductDetails productDetails = ProductDetailsLocalServiceUtil
					.getProduct(productInventory.getProductDetailsId());
			// review this line azar
			requiresShipping = true;

			long orderItemId = counterLocalService.increment();

			OrderItem orderItem = orderItemPersistence.create(orderItemId);

			orderItem.setCreatedDate(now);
			orderItem.setOrderId(order.getOrderId());
			orderItem.setCompanyId(companyId);
			orderItem.setProductInventoryId(cartItem.getCartItemId());
			orderItem.setSku(productInventory.getSku());
			orderItem.setName(productDetails.getName());
			orderItem.setDescription(productDetails.getDescription());
			orderItem.setPrice(EECUtil.calculateActualPrice(productInventory,
					count.intValue()) / count.intValue());
			try {
				orderItem.setTax(EECUtil.orderItemTax(companyId, productInventory, order.getShippingCountry(), order.getShippingState(),count.intValue() / count.intValue()));
				orderItem.setShippingPrice(EECUtil.orderItemShippingPrice(companyId,productInventory,order.getShippingCountry(), order.getShippingState(),count.intValue() / count.intValue()));
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			orderItem.setQuantity(count.intValue());
			orderItemPersistence.update(orderItem);
		}

		order.setModifiedDate(new Date());
		order.setRequiresShipping(requiresShipping);
		order.setInsure(cart.isInsure());
		order.setInsurance(EECUtil.calculateInsurance(productInventories));
		order.setCouponCodes(cart.getCouponCodes());
		if (Validator.isNotNull(coupon))
			order.setCouponVersion(coupon.getVersion());

		try {
			order.setShipping(EECUtil.calculateShippingPrice(companyId,
					productInventories, order.getShippingCountry(),
					order.getShippingState()));
			order.setTax(EECUtil.calculateTax(companyId, productInventories,
					order.getShippingCountry(), order.getShippingState(),userId,cart.getCoupon()));
			order.setCouponDiscount(EECUtil.calculateCouponDiscount(companyId,
					userId, productInventories, cart.getCoupon(),
					order.getShippingCountry(), order.getShippingState()));
		} catch (DocumentException e) {
			_log.info(e.getClass());
		}

		double walletAmount = ((EECUtil.calculateTotal(order) - cart
				.getWalletAmount()) < 0) ? EECUtil.calculateTotal(order) : cart
				.getWalletAmount();
		order.setWalletAmount(walletAmount);
		order.setSendOrderEmail(true);

		orderPersistence.update(order);
		if (cart.getWalletAmount() > 0)
			orderRefundTrackerLocalService.updateOrderRefundTracker(0,
					companyId, userId, null, walletAmount, true,
					EECConstants.WALLET, null, null);

		return order;
	}

	protected void validate(String billingFirstName, String billingLastName,
			String billingEmailAddress, String billingStreet,
			String billingCity, String billingState, String billingZip,
			String billingCountry, String billingPhone, boolean shipToBilling,
			String shippingFirstName, String shippingLastName,
			String shippingEmailAddress, String shippingStreet,
			String shippingCity, String shippingState, String shippingZip,
			String shippingCountry, String shippingPhone)
			throws PortalException {

		if (Validator.isNull(billingFirstName)) {
			throw new BillingFirstNameException();
		} else if (Validator.isNull(billingLastName)) {
			throw new BillingLastNameException();
		} else if (!Validator.isEmailAddress(billingEmailAddress)) {
			throw new BillingEmailAddressException();
		} else if (Validator.isNull(billingStreet)) {
			throw new BillingStreetException();
		} else if (Validator.isNull(billingCity)) {
			throw new BillingCityException();
		} else if (Validator.isNull(billingState)) {
			throw new BillingStateException();
		} else if (Validator.isNull(billingZip)) {
			throw new BillingZipException();
		} else if (Validator.isNull(billingCountry)) {
			throw new BillingCountryException();
		} else if (Validator.isNull(billingPhone)) {
			throw new BillingPhoneException();
		}

		if (!shipToBilling) {
			if (Validator.isNull(shippingFirstName)) {
				throw new ShippingFirstNameException();
			} else if (Validator.isNull(shippingLastName)) {
				throw new ShippingLastNameException();
			} else if (!Validator.isEmailAddress(shippingEmailAddress)) {
				throw new ShippingEmailAddressException();
			} else if (Validator.isNull(shippingStreet)) {
				throw new ShippingStreetException();
			} else if (Validator.isNull(shippingCity)) {
				throw new ShippingCityException();
			} else if (Validator.isNull(shippingState)) {
				throw new ShippingStateException();
			} else if (Validator.isNull(shippingZip)) {
				throw new ShippingZipException();
			} else if (Validator.isNull(shippingCountry)) {
				throw new ShippingCountryException();
			} else if (Validator.isNull(shippingPhone)) {
				throw new ShippingPhoneException();
			}
		}
	}

	public int getTodayOrderCount(String isNotStatus, long companyId)
			throws SystemException {
		return OrderFinderUtil.getTodayOrderCount(isNotStatus, companyId);
	}

	public int getLastNoOfDaysOrderCount(int day, String isNotStatus,
			long companyId) throws SystemException {
		return OrderFinderUtil.getLastNoOfDaysOrderCount(day, isNotStatus,
				companyId);
	}

	public Map<String, String> getLastWeekEachDayCount(String isNotStatus,
			long companyId) throws SystemException {
		return OrderFinderUtil.getLastWeekEachDayCount(isNotStatus, companyId);
	}

	public int getTodayPurchasedUserCount(String status, long companyId)
			throws SystemException {
		return OrderFinderUtil.getTodayPurchasedUserCount(status, companyId);
	}

	public int getCouponUsedCount(long companyId, String couponCode)
			throws SystemException {
		return orderPersistence.findByCouponCode(companyId, couponCode).size();
	}

	public int getCouponVersionUsedCount(long companyId, String couponCode,
			double couponVersion) throws SystemException {
		return orderPersistence.findByCouponCodeVersion(companyId, couponCode,
				couponVersion).size();
	}

	public int getCouponUsedByUserCount(long companyId, long userId,
			String couponCode) throws SystemException {
		return orderPersistence.findByUserCouponCode(companyId, userId,
				couponCode).size();
	}

	public Order updateOrder(long orderId, String paymentType,
			String paymentStatus, String history) throws SystemException,
			NoSuchOrderException {
		Order order = orderPersistence.findByPrimaryKey(orderId);
		return updateOrder(order, paymentType, paymentStatus, history);
	}

	public Order updateOrder(Order order, String paymentType,
			String paymentStatus, String history) throws SystemException {
		if (Validator.isNotNull(paymentType))
			order.setPaymentType(paymentType);
		if (Validator.isNotNull(paymentStatus))
			order.setPaymentStatus(paymentStatus);
		if (Validator.isNotNull(history))
			order.setHistory(history);
		order.setModifiedDate(new Date());
		orderPersistence.update(order);
		return order;
	}

	public Order updateCancelledOrder(long orderId, double decreaseTaxRate,
			double decreaseShipping, double decreseCouponDiscount,
			double decreseWalletAmount) throws SystemException,
			NoSuchOrderException {
		Order order = orderPersistence.fetchByPrimaryKey(orderId);
		order.setTax(order.getTax() - decreaseTaxRate);
		order.setShipping(order.getShipping() - decreaseShipping);
		order.setCouponDiscount(order.getCouponDiscount()
				- decreseCouponDiscount);
		order.setWalletAmount((order.getWalletAmount() - decreseWalletAmount) < 0 ? 0
				: order.getWalletAmount() - decreseWalletAmount);
		orderPersistence.update(order);
		return order;
	}

	public List<Order> getOrderItemStatusList(long companyId,
			String orderItemStatus) {
		return OrderItemFinderUtil.getOrderItemStatusList(companyId,
				orderItemStatus);
	}

	public List<Order> findByOrderNumber(long companyId, String orderNumber)
			throws SystemException {
		return orderPersistence.findByOrderNumber(companyId, orderNumber);
	}

	public List<Order> findByOrderNumberUserId(long companyId, long userId,
			String orderNumber) throws SystemException {
		return orderPersistence.findByOrderNumberUserId(companyId, userId,
				orderNumber);
	}

	public Long getTotalOrderCount(String isNotStatus, long companyId)
			throws SystemException {

		DynamicQuery dQ = DynamicQueryFactoryUtil.forClass(OrderItem.class,
				PortletClassLoaderUtil.getClassLoader());
		dQ.setProjection(ProjectionFactoryUtil.distinct(ProjectionFactoryUtil
				.property("orderId")));
		dQ.add(RestrictionsFactoryUtil.eq("companyId", companyId));
		dQ.add(RestrictionsFactoryUtil.ne("orderItemStatus", isNotStatus));

		return (long) OrderItemLocalServiceUtil.dynamicQuery(dQ).size();
	}

	public Order deleteOrderCoupon(long orderId) throws SystemException {
		Order order = orderPersistence.fetchByPrimaryKey(orderId);
		order.setCouponCodes(null);
		orderPersistence.update(order);
		return order;
	}

	private static Log _log = LogFactoryUtil
			.getLog(OrderLocalServiceImpl.class);
}