/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.impl.CartPermissionImpl;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.base.CartPermissionLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.CartPermissionUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
/**
 * The implementation of the cart permission local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.CartPermissionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.CartPermissionLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.CartPermissionLocalServiceUtil
 */
public class CartPermissionLocalServiceImpl
	extends CartPermissionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.esquare.ecommerce.service.CartPermissionLocalServiceUtil} to access the cart permission local service.
	 */
	public List<CartPermission> getCartPermissionDetail(long companyId) throws SystemException{
		
		return CartPermissionUtil.findByCompanyId(companyId);
	}
	
	public CartPermission getCartPermissionCI_PK(long companyId , String permissionKey) {
		
		try {
			return CartPermissionUtil.fetchByCI_PK(companyId, permissionKey);
		} catch (SystemException e) {}
		
		return null;
	}
	
	public void createCartPermission(long companyId, String key, String value, int price ,boolean unlimitedPck) throws SystemException {
		CartPermission cartPermission = new CartPermissionImpl();
		long permissionId = CounterLocalServiceUtil.increment("CartPermission.class");
		cartPermission.setPermissionId(permissionId);
		cartPermission.setCompanyId(companyId);
		cartPermission.setPermissionKey(key);
		cartPermission.setValue(value);
		cartPermission.setPrice(price);
		cartPermission.setUnlimitedPck(unlimitedPck);
		CartPermissionLocalServiceUtil.addCartPermission(cartPermission);
	}
}