/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import com.esquare.ecommerce.NoSuchPaymentUserDetailException;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.service.base.PaymentUserDetailLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.PaymentUserDetailUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the payment user detail local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.PaymentUserDetailLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.PaymentUserDetailLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil
 */
public class PaymentUserDetailLocalServiceImpl
	extends PaymentUserDetailLocalServiceBaseImpl {
	
	public PaymentUserDetail findByUserId(long userId) throws SystemException, NoSuchPaymentUserDetailException{
		
		return PaymentUserDetailUtil.findByUserId(userId);
		}
	
	public PaymentUserDetail findByCompanyId(long companyId) throws SystemException {
		
		return paymentUserDetailPersistence.fetchByCompanyId(companyId);
	}

	public PaymentUserDetail updatePaymentUserDetail(long recId, String name,
			String address, String country, String state, String city,
			long pinCode, String email, long phoneNo) throws SystemException {

		PaymentUserDetail ebsUserDetail = paymentUserDetailPersistence
				.fetchByPrimaryKey(recId);
		ebsUserDetail.setName(name);
		ebsUserDetail.setAddress(address);
		ebsUserDetail.setCountry(country);
		ebsUserDetail.setState(state);
		ebsUserDetail.setCity(city);
		ebsUserDetail.setPinCode(pinCode);
		ebsUserDetail.setEmail(email);
		ebsUserDetail.setPhoneNo(phoneNo);
		paymentUserDetailPersistence.update(ebsUserDetail);
		return ebsUserDetail;

	}
}