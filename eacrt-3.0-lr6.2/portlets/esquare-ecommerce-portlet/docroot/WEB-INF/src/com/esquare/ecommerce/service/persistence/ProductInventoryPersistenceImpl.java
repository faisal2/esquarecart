/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchProductInventoryException;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.impl.ProductInventoryImpl;
import com.esquare.ecommerce.model.impl.ProductInventoryModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the product inventory service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see ProductInventoryPersistence
 * @see ProductInventoryUtil
 * @generated
 */
public class ProductInventoryPersistenceImpl extends BasePersistenceImpl<ProductInventory>
	implements ProductInventoryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductInventoryUtil} to access the product inventory persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductInventoryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED,
			ProductInventoryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED,
			ProductInventoryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_FETCH_BY_PRODUCTDETAILSID = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED,
			ProductInventoryImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByProductDetailsId", new String[] { Long.class.getName() },
			ProductInventoryModelImpl.PRODUCTDETAILSID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PRODUCTDETAILSID = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProductDetailsId", new String[] { Long.class.getName() });

	/**
	 * Returns the product inventory where productDetailsId = &#63; or throws a {@link com.esquare.ecommerce.NoSuchProductInventoryException} if it could not be found.
	 *
	 * @param productDetailsId the product details ID
	 * @return the matching product inventory
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory findByProductDetailsId(long productDetailsId)
		throws NoSuchProductInventoryException, SystemException {
		ProductInventory productInventory = fetchByProductDetailsId(productDetailsId);

		if (productInventory == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("productDetailsId=");
			msg.append(productDetailsId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductInventoryException(msg.toString());
		}

		return productInventory;
	}

	/**
	 * Returns the product inventory where productDetailsId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param productDetailsId the product details ID
	 * @return the matching product inventory, or <code>null</code> if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchByProductDetailsId(long productDetailsId)
		throws SystemException {
		return fetchByProductDetailsId(productDetailsId, true);
	}

	/**
	 * Returns the product inventory where productDetailsId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param productDetailsId the product details ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching product inventory, or <code>null</code> if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchByProductDetailsId(long productDetailsId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { productDetailsId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
					finderArgs, this);
		}

		if (result instanceof ProductInventory) {
			ProductInventory productInventory = (ProductInventory)result;

			if ((productDetailsId != productInventory.getProductDetailsId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PRODUCTINVENTORY_WHERE);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSID_PRODUCTDETAILSID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(productDetailsId);

				List<ProductInventory> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ProductInventoryPersistenceImpl.fetchByProductDetailsId(long, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ProductInventory productInventory = list.get(0);

					result = productInventory;

					cacheResult(productInventory);

					if ((productInventory.getProductDetailsId() != productDetailsId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
							finderArgs, productInventory);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProductInventory)result;
		}
	}

	/**
	 * Removes the product inventory where productDetailsId = &#63; from the database.
	 *
	 * @param productDetailsId the product details ID
	 * @return the product inventory that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory removeByProductDetailsId(long productDetailsId)
		throws NoSuchProductInventoryException, SystemException {
		ProductInventory productInventory = findByProductDetailsId(productDetailsId);

		return remove(productInventory);
	}

	/**
	 * Returns the number of product inventories where productDetailsId = &#63;.
	 *
	 * @param productDetailsId the product details ID
	 * @return the number of matching product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByProductDetailsId(long productDetailsId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PRODUCTDETAILSID;

		Object[] finderArgs = new Object[] { productDetailsId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTINVENTORY_WHERE);

			query.append(_FINDER_COLUMN_PRODUCTDETAILSID_PRODUCTDETAILSID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(productDetailsId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PRODUCTDETAILSID_PRODUCTDETAILSID_2 =
		"productInventory.productDetailsId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_SKU = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED,
			ProductInventoryImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchBySKU",
			new String[] { Long.class.getName(), String.class.getName() },
			ProductInventoryModelImpl.COMPANYID_COLUMN_BITMASK |
			ProductInventoryModelImpl.SKU_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SKU = new FinderPath(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySKU",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the product inventory where companyId = &#63; and sku = &#63; or throws a {@link com.esquare.ecommerce.NoSuchProductInventoryException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param sku the sku
	 * @return the matching product inventory
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory findBySKU(long companyId, String sku)
		throws NoSuchProductInventoryException, SystemException {
		ProductInventory productInventory = fetchBySKU(companyId, sku);

		if (productInventory == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", sku=");
			msg.append(sku);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchProductInventoryException(msg.toString());
		}

		return productInventory;
	}

	/**
	 * Returns the product inventory where companyId = &#63; and sku = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param sku the sku
	 * @return the matching product inventory, or <code>null</code> if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchBySKU(long companyId, String sku)
		throws SystemException {
		return fetchBySKU(companyId, sku, true);
	}

	/**
	 * Returns the product inventory where companyId = &#63; and sku = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param sku the sku
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching product inventory, or <code>null</code> if a matching product inventory could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchBySKU(long companyId, String sku,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { companyId, sku };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_SKU,
					finderArgs, this);
		}

		if (result instanceof ProductInventory) {
			ProductInventory productInventory = (ProductInventory)result;

			if ((companyId != productInventory.getCompanyId()) ||
					!Validator.equals(sku, productInventory.getSku())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PRODUCTINVENTORY_WHERE);

			query.append(_FINDER_COLUMN_SKU_COMPANYID_2);

			boolean bindSku = false;

			if (sku == null) {
				query.append(_FINDER_COLUMN_SKU_SKU_1);
			}
			else if (sku.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SKU_SKU_3);
			}
			else {
				bindSku = true;

				query.append(_FINDER_COLUMN_SKU_SKU_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindSku) {
					qPos.add(sku);
				}

				List<ProductInventory> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SKU,
						finderArgs, list);
				}
				else {
					if ((list.size() > 1) && _log.isWarnEnabled()) {
						_log.warn(
							"ProductInventoryPersistenceImpl.fetchBySKU(long, String, boolean) with parameters (" +
							StringUtil.merge(finderArgs) +
							") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
					}

					ProductInventory productInventory = list.get(0);

					result = productInventory;

					cacheResult(productInventory);

					if ((productInventory.getCompanyId() != companyId) ||
							(productInventory.getSku() == null) ||
							!productInventory.getSku().equals(sku)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SKU,
							finderArgs, productInventory);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SKU,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProductInventory)result;
		}
	}

	/**
	 * Removes the product inventory where companyId = &#63; and sku = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param sku the sku
	 * @return the product inventory that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory removeBySKU(long companyId, String sku)
		throws NoSuchProductInventoryException, SystemException {
		ProductInventory productInventory = findBySKU(companyId, sku);

		return remove(productInventory);
	}

	/**
	 * Returns the number of product inventories where companyId = &#63; and sku = &#63;.
	 *
	 * @param companyId the company ID
	 * @param sku the sku
	 * @return the number of matching product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBySKU(long companyId, String sku) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SKU;

		Object[] finderArgs = new Object[] { companyId, sku };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PRODUCTINVENTORY_WHERE);

			query.append(_FINDER_COLUMN_SKU_COMPANYID_2);

			boolean bindSku = false;

			if (sku == null) {
				query.append(_FINDER_COLUMN_SKU_SKU_1);
			}
			else if (sku.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_SKU_SKU_3);
			}
			else {
				bindSku = true;

				query.append(_FINDER_COLUMN_SKU_SKU_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindSku) {
					qPos.add(sku);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SKU_COMPANYID_2 = "productInventory.companyId = ? AND ";
	private static final String _FINDER_COLUMN_SKU_SKU_1 = "productInventory.sku IS NULL";
	private static final String _FINDER_COLUMN_SKU_SKU_2 = "productInventory.sku = ?";
	private static final String _FINDER_COLUMN_SKU_SKU_3 = "(productInventory.sku IS NULL OR productInventory.sku = '')";

	public ProductInventoryPersistenceImpl() {
		setModelClass(ProductInventory.class);
	}

	/**
	 * Caches the product inventory in the entity cache if it is enabled.
	 *
	 * @param productInventory the product inventory
	 */
	@Override
	public void cacheResult(ProductInventory productInventory) {
		EntityCacheUtil.putResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryImpl.class, productInventory.getPrimaryKey(),
			productInventory);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
			new Object[] { productInventory.getProductDetailsId() },
			productInventory);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SKU,
			new Object[] {
				productInventory.getCompanyId(), productInventory.getSku()
			}, productInventory);

		productInventory.resetOriginalValues();
	}

	/**
	 * Caches the product inventories in the entity cache if it is enabled.
	 *
	 * @param productInventories the product inventories
	 */
	@Override
	public void cacheResult(List<ProductInventory> productInventories) {
		for (ProductInventory productInventory : productInventories) {
			if (EntityCacheUtil.getResult(
						ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
						ProductInventoryImpl.class,
						productInventory.getPrimaryKey()) == null) {
				cacheResult(productInventory);
			}
			else {
				productInventory.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all product inventories.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductInventoryImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductInventoryImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the product inventory.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProductInventory productInventory) {
		EntityCacheUtil.removeResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryImpl.class, productInventory.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(productInventory);
	}

	@Override
	public void clearCache(List<ProductInventory> productInventories) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProductInventory productInventory : productInventories) {
			EntityCacheUtil.removeResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
				ProductInventoryImpl.class, productInventory.getPrimaryKey());

			clearUniqueFindersCache(productInventory);
		}
	}

	protected void cacheUniqueFindersCache(ProductInventory productInventory) {
		if (productInventory.isNew()) {
			Object[] args = new Object[] { productInventory.getProductDetailsId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
				args, productInventory);

			args = new Object[] {
					productInventory.getCompanyId(), productInventory.getSku()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SKU, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SKU, args,
				productInventory);
		}
		else {
			ProductInventoryModelImpl productInventoryModelImpl = (ProductInventoryModelImpl)productInventory;

			if ((productInventoryModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PRODUCTDETAILSID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productInventory.getProductDetailsId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
					args, productInventory);
			}

			if ((productInventoryModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SKU.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productInventory.getCompanyId(),
						productInventory.getSku()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SKU, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_SKU, args,
					productInventory);
			}
		}
	}

	protected void clearUniqueFindersCache(ProductInventory productInventory) {
		ProductInventoryModelImpl productInventoryModelImpl = (ProductInventoryModelImpl)productInventory;

		Object[] args = new Object[] { productInventory.getProductDetailsId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID, args);

		if ((productInventoryModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PRODUCTDETAILSID.getColumnBitmask()) != 0) {
			args = new Object[] {
					productInventoryModelImpl.getOriginalProductDetailsId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTDETAILSID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PRODUCTDETAILSID,
				args);
		}

		args = new Object[] {
				productInventory.getCompanyId(), productInventory.getSku()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKU, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SKU, args);

		if ((productInventoryModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_SKU.getColumnBitmask()) != 0) {
			args = new Object[] {
					productInventoryModelImpl.getOriginalCompanyId(),
					productInventoryModelImpl.getOriginalSku()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SKU, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_SKU, args);
		}
	}

	/**
	 * Creates a new product inventory with the primary key. Does not add the product inventory to the database.
	 *
	 * @param inventoryId the primary key for the new product inventory
	 * @return the new product inventory
	 */
	@Override
	public ProductInventory create(long inventoryId) {
		ProductInventory productInventory = new ProductInventoryImpl();

		productInventory.setNew(true);
		productInventory.setPrimaryKey(inventoryId);

		return productInventory;
	}

	/**
	 * Removes the product inventory with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param inventoryId the primary key of the product inventory
	 * @return the product inventory that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory remove(long inventoryId)
		throws NoSuchProductInventoryException, SystemException {
		return remove((Serializable)inventoryId);
	}

	/**
	 * Removes the product inventory with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the product inventory
	 * @return the product inventory that was removed
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory remove(Serializable primaryKey)
		throws NoSuchProductInventoryException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ProductInventory productInventory = (ProductInventory)session.get(ProductInventoryImpl.class,
					primaryKey);

			if (productInventory == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductInventoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(productInventory);
		}
		catch (NoSuchProductInventoryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProductInventory removeImpl(ProductInventory productInventory)
		throws SystemException {
		productInventory = toUnwrappedModel(productInventory);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(productInventory)) {
				productInventory = (ProductInventory)session.get(ProductInventoryImpl.class,
						productInventory.getPrimaryKeyObj());
			}

			if (productInventory != null) {
				session.delete(productInventory);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (productInventory != null) {
			clearCache(productInventory);
		}

		return productInventory;
	}

	@Override
	public ProductInventory updateImpl(
		com.esquare.ecommerce.model.ProductInventory productInventory)
		throws SystemException {
		productInventory = toUnwrappedModel(productInventory);

		boolean isNew = productInventory.isNew();

		Session session = null;

		try {
			session = openSession();

			if (productInventory.isNew()) {
				session.save(productInventory);

				productInventory.setNew(false);
			}
			else {
				session.merge(productInventory);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductInventoryModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
			ProductInventoryImpl.class, productInventory.getPrimaryKey(),
			productInventory);

		clearUniqueFindersCache(productInventory);
		cacheUniqueFindersCache(productInventory);

		return productInventory;
	}

	protected ProductInventory toUnwrappedModel(
		ProductInventory productInventory) {
		if (productInventory instanceof ProductInventoryImpl) {
			return productInventory;
		}

		ProductInventoryImpl productInventoryImpl = new ProductInventoryImpl();

		productInventoryImpl.setNew(productInventory.isNew());
		productInventoryImpl.setPrimaryKey(productInventory.getPrimaryKey());

		productInventoryImpl.setInventoryId(productInventory.getInventoryId());
		productInventoryImpl.setCompanyId(productInventory.getCompanyId());
		productInventoryImpl.setCreateDate(productInventory.getCreateDate());
		productInventoryImpl.setModifiedDate(productInventory.getModifiedDate());
		productInventoryImpl.setProductDetailsId(productInventory.getProductDetailsId());
		productInventoryImpl.setCustomFieldRecId(productInventory.getCustomFieldRecId());
		productInventoryImpl.setSku(productInventory.getSku());
		productInventoryImpl.setPrice(productInventory.getPrice());
		productInventoryImpl.setDiscount(productInventory.getDiscount());
		productInventoryImpl.setQuantity(productInventory.getQuantity());
		productInventoryImpl.setBarcode(productInventory.getBarcode());
		productInventoryImpl.setTaxes(productInventory.isTaxes());
		productInventoryImpl.setFreeShipping(productInventory.isFreeShipping());
		productInventoryImpl.setWeight(productInventory.getWeight());
		productInventoryImpl.setShippingFormula(productInventory.getShippingFormula());
		productInventoryImpl.setOutOfStockPurchase(productInventory.isOutOfStockPurchase());
		productInventoryImpl.setSimilarProducts(productInventory.isSimilarProducts());
		productInventoryImpl.setLimitCatalogs(productInventory.getLimitCatalogs());
		productInventoryImpl.setLimitProducts(productInventory.getLimitProducts());
		productInventoryImpl.setVisibility(productInventory.isVisibility());
		productInventoryImpl.setColumn1(productInventory.getColumn1());
		productInventoryImpl.setColumn2(productInventory.getColumn2());
		productInventoryImpl.setColumn3(productInventory.getColumn3());

		return productInventoryImpl;
	}

	/**
	 * Returns the product inventory with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the product inventory
	 * @return the product inventory
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductInventoryException, SystemException {
		ProductInventory productInventory = fetchByPrimaryKey(primaryKey);

		if (productInventory == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductInventoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return productInventory;
	}

	/**
	 * Returns the product inventory with the primary key or throws a {@link com.esquare.ecommerce.NoSuchProductInventoryException} if it could not be found.
	 *
	 * @param inventoryId the primary key of the product inventory
	 * @return the product inventory
	 * @throws com.esquare.ecommerce.NoSuchProductInventoryException if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory findByPrimaryKey(long inventoryId)
		throws NoSuchProductInventoryException, SystemException {
		return findByPrimaryKey((Serializable)inventoryId);
	}

	/**
	 * Returns the product inventory with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the product inventory
	 * @return the product inventory, or <code>null</code> if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ProductInventory productInventory = (ProductInventory)EntityCacheUtil.getResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
				ProductInventoryImpl.class, primaryKey);

		if (productInventory == _nullProductInventory) {
			return null;
		}

		if (productInventory == null) {
			Session session = null;

			try {
				session = openSession();

				productInventory = (ProductInventory)session.get(ProductInventoryImpl.class,
						primaryKey);

				if (productInventory != null) {
					cacheResult(productInventory);
				}
				else {
					EntityCacheUtil.putResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
						ProductInventoryImpl.class, primaryKey,
						_nullProductInventory);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ProductInventoryModelImpl.ENTITY_CACHE_ENABLED,
					ProductInventoryImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return productInventory;
	}

	/**
	 * Returns the product inventory with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param inventoryId the primary key of the product inventory
	 * @return the product inventory, or <code>null</code> if a product inventory with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductInventory fetchByPrimaryKey(long inventoryId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)inventoryId);
	}

	/**
	 * Returns all the product inventories.
	 *
	 * @return the product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductInventory> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product inventories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductInventoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product inventories
	 * @param end the upper bound of the range of product inventories (not inclusive)
	 * @return the range of product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductInventory> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the product inventories.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.ProductInventoryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product inventories
	 * @param end the upper bound of the range of product inventories (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductInventory> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProductInventory> list = (List<ProductInventory>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTINVENTORY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTINVENTORY;

				if (pagination) {
					sql = sql.concat(ProductInventoryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProductInventory>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductInventory>(list);
				}
				else {
					list = (List<ProductInventory>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the product inventories from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ProductInventory productInventory : findAll()) {
			remove(productInventory);
		}
	}

	/**
	 * Returns the number of product inventories.
	 *
	 * @return the number of product inventories
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTINVENTORY);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the product inventory persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.ProductInventory")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ProductInventory>> listenersList = new ArrayList<ModelListener<ProductInventory>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ProductInventory>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductInventoryImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRODUCTINVENTORY = "SELECT productInventory FROM ProductInventory productInventory";
	private static final String _SQL_SELECT_PRODUCTINVENTORY_WHERE = "SELECT productInventory FROM ProductInventory productInventory WHERE ";
	private static final String _SQL_COUNT_PRODUCTINVENTORY = "SELECT COUNT(productInventory) FROM ProductInventory productInventory";
	private static final String _SQL_COUNT_PRODUCTINVENTORY_WHERE = "SELECT COUNT(productInventory) FROM ProductInventory productInventory WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "productInventory.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProductInventory exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProductInventory exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductInventoryPersistenceImpl.class);
	private static ProductInventory _nullProductInventory = new ProductInventoryImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ProductInventory> toCacheModel() {
				return _nullProductInventoryCacheModel;
			}
		};

	private static CacheModel<ProductInventory> _nullProductInventoryCacheModel = new CacheModel<ProductInventory>() {
			@Override
			public ProductInventory toEntityModel() {
				return _nullProductInventory;
			}
		};
}