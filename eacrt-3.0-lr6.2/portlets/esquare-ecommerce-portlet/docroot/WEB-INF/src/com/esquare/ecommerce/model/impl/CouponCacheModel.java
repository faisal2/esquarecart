/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Coupon;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Coupon in entity cache.
 *
 * @author Esquare
 * @see Coupon
 * @generated
 */
public class CouponCacheModel implements CacheModel<Coupon>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{couponId=");
		sb.append(couponId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", version=");
		sb.append(version);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", couponCode=");
		sb.append(couponCode);
		sb.append(", couponName=");
		sb.append(couponName);
		sb.append(", description=");
		sb.append(description);
		sb.append(", couponLimit=");
		sb.append(couponLimit);
		sb.append(", userLimit=");
		sb.append(userLimit);
		sb.append(", discountType=");
		sb.append(discountType);
		sb.append(", discount=");
		sb.append(discount);
		sb.append(", minOrder=");
		sb.append(minOrder);
		sb.append(", discountLimit=");
		sb.append(discountLimit);
		sb.append(", limitCatalogs=");
		sb.append(limitCatalogs);
		sb.append(", limitProducts=");
		sb.append(limitProducts);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", active=");
		sb.append(active);
		sb.append(", archived=");
		sb.append(archived);
		sb.append(", neverExpire=");
		sb.append(neverExpire);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Coupon toEntityModel() {
		CouponImpl couponImpl = new CouponImpl();

		couponImpl.setCouponId(couponId);
		couponImpl.setGroupId(groupId);
		couponImpl.setCompanyId(companyId);
		couponImpl.setUserId(userId);
		couponImpl.setVersion(version);

		if (userName == null) {
			couponImpl.setUserName(StringPool.BLANK);
		}
		else {
			couponImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			couponImpl.setCreateDate(null);
		}
		else {
			couponImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			couponImpl.setModifiedDate(null);
		}
		else {
			couponImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (couponCode == null) {
			couponImpl.setCouponCode(StringPool.BLANK);
		}
		else {
			couponImpl.setCouponCode(couponCode);
		}

		if (couponName == null) {
			couponImpl.setCouponName(StringPool.BLANK);
		}
		else {
			couponImpl.setCouponName(couponName);
		}

		if (description == null) {
			couponImpl.setDescription(StringPool.BLANK);
		}
		else {
			couponImpl.setDescription(description);
		}

		couponImpl.setCouponLimit(couponLimit);
		couponImpl.setUserLimit(userLimit);

		if (discountType == null) {
			couponImpl.setDiscountType(StringPool.BLANK);
		}
		else {
			couponImpl.setDiscountType(discountType);
		}

		couponImpl.setDiscount(discount);
		couponImpl.setMinOrder(minOrder);

		if (discountLimit == null) {
			couponImpl.setDiscountLimit(StringPool.BLANK);
		}
		else {
			couponImpl.setDiscountLimit(discountLimit);
		}

		if (limitCatalogs == null) {
			couponImpl.setLimitCatalogs(StringPool.BLANK);
		}
		else {
			couponImpl.setLimitCatalogs(limitCatalogs);
		}

		if (limitProducts == null) {
			couponImpl.setLimitProducts(StringPool.BLANK);
		}
		else {
			couponImpl.setLimitProducts(limitProducts);
		}

		if (startDate == Long.MIN_VALUE) {
			couponImpl.setStartDate(null);
		}
		else {
			couponImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			couponImpl.setEndDate(null);
		}
		else {
			couponImpl.setEndDate(new Date(endDate));
		}

		couponImpl.setActive(active);
		couponImpl.setArchived(archived);
		couponImpl.setNeverExpire(neverExpire);

		couponImpl.resetOriginalValues();

		return couponImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		couponId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		version = objectInput.readDouble();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		couponCode = objectInput.readUTF();
		couponName = objectInput.readUTF();
		description = objectInput.readUTF();
		couponLimit = objectInput.readInt();
		userLimit = objectInput.readInt();
		discountType = objectInput.readUTF();
		discount = objectInput.readDouble();
		minOrder = objectInput.readDouble();
		discountLimit = objectInput.readUTF();
		limitCatalogs = objectInput.readUTF();
		limitProducts = objectInput.readUTF();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();
		active = objectInput.readBoolean();
		archived = objectInput.readBoolean();
		neverExpire = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(couponId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeDouble(version);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (couponCode == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(couponCode);
		}

		if (couponName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(couponName);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeInt(couponLimit);
		objectOutput.writeInt(userLimit);

		if (discountType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(discountType);
		}

		objectOutput.writeDouble(discount);
		objectOutput.writeDouble(minOrder);

		if (discountLimit == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(discountLimit);
		}

		if (limitCatalogs == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(limitCatalogs);
		}

		if (limitProducts == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(limitProducts);
		}

		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);
		objectOutput.writeBoolean(active);
		objectOutput.writeBoolean(archived);
		objectOutput.writeBoolean(neverExpire);
	}

	public long couponId;
	public long groupId;
	public long companyId;
	public long userId;
	public double version;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String couponCode;
	public String couponName;
	public String description;
	public int couponLimit;
	public int userLimit;
	public String discountType;
	public double discount;
	public double minOrder;
	public String discountLimit;
	public String limitCatalogs;
	public String limitProducts;
	public long startDate;
	public long endDate;
	public boolean active;
	public boolean archived;
	public boolean neverExpire;
}