/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import java.util.Map;

import com.esquare.ecommerce.model.WishListItem;
import com.esquare.ecommerce.service.WishListLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * The extended model implementation for the WishList service. Represents a row in the &quot;EEC_WishList&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.model.WishList} interface.
 * </p>
 *
 * @author Esquare
 */
public class WishListImpl extends WishListBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a wish list model instance should use the {@link com.esquare.ecommerce.model.WishList} interface instead.
	 */
	public WishListImpl() {
	}
	public Map<WishListItem, Integer> getProductinventories() throws SystemException {
		return WishListLocalServiceUtil.getProductInventories(getCompanyId(), getProductInventoryIds());
	}

	public int getProductsSize() {
		return StringUtil.split(getProductInventoryIds()).length;
	}
	public void addProductInventoryId(long productInventoryId) {
		setProductInventoryIds(StringUtil.add(getProductInventoryIds(), String.valueOf(productInventoryId),
				StringPool.COMMA, true));
	}
}