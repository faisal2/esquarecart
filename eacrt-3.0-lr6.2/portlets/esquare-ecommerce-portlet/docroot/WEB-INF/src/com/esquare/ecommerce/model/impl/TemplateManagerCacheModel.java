/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.TemplateManager;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TemplateManager in entity cache.
 *
 * @author Esquare
 * @see TemplateManager
 * @generated
 */
public class TemplateManagerCacheModel implements CacheModel<TemplateManager>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(17);

		sb.append("{recId=");
		sb.append(recId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", cssId=");
		sb.append(cssId);
		sb.append(", larName=");
		sb.append(larName);
		sb.append(", templateDesc=");
		sb.append(templateDesc);
		sb.append(", folderId=");
		sb.append(folderId);
		sb.append(", packageType=");
		sb.append(packageType);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TemplateManager toEntityModel() {
		TemplateManagerImpl templateManagerImpl = new TemplateManagerImpl();

		templateManagerImpl.setRecId(recId);

		if (title == null) {
			templateManagerImpl.setTitle(StringPool.BLANK);
		}
		else {
			templateManagerImpl.setTitle(title);
		}

		if (cssId == null) {
			templateManagerImpl.setCssId(StringPool.BLANK);
		}
		else {
			templateManagerImpl.setCssId(cssId);
		}

		if (larName == null) {
			templateManagerImpl.setLarName(StringPool.BLANK);
		}
		else {
			templateManagerImpl.setLarName(larName);
		}

		if (templateDesc == null) {
			templateManagerImpl.setTemplateDesc(StringPool.BLANK);
		}
		else {
			templateManagerImpl.setTemplateDesc(templateDesc);
		}

		templateManagerImpl.setFolderId(folderId);

		if (packageType == null) {
			templateManagerImpl.setPackageType(StringPool.BLANK);
		}
		else {
			templateManagerImpl.setPackageType(packageType);
		}

		if (modifiedDate == Long.MIN_VALUE) {
			templateManagerImpl.setModifiedDate(null);
		}
		else {
			templateManagerImpl.setModifiedDate(new Date(modifiedDate));
		}

		templateManagerImpl.resetOriginalValues();

		return templateManagerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recId = objectInput.readLong();
		title = objectInput.readUTF();
		cssId = objectInput.readUTF();
		larName = objectInput.readUTF();
		templateDesc = objectInput.readUTF();
		folderId = objectInput.readLong();
		packageType = objectInput.readUTF();
		modifiedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (cssId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cssId);
		}

		if (larName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(larName);
		}

		if (templateDesc == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(templateDesc);
		}

		objectOutput.writeLong(folderId);

		if (packageType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(packageType);
		}

		objectOutput.writeLong(modifiedDate);
	}

	public long recId;
	public String title;
	public String cssId;
	public String larName;
	public String templateDesc;
	public long folderId;
	public String packageType;
	public long modifiedDate;
}