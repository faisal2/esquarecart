/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.ShippingRates;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing ShippingRates in entity cache.
 *
 * @author Esquare
 * @see ShippingRates
 * @generated
 */
public class ShippingRatesCacheModel implements CacheModel<ShippingRates>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{recId=");
		sb.append(recId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", countryId=");
		sb.append(countryId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", shippingType=");
		sb.append(shippingType);
		sb.append(", rangeFrom=");
		sb.append(rangeFrom);
		sb.append(", rangeTo=");
		sb.append(rangeTo);
		sb.append(", price=");
		sb.append(price);
		sb.append(", xml=");
		sb.append(xml);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ShippingRates toEntityModel() {
		ShippingRatesImpl shippingRatesImpl = new ShippingRatesImpl();

		shippingRatesImpl.setRecId(recId);
		shippingRatesImpl.setCompanyId(companyId);

		if (countryId == null) {
			shippingRatesImpl.setCountryId(StringPool.BLANK);
		}
		else {
			shippingRatesImpl.setCountryId(countryId);
		}

		if (name == null) {
			shippingRatesImpl.setName(StringPool.BLANK);
		}
		else {
			shippingRatesImpl.setName(name);
		}

		if (shippingType == null) {
			shippingRatesImpl.setShippingType(StringPool.BLANK);
		}
		else {
			shippingRatesImpl.setShippingType(shippingType);
		}

		shippingRatesImpl.setRangeFrom(rangeFrom);
		shippingRatesImpl.setRangeTo(rangeTo);
		shippingRatesImpl.setPrice(price);

		if (xml == null) {
			shippingRatesImpl.setXml(StringPool.BLANK);
		}
		else {
			shippingRatesImpl.setXml(xml);
		}

		shippingRatesImpl.resetOriginalValues();

		return shippingRatesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recId = objectInput.readLong();
		companyId = objectInput.readLong();
		countryId = objectInput.readUTF();
		name = objectInput.readUTF();
		shippingType = objectInput.readUTF();
		rangeFrom = objectInput.readDouble();
		rangeTo = objectInput.readDouble();
		price = objectInput.readDouble();
		xml = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recId);
		objectOutput.writeLong(companyId);

		if (countryId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(countryId);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (shippingType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shippingType);
		}

		objectOutput.writeDouble(rangeFrom);
		objectOutput.writeDouble(rangeTo);
		objectOutput.writeDouble(price);

		if (xml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(xml);
		}
	}

	public long recId;
	public long companyId;
	public String countryId;
	public String name;
	public String shippingType;
	public double rangeFrom;
	public double rangeTo;
	public double price;
	public String xml;
}