/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.LinkedHashMap;
import java.util.List;

import com.esquare.ecommerce.NoSuchProductInventoryException;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.service.base.ProductInventoryLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.ProductInventoryFinderUtil;
import com.esquare.ecommerce.service.persistence.ProductInventoryUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * The implementation of the product inventory local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.ProductInventoryLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.ProductInventoryLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil
 */
public class ProductInventoryLocalServiceImpl extends
		ProductInventoryLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * Never reference this interface directly. Always use {@link
	 * com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil} to access
	 * the product inventory local service.
	 */
	public ProductInventory findByProductDetailsId(long productDetailsId)
			throws SystemException, NoSuchProductInventoryException {
		return ProductInventoryUtil.findByProductDetailsId(productDetailsId);
	}

	public List<ProductInventory> getProductList(String catalogIds,
			int viewCount) throws SystemException,
			NoSuchProductInventoryException {
		return ProductInventoryFinderUtil.getProductList(catalogIds, viewCount);
	}

	public int getStockCount(long companyId, int quantity)
			throws SystemException {
		return ProductInventoryFinderUtil.getStockCount(companyId, quantity);
	}

	public ProductInventory fetchBySKU(long companyId, String sku)
			throws SystemException {

		return productInventoryPersistence.fetchBySKU(companyId, sku);
	}

	public double getPriceRangeByCatalogId(String catalogIds, String orderType)
			throws SystemException, NoSuchProductInventoryException {
		return ProductInventoryFinderUtil.getPriceRangeByCatalogId(catalogIds,
				orderType);
	}

	public void getCatalogIdsbyCreateDate(long companyId,
			LinkedHashMap<String, String> childCatalogMap)
			throws SystemException, NoSuchProductInventoryException {
		ProductInventoryFinderUtil.getCatalogIdsbyCreateDate(companyId,
				childCatalogMap);
	}

	public void getCatalogIdsbyDiscount(long companyId,
			LinkedHashMap<String, String> childCatalogMap)
			throws SystemException, NoSuchProductInventoryException {
		ProductInventoryFinderUtil.getCatalogIdsbyDiscount(companyId,
				childCatalogMap);
	}

	public List<ProductInventory> getProductListByPricePange(long companyId,
			long curProductId, String searchName, String catalogIds,
			String priceRanges, String sortByName, String cmd)
			throws SystemException, NoSuchProductInventoryException {
		return ProductInventoryFinderUtil.getProductListByPricePange(companyId,
				curProductId, searchName, catalogIds, priceRanges, sortByName,
				cmd);
	}

	public List<ProductInventory> getRelatedProductList(long catalogId)
			throws SystemException, NoSuchProductInventoryException {
		return ProductInventoryFinderUtil.getRelatedProductList(catalogId);
	}

	public List<ProductInventory> getShowMoreProductList(long companyId,
			long curProductId, String searchName, String catalogIds,
			String priceRanges, String sortByName, String cmd, int limit)
			throws SystemException, NoSuchProductInventoryException {
		return ProductInventoryFinderUtil.getShowMoreProductList(companyId,
				curProductId, searchName, catalogIds, priceRanges, sortByName,
				cmd, limit);
	}

	public void getCatalogIdsbySearch(long companyId, String productName,
			LinkedHashMap<String, String> childCatalogMap)
			throws SystemException, NoSuchProductInventoryException {
		ProductInventoryFinderUtil.getCatalogIdsbySearch(companyId,
				productName, childCatalogMap);
	}

	public void getCatalogIdsbySimilarProduct(long companyId,
			long productInventoryId,
			LinkedHashMap<String, String> childCatalogMap)
			throws PortalException, SystemException {
		double price = ProductInventoryLocalServiceUtil.getProductInventory(
				productInventoryId).getPrice();
		double priceRangeOffset = 0l;
		try {
			priceRangeOffset = Double.parseDouble(EECUtil.getPreference(
					companyId, EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT));
		} catch (NumberFormatException exception) {
			priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
		} catch (NullPointerException exception) {
			priceRangeOffset = PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		double priceRangeOffsetValue = price * (priceRangeOffset / 100);
		ProductInventoryFinderUtil.getCatalogIdsbySimilarProduct(companyId,
				price - priceRangeOffsetValue, price + priceRangeOffsetValue,
				childCatalogMap);
	}

	public void getCatalogIdsbyRelatedProduct(long companyId,
			long productInventoryId,
			LinkedHashMap<String, String> childCatalogMap)
			throws PortalException, SystemException {
		ProductInventoryFinderUtil.getCatalogIdsbyRelatedProduct(companyId,
				productInventoryId, childCatalogMap);

	}
	public List<ProductInventory> getSimilarProduct(long companyId,long curInventoryId, double minPrice,
			double maxPrice,boolean visibility , boolean descOrder ,int limit) {
				return ProductInventoryFinderUtil.getSimilarProduct(companyId, curInventoryId,minPrice, maxPrice, visibility, descOrder, limit);
		
	}
	public List<ProductInventory> getRelatedProduct(long companyId,String limitProducts,boolean visibility , boolean descOrder ,int limit) {
				return ProductInventoryFinderUtil.getRelatedProduct(companyId,limitProducts, visibility, descOrder, limit);
	}

	private static Log _log = LogFactoryUtil
			.getLog(ProductInventoryLocalServiceImpl.class);
}