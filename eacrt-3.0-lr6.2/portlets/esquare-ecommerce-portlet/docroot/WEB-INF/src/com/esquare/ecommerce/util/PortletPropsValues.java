package com.esquare.ecommerce.util;

import java.util.Date;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.util.portlet.PortletProps;

public class PortletPropsValues {

	public static String DEFAULT_TEMPLATE_LAR_FOLDER_PATH = PortletProps.get("default.template.lar.folder.path");

	public static String BASIC_B2C_TEMPLATE_LAR_NAME = PortletProps.get("basic.b2c.template.lar.name");

	public static String BASIC_B2B_TEMPLATE_LAR_NAME = PortletProps.get("basic.b2b.template.lar.name");
	
	public static String ADMIN_TEMPLATE_LAR_NAME = PortletProps.get("admin.template.lar.names");

	public static String DEFAULT_SHOP_ADMIN_FIRSTNAME = PortletProps.get("default.shop.admin.firstname");
	
	public static String DEFAULT_SHOP_ADMIN_LASTTNAME = PortletProps.get("default.shop.admin.lastname");
	
	public static String[] TEMPLATE_COLOR_SCHEME = PortletProps.getArray("template.color.scheme");
	
	public static String TEMPLATE_IMAGE = PortletProps.get("template.image");

	public static String TEMPLATE_ADD_IMAGE = PortletProps.get("template.add.image");

	public static String TEMPLATE_TEMPLATE_IMAGE = PortletProps.get("template.template.image");

	public static String TEMPLATE_ECOMMERCE_TEMPLATES = PortletProps.get("template.ecommerce.templates");

	public static String DEFAULT_SHOP_ADMIN_ROLE = PortletProps.get("default.shop.admin.role");

	public static String LIFERAY_REGULAR_ROLE_TYPE = PortletProps.get("liferay.regular.role.type");

	public static String DEFAULT_SHOP_ADMIN_DESCRIPTION = PortletProps.get("default.shop.admin.description");

	public static String DEFAULT_ECC_CATALOG_DESCRIPTION = PortletProps.get("default.ecc.catalog.description");

	public static String INSTANCE_NAME = PortletProps.get("instance.name");

	public static String INSTANCE_EMAILID = PortletProps.get("instance.email");

	public static String INSTANCE_PASSWORD = PortletProps.get("instance.password");

	public static String INSTANCE_VIRTUAL_HOST = PortletProps.get("instance.virtual.host");
	
	// Twitter Connect SSO start
	public static boolean TWITTER_CONNECT_AUTH_ENABLED = GetterUtil.getBoolean(PortletProps.get("twitter.connect.auth.enabled"));

	public static String TWITTER_CONNECT_APP_ID = PortletProps.get("twitter.connect.app.id");

	public static String TWITTER_CONNECT_APP_SECRET = PortletProps.get("twitter.connect.app.secret");

	public static String TWITTER_CONNECT_OAUTH_AUTH_URL = PortletProps.get("twitter.connect.oauth.auth.url");

	public static String  TWITTER_CONNECT_OAUTH_AUTH_VERIFY_CREDENTIALS_URL = PortletProps.get("twitter.connect.oauth.verify.credentials.url");
	
	// Twitter Connect SSO end
  
	
	// LinkedIn Connect SSO start
	public static boolean LINKEDIN_CONNECT_AUTH_ENABLED = GetterUtil.getBoolean(PortletProps.get("linkedIn.connect.auth.enabled"));
	
	public static String LINKEDIN_CONNECT_APP_ID = PortletProps.get("linkedIn.connect.app.id");

	public static String LINKEDIN_CONNECT_APP_SECRET = PortletProps.get("linkedIn.connect.app.secret");

	public static boolean LINKEDIN_CONNECT_PROFILE_VIEW_ENABLED = GetterUtil.getBoolean(PortletProps.get("linkedIn.connect.profile.view.enabled"));

	public static boolean LINKEDIN_CONNECT_AUTO_LOGIN_ENABLED = GetterUtil.getBoolean(PortletProps.get("linkedIn.connect.auto.login.enabled"));

	
	
	// LinkedIn Connect SSO end
	
	public static String ECOMMERCE_CATALOGS=PortletProps.get("ecommerce-catalogs");

	public static final boolean CAPTCHA_CHECK_PORTAL_SEND_PASSWORD = GetterUtil.getBoolean(PropsUtil.get(PropsKeys.CAPTCHA_CHECK_PORTAL_SEND_PASSWORD));

	public static final boolean USERS_REMINDER_QUERIES_ENABLED = GetterUtil.getBoolean(PropsUtil.get(PropsKeys.USERS_REMINDER_QUERIES_ENABLED));

	public static final boolean USERS_REMINDER_QUERIES_REQUIRED = GetterUtil.getBoolean(PropsUtil.get(PropsKeys.USERS_REMINDER_QUERIES_REQUIRED));

	public static String DEFAULT_INSTANCE_MAIL_DOMAIN = PortletProps.get("default.instance.mail.domain");
	
	public static String CURRENT_WORKING_ENVIRONMENT = PortletProps.get("current.working.environment");
	
	public static String PACKAGE_TYPE_FREE = PortletProps.get("package.type.free");
	
	public static String PACKAGE_TYPE_BASIC = PortletProps.get("package.type.basic");
	
	public static String PACKAGE_TYPE_STANDARD = PortletProps.get("package.type.standard");
	
	public static String PACKAGE_TYPE_PREMIUM = PortletProps.get("package.type.premium");
	
	public static String PACKAGE_TYPE_ENTERPRISE = PortletProps.get("package.type.enterprise");
	
	public static String[] PACKAGE_TYPES = PortletProps.getArray("package.types");
	
	public static long GROUP_ID = GetterUtil.getLong(PortletProps.get("groupId.instance"));
	
	public static String DEFAULT_HEADER_TEMPLATE = PortletProps.get("default.header.template.name");
	
	public static String DEFAULT_FOOTER_TEMPLATE = PortletProps.get("default.footer.template.name");
	
	public static String DEFAULT_LIST_TEMPLATE = PortletProps.get("default.list.template.name");
	
	public static String HOMEPAGE_GRID_TEMPLATE = PortletProps.get("home.grid.template.name");
	
	public static String DEFAULT_GRID_TEMPLATE = PortletProps.get("default.grid.template.name");
	
	public static String DEFAULT_DETAIL_TEMPLATE = PortletProps.get("default.detail.template.name");
	
	public static String DEFINE_FIELDS_TEMPLATE = PortletProps.get("define.fields.template.name");
	
	public static String DEFINE_FIELDS_HEADER_TEMPLATE = PortletProps.get("define.fields.header.template.name");
	
	
	public static String DEFINE_FIELDS_FOOTER_TEMPLATE = PortletProps.get("define.fields.footer.template.name");
	
	public static String CART_HEADER_TEMPLATE = PortletProps.get("cart.header.template.name");
	
	public static String CART_CONTENT_TEMPLATE = PortletProps.get("cart.content.template.name");
	
	public static String CART_FOOTER_TEMPLATE = PortletProps.get("cart.footer.template.name");
	
	public static String SHOP_ADMIN_ROLE = PortletProps.get("shop.admin.role");
	
	public static String EEC_GRIDCLASS = PortletProps.get("eec.gridclass");
	
	public static String EEC_HOME_GRIDCLASS = PortletProps.get("eec.home.gridclass");
	
	public static String EEC_LISTCLASS = PortletProps.get("eec.listclass");
	
	public static String EEC_WISH_LIST_CLASS = PortletProps.get("eec.wislistclass");
	
	public static String[] PACKAGE_PERMISSION_KEY = PortletProps.getArray("package.permission.key");
	
	public static String ORDERSUMMARY_CONTENT_TEMPLATE = PortletProps.get("ordersummary.content.template.name");
	
	public static String[] PACKAGE_PERMISSION_PRICE = PortletProps.getArray("package.permission.price");

	public static String[] FREE_PACKAGE_PERMISSION_VALUE = PortletProps.getArray("free.package.permission.value");

	public static String[] BASIC_PACKAGE_PERMISSION_VALUE = PortletProps.getArray("basic.package.permission.value");

	public static String[] STANDARD_PACKAGE_PERMISSION_VALUE = PortletProps.getArray("standard.package.permission.value");
	
	public static long EEC_MINIMUM_ORDER = GetterUtil.getLong(PortletProps.get("eec.minimum.order"));
	
	public static double EEC_TAX_RATE = GetterUtil.getDouble(PortletProps.get("eec.tax.rate"));
	
	public static final String EBS_SECRET_KEY = PortletProps.get("ebs.secret.key");
	
	public static final String EBS_ACCOUNT_ID = PortletProps.get("ebs.account.id");
	
	public static final String EBS_PAYMENT_MODE = PortletProps.get("ebs.payment.mode");
	
	public static final String SHOWROOM_CITY_NAMES = PortletProps.get("showroom.city.names");
	
	public static int REQUIRED_DAYS_TO_DELIVER_NEAR = GetterUtil.getInteger(PortletProps.get("required.days.to.deliver.near"));
	
	public static int REQUIRED_DAYS_TO_DELIVER_FAR = GetterUtil.getInteger(PortletProps.get("required.days.to.deliver.far"));
	
	public static String[] PREMIUM_PACKAGE_PERMISSION_VALUE = PortletProps.getArray("premium.package.permission.value");
	
	public static String[] ENTERPRISE_PACKAGE_PERMISSION_VALUE = PortletProps.getArray("enterprise.package.permission.value");
	
	public static String DEFAULT_B2C_TEMPLATE_NAME = PortletProps.get("default.b2c.template.name");
	
	public static final String SEARCH_TEXT_PLACEHOLDER = PortletProps.get("search.text.placeholder");
	
	public static String PACKAGE_MONTHS_DURATION = PortletProps.get("package.months.duration");
	
	public static int MONTHLY_FREE_PACKAGE_CHARGES = GetterUtil.getInteger(PortletProps.get("monthly.free.package.charges"));
	
	public static int MONTHLY_BASIC_PACKAGE_CHARGES = GetterUtil.getInteger(PortletProps.get("monthly.basic.package.charges"));
	
	public static int MONTHLY_STANDARD_PACKAGE_CHARGES = GetterUtil.getInteger(PortletProps.get("monthly.standard.package.charges"));
	
	public static int MONTHLY_PREMIUM_PACKAGE_CHARGES = GetterUtil.getInteger(PortletProps.get("monthly.premium.package.charges"));
	
	public static int MONTHLY_ENTERPRISE_PACKAGE_CHARGES = GetterUtil.getInteger(PortletProps.get("monthly.enterprise.package.charges"));
	
	public static String[] BASIC_PACKAGE_PERMISSION_PRICE = PortletProps.getArray("basic.package.permission.price");
	
	public static String[] STANDARD_PACKAGE_PERMISSION_PRICE = PortletProps.getArray("standard.package.permission.price");
	
	public static String[] PREMIUM_PACKAGE_PERMISSION_PRICE = PortletProps.getArray("premium.package.permission.price");
	
	public static final String WISHLIST_CONTENT_TEMP=PortletProps.get("wishlist.content.temp");
	
	public static final String WISHLIST_TITLE_NAME=PortletProps.get("wishlist.title.name");
	
	public static final String ORDER_TRACKER_TITLE_NAME=PortletProps.get("order.tracker.title.name");
	
	public static final String MY_WALLET_TRACKER=PortletProps.get("my.wallet.tracker");
	
	public static final String WALLET_USED_AMOUNT=PortletProps.get("wallet.used.amount");
	
	public static final String DEFAULT_SHIPPINGRATE_NAME=PortletProps.get("default.shippingrate.name");
	
	public static final String SHIPPING_PRICE_CRITERIA=PortletProps.get("shipping.price.criteria");
	
	public static final String SHIPPING_WEIGHT_CRITERIA=PortletProps.get("shipping.weight.criteria");
	
	public static double DEFAULT_SHIPPING_PRICE=GetterUtil.getDouble(PortletProps.get("default.shipping.price"));
	
	public static long DEFAULT_SHIPPING_RANGE_FROM=GetterUtil.getLong(PortletProps.get("default.shipping.rangeFrom"));
	
	public static long DEFAULT_SHIPPING_RANGE_TO=GetterUtil.getLong(PortletProps.get("default.shipping.rangeTo"));
	
	public static String DETAILSVIEW_TAXES_CONTENT=PortletProps.get("detailview.taxes.content");
			
	public static String DETAILSVIEW_FREESHIPPING_CONTENT=PortletProps.get("detailview.freeshipping.content");
	
	public static final String CreateAccount_EMAIL_CONFIRMATION_BODY=PortletProps.get("CreateAccount.emailConfirmation.body");
	
	public static final String SendCoupon_EMAIL_CONFIRMATION_BODY = PortletProps.get("SendCoupon.emailConfirmation.body");
	
	public static final String Order_EMAIL_CONFIRMATION_BODY = PortletProps.get("Order.emailConfirmation.body");
	
	public static final String Shipping_EMAIL_CONFIRMATION_BODY = PortletProps.get("Shipping.emailConfirmation.body");
	
	public static final String Cancel_EMAIL_CONFIRMATION_BODY = PortletProps.get("Cancel.emailConfirmation.body");
	
	public static final String Return_EMAIL_CONFIRMATION_BODY = PortletProps.get("Return.emailConfirmation.body");
	
	public static final String Delivered_EMAIL_CONFIRMATION_BODY = PortletProps.get("Delivered.emailConfirmation.body");
	
	public static final String CancelReq_EMAIL_CONFIRMATION_BODY = PortletProps.get("CancelReq.emailConfirmation.body");
	
	public static final String SubscribedUser_EMAIL_CONFIRMATION_BODY = PortletProps.get("SubscribedUser.emailConfirmation.body");
	
	
	
	public static final String CreateAccount_SMS_CONFIRMATION_BODY=PortletProps.get("CreateAccount.smsConfirmation.body");
	
	public static final String SendCoupon_SMS_CONFIRMATION_BODY = PortletProps.get("SendCoupon.smsConfirmation.body");
	
	public static final String Order_SMS_CONFIRMATION_BODY = PortletProps.get("Order.smsConfirmation.body");
	
	public static final String Shipping_SMS_CONFIRMATION_BODY = PortletProps.get("Shipping.smsConfirmation.body");
	
	public static final String Cancel_SMS_CONFIRMATION_BODY = PortletProps.get("Cancel.smsConfirmation.body");
	
	public static final String Return_SMS_CONFIRMATION_BODY = PortletProps.get("Return.smsConfirmation.body");
	
	public static final String Delivered_SMS_CONFIRMATION_BODY = PortletProps.get("Delivered.smsConfirmation.body");
	
	public static final String CancelReq_SMS_CONFIRMATION_BODY = PortletProps.get("CancelReq.smsConfirmation.body");
	
	public static final String SubscribedUser_SMS_CONFIRMATION_BODY = PortletProps.get("SubscribedUser.smsConfirmation.body");
	
	
	
	public static String CART_EMPTY_MESSAGE = PortletProps.get("cart.empty.message");
	
	public static String DEFINE_FIELDS_NOTAVAILABLE = PortletProps.get("define.fields.notavailable");

	public static String BEFORE_EXPIRE_ALERT_BODY = PortletProps.get("before.expire.alert.body");

	public static String AFTER_EXPIRE_ALERT_BODY = PortletProps.get("after.expire.alert.body");

	public static String EXPIRE_ALERT_BODY = PortletProps.get("expire.alert.body");

	public static String DELETE_ALERT_BODY = PortletProps.get("delete.alert.body");
	
	public static String BEFORE_EXPIRE_ALERT_SUBJECT = PortletProps.get("before.expire.alert.subject");

	public static String AFTER_EXPIRE_ALERT_SUBJECT = PortletProps.get("after.expire.alert.subject");

	public static String EXPIRE_ALERT_SUBJECT = PortletProps.get("expire.alert.subject");

	public static String DELETE_ALERT_SUBJECT = PortletProps.get("delete.alert.subject");
	
	public static String EEC_NEWLAUNCH_CLASS = PortletProps.get("eec.newlaunch");
	
	public static String EEC_DISCOUNT_CLASS = PortletProps.get("eec.discount");
	
	public static String EEC_BESTSELLER_CLASS = PortletProps.get("eec.bestseller");

	public static String[] COMPARE_COMMON_PROPERTIES = PortletProps.getArray("compare.common.properties");
	
	public static final String CUSTOM_FIELD_TAG = PortletProps.get("custom-field.tag");
	
	public static final String REGION_TAG = PortletProps.get("region.tag");
	
	public static final String ID_ATTRIBUTE = PortletProps.get("id.attribute");
	
	public static final String NAME_ATTRIBUTE = PortletProps.get("name.attribute");
	
	public static final String VALUE_ATTRIBUTE = PortletProps.get("value.attribute");
	
	//package billing start
	
	public static String PACKAGE_BILLING_TABS = PortletProps.get("package.billing.tabs");
	
	public static Date PACKAGE_BILLING_VIEW_YEAR_RANGE_START= GetterUtil.getDate(PortletProps.get("package.billing.view.yearRangeStart"), EECConstants.sdf);
	
	public static final String PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_BODY = PortletProps.get("package.billing.default.payment.confirmation.email.body");
	
	public static final String PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_SUBJECT = PortletProps.get("package.billing.default.payment.confirmation.email.subject");
	
	public static String PACKAGE_BILLING_VIEW_BILL_TEMPLATE = PortletProps.get("package.billing.view.bill.template");
	
	public static final String PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_BODY_NAME = PortletProps.get("package.billing.payment.confirmation.email.body.name");
	
	public static final String PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_SUBJECT_NAME = PortletProps.get("package.billing.payment.confirmation.email.subject.name");
	
	public static String PACKAGE_BILLING_VIEW_BILL_TEMPLATE_NAME = PortletProps.get("package.billing.view.bill.template.name");
	
	//package billing end
	public static int NEW_LAUNCH_MONTH_DIFF = GetterUtil.getInteger(PortletProps.get("new-launch.month.diff"));

	public static String WISHLIST_HEADER_TEMPLATE = PortletProps.get("wishlist.header.template.name");
	
	public static String ECART_SHIPPING_ADDRESS = PortletProps.get("shipping.address.template.name");

	
	public static String ADMIN_EMAIL_FROM_NAME = PortletProps.get("admin.email.from.name");
	
	public static String ADMIN_EMAIL_FROM_ADDRESS = PortletProps.get("admin.email.from.address");
	
	public static String DEFAULT_PORTAL_URL = PortletProps.get("default.portal.url");
	
	public static String DEVELOPMENT_DEFAULT_PORTAL_URL = PortletProps.get("development.default.portal.url");
	
	public static String FILTER_PRICE_SLIDER_TYPE = PortletProps.get("filter.price.slider.type");
	
	public static String FILTER_PRICE_CHECKBOX_TYPE = PortletProps.get("filter.price.checkbox.type");
	
	public static String INSTANCE_CREATE_DEFAULT_CONFIRMATION_EMAIL_SUBJECT=PortletProps.get("instance.create.confirmation.email.subject");
	
	public static String INSTANCE_CREATE_DEFAULT_CONFIRMATION_EMAIL_BODY=PortletProps.get("instance.create.confirmation.email.body");
    //same name in template cntl panel
	public static final String INSTANCE_CREATE_CONFIRMATION_EMAIL_SUBJECT_NAME = PortletProps.get("instance.create.confirmation.subject.template.name");
	
	public static final String INSTANCE_CREATE_CONFIRMATION_EMAIL_BODY_NAME = PortletProps.get("instance.create.confirmation.body.template.name");
	
	public static int VIEWMORE_LIMIT= GetterUtil.getInteger(PortletProps.get("userview.view.more.limit"));
	
	public static String PRODUCT_IN_STOCK = PortletProps.get("product.in.stock");
	
	public static String PRODUCT_OUT_OF_STOCK = PortletProps.get("product.out.of.stock");
	
	public static int DELIVER_BUSINESSDAYS_MIN= GetterUtil.getInteger(PortletProps.get("deliver.business.min"));
	
	public static int DELIVER_BUSINESSDAYS_MAX= GetterUtil.getInteger(PortletProps.get("deliver.business.max"));
	
	public static String DETAILS_ADD_TO_COMPARE_DIV_ID= PortletProps.get("detail.add.comapre.div.id");
	
	public static String DETAILS_ADDED_TO_COMPARE_DIV_ID= PortletProps.get("detail.added.comapre.div.id");
	
	public static String WISHLIST_EMPTY_MESSAGE = PortletProps.get("wishlist.empty.message");
	
	public static String DEFAULT_CURRENCY_TYPE = PortletProps.get("default.currency.type");
	
	public static String[] REASON_TO_CANCEL_ORDER = PortletProps.getArray("reason.to.cancel.order");
	
	public static String[] WALLET_PAYMENT_TYPE = PortletProps.getArray("wallet.payment.type");
	
	public static String WALLET_PENDING_STATUS = PortletProps.get("wallet.pending.status");
	
	public static String WALLET_RECEIVED_STATUS = PortletProps.get("wallet.received.status");
	
	public static String ECAR_RELATED_AND_SIMILAR_PRODUCT_TTEMPLATE = PortletProps.get("ecart.similar.related.product.name");
	
	public static int SIMILAR_PRODUCT_OFFSET_LIMIT = GetterUtil.getInteger(PortletProps.get("similar.product.offset.limit"));
	
	public static String CATALOG_ERROR_MESSAGE = PortletProps.get("catalog.error.message");
	
	public static String PRODUCT_ERROR_MESSAGE = PortletProps.get("product.error.message");
	
	public static String DEFAULT_EXCELSHEET_DOWNLOAD = PortletProps.get("default.excelsheet.download");
}