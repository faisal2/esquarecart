package com.esquare.ecommerce.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import javax.portlet.ActionRequest;
import javax.portlet.PortletException;

import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.SMSConfiguration;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.SMSConfigurationLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.ContentUtil;
import com.liferay.util.portlet.PortletProps;

public class SMSUtil {

	public static void sendSMS(ActionRequest actionRequest, long orderId, String cmd)
			throws IOException, PortletException, SystemException {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String textMessage = ContentUtil.get(PortletProps.get(cmd+ ".smsConfirmation.subject"));
		Order order = null;
		try {
			order = OrderLocalServiceUtil.getOrder(orderId);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long companyId = themeDisplay.getCompanyId();
		String userName = "";
		String password = "";
		String smsType = "";
		
		String mobile = order.getShippingPhone();
		String trackNumber = order.getNumber();
		
		String[] oldStub = new String[] {"[$ORDER_NUMBER$]"};
		String[] newStub = new String[] {trackNumber};
		textMessage = StringUtil.replace(textMessage, oldStub, newStub);

		long id = CounterLocalServiceUtil.increment();
		List<SMSConfiguration> smsConfigurationList = SMSConfigurationLocalServiceUtil
				.findByCompanyId(companyId);

		for (SMSConfiguration smsConfiguration : smsConfigurationList) {
			if (smsConfiguration.getSmsType().equalsIgnoreCase(
					EECConstants.SMS_HHORIZON)) {
				userName = smsConfiguration.getUserName();
				password = smsConfiguration.getPassword();
			} else if (smsConfiguration.getSmsType().equalsIgnoreCase(
					EECConstants.VIANET)) {
				userName = smsConfiguration.getUserName();
				password = smsConfiguration.getPassword();
			}
		}

		HttpURLConnection connection = null;
		URL completeSenderURL = null;
		String conncetionResponse = null;

		String completeSenderURLString = null;
		if (smsType.equalsIgnoreCase(EECConstants.SMS_HHORIZON)) {
			completeSenderURLString = callSMSHorizon(userName, password,
					mobile, textMessage);
		} else if (smsType.equalsIgnoreCase(EECConstants.VIANET)) {
			completeSenderURLString = callVianet(userName, password, id,
					mobile, textMessage);
		} else {
			completeSenderURLString = callSMSHorizon(userName, password,
					mobile, textMessage);
		}

		_log.info("SMS Sender URL :::" + completeSenderURLString);

		// Create JAVA NET URL from URL String
		completeSenderURL = new URL(completeSenderURLString);
		connection = (HttpURLConnection) completeSenderURL.openConnection();
		connection.setDoOutput(false);
		connection.setDoInput(true);
		conncetionResponse = connection.getResponseMessage();
		int responseCode = connection.getResponseCode();
		String responseMessage = connection.getResponseMessage();
		if (responseCode == HttpURLConnection.HTTP_OK) {
			connection.disconnect();
			SessionMessages.add(actionRequest.getPortletSession(),
					"SMS-send-success");
		} else {
			SessionErrors.add(actionRequest.getPortletSession(),
					"SMS-send-error");
		}
	}

	@SuppressWarnings("unused")
	private String callWay2SMS(String userName, String password, long id,
			String mobileNumber, String textMessage, String action) {
		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {
			smsSenderURLQueryString.append("custid=undefined");
			smsSenderURLQueryString.append("&HiddenAction=instantsms");
			smsSenderURLQueryString.append("&Action="
					+ URLEncoder.encode(password, "UTF-8"));
			smsSenderURLQueryString.append("&login=&pass=&MobNo="
					+ URLEncoder.encode(mobileNumber, "UTF-8"));
			smsSenderURLQueryString.append("&textArea="
					+ URLEncoder.encode(textMessage, "UTF-8"));

			completeSenderURLString = EECConstants.WAY2_SMS_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private static String callVianet(String userName, String password, long id,
			String mobileNumber, String textMessage) {
		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {
			smsSenderURLQueryString.append("username="
					+ URLEncoder.encode(userName, "UTF-8"));
			smsSenderURLQueryString.append("&password="
					+ URLEncoder.encode(password, "UTF-8"));
			smsSenderURLQueryString.append("&msgid="
					+ URLEncoder.encode(String.valueOf(id), "UTF-8"));
			smsSenderURLQueryString.append("&tel="
					+ URLEncoder.encode(EECConstants.COUNTRY_CODE
							+ mobileNumber, "UTF-8"));
			smsSenderURLQueryString.append("&msg="
					+ URLEncoder.encode(textMessage, "UTF-8"));

			completeSenderURLString = EECConstants.VIANET_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private static String callSMSHorizon(String userName, String password,
			String mobileNumber, String textMessage) {

		StringBuilder smsSenderURLQueryString = new StringBuilder();
		String completeSenderURLString = null;
		try {
			smsSenderURLQueryString.append("user="
					+ URLEncoder.encode(userName, "UTF-8"));
			smsSenderURLQueryString.append("&apikey="
					+ URLEncoder.encode(EECConstants.SMS_HORIZON_API_VALUE,
							"UTF-8"));
			smsSenderURLQueryString.append("&mobile="
					+ URLEncoder.encode(mobileNumber, "UTF-8"));
			smsSenderURLQueryString.append("&message="
					+ URLEncoder.encode(textMessage, "UTF-8"));
			smsSenderURLQueryString.append("&senderid="
					+ URLEncoder.encode("MYTEXT", "UTF-8"));
			smsSenderURLQueryString.append("&type=" + "TXT");

			completeSenderURLString = EECConstants.SMS_HORIZON_HOST_URL
					+ StringPool.QUESTION + smsSenderURLQueryString.toString();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return completeSenderURLString;
	}

	private static Log _log = LogFactoryUtil.getLog(SMSUtil.class);
}
