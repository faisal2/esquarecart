package com.esquare.ecommerce.service.persistence;

import java.util.List;

import com.esquare.ecommerce.model.ShippingRates;
import com.esquare.ecommerce.model.impl.ShippingRatesImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ShippingRatesFinderImpl extends BasePersistenceImpl implements
		ShippingRatesFinder {
	@SuppressWarnings("unchecked")
	public List<ShippingRates> getShippingSettingRecord(long companyId,
			String countryId, String shippingType, double value) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(GET_SHIPPINGSETTING);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ShippingRates", ShippingRatesImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(companyId);
			qPos.add(countryId);
			qPos.add(shippingType);
			qPos.add(value);

		} catch (ORMException e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);

		}
		return (List<ShippingRates>) query.list();
	}

	@SuppressWarnings("unchecked")
	public List<ShippingRates> findByCompanyId(long companyId) {
		Session session = null;
		SQLQuery query = null;
		try {
			String sql = CustomSQLUtil.get(FINDBY_COMPANYID);
			session = openSession();
			query = session.createSQLQuery(sql);
			query.setCacheable(false);
			query.addEntity("ShippingRates", ShippingRatesImpl.class);
			QueryPos qPos = QueryPos.getInstance(query);
			qPos.add(companyId);

		} catch (ORMException e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return (List<ShippingRates>) query.list();
	}

	public boolean findShippingRanges(long companyId, String countryId,
			long recId, String shippingType, double rangeFrom, double rangeTo) {
		Session session = null;
		SQLQuery query = null;
		try {
			if ((rangeFrom > 0) && (rangeTo > 0)) {
				String sql = CustomSQLUtil.get(FIND_SHIPPINGRANGES);
				session = openSession();
				query = session.createSQLQuery(sql);
				query.setCacheable(false);
				query.addEntity("ShippingRates", ShippingRatesImpl.class);
				QueryPos qPos = QueryPos.getInstance(query);
				qPos.add(companyId);
				qPos.add(countryId);
				qPos.add(shippingType);
				qPos.add(rangeFrom);
				qPos.add(rangeTo);
				qPos.add(recId);
				if (query.list().size() > 0) {
					return true;
				}
			}
		} catch (ORMException e) {
			_log.info(e.getClass());
		} finally {
			closeSession(session);
		}
		return false;
	}

	public static String GET_SHIPPINGSETTING = "getShippingSetting";
	public static String FINDBY_COMPANYID = "findByCompanyId";
	public static String FIND_SHIPPINGRANGES = "findShippingRanges";
	private static Log _log = LogFactoryUtil
			.getLog(ShippingRatesFinderImpl.class);

}