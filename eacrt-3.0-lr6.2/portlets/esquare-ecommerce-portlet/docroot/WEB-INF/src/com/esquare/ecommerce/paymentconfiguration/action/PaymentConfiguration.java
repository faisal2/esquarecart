package com.esquare.ecommerce.paymentconfiguration.action;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.esquare.ecommerce.model.impl.PaymentConfigurationImpl;
import com.esquare.ecommerce.service.PaymentConfigurationLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PaymentAdminConfiguration
 */
public class PaymentConfiguration extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();
		long userId = themeDisplay.getUserId();
		long paypal_configuration_id = ParamUtil.getLong(actionRequest,
				"paypal_configuration_id");
		long other_configuration_id = ParamUtil.getLong(actionRequest,
				"other_configuration_id");
		long cod_configuration_id = ParamUtil.getLong(actionRequest,
				"cod_configuration_id");
		String payPalMerchantId = ParamUtil.getString(actionRequest,
				"payPalMerchantId");
		String payPalSecretKey = ParamUtil.getString(actionRequest,
				"paypalSecretKey");
		String payPalWorkingKey = ParamUtil.getString(actionRequest,
				"certificateId");
		boolean payPalActive = ParamUtil.getBoolean(actionRequest,
				"payPalActive");
		boolean otherActive = ParamUtil
				.getBoolean(actionRequest, "otherActive");
		boolean codActive = ParamUtil.getBoolean(actionRequest, "codActive");
		String otherPaymentType = ParamUtil.getString(actionRequest,
				"otherPaymentType");
		String otherSecretKey = ParamUtil.getString(actionRequest,
				"otherSecretKey");
		String otherWorkingKey = ParamUtil.getString(actionRequest,
				"otherWorkingKey");
		String otherMerchantId = ParamUtil.getString(actionRequest,
				"otherMerchantId");
		if (paypal_configuration_id == 0) {
			addPaymentConfiguration(userId, companyId, EECConstants.PAYPAL,
					payPalMerchantId, payPalSecretKey, payPalActive,payPalWorkingKey);
		} else if (paypal_configuration_id > 0) {
			updatePaymentConfiguration(paypal_configuration_id,
					EECConstants.PAYPAL, payPalMerchantId, payPalSecretKey,
					payPalActive,payPalWorkingKey);
		}
		if (other_configuration_id == 0) {
			addPaymentConfiguration(userId, companyId, otherPaymentType,
					otherMerchantId, otherSecretKey, otherActive,otherWorkingKey);
		} else if (other_configuration_id > 0) {
			updatePaymentConfiguration(other_configuration_id,
					otherPaymentType, otherMerchantId, otherSecretKey,
					otherActive,otherWorkingKey);
		}
		if (cod_configuration_id == 0) {
			addPaymentConfiguration(userId, companyId, EECConstants.COD,
					StringPool.BLANK, StringPool.BLANK, codActive,StringPool.BLANK);
		} else if (cod_configuration_id > 0) {
			updatePaymentConfiguration(cod_configuration_id, EECConstants.COD,
					StringPool.BLANK, StringPool.BLANK, codActive,StringPool.BLANK);
		}
		super.processAction(actionRequest, actionResponse);
	}

	protected void updatePaymentConfiguration(Long id, String paymentType,
			String merchantId, String secretKey, boolean active,String workingKey) {
		try {
			PaymentConfigurationLocalServiceUtil.updatePaymentConfiguration(id,
					paymentType, merchantId, secretKey, active,workingKey);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}

	}

	protected void addPaymentConfiguration(long userId, long companyId,
			String paymentType, String merchantId, String secretKey,
			boolean active,String workingKey) {

		try {
			PaymentConfigurationLocalServiceUtil.addPaymentConfiguration(
					userId, companyId, paymentType, merchantId, secretKey,
					active,workingKey);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(PaymentConfiguration.class);
}