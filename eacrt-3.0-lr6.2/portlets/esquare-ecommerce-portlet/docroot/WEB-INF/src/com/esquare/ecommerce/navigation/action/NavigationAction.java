package com.esquare.ecommerce.navigation.action;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class NavigationAction extends MVCPortlet {
	
	
	public void updateProcess(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		String catalogId = ParamUtil.getString(actionRequest, "catalogId");
		PortletSession psession = actionRequest.getPortletSession();	
		psession.setAttribute("categoryId",catalogId, PortletSession.APPLICATION_SCOPE);
		actionResponse.sendRedirect("/web/guest/detailspage");
		String parentCatalogId = ParamUtil.getString(actionRequest, "parentCatalogId");
		psession.setAttribute("parentId",parentCatalogId, PortletSession.APPLICATION_SCOPE);
		}
	  }