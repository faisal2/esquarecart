/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;
import java.util.List;

import com.esquare.ecommerce.model.OrderRefundTracker;
import com.esquare.ecommerce.service.OrderRefundTrackerLocalServiceUtil;
import com.esquare.ecommerce.service.base.OrderRefundTrackerLocalServiceBaseImpl;
import com.esquare.ecommerce.util.EECConstants;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the order refund tracker local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.OrderRefundTrackerLocalService}
 * interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.OrderRefundTrackerLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.OrderRefundTrackerLocalServiceUtil
 */
public class OrderRefundTrackerLocalServiceImpl extends
		OrderRefundTrackerLocalServiceBaseImpl {
	public OrderRefundTracker updateOrderRefundTracker(long refundTrackerId,
			long companyId, long userId, String referenceNumber,
			double amountClaimed, boolean paymentStatus, String paymentMode,
			String comments, Date expectedDeliveryDate) throws SystemException {
		OrderRefundTracker orderRefundTracker = null;
		if (refundTrackerId <= 0) {
			refundTrackerId = counterLocalService.increment();
			orderRefundTracker = orderRefundTrackerPersistence
					.create(refundTrackerId);
			orderRefundTracker.setCreateDate(new Date());
			myWalletLocalService.updateMyWallet(companyId, userId,
					-amountClaimed);
		} else {
			orderRefundTracker = orderRefundTrackerPersistence
					.fetchByPrimaryKey(refundTrackerId);
			orderRefundTracker.setExpectedDeliveryDate(expectedDeliveryDate);
			orderRefundTracker.setModifiedDate(new Date());
		}
		orderRefundTracker.setPaymentStatus(paymentStatus);
		orderRefundTracker.setCompanyId(companyId);
		orderRefundTracker.setUserId(userId);
		orderRefundTracker.setReferenceNumber(referenceNumber);
		orderRefundTracker.setAmountClaimed(amountClaimed);
		orderRefundTracker.setPaymentMode(paymentMode);
		orderRefundTracker.setComments(comments);

		return orderRefundTrackerPersistence.update(orderRefundTracker);
	}

	public List<OrderRefundTracker> findByCompanyId(long companyId,
			boolean paymentStatus) throws SystemException {
		return orderRefundTrackerPersistence
				.findByC_S(companyId, paymentStatus);
	}

	public List<OrderRefundTracker> findByUserId(long companyId, long userId,
			String paymentMode) {
		List<OrderRefundTracker> orderRefundlist = null;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
					.forClass(OrderRefundTracker.class);
			dynamicQuery
					.add(RestrictionsFactoryUtil.eq("companyId", companyId));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("userId", userId));
			Criterion eqCriterion = RestrictionsFactoryUtil.eq("paymentMode",
					paymentMode);
			if (paymentMode.equals(EECConstants.WALLET)) {
				dynamicQuery.add(RestrictionsFactoryUtil.eq("paymentMode",
						paymentMode));
			} else {
				Criterion notEqCriterion = RestrictionsFactoryUtil
						.not(RestrictionsFactoryUtil.eq("paymentMode",
								EECConstants.WALLET));
				dynamicQuery.add(notEqCriterion);
			}
			orderRefundlist = (List<OrderRefundTracker>) OrderRefundTrackerLocalServiceUtil
					.dynamicQuery(dynamicQuery);
		} catch (SystemException e) {
		}
		return orderRefundlist;
	}

	public List<OrderRefundTracker> findByPaymentMode(long companyId,
			String paymentMode) {
		List<OrderRefundTracker> orderRefundlist = null;
		try {
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
					.forClass(OrderRefundTracker.class);
			dynamicQuery
					.add(RestrictionsFactoryUtil.eq("companyId", companyId));
			dynamicQuery.add(RestrictionsFactoryUtil.eq("paymentStatus",
					Boolean.TRUE));
			Criterion eqCriterion = RestrictionsFactoryUtil.eq("paymentMode",
					paymentMode);
			if (paymentMode.equals(EECConstants.WALLET)) {
				dynamicQuery.add(RestrictionsFactoryUtil.eq("paymentMode",
						paymentMode));
			} else {
				Criterion notEqCriterion = RestrictionsFactoryUtil
						.not(RestrictionsFactoryUtil.eq("paymentMode",
								EECConstants.WALLET));
				dynamicQuery.add(notEqCriterion);
			}
			orderRefundlist = (List<OrderRefundTracker>) OrderRefundTrackerLocalServiceUtil
					.dynamicQuery(dynamicQuery);
		} catch (SystemException e) {
		}
		return orderRefundlist;
	}
}