package com.esquare.ecommerce.coupon.action;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.esquare.ecommerce.CouponCodeException;
import com.esquare.ecommerce.CouponDateException;
import com.esquare.ecommerce.CouponDescriptionException;
import com.esquare.ecommerce.CouponEndDateException;
import com.esquare.ecommerce.CouponNameException;
import com.esquare.ecommerce.CouponStartDateException;
import com.esquare.ecommerce.DuplicateCouponCodeException;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.service.CouponLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.shopping.CouponDiscountException;
import com.liferay.portlet.shopping.CouponLimitCategoriesException;
import com.liferay.portlet.shopping.CouponLimitSKUsException;
import com.liferay.portlet.shopping.CouponMinimumOrderException;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CouponAction
 */
public class CouponAction extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		long couponId = ParamUtil.getLong(actionRequest, "couponId");
		String redirect = ParamUtil.getString(actionRequest, "redirect");
		try {
			if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.UPDATE)) {
				updateCoupon(actionRequest);
			} else if (cmd.equals(EECConstants.DELETE)) {
				deleteCoupons(actionRequest);
			} else if (cmd.equals(EECConstants.DEACTIVATE)) {
				deactivateCoupon(actionRequest);
				actionResponse.setRenderParameter(EECConstants.ACTIVE,
						String.valueOf(Boolean.TRUE));
			} else if (cmd.equals(EECConstants.PUBLISH)) {
				activateCoupon(actionRequest);
				actionResponse.setRenderParameter(EECConstants.ACTIVE,
						String.valueOf(Boolean.FALSE));
			} else if (cmd.equals(EECConstants.SEARCH)) {
				List<Coupon> coupons = searchCoupon(actionRequest,
						actionResponse);
				actionRequest.setAttribute(EECConstants.SEARCH_LIST, coupons);
			}
		} catch (Exception e) {
			_log.info(e.getClass());
			if (e instanceof CouponCodeException
					|| e instanceof CouponDateException
					|| e instanceof CouponDescriptionException
					|| e instanceof CouponDiscountException
					|| e instanceof CouponEndDateException
					|| e instanceof CouponLimitCategoriesException
					|| e instanceof CouponLimitSKUsException
					|| e instanceof CouponMinimumOrderException
					|| e instanceof CouponNameException
					|| e instanceof CouponStartDateException
					|| e instanceof DuplicateCouponCodeException) {
				SessionErrors.add(actionRequest, e.getClass());
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/coupon/add_coupon.jsp");
				if (Validator.isNotNull(couponId))
					actionResponse.setRenderParameter("couponId",
							String.valueOf(couponId));
				if (Validator.isNotNull(redirect))
					actionResponse.setRenderParameter("redirect", redirect);
			}
		}

		super.processAction(actionRequest, actionResponse);
	}

	protected List<Coupon> searchCoupon(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		List<Coupon> coupons = null;
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		if (actionRequest.getRemoteUser() == null) {
			actionResponse.sendRedirect(themeDisplay.getURLSignIn());
		}

		String couponCode = ParamUtil.getString(actionRequest,
				EECConstants.COUPON_CODE);
		String couponName = ParamUtil.getString(actionRequest,
				EECConstants.COUPON_NAME);
		String discountType = ParamUtil.getString(actionRequest,
				EECConstants.DISCOUNT_TYPE);
		boolean isAndOperator = ParamUtil.getBoolean(actionRequest,
				EECConstants.AND_OPERATOR);
		boolean active = ParamUtil.getBoolean(actionRequest,
				EECConstants.ACTIVE);
		try {
			coupons = CouponLocalServiceUtil.searchCoupon(
					themeDisplay.getScopeGroupId(),
					themeDisplay.getCompanyId(), couponCode, couponName,
					discountType, isAndOperator, active);
		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		actionResponse.setRenderParameter(EECConstants.COUPON_CODE, couponCode);
		actionResponse.setRenderParameter(EECConstants.COUPON_NAME, couponName);
		actionResponse.setRenderParameter(EECConstants.DISCOUNT_TYPE,
				discountType);
		actionResponse.setRenderParameter(EECConstants.AND_OPERATOR,
				String.valueOf(isAndOperator));
		actionResponse.setRenderParameter(EECConstants.ACTIVE,
				String.valueOf(active));
		return coupons;

	}

	protected void deleteCoupons(ActionRequest actionRequest) throws Exception {
		long[] deleteCouponIds = StringUtil.split(
				ParamUtil.getString(actionRequest, "deleteCouponIds"), 0L);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		int couponUsedCount = 0;
		for (int i = 0; i < deleteCouponIds.length; i++) {
			Coupon coupon = CouponLocalServiceUtil
					.getCoupon(deleteCouponIds[i]);
			couponUsedCount = OrderLocalServiceUtil.getCouponVersionUsedCount(
					themeDisplay.getCompanyId(), coupon.getCouponCode(),
					coupon.getVersion());
			if (couponUsedCount > 0) {
				coupon.setArchived(true);
				coupon.setActive(false);
				CouponLocalServiceUtil.updateCoupon(coupon);
			} else {
				CouponLocalServiceUtil.deleteCoupon(deleteCouponIds[i]);
			}
		}
	}

	protected void deactivateCoupon(ActionRequest actionRequest)
			throws Exception {
		String couponIdString = ParamUtil.getString(actionRequest, "couponId");
		long couponId = 0l;
		if (Validator.isNotNull(couponIdString)) {
			couponId = Long.parseLong(couponIdString);
		}
		CouponLocalServiceUtil.changeCouponStatus(couponId, Boolean.FALSE);
	}

	protected void activateCoupon(ActionRequest actionRequest) throws Exception {
		String couponIdString = ParamUtil.getString(actionRequest, "couponId");
		long couponId = 0l;
		if (Validator.isNotNull(couponIdString)) {
			couponId = Long.parseLong(couponIdString);
		}
		CouponLocalServiceUtil.changeCouponStatus(couponId, Boolean.TRUE);
	}

	protected void updateCoupon(ActionRequest actionRequest) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long couponId = ParamUtil.getLong(actionRequest, "couponId");
		double discount = 0.0;

		String limitProducts = null;
		String limitCatalogs = null;

		String couponCode = ParamUtil.getString(actionRequest, "couponCode");
		String couponName = ParamUtil.getString(actionRequest, "couponName");
		int couponLimit = ParamUtil.getInteger(actionRequest, "couponLimit");
		int userLimit = ParamUtil.getInteger(actionRequest, "userLimit");
		String description = ParamUtil.getString(actionRequest, "description");
		int startDateMonth = ParamUtil.getInteger(actionRequest,
				"startDateMonth");
		int startDateDay = ParamUtil.getInteger(actionRequest, "startDateDay");
		int startDateYear = ParamUtil
				.getInteger(actionRequest, "startDateYear");
		int startDateHour = ParamUtil
				.getInteger(actionRequest, "startDateHour");
		int startDateMinute = ParamUtil.getInteger(actionRequest,
				"startDateMinute");
		int startDateAmPm = ParamUtil
				.getInteger(actionRequest, "startDateAmPm");

		if (startDateAmPm == Calendar.PM) {
			startDateHour += 12;
		}

		int endDateMonth = ParamUtil.getInteger(actionRequest, "endDateMonth");
		int endDateDay = ParamUtil.getInteger(actionRequest, "endDateDay");
		int endDateYear = ParamUtil.getInteger(actionRequest, "endDateYear");
		int endDateHour = ParamUtil.getInteger(actionRequest, "endDateHour");
		int endDateMinute = ParamUtil
				.getInteger(actionRequest, "endDateMinute");
		int endDateAmPm = ParamUtil.getInteger(actionRequest, "endDateAmPm");
		boolean neverExpire = ParamUtil
				.getBoolean(actionRequest, "neverExpire");

		if (endDateAmPm == Calendar.PM) {
			endDateHour += 12;
		}

		String discountType = ParamUtil
				.getString(actionRequest, "discountType");
		double discountActual = ParamUtil.getDouble(actionRequest,
				"discountActual");
		double discountPercentage = ParamUtil.getDouble(actionRequest,
				"discountPercentage");
		double minOrder = ParamUtil.getDouble(actionRequest, "minOrder");
		String discountLimit = ParamUtil.getString(actionRequest,
				"discountLimit");
		if (discountLimit.equalsIgnoreCase(EECConstants.discountLimit[1])) {
			String[] limitCatalogsArray = ParamUtil.getParameterValues(
					actionRequest, "limitCatalogs");
			limitCatalogs = EECUtil.convertToCommaDelimited(limitCatalogsArray);

		} else if (discountLimit
				.equalsIgnoreCase(EECConstants.discountLimit[2])) {

			String[] limitProductsArray = ParamUtil.getParameterValues(
					actionRequest, "limitProducts");
			limitProducts = EECUtil.convertToCommaDelimited(limitProductsArray);

		}

		boolean active = ParamUtil.getBoolean(actionRequest, "active");

		Date startDate = PortalUtil.getDate(startDateMonth, startDateDay,
				startDateYear, startDateHour, startDateMinute,
				themeDisplay.getTimeZone(), CouponStartDateException.class);
		Date endDate = null;

		if (!neverExpire) {
			endDate = PortalUtil.getDate(endDateMonth, endDateDay, endDateYear,
					endDateHour, endDateMinute, themeDisplay.getTimeZone(),
					CouponEndDateException.class);
		}

		if (discountType.equalsIgnoreCase(EECConstants.DISCOUNT_TYPE_ACTUAL)) {
			discount = discountActual;
		} else if (discountType
				.equalsIgnoreCase(EECConstants.DISCOUNT_TYPE_PERCENTAGE)) {
			discount = discountPercentage;
		} else {
			limitProducts = null;
			limitCatalogs = null;
		}

		if (Validator.isNull(limitCatalogs) && Validator.isNull(limitProducts))
			discountLimit = EECConstants.discountLimit[0];

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
				Coupon.class.getName(), actionRequest);
		if (couponId <= 0) {
			// Add coupon
			CouponLocalServiceUtil.addCoupon(themeDisplay.getUserId(),
					couponCode, couponName, description, startDate, endDate,
					neverExpire, active, couponLimit, userLimit, minOrder,
					discountType, discount, discountLimit, limitCatalogs,
					limitProducts, serviceContext);
		} else {
			// Update coupon
			CouponLocalServiceUtil.updateCoupon(themeDisplay.getUserId(),
					couponId, couponName, description, startDate, endDate,
					neverExpire, active, couponLimit, userLimit, minOrder,
					discountType, discount, discountLimit, limitCatalogs,
					limitProducts, serviceContext);

		}
	}

	private static Log _log = LogFactoryUtil.getLog(CouponAction.class);
}
