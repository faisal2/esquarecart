/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.TaxesSetting;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing TaxesSetting in entity cache.
 *
 * @author Esquare
 * @see TaxesSetting
 * @generated
 */
public class TaxesSettingCacheModel implements CacheModel<TaxesSetting>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{recId=");
		sb.append(recId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", countryId=");
		sb.append(countryId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", taxRate=");
		sb.append(taxRate);
		sb.append(", xml=");
		sb.append(xml);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TaxesSetting toEntityModel() {
		TaxesSettingImpl taxesSettingImpl = new TaxesSettingImpl();

		taxesSettingImpl.setRecId(recId);
		taxesSettingImpl.setCompanyId(companyId);

		if (countryId == null) {
			taxesSettingImpl.setCountryId(StringPool.BLANK);
		}
		else {
			taxesSettingImpl.setCountryId(countryId);
		}

		if (name == null) {
			taxesSettingImpl.setName(StringPool.BLANK);
		}
		else {
			taxesSettingImpl.setName(name);
		}

		taxesSettingImpl.setTaxRate(taxRate);

		if (xml == null) {
			taxesSettingImpl.setXml(StringPool.BLANK);
		}
		else {
			taxesSettingImpl.setXml(xml);
		}

		taxesSettingImpl.resetOriginalValues();

		return taxesSettingImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recId = objectInput.readLong();
		companyId = objectInput.readLong();
		countryId = objectInput.readUTF();
		name = objectInput.readUTF();
		taxRate = objectInput.readDouble();
		xml = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recId);
		objectOutput.writeLong(companyId);

		if (countryId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(countryId);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeDouble(taxRate);

		if (xml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(xml);
		}
	}

	public long recId;
	public long companyId;
	public String countryId;
	public String name;
	public double taxRate;
	public String xml;
}