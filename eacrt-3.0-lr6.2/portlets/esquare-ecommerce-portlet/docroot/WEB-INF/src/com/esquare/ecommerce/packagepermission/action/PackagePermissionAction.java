package com.esquare.ecommerce.packagepermission.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PackagePermissionAction extends MVCPortlet {

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		Long companyId = ParamUtil.getLong(actionRequest, "companyId");
		PortletSession pSession = actionRequest.getPortletSession();
		pSession.setAttribute("SELECTED_INSTANCE_ID", String.valueOf(companyId));

		String totalMonth = ParamUtil.getString(actionRequest, "Month");
		String totalPrice = ParamUtil.getString(actionRequest, "totalPrice");

		String[] permissionKeys = PortletPropsValues.PACKAGE_PERMISSION_KEY;
		String type = StringPool.BLANK;
		int price = 0;
		boolean unlimitedPck = false;
		CartPermission cartPermission = null;
		try {
			for (String permissionKey : permissionKeys) {
				type = ParamUtil.getString(actionRequest, permissionKey
						+ "_type");
				price = ParamUtil.getInteger(actionRequest, permissionKey
						+ "_price");
				unlimitedPck = ParamUtil.getBoolean(actionRequest,
						permissionKey + "_check");
				cartPermission = CartPermissionLocalServiceUtil
						.getCartPermissionCI_PK(companyId, permissionKey);

				if (cartPermission == null) {
					CartPermissionLocalServiceUtil
							.createCartPermission(companyId, permissionKey,
									type, price, unlimitedPck);
				} else {
					cartPermission.setValue(type);
					cartPermission.setPrice(price);
					cartPermission.setUnlimitedPck(unlimitedPck);
					CartPermissionLocalServiceUtil
							.updateCartPermission(cartPermission);
				}
			}
			// for Month update
			cartPermission = CartPermissionLocalServiceUtil
					.getCartPermissionCI_PK(companyId, EECConstants.MONTH);
			if (cartPermission == null) {
				CartPermissionLocalServiceUtil.createCartPermission(companyId,
						EECConstants.MONTH,
						PortletPropsValues.PACKAGE_MONTHS_DURATION, 0, false);
			} else {
				cartPermission.setPrice(Integer.parseInt(totalPrice));
				cartPermission.setValue(totalMonth);
				CartPermissionLocalServiceUtil
						.updateCartPermission(cartPermission);
			}

		} catch (SystemException e) {
			_log.info(e.getClass());
		}

		// actionResponse.setRenderParameter("jspPage",
		// "/html/eec/common/cart/billing_address.jsp");
		// actionResponse.sendRedirect("/html/eec/common/packagepermission/edit.jsp");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String cmd = resourceRequest.getParameter("cmd");
		String query = resourceRequest.getParameter("q");
		String alphabet = ParamUtil.getString(resourceRequest, "alphabet");

		if (Validator.isNotNull(cmd) && Validator.equals(cmd, "addRecords")) {
			loadRecords(resourceRequest, resourceResponse, alphabet);
		} else {
			autoCompleteData(resourceRequest, resourceResponse, query);
		}

	}

	public void loadRecords(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, String alphabet)
			throws IOException, PortletException {

		Company company = null;
		try {
			company = CompanyLocalServiceUtil
					.fetchCompanyByVirtualHost(alphabet);
			PortletSession pSession = resourceRequest.getPortletSession();
			pSession.setAttribute("SELECTED_INSTANCE_ID",
					String.valueOf(company.getCompanyId()));
		} catch (SystemException e) {
			_log.info(e.getClass());
		}

	}

	public void autoCompleteData(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, String query)
			throws IOException, PortletException {
		JSONObject json = JSONFactoryUtil.createJSONObject();
		JSONArray results = JSONFactoryUtil.createJSONArray();
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil
				.forClass(Company.class);
		Criterion criterion = RestrictionsFactoryUtil.like("webId", query
				+ StringPool.PERCENT);
		dynamicQuery.add(criterion);

		List<Company> DetailsList = null;
		try {
			DetailsList = (List<Company>) CompanyLocalServiceUtil
					.dynamicQuery(dynamicQuery);

		} catch (SystemException e) {
			_log.info(e.getClass());
		}
		for (Company webIds : DetailsList) {
			JSONObject listEntry = JSONFactoryUtil.createJSONObject();
			listEntry.put("name", webIds.getWebId());
			results.put(listEntry);
		}

		json.put("response", results);
		PrintWriter writer = resourceResponse.getWriter();
		writer.println(json.toString());

	}

	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(renderRequest, EECConstants.CMD);

		super.doView(renderRequest, renderResponse);
	}

	private static Log _log = LogFactoryUtil
			.getLog(PackagePermissionAction.class);
}
