package com.esquare.ecommerce.util;

import java.math.BigInteger;
import java.security.MessageDigest;

import com.esquare.ecommerce.model.PaymentConfiguration;
import com.esquare.ecommerce.service.PaymentConfigurationLocalServiceUtil;
import com.liferay.portal.kernel.util.Validator;

	
public class EBSUtil {
	public static String md5(String str) throws Exception {
		MessageDigest m = MessageDigest.getInstance("MD5");

		byte[] data = str.getBytes();

		m.update(data, 0, data.length);

		BigInteger i = new BigInteger(1, m.digest());

		String hash = String.format("%1$032X", i);

		return hash;
	}
	public static String getEbsSecretKey(long companyId){
		String secretKey=null;
		PaymentConfiguration paymentConfiguration=PaymentConfigurationLocalServiceUtil.findByPaymentType(companyId, EECConstants.EBS);
	if(Validator.isNotNull(paymentConfiguration)){
		secretKey= paymentConfiguration.getSecretKey();
	}
	else {
		secretKey=PortletPropsValues.EBS_SECRET_KEY;
	}
	return secretKey;		
	}
	public static String getEbsMerchantId(long companyId){
		String secretKey=null;
		PaymentConfiguration paymentConfiguration=PaymentConfigurationLocalServiceUtil.findByPaymentType(companyId, EECConstants.EBS);
	if(Validator.isNotNull(paymentConfiguration)){
		secretKey= paymentConfiguration.getMerchantId();
	}
	else {
		secretKey=PortletPropsValues.EBS_ACCOUNT_ID;
	}
	return secretKey;		
	}
}
