/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.http;

import com.esquare.ecommerce.service.CatalogServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.esquare.ecommerce.service.CatalogServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.esquare.ecommerce.model.CatalogSoap}.
 * If the method in the service utility returns a
 * {@link com.esquare.ecommerce.model.Catalog}, that is translated to a
 * {@link com.esquare.ecommerce.model.CatalogSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Esquare
 * @see CatalogServiceHttp
 * @see com.esquare.ecommerce.model.CatalogSoap
 * @see com.esquare.ecommerce.service.CatalogServiceUtil
 * @generated
 */
public class CatalogServiceSoap {
	public static com.esquare.ecommerce.model.CatalogSoap addCatalog(
		long parentCatalogId, java.lang.String name,
		java.lang.String description, boolean showNavigation, long folderId,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws RemoteException {
		try {
			com.esquare.ecommerce.model.Catalog returnValue = CatalogServiceUtil.addCatalog(parentCatalogId,
					name, description, showNavigation, folderId, serviceContext);

			return com.esquare.ecommerce.model.CatalogSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.esquare.ecommerce.model.CatalogSoap updateCatalog(
		long catalogId, long parentCatalogId, java.lang.String name,
		java.lang.String description, boolean showNavigation, long folderId,
		boolean mergeWithParentCatalog,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws RemoteException {
		try {
			com.esquare.ecommerce.model.Catalog returnValue = CatalogServiceUtil.updateCatalog(catalogId,
					parentCatalogId, name, description, showNavigation,
					folderId, mergeWithParentCatalog, serviceContext);

			return com.esquare.ecommerce.model.CatalogSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static com.esquare.ecommerce.model.CatalogSoap deleteCatalog(
		long catalogId) throws RemoteException {
		try {
			com.esquare.ecommerce.model.Catalog returnValue = CatalogServiceUtil.deleteCatalog(catalogId);

			return com.esquare.ecommerce.model.CatalogSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(CatalogServiceSoap.class);
}