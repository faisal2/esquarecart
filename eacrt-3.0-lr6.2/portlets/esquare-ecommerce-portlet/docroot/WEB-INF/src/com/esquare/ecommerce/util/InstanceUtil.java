package com.esquare.ecommerce.util;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.servlet.ServletContext;

import com.esquare.ecommerce.dashboard.util.DashboardUtil;
import com.esquare.ecommerce.instance.action.AddHost;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.impl.InstanceImpl;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.lar.PortletDataHandlerKeys;
import com.liferay.portal.kernel.lar.UserIdStrategy;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.MethodKey;
import com.liferay.portal.kernel.util.PortalClassInvoker;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.model.ResourcePermission;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.security.permission.ResourceActionsUtil;
import com.liferay.portal.service.CompanyServiceUtil;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;

public class InstanceUtil {

	public static String VIRTUAL_HOST = PortletPropsValues.INSTANCE_VIRTUAL_HOST;

	public static String createInstance(ActionRequest actionRequest,
			String webId, String emailAddress, String password,
			String packageType) {

		String mx = PortletPropsValues.DEFAULT_INSTANCE_MAIL_DOMAIN;
		int maxUsers = Integer.parseInt(DashboardUtil.getPackageValue(
				packageType, EECConstants.MAX_USER))
				+ EECConstants.DEFAULT_USER_COUNT;

		String virtualHost = webId.replace(VIRTUAL_HOST, StringPool.BLANK)
				.replaceAll("\\.", "-") + "." + EECConstants.ECART_WEBID;

		/*
		 * if (packageType.equals(PortletPropsValues.PACKAGE_TYPE_FREE)) {
		 * virtualHost = webId.replace(VIRTUAL_HOST, StringPool.BLANK)
		 * .replaceAll("\\.", "-") + "." + EECConstants.ECART_WEBID; } else {
		 * virtualHost = webId; }
		 */

		try {
			File basicB2CLar = new File(
					PortletPropsValues.DEFAULT_TEMPLATE_LAR_FOLDER_PATH
							+ PortletPropsValues.BASIC_B2C_TEMPLATE_LAR_NAME);
			File basicB2BLar = new File(
					PortletPropsValues.DEFAULT_TEMPLATE_LAR_FOLDER_PATH
							+ PortletPropsValues.BASIC_B2B_TEMPLATE_LAR_NAME);

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
					.getAttribute(WebKeys.THEME_DISPLAY);

			Company company = null;

			try {
				company = CompanyServiceUtil.addCompany(virtualHost,
						virtualHost, mx, PropsKeys.SHARD_DEFAULT_NAME, false,
						maxUsers, true);

				ServletContext servletContext = (ServletContext) actionRequest
						.getAttribute(WebKeys.CTX);

				// Calling external method PortalInstances.initCompany() for
				// initializing company

				ClassLoader pcl = PortalClassLoaderUtil.getClassLoader();
				Class lClass = pcl
						.loadClass("com.liferay.portal.util.PortalInstances");
				MethodKey key = new MethodKey(lClass, "initCompany",
						ServletContext.class, String.class);
				PortalClassInvoker.invoke(false, key, new Object[] {
						servletContext, company.getWebId() });
			} catch (Exception e) {
				_log.info(e.getClass());
			}

			// End

			if (PortletPropsValues.CURRENT_WORKING_ENVIRONMENT
					.equals("Production")) {
				DNSSettings.createDomain(virtualHost);
			} else {
				AddHost addhost = new AddHost();
				addhost.addAHost(virtualHost);
			}

			User instanceAdmin = UserLocalServiceUtil.getUserByEmailAddress(
					company.getCompanyId(),
					(PropsUtil
							.get(PropsKeys.DEFAULT_ADMIN_EMAIL_ADDRESS_PREFIX)
							+ "@" + company.getMx()));

			ServiceContext serviceContext = ServiceContextFactory.getInstance(
					User.class.getName(), actionRequest);
			// Group group = GroupLocalServiceUtil.addGroup(
			// instanceAdmin.getUserId(), null, 0l,
			// EECConstants.SHOP_ADMINNAME, EECConstants.SHOP_ADMINNAME,
			// com.liferay.portal.model.GroupConstants.TYPE_SITE_PRIVATE,
			// null, true, true, serviceContext);

			Group group = GroupLocalServiceUtil.addGroup(
					instanceAdmin.getUserId(),
					GroupConstants.DEFAULT_PARENT_GROUP_ID, null, 0l,
					GroupConstants.DEFAULT_LIVE_GROUP_ID,
					EECConstants.SHOP_ADMINNAME, EECConstants.SHOP_ADMINNAME,
					com.liferay.portal.model.GroupConstants.TYPE_SITE_PRIVATE,
					true, GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION, null,
					true, true, serviceContext);

			Group guestGroup = GroupLocalServiceUtil.getGroup(
					company.getCompanyId(), EECConstants.GUEST);

			User user = UserServiceUtil.addUserWithWorkflow(
					company.getCompanyId(), false, password, password, true,
					null, emailAddress, 0l, StringPool.BLANK,
					themeDisplay.getLocale(),
					PortletPropsValues.DEFAULT_SHOP_ADMIN_FIRSTNAME,
					StringPool.BLANK,
					PortletPropsValues.DEFAULT_SHOP_ADMIN_LASTTNAME, 0, 0,
					true, 01, 01, 1970, StringPool.BLANK, null, null, null,
					null, false, serviceContext);
			Role role = createShopAdminRole(instanceAdmin.getUserId(),
					company.getCompanyId());

			// Assigning Role Permission
			long[] siteUserId = new long[] { user.getUserId() };
			long[] siteRoleId = new long[] { role.getRoleId() };

			UserLocalServiceUtil.addGroupUsers(group.getGroupId(), siteUserId);
			RoleLocalServiceUtil.addUserRoles(user.getUserId(), siteRoleId);

			addDefaultLayoutsByLAR(instanceAdmin.getUserId(),
					guestGroup.getGroupId(), false, basicB2CLar, serviceContext);
			addDefaultLayoutsByLAR(instanceAdmin.getUserId(),
					group.getGroupId(), true, basicB2BLar, serviceContext);

			// Adding Instance Details in Custom Instance Table
			Instance instance = new InstanceImpl();
			long companyId = company.getCompanyId();
			long instanceUserId = user.getUserId();
			instance.setCompanyId(companyId);
			instance.setInstanceUserId(instanceUserId);
			instance.setCreatedDate(new Date());

			Date expDate = new Date();
			Calendar c = Calendar.getInstance();
			c.setTime(expDate);
			c.add(Calendar.DATE, 15);
			expDate = c.getTime();
			instance.setExpiryDate(expDate);
			instance.setPackageType(packageType);
			instance.setCurrentTemplate(PortletPropsValues.DEFAULT_B2C_TEMPLATE_NAME);
			InstanceLocalServiceUtil.addInstance(instance);
			// End

			// calling Add cartPermission
			cartPermission(companyId, packageType);

			// Adding basic role permission for new instance
			Role role1 = RoleLocalServiceUtil.getRole(
					themeDisplay.getCompanyId(), packageType);
			List<ResourcePermission> resourcePermissionList = ResourcePermissionLocalServiceUtil
					.getRoleResourcePermissions(role1.getRoleId());
			for (int i = 0; i < resourcePermissionList.size(); i++) {
				ResourcePermission resourcePermission = resourcePermissionList
						.get(i);

				String primKey = resourcePermission.getPrimKey();
				int scope = resourcePermission.getScope();
				if (primKey.equals(String.valueOf(themeDisplay.getCompanyId()))) {
					primKey = String.valueOf(company.getCompanyId());
				}

				List<String> actions = ResourceActionsUtil.getResourceActions(
						null, resourcePermission.getName());
				for (String actionId : actions) {
					ResourcePermissionLocalServiceUtil.addResourcePermission(
							company.getCompanyId(),
							resourcePermission.getName(), scope, primKey,
							role.getRoleId(), actionId);
				}
			}
			// End

			// add file view permission for guest
			ResourcePermissionLocalServiceUtil.setResourcePermissions(
					companyId,
					DLFileEntry.class.getName(),
					ResourceConstants.SCOPE_COMPANY,
					String.valueOf(companyId),
					RoleLocalServiceUtil.getRole(companyId,
							GroupConstants.GUEST).getRoleId(),
					new String[] { ActionKeys.VIEW });
			ResourcePermissionLocalServiceUtil.setResourcePermissions(
					companyId,
					"com.liferay.portlet.documentlibrary",
					ResourceConstants.SCOPE_COMPANY,
					String.valueOf(companyId),
					RoleLocalServiceUtil.getRole(companyId,
							GroupConstants.GUEST).getRoleId(),
					new String[] { ActionKeys.VIEW });

			// send mail
			sendMail(user, companyId, themeDisplay.getScopeGroupId(),
					virtualHost, password);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return virtualHost;
	}

	public static Role createShopAdminRole(long userId, long companyId)
			throws SystemException, PortalException {

		Map<Locale, String> titleMap = LocalizationUtil
				.getLocalizationMap(PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE);

		String description = PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE;

		Map<Locale, String> descriptionMap = LocalizationUtil
				.getLocalizationMap(description);

		int type = Integer
				.valueOf(PortletPropsValues.LIFERAY_REGULAR_ROLE_TYPE);
		Role role = null;

		/*
		 * role = RoleLocalServiceUtil.addRole(userId, companyId,
		 * PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE, titleMap, descriptionMap,
		 * type);
		 */
		role = RoleLocalServiceUtil.addRole(userId, null, 0,
				PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE, titleMap,
				descriptionMap, type, null, null);
		return role;
	}

	protected static void addDefaultLayoutsByLAR(long userId, long groupId,
			boolean privateLayout, File larFile, ServiceContext serviceContext) {

		try {

			LayoutLocalServiceUtil.importLayouts(userId, groupId,
					privateLayout, getLayoutSetPrototypesParameters(true),
					larFile);
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (PortalException e) {
			_log.info(e.getClass());
		}
	}

	public static void cartPermission(long companyId, String packageType)
			throws SystemException {
		String[] permissionKeys = PortletPropsValues.PACKAGE_PERMISSION_KEY;
		String[] packageValue = DashboardUtil.getPackageValue(packageType);
		for (int i = 0; i < permissionKeys.length; i++) {
			CartPermissionLocalServiceUtil.createCartPermission(companyId,
					permissionKeys[i], packageValue[i], 0, false);
		}

		CartPermissionLocalServiceUtil.createCartPermission(companyId,
				EECConstants.MONTH, PortletPropsValues.PACKAGE_MONTHS_DURATION,
				0, false);
	}

	public static void sendMail(User user, long companyId, long groupId,
			String virtualHost, String password) throws PortalException,
			SystemException {
		String groupFriendlyUrl = GroupLocalServiceUtil.getGroup(companyId,
				EECConstants.SHOP_ADMINNAME).getFriendlyURL();
		String[] oldSubsbody = { "[$STORE_ADMIN_PANEL_URL$]",
				"[$STORE_ADMIN_EMAIL$]", "[$STORE_ADMIN_PASSWORD$]" };
		String[] newSubsbody = {
				PortletPropsValues.CURRENT_WORKING_ENVIRONMENT
						.equals("Production") ? virtualHost + "/group"
						+ groupFriendlyUrl : virtualHost + ":8080/group"
						+ groupFriendlyUrl, user.getEmailAddress(), password };

		try {
			String body = EECUtil
					.getEsquareCartMailContent(
							EECConstants.DEFAULT_COMPANY_ID,
							PortletPropsValues.INSTANCE_CREATE_CONFIRMATION_EMAIL_BODY_NAME,
							PortletPropsValues.INSTANCE_CREATE_DEFAULT_CONFIRMATION_EMAIL_BODY);
			String subject = EECUtil
					.getEsquareCartMailContent(
							EECConstants.DEFAULT_COMPANY_ID,
							PortletPropsValues.INSTANCE_CREATE_CONFIRMATION_EMAIL_SUBJECT_NAME,
							PortletPropsValues.INSTANCE_CREATE_DEFAULT_CONFIRMATION_EMAIL_SUBJECT);
			body = StringUtil.replace(body, oldSubsbody, newSubsbody);
			InternetAddress from = new InternetAddress(
					PortletPropsValues.ADMIN_EMAIL_FROM_ADDRESS,
					PortletPropsValues.ADMIN_EMAIL_FROM_NAME);
			InternetAddress to = new InternetAddress(user.getEmailAddress(),
					"UN");
			MailMessage message = new MailMessage(from, to, subject, body, true);
			MailServiceUtil.sendEmail(message);
		} catch (Exception e) {
			_log.info(e.getClass());

		}

	}

	protected static Map<String, String[]> getLayoutSetPrototypesParameters(
			boolean importData) {

		Map<String, String[]> parameterMap = new LinkedHashMap<String, String[]>();

		parameterMap.put(PortletDataHandlerKeys.CATEGORIES,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.DELETE_MISSING_LAYOUTS,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.DELETE_PORTLET_DATA,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.IGNORE_LAST_PUBLISH_DATE,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.LAYOUT_SET_SETTINGS,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(
				PortletDataHandlerKeys.LAYOUT_SET_PROTOTYPE_LINK_ENABLED,
				new String[] { Boolean.TRUE.toString() });
		// parameterMap.put(
		// PortletDataHandlerKeys.LAYOUTS_IMPORT_MODE,
		// new String[] {
		// PortletDataHandlerKeys.
		// LAYOUTS_IMPORT_MODE_CREATED_FROM_PROTOTYPE
		// });
		parameterMap.put(PortletDataHandlerKeys.PERMISSIONS,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_CONFIGURATION,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_CONFIGURATION_ALL,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.PORTLET_SETUP_ALL,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.THEME_REFERENCE,
				new String[] { Boolean.TRUE.toString() });
		parameterMap.put(PortletDataHandlerKeys.UPDATE_LAST_PUBLISH_DATE,
				new String[] { Boolean.FALSE.toString() });
		parameterMap.put(PortletDataHandlerKeys.USER_ID_STRATEGY,
				new String[] { UserIdStrategy.CURRENT_USER_ID });

		if (importData) {
			parameterMap
					.put(PortletDataHandlerKeys.DATA_STRATEGY,
							new String[] { PortletDataHandlerKeys.DATA_STRATEGY_MIRROR });
			parameterMap.put(PortletDataHandlerKeys.LOGO,
					new String[] { Boolean.TRUE.toString() });
			parameterMap.put(PortletDataHandlerKeys.PORTLET_DATA,
					new String[] { Boolean.TRUE.toString() });
			parameterMap.put(PortletDataHandlerKeys.PORTLET_DATA_ALL,
					new String[] { Boolean.TRUE.toString() });
		}
		return parameterMap;
	}

	private static Log _log = LogFactoryUtil.getLog(InstanceUtil.class);
}