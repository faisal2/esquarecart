/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchMyWalletException;
import com.esquare.ecommerce.model.MyWallet;
import com.esquare.ecommerce.model.impl.MyWalletImpl;
import com.esquare.ecommerce.model.impl.MyWalletModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the my wallet service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see MyWalletPersistence
 * @see MyWalletUtil
 * @generated
 */
public class MyWalletPersistenceImpl extends BasePersistenceImpl<MyWallet>
	implements MyWalletPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link MyWalletUtil} to access the my wallet persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = MyWalletImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletModelImpl.FINDER_CACHE_ENABLED, MyWalletImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletModelImpl.FINDER_CACHE_ENABLED, MyWalletImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public MyWalletPersistenceImpl() {
		setModelClass(MyWallet.class);
	}

	/**
	 * Caches the my wallet in the entity cache if it is enabled.
	 *
	 * @param myWallet the my wallet
	 */
	@Override
	public void cacheResult(MyWallet myWallet) {
		EntityCacheUtil.putResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletImpl.class, myWallet.getPrimaryKey(), myWallet);

		myWallet.resetOriginalValues();
	}

	/**
	 * Caches the my wallets in the entity cache if it is enabled.
	 *
	 * @param myWallets the my wallets
	 */
	@Override
	public void cacheResult(List<MyWallet> myWallets) {
		for (MyWallet myWallet : myWallets) {
			if (EntityCacheUtil.getResult(
						MyWalletModelImpl.ENTITY_CACHE_ENABLED,
						MyWalletImpl.class, myWallet.getPrimaryKey()) == null) {
				cacheResult(myWallet);
			}
			else {
				myWallet.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all my wallets.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(MyWalletImpl.class.getName());
		}

		EntityCacheUtil.clearCache(MyWalletImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the my wallet.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(MyWallet myWallet) {
		EntityCacheUtil.removeResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletImpl.class, myWallet.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<MyWallet> myWallets) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (MyWallet myWallet : myWallets) {
			EntityCacheUtil.removeResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
				MyWalletImpl.class, myWallet.getPrimaryKey());
		}
	}

	/**
	 * Creates a new my wallet with the primary key. Does not add the my wallet to the database.
	 *
	 * @param userId the primary key for the new my wallet
	 * @return the new my wallet
	 */
	@Override
	public MyWallet create(long userId) {
		MyWallet myWallet = new MyWalletImpl();

		myWallet.setNew(true);
		myWallet.setPrimaryKey(userId);

		return myWallet;
	}

	/**
	 * Removes the my wallet with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param userId the primary key of the my wallet
	 * @return the my wallet that was removed
	 * @throws com.esquare.ecommerce.NoSuchMyWalletException if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet remove(long userId)
		throws NoSuchMyWalletException, SystemException {
		return remove((Serializable)userId);
	}

	/**
	 * Removes the my wallet with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the my wallet
	 * @return the my wallet that was removed
	 * @throws com.esquare.ecommerce.NoSuchMyWalletException if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet remove(Serializable primaryKey)
		throws NoSuchMyWalletException, SystemException {
		Session session = null;

		try {
			session = openSession();

			MyWallet myWallet = (MyWallet)session.get(MyWalletImpl.class,
					primaryKey);

			if (myWallet == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchMyWalletException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(myWallet);
		}
		catch (NoSuchMyWalletException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected MyWallet removeImpl(MyWallet myWallet) throws SystemException {
		myWallet = toUnwrappedModel(myWallet);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(myWallet)) {
				myWallet = (MyWallet)session.get(MyWalletImpl.class,
						myWallet.getPrimaryKeyObj());
			}

			if (myWallet != null) {
				session.delete(myWallet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (myWallet != null) {
			clearCache(myWallet);
		}

		return myWallet;
	}

	@Override
	public MyWallet updateImpl(com.esquare.ecommerce.model.MyWallet myWallet)
		throws SystemException {
		myWallet = toUnwrappedModel(myWallet);

		boolean isNew = myWallet.isNew();

		Session session = null;

		try {
			session = openSession();

			if (myWallet.isNew()) {
				session.save(myWallet);

				myWallet.setNew(false);
			}
			else {
				session.merge(myWallet);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
			MyWalletImpl.class, myWallet.getPrimaryKey(), myWallet);

		return myWallet;
	}

	protected MyWallet toUnwrappedModel(MyWallet myWallet) {
		if (myWallet instanceof MyWalletImpl) {
			return myWallet;
		}

		MyWalletImpl myWalletImpl = new MyWalletImpl();

		myWalletImpl.setNew(myWallet.isNew());
		myWalletImpl.setPrimaryKey(myWallet.getPrimaryKey());

		myWalletImpl.setUserId(myWallet.getUserId());
		myWalletImpl.setCompanyId(myWallet.getCompanyId());
		myWalletImpl.setAmount(myWallet.getAmount());

		return myWalletImpl;
	}

	/**
	 * Returns the my wallet with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the my wallet
	 * @return the my wallet
	 * @throws com.esquare.ecommerce.NoSuchMyWalletException if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet findByPrimaryKey(Serializable primaryKey)
		throws NoSuchMyWalletException, SystemException {
		MyWallet myWallet = fetchByPrimaryKey(primaryKey);

		if (myWallet == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchMyWalletException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return myWallet;
	}

	/**
	 * Returns the my wallet with the primary key or throws a {@link com.esquare.ecommerce.NoSuchMyWalletException} if it could not be found.
	 *
	 * @param userId the primary key of the my wallet
	 * @return the my wallet
	 * @throws com.esquare.ecommerce.NoSuchMyWalletException if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet findByPrimaryKey(long userId)
		throws NoSuchMyWalletException, SystemException {
		return findByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns the my wallet with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the my wallet
	 * @return the my wallet, or <code>null</code> if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		MyWallet myWallet = (MyWallet)EntityCacheUtil.getResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
				MyWalletImpl.class, primaryKey);

		if (myWallet == _nullMyWallet) {
			return null;
		}

		if (myWallet == null) {
			Session session = null;

			try {
				session = openSession();

				myWallet = (MyWallet)session.get(MyWalletImpl.class, primaryKey);

				if (myWallet != null) {
					cacheResult(myWallet);
				}
				else {
					EntityCacheUtil.putResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
						MyWalletImpl.class, primaryKey, _nullMyWallet);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(MyWalletModelImpl.ENTITY_CACHE_ENABLED,
					MyWalletImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return myWallet;
	}

	/**
	 * Returns the my wallet with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param userId the primary key of the my wallet
	 * @return the my wallet, or <code>null</code> if a my wallet with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public MyWallet fetchByPrimaryKey(long userId) throws SystemException {
		return fetchByPrimaryKey((Serializable)userId);
	}

	/**
	 * Returns all the my wallets.
	 *
	 * @return the my wallets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MyWallet> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the my wallets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.MyWalletModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of my wallets
	 * @param end the upper bound of the range of my wallets (not inclusive)
	 * @return the range of my wallets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MyWallet> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the my wallets.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.MyWalletModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of my wallets
	 * @param end the upper bound of the range of my wallets (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of my wallets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<MyWallet> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<MyWallet> list = (List<MyWallet>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_MYWALLET);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_MYWALLET;

				if (pagination) {
					sql = sql.concat(MyWalletModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<MyWallet>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<MyWallet>(list);
				}
				else {
					list = (List<MyWallet>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the my wallets from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (MyWallet myWallet : findAll()) {
			remove(myWallet);
		}
	}

	/**
	 * Returns the number of my wallets.
	 *
	 * @return the number of my wallets
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_MYWALLET);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the my wallet persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.MyWallet")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<MyWallet>> listenersList = new ArrayList<ModelListener<MyWallet>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<MyWallet>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(MyWalletImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_MYWALLET = "SELECT myWallet FROM MyWallet myWallet";
	private static final String _SQL_COUNT_MYWALLET = "SELECT COUNT(myWallet) FROM MyWallet myWallet";
	private static final String _ORDER_BY_ENTITY_ALIAS = "myWallet.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MyWallet exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(MyWalletPersistenceImpl.class);
	private static MyWallet _nullMyWallet = new MyWalletImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<MyWallet> toCacheModel() {
				return _nullMyWalletCacheModel;
			}
		};

	private static CacheModel<MyWallet> _nullMyWalletCacheModel = new CacheModel<MyWallet>() {
			@Override
			public MyWallet toEntityModel() {
				return _nullMyWallet;
			}
		};
}