/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.List;

import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.service.base.ProductDetailsLocalServiceBaseImpl;
import com.esquare.ecommerce.service.persistence.ProductDetailsFinderUtil;
import com.esquare.ecommerce.service.persistence.ProductDetailsUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the product details local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.esquare.ecommerce.service.ProductDetailsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.ProductDetailsLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil
 */
public class ProductDetailsLocalServiceImpl extends
		ProductDetailsLocalServiceBaseImpl {
	public List<ProductDetails> getProductDetails(long companyId,
			boolean visibility, int start, int end) throws SystemException {
		return ProductDetailsUtil.findByProductDetailsList(companyId,
				visibility, start, end);
	}

	public List<ProductDetails> getProductDetails(long companyId, int start,
			int end) throws SystemException {
		return ProductDetailsUtil.findByCompanyId(companyId, start, end);
	}
	
	public List<ProductDetails> getProductDetailList(long companyId) throws SystemException {
		return ProductDetailsUtil.findByCompanyId(companyId);
	}
	
	public List<ProductDetails> getProductDetails(long companyId, long catalogId,boolean visibility)
			throws SystemException {

			return productDetailsPersistence.findByC_C_V(companyId, catalogId,visibility);
		}
	public ProductDetails getProduct(long productDeatailsId)
			throws PortalException, SystemException {

			return productDetailsPersistence.findByPrimaryKey(productDeatailsId);
		}

	
	public List<ProductDetails> getProductDetailsList(long companyId, long catalogId)
			throws SystemException {

			return productDetailsPersistence.findByC_C(companyId, catalogId);
		}
	public ProductDetails fetchByName(long companyId,String name)
			throws SystemException {

			return productDetailsPersistence.fetchByName(companyId,name);
		}
	
	public List<ProductDetails> getOutofStockLessStock(long companyId,int quantity)
			throws SystemException	{
		return ProductDetailsFinderUtil.getOutofStockLessStock(companyId,quantity);
	}
}