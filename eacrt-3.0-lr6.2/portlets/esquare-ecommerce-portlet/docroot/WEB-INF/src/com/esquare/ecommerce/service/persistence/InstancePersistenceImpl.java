/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.persistence;

import com.esquare.ecommerce.NoSuchInstanceException;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.impl.InstanceImpl;
import com.esquare.ecommerce.model.impl.InstanceModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the instance service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Esquare
 * @see InstancePersistence
 * @see InstanceUtil
 * @generated
 */
public class InstancePersistenceImpl extends BasePersistenceImpl<Instance>
	implements InstancePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link InstanceUtil} to access the instance persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InstanceImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceModelImpl.FINDER_CACHE_ENABLED, InstanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceModelImpl.FINDER_CACHE_ENABLED, InstanceImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public InstancePersistenceImpl() {
		setModelClass(Instance.class);
	}

	/**
	 * Caches the instance in the entity cache if it is enabled.
	 *
	 * @param instance the instance
	 */
	@Override
	public void cacheResult(Instance instance) {
		EntityCacheUtil.putResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceImpl.class, instance.getPrimaryKey(), instance);

		instance.resetOriginalValues();
	}

	/**
	 * Caches the instances in the entity cache if it is enabled.
	 *
	 * @param instances the instances
	 */
	@Override
	public void cacheResult(List<Instance> instances) {
		for (Instance instance : instances) {
			if (EntityCacheUtil.getResult(
						InstanceModelImpl.ENTITY_CACHE_ENABLED,
						InstanceImpl.class, instance.getPrimaryKey()) == null) {
				cacheResult(instance);
			}
			else {
				instance.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all instances.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(InstanceImpl.class.getName());
		}

		EntityCacheUtil.clearCache(InstanceImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the instance.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Instance instance) {
		EntityCacheUtil.removeResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceImpl.class, instance.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Instance> instances) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Instance instance : instances) {
			EntityCacheUtil.removeResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
				InstanceImpl.class, instance.getPrimaryKey());
		}
	}

	/**
	 * Creates a new instance with the primary key. Does not add the instance to the database.
	 *
	 * @param companyId the primary key for the new instance
	 * @return the new instance
	 */
	@Override
	public Instance create(long companyId) {
		Instance instance = new InstanceImpl();

		instance.setNew(true);
		instance.setPrimaryKey(companyId);

		return instance;
	}

	/**
	 * Removes the instance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param companyId the primary key of the instance
	 * @return the instance that was removed
	 * @throws com.esquare.ecommerce.NoSuchInstanceException if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance remove(long companyId)
		throws NoSuchInstanceException, SystemException {
		return remove((Serializable)companyId);
	}

	/**
	 * Removes the instance with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the instance
	 * @return the instance that was removed
	 * @throws com.esquare.ecommerce.NoSuchInstanceException if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance remove(Serializable primaryKey)
		throws NoSuchInstanceException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Instance instance = (Instance)session.get(InstanceImpl.class,
					primaryKey);

			if (instance == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInstanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(instance);
		}
		catch (NoSuchInstanceException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Instance removeImpl(Instance instance) throws SystemException {
		instance = toUnwrappedModel(instance);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(instance)) {
				instance = (Instance)session.get(InstanceImpl.class,
						instance.getPrimaryKeyObj());
			}

			if (instance != null) {
				session.delete(instance);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (instance != null) {
			clearCache(instance);
		}

		return instance;
	}

	@Override
	public Instance updateImpl(com.esquare.ecommerce.model.Instance instance)
		throws SystemException {
		instance = toUnwrappedModel(instance);

		boolean isNew = instance.isNew();

		Session session = null;

		try {
			session = openSession();

			if (instance.isNew()) {
				session.save(instance);

				instance.setNew(false);
			}
			else {
				session.merge(instance);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
			InstanceImpl.class, instance.getPrimaryKey(), instance);

		return instance;
	}

	protected Instance toUnwrappedModel(Instance instance) {
		if (instance instanceof InstanceImpl) {
			return instance;
		}

		InstanceImpl instanceImpl = new InstanceImpl();

		instanceImpl.setNew(instance.isNew());
		instanceImpl.setPrimaryKey(instance.getPrimaryKey());

		instanceImpl.setCompanyId(instance.getCompanyId());
		instanceImpl.setInstanceUserId(instance.getInstanceUserId());
		instanceImpl.setCreatedDate(instance.getCreatedDate());
		instanceImpl.setExpiryDate(instance.getExpiryDate());
		instanceImpl.setPackageType(instance.getPackageType());
		instanceImpl.setCurrentTemplate(instance.getCurrentTemplate());

		return instanceImpl;
	}

	/**
	 * Returns the instance with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the instance
	 * @return the instance
	 * @throws com.esquare.ecommerce.NoSuchInstanceException if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInstanceException, SystemException {
		Instance instance = fetchByPrimaryKey(primaryKey);

		if (instance == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInstanceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return instance;
	}

	/**
	 * Returns the instance with the primary key or throws a {@link com.esquare.ecommerce.NoSuchInstanceException} if it could not be found.
	 *
	 * @param companyId the primary key of the instance
	 * @return the instance
	 * @throws com.esquare.ecommerce.NoSuchInstanceException if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance findByPrimaryKey(long companyId)
		throws NoSuchInstanceException, SystemException {
		return findByPrimaryKey((Serializable)companyId);
	}

	/**
	 * Returns the instance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the instance
	 * @return the instance, or <code>null</code> if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		Instance instance = (Instance)EntityCacheUtil.getResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
				InstanceImpl.class, primaryKey);

		if (instance == _nullInstance) {
			return null;
		}

		if (instance == null) {
			Session session = null;

			try {
				session = openSession();

				instance = (Instance)session.get(InstanceImpl.class, primaryKey);

				if (instance != null) {
					cacheResult(instance);
				}
				else {
					EntityCacheUtil.putResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
						InstanceImpl.class, primaryKey, _nullInstance);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(InstanceModelImpl.ENTITY_CACHE_ENABLED,
					InstanceImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return instance;
	}

	/**
	 * Returns the instance with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param companyId the primary key of the instance
	 * @return the instance, or <code>null</code> if a instance with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Instance fetchByPrimaryKey(long companyId) throws SystemException {
		return fetchByPrimaryKey((Serializable)companyId);
	}

	/**
	 * Returns all the instances.
	 *
	 * @return the instances
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Instance> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the instances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.InstanceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of instances
	 * @param end the upper bound of the range of instances (not inclusive)
	 * @return the range of instances
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Instance> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the instances.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.esquare.ecommerce.model.impl.InstanceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of instances
	 * @param end the upper bound of the range of instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of instances
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<Instance> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Instance> list = (List<Instance>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_INSTANCE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INSTANCE;

				if (pagination) {
					sql = sql.concat(InstanceModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Instance>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<Instance>(list);
				}
				else {
					list = (List<Instance>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the instances from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (Instance instance : findAll()) {
			remove(instance);
		}
	}

	/**
	 * Returns the number of instances.
	 *
	 * @return the number of instances
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INSTANCE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the instance persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.esquare.ecommerce.model.Instance")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Instance>> listenersList = new ArrayList<ModelListener<Instance>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Instance>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(InstanceImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_INSTANCE = "SELECT instance FROM Instance instance";
	private static final String _SQL_COUNT_INSTANCE = "SELECT COUNT(instance) FROM Instance instance";
	private static final String _ORDER_BY_ENTITY_ALIAS = "instance.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Instance exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(InstancePersistenceImpl.class);
	private static Instance _nullInstance = new InstanceImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Instance> toCacheModel() {
				return _nullInstanceCacheModel;
			}
		};

	private static CacheModel<Instance> _nullInstanceCacheModel = new CacheModel<Instance>() {
			@Override
			public Instance toEntityModel() {
				return _nullInstance;
			}
		};
}