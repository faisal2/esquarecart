/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.CancelledOrderItems;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CancelledOrderItems in entity cache.
 *
 * @author Esquare
 * @see CancelledOrderItems
 * @generated
 */
public class CancelledOrderItemsCacheModel implements CacheModel<CancelledOrderItems>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{orderItemId=");
		sb.append(orderItemId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", orderId=");
		sb.append(orderId);
		sb.append(", price=");
		sb.append(price);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", cancelledDate=");
		sb.append(cancelledDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CancelledOrderItems toEntityModel() {
		CancelledOrderItemsImpl cancelledOrderItemsImpl = new CancelledOrderItemsImpl();

		cancelledOrderItemsImpl.setOrderItemId(orderItemId);
		cancelledOrderItemsImpl.setCompanyId(companyId);
		cancelledOrderItemsImpl.setUserId(userId);
		cancelledOrderItemsImpl.setOrderId(orderId);
		cancelledOrderItemsImpl.setPrice(price);
		cancelledOrderItemsImpl.setQuantity(quantity);

		if (cancelledDate == Long.MIN_VALUE) {
			cancelledOrderItemsImpl.setCancelledDate(null);
		}
		else {
			cancelledOrderItemsImpl.setCancelledDate(new Date(cancelledDate));
		}

		cancelledOrderItemsImpl.resetOriginalValues();

		return cancelledOrderItemsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		orderItemId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		orderId = objectInput.readLong();
		price = objectInput.readDouble();
		quantity = objectInput.readInt();
		cancelledDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(orderItemId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(orderId);
		objectOutput.writeDouble(price);
		objectOutput.writeInt(quantity);
		objectOutput.writeLong(cancelledDate);
	}

	public long orderItemId;
	public long companyId;
	public long userId;
	public long orderId;
	public double price;
	public int quantity;
	public long cancelledDate;
}