package com.esquare.ecommerce.packagebilling.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.EcartPackageBilling;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.EcartEbsPaymentLocalServiceUtil;
import com.esquare.ecommerce.service.EcartPackageBillingLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.SubscriptionSender;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PackageBillingAction extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);

		if (cmd.equalsIgnoreCase(EECConstants.EDIT)) {
			updatePaymentUserDetail(actionRequest);
		} else if (cmd.equalsIgnoreCase(EECConstants.EBS_RESPONSE)) {
			completePackageBilling(actionRequest, actionResponse);

		}
		super.processAction(actionRequest, actionResponse);
	}

	protected void completePackageBilling(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String[] tabName = PortletPropsValues.PACKAGE_BILLING_TABS.split(",");
		String country = null;
		String state = null;

		long companyId = themeDisplay.getCompanyId();
		EECUtil.getEBSResponse(actionRequest);
		@SuppressWarnings("unchecked")
		HashMap<String, String> map = (HashMap<String, String>) actionRequest
				.getAttribute("EBSResponse");
		long responseCode = Long.parseLong(map.get("ResponseCode"));
		String isFlagged = map.get("IsFlagged");
		String responseMessage = map.get("ResponseMessage");
		String dateCreatedString = map.get("DateCreated");
		Date dateCreated = EECUtil.stringToDate(dateCreatedString,
				"yyyy-MM-dd hh:mm:ss");
		String amount = map.get("Amount").toString();
		String transactionIdString = map.get("TransactionID");
		long transactionId = Long.parseLong(transactionIdString);
		String orderId = map.get("MerchantRefNo").toString();
		StringBuffer billId = new StringBuffer();
		Date fromDate = null;
		Date toDate = null;
		String body = null;
		long paymentMethod = Long.parseLong(map.get("PaymentMethod").trim());
		long paymentId = Long.parseLong(map.get("PaymentID"));
		if (responseCode == 0) {
			if (isFlagged.equalsIgnoreCase("NO")) {
				try {
					ServiceContext serviceContext = ServiceContextFactory
							.getInstance(actionRequest);
					PaymentUserDetail userDetail = PaymentUserDetailLocalServiceUtil
							.findByCompanyId(companyId);
					String emailFromName = PortletPropsValues.ADMIN_EMAIL_FROM_NAME;
					String emailFromAddress = PortletPropsValues.ADMIN_EMAIL_FROM_ADDRESS;
					String subject = EECUtil
							.getEsquareCartMailContent(
									EECConstants.DEFAULT_COMPANY_ID,
									PortletPropsValues.PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_SUBJECT_NAME,
									PortletPropsValues.PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_SUBJECT);
					body = EECUtil
							.getEsquareCartMailContent(
									EECConstants.DEFAULT_COMPANY_ID,
									PortletPropsValues.PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_BODY_NAME,
									PortletPropsValues.PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_BODY);
					EcartEbsPaymentLocalServiceUtil.addEcartEbsPayment(
							companyId, orderId, userDetail.getUserId(),
							responseCode, responseMessage, dateCreated,
							paymentId, paymentMethod, amount, isFlagged,
							transactionId);
					List<EcartPackageBilling> ecartPackageBillings = EcartPackageBillingLocalServiceUtil
							.findByCompanyTransactionId(companyId, 0);
					String delim = StringPool.BLANK;
					for (EcartPackageBilling ecartPackageBilling : ecartPackageBillings) {
						billId.append(delim).append(
								ecartPackageBilling.getBillId());
						delim = ",";
						EcartPackageBillingLocalServiceUtil.addTranscationId(
								ecartPackageBilling.getBillId(), transactionId);
					}
					CartPermission cartPermission = CartPermissionLocalServiceUtil
							.getCartPermissionCI_PK(companyId,
									EECConstants.MONTH);
					Instance instance = InstanceLocalServiceUtil
							.getInstance(companyId);
					fromDate = instance.getExpiryDate();
					Calendar expDate = Calendar.getInstance();
					expDate.setTime(fromDate);
					expDate.add(Calendar.MONTH,
							Integer.parseInt(cartPermission.getValue()));
					toDate = expDate.getTime();
					instance.setExpiryDate(toDate);
					InstanceLocalServiceUtil.updateInstance(instance);
					try {
						country = CountryServiceUtil.getCountry(
								Long.parseLong(userDetail.getCountry()))
								.getName();
						state = RegionServiceUtil.getRegion(
								Long.parseLong(userDetail.getState()))
								.getName();
					} catch (Exception e) {
						_log.error(e.getCause());
					}
					String[] oldStub = new String[] { "[$TO_NAME$]",
							"[$PORTLET_NAME$]", "[$EVENT_TITLE$]",
							"[$TRANSACTION_ID$]", "[$FROM_NAME$]",
							"[$FROM_ADDRESS$]", "[$PORTAL_URL$]",
							"[$CUSTOMER_NAME$]", "[$STORE_ID$]",
							"[$STORE_DOMAIN$]", "[$COUNTRY$]", "[$STATE$]",
							"[$BILL_ID$]", "[$BILL_FROM$]", "[$BILL_TO$]",
							"[$DATE$]" };
					String[] newStub = new String[] { userDetail.getName(),
							"PackageBillings", "Invoice",
							String.valueOf(transactionId), emailFromName,
							emailFromAddress,
							PortletPropsValues.DEFAULT_PORTAL_URL,
							userDetail.getName(), String.valueOf(companyId),
							themeDisplay.getCompany().getWebId(), country,
							state, String.valueOf(billId),
							EECUtil.dateToString(fromDate, "dd/MM/yyyy"),
							EECUtil.dateToString(toDate, "dd/MM/yyyy"),
							EECUtil.dateToString(new Date(), "dd/MM/yyyy") };
					body = StringUtil.replace(body, oldStub, newStub);
					subject = StringUtil.replace(subject, oldStub, newStub);
					SubscriptionSender subscriptionSender = new SubscriptionSender();
					subscriptionSender.setBody(body);
					subscriptionSender.setCompanyId(companyId);
					subscriptionSender.setFrom(emailFromAddress, emailFromName);
					subscriptionSender.setHtmlFormat(true);
					subscriptionSender
							.setMailId("transactionId", transactionId);
					subscriptionSender.setScopeGroupId(themeDisplay
							.getScopeGroupId());
					subscriptionSender.setServiceContext(serviceContext);
					subscriptionSender.setSubject(subject);
					subscriptionSender.setUserId(userDetail.getUserId());
					subscriptionSender.addRuntimeSubscribers(
							userDetail.getEmail(), userDetail.getName());

					subscriptionSender.flushNotificationsAsync();

				} catch (Exception e) {
					_log.error(e.getClass());
				}
			} else if (isFlagged.equalsIgnoreCase("YES")) {
				actionResponse.setRenderParameter("tabs", tabName[3]);
				actionResponse.setRenderParameter("isFlagged", "true");
				return;
			}
			actionResponse.setRenderParameter("tabs", tabName[3]);
			actionResponse.setRenderParameter("inVoice", body);
			actionResponse.setRenderParameter("transactionId",
					transactionIdString);
		} else {
			actionResponse.setRenderParameter("tabs", tabName[3]);
			actionResponse.setRenderParameter("isTransactionFailed", "true");
		}
	}

	protected void updatePaymentUserDetail(ActionRequest actionRequest) {
		long recId = ParamUtil.getLong(actionRequest, "recId");
		String name = ParamUtil.getString(actionRequest, "name");
		String address = ParamUtil.getString(actionRequest, "address");
		String country = ParamUtil.getString(actionRequest, "country");
		String state = ParamUtil.getString(actionRequest, "state");
		String city = ParamUtil.getString(actionRequest, "city");
		long pinCode = ParamUtil.getLong(actionRequest, "pinCode");
		String email = ParamUtil.getString(actionRequest, "email");
		long phoneNo = ParamUtil.getLong(actionRequest, "phoneNo");
		if (recId > 0) {
			try {
				PaymentUserDetailLocalServiceUtil.updatePaymentUserDetail(
						recId, name, address, country, state, city, pinCode,
						email, phoneNo);
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long companyId = themeDisplay.getCompanyId();
		String cmd = ParamUtil.getString(resourceRequest, EECConstants.CMD);
		int month = ParamUtil.getInteger(resourceRequest, "month", 0);
		int year = ParamUtil.getInteger(resourceRequest, "year", 0);
		String inVoice = ParamUtil.getString(resourceRequest, "inVoice");
		String billingTemplate = null;
		if (cmd.equals(EECConstants.DOWNLOAD)) {
			if (Validator.isNotNull(inVoice)) {
				billingTemplate = inVoice;
			} else if (month != 0 && year != 0) {

				Instance instance = null;
				String country = null;
				String state = null;
				CartPermission cartPermission = CartPermissionLocalServiceUtil
						.getCartPermissionCI_PK(companyId, EECConstants.MONTH);
				PaymentUserDetail paymentUserDetail = null;
				try {
					instance = InstanceLocalServiceUtil
							.getInstance(themeDisplay.getCompanyId());
					paymentUserDetail = PaymentUserDetailLocalServiceUtil
							.findByUserId(themeDisplay.getUserId());
				} catch (PortalException e) {
					_log.info(e.getClass());
				} catch (SystemException e) {
					_log.info(e.getClass());
				}
				EcartPackageBilling packageBilling = EcartPackageBillingLocalServiceUtil
						.getPackageBillingList(month + 1, year, companyId).get(
								0);
				try {
					country = CountryServiceUtil.getCountry(
							Long.parseLong(paymentUserDetail.getCountry()))
							.getName();
					state = RegionServiceUtil.getRegion(
							Long.parseLong(paymentUserDetail.getState()))
							.getName();
				} catch (Exception e) {
					_log.error(e.getCause());
				}
				Calendar toDateCalendar = Calendar.getInstance();
				toDateCalendar.setTime(packageBilling.getBillingDate());
				toDateCalendar.add(Calendar.MONTH,
						Integer.parseInt(cartPermission.getValue()));
				Date toDate = toDateCalendar.getTime();
				billingTemplate = EECUtil
						.getEsquareCartMailContent(
								EECConstants.DEFAULT_COMPANY_ID,
								PortletPropsValues.PACKAGE_BILLING_VIEW_BILL_TEMPLATE_NAME,
								PortletPropsValues.PACKAGE_BILLING_VIEW_BILL_TEMPLATE);
				billingTemplate = StringUtil.replace(
						billingTemplate,
						new String[] { "[$CUR_DATE$]", "[$BILL_TITLE$]",
								"[$NAME$]", "[$STORE_NAME$]", "[$STORE_ID$]",
								"[$COUNTRY$]", "[$STATE$]", "[$BILL_ID$]",
								"[$BILL_FROM$]", "[$BILL_TO$]",
								"[$PACKAGE_TYPE$]", "[$DESCRIPTION$]",
								"[$AMOUNT$]" },
						new String[] {
								EECUtil.dateToString(new Date(), "dd/MM/yyyy"),
								"Package Bill",
								paymentUserDetail.getName(),
								themeDisplay.getCompany().getWebId(),
								String.valueOf(companyId),
								country,
								state,
								String.valueOf(packageBilling.getBillId()),
								EECUtil.dateToString(
										packageBilling.getBillingDate(),
										"dd/MM/yyyy"),
								EECUtil.dateToString(toDate, "dd/MM/yyyy"),
								instance.getPackageType(), "NONE",
								String.valueOf(packageBilling.getAmount()) });
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			OutputStream portletOutputStream = null;
			Document document = null;
			try {
				document = new Document();
				PdfWriter pdfWriter = PdfWriter.getInstance(document, baos);
				document.open();
				XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
				xmlWorkerHelper.parseXHtml(pdfWriter, document,
						new StringReader(billingTemplate));
				document.close();
				resourceResponse.setContentType("application/pdf");
				resourceResponse.setProperty(HttpHeaders.CONTENT_DISPOSITION,
						"attachement;filename=Package Bill");
				resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
						"max-age=3600, must-revalidate");
				resourceResponse.setContentLength(baos.size());
				portletOutputStream = (OutputStream) resourceResponse
						.getPortletOutputStream();
				baos.writeTo(portletOutputStream);
				portletOutputStream.flush();

			} catch (Exception e) {
				_log.info(e.getClass());
			} finally {
				portletOutputStream.close();
				baos.close();
			}
		} else if (cmd.equals(EECConstants.MONTH)) {
			List<Integer> monthList = EcartPackageBillingLocalServiceUtil
					.getPackageBillingMonth(year, companyId);
			JSONObject jsonFeed = JSONFactoryUtil.createJSONObject();
			JSONArray monthValue = JSONFactoryUtil.getJSONFactory()
					.createJSONArray();
			for (Integer countryObj : monthList) {
				monthValue.put(countryObj - 1);
			}
			jsonFeed.put("monthValue", monthValue);
			resourceResponse.setContentType("application/json");
			resourceResponse.setCharacterEncoding("UTF-8");
			resourceResponse.getWriter().write(jsonFeed.toString());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(PackageBillingAction.class);
}
