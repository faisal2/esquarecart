package com.esquare.ecommerce.customField.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.stream.XMLStreamException;

import com.esquare.ecommerce.model.EcustomField;
import com.esquare.ecommerce.model.impl.EcustomFieldImpl;
import com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CustomFieldAction extends MVCPortlet {
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.EDIT)) {
			try {
				updateCustomField(actionRequest, actionResponse);
			} catch (PortalException e) {
				_log.info(e.getClass());
			}
		} else if (cmd.equals(EECConstants.DELETE)) {
			deleteCustomField(actionRequest);
		}
	}

	private void updateCustomField(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException {
		long recId = ParamUtil.getLong(actionRequest, "recId");
		String title = ParamUtil.getString(actionRequest, "title");
		long companyId = ParamUtil.getLong(actionRequest, "companyId");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String xml = StringPool.BLANK;
		EcustomField entityImpl = new EcustomFieldImpl();
		entityImpl.setTitle(title);
		entityImpl.setCompanyId(companyId);
		try {
			if (recId <= 0) {
				recId = CounterLocalServiceUtil.increment("EcustomField.class");
				entityImpl.setCustomFieldRecId(recId);
				xml = getXMLContent(actionRequest);
				entityImpl.setXml(xml);
				EcustomFieldLocalServiceUtil.addEcustomField(entityImpl);
			} else {
				entityImpl.setCustomFieldRecId(recId);
				xml = getXMLContent(actionRequest);
				entityImpl.setXml(xml);
				EcustomFieldLocalServiceUtil.updateEcustomField(entityImpl);
			}
		} catch (SystemException e) {
			_log.info(e.getClass());
		} catch (XMLStreamException e) {
			_log.info(e.getClass());
		}
		String portletName = (String) actionRequest
				.getAttribute(WebKeys.PORTLET_ID);
		PortletURL redirectURL = PortletURLFactoryUtil
				.create(PortalUtil.getHttpServletRequest(actionRequest),
						portletName, themeDisplay.getLayout().getPlid(),
						PortletRequest.RENDER_PHASE);
		try {
			actionResponse.sendRedirect(redirectURL.toString());
		} catch (IOException e) {
			_log.info(e.getClass());
		}
	}

	private String getXMLContent(ActionRequest actionRequest)
			throws XMLStreamException {
		String xmlContent = StringPool.BLANK;
		String formFieldsIndexes = ParamUtil.getString(actionRequest,
				"formFieldsIndexes");
		String[] fileds = formFieldsIndexes.split(",");
		StringBuffer sb_KeyName = new StringBuffer();
		StringBuffer sb_KeyType = new StringBuffer();
		for (int i = 0; i < fileds.length; i++) {
			sb_KeyName.append(ParamUtil.getString(actionRequest, "fieldLabel"
					+ fileds[i]));
			sb_KeyName.append(",");
			String keyType = ParamUtil.getString(actionRequest, "fieldType"
					+ fileds[i]);
			if (Validator.isNull(keyType))
				keyType = "text";
			sb_KeyType.append(keyType);
			sb_KeyType.append(",");
		}
		xmlContent = EECUtil.addXmlContent(StringPool.BLANK,
				PortletPropsValues.CUSTOM_FIELD_TAG,
				PortletPropsValues.ID_ATTRIBUTE, fileds, sb_KeyName.toString()
						.split(","), sb_KeyType.toString().split(","));
		return xmlContent;
	}

	private void deleteCustomField(ActionRequest actionRequest) {
		long[] deleteFieldIds = StringUtil.split(
				ParamUtil.getString(actionRequest, "deleteFieldIds"), 0L);
		if (Validator.isNotNull(deleteFieldIds)) {
			for (int i = 0; i < deleteFieldIds.length; i++) {
				try {
					EcustomFieldLocalServiceUtil
							.deleteEcustomField(deleteFieldIds[i]);
				} catch (Exception e) {
				}
			}
		}
	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String cmd = request.getParameter(EECConstants.CMD);
		if (cmd.equals("TITLE")) {
			response.setContentType("text/javascript");
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			try {
				long recId = ParamUtil.getLong(request, "recId");
				String title = request.getParameter("title");
				long companyId = ParamUtil.getLong(request, "companyId");
				boolean isTitle = false;
				EcustomField ecustomField = null;
				try {
					ecustomField = EcustomFieldLocalServiceUtil.findByTitle(
							companyId, title);
					/*
					 * if(ecustomField!=null){ isTitle=true; }
					 */
					if (recId <= 0) {
						if (ecustomField != null) {
							isTitle = true;
						}
					} else {
						if (ecustomField != null
								&& ecustomField.getCustomFieldRecId() != recId) {
							isTitle = true;
						}
					}

				} catch (Exception e) {
				}
				jsonObject.put("retVal", isTitle);
				PrintWriter writer = response.getWriter();
				writer.write(jsonObject.toString());

			} catch (Exception e) {
			}

		}
	}

	private static Log _log = LogFactoryUtil.getLog(CustomFieldAction.class);
}
