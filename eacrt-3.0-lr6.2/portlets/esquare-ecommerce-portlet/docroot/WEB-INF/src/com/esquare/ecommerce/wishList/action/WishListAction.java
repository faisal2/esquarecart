package com.esquare.ecommerce.wishList.action;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.xml.namespace.QName;

import com.esquare.ecommerce.CouponActiveException;
import com.esquare.ecommerce.CouponEndDateException;
import com.esquare.ecommerce.CouponStartDateException;
import com.esquare.ecommerce.NoSuchCouponException;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.WishList;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.service.WishListLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.shopping.CartMinQuantityException;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class WishListAction extends MVCPortlet {

	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		String cmd1 = ParamUtil.getString(actionRequest, EECConstants.CMD1);
		if (cmd1.equals(EECConstants.WISHLIST_UPDATE)) {
			try {
				updateWishList(actionRequest, actionResponse);
			} catch (Exception e) {
				if (e instanceof CartMinQuantityException
						|| e instanceof CouponActiveException
						|| e instanceof CouponEndDateException
						|| e instanceof CouponStartDateException
						|| e instanceof NoSuchCouponException) {

					SessionErrors.add(actionRequest, e.getClass(), e);
				}
			}
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/wishList/view.jsp");
		} else if (cmd1.equals(EECConstants.PRODUCT)) {
			try {
				redirectToDetailsPage(actionRequest, actionResponse);
			} catch (PortalException e) {
				_log.info(e.getClass());
			} catch (SystemException e) {
				_log.info(e.getClass());
			}
		}
	}

	protected void updateWishList(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException,
			SystemException {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		WishList wishList = EECUtil.getWishList(actionRequest);
		if (cmd.equals(EECConstants.ADD)) {
			long productInventoryId = ParamUtil.getLong(actionRequest,
					"productInventoryIds");
			ProductInventory productInventory = ProductInventoryLocalServiceUtil
					.getProductInventory(productInventoryId);
			if (EECUtil.isInStock(productInventory)) {
				wishList.addProductInventoryId(productInventoryId);
			}

		} else {
			String productInventoryIds = ParamUtil.getString(actionRequest,
					"productInventoryIds");
			wishList.setProductInventoryIds(productInventoryIds);

		}

		WishListLocalServiceUtil.updateWishList(wishList.getUserId(),
				wishList.getCompanyId(), wishList.getGroupId(),
				wishList.getProductInventoryIds());
		if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.UPDATE)) {
			addSuccessMessage(actionRequest, actionResponse);
		}
	}

	public void redirectToDetailsPage(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortalException,
			SystemException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String curProductId = actionRequest.getParameter("curProductId");
		QName qName = new QName("http://liferay.com", "productId", "x");
		actionResponse.setEvent(qName, curProductId + "," + "detailsView");
		long Plid = LayoutLocalServiceUtil.getFriendlyURLLayout(
				themeDisplay.getScopeGroupId(), false,
				EECConstants.PRODUCT_DETAILS_PAGE_FRIENDLY_URL).getPlid();
		PortletURL redirectURL = EECUtil.getPortletURL(actionRequest,
				EECConstants.USER_VIEW_PORTLET_NAME, Plid,
				ActionRequest.RENDER_PHASE,
				"/html/eec/common/userview/detail_view.jsp", null,
				LiferayWindowState.NORMAL, LiferayPortletMode.VIEW);
		redirectURL.setParameter("curProductId", curProductId);
		actionResponse.sendRedirect(redirectURL.toString());
	}

	private static Log _log = LogFactoryUtil.getLog(WishListAction.class);
}