package com.esquare.ecommerce.mainpackagebilling.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.esquare.ecommerce.model.CartPermission;
import com.esquare.ecommerce.model.EcartPackageBilling;
import com.esquare.ecommerce.model.Instance;
import com.esquare.ecommerce.model.PaymentUserDetail;
import com.esquare.ecommerce.service.CartPermissionLocalServiceUtil;
import com.esquare.ecommerce.service.EcartEbsPaymentLocalServiceUtil;
import com.esquare.ecommerce.service.EcartPackageBillingLocalServiceUtil;
import com.esquare.ecommerce.service.InstanceLocalServiceUtil;
import com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.util.SubscriptionSender;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class MainPackageBillingAction
 */
public class MainPackageBillingAction extends MVCPortlet {

	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		String country = null;
		String state = null;

		EECUtil.getEBSResponse(actionRequest);
		@SuppressWarnings("unchecked")
		HashMap<String, String> map = (HashMap<String, String>) actionRequest
				.getAttribute("EBSResponse");
		long responseCode = Long.parseLong(map.get("ResponseCode"));
		String isFlagged = map.get("IsFlagged");
		String responseMessage = map.get("ResponseMessage");
		String dateCreatedString = map.get("DateCreated");
		Date dateCreated = EECUtil.stringToDate(dateCreatedString,
				"yyyy-MM-dd hh:mm:ss");
		String amount = map.get("Amount").toString();
		String transactionIdString = map.get("TransactionID");
		long transactionId = Long.parseLong(transactionIdString);
		String orderId = map.get("MerchantRefNo").toString();

		StringBuffer billId = new StringBuffer();
		Date fromDate = null;
		Date toDate = null;
		String body = null;
		long paymentMethod = Long.parseLong(map.get("PaymentMethod").trim());
		long paymentId = Long.parseLong(map.get("PaymentID"));
		if (responseCode == 0) {
			if (isFlagged.equalsIgnoreCase("NO")) {
				try {
					long companyId = EcartPackageBillingLocalServiceUtil
							.getEcartPackageBilling(Long.valueOf(orderId))
							.getCompanyId();
					CartPermission cartPermission = CartPermissionLocalServiceUtil
							.getCartPermissionCI_PK(companyId,
									EECConstants.MONTH);
					Instance instance = InstanceLocalServiceUtil
							.getInstance(companyId);
					Company company = CompanyLocalServiceUtil
							.getCompany(companyId);
					ServiceContext serviceContext = ServiceContextFactory
							.getInstance(actionRequest);
					PaymentUserDetail paymentUserDetail = PaymentUserDetailLocalServiceUtil
							.findByCompanyId(companyId);
					String emailFromName = PortletPropsValues.ADMIN_EMAIL_FROM_NAME;
					String emailFromAddress = PortletPropsValues.ADMIN_EMAIL_FROM_ADDRESS;
					String subject = EECUtil
							.getEsquareCartMailContent(
									EECConstants.DEFAULT_COMPANY_ID,
									PortletPropsValues.PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_SUBJECT_NAME,
									PortletPropsValues.PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_SUBJECT);
					body = EECUtil
							.getEsquareCartMailContent(
									EECConstants.DEFAULT_COMPANY_ID,
									PortletPropsValues.PACKAGE_BILLING_PAYMENT_CONFIRMATION_EMAIL_BODY_NAME,
									PortletPropsValues.PACKAGE_BILLING_DEFAULT_PAYMENT_CONFIRMATION_EMAIL_BODY);
					EcartEbsPaymentLocalServiceUtil.addEcartEbsPayment(
							companyId, orderId, paymentUserDetail.getUserId(),
							responseCode, responseMessage, dateCreated,
							paymentId, paymentMethod, amount, isFlagged,
							transactionId);
					List<EcartPackageBilling> ecartPackageBillings = EcartPackageBillingLocalServiceUtil
							.findByCompanyTransactionId(companyId, 0);
					String delim = StringPool.BLANK;
					for (EcartPackageBilling ecartPackageBilling : ecartPackageBillings) {
						billId.append(delim).append(
								ecartPackageBilling.getBillId());
						delim = ",";
						EcartPackageBillingLocalServiceUtil.addTranscationId(
								ecartPackageBilling.getBillId(), transactionId);
					}

					if (company.getActive()) {
						fromDate = instance.getExpiryDate();
						Calendar expDate = Calendar.getInstance();
						expDate.setTime(fromDate);
						expDate.add(Calendar.MONTH,
								Integer.parseInt(cartPermission.getValue()));
						toDate = expDate.getTime();
						instance.setExpiryDate(toDate);
						InstanceLocalServiceUtil.updateInstance(instance);
					} else {
						fromDate = new Date();
						Calendar expDate = Calendar.getInstance();
						expDate.setTime(fromDate);
						expDate.add(Calendar.MONTH,
								Integer.parseInt(cartPermission.getValue()));
						toDate = expDate.getTime();
						instance.setExpiryDate(toDate);
						InstanceLocalServiceUtil.updateInstance(instance);
						company.setActive(true);
						CompanyLocalServiceUtil.updateCompany(company);

					}
					try {
						country = CountryServiceUtil.getCountry(
								Long.parseLong(paymentUserDetail.getCountry()))
								.getName();
						state = RegionServiceUtil.getRegion(
								Long.parseLong(paymentUserDetail.getState()))
								.getName();
					} catch (Exception e) {
						_log.error(e.getCause());
					}
					String[] oldStub = new String[] { "[$TO_NAME$]",
							"[$PORTLET_NAME$]", "[$EVENT_TITLE$]",
							"[$TRANSACTION_ID$]", "[$FROM_NAME$]",
							"[$FROM_ADDRESS$]", "[$PORTAL_URL$]",
							"[$CUSTOMER_NAME$]", "[$STORE_ID$]",
							"[$STORE_DOMAIN$]", "[$COUNTRY$]", "[$STATE$]",
							"[$BILL_ID$]", "[$BILL_FROM$]", "[$BILL_TO$]",
							"[$DATE$]" };
					String[] newStub = new String[] {
							paymentUserDetail.getName(), "PackageBillings",
							"Invoice", String.valueOf(transactionId),
							emailFromName, emailFromAddress,
							PortletPropsValues.DEFAULT_PORTAL_URL,
							paymentUserDetail.getName(),
							String.valueOf(companyId), company.getWebId(),
							country, state, String.valueOf(billId),
							EECUtil.dateToString(fromDate, "dd/MM/yyyy"),
							EECUtil.dateToString(toDate, "dd/MM/yyyy"),
							EECUtil.dateToString(new Date(), "dd/MM/yyyy") };
					body = StringUtil.replace(body, oldStub, newStub);
					subject = StringUtil.replace(subject, oldStub, newStub);
					SubscriptionSender subscriptionSender = new SubscriptionSender();
					subscriptionSender.setBody(body);
					subscriptionSender.setCompanyId(companyId);
					subscriptionSender.setFrom(emailFromAddress, emailFromName);
					subscriptionSender.setHtmlFormat(true);
					subscriptionSender
							.setMailId("transactionId", transactionId);
					subscriptionSender.setServiceContext(serviceContext);
					subscriptionSender.setSubject(subject);
					subscriptionSender.setUserId(paymentUserDetail.getUserId());
					subscriptionSender.addRuntimeSubscribers(
							paymentUserDetail.getEmail(),
							paymentUserDetail.getName());

					subscriptionSender.flushNotificationsAsync();

				} catch (Exception e) {
					_log.error(e.getClass());
				}
			} else if (isFlagged.equalsIgnoreCase("YES")) {
				actionResponse
						.setRenderParameter("jspPage",
								"/html/eec/common/main_package_billing/paybill_payment_flagged.jsp");
				return;
			}
			actionResponse
					.setRenderParameter("jspPage",
							"/html/eec/common/main_package_billing/paybill_payment_success.jsp");
			actionResponse.setRenderParameter("inVoice", body);
			actionResponse.setRenderParameter("transactionId",
					transactionIdString);
		} else {
			actionResponse
					.setRenderParameter("jspPage",
							"/html/eec/common/main_package_billing/paybill_payment_failure.jsp");
		}
	}

	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String billingTemplate = ParamUtil
				.getString(resourceRequest, "inVoice");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		OutputStream portletOutputStream = null;
		Document document = null;
		try {
			document = new Document();
			PdfWriter pdfWriter = PdfWriter.getInstance(document, baos);
			document.open();
			XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
			xmlWorkerHelper.parseXHtml(pdfWriter, document, new StringReader(
					billingTemplate));
			document.close();
			resourceResponse.setContentType("application/pdf");
			resourceResponse.setProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachement;filename=Package Bill");
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
					"max-age=3600, must-revalidate");
			resourceResponse.setContentLength(baos.size());
			portletOutputStream = (OutputStream) resourceResponse
					.getPortletOutputStream();
			baos.writeTo(portletOutputStream);
			portletOutputStream.flush();

		} catch (Exception e) {
			_log.info(e.getClass());
		} finally {
			portletOutputStream.close();
			baos.close();
		}
	}

	private static Log _log = LogFactoryUtil
			.getLog(MainPackageBillingAction.class);
}
