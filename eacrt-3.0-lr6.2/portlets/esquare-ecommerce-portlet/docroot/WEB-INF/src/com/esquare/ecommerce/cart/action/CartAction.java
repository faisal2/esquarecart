package com.esquare.ecommerce.cart.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.esquare.ecommerce.BillingCityException;
import com.esquare.ecommerce.BillingCountryException;
import com.esquare.ecommerce.BillingEmailAddressException;
import com.esquare.ecommerce.BillingFirstNameException;
import com.esquare.ecommerce.BillingLastNameException;
import com.esquare.ecommerce.BillingPhoneException;
import com.esquare.ecommerce.BillingStateException;
import com.esquare.ecommerce.BillingStreetException;
import com.esquare.ecommerce.BillingZipException;
import com.esquare.ecommerce.CouponActiveException;
import com.esquare.ecommerce.CouponEndDateException;
import com.esquare.ecommerce.CouponStartDateException;
import com.esquare.ecommerce.NoSuchCouponException;
import com.esquare.ecommerce.ShippingCityException;
import com.esquare.ecommerce.ShippingCountryException;
import com.esquare.ecommerce.ShippingEmailAddressException;
import com.esquare.ecommerce.ShippingFirstNameException;
import com.esquare.ecommerce.ShippingLastNameException;
import com.esquare.ecommerce.ShippingPhoneException;
import com.esquare.ecommerce.ShippingStateException;
import com.esquare.ecommerce.ShippingStreetException;
import com.esquare.ecommerce.ShippingZipException;
import com.esquare.ecommerce.WalletLimitException;
import com.esquare.ecommerce.model.Cart;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.model.PayPalOrderTracker;
import com.esquare.ecommerce.model.ProductInventory;
import com.esquare.ecommerce.model.WishList;
import com.esquare.ecommerce.model.WishListItem;
import com.esquare.ecommerce.model.impl.PayPalOrderTrackerImpl;
import com.esquare.ecommerce.service.CartLocalServiceUtil;
import com.esquare.ecommerce.service.EBSOrderTrackerLocalServiceUtil;
import com.esquare.ecommerce.service.OrderItemLocalServiceUtil;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.PayPalOrderTrackerLocalServiceUtil;
import com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil;
import com.esquare.ecommerce.service.WishListLocalServiceUtil;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.esquare.ecommerce.util.EmailUtil;
import com.esquare.ecommerce.util.PortletPropsValues;
import com.esquare.ecommerce.util.SMSUtil;
import com.liferay.portal.kernel.captcha.CaptchaException;
import com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException;
import com.liferay.portal.kernel.captcha.CaptchaTextException;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.shopping.CartMinQuantityException;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CartAction extends MVCPortlet {
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = themeDisplay.getUserId();
		long companyId = themeDisplay.getCompanyId();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		String cmd1 = ParamUtil.getString(actionRequest, EECConstants.CMD1);
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);

		if (cmd1.equals(EECConstants.CART_UPDATE)) {

			try {
				updateCart(actionRequest, actionResponse);
			} catch (Exception e) {
				if (e instanceof CartMinQuantityException
						|| e instanceof CouponActiveException
						|| e instanceof CouponEndDateException
						|| e instanceof CouponStartDateException
						|| e instanceof NoSuchCouponException) {

					SessionErrors.add(actionRequest, e.getClass(), e);
				}
			}
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/cart/view.jsp");
		} else if (cmd1.equals(EECConstants.CHECKOUT)
				|| cmd.equals(EECConstants.EBS_RESPONSE)) {

			try {
				getLatestOrder(actionRequest);
			} catch (Exception e) {
				_log.info(e, e);
			}
			if (cmd.equals(EECConstants.COD) || cmd.equals(EECConstants.WALLET)) {
				try {
					checkCaptcha(actionRequest);
					saveLatestOrder(actionRequest);
					Order order = (Order) actionRequest
							.getAttribute(EECConstants.SHOPPING_ORDER);
					Date expectedDelivereyDate = expectedDeliveryDate(order
							.getOrderId());
					OrderLocalServiceUtil.updateOrder(order, cmd, cmd, null);
					OrderItemLocalServiceUtil.updateOrderItemStatusByOrder(
							order.getOrderId(),
							EECConstants.ORDER_STATUS_PROCESSING, null,
							expectedDelivereyDate, null, 0l, false);
					forwardCheckout(actionRequest, actionResponse);
					EmailUtil.sendMail(actionRequest, order.getOrderId(), cmd1,
							null, null);
					SMSUtil.sendSMS(actionRequest, order.getOrderId(), cmd1);

				} catch (Exception e) {
					_log.info(e, e);
					if (e instanceof CaptchaTextException
							|| e instanceof CaptchaMaxChallengesException) {
						SessionErrors.add(actionRequest, e.getClass(), e);
						actionResponse.setRenderParameter("jspPage",
								"/html/eec/common/cart/payment_type.jsp");
					} else if (e instanceof WalletLimitException) {
						SessionErrors.add(actionRequest, e.getClass(), e);
						actionResponse.setRenderParameter("jspPage",
								"/html/eec/common/cart/view.jsp");
					}
				}

			} else if (cmd.equals(EECConstants.PAYMENT_TYPE)) {
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/cart/payment_type.jsp");
			} else if (cmd.equals(EECConstants.UPDATE)) {
				try {
					updateLatestOrder(actionRequest);
					actionResponse.setRenderParameter("jspPage",
							"/html/eec/common/cart/confirmation_page.jsp");
				} catch (Exception e) {
					_log.info(e, e);
					if (e instanceof BillingCityException
							|| e instanceof BillingCountryException
							|| e instanceof BillingEmailAddressException
							|| e instanceof BillingFirstNameException
							|| e instanceof BillingLastNameException
							|| e instanceof BillingPhoneException
							|| e instanceof BillingStateException
							|| e instanceof BillingStreetException
							|| e instanceof BillingZipException
							|| e instanceof ShippingCityException
							|| e instanceof ShippingCountryException
							|| e instanceof ShippingEmailAddressException
							|| e instanceof ShippingFirstNameException
							|| e instanceof ShippingLastNameException
							|| e instanceof ShippingPhoneException
							|| e instanceof ShippingStateException
							|| e instanceof ShippingStreetException
							|| e instanceof ShippingZipException) {

						SessionErrors.add(actionRequest, e.getClass());

						actionResponse.setRenderParameter("jspPage",
								"/html/eec/common/cart/billing_address.jsp");
					} else if (e instanceof PrincipalException) {
						_log.error(e);
					}

				}
			} else if (cmd.equals(EECConstants.VIEW)) {
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/cart/success.jsp");
			} else if (cmd.equals(EECConstants.EBS_RESPONSE)) {
				EECUtil.getEBSResponse(actionRequest);
				@SuppressWarnings("unchecked")
				HashMap<String, String> map = (HashMap<String, String>) actionRequest
						.getAttribute("EBSResponse");
				long responseCode = Integer.parseInt(map.get("ResponseCode"));
				String isFlagged = map.get("IsFlagged");
				String responseMessage = map.get("ResponseMessage");
				String dateCreated = map.get("DateCreated");
				String amount = map.get("Amount").toString();
				long transactionId = Integer.parseInt(map.get("TransactionID"));
				long orderId = Integer.parseInt(map.get("MerchantRefNo"));
				Order order = null;
				try {
					order = OrderLocalServiceUtil.getOrder(orderId);
				} catch (Exception e1) {
				}
				long paymentMethod = Integer.parseInt(map.get("PaymentMethod")
						.trim());
				long paymentId = Integer.parseInt(map.get("PaymentID"));
				Date createDate = null;
				try {
					createDate = dateFormat.parse(dateCreated);
				} catch (ParseException e) {
					_log.error(e);
				}
				Date expectedDeliveryDate = expectedDeliveryDate(orderId);
				if (responseCode == 0) {
					String paymentStatus = null;
					String orderItemStatus = null;
					if (isFlagged.equalsIgnoreCase("NO")) {
						paymentStatus = EECConstants.PAYMENT_SUCCESSFUL;
					} else if (isFlagged.equalsIgnoreCase("YES")) {
						paymentStatus = EECConstants.PAYMENT_PENDING;
						expectedDeliveryDate = null;
					}

					try {
						addToEBSOrderTracker(companyId, orderId, userId,
								responseCode, responseMessage, createDate,
								paymentId, paymentMethod, amount, isFlagged,
								transactionId);
						OrderLocalServiceUtil.updateOrder(order,
								EECConstants.EBS, paymentStatus, null);
						saveLatestOrder(actionRequest);
						OrderItemLocalServiceUtil.updateOrderItemStatusByOrder(
								order.getOrderId(),
								EECConstants.ORDER_STATUS_PROCESSING, null,
								expectedDeliveryDate, null, 0l, false);
						forwardCheckout(actionRequest, actionResponse);
					} catch (Exception e) {
						_log.info(e.getClass());
						e.printStackTrace();
					}
				}
			} else {
				try {
					updateWalletAmount(actionRequest, userId, companyId);
				} catch (Exception exception) {
					if (exception instanceof WalletLimitException) {
						SessionErrors.add(actionRequest, exception.getClass());
						actionResponse.setRenderParameter("jspPage",
								"/html/eec/common/cart/view.jsp");
						return;
					}
				}
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/cart/billing_address.jsp");
			}
		} else if (cmd1.equals(EECConstants.WISHLIST_UPDATE)) {

			try {
				updateCart(actionRequest, actionResponse);
				deleteWishListProductId(actionRequest, actionResponse);
			} catch (Exception e) {
				if (e instanceof CartMinQuantityException
						|| e instanceof CouponActiveException
						|| e instanceof CouponEndDateException
						|| e instanceof CouponStartDateException
						|| e instanceof NoSuchCouponException) {

					SessionErrors.add(actionRequest, e.getClass(), e);
				}
			}
			actionResponse.setRenderParameter("jspPage",
					"/html/eec/common/cart/view.jsp");

		}

		super.processAction(actionRequest, actionResponse);
	}

	protected void deleteWishListProductId(ActionRequest actionRequest,
			ActionResponse actionResponse) throws PortalException,
			SystemException {
		WishList wishList = EECUtil.getWishList(actionRequest);
		Map products = wishList.getProductinventories();
		String productInventoryIds = StringPool.BLANK;
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		if (cmd.equals(EECConstants.ADD)) {
			long productInventoryId = ParamUtil.getLong(actionRequest,
					"productInventoryIds");
			Iterator iterator = products.entrySet().iterator();
			for (int i = 0; iterator.hasNext(); i++) {
				Map.Entry entry = (Map.Entry) iterator.next();
				WishListItem wishListItem = (WishListItem) entry.getKey();
				ProductInventory productInventory = wishListItem
						.getProductInventory();
				if (productInventory.getInventoryId() != productInventoryId)
					productInventoryIds += productInventory.getInventoryId()
							+ ",";
			}
			WishListLocalServiceUtil.updateWishList(wishList.getUserId(),
					wishList.getCompanyId(), wishList.getGroupId(),
					productInventoryIds);
		}

	}

	protected void updateCoupon(ActionRequest actionRequest) {

	}

	protected void getLatestOrder(ActionRequest actionRequest) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		Order order = OrderLocalServiceUtil.getLatestOrder(
				themeDisplay.getUserId(), themeDisplay.getCompanyId());

		actionRequest.setAttribute(EECConstants.SHOPPING_ORDER, order);
	}

	protected void forwardCheckout(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		// Cart cart = EECUtil.getCart(actionRequest);

		Order order = (Order) actionRequest
				.getAttribute(EECConstants.SHOPPING_ORDER);

		PortletURL returnURL = EECUtil.getPortletURL(actionRequest,
				EECConstants.CART_PORTLET_NAME, themeDisplay.getPlid(),
				ActionRequest.RENDER_PHASE,
				"/html/eec/common/cart/success.jsp", null,
				LiferayWindowState.NORMAL, LiferayPortletMode.VIEW);
		returnURL
				.setParameter("orderNumber", String.valueOf(order.getNumber()));
		returnURL.setParameter("ordId", Long.toString(order.getOrderId()));
		ServiceContext serviceContext = ServiceContextFactory
				.getInstance(actionRequest);
		if (Validator.isNotNull(order)) {
			try {
				String orderItemDetails = EECUtil.getOrderItemDetails(
						serviceContext.getRequest(), order.getOrderId(),
						themeDisplay.getCompanyId());
				OrderLocalServiceUtil.sendEmail(order,
						EECConstants.CONFIRMATION_MAIL, StringPool.BLANK,
						StringPool.BLANK, orderItemDetails, serviceContext);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		actionResponse.sendRedirect(returnURL.toString());

	}

	protected void updateCart(ActionRequest actionRequest,
			ActionResponse actionResponse) throws Exception {
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		Cart cart = EECUtil.getCart(actionRequest);

		if (cmd.equals(EECConstants.ADD)) {
			long productInventoryIds = ParamUtil.getLong(actionRequest,
					"productInventoryIds");
			ProductInventory productInventory = ProductInventoryLocalServiceUtil
					.getProductInventory(productInventoryIds);
			if (EECUtil.isInStock(productInventory)) {
				cart.addProductInventoryId(productInventoryIds);
			}

		} else {
			String productInventoryIds = ParamUtil.getString(actionRequest,
					"productInventoryIds");
			String couponCodes = ParamUtil.getString(actionRequest,
					"couponCodes");
			int altShipping = ParamUtil
					.getInteger(actionRequest, "altShipping");
			boolean insure = ParamUtil.getBoolean(actionRequest, "insure");

			cart.setProductInventoryIds(productInventoryIds);
			cart.setCouponCodes(couponCodes);
			cart.setAltShipping(altShipping);
			cart.setInsure(insure);
		}

		CartLocalServiceUtil.updateCart(cart.getUserId(), cart.getCompanyId(),
				cart.getGroupId(), cart.getProductInventoryIds(),
				cart.getCouponCodes(), cart.getAltShipping(), cart.isInsure());
		try {
			updateWalletAmount(actionRequest, cart.getUserId(),
					cart.getCompanyId());
		} catch (Exception exception) {
			if (exception instanceof WalletLimitException) {
				SessionErrors.add(actionRequest, exception.getClass());
				actionResponse.setRenderParameter("jspPage",
						"/html/eec/common/cart/view.jsp");
				return;
			}
		}
		if (cmd.equals(EECConstants.ADD) || cmd.equals(EECConstants.UPDATE)) {
			addSuccessMessage(actionRequest, actionResponse);
		}
	}

	protected void updateLatestOrder(ActionRequest actionRequest)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		String billingFirstName = ParamUtil.getString(actionRequest,
				"billingFirstName");
		String billingLastName = ParamUtil.getString(actionRequest,
				"billingLastName");
		String billingEmailAddress = ParamUtil.getString(actionRequest,
				"billingEmailAddress");
		String billingCompany = ParamUtil.getString(actionRequest,
				"billingCompany");
		String billingStreet = ParamUtil.getString(actionRequest,
				"billingStreet");
		String billingCity = ParamUtil.getString(actionRequest, "billingCity");

		String billingState = ParamUtil
				.getString(actionRequest, "billingState");

		String billingZip = ParamUtil.getString(actionRequest, "billingZip");
		String billingCountry = ParamUtil.getString(actionRequest,
				"billingCountry");
		String billingCountryId = "";
		try {
			billingCountryId = ((Long.parseLong(billingCountry)) > 0) ? CountryServiceUtil
					.getCountry(Long.parseLong(billingCountry)).getA2() : "";
		} catch (NumberFormatException numberFormatException) {
			billingCountryId = null;
		}
		String billingPhone = ParamUtil
				.getString(actionRequest, "billingPhone");

		boolean shipToBilling = ParamUtil.getBoolean(actionRequest,
				"shipToBilling");
		String shippingFirstName = ParamUtil.getString(actionRequest,
				"shippingFirstName");
		String shippingLastName = ParamUtil.getString(actionRequest,
				"shippingLastName");
		String shippingEmailAddress = ParamUtil.getString(actionRequest,
				"shippingEmailAddress");
		String shippingCompany = ParamUtil.getString(actionRequest,
				"shippingCompany");
		String shippingStreet = ParamUtil.getString(actionRequest,
				"shippingStreet");
		String shippingCity = ParamUtil
				.getString(actionRequest, "shippingCity");

		String shippingState = ParamUtil.getString(actionRequest,
				"shippingState");

		String shippingZip = ParamUtil.getString(actionRequest, "shippingZip");
		String shippingCountry = ParamUtil.getString(actionRequest,
				"shippingCountry");
		String shippingCountryId = "";
		try {
			shippingCountryId = ((Long.parseLong(shippingCountry)) > 0) ? CountryServiceUtil
					.getCountry(Long.parseLong(shippingCountry)).getA2() : "";
		} catch (NumberFormatException numberFormatException) {
			shippingCountryId = null;
		}
		String shippingPhone = ParamUtil.getString(actionRequest,
				"shippingPhone");
		String comments = ParamUtil.getString(actionRequest, "comments");
		Order order = OrderLocalServiceUtil.updateLatestOrder(
				themeDisplay.getUserId(), themeDisplay.getCompanyId(),
				billingFirstName, billingLastName, billingEmailAddress,
				billingCompany, billingStreet, billingCity, billingState,
				billingZip, billingCountryId, billingPhone, shipToBilling,
				shippingFirstName, shippingLastName, shippingEmailAddress,
				shippingCompany, shippingStreet, shippingCity, shippingState,
				shippingZip, shippingCountryId, shippingPhone, comments);

		actionRequest.setAttribute(EECConstants.SHOPPING_ORDER, order);
	}

	protected void saveLatestOrder(ActionRequest actionRequest)
			throws Exception {

		Cart cart = EECUtil.getCart(actionRequest);
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String cmd = ParamUtil.getString(actionRequest, EECConstants.CMD);
		Order latestOrder = OrderLocalServiceUtil.getLatestOrder(
				themeDisplay.getUserId(), themeDisplay.getCompanyId());
		double grandTotal = EECUtil.calculateTotal(themeDisplay.getCompanyId(),
				themeDisplay.getUserId(), cart.getProductinventories(),
				latestOrder.getShippingCountry(),
				latestOrder.getShippingState(), cart.getCoupon(),
				cart.getAltShipping(), cart.isInsure());
		double myWalletAmount = EECUtil
				.myWalletAmount(themeDisplay.getUserId());
		if ((cmd.equals(EECConstants.WALLET) && myWalletAmount < grandTotal)
				|| (cmd.equals(EECConstants.COD) && myWalletAmount < cart
						.getWalletAmount())) {
			throw new WalletLimitException();
		}
		Order order = OrderLocalServiceUtil.saveLatestOrder(
				themeDisplay.getCompanyId(), themeDisplay.getUserId(), cart);
		updateStock(order.getOrderId());
		actionRequest.setAttribute(EECConstants.SHOPPING_ORDER, order);
	}

	protected void checkCaptcha(ActionRequest actionRequest)
			throws CaptchaException {
		CaptchaUtil.check(actionRequest);
	}

	public Date expectedDeliveryDate(long companyId) {

		String businessdays_maxPref = null;
		try {
			businessdays_maxPref = EECUtil.getPreference(companyId,
					"businessdays_max");
		} catch (SystemException e) {
			_log.info(e, e);
		}
		Calendar c = Calendar.getInstance();
		Date expectedDeliveryDate = new Date();
		c.setTime(new Date()); // Now use today date.
		try {
			c.add(Calendar.DATE, Integer.parseInt(businessdays_maxPref));
		} catch (NumberFormatException numberFormatException) {
			c.add(Calendar.DATE, PortletPropsValues.DELIVER_BUSINESSDAYS_MAX);
		}
		expectedDeliveryDate = c.getTime();
		return expectedDeliveryDate;
	}

	public void addToEBSOrderTracker(long companyId, long orderId, long userId,
			long responseCode, String responseMessage, Date createDate,
			long paymentId, long paymentMethod, String amount,
			String isFlagged, long transactionId) {

		EBSOrderTrackerLocalServiceUtil.addEBSOrderTracker(companyId, orderId,
				userId, responseCode, responseMessage, createDate, paymentId,
				paymentMethod, amount, isFlagged, transactionId);

	}

	public void redirectToPaypal(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException,
			DocumentException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
					.getAttribute(WebKeys.THEME_DISPLAY);
			Cart cart = EECUtil.getCart(actionRequest);
			String paypalResponse = ParamUtil.getString(actionRequest,
					"paypalResponse");
			Map ProductInventories = cart.getProductinventories();

			Coupon coupon = cart.getCoupon();
			int altShipping = cart.getAltShipping();

			Order order = OrderLocalServiceUtil.getLatestOrder(
					themeDisplay.getUserId(), themeDisplay.getCompanyId());
			double amount = EECUtil.calculateTotal(themeDisplay.getCompanyId(),
					themeDisplay.getUserId(), ProductInventories,
					order.getBillingCountry(), order.getBillingState(), coupon,
					altShipping, cart.isInsure());

			actionResponse.setProperty(EECConstants.CMD, "cmdValue");

			String redirectURL = EECUtil.getPayPalRedirectURL(order, amount,
					paypalResponse, themeDisplay.getCompanyId());
			actionResponse.sendRedirect(redirectURL);
		} catch (PortalException e) {
			_log.info(e, e);
		} catch (SystemException e) {
			_log.info(e, e);
		}

	}

	public void paypalResponse(ActionRequest actionRequest,
			ActionResponse actionResponse) {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);

		HttpServletRequest httpReq = PortalUtil
				.getOriginalServletRequest(PortalUtil
						.getHttpServletRequest(actionRequest));
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(actionRequest);
		HttpSession httpSession = httpServletRequest.getSession();
		String transactionId = httpReq.getParameter("tx");
		String responseMessage = httpReq.getParameter("st");
		String amountString = httpReq.getParameter("amt");
		String ord = httpReq.getParameter("item_number");
		String currencyCode = httpReq.getParameter("cc");
		long orderId = Long.parseLong(ord);
		String paymentStatus = null;
		Order order = null;
		String inr = EECConstants.INR;
		String usd = EECConstants.USD;

		// String amount = doubleFormat.format(total);
		double amounts = Double.parseDouble(amountString);
		// int amounts = Integer.parseInt(amountString);
		NumberFormat doubleFormat = NumberFormat
				.getNumberInstance(Locale.ENGLISH);
		String inrAmount = doubleFormat.format(EECUtil
				.findExchangeRateAndConvert(usd, inr, amounts));
		PayPalOrderTracker paypal = new PayPalOrderTrackerImpl();
		try {
			paypal.setCompanyId(themeDisplay.getCompanyId());
			paypal.setAmount(inrAmount);
			paypal.setUserId(themeDisplay.getUserId());
			paypal.setTransactionId(transactionId);
			paypal.setOrderId(orderId);
			paypal.setCreateDate(new Date());
			paypal.setCurencyCode(currencyCode);
			paypal.setResponseMessage(responseMessage);

			PayPalOrderTrackerLocalServiceUtil.addPayPalOrderTracker(paypal);

			try {
				order = OrderLocalServiceUtil.getOrder(orderId);
			} catch (Exception e1) {
			}

			if (responseMessage.equalsIgnoreCase(EECConstants.COMPLETED)) {
				paymentStatus = EECConstants.PAYMENT_SUCCESSFUL;
			}
			Date expectedDeliveryDate = expectedDeliveryDate(orderId);

			OrderLocalServiceUtil.updateOrder(order, EECConstants.PAYPAL,
					paymentStatus, null);
			saveLatestOrder(actionRequest);

			OrderItemLocalServiceUtil.updateOrderItemStatusByOrder(
					order.getOrderId(), EECConstants.ORDER_STATUS_PROCESSING,
					null, expectedDeliveryDate, null, 0l, false);
			forwardCheckout(actionRequest, actionResponse);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateStock(long orderId) throws Exception {
		List<OrderItem> results = OrderItemLocalServiceUtil
				.getOrderItems(orderId);
		for (OrderItem result : results) {
			ProductInventory inventory = ProductInventoryLocalServiceUtil
					.getProductInventory(Long.valueOf(result
							.getProductInventoryId()));
			long stockQuantity = 0l;
			stockQuantity = (inventory.getQuantity() - result.getQuantity());
			inventory.setQuantity(stockQuantity);
			ProductInventoryLocalServiceUtil.updateProductInventory(inventory);
		}
	}

	public Cart updateWalletAmount(ActionRequest actionRequest, long userId,
			long companyId) throws PortalException, SystemException,
			DocumentException {
		Double walletAmount = ParamUtil.getDouble(actionRequest,
				"walletAmount", 0d);
		Cart cart = EECUtil.getCart(actionRequest);
		Coupon coupon = cart.getCoupon();
		Order order = OrderLocalServiceUtil.getLatestOrder(userId, companyId);
		double grandTotal = EECUtil.calculateTotal(companyId, userId,
				cart.getProductinventories(), order.getShippingCountry(),
				order.getShippingState(), coupon, cart.getAltShipping(),
				cart.isInsure());
		return CartLocalServiceUtil.updateWalletAmount(cart.getCartId(),
				userId, walletAmount, grandTotal);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String cmd = ParamUtil.getString(resourceRequest, EECConstants.CMD);
		if (cmd.equals(EECConstants.CAPTCHA)) {
			try {
				CaptchaUtil.serveImage(resourceRequest, resourceResponse);
			} catch (Exception e) {
				_log.info(e, e);
			}
		} else {
			ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest
					.getAttribute(WebKeys.THEME_DISPLAY);
			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();
			double myWalletAmount = 0;
			try {
				myWalletAmount = EECUtil.myWalletAmount(themeDisplay
						.getUserId());
			} catch (SystemException e) {
				_log.info(e, e);
			}
			jsonObject.put("retVal", myWalletAmount);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(jsonObject.toString());
			writer.flush();
		}

	}

	private static Log _log = LogFactoryUtil.getLog(CartAction.class);
}