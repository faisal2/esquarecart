package com.esquare.ecommerce.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.esquare.ecommerce.model.ProductDetails;
import com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class AutocompleteData {

	private int totalproducts;

	private List<String> products;

	private static AutocompleteData autocompleteInstance = null;

	public synchronized static AutocompleteData getInstance(long companyId) {
		if (autocompleteInstance == null) {
			autocompleteInstance = new AutocompleteData(companyId);
		}
		return autocompleteInstance;
	}

	public AutocompleteData(long companyId) {
		try {
			/*List<Catalog> list = CatalogLocalServiceUtil.getCatalogs(companyId);
			catalog = new ArrayList<String>();
			for (Catalog parentCategory : list) {
				int subCount = CatalogLocalServiceUtil.getCatalogsCount(
						parentCategory.getCompanyId(),
						parentCategory.getCatalogId());
				List<Catalog> subCategories = CatalogLocalServiceUtil
						.getCatalogs(parentCategory.getCompanyId(),
								parentCategory.getCatalogId(), 0, subCount);
				if (subCategories.size() == 0)
					continue;

				for (int i = 0; i < subCategories.size(); i++) {
					Catalog subCategory = (Catalog) subCategories.get(i);

					catalog.add(subCategory.getName());
				}
			}*/
			
			
			List<ProductDetails> list = ProductDetailsLocalServiceUtil.getProductDetailList(companyId);
			products = new ArrayList<String>();
			for (ProductDetails productDetails : list) {
				products.add(productDetails.getName());
				}

		} catch (Exception e) {
			_log.info(e.getClass());
		}

		totalproducts = products.size();
	}

	public List<String> getData(String query) {
		String category_temp = null;
		query = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for (int i = 0; i < totalproducts; i++) {
			category_temp = products.get(i).toLowerCase();
			if (category_temp.startsWith(query)) {
				matched.add(products.get(i));
			}
		}
		Collections.sort(matched);
		return matched;
	}

	public List<String> getData() {
		return products;
	}
	private static Log _log = LogFactoryUtil.getLog(AutocompleteData.class);
}