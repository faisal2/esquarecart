/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Instance;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Instance in entity cache.
 *
 * @author Esquare
 * @see Instance
 * @generated
 */
public class InstanceCacheModel implements CacheModel<Instance>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{companyId=");
		sb.append(companyId);
		sb.append(", instanceUserId=");
		sb.append(instanceUserId);
		sb.append(", createdDate=");
		sb.append(createdDate);
		sb.append(", expiryDate=");
		sb.append(expiryDate);
		sb.append(", packageType=");
		sb.append(packageType);
		sb.append(", currentTemplate=");
		sb.append(currentTemplate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Instance toEntityModel() {
		InstanceImpl instanceImpl = new InstanceImpl();

		instanceImpl.setCompanyId(companyId);
		instanceImpl.setInstanceUserId(instanceUserId);

		if (createdDate == Long.MIN_VALUE) {
			instanceImpl.setCreatedDate(null);
		}
		else {
			instanceImpl.setCreatedDate(new Date(createdDate));
		}

		if (expiryDate == Long.MIN_VALUE) {
			instanceImpl.setExpiryDate(null);
		}
		else {
			instanceImpl.setExpiryDate(new Date(expiryDate));
		}

		if (packageType == null) {
			instanceImpl.setPackageType(StringPool.BLANK);
		}
		else {
			instanceImpl.setPackageType(packageType);
		}

		if (currentTemplate == null) {
			instanceImpl.setCurrentTemplate(StringPool.BLANK);
		}
		else {
			instanceImpl.setCurrentTemplate(currentTemplate);
		}

		instanceImpl.resetOriginalValues();

		return instanceImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		companyId = objectInput.readLong();
		instanceUserId = objectInput.readLong();
		createdDate = objectInput.readLong();
		expiryDate = objectInput.readLong();
		packageType = objectInput.readUTF();
		currentTemplate = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(instanceUserId);
		objectOutput.writeLong(createdDate);
		objectOutput.writeLong(expiryDate);

		if (packageType == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(packageType);
		}

		if (currentTemplate == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(currentTemplate);
		}
	}

	public long companyId;
	public long instanceUserId;
	public long createdDate;
	public long expiryDate;
	public String packageType;
	public String currentTemplate;
}