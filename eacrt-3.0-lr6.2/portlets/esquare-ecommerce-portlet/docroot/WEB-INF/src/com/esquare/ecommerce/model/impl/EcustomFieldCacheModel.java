/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.EcustomField;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing EcustomField in entity cache.
 *
 * @author Esquare
 * @see EcustomField
 * @generated
 */
public class EcustomFieldCacheModel implements CacheModel<EcustomField>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{customFieldRecId=");
		sb.append(customFieldRecId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", xml=");
		sb.append(xml);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EcustomField toEntityModel() {
		EcustomFieldImpl ecustomFieldImpl = new EcustomFieldImpl();

		ecustomFieldImpl.setCustomFieldRecId(customFieldRecId);
		ecustomFieldImpl.setCompanyId(companyId);

		if (title == null) {
			ecustomFieldImpl.setTitle(StringPool.BLANK);
		}
		else {
			ecustomFieldImpl.setTitle(title);
		}

		if (xml == null) {
			ecustomFieldImpl.setXml(StringPool.BLANK);
		}
		else {
			ecustomFieldImpl.setXml(xml);
		}

		ecustomFieldImpl.resetOriginalValues();

		return ecustomFieldImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		customFieldRecId = objectInput.readLong();
		companyId = objectInput.readLong();
		title = objectInput.readUTF();
		xml = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(customFieldRecId);
		objectOutput.writeLong(companyId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (xml == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(xml);
		}
	}

	public long customFieldRecId;
	public long companyId;
	public String title;
	public String xml;
}