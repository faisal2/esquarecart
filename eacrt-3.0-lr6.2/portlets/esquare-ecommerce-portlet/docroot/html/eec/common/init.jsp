<%@page import="java.text.DecimalFormatSymbols"%>
<%@page import="com.esquare.ecommerce.service.CancelledOrderItemsLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>

<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="javax.portlet.PortletRequest"%>
<%@ page import="javax.portlet.PortletModeException"%>
<%@ page import="javax.portlet.PortletMode"%>
<%@ page import="javax.portlet.WindowStateException"%>
<%@ page import="javax.portlet.WindowState"%>
<%@ page import="javax.portlet.RenderRequest"%>
<%@ page import="javax.portlet.RenderResponse" %>
<%@ page import="javax.portlet.ResourceRequest" %>
<%@ page import="javax.portlet.ResourceResponse" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="javax.portlet.PortletSession"%>
<%@ page import="javax.portlet.ActionRequest"%>

<%@ page import="com.liferay.portlet.PortletURLUtil" %>
<%@ page import="com.liferay.portlet.PortletURLFactoryUtil"%>

<%@ page import="com.liferay.portal.util.PortletKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.captcha.CaptchaTextException"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.bean.BeanParamUtil" %>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@ page import="com.liferay.portal.kernel.language.UnicodeLanguageUtil"%>
<%@ page import="com.liferay.portal.kernel.util.StringBundler" %>
<%@ page import="com.liferay.portal.kernel.util.UnicodeFormatter" %>
<%@ page import="com.liferay.portal.kernel.util.LocalizationUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PrefsParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ArrayUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ page import="com.liferay.portal.kernel.util.PropsKeys"%>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Constants"%>
<%@ page import="com.liferay.portal.kernel.util.StringPool" %>
<%@ page import="com.liferay.portal.kernel.util.ListUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@ page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@ page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@ page import="com.liferay.portlet.dynamicdatamapping.model.DDMTemplate"%>

<%@ page import="com.liferay.portal.kernel.dao.search.SearchContainer" %>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>

<%@ page import="com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.Projection"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.OrderFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>

<%@ page import="com.esquare.ecommerce.util.EECConstants"%>
<%@ page import="com.esquare.ecommerce.util.PortletPropsValues"%>

<%@ page import="com.esquare.ecommerce.model.EcustomField" %>
<%@ page import="com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil" %>

<%@ page import="com.esquare.ecommerce.service.CartPermissionLocalServiceUtil"%>
<%@ page import="com.esquare.ecommerce.model.CartPermission"%>

<%@ page import="com.esquare.ecommerce.userview.util.UserViewUtil"%>
<%@ page import="com.esquare.ecommerce.util.EECUtil"%>

<%@ page import="com.esquare.ecommerce.model.Catalog" %>
<%@ page import="com.esquare.ecommerce.service.CatalogLocalServiceUtil" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.HashSet"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.Currency"%>
<%@page import="java.util.HashMap"%>

<%@ page import="java.text.NumberFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DecimalFormat"%>

<%@page import="com.liferay.portal.NoSuchRoleException"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
 
<%
themeDisplay=(ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);

WindowState windowState = null;
PortletMode portletMode = null;

PortletURL currentURLObj = null;

if (renderRequest != null) {
	windowState = renderRequest.getWindowState();
	portletMode = renderRequest.getPortletMode();

	currentURLObj = PortletURLUtil.getCurrent(renderRequest, renderResponse);
}
else if (resourceRequest != null) {
	windowState = resourceRequest.getWindowState();
	portletMode = resourceRequest.getPortletMode();

	currentURLObj = PortletURLUtil.getCurrent(resourceRequest, resourceResponse);
}

String currentURL = currentURLObj.toString();

PortletPreferences preferences = null;

if (renderRequest != null) {
	preferences = renderRequest.getPreferences();
}
String ctxPath = request.getContextPath();
if (ctxPath != null && ctxPath.length()==1) {
	ctxPath = "";
}
 String currencyType=PortletPropsValues.DEFAULT_CURRENCY_TYPE;
String currencyPref=EECUtil.getPreference(themeDisplay.getCompanyId(), "currencyType");
if(Validator.isNotNull(currencyPref)){
	currencyType=currencyPref;
} 
Currency currency = Currency.getInstance(currencyType);

NumberFormat percentFormat = NumberFormat.getPercentInstance(locale);

NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);

DecimalFormatSymbols dfs = new DecimalFormatSymbols();
dfs.setCurrency(currency);
if (currency.getSymbol().equalsIgnoreCase("INR")) {
	String rupeeSymbol = "Rs.";
	/* replace with your symbol here*/
	dfs.setCurrencySymbol(rupeeSymbol);
}
((DecimalFormat)currencyFormat).setDecimalFormatSymbols(dfs);
currencyFormat.setMinimumFractionDigits(2);
currencyFormat.setMaximumFractionDigits(2);
currencyFormat.setCurrency(currency);

DecimalFormat dformat = new DecimalFormat("#.00"); 

boolean isShopAdmin = false;
try{
isShopAdmin = RoleLocalServiceUtil.hasUserRole(themeDisplay.getUserId(),themeDisplay.getCompanyId(),PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE,true);
}catch(NoSuchRoleException e){
}

%>


<script type="text/javascript"src="<%=request.getContextPath()%>/js/eec/common/main.js"></script>
<!--  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/search/jquery.ui.js"></script>
