<%@ include file="/html/eec/common/coupon/init.jsp"%>
<%
ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
Coupon coupon = (Coupon) row.getObject();
String couponId = String.valueOf(coupon.getPrimaryKey());
boolean activated=coupon.getActive();
%>
<portlet:renderURL var="editURL">
<portlet:param name="jspPage" value="/html/eec/common/coupon/add_coupon.jsp" />
<portlet:param name="couponId" value="<%=couponId%>" />
<portlet:param name="redirect" value="<%=themeDisplay.getURLCurrent()%>" />
</portlet:renderURL>
<portlet:actionURL var="deactivateURL">
<portlet:param name="<%=EECConstants.CMD %>" value="<%=EECConstants.DEACTIVATE%>" />
<portlet:param name="couponId" value="<%=couponId%>" />
</portlet:actionURL>
<portlet:actionURL var="activateURL">
<portlet:param name="<%=EECConstants.CMD %>" value="<%=EECConstants.PUBLISH%>" />
<portlet:param name="couponId" value="<%=couponId%>" />
</portlet:actionURL>
<liferay-ui:icon-menu>
<c:if test='<%=activated%>'>
	<liferay-ui:icon image="deactivate" message="deactivate"
		url='<%= deactivateURL %>'  />
		<liferay-ui:icon image="edit" message="Edit"
		url='<%= editURL %>'  />
		</c:if>
		<c:if test='<%=!activated%>'>
		<liferay-ui:icon image="activate" message="activate"
		url='<%= activateURL %>'  />
		
		</c:if>
	
</liferay-ui:icon-menu>