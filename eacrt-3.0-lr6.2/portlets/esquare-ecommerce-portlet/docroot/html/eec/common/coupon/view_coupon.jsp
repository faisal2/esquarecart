<%@ include file="/html/eec/common/coupon/init.jsp"%>
<%
List<Coupon> coup = CouponLocalServiceUtil.getCoupons(-1, -1);
List<Coupon> coupons=(List<Coupon>)request.getAttribute(EECConstants.SEARCH_LIST);
if(Validator.isNull(coupons)){
	String couponCode= ParamUtil.getString(request, EECConstants.COUPON_CODE);
	String couponName= ParamUtil.getString(request, EECConstants.COUPON_NAME);
	String discountType = ParamUtil.getString(request, EECConstants.DISCOUNT_TYPE);
	boolean isAndOperator=ParamUtil.getBoolean(request, EECConstants.AND_OPERATOR,true);
	boolean active =ParamUtil.getBoolean(request, EECConstants.ACTIVE,true);
	coupons=CouponLocalServiceUtil.searchCoupon(themeDisplay.getScopeGroupId(), themeDisplay.getCompanyId(), couponCode,couponName,discountType, isAndOperator, active);
}
PortletURL editURL = renderResponse.createRenderURL();
editURL.setParameter("jspPage", "/html/eec/common/coupon/add_coupon.jsp");
editURL.setParameter("redirect", themeDisplay.getURLCurrent());
%>
<portlet:renderURL var="addCouponURL">
<portlet:param name="redirect" value="<%=themeDisplay.getURLCurrent()%>" />
<portlet:param name="jspPage" value="/html/eec/common/coupon/add_coupon.jsp" />
</portlet:renderURL>

 <div class="dashboard-maindiv2" >
 <div class="dashboard-headingstyle">Coupons </div>
<hr style="margin-top: 13px;">
<liferay-util:include page="/html/eec/common/coupon/coupon_search.jsp" servletContext="<%=this.getServletContext() %>"	/>

</div>

<aui:form  method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden" />
	<aui:input name="deleteCouponIds" type="hidden" />
	
	
<div id="dbcoupon-viewcouponmaindiv">
<liferay-ui:search-container  id="dbcoupon-searchresult" delta="5" emptyResultsMessage="Sorry. There are no items to display." rowChecker="<%=new RowChecker(renderResponse) %>" >
	<liferay-ui:search-container-results total="<%=coupons.size()%>"
		results="<%=ListUtil.subList(coupons, searchContainer.getStart(),
						searchContainer.getEnd())%>" />
	
	<liferay-ui:search-container-row modelVar="coupon"
		className="com.esquare.ecommerce.model.Coupon" 	keyProperty="couponId">
		<%
		editURL.setParameter("couponId", String.valueOf(coupon.getCouponId()));
		long couponUsedCount = OrderLocalServiceUtil.getCouponUsedCount(themeDisplay.getCompanyId(),
				coupon.getCouponCode());
         %>
		<liferay-ui:search-container-column-text name="Code" property="couponCode" href="<%=editURL.toString()%>" />
			<liferay-ui:search-container-column-text name="CouponName" 	property="couponName" href="<%=editURL.toString()%>" />
		<liferay-ui:search-container-column-text name="Description" property="description" href="<%=editURL.toString()%>" />
		<liferay-ui:search-container-column-text name="StartDate" href="<%=editURL.toString()%>" >
		<fmt:formatDate value="<%=coupon.getStartDate()%>" 	pattern="dd-MM-yyyy" />
		</liferay-ui:search-container-column-text>
		<c:choose>
		<c:when test="<%= coupon.getEndDate() != null %>">
	<liferay-ui:search-container-column-text name="Expiration" href="<%=editURL.toString()%>" >
		<fmt:formatDate value="<%=coupon.getEndDate()%>"
					pattern="dd/MM/yyyy" />
		</liferay-ui:search-container-column-text>
		</c:when>
		<c:otherwise>
			<liferay-ui:search-container-column-text name="Expiration" href="<%=editURL.toString()%>" value='<%=LanguageUtil.get(pageContext, "never")%>'/>	
			</c:otherwise>
		</c:choose>
		<liferay-ui:search-container-column-text name="DiscountType" property="discountType" href="<%=editURL.toString()%>" />
		<liferay-ui:search-container-column-text name="Coupon Used Count"  href="<%=editURL.toString()%>" value="<%=String.valueOf(couponUsedCount)%>"/>
		<liferay-ui:search-container-column-jsp name="Action"
			path="/html/eec/common/coupon/coupon_action.jsp" />	
	</liferay-ui:search-container-row>

	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
</liferay-ui:search-container>
</div>

</aui:form>
</div>
<div align="center">
<aui:button-row>
<%if(coup.size() > 0){ %>
		<aui:button value="Delete" onClick='<%= renderResponse.getNamespace() + "deleteCoupons();" %>' cssClass="coupondelete" />
	<%} %>	
		<aui:button  value="Add Coupon" href="<%=addCouponURL.toString()%>" cssClass="couponadd"/>
	
	</aui:button-row>
	</div>
<aui:script>
	Liferay.provide(
		window,
		'<portlet:namespace />deleteCoupons',
		function() {
					document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=EECConstants.DELETE%>";
					var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, "<portlet:namespace />allRowIds");
					if(checkBoxValue==""||checkBoxValue==null){
							alert("Please select atleast One entry to Delete");
							return false;
					}
					if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-you-want-to-delete-the-selected-coupons") %>')) {
					document.<portlet:namespace />fm.<portlet:namespace />deleteCouponIds.value=checkBoxValue;
				submitForm(document.<portlet:namespace />fm, "<portlet:actionURL/>");
			}
		},
		['liferay-util-list-fields']
	);
	
</aui:script>

<style>
.column-content, .column-content-center {
    padding: 0;
}



</style>