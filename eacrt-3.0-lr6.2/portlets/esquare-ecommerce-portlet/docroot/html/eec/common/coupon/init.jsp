<%@ include file="/html/eec/common/init.jsp" %>

<%@ page import="com.esquare.ecommerce.model.Coupon"%>
<%@ page import="com.esquare.ecommerce.service.CouponLocalServiceUtil"%>
 
<%@ page import="com.esquare.ecommerce.DuplicateCouponCodeException"%>
<%@ page import="com.esquare.ecommerce.CouponStartDateException"%>
<%@ page import="com.esquare.ecommerce.CouponNameException"%>
<%@ page import="com.liferay.portlet.shopping.CouponMinimumOrderException"%>
<%@ page import="com.esquare.ecommerce.CouponEndDateException"%>
<%@ page import="com.liferay.portlet.shopping.CouponDiscountException"%>
<%@ page import="com.esquare.ecommerce.CouponDescriptionException"%>
<%@ page import="com.esquare.ecommerce.CouponDateException"%>
<%@ page import="com.esquare.ecommerce.CouponCodeException"%>
<%@ page import="com.esquare.ecommerce.service.OrderLocalServiceUtil"%>
<%@ page import="com.esquare.ecommerce.model.ProductDetails"%>
<%@ page import="com.esquare.ecommerce.model.Catalog"%>
<%@ page import="com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil"%>
<%@ page import="com.esquare.ecommerce.service.CatalogLocalServiceUtil"%>
