<%@ include file="/html/eec/common/coupon/init.jsp"%>
		<%
		String redirect = ParamUtil.getString(request, "redirect");
		String couponIdString=request.getParameter("couponId");
		long couponId=0l;
		Coupon coupon=null;
		boolean couponLimitChk=false;
		boolean userLimitChk=false;
		try{
			couponId = Long.parseLong(couponIdString);
		 coupon=CouponLocalServiceUtil.getCoupon(couponId);
		}catch(Exception exception){}
		Double discountActual= 0.0;
		Double discountPercentage = 0.0;
		String couponCode = BeanParamUtil.getString(coupon, request, "couponCode");
		String discountType = BeanParamUtil.getString(coupon, request, "discountType");
		String discountLimit = BeanParamUtil.getString(coupon, request, "discountLimit");
		Double discount = BeanParamUtil.getDouble(coupon, request, "discount");
		int couponLimit = BeanParamUtil.getInteger(coupon, request, "couponLimit");
		int userLimit = BeanParamUtil.getInteger(coupon, request, "userLimit");
		boolean active = BeanParamUtil.getBoolean(coupon, request, "active",true);
		boolean neverExpire = BeanParamUtil.getBoolean(coupon, request, "neverExpire",true);
		
		if (couponLimit>0) {
			couponLimitChk = true;
		}
		if (userLimit>0) {
			userLimitChk = true;
		}
		
		if(discountType.equals(EECConstants.DISCOUNT_TYPE_ACTUAL)){
			discountActual=discount;
		}
		if(discountType.equals(EECConstants.DISCOUNT_TYPE_PERCENTAGE)){
			discountPercentage=discount;
		}
		if (coupon != null) {
			if (coupon.getEndDate() != null) {
				neverExpire = false;
			}
		}
		List<ProductDetails> productList = null;
		List<Catalog> catalogList = null;
		productList = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(), -1, -1);
		
		try {
			productList = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(), -1, -1);
			catalogList = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId());
		} 	
		catch (SystemException e1) {}
		catch(Exception e){}
		%>
		<portlet:actionURL var="editCouponURL">
		</portlet:actionURL>
		
<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle" id="dashboard-maindiv" >Add Coupons</div>
	<hr style="margin-top: 13px;">
		<aui:form name="fm" method="POST" action="<%=editCouponURL.toString()%>" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveCoupon();" %>'>
		<aui:input name="<%= EECConstants.CMD %>" type="hidden" />
		<aui:input name="couponId" type="hidden" value="<%= couponId %>" />
		<aui:model-context bean="<%= coupon %>" model="<%=Coupon.class %>" />
		
	<liferay-ui:error exception="<%= CouponCodeException.class %>" message="please-enter-a-valid-code" />
	<liferay-ui:error exception="<%= CouponDateException.class %>" message="please-enter-a-start-date-that-comes-before-the-expiration-date" />
	<liferay-ui:error exception="<%= CouponDescriptionException.class %>" message="please-enter-a-valid-description" />
	<liferay-ui:error exception="<%= CouponDiscountException.class %>" message="please-enter-a-valid-number" />
	<liferay-ui:error exception="<%= CouponEndDateException.class %>" message="please-enter-a-valid-expiration-date" />
	<liferay-ui:error exception="<%= CouponMinimumOrderException.class %>" message="please-enter-a-valid-number" />
	<liferay-ui:error exception="<%= CouponNameException.class %>" message="please-enter-a-valid-name" />
	<liferay-ui:error exception="<%= CouponStartDateException.class %>" message="please-enter-a-valid-start-date" />
	<liferay-ui:error exception="<%= DuplicateCouponCodeException.class %>" message="please-enter-a-unique-code" />
<%-- 		<liferay-ui:panel-container extended="<%= true %>" id="discount" persistState="<%= true %>">		 --%>
		<aui:fieldset>
		<aui:input  name="redirect" type="hidden" value="<%=redirect%>" />
		<c:choose>
		<c:when test="<%= coupon == null %>">
		<aui:input name="couponCode" showRequiredLabel="false" label='Coupon Code<span style="color:red;font-size: 14px;">*</span>' >
		<aui:validator name="required"/>
		</aui:input>
		<aui:button value="autogenerate-code" name="autoCode"  onClick="randString();" />
		</c:when>
		<c:otherwise>
				<aui:field-wrapper label="Coupon Code">
						<liferay-ui:input-resource url="<%= couponCode %>" />
				</aui:field-wrapper>
			</c:otherwise>
		</c:choose>
		<aui:input  name="couponName" showRequiredLabel="false" label='Coupon Name<span style="color:red;font-size: 14px;">*</span>'>
		<aui:validator name="required"/>
		</aui:input>
		<aui:input  name="couponLimitChk" type="checkbox" value="<%=couponLimitChk%>"  />
		<aui:input name="couponLimit" label="">
		<aui:validator name="custom" errorMessage="Please enter coupon limit">
		function (val, fieldNode, ruleValue) {
				var reg = /^\d+$/;
				var couponLimitChk = document.getElementById("<portlet:namespace />couponLimitChkCheckbox").checked; 
					return !(couponLimitChk&&(val.length == 0||val=="0" || !reg.test(val)));
				}
		</aui:validator>
		</aui:input>
		<aui:input  name="userLimitChk" type="checkbox" value="<%=userLimitChk%>" />
		<aui:input name="userLimit" label=""  >
		<aui:validator name="custom" errorMessage="Please enter user limit">
		function (val, fieldNode, ruleValue) {
				var reg = /^\d+$/;
				var userLimitChk = document.getElementById("<portlet:namespace />userLimitChkCheckbox").checked;
				return !(userLimitChk&&(val.length == 0||val=="0" || !reg.test(val)));
				}
		</aui:validator>
		</aui:input>
		<aui:input name="description"  showRequiredLabel="false" label='Description<span style="color:red;font-size: 14px;">*</span>'>
		<aui:validator name="required"/>
		</aui:input>
		<aui:input name="startDate" />
		<aui:input  dateTogglerCheckboxLabel="never-expire" disabled="<%= neverExpire %>" name="endDate" />
		
		<aui:input  name="active" type="checkbox" value="<%=active%>" />
		</aui:fieldset >
		
		<aui:select name="discountType" >
		<% for(int i = 0; i < EECConstants.DISCOUNT_TYPES.length; i++) {%>
			<aui:option label="<%= EECConstants.DISCOUNT_TYPES[i] %>" selected="<%= discountType.equals(EECConstants.DISCOUNT_TYPES[i]) %>" />
		</aui:select>
		<%} %>
		<aui:fieldset id="discActual">
			<aui:input  name="discountActual" type="text" value="<%=discountActual%>">
			<aui:validator name="custom" errorMessage="Please enter valid discount Value">
			function (val, fieldNode, ruleValue) {
			var discountType=A.one('#<portlet:namespace/>discountType').val();
			if(discountType=='<%=EECConstants.DISCOUNT_TYPE_ACTUAL%>')
					{
					var regexp =/^[+\-]?(\d+([.,]\d+)?)+$/;
			return ((val.search(regexp) != -1)&&val>0);
					}
				return true;
			}
			</aui:validator>
			</aui:input>
		</aui:fieldset>
		<aui:fieldset id="discPercentage"> 
			<aui:input name="discountPercentage" type="text" value="<%=discountPercentage%>" label='Discount(%)<span style="color:red;font-size: 14px;">*</span>'>
			<aui:validator name="custom" errorMessage="please enter valid discount value,should be between 0 to 100">
			function (val, fieldNode, ruleValue) {
			var discountType=A.one('#<portlet:namespace/>discountType').val();
			if(discountType=='<%=EECConstants.DISCOUNT_TYPE_PERCENTAGE%>'){
			var regexp =/^[+\-]?(\d+([.,]\d+)?)+$/;
			return ((val.search(regexp) != -1)&&(val>0 && val<100));
			}
			return true;
			}
			</aui:validator>
			</aui:input>
		</aui:fieldset>
		<aui:input name="minOrder" label='Minimum Order<span style="color:red;font-size: 14px;">*</span>'>
		<aui:validator name="custom" errorMessage="Please enter valid min order">
		function (val, fieldNode, ruleValue) {
		var regexp =/^[+\-]?(\d+([.,]\d+)?)+$/;
			return ((val.search(regexp) != -1)&&val>0);
		}
		</aui:validator>
		</aui:input>
		<aui:fieldset id="discLimit"> 
		<aui:select  name="discountLimit" >
		<% for(int i = 0; i < EECConstants.discountLimit.length; i++) {%>
							<aui:option label="<%= EECConstants.discountLimit[i] %>" selected="<%= discountLimit.equals(EECConstants.discountLimit[i]) %>" />
						</aui:select>
		<%} %>
		</aui:fieldset>			
	<div id="<portlet:namespace />product_list_div">
				<label><liferay-ui:message key="limit-products"/> </label>
				<select id="<portlet:namespace />limitProducts" name="<portlet:namespace />limitProducts" multiple="multiple" class="countrySpec-class" >
				   <%
				   for(ProductDetails productDetails: productList) {
					   String select = "";
					   if(Validator.isNotNull(coupon) && coupon.getLimitProducts().contains(productDetails.getName()))select="selected";
				   %>
				    <option value="<%=productDetails.getName()%>" <%=select %> ><%=productDetails.getName() %></option>
				   <% }%>
				</select>
</div>

<div id="<portlet:namespace />catalog_list_div">
				<label><liferay-ui:message key="limit-catalogs"/></label>
				<select id="<portlet:namespace />limitCatalogs" name="<portlet:namespace />limitCatalogs" multiple="multiple" class="countrySpec-class" >
				   <%
				   for(Catalog catalog : catalogList) {
					   String select = "";
					   if(Validator.isNotNull(coupon) && coupon.getLimitCatalogs().contains(catalog.getName()))select="selected";
				   %>
				    <option value="<%=catalog.getName()%>" <%=select%> ><%=catalog.getName() %></option>
				   <% }%>
				</select>
</div>
<div align="center">
	<span>	
	<aui:button type="submit" value="<%= (coupon == null) ? EECConstants.ADD : EECConstants.UPDATE %>" cssClass="submitcoupon"/></span>
	<aui:button value="Cancel" href="<%=redirect%>"  style="margin-left: 0px;"  cssClass="cancelcoupon1"/>

</div>
</aui:form>
</div>

<script>
$(function(){
	$('select#<portlet:namespace />limitProducts').tokenize();
	$('select#<portlet:namespace />limitCatalogs').tokenize();
});

</script>


<aui:script use="aui-base">
Liferay.provide(
window,
'<portlet:namespace />saveCoupon',
function() {
		document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=(coupon == null) ? EECConstants.ADD : EECConstants.UPDATE %>";
		submitForm(document.<portlet:namespace />fm);
	});
	
 AUI().ready('aui-base','event','node',function(A) {
		var couponLimitCheckbox= A.one('#<portlet:namespace/>couponLimitChkCheckbox'),
			couponLimit=A.one('#<portlet:namespace/>couponLimit'),
			endDate=A.one('#<portlet:namespace/>endDate'),
			userLimitCheckbox=A.one('#<portlet:namespace/>userLimitChkCheckbox'),
			neverExpireCheckbox=A.one('#<portlet:namespace/>neverExpireCheckbox'),
			userLimit=A.one('#<portlet:namespace/>userLimit'),
			discLimit= A.one('#<portlet:namespace/>discLimit'),
			discountLimit= A.one('#<portlet:namespace/>discountLimit'),
			product_list_div=A.one('#<portlet:namespace/>product_list_div'),
			catalog_list_div=A.one('#<portlet:namespace/>catalog_list_div'),
			limitProducts=A.one('#<portlet:namespace/>limitProducts'),
			limitCatalogs=A.one('#<portlet:namespace/>limitCatalogs'),
			discountType=A.one('#<portlet:namespace/>discountType'),
			discountActual=A.one('#<portlet:namespace/>discActual'),
			discountPercentage=A.one('#<portlet:namespace/>discPercentage'),
			couponLimitCheckboxValue = document.getElementById("<portlet:namespace />couponLimitChkCheckbox").checked,
			userLimitCheckboxValue = document.getElementById("<portlet:namespace />userLimitChkCheckbox").checked;
			<portlet:namespace />showHideCouponLimitValue(couponLimitCheckboxValue);
			<portlet:namespace />showHideUserLimitValue(userLimitCheckboxValue);
			<portlet:namespace />discountTypeEvent(discountType);
			
        couponLimitCheckbox.on('click', function(){   
         var status = document.getElementById("<portlet:namespace />couponLimitChkCheckbox").checked;
         <portlet:namespace />showHideCouponLimitValue(status);
          });
          
            userLimitCheckbox.on('click', function(){   
         var status = document.getElementById("<portlet:namespace />userLimitChkCheckbox").checked;
         <portlet:namespace />showHideUserLimitValue(status);
          });
          
           discountType.on('change', function(){ 
        <portlet:namespace />discountTypeEvent(this);
          });
          
          discountLimit.on('change', function(){ 
         <portlet:namespace />discountLimitEvent(this);
          });
          
         function <portlet:namespace />showHideCouponLimitValue(status){
         if(status){
		         couponLimit.show();
         }else{
         		 couponLimit.hide();
		         couponLimit.val("");
         		 
         } 
     }
      function <portlet:namespace />showHideUserLimitValue(status){
         if(status){
		       userLimit.show();
         }else{
       		   userLimit.hide();
	           userLimit.val("");
         } 
     }
        function <portlet:namespace />discountTypeEvent(discountType){
        
         if(discountType.val()=='<%=EECConstants.DISCOUNT_TYPE_ACTUAL%>'){
         <portlet:namespace />discountLimitEvent(discountLimit);
		          discLimit.show();
		          discountActual.show();
		          discountPercentage.val("");
		          discountPercentage.hide();
         }
         else if(discountType.val()=='<%=EECConstants.DISCOUNT_TYPE_PERCENTAGE%>')
         {  
         <portlet:namespace />discountLimitEvent(discountLimit);      
		          discLimit.show();
		          discountPercentage.show();
		          discountActual.val("");
		          discountActual.hide();
         }
         else if((discountType.val()=='<%=EECConstants.DISCOUNT_TYPE_TAX_FREE%>')||(discountType.val()=='<%=EECConstants.DISCOUNT_TYPE_FREE_SHIPPING%>'))
         {
		          discountPercentage.hide();
		          discountActual.hide();
		          discountPercentage.val("");
		          discountActual.val("");
		          discLimit.hide();
		          product_list_div.hide();
		          catalog_list_div.hide();
         }
        }  
        
        function <portlet:namespace />discountLimitEvent(discountLimit){
        
        if(discountLimit.val()=='All Orders'){
		          product_list_div.hide();
		          catalog_list_div.hide();
         }
         else if(discountLimit.val()=='Specific Catalog')
         {
		         product_list_div.hide();
		         catalog_list_div.show();
         }
         else if(discountLimit.val()=='Specific Product')
         {
		         product_list_div.show();
		         catalog_list_div.hide();
         }
        }
          });
          

</aui:script>

<script>
function randString(n)
{
    if(!n)
    {
        n = 12;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

    for(var i=0; i < n; i++)
    {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    var test = text;
    document.getElementById("<portlet:namespace />couponCode").value = test;
    return text;
} 
</script>
<style>
#_coupon_WAR_esquareecommerceportlet_autoCode {
    background: #45a5d5 none repeat scroll 0 0;
    border: 0 none;
    color: white;
    font-family: myfont;
    font-size: larger;
    width: 263px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
}
#_coupon_WAR_esquareecommerceportlet_autoCode:hover{
	background-color: #CDDC39;
}

</style>