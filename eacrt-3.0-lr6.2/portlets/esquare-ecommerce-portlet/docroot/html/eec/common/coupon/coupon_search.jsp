<%@ include file="/html/eec/common/coupon/init.jsp"%>

<%
String couponCode= ParamUtil.getString(request, EECConstants.COUPON_CODE);
String couponName= ParamUtil.getString(request, EECConstants.COUPON_NAME);
String discountType = ParamUtil.getString(request, EECConstants.DISCOUNT_TYPE);
boolean isAndOperator=ParamUtil.getBoolean(request, EECConstants.AND_OPERATOR,true);
boolean active =ParamUtil.getBoolean(request, EECConstants.ACTIVE,true);
%>

<portlet:actionURL var="searchURL" />

<div class="dbcoupon-couponmain">
<aui:form action="<%= searchURL.toString() %>" method="post" name="fm1">
	<aui:fieldset>
    <aui:column>
	<aui:select  name="<%= EECConstants.AND_OPERATOR %>"  >
			<aui:option  selected="<%=isAndOperator %>" value="<%=isAndOperator %>" class="dbcoupon-coupon"  ><%=isAndOperator?"All":"Any"%></aui:option>
			<aui:option  selected="<%= !isAndOperator %>" value="<%=!isAndOperator %>"  class="dbcoupon-coupon" ><%=!isAndOperator?"All":"Any"%></aui:option>
		</aui:select>
	</aui:column>
	</aui:fieldset>
<aui:fieldset>
	
		<aui:input  name="<%=EECConstants.COUPON_CODE %>" size="20" type="text" value="<%=couponCode %>"  class="dbcoupon-coupon"  />
		<aui:input name="<%=EECConstants.CMD %>" type="hidden" value="<%=EECConstants.SEARCH %>" />
	
	
		<aui:input  name="<%=EECConstants.COUPON_NAME %>" size="20" type="text" value="<%=couponName %>"  class="dbcoupon-coupon"  />
	
		<aui:select name="<%= EECConstants.DISCOUNT_TYPE %>" showEmptyOption="<%= true %>" >

			<%
			for (int i = 0; i < EECConstants.DISCOUNT_TYPES.length; i++) {
			%>
				<aui:option label="<%= EECConstants.DISCOUNT_TYPES[i] %>" selected="<%= discountType.equals(EECConstants.DISCOUNT_TYPES[i]) %>" />
			<%
			}
			%>

		</aui:select>


	<aui:column>
		<aui:select name="<%= EECConstants.ACTIVE %>" >
			<aui:option  selected="<%=active%>" value="<%=active %>"  ><%=active?"Yes":"No"%></aui:option>
			<aui:option  selected="<%=!active%>" value="<%=!active%>" ><%=!active?"Yes":"No" %></aui:option>
		</aui:select>
	</aui:column>
	
</aui:fieldset>
	
	<div id="dbcoupon-searchdiv" class="dashboard-buttons">
		<aui:button type="submit" value="Search" cssClass="couponsearch" />
	</div>


</aui:form>
</div>
<aui:script>
	<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
		Liferay.Util.focusFormField(document.<portlet:namespace />fm1.<portlet:namespace /><%= EECConstants.COUPON_CODE %>);
	</c:if>
</aui:script>

<style>
fieldset, .form fieldset {
    margin-bottom: 0;
}

</style>