<%@ include file="/html/eec/common/main_package_billing/init.jsp"%>
<portlet:actionURL var="returnURLEbs">
	<portlet:param name="<%=EECConstants.CMD%>"
		value="<%=EECConstants.EBS_RESPONSE%>" />
</portlet:actionURL>
<portlet:renderURL var="searchURL" />
<%
	String returnURL = returnURLEbs.toString() + "&DR={DR}";
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_id");
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_count");
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_pos");
	
	Long companyId = ParamUtil.getLong(request, "companyId", 0l);
	boolean isSearch = ParamUtil.getBoolean(request, "isSearch",false);
	Instance instance = null;
	Company companyCustomer = null;
	PaymentUserDetail paymentUserDetail= null;
	List<EcartPackageBilling> ecartPackageBillings = null;
	double currentAmount = 0d;
	double dueAmount = 0d;
	double totalAmount = 0d;
	long billId = 0l;

	try {
		instance = InstanceLocalServiceUtil.getInstance(companyId);
		companyCustomer = CompanyLocalServiceUtil.getCompany(companyId);
		paymentUserDetail = PaymentUserDetailLocalServiceUtil
				.findByCompanyId(companyId);
		ecartPackageBillings = EcartPackageBillingLocalServiceUtil
				.findByCompanyTransactionId(companyId, 0);
		if (ecartPackageBillings.size() > 0) {
			currentAmount = ecartPackageBillings.get(0).getAmount();
			billId = ecartPackageBillings.get(0).getBillId();
			for (int i = 1; i < ecartPackageBillings.size(); i++) {
				dueAmount += ecartPackageBillings.get(i).getAmount();
			}
			totalAmount = currentAmount + dueAmount;
		}
	} catch (Exception exception) {
	}

	
	String md5HashData = PortletPropsValues.EBS_SECRET_KEY;
	String paymentMode = PortletPropsValues.EBS_PAYMENT_MODE;
	String accountNumber = PortletPropsValues.EBS_ACCOUNT_ID;
	md5HashData += '|' + accountNumber + '|' + totalAmount + '|'
			+ billId + '|' + returnURL + '|' + paymentMode;
	//
	
%>

<aui:form name="fm" action="<%=searchURL.toString()%>" method="POST">
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<aui:input name="companyId" label="Company Id">
				<aui:validator name="required" />
				<aui:validator name="digits" />
			</aui:input>
			<aui:input name="isSearch" value="<%=true%>" type="hidden" />
		</aui:column>
		<aui:column columnWidth="25">
			<aui:button value="Submit" type="submit" />
		</aui:column>
	</aui:layout>
</aui:form>
<c:choose>
	<c:when	test="<%=companyId > 0 && Validator.isNotNull(companyCustomer)&& Validator.isNotNull(instance)%>" >
		<aui:layout>
	<aui:column columnWidth="100" first="true">
	Please verify your account details and payment amount due:
		</aui:column>
</aui:layout>
		<aui:layout>
			<aui:column columnWidth="25" first="true">
					Web Id:
		</aui:column>
			<aui:column columnWidth="75" last="true">
				<%=companyCustomer.getWebId()%>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="25" first="true">
					Company Id:
		</aui:column>
			<aui:column columnWidth="75" last="true">
				<%=companyId%>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="25" first="true">
					Package Type:
		</aui:column>
			<aui:column columnWidth="75" last="true">
				<%=instance.getPackageType()%>
			</aui:column>
		</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Name:
		</aui:column>
	<aui:column columnWidth="25">
		<%=(Validator.isNotNull(paymentUserDetail))?paymentUserDetail.getName():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Company Id:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=companyId%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Package Type:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=instance.getPackageType()%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					latest Bill Id:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=billId%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Previous Due:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		Rs.<%=dueAmount%>/-
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Current Invioce:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		Rs.<%=currentAmount%>/-
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Total Amount:
		</aui:column>
	<aui:column columnWidth="75" last="true">
		Rs.<%=totalAmount%>/-
	</aui:column>
</aui:layout>
<c:if test="<%=totalAmount>0%>">
<aui:layout>
By clicking on 'Pay Now' below you are agreeing to the Terms and conditions.
</aui:layout>
<aui:form action="https://secure.ebs.in/pg/ma/sale/pay" name="fm"
	method="post" id="fm">
	<input name="account_id" type="hidden" value="<%=accountNumber%>">
	<input name="email" type="hidden" value="<%=paymentUserDetail.getEmail()%>">
	<input name="name"  type="hidden" value="<%=paymentUserDetail.getName()%>">
	<input name="city"  type="hidden" value="<%=paymentUserDetail.getCity()%>">
	<input name="address"  type="hidden" 
		value="<%=Validator.isNotNull(paymentUserDetail.getAddress()) ? paymentUserDetail.getAddress():"Not Applicable"%>">
	<input name="phone"  type="hidden" value="<%=paymentUserDetail.getPhoneNo()%>">
	<input name="postal_code"  type="hidden"
		value="<%=paymentUserDetail.getPinCode()%>">
	<input name="return_url" type="hidden"  value="<%=returnURL%>">
	<input name="mode" type="hidden" value="<%=paymentMode%>">
	<input name="reference_no" type="hidden" value='<%=billId%>'>
	<input name="secure_hash" type="hidden" 
		value="<%=EBSUtil.md5(md5HashData)%>" />
	<input name="description" type="hidden"
		value="<%=instance.getPackageType()%>">
	<input name="country" type="hidden" id="country"
		value="<%=paymentUserDetail.getCountry()%>">
	<input name="state" type="hidden" id="state"
		value="<%=paymentUserDetail.getState()%>">
	<input name="amount" type="hidden" value="<%=totalAmount%>" />
	<aui:button-row>
		<aui:button type="submit" name="submit" value="Pay Now" />
	</aui:button-row>
</aui:form>
</c:if>
 </c:when>
	<c:when test="<%=isSearch%>">
Your Search return No Result
</c:when>
</c:choose>