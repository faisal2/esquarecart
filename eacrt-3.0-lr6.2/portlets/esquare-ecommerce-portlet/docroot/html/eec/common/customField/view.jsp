<%@ include file="/html/eec/common/customField/init.jsp" %>
<%
//themeDisplay.setIncludeServiceJs(true);
String redirect = PortalUtil.getCurrentURL(request);
List<EcustomField> ecustomFieldlist=EcustomFieldLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId());

PortletURL actionURL = renderResponse.createActionURL();

PortletURL addURL = renderResponse.createRenderURL();
addURL.setParameter("jspPage","/html/eec/common/customField/addCustomField.jsp");

PortletURL editURL = renderResponse.createRenderURL();
editURL.setParameter("jspPage","/html/eec/common/customField/editCustomField.jsp");

%>


<div class="dashboard-maindiv2">
<aui:form method="post" name="frm">
<div class="dashboard-headingstyle">Custom Fields</div>
<hr style="margin-top: 13px;">
<div id="dbcustom-buttons" style="padding-left: 20px;">
		<input type="button" value="Add" class="ecartaddbuttoncustom" onClick="location.href='<%=addURL.toString()%>';" />
	
	<c:if test="<%=ecustomFieldlist.size()>0%>">
		<input type="button" onClick='<%= renderResponse.getNamespace() + "deleteItem();" %>' value="Delete"  class="ecartaddbuttoncustom" />
	</c:if>
</div>


<aui:input name="<%=EECConstants.CMD%>" type="hidden" />
<aui:input name="deleteFieldIds" type="hidden" />

		<liferay-ui:search-container emptyResultsMessage="There is No Custom Field Records" delta="10" rowChecker="<%=new RowChecker(renderResponse) %>">
		<liferay-ui:search-container-results total="<%=ecustomFieldlist.size()%>"
		results="<%=ListUtil.subList(ecustomFieldlist, searchContainer.getStart(),
						searchContainer.getEnd())%>" />
		<liferay-ui:search-container-row modelVar="ecustomField" className="com.esquare.ecommerce.model.EcustomField" keyProperty="customFieldRecId">
											
				<liferay-ui:search-container-column-text 
				name="title"/>
				<%editURL.setParameter("RECID",String.valueOf(ecustomField.getCustomFieldRecId())); %>
				<liferay-ui:search-container-column-text 
				name="" href="<%= editURL.toString() %>" value="Edit"/>
			
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
</liferay-ui:search-container>
</aui:form>
</div>
<aui:script>
Liferay.provide(
		window,
		'<portlet:namespace />deleteItem',
		function() {
					document.<portlet:namespace />frm.action="<%= actionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
					var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />frm, "<portlet:namespace />allRowIds");
					if(checkBoxValue==""||checkBoxValue==null){
							alert("Please select atleast One entry to Delete");
							return false;
					}
					if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "Deleting this entry  will reflect in product General Features \n Are you sure you want to delete the selected field's?") %>')) {
					document.<portlet:namespace />frm.<portlet:namespace />deleteFieldIds.value=checkBoxValue;
					submitForm(document.<portlet:namespace />frm);
			}
		},
		['liferay-util-list-fields']
	);
</aui:script>
<style>
.ecartaddbuttoncustom{
	background-color: #ffc107;
    border: 0 none;
    color: white;
    font-family: myfont !important;
    font-size: medium !important;
    height: 35px;
    min-width: 110px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
}
.ecartaddbuttoncustom:hover{
background-color: #cddc39;
}

</style>