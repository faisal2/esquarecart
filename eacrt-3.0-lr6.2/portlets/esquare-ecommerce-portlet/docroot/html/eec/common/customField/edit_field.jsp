<%@ include file="/html/eec/common/customField/init.jsp" %>

<%
int index = ParamUtil.getInteger(renderRequest, "index", GetterUtil.getInteger((String)request.getAttribute("configuration.jsp-index")));
int formFieldsIndex = GetterUtil.getInteger((String)request.getAttribute("configuration.jsp-formFieldsindex"));
boolean fieldsEditingDisabled = GetterUtil.getBoolean((String)request.getAttribute("configuration.jsp-fieldsEditingDisabled"));
List<ShippingRegion> customFieldList=(List<ShippingRegion>)request.getAttribute("CUSTOMFIELDS_LIST");
String recIdParam=(String)request.getAttribute("RECID");
String fieldLabelXml = LocalizationUtil.getLocalizationXmlFromPreferences(preferences, renderRequest, "fieldLabel" + formFieldsIndex);
String fieldLabel = LocalizationUtil.getLocalization(fieldLabelXml, themeDisplay.getLanguageId());
String fieldType = PrefsParamUtil.getString(preferences, renderRequest, "fieldType" + formFieldsIndex);
boolean fieldOptional = PrefsParamUtil.getBoolean(preferences, renderRequest, "fieldOptional" + formFieldsIndex);
String fieldOptionsXml = LocalizationUtil.getLocalizationXmlFromPreferences(preferences, renderRequest, "fieldOptions" + formFieldsIndex);
String fieldOptions = LocalizationUtil.getLocalization(fieldOptionsXml, themeDisplay.getLanguageId());
String fieldValidationScript = StringPool.BLANK;
String fieldValidationErrorMessage = StringPool.BLANK;


if(Validator.isNotNull(recIdParam)){
	try{
		//EcustomField field = (EcustomField) EcustomFieldLocalServiceUtil.findByCompanyIdFieldOrder(themeDisplay.getCompanyId(), index);
		//fieldLabel = field.getKeyValue();
		//fieldType = field.getKeyType();
		fieldLabel=customFieldList.get(index-1).getName();
		fieldType = customFieldList.get(index-1).getKeyValue();
	//	fieldOptions = field.getOptionsValue();
	} catch(Exception e){
		e.printStackTrace();
	}
}else{
	fieldLabel = "";
	fieldType = "";
	fieldOptions = "";
}
%>

<div id="infields">
<div class="field-row field-row">
<div >
	<c:choose>
		<c:when test="<%= !fieldsEditingDisabled %>">
			<aui:input type="hidden" name='<%= "_field" + index %>' />
			<aui:field-wrapper cssClass="label-name" ><div id="eecartcustom-name"> Name </div>
				<aui:input id="dbcustom-namefiled" type="text" name='<%= "fieldLabel" + index %>' value='<%= fieldLabel %>' label="" maxlength='35'>
					<aui:validator name="required" errorMessage="Custom field is required" />
					<aui:validator name="custom"  errorMessage="Please enter alpha only">
				        function (val, fieldNode, ruleValue) {
				                var returnValue = true;
				                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>?0123456789";
				                for (var i = 0; i < val.length; i++) {
				                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
				                     returnValue = false;
				                    }
				                }
				                return returnValue;
				        }
				    </aui:validator>
				</aui:input>
			</aui:field-wrapper>
		</c:when>
		<c:otherwise>
			<dl class="editing-disabled">
				<dt>
					<liferay-ui:message key="name" />
				</dt>
				<dd>
					<%= fieldLabel %>
				</dd>
		</c:otherwise>
	</c:choose>
</div>




<div id="type">

	<c:choose>
		<c:when test="<%= !fieldsEditingDisabled %>" >
			<aui:select label="type" name='<%= "fieldType" + index %>' class="eecartcustom-select" >
				<aui:option selected='<%= fieldType.equals("text") %>' value="text" label="Text Box"/>
				<aui:option selected='<%= fieldType.equals("textarea") %>' value="textarea" label="Text Area"/>
			</aui:select>
		</c:when>
		<c:otherwise>
				<dt>
					<liferay-ui:message key="type" />
				</dt>
				<dd>
					<liferay-ui:message key="<%= fieldType %>" />
				</dd>
		</c:otherwise>
	</c:choose>
	
</div>
<div id="options">
	<c:choose>
		<c:when test="<%= !fieldsEditingDisabled %>">
			<aui:input cssClass="optional-control" inlineLabel="left" label="optional" name='<%= "fieldOptional" + index %>' type="checkbox" value="<%= fieldOptional %>" />
		</c:when>
		<c:otherwise>
				<dt>
					<liferay-ui:message key="optional" />
				</dt>
				<dd>
					<liferay-ui:message key='<%= fieldOptional ? "yes" : "no" %>' />
				</dd>
		</c:otherwise>
	</c:choose>

	<c:choose>
		<c:when test="<%= !fieldsEditingDisabled %>">
			<aui:field-wrapper cssClass='<%= "options" + ((Validator.isNull(fieldType) || (!fieldType.equals("options") && !fieldType.equals("radio"))) ? " hide" : StringPool.BLANK) %>' helpMessage="add-options-separated-by-commas" label="options">
				<input type="text" name="<portlet:namespace /><%= "fieldOptions" + index %>" value="<%= fieldOptions %>"/>
			</aui:field-wrapper>
		</c:when>
		<c:when test="<%= Validator.isNotNull(fieldOptions) %>">
				<dt>
					<liferay-ui:message key="options" />
				</dt>
				<dd>
					<%= fieldOptions %>
				</dd>
		</c:when>
	</c:choose>
</div>
	<c:if test="<%= false %>">
		<c:choose>
			<c:when test="<%= !fieldsEditingDisabled %>">
				<div class="validation">
					<liferay-ui:error key='<%= "invalidValidationDefinition" + index %>' message="please-enter-both-the-validation-code-and-the-error-message" />

					<aui:a cssClass="validation-link" href="javascript:;"><liferay-ui:message key="validation" /> &raquo;</aui:a>

					<div class='validation-input <%= Validator.isNull(fieldValidationScript) ? "hide" : "" %>'>
						<aui:column columnWidth="50">
							<aui:input cssClass="validation-script" cols="80" label="validation-script" name='<%= "fieldValidationScript" + index %>' style="width: 95%" type="textarea" value="<%= fieldValidationScript %>" wrap="off" />

							<aui:input cssClass="lfr-input-text-container" cols="80" label="validation-error-message" name='<%= "fieldValidationErrorMessage" + index %>' size="80" value="<%= fieldValidationErrorMessage %>" />
						</aui:column>
					</div>
				</div>
			</c:when>
			<c:when test="<%= Validator.isNotNull(fieldValidationScript) %>">
					<dt class="optional">
						<liferay-ui:message key="validation" />
					</dt>
					<dd>
						<pre><%= fieldValidationScript %></pre>
					</dd>
					<dt class="optional">
						<liferay-ui:message key="validation-error-message" />
					</dt>
					<dd>
						<%= fieldValidationErrorMessage %>
					</dd>
			</c:when>
			<c:otherwise>
					<dt class="optional">
						<liferay-ui:message key="validation" />
					</dt>
					<dd>
						<liferay-ui:message key="this-field-does-not-have-any-specific-validation" />
					</dd>
			</c:otherwise>
		</c:choose>
	</c:if>

	<c:if test="<%= fieldsEditingDisabled %>">
		</dl>
	</c:if>
</div></div>



<style>
.field-input, .field-labels-top .field-input{
height: 29px;
    margin-left: 11px;
    margin-top: 14px;
    padding-top: 4px;
    }
</style>