<%@ include file="/html/eec/common/customField/init.jsp" %>

<%
boolean fieldsEditingDisabled = false;
PortletURL actionURL = renderResponse.createActionURL();

PortletURL backURL = renderResponse.createRenderURL();

backURL.setParameter("jspPage", "/html/eec/common/customField/view.jsp");
%>
<liferay-portlet:actionURL portletConfiguration="true"
	 var="configurationURL" />
	 
	 <div class="dashboard-maindiv2">
	<div class="dashboard-headingstyle"> Add Custom Fields</div>
	<hr style="margin-top: 13px;">
<aui:form action="<%= configurationURL %>" method="post">
<aui:input type="hidden" name="companyId" value="<%=themeDisplay.getCompanyId() %>" id="companyId"/>
<aui:input type="hidden" name="<%=EECConstants.CMD %>" value="<%=EECConstants.ADD %>"/>
	<div style="margin-left: 20px;">
	<aui:input  cssClass="lfr-input-text-container" name="title" type="text" maxlength='35' label='Title<span style="color:red;font-size: 14px;">*</span>'>
	<aui:validator name="required" errorMessage="Title field is required" />
	<aui:validator name="custom" errorMessage="Spectial characters Not allowed">
							function (val, fieldNode, ruleValue) {
								var regexp =/^\d*[a-zA-Z][a-zA-Z\d\s%&-]*$/;
								if(val.search(regexp) != -1){
										return true;
								}
								return false;
							}
	</aui:validator>
	</aui:input>
	</div>
	<div id="titleValidateMsg" class="hide">
			<span style="color:red;">Title is already Exist.</span>
	</div>
	<br/>
<div id="custom">
	<liferay-ui:panel-container id="webFormConfiguration"
		extended="<%= Boolean.TRUE %>" persistState="<%= true %>">
		<liferay-ui:panel id='webFormFields'
			title='<%= LanguageUtil.get(pageContext, "formfields") %>'
			collapsible="<%= true %>" persistState="<%= true %>"
			extended="<%= true %>">
			
		<div id="requirdFielddiv">
			<aui:fieldset cssClass="rows-container webFields">

				<aui:input name="updateFields" type="hidden"
					value="<%= !fieldsEditingDisabled %>" />

				<%
					String formFieldsIndexesParam = ParamUtil.getString(renderRequest, "formFieldsIndexes") ;
				
					int[] formFieldsIndexes = null;
	
					if (Validator.isNotNull(formFieldsIndexesParam)) {
						formFieldsIndexes = StringUtil.split(formFieldsIndexesParam, 0);
					}
					else {
						formFieldsIndexes = new int[0];
	
						for (int i = 1; true; i++) {
							String fieldLabel = PrefsParamUtil.getString(preferences, request, "fieldLabel" + i);
	
							if (Validator.isNull(fieldLabel)) {
								break;
							}
	
							formFieldsIndexes = ArrayUtil.append(formFieldsIndexes, i);
						}
	
						if (formFieldsIndexes.length == 0) {
							formFieldsIndexes = ArrayUtil.append(formFieldsIndexes, -1);
						}
					}
	
					int index = 1;
					for (int formFieldsIndex : formFieldsIndexes) {
						request.setAttribute("configuration.jsp-index", String.valueOf(index));
						request.setAttribute("configuration.jsp-formFieldsindex", String.valueOf(formFieldsIndex));
						request.setAttribute("configuration.jsp-fieldsEditingDisabled", String.valueOf(fieldsEditingDisabled));
					%>
				<div class="lfr-form-row"		id="<portlet:namespace/>fieldset<%= formFieldsIndex %>">
				<div class="row-fields"><jsp:include page="edit_field.jsp" />
				</div>
				</div>

				<%
						index++;
					}
					%>

				<aui:input type="hidden"  name="index" value="<%=index %>" />
			</aui:fieldset>
			</div>
		</liferay-ui:panel>
	</liferay-ui:panel-container></div>
	<div id="dbcustom-savebuttons">
	<span class="dashboard-buttons">
	 <aui:button type="submit" id="but" cssClass="submitcoupon"/></span>
	<%-- <aui:button type="button" value="Cancel" href="location.href='<%=backURL.toString()%>';"/> --%>
	<aui:button type="reset" value="Reset" cssClass="cancelcoupon1"/>
	
	</div>
	</aui:form>
	</div>
	
	<aui:script use="liferay-auto-fields">
	var toggleOptions = function(event) {
	
		var select = this;

		var formRow = select.ancestor('.lfr-form-row');
		var value = select.val();

		var optionsDiv = formRow.one('.options');

		if (value == 'options' || value == 'radio') {
			optionsDiv.all('label').show();
			optionsDiv.show();
		}
		else if (value == 'paragraph') {

			// Show just the text field and not the labels since there
			// are multiple choice inputs

			optionsDiv.all('label').hide();
			optionsDiv.show();
		}
		else {
			optionsDiv.hide();
		}

		var optionalControl = formRow.one('.optional-control');
		var labelName = formRow.one('.label-name');

		if (value == 'paragraph') {
			var inputName = labelName.one('input');

			inputName.val('<liferay-ui:message key="paragraph" />');
			inputName.fire('change');

			labelName.hide();
			optionalControl.hide();

			optionalControl.all('input[type="checkbox"]').attr('checked', 'true');
			optionalControl.all('input[type="hidden"]').attr('value', 'true');
		}
		else {
			optionalControl.show();
			labelName.show();
		}
	};

	var toggleValidationOptions = function(event) {
	
		this.next().toggle();
	};

	var webFields = A.one('.webFields');

	webFields.all('select').each(toggleOptions);

	<c:if test="<%=!fieldsEditingDisabled %>">
		A.delegate('change', toggleOptions, webFields, 'select');
		A.delegate('click', toggleValidationOptions, webFields, '.validation-link');

		A.delegate(
			'change',
			function(event) {
			
				var input = event.currentTarget;
				var row = input.ancestor('.field-row');
				var label = row.one('.field-title');

				if (label) {
					label.html(input.get('value'));
				}
			},
			webFields,
			'.label-name input'
		);

		new Liferay.AutoFields(
			{
				contentBox: webFields,
				fieldIndexes: '<portlet:namespace />formFieldsIndexes',
				sortable: true,
				sortableHandle: '.field-label'
			}
		).render();
	</c:if>
</aui:script>

<aui:script use="aui-autocomplete-deprecated,aui-io-request-deprecated">

AUI().ready('aui-base','event','node',function(A) {
				var companyId= document.getElementById('<portlet:namespace />companyId').value;
 	 			var myDivA = A.one('#titleValidateMsg');
          	 	A.one('#<portlet:namespace />title').on('blur', function() {
				var title = document.getElementById('<portlet:namespace />title').value;
				var url = '<portlet:resourceURL/>';
				
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />title: title,
							<portlet:namespace />companyId: companyId,
							<portlet:namespace /><%=EECConstants.CMD%>: "TITLE",
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function() {
								var message = this.get('responseData').retVal;
								if (message) {
									document.getElementById('<portlet:namespace />title').value = "";
									myDivA.show();
								}
								else {
									myDivA.hide();
								}
							}
							
						}
					}
					
				); //END of io Request
			
		},
		['aui-io-deprecated']
	);  
}); 
</aui:script>

<style>
.control-group.form-inline {
    display: none;
}

</style>
