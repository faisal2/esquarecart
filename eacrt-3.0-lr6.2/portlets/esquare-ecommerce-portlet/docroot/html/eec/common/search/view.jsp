<%@ include file="/html/eec/common/search/init.jsp" %>
<%
PortletSession ps=renderRequest.getPortletSession();
if(themeDisplay.getURLCurrent().equals(StringPool.SLASH)){
	ps.removeAttribute("productName",PortletSession.APPLICATION_SCOPE);		
}
String productName=(String) ps.getAttribute("productName",PortletSession.APPLICATION_SCOPE);	
int total = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
List<Catalog> results = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId());
AutocompleteData autoData = AutocompleteData.getInstance(themeDisplay.getCompanyId());
String autoCompleteData = "";
for (int i =0; i <autoData.getData().size(); i++) {
    autoCompleteData = autoCompleteData+ (autoCompleteData.equals("") ? "":",") + autoData.getData().get(i);

}

%>
<portlet:actionURL  var="actionURL">
    <portlet:param name="<%= Constants.CMD %>" value="<%= Constants.ADD %>" />
</portlet:actionURL>
<%
long layoutId = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, "/products").getPlid();
PortletURL detailViewURL = PortletURLFactoryUtil.create(request,EECConstants.USER_VIEW_PORTLET_NAME, layoutId, "ACTION_PHASE");
detailViewURL.setParameter("javax.portlet.action", "navigationProcess");
detailViewURL.setParameter(Constants.CMD, EECConstants.SEARCH);
detailViewURL.setWindowState(WindowState.NORMAL);
detailViewURL.setPortletMode(PortletMode.VIEW);
%>

<aui:form id="search-form"  method="post" name="fm" >
<div class="row-fluid" style="margin-top: 25px;">

<div class="span7" id="ecommercesearch"><aui:input type="text" name="productName"  value="<%=productName %>" label="" size="40" placeHolder="<%=PortletPropsValues.SEARCH_TEXT_PLACEHOLDER%>" /></div>
<div class="span2" id="eec-homesearchbut" >
<aui:button type="submit" value="Search" id="detail_pro_added_to_wishlist" onClick='<%= renderResponse.getNamespace() + "searchProduct();" %>'/> 
</div>

</div>
</aui:form>

<script type="text/javascript">
jQuery(document).ready(function(){
    var autoData = '<%=autoCompleteData%>';
    var data = autoData.split(",");
    $('#search-form').submit(function(e) {
        if (!$('#search').val()) {
          e.preventDefault();
        }       
     });
   jQuery("#<portlet:namespace/>productName").autocomplete({ 
       source:data
    });
});

</script>
<aui:script>
function <portlet:namespace />searchProduct() {
	var productName=document.getElementById("<portlet:namespace />productName").value;
	if((productName!="<%=PortletPropsValues.SEARCH_TEXT_PLACEHOLDER%>") ){
	document.<portlet:namespace />fm.action="<%= detailViewURL.toString()%>" + "&productName=" + productName;
	submitForm(document.<portlet:namespace />fm);
	}
}
</aui:script> 



