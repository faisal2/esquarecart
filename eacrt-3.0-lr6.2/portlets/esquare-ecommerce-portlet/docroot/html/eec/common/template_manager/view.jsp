<%@page import="com.itextpdf.text.log.SysoCounter"%>
<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.esquare.ecommerce.service.TemplateManagerLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.service.InstanceLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.Instance"%>
<%@page import="com.esquare.ecommerce.model.TemplateManager"%>

<%@page import="com.liferay.portal.security.permission.PermissionChecker"%>
<%@page import="com.liferay.portal.service.LayoutSetLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.LayoutSet"%>
<%@page import="com.liferay.portal.model.GroupConstants"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.GroupLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.NoSuchFileEntryException"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolder"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>

<%@page import="javax.portlet.RenderResponse"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.List"%>

<script src="<%=request.getContextPath()%>/js/eec/common/template/jquery.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/eec/common/template/highslide.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/eec/common/template/highslide-with-gallery.js"></script>
<%
String dir=request.getContextPath()+"/images/eec/common/template/template_gallery/";
%>
<script type="text/javascript">
hs.graphicsDir = '<%=dir%>';
hs.align = 'center';
hs.transitions = ['expand', 'crossfade'];
hs.outlineType = 'glossy-dark';
hs.wrapperClassName = 'dark';
hs.fadeInOut = true;
hs.dimmingOpacity = 0.75;

// Add the controlbar
if (hs.addSlideshow) hs.addSlideshow({
	//slideshowGroup: 'group1',
	interval: 5000,
	repeat: false,
	useControls: true,
	fixedControls: 'fit',
	overlayOptions: {
		opacity: .6,
		position: 'bottom center',
		hideOnMouseOut: true
	}
});

</script>

<%
	String plns = "";
	if (renderRequest != null) {
		plns = renderResponse.getNamespace();
	}
	
	PortletURL applylarURL = renderResponse.createActionURL();
	applylarURL.setParameter(ActionRequest.ACTION_NAME, "applylar");
	
	PortletURL imageShowURL = renderResponse.createRenderURL();
	imageShowURL.setParameter("jspPage", "/html/eec/common/template_manager/images_show.jsp");
	imageShowURL.setWindowState(LiferayWindowState.EXCLUSIVE);
	
	PortletURL portletURL = renderResponse.createRenderURL();
	String tabs1 = ParamUtil.getString(request, "tabs1",PortletPropsValues.PACKAGE_TYPE_FREE);
	portletURL.setParameter("tabs1", tabs1);
	
	String curTempName = "";
	String pakType = "";
	if(themeDisplay.getCompanyId()!= EECConstants.DEFAULT_COMPANY_ID){
	Instance instance = InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId());
	curTempName = instance.getCurrentTemplate();
	pakType = instance.getPackageType();
	}
	
	long guestGroupId = GroupLocalServiceUtil.getGroup(themeDisplay.getCompanyId(), GroupConstants.GUEST).getGroupId();
	String colorSchemeId = LayoutSetLocalServiceUtil.getLayoutSet(guestGroupId, false).getColorSchemeId();
	
	PortletURL editTemplateURL = renderResponse.createRenderURL();
	editTemplateURL.setParameter(Constants.CMD, Constants.EDIT);
	
	
%>
 <c:if test='<%= SessionMessages.contains(renderRequest, "request_processed") %>'>
	<div class="portlet-msg-success">
				<liferay-ui:message key='<%= HtmlUtil.escape((String)SessionMessages.get(renderRequest, "request_processed")) %>'/>
		</div>
</c:if>
<portlet:renderURL var="templateURL">
	<portlet:param name="jspPage" value="/html/eec/common/template_manager/add_template.jsp" />
	<portlet:param name="tabs1" value="<%=tabs1%>" />
	
</portlet:renderURL>

<portlet:actionURL name="tempDelete" var="deleteTempURL">
	<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.DELETE %>" />
</portlet:actionURL>


<div class="dashboard-maindiv2" id="temp1">
<div class="dashboard-pagemainheading70" id="dashboard-maindiv" > Template Manager</div>


<aui:form name="frm" action="<%=applylarURL %>" method="post">

<!-- 	<span class="dashboardsetting-emailtab" > -->

	<div id="dashboardsetting-id">
	<span class="dashboardsetting-emailtab" >
	 <%-- <liferay-ui:tabs 
	     names='<%=PortletPropsValues.PACKAGE_TYPE_FREE+","+PortletPropsValues.PACKAGE_TYPE_BASIC+","+PortletPropsValues.PACKAGE_TYPE_STANDARD+","+PortletPropsValues.PACKAGE_TYPE_PREMIUM+","+PortletPropsValues.PACKAGE_TYPE_ENTERPRISE%>'
	     portletURL="<%= portletURL %>"
	     tabsValues="<%=tabs1%>" param="tabs1"
	     refresh="true"> --%>
	  
	<c:if test="<%= permissionChecker.isOmniadmin() %>">
		<div style="padding-left:511px;">
			<input type='checkbox' name="<%=plns %>check" onClick='<%=plns %>checkAll(this);'/>
			<button onClick="<%= plns %>confirmDel('delmultiple');" class="adminview"/>Delete</button>
			<aui:button value="Add Template" onClick="<%= templateURL%>" style=" vertical-align: top; background-color: #45a5d5; border: 0 none; text-shadow: none; color: white; height: 38px; width: 120px;" />
			
			<%-- <img src='<%=request.getContextPath() %>/images/eec/common/product/delete.png' alt="Search" width='68px' height='32px' onClick="<%= plns %>confirmDel('delmultiple');"/> --%>
			<%-- <button onClick="<%= templateURL%>" value="Add Template" class="adminview1"/>Add Template</button> --%>
		</div>
		<br /><br />
	</c:if>
	
		<% 
	        List<TemplateManager> templatList =TemplateManagerLocalServiceUtil.getTemplateManager(tabs1);
			DLFileEntry image = null;
		%>	
		
			<ul class="vslide" >
				
				<% 
				for (TemplateManager tlist : templatList) {
					try {
						image = DLFileEntryLocalServiceUtil.getFileEntry(PortletPropsValues.GROUP_ID, tlist.getFolderId(), PortletPropsValues.TEMPLATE_IMAGE+"1");
					} catch (NoSuchFileEntryException e) {}
					if(image==null) continue;
					
					String prefixURL = EECConstants.PREFIXURL+PortletPropsValues.DEVELOPMENT_DEFAULT_PORTAL_URL;
					if (PortletPropsValues.CURRENT_WORKING_ENVIRONMENT.equals(EECConstants.PRODUCTION)) {
						prefixURL = EECConstants.PREFIXURL+PortletPropsValues.DEFAULT_PORTAL_URL;
					}
				System.out.println("prefixURLprefixURLprefixURL::::::"+prefixURL);
					
					String imageURL = prefixURL +"/documents/" + image.getGroupId() + "/" + image.getFolderId() + "/" + image.getTitle()+"/"+image.getUuid();
					applylarURL.setParameter("templateName", tlist.getLarName()); 
					applylarURL.setParameter("cssId", tlist.getCssId());
					
					imageShowURL.setParameter("folderId", String.valueOf(tlist.getFolderId()));
					String popup = "javascript:popup('" + imageShowURL.toString() + "');";
					
					editTemplateURL.setParameter("tempId", String.valueOf(tlist.getRecId()));
				%>
				<li><div class="highslide-gallery">
					<div>
						<div id="schemeTitle"><%= tlist.getTitle()%> </div><br/>
						<c:if test="<%= permissionChecker.isOmniadmin() %>">
							<input type='checkbox' name="<%=plns %>deleteItem" value='<%= tlist.getRecId()%>' onClick='<%=plns %>checkAllRev(this);' style="position: relative; bottom: 145px;"/>
						</c:if>
						
						<a href="<%=imageURL%>" rel="" class="highslide" onclick="return hs.expand(this)"><img width="140" height="200" src="<%=imageURL%>" id="testt" class="testt1"/></a></div><br/>
						<c:if test="<%=!permissionChecker.isOmniadmin()  %>">
							<c:choose>
								<c:when test="<%= colorSchemeId.equals(tlist.getCssId()) && tlist.getLarName().contains(curTempName)%>">
								<button class="theme1"/><i class="icon-ok"></i>&nbsp;Selected Theme</button>
										 <!-- <span style="color:orange;font-weight:bold;">Selected Theme</span>  -->
										<div class="selectedtheme"> </div>
								</c:when>
								<c:otherwise>
								<%if(EECUtil.getPackageValue(pakType)>=EECUtil.getPackageValue(tlist.getPackageType())){ %>
								<a href="<%= applylarURL.toString() %>"><input type="button" value="Apply" class="productdelete" onClick href="<%= applylarURL.toString() %>"/></a>
 							<%-- <a href="<%= applylarURL.toString() %>"><img width="100" height="30" src="<%=request.getContextPath()%>/images/eec/common/template/button.png" style="margin-left:55px;" onclick="sucess('<%=image.getUuid()%>');"/></a> --%> 
										<%-- <span style="padding-left: 30px;">	<aui:button type="button" value="Apply" class="dashboard-buttons" href="<%=applylarURL.toString()%>" /> </span> --%>
								<%}%>
								</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="<%= permissionChecker.isOmniadmin() %>">
						<a href="<%= editTemplateURL %>" ><img src="<%=ctxPath%>/images/eec/common/catalog/edit.png"/>&nbsp;&nbsp;Edit</a>
						</c:if>
						<div id="<%=image.getUuid() %>" style="display: none;width: 115px; margin-top: 0px; position: relative; top: 12px; left: 48px; height: 0px;color:orange;"><b>In Progress...</b></div>
					</div>
				</li>
					 
				<% } %>
			</ul>
		
	<%-- </liferay-ui:tabs> --%>
</span>
	</div>	
	
	
</aui:form>
</div>
<script>
	
	function sucess(param) {
		document.getElementById(param).style.display="block";
		 var getsucess = document.getElementById("success").innerHTML;
		document.getElementById("sucess1").innerHTML=getsucess;
		alert(this.src);
	}
	
	function <portlet:namespace/>checkAll(obj) {
		getCheckAll(obj,"<portlet:namespace/>");
	}	
	function <portlet:namespace/>checkAllRev() {
		getCheckAllRev("<portlet:namespace/>",document.<portlet:namespace/>frm.<portlet:namespace/>check);
	}
	function <portlet:namespace/>confirmDel(param) {
		msg = "Are you sure to delete the record?";
		if(param == "delmultiple") {
			if(getCheckedCount("<portlet:namespace/>")==0){
				alert("No Items are selected !");
			} else {
				msg = "Are you sure to delete the record(s)?";
				if(confirm(msg)) {
					document.<portlet:namespace/>frm.action="<%= deleteTempURL.toString()%>";
					document.<portlet:namespace/>frm.submit();
				}
			}
		} 
	}
</script>

<style type="text/css">
.vslide li {
    display: inline-block;
    vertical-align: top;
}
.panel-hd {
	display:none;
}
.yui3-widget-content-expanded {
background:transperent;
}

.dialog-content {
    border: none;
}

.autocomplete-results-content, .calendar-content, .colorpicker-content, .dialog-content, .overlaycontext-content {
    background: none repeat scroll 0 0 transparent;
}

.menu-content, body > .lfr-menu-list ul, .underlay-content, .dialog-content {
box-shadow:none;
}

.dialog-content, .date-picker-content, .overlaycontextpanel-content, .tooltip-content, .colorpicker-content, .tree-drag-helper-content {
    box-shadow: none;
}

.dialog-bd {
    overflow-y: visible;
}

.portlet-msg-success, .lfr-message-success{
	 display:none;
	}

.adminview {
    background-color: #45a5d5;
    border: 0 none;
    color: white;
    height: 38px;
    width: 120px;
}

.aui .btn {
background-color: #45a5d5;
background-image: none;
}

</style>