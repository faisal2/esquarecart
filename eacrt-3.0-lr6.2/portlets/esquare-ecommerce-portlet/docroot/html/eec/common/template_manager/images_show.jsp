<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.liferay.portlet.documentlibrary.service.DLAppServiceUtil"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="java.util.List"%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/eec/common/template/fotorama.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/eec/common/template/fotorama.js"></script>

<% 
	long folderId=Long.valueOf( ParamUtil.getString(request, "folderId"));
	List<FileEntry> fileEntries =DLAppServiceUtil.getFileEntries(PortletPropsValues.GROUP_ID, folderId);
%>	
	 
<div id="imagediv" class="fotorama" style="display:none;">
	<%		
	
		for(FileEntry image1:fileEntries){
					String imageURL = "/documents/" + image1.getGroupId() + "/" + image1.getFolderId() + "/" + image1.getTitle()+"/"+image1.getUuid();
	%>
				<img  src="<%=imageURL%>" />
			
	<%	} %>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
  $("#imagediv").delay(500).fadeIn(800);  
  $(".overlay-content.overlaymask-content.yui3-widget-stdmod.yui3-widget-content-expanded").click(function() {
 	  	top.document.getElementById('closethick').click();
  });
});
</script>
