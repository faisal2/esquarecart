<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.esquare.ecommerce.service.TemplateManagerLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.TemplateManager"%>
<%@page import="com.esquare.ecommerce.util.PortletPropsValues"%>
<%@page import="java.io.File"%>

<portlet:actionURL var="addtemplateURL" name="addTemplate"/>

<%
	String templateRecId = (String)request.getAttribute("templateId");
	TemplateManager tempManager = TemplateManagerLocalServiceUtil.getTemplateManager(Long.parseLong(templateRecId));
	request.setAttribute("EditfolderId", String.valueOf(tempManager.getFolderId()));
%>

<h4> <span style="color:#928;">Please do one process at one time(Add / Edit / Delete) </span></h4>

<aui:form action="<%= addtemplateURL %>" method="post" enctype="multipart/form-data">

	<input type="hidden" name="folderId" value="<%= tempManager.getFolderId()%>"/>
	<input type="hidden" name="recId" value="<%= templateRecId%>"/>
	<input type="hidden" name="cmd" value="<%=Constants.EDIT%>"/>
	
	<aui:fieldset label="TemplateManager" column="true">
		<aui:layout>
				<table>
					<tr>
						<td><b>Title:</b></td>
						<td><aui:input type="text" name="title" label="" value='<%= tempManager.getTitle() != null ? tempManager.getTitle() : ""%>'/></td>
					</tr>
					<tr>
						<td><b>LAR Name:</b></td>
						<td>
							<aui:select name="larName" label="">
								<%
								String selectedlar = tempManager.getLarName();
								final File folder = new File(PortletPropsValues.DEFAULT_TEMPLATE_LAR_FOLDER_PATH);
								String adminTemplate = PortletPropsValues.ADMIN_TEMPLATE_LAR_NAME;
								for (final File fileEntry : folder.listFiles()) { 
									if(fileEntry.getName().contains(".lar")) {
									if(!adminTemplate.contains(fileEntry.getName())){ %>
									<aui:option value="<%= fileEntry.getName() %>" selected ='<%= selectedlar.equalsIgnoreCase(fileEntry.getName()) ? true :false %>' ><%= fileEntry.getName() %></aui:option>
								<% } } } %>
							</aui:select>
						</td>
					</tr>
					<tr>
						<td><b>Template Description:</b></td>
						<td><aui:input type="text" name="templateDesc" label=""  value='<%= tempManager.getTemplateDesc() != null ? tempManager.getTemplateDesc() : ""  %>' /></td>
					</tr>
					<tr>
						<td><b>Package:</b></td>
						<td>
							<% String  packageType = tempManager.getPackageType();%>
							<aui:select name="packageType" label="">
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPE_FREE %>" label="<%= PortletPropsValues.PACKAGE_TYPE_FREE%>" selected='<%= packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_FREE) ? true :false %>' />
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPE_BASIC%>" label="<%= PortletPropsValues.PACKAGE_TYPE_BASIC%>" selected='<%= packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_BASIC) ? true :false %>' />
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPE_STANDARD%>" label="<%= PortletPropsValues.PACKAGE_TYPE_STANDARD%>" selected='<%= packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_STANDARD) ? true :false %>' />
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPE_PREMIUM%>" label="<%= PortletPropsValues.PACKAGE_TYPE_PREMIUM%>" selected='<%= packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_PREMIUM) ? true :false %>' />
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPE_ENTERPRISE%>" label="<%= PortletPropsValues.PACKAGE_TYPE_ENTERPRISE%>" selected='<%= packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_ENTERPRISE) ? true :false %>' />
							</aui:select>
						</td>
					</tr>
					<tr>
						<td><b>CSS Id:</b></td>
						<td><% String[] colorSchemeId = PortletPropsValues.TEMPLATE_COLOR_SCHEME; 
							   long cssId =Long.parseLong(tempManager.getCssId());
							%>
							<aui:select name="cssId" label="">
								<% int i;
								for(i=0; i < colorSchemeId.length;i++) { %>
									<aui:option value='<%="0"+(i+1) %>' label='<%=colorSchemeId[i] %>' selected='<%= cssId == 0+(i+1) ? true : false%>'/>
								<%} %>
							</aui:select>
						</td>
					</tr>
				</table>
				<%@ include file="/html/eec/common/template_manager/image_upload.jsp"%>
			
			<aui:button-row>
				<aui:button type="submit" value="Update" />
				<aui:button type="button" value="Cancel" />
			</aui:button-row>
			
		</aui:layout>
	</aui:fieldset>
</aui:form>