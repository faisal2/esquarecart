<%@page import="java.util.List"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.util.PortletPropsValues"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>

<%
	boolean fieldsEditingDisabled = false;
%>
	
	<liferay-ui:panel-container id="webFormConfiguration"
		extended="<%= Boolean.TRUE %>" persistState="<%= true %>">
			<aui:fieldset cssClass="rows-container webFields">
				<aui:input name="updateFields" type="hidden"
					value="<%= !fieldsEditingDisabled %>" />
				<%
					String formFieldsIndexesParam = ParamUtil.getString(renderRequest, "formFieldsIndexes");
					String EditfolderId = (String)request.getAttribute("EditfolderId");
					
					if(Validator.isNotNull(EditfolderId)){
						List<DLFileEntry> dlFileEntry =  null;
						
						try{
							int count = DLFileEntryLocalServiceUtil.getFileEntriesCount(PortletPropsValues.GROUP_ID, Long.parseLong(EditfolderId));
							dlFileEntry = DLFileEntryLocalServiceUtil.getFileEntries(PortletPropsValues.GROUP_ID, Long.parseLong(EditfolderId), 0, count, null);
						}catch(Exception e){
							//
						}
						int i = 1;
						 for(DLFileEntry imageField: dlFileEntry) {
							 formFieldsIndexesParam += imageField.getFileEntryId()+((i<imageField.getSize())? ",":"");
							 i++;
						 }
						
						request.setAttribute("FormFieldsIndexesParam",formFieldsIndexesParam);
					}
					
					
					int[] formFieldsIndexes = null;
					if (Validator.isNotNull(formFieldsIndexesParam)) {
						formFieldsIndexes = StringUtil.split(formFieldsIndexesParam, 0);
					}
					else {
						formFieldsIndexes = new int[0];
	
						for (int i = 1; true; i++) {
							String fieldLabel = PrefsParamUtil.getString(preferences, request, "fieldLabel" + i);
	
							if (Validator.isNull(fieldLabel)) {
								break;
							}
	
							formFieldsIndexes = ArrayUtil.append(formFieldsIndexes, i);
						}
	
						if (formFieldsIndexes.length == 0) {
							formFieldsIndexes = ArrayUtil.append(formFieldsIndexes, -1);
						}
					}
	
					int index = 1;
					for (int formFieldsIndex : formFieldsIndexes) {
						request.setAttribute("configuration.jsp-index", String.valueOf(index));
						request.setAttribute("configuration.jsp-formFieldsindex", String.valueOf(formFieldsIndex));
						request.setAttribute("configuration.jsp-fieldsEditingDisabled", String.valueOf(fieldsEditingDisabled));
					%>
				<div class="lfr-form-row"
					id="<portlet:namespace/>fieldset<%= formFieldsIndex %>">
				<div class="row-fields"><jsp:include page="edit_field.jsp" />
				</div>
				</div>

				<%
						index++;
					}
					%>

				<input type="hidden" id="indexId" name="index" value="<%=index %>" />
			</aui:fieldset>
	</liferay-ui:panel-container>

<aui:script use="liferay-auto-fields">
	var toggleOptions = function(event) {
	
		var select = this;

		var formRow = select.ancestor('.lfr-form-row');
		var value = select.val();

		var optionsDiv = formRow.one('.options');

		if (value == 'options' || value == 'radio') {
			optionsDiv.all('label').show();
			optionsDiv.show();
		}
		else if (value == 'paragraph') {

			// Show just the text field and not the labels since there
			// are multiple choice inputs

			optionsDiv.all('label').hide();
			optionsDiv.show();
		}
		else {
			optionsDiv.hide();
		}

		var optionalControl = formRow.one('.optional-control');
		var labelName = formRow.one('.label-name');

		if (value == 'paragraph') {
			var inputName = labelName.one('input');

			inputName.val('<liferay-ui:message key="paragraph" />');
			inputName.fire('change');

			labelName.hide();
			optionalControl.hide();

			optionalControl.all('input[type="checkbox"]').attr('checked', 'true');
			optionalControl.all('input[type="hidden"]').attr('value', 'true');
		}
		else {
			optionalControl.show();
			labelName.show();
		}
	};

	var toggleValidationOptions = function(event) {
	
		this.next().toggle();
	};

	var webFields = A.one('.webFields');

	webFields.all('select').each(toggleOptions);

	<c:if test="<%=!fieldsEditingDisabled %>">
		A.delegate('change', toggleOptions, webFields, 'select');
		A.delegate('click', toggleValidationOptions, webFields, '.validation-link');

		A.delegate(
			'change',
			function(event) {
			
				var input = event.currentTarget;
				var row = input.ancestor('.field-row');
				var label = row.one('.field-title');

				if (label) {
					label.html(input.get('value'));
				}
			},
			webFields,
			'.label-name input'
		);

		new Liferay.AutoFields(
			{
				contentBox: webFields,
				fieldIndexes: '<portlet:namespace />formFieldsIndexes',
				sortable: true,
				sortableHandle: '.field-label'
			}
		).render();
	</c:if>
</aui:script>