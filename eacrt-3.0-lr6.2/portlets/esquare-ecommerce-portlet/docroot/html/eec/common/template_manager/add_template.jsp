<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.esquare.ecommerce.util.PortletPropsValues"%>
<%@page import="java.io.File"%>
<%
String packageType=ParamUtil.getString(request,"tabs1");
%>
<portlet:actionURL var="addtemplateURL" name="addTemplate"/>

<aui:form action="<%= addtemplateURL %>" method="post" enctype="multipart/form-data">
	<aui:fieldset label="TemplateManager" column="true">
		<aui:layout>
				<table>
					<tr>
						<td><b>Title:</b></td>
						<td><aui:input type="text" name="title" label=""/></td>
					</tr>
					<tr>
						<td><b>LAR Name:</b></td>
						<td>
							<aui:select name="larName" label="">
								<%
								final File folder = new File(PortletPropsValues.DEFAULT_TEMPLATE_LAR_FOLDER_PATH);
								String adminTemplate = PortletPropsValues.ADMIN_TEMPLATE_LAR_NAME;
								for (final File fileEntry : folder.listFiles()) { 
									if(fileEntry.getName().contains(".lar")) {
									if(!adminTemplate.contains(fileEntry.getName())){ %>
									<aui:option value="<%= fileEntry.getName() %>"><%= fileEntry.getName() %></aui:option>
								<% } } } %>
							</aui:select>
						</td>
					</tr>
					<tr>
						<td><b>Template Description:</b></td>
						<td><aui:input type="text" name="templateDesc" label=""  /></td>
					</tr>
					<tr>
						<td><b>Package:</b></td>
						<td>
							<aui:select name="packageType" label="">
							   <%for(int i=0;i<PortletPropsValues.PACKAGE_TYPES.length;i++){ %>
								<aui:option value="<%= PortletPropsValues.PACKAGE_TYPES[i]%>" label="<%= PortletPropsValues.PACKAGE_TYPES[i]%>" selected="<%=packageType.equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPES[i])?true:false%>"/>
								<%} %>
							</aui:select>
						</td>
					</tr>
					<tr>
						<td><b>CSS Id:</b></td>
						<td><% String[] colorSchemeId = PortletPropsValues.TEMPLATE_COLOR_SCHEME; %>
							<aui:select name="cssId" label="">
								<% int i;
								for(i=0; i < colorSchemeId.length;i++) {%>
									<aui:option value='<%="0"+(i+1) %>' label='<%=colorSchemeId[i] %>'/>
								<%} %>
							</aui:select>
						</td>
					</tr>
				</table>
				<%@ include file="/html/eec/common/template_manager/image_upload.jsp"%>
			
			<aui:button-row>
				<aui:button type="submit" value="Save" />
				<aui:button type="reset" value="Reset" />
			</aui:button-row>
			
		</aui:layout>
	</aui:fieldset>
</aui:form>
