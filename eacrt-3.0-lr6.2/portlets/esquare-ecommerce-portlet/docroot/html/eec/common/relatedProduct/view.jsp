<%@ include file="/html/eec/common/relatedProduct/init.jsp" %>

<%
		PortletSession pSession=renderRequest.getPortletSession();
		String eventProductId=(String)pSession.getAttribute("EVENT_PRODUCTID",pSession.PORTLET_SCOPE);
		long productId=0;
		if(Validator.isNotNull(eventProductId)){
			productId=Long.parseLong(eventProductId);
		}
		String 	details =StringPool.BLANK;
		String type=(String)request.getParameter("type");
		long productdetailsPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(
				themeDisplay.getScopeGroupId(), false,EECConstants.PRODUCT_DETAILS_PAGE_FRIENDLY_URL).getPlid();
		PortletURL actionURL=EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productdetailsPlid,
				ActionRequest.ACTION_PHASE, null,"redirectToDetailsPage", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
		
		long productsPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.PRDODUCTS_PAGE_FRIENDLY_URL).getPlid();
		PortletURL userViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productsPlid,
				ActionRequest.ACTION_PHASE, null,"navigationProcess", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
		
		if(productId>0){
		details = UserViewUtil.getRelatedProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),productId,type,themeDisplay.getPathThemeImages(),actionURL,userViewURL,ctxPath,renderRequest);
		} 
	
%>
<aui:form method="post" name="fm">
<%
if(Validator.isNotNull(eventProductId)){%>
	<%=details %>
<%}%>
	
</aui:form>
<%
pSession.removeAttribute("EVENT_PRODUCTID",pSession.PORTLET_SCOPE);
%>



