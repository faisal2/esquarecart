<%@ include file="/html/eec/common/filter/init.jsp" %>
<div id="filterbox">
<%
	long layoutId = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, "/products").getPlid();
	PortletURL userViewURL = PortletURLFactoryUtil.create(request,EECConstants.USER_VIEW_PORTLET_NAME, layoutId, "ACTION_PHASE");
	userViewURL.setParameter("javax.portlet.action", "navigationProcess");
	userViewURL.setWindowState(WindowState.NORMAL);
	userViewURL.setPortletMode(PortletMode.VIEW);
	
	PortletPreferences preference= PrefsPropsUtil.getPreferences(themeDisplay.getCompanyId());
	String priceRangeType=StringPool.BLANK;
	String priceRangeTypePref=EECUtil.getPreference(themeDisplay.getCompanyId(), "priceRangeType");
	if(Validator.isNotNull(priceRangeTypePref)){
		priceRangeType=priceRangeTypePref;
	}
	boolean isdisplayCategory=true;
	String subcatalogIds=StringPool.BLANK;
	LinkedHashMap<String,String> childCatalogMap=new LinkedHashMap<String,String>();
	List<ProductInventory> filterContentList=null;
	PortletSession psession = renderRequest.getPortletSession();	
	String parentCatalogId = (String) psession.getAttribute("parentCatalogId",psession.APPLICATION_SCOPE);	
	 long productId=0;
		String CurProductId = (String) psession.getAttribute("CurProductId",PortletSession.APPLICATION_SCOPE);
		if(Validator.isNotNull(CurProductId)){
			productId=Long.parseLong(CurProductId);
		}
	String productName=(String) psession.getAttribute("productName",psession.APPLICATION_SCOPE);	
	String cmd=(String)psession.getAttribute("FILTER_EVENT_CMD",psession.PORTLET_SCOPE);
	if(Validator.isNotNull(parentCatalogId)) {
		UserViewUtil.getChildCatalogMap(themeDisplay.getCompanyId(),Long.valueOf(parentCatalogId).longValue(), childCatalogMap);
		isdisplayCategory=false;
		subcatalogIds=childCatalogMap.values().toString().substring(1, childCatalogMap.values().toString().length()-1).replaceAll("\\s+","");
	}
	
//	Category Name display Based on New Launch,Hot Deals,etc.
	
	if(Validator.isNotNull(cmd) && (isdisplayCategory)){
		if(cmd.equals(EECConstants.NEWLAUNCH)){
		ProductInventoryLocalServiceUtil.getCatalogIdsbyCreateDate(themeDisplay.getCompanyId(),childCatalogMap);
		}else if(cmd.equals(EECConstants.HOTDEAL)){
		ProductInventoryLocalServiceUtil.getCatalogIdsbyDiscount(themeDisplay.getCompanyId(),childCatalogMap);
		}else if(cmd.equals(EECConstants.SEARCH)){
			ProductInventoryLocalServiceUtil.getCatalogIdsbySearch(themeDisplay.getCompanyId(), productName, childCatalogMap);
		}else if(cmd.equals(EECConstants.SIMILAR_PRODUCTS)){
			ProductInventoryLocalServiceUtil.getCatalogIdsbySimilarProduct(themeDisplay.getCompanyId(), productId, childCatalogMap);
		}else if(cmd.equals(EECConstants.RELATED_PRODUCT)){
			ProductInventoryLocalServiceUtil.getCatalogIdsbyRelatedProduct(themeDisplay.getCompanyId(), productId, childCatalogMap);
		}
		subcatalogIds=childCatalogMap.values().toString().substring(1, childCatalogMap.values().toString().length()-1).replaceAll("\\s+","");
		String[] catalogIds=subcatalogIds.split(",");
		if(catalogIds.length>=1){%>
		

		<label for="catalogName" class="filterheading">Category</label>
		<br>
		
		<%	Iterator it = childCatalogMap.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry entry = (Map.Entry) it.next();
		    String id=entry.getValue().toString();
			   if(id.contains(StringPool.COMMA))id= id.substring(0,id.indexOf(StringPool.COMMA));
				%>
				<input type="checkbox" id="catalogName_<%=id%>" value="<%=(String)entry.getKey()%>" name="catalogName" onclick='javascript:filterByBrandName("<%=(String)entry.getValue()%>");'><span class="filtertext">&nbsp;&nbsp;<%=(String)entry.getKey()%></span></input>
				<br/>

			<%}
		}
		
	}		
		
	
//	Sub Category Name display while selecting parent category from navigation menu or categories portlet

	if(Validator.isNotNull(parentCatalogId)){
		if(childCatalogMap.size()>=1){%>

		<label for="catalogName" class="filterheading">Category</label>
		<br>
		<%	Iterator it = childCatalogMap.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry entry = (Map.Entry) it.next();
		    String id=entry.getValue().toString();
		   if(id.contains(StringPool.COMMA))id= id.substring(0,id.indexOf(StringPool.COMMA));
				%>
				<b><input type="checkbox" id="brandName_<%=id%>" value="<%=(String)entry.getKey()%>" name="brandName" onclick='javascript:filterByBrandName("<%=(String)entry.getValue()%>");'><span class="filtertext">&nbsp;&nbsp;<%=(String)entry.getKey()%></span></input></b><br/><br/>
			<%}
		}
	}
 
	double min=ProductInventoryLocalServiceUtil.getPriceRangeByCatalogId(subcatalogIds,"ASC");
	double max=ProductInventoryLocalServiceUtil.getPriceRangeByCatalogId(subcatalogIds,"DESC");
	
	
	int minCount=EECUtil.getDigitsCount(min);
	int maxCount=EECUtil.getDigitsCount(max);
	
	double min1=EECUtil.getMinPrice(min);
	double max1=EECUtil.getMaxPrice(max);
	
	double price =Math.round(max1/maxCount); 
	if(max>0.0) {%>

		<br>
		<div class="scroll">
		<label class="filterheading">Price Range</label>
		<br>
		
	<%	if(priceRangeType.equals(PortletPropsValues.FILTER_PRICE_SLIDER_TYPE)){%>
		<span class="filterheading">Enter Price Range:</span>
		<br/>
	    <input type="text" id="my_min" style="color:#EC4D25; font-weight:bold;" class="price_range" size='5' value="<%=Math.round(min)%>" onkeypress="return validate(event)"> -
		<input type="text" id="my_max" style="color:#EC4D25; font-weight:bold;" class="price_range" size='5' value="<%=Math.round(max)%>" onkeypress="return validate(event)"><br/>
	 	 <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
	    <div class="ui-slider-range ui-widget-header"></div>
	    <a class="ui-slider-handle ui-state-default ui-corner-all" href="http://jqueryui.com/demos/slider/range.html#"></a><a class="ui-slider-handle ui-state-default ui-corner-all" href="http://jqueryui.com/demos/slider/range.html#"></a></div>
			
	<%}else{
		String startValue=StringPool.BLANK;
		for(double i=min1;i<=max1;i=i+price){ 
			if(i==min)startValue=String.valueOf(Math.round(i));
			else startValue=String.valueOf(Math.round(i+1));
			if(Validator.isNotNull(subcatalogIds)){
			   	filterContentList=ProductInventoryLocalServiceUtil.getProductListByPricePange(themeDisplay.getCompanyId(),productId,productName,subcatalogIds,startValue+StringPool.SPACE+"AND"+StringPool.SPACE+String.valueOf(Math.round(i+price)),StringPool.BLANK,cmd);
			   	if(filterContentList.size()>0){
			   	String value=startValue+StringPool.SPACE+"AND"+StringPool.SPACE+String.valueOf(Math.round(i+price));
			%>
				<input type="checkbox"  id="priceRange_<%=value%>" value="<%=value%>" name="priceRange" onclick="filterByPriceRange(this.value);"> <span class="filtertext"> &nbsp;&nbsp;Rs. <%=startValue%> - Rs. <%=Math.round(i+price)%>&nbsp;&nbsp;</span></input>
				<br/>
			<%}
			}
		  }
	   }
	 }%>
	 </div>
<!--  Sorting categories like AtoZ,ZtoA,etc.  -->
<br>
	<div class="scroll">
	
	<label for="sortByPrice" class="filterheading">Sort By</label>
	<br/>
	<input type="radio" id="sortBy_AtoZ" value="AtoZ" name="sortByName" onclick="sortByNameandPrice(this.value);" checked="checked"> <span class="filtertext">A to Z </span></input><br/>
	<input type="radio" id="sortBy_ZtoA" value="ZtoA" name="sortByName" onclick="sortByNameandPrice(this.value);"><span class="filtertext"> Z to A</span></input><br/>
	<input type="radio" id="sortBy_LowtoHigh" value="LowtoHigh" name="sortByName" onclick="sortByNameandPrice(this.value);"><span class="filtertext"> Low to High Price</span></input><br/>
	<input type="radio" id="sortBy_HightoLow" value="HightoLow" name="sortByName" onclick="sortByNameandPrice(this.value);" ><span class="filtertext"> High to Low Price</span></input><br/><br/>
	</div>
	
<aui:script>
AUI().use('event', 'node', 'aui-loading-mask-deprecated',function(A) {
  A.one('#p_p_id<portlet:namespace />').plug(A.LoadingMask, 
                  { 
           background: '#FFFFFF',
         });
});

function filterByBrandName(catalogId) { 
var A = AUI(); 
A.one('#p_p_id<portlet:namespace />').loadingmask.show();
	Liferay.fire('FILTER_BY_BRAND', {
		curCatalogId : catalogId
	         });
  	}
  	
  	function sortByNameandPrice(value) {  
  	var A = AUI(); 
  	A.one('#p_p_id<portlet:namespace />').loadingmask.show();
	Liferay.fire('SORT_BY_NAME&PRICE', {
		sortByNameandPrice : value
	         });
  	}
  	
  	Liferay.on('SEL_PRICE_RANGES',function(event) {
    	if(event.priceRanges!='null'){
      		var priceRanges = event.priceRanges.split("@");
       		for (var i=0; i < priceRanges.length; i++){
	        	document.getElementById('priceRange_'+priceRanges[i]).checked=true;
        	}
        }
      }
    );
    
  	Liferay.on('BRAND_IDS',function(event) {
  		if(event.brandIds!='null'){
      	var catalogIds = event.brandIds.split(",");
	        for (var i=0; i < catalogIds.length; i++){
		        var isdisplayCategory=<%=isdisplayCategory %>;
			        if(isdisplayCategory){
			        	if(document.getElementById('catalogName_'+catalogIds[i])!= null)document.getElementById('catalogName_'+catalogIds[i]).checked=true;
			        }else{
			         	if(document.getElementById('brandName_'+catalogIds[i])!= null)document.getElementById('brandName_'+catalogIds[i]).checked=true;
			        }
	        }
        }
      }
    );
    
   Liferay.on('SORT_BY',function(event) {
  		 if(event.selsortBy!='null'){
  	    	 document.getElementById('sortBy_'+event.selsortBy).checked=true;
  		 }
      }
    );
    
   
    function filterByPriceRange(pricerange) {
    var A = AUI(); 
    A.one('#p_p_id<portlet:namespace />').loadingmask.show();
	Liferay.fire('FILTER_BY_PRICE',pricerange);
  	}
    
</aui:script>
<script>

$(function() {
	                /*************  Initialize Price Range Slider *************/
	var minimum= parseInt($("#my_min").val()); 
	var maximum= parseInt($("#my_max").val());
	                $( "#slider-range" ).slider({
	                        range: true,
	                        min: minimum,
	                        max: maximum,
							step: 1000,
	                        values: [ $("#my_min").val(), $("#my_max").val()], // The default range
	                        slide: function( event, ui ) {
	                                $( "#my_min" ).val(ui.values[ 0 ]); // Display and selected the min Price
	                                $( "#my_max" ).val(ui.values[ 1 ]); // Display and selected the max Price
	                        },
							change: function(e,ui){
							Liferay.fire('FILTER_BY_SLIDER_PRICE',ui.values[ 0 ]+" AND "+ui.values[ 1 ] );
						}	
	                });
	        /*************  Set Initial Value of the Dropdown Box *************/
	                //For dropdown box
	                $( "#my_min" ).val($( "#slider-range" ).slider( "values", 0 ));
	                $( "#my_max" ).val($( "#slider-range" ).slider( "values", 1 ));
	        });
	
/************* When user choose from dropdown box directly *************/
                $("input.price_range").change(function () {
                        var myMinValue =parseInt($('#my_min').val());
                        var myMaxValue =parseInt($('#my_max').val()); 
                        //Make changes on the slider itself
						if(myMinValue <= myMaxValue) {
                                $( "#slider-range" ).slider({
                                        values: [myMinValue,myMaxValue]                                                      
                                });
                        } else {
                                alert("Invalid Input"); 
                        }
                });

function validate(key)
{
var keycode = (key.which) ? key.which : key.keyCode;
if (!(keycode==8 || keycode==46)&&(keycode < 48 || keycode > 57))
{
return false;
}
}
</script>
</div>
<style>
.aui input[type="radio"], .aui input[type="checkbox"] {
    margin: 0;
}
.scroll{
max-height: 200px;
overflow-y: auto;
}
</style>