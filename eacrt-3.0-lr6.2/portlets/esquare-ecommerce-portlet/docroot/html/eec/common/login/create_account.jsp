<%@ include file="/html/eec/common/login/init.jsp"%>

<%
	String twitterId = ParamUtil.getString(request, "twitterId");
	String firstName = ParamUtil.getString(request, "firstName");
	String iString = (String) request.getAttribute("isPopupLogin");
	boolean isPopupLogin = ParamUtil
			.getBoolean(request, "isPopupLogin");
	if (Validator.isNotNull(iString))
		isPopupLogin = Boolean.valueOf(iString);
	String redirect = ParamUtil.getString(request, "redirect");
	
	String star = "&nbsp;<font color='red' style='font-size: 12px;'>*</font>";
%>


<portlet:actionURL var="createAccoutURL" />
<div style="width: 200px; display: block; margin-left: auto;  margin-right: auto;">
	<aui:form action="<%=createAccoutURL%>" method="post" name="fm">
		<liferay-ui:error exception="<%=CompanyMaxUsersException.class%>"
			message="unable-to-create-user-account-because-the-maximum-number-of-users-has-been-reached" />
		<liferay-ui:error exception="<%=ContactFirstNameException.class%>"
			message="please-enter-a-valid-name" />
		<liferay-ui:error exception="<%=ContactFullNameException.class%>"
			message="please-enter-a-valid-first-middle-and-last-name" />
		<liferay-ui:error exception="<%=ContactLastNameException.class%>"
			message="please-enter-a-valid-last-name" />
		<liferay-ui:error
			exception="<%=DuplicateUserEmailAddressException.class%>"
			message="the-email-address-you-requested-is-already-taken" />
		<liferay-ui:error exception="<%=DuplicateUserIdException.class%>"
			message="the-user-id-you-requested-is-already-taken" />
		<liferay-ui:error
			exception="<%=DuplicateUserScreenNameException.class%>"
			message="the-screen-name-you-requested-is-already-taken" />
		<liferay-ui:error exception="<%=EmailAddressException.class%>"
			message="please-enter-a-valid-email-address" />
		<aui:input name="<%=EECConstants.CMD%>" type="hidden"
			value="<%=EECConstants.ADD%>" />
		<aui:input name="redirect" type="hidden" value="<%=redirect%>" />
		<aui:input type="hidden" name="successMessage" value="successMessage" />

			<aui:fieldset>
				<span class="ecart-headfontfam">
				<aui:column>
					<aui:input name="isPopupLogin" type="hidden"
						value="<%=isPopupLogin%>" />
					<aui:input model="<%=User.class%>" name="firstName"  showRequiredLabel="false"	value="<%=firstName%>" label="" placeholder="Name"
									type="text" >
						<aui:validator name="required"
							errorMessage="please-enter-a-valid-name" />
							<aui:validator name="minLength">
							'4'
							</aui:validator>
							<aui:validator name="alphanum"/>
					</aui:input>
						<aui:input model="<%=User.class%>" name="emailAddress" showRequiredLabel="false" 
									label="" placeholder="Email"  type="text">
									<aui:validator name="required" />
									<aui:validator name="email" />
								</aui:input>
						<aui:input name="twitterId" type="hidden" value="<%=twitterId%>" /> 
						<c:if test="<%=Validator.isNull(twitterId)%>">
						<aui:input label="" placeholder="Password" name="password1" size="30" showRequiredLabel="false" 
										type="password" value="">
										<aui:validator name="required" errorMessage="please-enter-a-valid-password"/>
										<aui:validator name="minLength">
										'6'
										</aui:validator>
									</aui:input>
							<aui:input label="" placeholder="Confirm Password" name="password2" size="30" showRequiredLabel="false" 
										type="password" value="">
										<aui:validator name="equalTo" errorMessage="password-does-not-match-the-confirm-password">
						'#<portlet:namespace />password1'
					</aui:validator>
										<aui:validator name="required" errorMessage="please-enter-a-valid-password"/>
									</aui:input>
						</c:if>
				</aui:column>
				</span>
			</aui:fieldset>

				<input type="submit" value="Create Account" id="createaccountbutton" label="Create Account"/>
	<div>
				
	</aui:form>
</div>

<div>
				
		<c:if test="<%=Validator.isNull(twitterId)%>">
			<liferay-util:include page="/html/eec/common/login/navigation.jsp"
				servletContext="<%=this.getServletContext()%>" />
		</c:if>
</div>		


<style type="text/css">



.dashboard-fields-lable, .field-label, .field-label-inline-label, .choice-label{
margin-left: 0}





.taglib-icon .taglib-text{
text-decoration: none;
}

.column-content, .column-content-center{
padding: 0 0}

[class~="aui-form-validator-message"]{
   
   height: 19px;
    left: 263px;
    margin-top: 0;
    top: 4px;
    }

a{
text-decoration: none;}


.fieldset {
    border: 0 none;
    padding-bottom: 0;
}
/* ----------login ---------- */

#_login_WAR_esquareecommerceportlet_firstName{

    background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/name.png") no-repeat scroll 215px center;
      border: 1px solid #ccc;
    box-shadow: none;
    font-family: Helvetica;
    font-size: small;
    height: 35px;
    margin-bottom: 10px;
    margin-left: 0;
    padding-left: 15px;
    width: 250px;
}
#_login_WAR_esquareecommerceportlet_emailAddress{
	
    background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/email.png") no-repeat scroll 215px center;
  border: 1px solid #ccc;
    box-shadow: none;
    font-family: Helvetica;
    font-size: small;
    height: 35px;
    margin-bottom: 10px;
    margin-left: 0;
    padding-left: 15px;
    width: 250px;
}
#_login_WAR_esquareecommerceportlet_password1{
background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/lock.png") no-repeat scroll 215px center;
     border: 1px solid #ccc;
    box-shadow: none;
    font-family: Helvetica;
    font-size: small;
    height: 35px;
    margin-bottom: 10px;
    margin-left: 0;
    padding-left: 15px;
    width: 250px;
}
#_login_WAR_esquareecommerceportlet_password2{
background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/password.png") no-repeat scroll 215px center;
     border: 1px solid #ccc;
    box-shadow: none;
    font-family: Helvetica;
    font-size: small;
    height: 35px;
    margin-bottom: 10px;
    margin-left: 0;
    padding-left: 15px;
    width: 250px;
}
#createaccountbutton{
  background-color: #45a5d5 !important;
    border: 0 none !important;
    color: white !important;
    display: inline-block;
    font-family: helvetica !important;
    font-size: small !important;
    padding: 8px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
    width: 110px !important;
}
#createaccountbutton:hover{
background-color: #4DC48A !important;
 cursor: pointer;
 }
</style>


