<%@ include file="/html/eec/common/login/init.jsp"%>

<%
PortletURL createAccount = renderResponse.createRenderURL();
createAccount.setParameter("jspPage", "/html/eec/common/login/create_account.jsp");
createAccount.setParameter("saveLastPath", "0");
createAccount.setWindowState(LiferayWindowState.POP_UP);
%>
<c:choose>
	<c:when test="<%=themeDisplay.isSignedIn()%>">
		<%
			String signedInAs = HtmlUtil.escape(themeDisplay.getUser().getFullName());

			if (themeDisplay.isShowMyAccountIcon()) {
				signedInAs = "<a href=\""+ HtmlUtil.escape(themeDisplay.getURLMyAccount().toString()) + "\">"
						+ signedInAs + "</a>";
			}
		%>

		<%=LanguageUtil.format(pageContext,	"you-are-signed-in-as-x", signedInAs, false)%>
	</c:when>
	<c:otherwise>
		<%
			boolean rememberMe = ParamUtil.getBoolean(request,"rememberMe");
			String redirect = ParamUtil.getString(request, "redirect");
			String iString=(String)request.getAttribute("isPopupLogin");
			boolean isPopupLogin = ParamUtil.getBoolean(request, "isPopupLogin");
			if(Validator.isNotNull(iString))isPopupLogin=Boolean.valueOf(iString);
		%>

		<%
		String message = themeDisplay.getURLCurrent();
		String name = "login";
		String result = message.replaceAll("home", name);
		String normal = "normal";
		String rdirectURL = result.replaceAll("exclusive",normal);
		%>
		<portlet:actionURL var="loginURL">
		<portlet:param name="resultURL" value="<%=rdirectURL %>" />
		</portlet:actionURL>
				<div align="center">
					<aui:form action="<%=loginURL%>" method="post" name="fm"  >
						<aui:input name="<%=EECConstants.CMD%>" type="hidden"
						value="<%=EECConstants.LOGIN%>" />
						<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
						<aui:input name="isPopupLogin" type="hidden"
						value="<%=isPopupLogin%>" />

			<c:choose>
				<c:when test='<%= SessionMessages.contains(request, "user_added") %>'>
					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_added");
					String userPassword = (String)SessionMessages.get(request, "user_added_password");
					%>
					<div class="portlet-msg-success">
						<c:choose>
							<c:when test="<%= company.isStrangersVerify() || Validator.isNull(userPassword) %>">
								<%= LanguageUtil.get(pageContext, "thank-you-for-creating-an-account") %>
								<c:if test="<%= company.isStrangersVerify() %>">
									<%= LanguageUtil.format(pageContext, "your-email-verification-code-has-been-sent-to-x", userEmailAddress) %>
								</c:if>
							</c:when>
							<c:otherwise>
								<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-your-password-is-x", userPassword, false) %>
							</c:otherwise>
						</c:choose>
						<c:if test="<%= PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_ENABLED) %>">
							<%= LanguageUtil.format(pageContext, "your-password-has-been-sent-to-x", userEmailAddress) %>
						</c:if>
					</div>
				</c:when>
				<c:when test='<%= SessionMessages.contains(request, "user_pending") %>'>
					<%
					String userEmailAddress = (String)SessionMessages.get(request, "user_pending");
					%>
					<div class="portlet-msg-success">
						<%= LanguageUtil.format(pageContext, "thank-you-for-creating-an-account.-you-will-be-notified-via-email-at-x-when-your-account-has-been-approved", userEmailAddress) %>
					</div>
				</c:when>
				<c:when test='<%= SessionMessages.contains(renderRequest, "request_processed") %>'>
					<%
					String passwordResetEmail=(String)SessionMessages.get(renderRequest, "request_processed");
					%>
					<div class="portlet-msg-success">
						<%= LanguageUtil.format(pageContext, "a-link-to-reset-your-password-has-been-sent-to-x", passwordResetEmail) %>
					</div>
				</c:when>
				
			</c:choose>
			
			<liferay-ui:error exception="<%= AuthException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= CompanyMaxUsersException.class %>" message="unable-to-login-because-the-maximum-number-of-users-has-been-reached" />
			<liferay-ui:error exception="<%= CookieNotSupportedException.class %>" message="authentication-failed-please-enable-browser-cookies" />
			<liferay-ui:error exception="<%= NoSuchUserException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= PasswordExpiredException.class %>" message="your-password-has-expired" />
			<liferay-ui:error exception="<%= UserEmailAddressException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserLockoutException.class %>" message="this-account-has-been-locked" />
			<liferay-ui:error exception="<%= UserPasswordException.class %>" message="authentication-failed" />
			<liferay-ui:error exception="<%= UserScreenNameException.class %>" message="authentication-failed" />
		
				<aui:fieldset  >
				<span class="ecart-headfontfam">
						<!-- <span class="ecart-headfontfam" style="color: #012E69">Login here</span> -->	
							<aui:input name="login"  type="text" label="" showRequiredLabel="false" placeholder="Email" cssClass="loginbox">
								<aui:validator name="required" />
								<aui:validator name="email" />
							</aui:input>
						<br>
						<aui:input name="password" type="password"  label="" showRequiredLabel="false" placeholder="Password">
							<aui:validator name="required" errorMessage="please-enter-password" />
						</aui:input>
						<span id="<portlet:namespace />passwordCapsLockSpan"
							style="display: none;"><liferay-ui:message
								key="caps-lock-is-on" /></span>
						<%-- <c:if test="<%=company.isAutoLogin()&& !GetterUtil.getBoolean(PropsUtil.get(PropsKeys.SESSION_DISABLED))%>">
							<aui:input checked="<%=rememberMe%>" name="rememberMe" type="checkbox" style="cursor:pointer;"/>
						</c:if> --%>
						</span>
						<div>
					<liferay-util:include page="/html/eec/common/login/navigation.jsp" servletContext="<%=this.getServletContext()%>" />
			</div>	
		
				</aui:fieldset>
		 <br>
		 			<div class="row-fluid">
						<div class="span6" style="padding-left: 92px;">
							<input type="submit"  value="Login"  id="loginbutton" value="Login" />
						</div>
						<div class="span6" style="padding-right: 92px;">
							
								<div id="loginbutton" onclick="popupShow();">New User</div>
						</div>
						</div>
				</div>	
		</aui:form>
		
		
				
			
		</div>	
				
		<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) %>">
			<aui:script>
				Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />login);
			</aui:script>
		</c:if>

		<aui:script use="aui-base">
			var password = A.one('#<portlet:namespace />password');

			if (password) {
				password.on(
					'keypress',
					function(event) {
						Liferay.Util.showCapsLock(event, '<portlet:namespace />passwordCapsLockSpan');
					}
				);
			}
		</aui:script>
	</c:otherwise>
</c:choose>
<aui:script >
if (window.opener) {
window.opener.parent.location.href = "<%= HtmlUtil.escapeJS(PortalUtil.getPortalURL(renderRequest) + themeDisplay.getURLSignIn()) %>";
window.close();
}


function popupShow(){
	AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',
	function(A) {
	    var popUpWindow=Liferay.Util.Window.getWindow(
	{
	dialog: {
		centered: true,
		constrain2view: true,
		width:500,
		
		//cssClass: 'yourCSSclassName',
		modal: true,
		resizable: false,
		
		}
		}).plug(A.Plugin.IO,
	{
	autoLoad: false
	}).render();
	popUpWindow.show();
	popUpWindow.titleNode.html("Create Account Here");
	popUpWindow.io.set('uri','<%=createAccount%>');
	popUpWindow.io.start();
	});
	}
</aui:script>


<style>
fieldset, .form fieldset{
margin-bottom: 0;
 }

.dashboard-fields-lable, .field-label, .field-label-inline-label, .choice-label{
margin-left: 0
}




.taglib-icon .taglib-text{
text-decoration: none;
}


a{
text-decoration: none;}

[class~="aui-form-validator-message"]{
   
   left: 274px;
    width: 254px;
      top: 2px;
    }
    .checkbox
    {
    width:150px !important;
    }
    
    
  #_login_WAR_esquareecommerceportlet_login{
	  border-radius: 3px !important;
    box-shadow: 1px 1px 0 2px #ddd;
    font-family: myfont;
    font-size: medium;
    height: 35px;
    margin-left: 0;
    width: 250px;
    background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/email.png") no-repeat scroll 230px center;
    
}
    #_login_WAR_esquareecommerceportlet_password{
    background: rgba(0, 0, 0, 0) url("/esquare-ecommerce-portlet/images/password.png") no-repeat scroll 230px center;
    border-radius: 3px !important;
    box-shadow: 1px 1px 0 2px #ddd;
    font-family: myfont;
    font-size: medium;
    height: 35px;
    margin-left: 0;
    width: 250px;
    }
    #loginbutton{
      background-color: #45a5d5 !important;
    border: 0 none !important;
    color: white !important;
    display: inline-block;
    font-family: myfont !important;
    font-size: medium !important;
    padding: 10px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
    width: 110px !important;
    margin-top: 0px; 
    margin-bottom: 0px;
    }
    #loginbutton:hover{
    background-color: #4DC48A !important;
 cursor: pointer;
    }
    .aui input[type="radio"], .aui input[type="checkbox"] {
    margin: 0 !important;
}
.aui .radio input[type="radio"], .aui .checkbox input[type="checkbox"] {
    float: none !important;
    }
    .ecart-headfontfam .required, .ecart-headfontfam .form-validator-stack {
    display: block;
    }
    .taglib-icon .taglib-text {
    font-family: myfont;
    text-decoration: none;
}
   </style>