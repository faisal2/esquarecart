<%@ include file="/html/eec/common/login/init.jsp"%>
<%
HttpSession httpSession=request.getSession();
String isPopupLogin =(String) httpSession.getAttribute("isPopupLogin");
httpSession.removeAttribute("isPopupLogin");

EECUtil.getCart(renderRequest);
%>

<aui:script use="aui-base">
Liferay.provide(
		window,
		'<portlet:namespace />closeDialog',
		function() {
			Liferay.fire(
				'closeWindow',
				{
					id:  'signInDialog'
				}
			);
		},
		['aui-base']
	);
	if (<%=Validator.isNotNull(isPopupLogin)&&isPopupLogin.equalsIgnoreCase("isPopupLoginTwitter")%>) {
<!-- 			window.opener.parent.location.reload(true);  -->
			window.close();
			window.opener.parent.location.href = "<%= HtmlUtil.escapeJS(themeDisplay.getPathMain())  %>";
	}
	else if(<%=Validator.isNotNull(isPopupLogin)&&isPopupLogin.equalsIgnoreCase("isPopupLogin")%>)
	{
	<portlet:namespace />closeDialog();
	parent.location.reload();
<!-- 	window.opener.parent.location.reload(true); -->
<%-- 	parent.location.href = "<%= HtmlUtil.escapeJS(PortalUtil.getPortalURL(renderRequest) /* + themeDisplay.getURLSignIn())  %>"; --%>
	window.close();
	}

</aui:script>