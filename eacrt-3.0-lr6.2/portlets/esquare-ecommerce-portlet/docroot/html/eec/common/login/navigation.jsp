<%@ include file="/html/eec/common/login/init.jsp"%>
<%
	String jspPage = ParamUtil.getString(request, "jspPage");
	String cmd= ParamUtil.getString(request,EECConstants.CMD);
	String tabs1=ParamUtil.getString(request,"tabs1");
	boolean showCreateAccountIcon = false;
	boolean showForgotPasswordIcon=false;
	boolean showFacebookConnectIcon=false;
	boolean showTwitterConnectIcon=false;
	boolean showLinkedInConnectIcon = false;
	String twitterURL=null;
	String taglibOpenTwitterConnectLoginWindow=null;
	long companyId=themeDisplay.getCompanyId();

	if (!jspPage.equals("/html/eec/common/login/create_account.jsp"))showCreateAccountIcon = true;
	if (!cmd.equals(EECConstants.FORGOT_PASSWORD_RENDER)&&!tabs1.equals("Signup"))showForgotPasswordIcon = true;
	if(FacebookConnectUtil.isEnabled(companyId))showFacebookConnectIcon=true;
	if(SocialNetworkConnectUtil.isTwitterEnabled(companyId))showTwitterConnectIcon=true;
	if(SocialNetworkConnectUtil.isLinkedInEnabled(companyId))showLinkedInConnectIcon=true;
	boolean isPopupLogin = ParamUtil.getBoolean(request, "isPopupLogin");
	PortletURL loginURL = renderResponse.createRenderURL();
	loginURL.setParameter("jspPage", "/html/eec/common/login/login.jsp");
	loginURL.setParameter("saveLastPath", "0");
	loginURL.setWindowState(LiferayWindowState.POP_UP);
%>
<%-- <portlet:renderURL var="loginURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
						<portlet:param name="jspPage" value="/html/eec/common/login/login.jsp" />
				</portlet:renderURL> --%>
		
			<liferay-ui:icon-list>
			<c:choose>
	        <c:when test="<%=showCreateAccountIcon %>">
				<portlet:renderURL var="createAccount">
						<portlet:param name="jspPage" value="/html/eec/common/login/create_account.jsp" />
						<portlet:param name="isPopupLogin" value='<%=isPopupLogin?"1":"0"%>' />
				</portlet:renderURL>
		 	<%-- <liferay-ui:icon image="add_user" message="create-account"
				url="<%=createAccount%>" />  --%>
			</c:when>
			<c:otherwise>
			
			
				 <aui:button type="button" value="Existing User" id="Existing" onclick="popupShow();" style="position:relative;left:151px;top: -67px;"/>
			<span></span>
			<%-- <liferay-ui:icon  message="Already User" url="<%=loginURL%>"	/> --%>
			
			</c:otherwise>
			</c:choose>
			
			
			<% 
		
		if(!ParamUtil.getString(request, "jspPage").equals("/html/eec/common/login/create_account.jsp"))
		{
		%>		  
			<c:if test="<%= showForgotPasswordIcon %>">
				<portlet:renderURL var="forgotPasswordURL">
				<portlet:param  name="<%=EECConstants.CMD%>" value="<%=EECConstants.FORGOT_PASSWORD_RENDER %>" />
				</portlet:renderURL>
				<liferay-ui:icon message="Forget Password ?" 	url="<%= forgotPasswordURL %>" />
			</c:if>
			
			<%} %>
			
				<c:if test="<%=showFacebookConnectIcon%>">
				<portlet:renderURL var="loginRedirectURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
					<portlet:param name="jspPage" value="/html/eec/common/login/login_redirect.jsp" />
				</portlet:renderURL>
				<%
						 String facebookAuthRedirectURL = FacebookConnectUtil
								.getRedirectURL(companyId);
						facebookAuthRedirectURL = HttpUtil.addParameter(
								facebookAuthRedirectURL, "redirect",
								HttpUtil.encodeURL(loginRedirectURL.toString()));
	
						String facebookAuthURL = FacebookConnectUtil
								.getAuthURL(companyId);
						facebookAuthURL = HttpUtil.addParameter(facebookAuthURL,
								"client_id", FacebookConnectUtil
										.getAppId(companyId));
						facebookAuthURL = HttpUtil.addParameter(facebookAuthURL,
								"redirect_uri", facebookAuthRedirectURL);
						facebookAuthURL = HttpUtil.addParameter(facebookAuthURL,
								"scope", "email");
	
						String taglibOpenFacebookConnectLoginWindow = "javascript:var facebookConnectLoginWindow = window.open('"
								+ facebookAuthURL.toString()
								+ "','facebook', 'align=center,directories=no,height=560,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=1000'); void(''); facebookConnectLoginWindow.focus();";
				 %>
				 			<br/><br/>
						<liferay-ui:icon  image="../eec/login/facebook1" 
						message="" url="<%=taglibOpenFacebookConnectLoginWindow%>" />
				</c:if>
				<%
boolean googleAuthEnabled = PrefsPropsUtil.getBoolean(themeDisplay.getCompanyId(), "google-auth-enabled", true);

if (Validator.isNull(PrefsPropsUtil.getString(company.getCompanyId(), "google-client-id")) || Validator.isNull(PrefsPropsUtil.getString(company.getCompanyId(), "google-client-secret"))) {
	googleAuthEnabled = false;
}
%>

<c:if test="<%= googleAuthEnabled %>">

	<%
	String googleAuthURL = PortalUtil.getPathContext() + "/c/portal/google_login?cmd=login";

	String taglibOpenGoogleLoginWindow = "javascript:var googleLoginWindow = window.open('" + googleAuthURL.toString() + "', 'google', 'align=center,directories=no,height=560,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=1000'); void(''); googleLoginWindow.focus();";
	%>

	<liferay-ui:icon
		message=" " 
		src="/html/portlet/login/navigation/google.png"
		url="<%= taglibOpenGoogleLoginWindow %>"
	 />
</c:if>
			
				<c:if test="<%=showTwitterConnectIcon%>">
					<portlet:actionURL var="calBackURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
						<portlet:param name="<%=EECConstants.CMD %>"
							value="<%=EECConstants.TWITTER %>" />
							<portlet:param name="isPopupLogin"
							value='<%= isPopupLogin ? "1" : "0" %>' /> 
				   </portlet:actionURL>
					<%
					 	OAuthService service = new ServiceBuilder().provider(TwitterApi.SSL.class).apiKey(SocialNetworkConnectUtil.getTwitterAppId(companyId)).apiSecret(SocialNetworkConnectUtil.getTwitterAppSecret(companyId)).callback(calBackURL.toString()).build();
						Token requestToken = service.getRequestToken();
						twitterURL = String.format(SocialNetworkConnectUtil.getTwitterAuthURL(),requestToken.getToken());
						String taglibDownURL = "javascript:" + renderResponse.getNamespace() + "twitter()";
						taglibOpenTwitterConnectLoginWindow = "var twitterConnectLoginWindow = window.open('"
								+ twitterURL
								+ "','twitter', 'align=center,directories=no,height=560,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=1000'); void(''); twitterConnectLoginWindow.focus();";
		
					%>
				
				<liferay-ui:icon  image="../eec/login/twitter1" message="" url="<%=taglibDownURL%>" />
				
				</c:if>
			
				<c:if test="<%=showLinkedInConnectIcon %>">
					<%
					boolean linkedInProfileFormPopupEnabled = SocialNetworkConnectUtil.isLinkedInProfileViewEnabled();
					boolean linkedInAutoLoginEnabled = SocialNetworkConnectUtil.isLinkedInAutoLoginEnabled();
					String linkedInAppId = SocialNetworkConnectUtil.getLinkedInAppId(companyId);
					%>
						<%@ include file="linkedinSignin.jsp" %>
				</c:if>
		  

		
			
		</liferay-ui:icon-list>
		
<aui:script >
Liferay.provide(
		window,
		'<portlet:namespace />closeDialog',
		function() {
			Liferay.fire(
				'closeWindow',
				{
					id: 'signInDialog'
				}
			);
		},
		['aui-base']
	);
function <portlet:namespace />twitter(){
<portlet:namespace />closeDialog();
<%=taglibOpenTwitterConnectLoginWindow %>
	}
	
function popupShow(){
	AUI().use('aui-base','aui-io-plugin-deprecated','liferay-util-window',
	function(A) {
	    var popUpWindow=Liferay.Util.Window.getWindow(
	{
	dialog: {
		centered: true,
		constrain2view: true,
		width:500,
		
		//cssClass: 'yourCSSclassName',
		modal: true,
		resizable: false,
		
		}
		}).plug(A.Plugin.IO,
	{
	autoLoad: false
	}).render();
	popUpWindow.show();
	popUpWindow.titleNode.html("Create Account Here");
	popUpWindow.io.set('uri','<%=loginURL%>');
	popUpWindow.io.start();
	});
	}

	</aui:script>
	
<style>
.aaaa{
 width: 90px;}
 
 #suxb_column1_0
 {
 margin-top:-3px !important;
 }
 #Existing{
 	background: #45a5d5 !important;
    border: 0 none !important;
    color: white !important;
    display: inline-block;
    font-family: helvetica !important;
    font-size: small !important;
    padding: 8px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
    width: 110px !important;
 }
  #Existing:hover{
 background-color: #4DC48A !important;
 cursor: pointer;
 }
</style>