<%@page import="com.esquare.ecommerce.product.util.ECCProductDetailsUtil"%>
<%@ include file="/html/eec/common/product/init.jsp"%>

<style type="text/css">
#sitemap li{
    font-size: 12px;
    font-weight: bold;
    }
    
#sitemap li a {
    color: highlight;
    text-decoration: none;
}

#sitemap li span, #sitemap li span.collapsed {
    margin-left: 3px;
    margin-top: -3px;
    height: 40px;
    width: 27px;
}
</style>
<%
PortletURL deleteURL = renderResponse.createActionURL();
deleteURL.setParameter(Constants.CMD, Constants.DELETE);

PortletURL editURL = renderResponse.createRenderURL();
editURL.setParameter("cmd","customField");
long curProductInventoryId=0;
PortletURL addCatalogURL = renderResponse.createRenderURL();
addCatalogURL.setParameter("cmd_add","ADD_CATALOG");
addCatalogURL.setParameter("cmd","customField");
	Catalog catalog = (Catalog)request.getAttribute("CATALOG");
	long catalogId = BeanParamUtil.getLong(catalog, request, "catalogId", EECConstants.DEFAULT_PARENT_CATALOG_ID);
	int total = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
	String relatedProductId=request.getParameter("relatedProductIds");
	String productInventoryId=request.getParameter("productInventoryId");
	
	String relatedProductIds=StringPool.BLANK;
	if(Validator.isNotNull(relatedProductId))relatedProductIds=relatedProductId;
	if(Validator.isNotNull(productInventoryId))curProductInventoryId=Long.parseLong(productInventoryId);
	List<Catalog> results = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId(), 0,0,total);
%>
<div class="sidebarmenu">
<h4> Please Select the Product</h4>

<ul id="sitemap" style="margin-left: -10px;">
	<%
	 String subCategoryList = "";
	for (Catalog catalogg : results) {
	%> 
	<li style="width: 100%;">
	<a href="javascript:showDiv('rowId<%=catalogg.getCatalogId()%>');"><%= catalogg.getName() %></a>	
		
		 <% 
		  subCategoryList = ECCProductDetailsUtil.getSubCatalogPopupList(themeDisplay.getCompanyId(), catalogg.getCatalogId(),curProductInventoryId,ctxPath,relatedProductIds);
				
			  %>
 

	<%=subCategoryList %>
	</li>
	
	<% } %>
	 
</ul>
</div><br/><br/>
<input type="button" value="Save" onclick="saveRelatedProduts();"/>
<input type="button" value="Cancel" onclick="cancelRelatedProduts();"/>
<script>
function saveRelatedProduts(){
	
	 	var cboxes = document.getElementsByName('productDetailIds');
	    var len = cboxes.length;
	    var productIds="";
	    for (var i=0; i<len; i++) {
	    	  if (cboxes[i].checked){
	    		  productIds = productIds +cboxes[i].value + ","
	 	      }
	    }
	    try {
	        window.opener.<portlet:namespace />selectProduts(productIds);
	    }
	    catch (err) {}
	    window.close();
	    return false;
	}
function cancelRelatedProduts(){
	 window.close();
}
function showDiv(id) {
	   var e = document.getElementById(id);
		if (e.style.display == 'none') {
		   e.style.display = 'block';
	    }
	    else if(e.style.display == 'block'){
	       e.style.display = 'none';
	    }
	}
function CloseMySelf(catalogId,catalogName) {
   try {
        window.opener.<portlet:namespace />selectCatalog(catalogId,catalogName);
    }
    catch (err) {}
    window.close();
    return false;
}
function checkSelectedProductIds(){
	var productIds= "<%=relatedProductIds%>";
	var relatedproductIds = productIds.split(",");
    for (var i=0; i < relatedproductIds.length; i++){
	         if((relatedproductIds.length>1)){
		        	document.getElementById('productDetailIds_'+relatedproductIds[i]).checked=true;
			}
     }
	
}
<%-- window.onload = relatedProductOnLoad;
function relatedProductOnLoad(){
 var productIds= "<%=relatedProductIds%>";
// alert(productIds);
 
} --%>
</script>
