<%@ include file="/html/eec/common/product/init.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
String plns = "";
if (renderRequest != null) {
	plns = renderResponse.getNamespace();
}
ProductDetails productDetails = (ProductDetails)request.getAttribute("PRODUCT_DETAILS");
PortletSession pSession=renderRequest.getPortletSession();



String selestedCatalogId = (String) pSession.getAttribute("CATALOG_ID");
long productDetailsId = BeanParamUtil.getLong(productDetails, request, "productDetailsId");
long catalogId = BeanParamUtil.getLong(productDetails, request, "catalogId", EECConstants.DEFAULT_PARENT_CATALOG_ID);

Catalog catalog =null;
String catalogName=StringPool.BLANK;
try {
	if(Validator.isNotNull(selestedCatalogId)){
		catalogId=Long.parseLong(selestedCatalogId);
		catalog= CatalogLocalServiceUtil.getCatalog(catalogId);
		catalogName = catalog.getName();
	}else{ 
	catalog= CatalogLocalServiceUtil.getCatalog(catalogId);
	catalogName = catalog.getName();
	catalogId=catalog.getCatalogId();
	}
}
catch (Exception e) {
}
ProductInventory productInventory=null;
List<ProductImages> productImages=null;
long productInventoryId=0;
boolean taxes=true;
boolean freeShipping=true;
boolean outOfStockPurchase=true;
boolean visibility=true;
boolean isrelatedProducts=false;
long customFieldRecId=0;

String relatedProductIds=StringPool.BLANK;
if(productDetails!=null){
	try{
	productInventory=ProductInventoryLocalServiceUtil.findByProductDetailsId(productDetails.getProductDetailsId());
	productImages=ProductImagesLocalServiceUtil.findByInventoryId(productInventory.getInventoryId());
	productInventoryId=productInventory.getInventoryId();
	relatedProductIds=productInventory.getLimitProducts();
	taxes=productInventory.getTaxes();
	freeShipping=productInventory.getFreeShipping();
	outOfStockPurchase=productInventory.getOutOfStockPurchase();
	visibility=productDetails.getVisibility();
	isrelatedProducts=productInventory.getSimilarProducts();
	customFieldRecId=productInventory.getCustomFieldRecId();
	}catch(Exception e){
	}
}



PortletURL actionURL = renderResponse.createActionURL();

boolean isRelatedProductsPermission = false;
CartPermission cartPermissionProduct  = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.RELATED_PRODUCT);
if(cartPermissionProduct!=null){
	isRelatedProductsPermission=Boolean.parseBoolean(cartPermissionProduct.getValue());
}


//Image
String image=StringPool.BLANK;
try{
productInventory=ProductInventoryLocalServiceUtil.findByProductDetailsId(productDetails.getProductDetailsId());
productImages=ProductImagesLocalServiceUtil.findByInventoryId(productInventory.getInventoryId());
FileEntry dlFileEntry=DLAppLocalServiceUtil.getFileEntry(productImages.get(0).getImageId());
String imageURL = "/documents/" + dlFileEntry.getGroupId() + "/" + dlFileEntry.getFolderId() + "/" + dlFileEntry.getTitle()+"/"+dlFileEntry.getUuid();
image="<img src='"+imageURL+"' alt='"+productDetails.getName()+"' width='70px' height='70px' style='padding-left: 25px'/>";
}catch(Exception e){}
%>
<portlet:renderURL var="renderURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/product/dynamic_field.jsp"/>
</portlet:renderURL>


<%-- <liferay-ui:error exception="<%= DuplicateProductNameException.class %>"
	message="Product already exists with the same name" /> --%>
<portlet:resourceURL var="resourceURL" />
<div class="dashboard-maindiv2">

<aui:form enctype="multipart/form-data" method="post" name="fm"
	onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveProduct();" %>'>
	<aui:input name="<%= EECConstants.CMD %>" type="hidden" />
	<aui:input name="catalogId" id="catalogId" type="hidden" value="<%= catalogId %>" />
	<aui:input type="hidden" name="companyId" id="companyId" value="<%=themeDisplay.getCompanyId()%>" />
	<aui:input type="hidden" name="companyIdd" id="companyIdd" value="<%=String.valueOf(themeDisplay.getCompanyId())%>" />
	<input name="productDetailsId" id="productDetailsId" type="hidden"
		value="<%= productDetailsId %>" />
	<aui:input name="productInventoryId" type="hidden" value="<%= productInventoryId %>" />
	
	
	<div class="dashboard-headingstyle">Add Products</div>
	<hr style="margin-top: 13px;">
	
	<!-- Select Parent Catalog  -->
	<div id="product-selectparent">
<portlet:renderURL var="selectCatalogURL"
					windowState="<%= LiferayWindowState.POP_UP.toString() %>">
					<portlet:param name="jspPage"
						value="/html/eec/common/catalog/select_catalog.jsp" />
					<portlet:param name="catalogId"
						value="<%= String.valueOf(catalogId) %>" />
				</portlet:renderURL>
				<%
				String taglibOpenCatalogWindow = "var catalogWindow = window.open('" + selectCatalogURL + "', 'Catalog', 'directories=no,height=640,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=680'); void('');catalogWindow.focus();";
				%>
				<div>
					<table>
						<tr>
							<td><div class="dbproduct-header"><liferay-ui:message key="select-parent-catalog" />:</div></td>
							<td class="ProductOpenwindowSelectedCatalogs"><aui:a href="#" id="catalogName" label="<%= catalogName %>" /></td>
							<td class="ProductSelectCatalogsButtons">
								<div id="catalog">
											<div id="selectdiv" >
												<%-- <aui:button onClick="<%= taglibOpenCatalogWindow %>" value="Select" id="selectproduct"/> --%>
												<aui:button onClick="<%= taglibOpenCatalogWindow %>" value="Select" cssClass="selectproduct"></aui:button>
											</div>
											<div id="removediv">
												<aui:button	onClick='<%= renderResponse.getNamespace() + "removeCatalog();" %>'	value="Remove" cssClass="selectproduct"/>
											</div>
								</div>
							</td>
						</tr>
					</table>
				</div><span id='<%=renderResponse.getNamespace()+"catalogNameErrMsgDiv"%>' style="color:red;display:none;">Parent Catalog field is required</span>

</div>
	<br>
		<div class="dashboard-headingdiv">	
<div class="dashboard-headingstyle" >Product Details</div>
		<div class="separator" id="product-separator" ></div>
		</div>
	<aui:fieldset class="dashboard-fieldsetalign">
		<div id="row1">
			<fieldset class="dashboard-fieldsetalign">
				<%-- <legend class="legend">
					<h5 class="header">
						<liferay-ui:message key="Products" />
					</h5>
				</legend> --%>

								
					<aui:input type="text" size="81" bean="<%= productDetails %>"
						model="<%= ProductDetails.class %>" name="name" label='Name<span style="color:red;font-size: 14px;">*</span>' maxlength="400" id="productNameId">	
						<aui:validator name="required" errorMessage="please-enter-a-product-name"/>
					</aui:input>
					<div id="nameValidateMsg" style="color:#b50303;font-family: Georgia;font-size: 14px;"></div>
		
					<aui:input bean="<%= productDetails %>" model="<%= ProductDetails.class %>" name="description" />
		
	
				<div>

					<div class="uploadimage">Upload Image:<span style="color:red;font-size: 14px;">*</span></div>
					
								
					<aui:input type="hidden" id="imageId" bean="<%= productImages %>"
								model="<%= ProductImages.class %>" name="imageId" />
								<%if(Validator.isNull(productImages) ){ %>
					<aui:input label="" name="imageFile" type="file" showRequiredLabel="false" >
						<aui:validator name="custom" errorMessage="Please select a image with valid extension ( png,jpeg,jpg ) and image size less than 200kb">
							function (val, fieldNode, ruleValue) {
								var imageIdValue = document.getElementById("<portlet:namespace />imageId").value,
										fileInput = document.getElementById("<portlet:namespace />imageFile"),
						<!--  add file to be accepted below/change error message too -->
									   acceptFiles='jpeg,jpg,png',
								       isString = A.Lang.isString,
								       getRegExp = A.DOM._getRegExp,			       
								       uploadSize=true,
            							size=0;
							            for(var num1=0;num1<fileInput.files.length;num1++)
							            {
							                  var file=fileInput.files[num1];
							                     if(file.size>204800)
							                   {
							                     uploadSize=false;
							                   }else{
							                        uploadSize=true;
							                   }
							                   size+=file.size;
							             }
									   if (isString(acceptFiles)) {
				                 				var extensions = acceptFiles.split(/,\s*|\b\s*/).join('|');
				                  				regex = getRegExp('[.](' + extensions + ')$', 'i');
			                      		}
			                      		if(imageIdValue==""||imageIdValue==null){
			                      			return ((regex && regex.test(val)) && val.length!=0) && uploadSize;
			                      		}else{
			                      			return ((val.length!=0?(regex && regex.test(val)):true) && uploadSize);
									     }
									    
							}
						</aui:validator>
					</aui:input>
					<%} else{%>
					<aui:input label="" name="imageFile" type="file" showRequiredLabel="false" >
					</aui:input>
					<%=image%>
					<%} %>
					
					
				</div>
				<div class="related">
					<portlet:renderURL var="relatedProductURL"	windowState="<%= LiferayWindowState.POP_UP.toString() %>">
						<portlet:param name="jspPage" value="/html/eec/common/product/relatedproduct.jsp" />
						<portlet:param name="relatedProductIds"	value="<%=relatedProductIds%>" />
						<portlet:param name="productInventoryId"	value="<%=String.valueOf(productInventoryId)%>" />
					</portlet:renderURL>
					<%
					String relatedProductlistWindow = "var catalogWindow = window.open('" + relatedProductURL + "', 'Catalog', 'directories=no,height=640,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=680'); void('');catalogWindow.focus();";
					%>
					
				<aui:input type="checkbox"	value="<%=isrelatedProducts%>" name="similarProducts" label="Related Products"
												id="similarProducts" />
					<div id="relatedproduct_DivId">
						<div id="selectdiv">
					<span>		
					<aui:button value="Select" onClick="<%= relatedProductlistWindow %>" type="button" style="margin-top: 10px; margin-bottom: 10px;"/></span>
						</div>
							
						<%
						StringBuffer sb=new StringBuffer();
						if(Validator.isNotNull(relatedProductIds)){
						String[] limitedProductIds=relatedProductIds.split(",");
						for(String limitedProductId:limitedProductIds){
							try{
								 ProductInventory inventory= ProductInventoryLocalServiceUtil.getProductInventory(Long.parseLong(limitedProductId));
								 ProductDetails pDetails=ProductDetailsLocalServiceUtil.getProductDetails(inventory.getProductDetailsId());
								 sb.append(pDetails.getName());
								 sb.append("\n");
								}catch(NoSuchProductInventoryException noSuchProductInventoryException){}
								catch(NoSuchProductDetailsException noSuchProductDetailsException){}
						}}
						%>
						<div id="relatedProductDiv">
						<aui:input name="limitProducts" type="hidden" id="relatedProducthidden" value="<%=relatedProductIds %>"/>
								<aui:input name="limitProductsname" label="Selected Products" id="relatedProductTxt" type="textarea" style="height: 105px;width: 350px;" value="<%=sb.toString() %>" readonly="readonly"/>
						</div>
				</div>
			</div>
		</fieldset>
	</div>
	<%
	
	%>
	<br>
	<div class="dashboard-headingdiv">	
	<div class="dashboard-headingstyle" >General Features</div>
		<div class="separator" id="product-separator" ></div></div>
	<div id="fields"><fieldset class="dashboard-fieldsetalign">
	<!-- <legend class="legend3"><h5 class="header3row">General Features</h5></legend> -->
	<aui:select name="ecustomFieldName" label="Custom Field Title" id="ecustomFieldName" style="color:black;">
	<aui:option>Select</aui:option>
	<%
	String custFieldTitle=StringPool.BLANK;
	List<EcustomField> ecustomFieldlist=EcustomFieldLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId());
	EcustomField custFieldobj =null;
	try{
	if(customFieldRecId>0){
		custFieldobj=EcustomFieldLocalServiceUtil.getEcustomField(customFieldRecId);
		custFieldTitle=custFieldobj.getTitle();
	}}catch(Exception e){}
	for(EcustomField ecustomFieldObj:ecustomFieldlist){
	%>
	<aui:option selected='<%= custFieldTitle.equals(ecustomFieldObj.getTitle()) %>' value="<%=ecustomFieldObj.getTitle()%>"><%=ecustomFieldObj.getTitle()%></aui:option>
	<%} %>
	</aui:select>
	
	
	<div id="customFieldDiv" ><!-- --></div>
	
	</fieldset>
	
	</div>
	<br>
	<div class="dashboard-headingdiv">	
	<div class="dashboard-headingstyle" >Inventory & Varient</div>
		<div class="separator" id="product-separator" ></div> </div>
	<div id="row2">
		<fieldset class="dashboard-fieldsetalign">
			<%-- <legend class="legend2">
				<h5 class="header2">
					<liferay-ui:message key="inventory-and-variants" />
				</h5>
			</legend> --%>


			<div>
				<aui:layout>
					<div class="row-fluid">
						<div class="span6">
					<aui:input type="text" bean="<%= productInventory %>"
										model="<%= ProductInventory.class %>" name="sku" label='SKU<span style="color:red;font-size: 14px;">*</span>' maxlength="15" showRequiredLabel="false">
							<aui:validator name="required" errorMessage="Please enter alphanumeric only" />
							<aui:validator name="alphanum" errorMessage="Please enter alphanumeric only"/>			
							</aui:input>
							<div id="skuValidateMsg" style="color:#b50303;font-family: Georgia;font-size: 14px;"></div>
					
							<aui:input	bean="<%= productInventory %>"
										model="<%= ProductInventory.class %>" name="price" label='Price<span style="color:red;font-size: 14px;">*</span>' showRequiredLabel="false">
								<aui:validator name="required"
											errorMessage="Price field is required" />
									<aui:validator name="custom" errorMessage="Price field is required">
										function (val, fieldNode, ruleValue) {
											if((val=='')||(val==null)||(val==0.0)){
												return false;
											}
											return true;
										}
									</aui:validator>
								<aui:validator name="number"
											errorMessage="Please enter valid Price value" />
							</aui:input>
					</div>
					<div class="span6">
						<aui:input bean="<%= productInventory %>"
										model="<%= ProductInventory.class %>" name="discount" label='Discount<span style="color:red;font-size: 14px;">*</span> <span>(%) </span>' showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="number" />
							<aui:validator name="range">
								[0,100]
							</aui:validator>
						</aui:input>
					
					
						<aui:input type="text" bean="<%= productInventory %>"
									model="<%= ProductInventory.class %>" name="quantity" label='Quantity<span style="color:red;font-size: 14px;">*</span>' maxlength="4" showRequiredLabel="false">
							<aui:validator name="required"	errorMessage="Quantity field is required" />
							<aui:validator name="custom" errorMessage="Quantity field is required">
								function (val, fieldNode, ruleValue) {
									if((val=='')||(val==null)){
										return false;
									}
									return true;
								}
							</aui:validator>
							<aui:validator name="number" errorMessage="Please enter valid Quantity value" />
						</aui:input>
				</div>
				</div>
			</aui:layout>
		</div><br />
		<table>
			<tr class="tax"><td>
				<aui:input type="checkbox" value="<%=taxes%>" name="taxes" />
				<aui:input type="checkbox" value="<%=freeShipping%>" 
									name="freeShipping"/>
				<div id="weightId" style="display:<%= (freeShipping) ? "none":"block" %>;">
					<aui:input type="text" label="Weight" bean="<%= productInventory %>"
									model="<%= ProductInventory.class %>" name="weight"></aui:input>
				</div>
				<aui:input type="checkbox" value="<%=outOfStockPurchase%>"
									name="outOfStockPurchase" />
			</td></tr>
			<tr><td>
				<div id="dbproduct-radio">
					<span ><liferay-ui:message key="visibility" />:</span><br />
					<div  id="dbproduct-radiobut">
						<div >
							<input type="radio" name="visibility" class="visible" value="<%=EECConstants.PRODUCT_VISIBLITY_TRUE %>"
								<%= (visibility==true)? "checked":"" %> />
							<span class="visiblekey"><liferay-ui:message key="visible" /></span>
						</div>
						<div >
							<input type="radio" name="visibility" class=""
								value="<%=EECConstants.PRODUCT_VISIBLITY_FALSE %>"
								<%= (visibility==false)? "checked":"" %> />
							<span class="hiddenkey"><liferay-ui:message key="hidden" /></span>
						</div>
					</div>
				</div>
			</td></tr>
		</table>
	</fieldset>
	</div>
</aui:fieldset>

	
				<aui:button-row style="padding-left: 20px;">
					<aui:button type="submit" value="Save" class="dashboard-buttons" cssClass="submitcoupon"/>
					<aui:button  value="Cancel"  onClick="history.go(-1)" cssClass="cancelcoupon1"/>
				</aui:button-row>
		
	

</aui:form>
</div>
<portlet:resourceURL var="testURL">
</portlet:resourceURL>
<%
pSession.removeAttribute("CUSTOM_FIELD_OBJECT",pSession.PORTLET_SCOPE);
pSession.removeAttribute("CATALOG_ID");
%>
<aui:script>

Liferay.provide(
		window,
		'<portlet:namespace />selectProduts',
		function(productIds) {
		if(productIds.length>0){
		document.getElementById("relatedProductDiv").style.display='block';
		}
		document.<portlet:namespace />fm.<portlet:namespace />relatedProductTxt.value="";
		document.<portlet:namespace />fm.<portlet:namespace />relatedProductTxt.value = productIds;
		document.<portlet:namespace />fm.<portlet:namespace />relatedProducthidden.value = productIds;
		},
		['aui-base']
	);



	function <portlet:namespace />saveProduct() {
	document.<portlet:namespace />fm.action="<%= actionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>";
	var catalogName = document.getElementById("<portlet:namespace />catalogName").innerHTML;
		if(catalogName==''){
		document.getElementById("<portlet:namespace />catalogNameErrMsgDiv").style.display='block';
		}else{
		submitForm(document.<portlet:namespace />fm);
		}
	}
	
	
	Liferay.provide(
		window,
		'<portlet:namespace />selectCatalog',
		function(catalogId, catalogName) {
			var A = AUI();
			document.<portlet:namespace />fm.<portlet:namespace />catalogId.value = catalogId;
				
			var nameEl = document.getElementById("<portlet:namespace />catalogName");
			nameEl.href = "<portlet:renderURL><portlet:param name="struts_action" value="/message_boards/view" /></portlet:renderURL>&<portlet:namespace />catalogId=" + catalogId;
			nameEl.innerHTML = catalogName + "&nbsp;";

			if (catalogId != <%= EECConstants.DEFAULT_PARENT_CATALOG_ID %>) {
				var mergeWithParent = A.one('#<portlet:namespace />merge-with-parent-checkbox-div');

				if (mergeWithParent) {
					mergeWithParent.show();
				}
			}
			var parentName=document.getElementById("<portlet:namespace />catalogName").innerHTML;
        	if(parentName!=''){
        		document.getElementById("removediv").style.display='block';
        		document.getElementById("<portlet:namespace />catalogNameErrMsgDiv").style.display='none';
        	}else{
        		document.getElementById("removediv").style.display='none';
        	}
		},
		['aui-base']
	);
	
	Liferay.provide(
		window,
		'<portlet:namespace />removeCatalog',
		function() {
			var A = AUI();

			document.<portlet:namespace />fm.<portlet:namespace />catalogId.value = "<%= EECConstants.DEFAULT_PARENT_CATALOG_ID %>";

			var nameEl = document.getElementById("<portlet:namespace />catalogName");

			nameEl.href = "";
			nameEl.innerHTML = "";

			var mergeWithParent = A.one('#<portlet:namespace />merge-with-parent-checkbox-div');
			var mergeWithParentCategory = A.one('#<portlet:namespace />mergeWithParentCategoryCheckbox');

			if (mergeWithParent) {
				mergeWithParent.hide();
			}

			if (mergeWithParentCategory) {
				mergeWithParentCategory.set('checked', false);
			}
			document.getElementById("removediv").style.display='none';
			
		},
		['aui-base']
	);
	
	
        function addCatalogOnLoad() {
      		showrelatedProductsDiv(<%=isrelatedProducts%>);
        	var parentCatalogName = document.getElementById("<portlet:namespace />catalogName").innerHTML;
        	if(parentCatalogName==''){
        		document.getElementById("removediv").style.display='none';
        	}else{
        		document.getElementById("removediv").style.display='block';
        	}
        	
        }
      function showrelatedProductsDiv(status){
       if(status){
    	  	  document.getElementById("relatedproduct_DivId").style.display = "block";
	    }
	    else {
	            document.getElementById("relatedproduct_DivId").style.display = "none";
	    }
    	}	
        window.onload = addCatalogOnLoad;
        
		

	function <portlet:namespace />selectCustomFieldName() {
				var companyId= document.getElementById('<portlet:namespace />companyId').value;
				var ecustomFieldName = document.getElementById('<portlet:namespace />ecustomFieldName').value;
				var url = '<%= resourceURL.toString() %>&companyId='+companyId+"&ecustomFieldName="+ecustomFieldName+"&<%= EECConstants.AUTOCOMPLETE %>=CUSTOMFIELD";
			
				jQuery.ajax({ type: 'POST', url: url, success: function() {
					Liferay.Portlet.refresh('#p_p_id<portlet:namespace />');
			    }});  
				
	}
			jQuery(function() {
			  jQuery(document).ready(function() {
				var ecustomFieldName = document.getElementById('<portlet:namespace />ecustomFieldName').value;
				var productDetailsId=document.getElementById("productDetailsId").value;
				var url ="<%= renderURL.toString() %>?"+ $.param( { ecustomFieldName: ecustomFieldName, productDetailsId: productDetailsId });
			    jQuery("#customFieldDiv").load(url);
			  })
			})
			
			jQuery(function() {
			  jQuery("#<portlet:namespace/>ecustomFieldName").change(function() {
				var ecustomFieldName = document.getElementById('<portlet:namespace />ecustomFieldName').value;
				var productDetailsId=document.getElementById("productDetailsId").value;
				var url ="<%= renderURL.toString() %>?"+ $.param( { ecustomFieldName: ecustomFieldName, productDetailsId: productDetailsId });
			    jQuery("#customFieldDiv").load(url);
			    jQuery("#customFieldDiv").show();
			  })
			})
</aui:script>

<aui:script use="aui-io-request">

AUI().ready('aui-base','event','node',function(A) {
 
	 A.one('#<portlet:namespace />freeShippingCheckbox').on('click', function(){ 
         var status = document.getElementById("<portlet:namespace />freeShippingCheckbox").checked;
         <portlet:namespace />showHideWeightField(status);
          });
          
 	function  <portlet:namespace />showHideWeightField(chkbox){
        if(chkbox == true) {
            document.getElementById("weightId").style.display = "none";
    	}
    	else {
            document.getElementById("weightId").style.display = "block";
    	}
    }
	
	A.one('#<portlet:namespace />similarProductsCheckbox').on('click', function(){ 
         var status = document.getElementById("<portlet:namespace />similarProductsCheckbox").checked;
         <portlet:namespace />showHideSimilarProducts(status);
	});
          
	function  <portlet:namespace />showHideSimilarProducts(chkbox){
	    if(chkbox == true) {
	          document.getElementById("relatedproduct_DivId").style.display = "block";
	  	}
	  	else {
	          document.getElementById("relatedproduct_DivId").style.display = "none";
	  	}
	}
 
	var companyId= document.getElementById('<portlet:namespace />companyId').value;
	
	var myDivA = A.one('#skuValidateMsg');
	A.one('#<portlet:namespace />sku').on('blur', function() {
	var sku = document.getElementById('<portlet:namespace />sku').value;
	var productInventoryId=document.getElementById("<portlet:namespace />productInventoryId").value;
	var url = '<%=resourceURL.toString()%>';
	
	A.io.request(
		url,
		{
			//data to be sent to server
			data: {
			<portlet:namespace />sku: sku,
			<portlet:namespace />companyId: companyId,
			<portlet:namespace />productInventoryId:productInventoryId,
			<portlet:namespace /><%=EECConstants.AUTOCOMPLETE%>: "SKU",
		},
		dataType: 'json',
		
		on: {
			failure: function() {
			},
			
			success: function() {
				var message = this.get('responseData').retVal;
				if (message) {
					document.getElementById('<portlet:namespace />sku').value = "";
									$('#skuValidateMsg').html("SKU is already Exist!");
									$('#skuValidateMsg').show();
								}
								else {
									$('#skuValidateMsg').hide();
								}
							}
							
						}
					}
					
				); //END of io Request
			
		},
		['aui-io']
	);  
	
	
            A.one('#<portlet:namespace />productNameId').on('blur', function() {
           		var productDetailsId=document.getElementById("productDetailsId").value;
				var productName = document.getElementById('<portlet:namespace />productNameId').value;
				var url = '<%=resourceURL.toString()%>';
				
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />companyId: companyId,
							<portlet:namespace />productName: productName,
							productDetailsId:productDetailsId,
							<portlet:namespace /><%=EECConstants.AUTOCOMPLETE%>: "PRODUCT_NAME",
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function() {
								var message = this.get('responseData').retVal;
								if (message) {
									document.getElementById('<portlet:namespace />productNameId').value = "";
									$('#nameValidateMsg').html("Product Name is already Exist!");
									$('#nameValidateMsg').show();
								}
								else {
									$('#nameValidateMsg').hide();
								}
							}
							
						}
					}
					
				); //END of io Request
			
		},
		['aui-io']
	);  
	
});
	
</aui:script>
<script>
	
	function addCatalogOnLoad() {
    	var parentCatalogName = document.getElementById("<portlet:namespace />catalogName").innerHTML;
    	if(parentCatalogName==''){
    		document.getElementById("removediv").style.display='none';
    	}else{
    		document.getElementById("removediv").style.display='block';
    	}
    	
    	if(<%= (isrelatedProducts)%>){
    	  	  document.getElementById("relatedproduct_DivId").style.display = "block";
	    }
	    else {
	            document.getElementById("relatedproduct_DivId").style.display = "none";
	    }
    		
    	
    }
    window.onload = addCatalogOnLoad;

</script>
<script>
function setUploadSize(fileInput)
        {   
	alert(fileInput);
	var uploadSize=true;
            var size=0;
            for(var num1=0;num1<fileInput.files.length;num1++)
            {
                  var file=fileInput.files[num1];
                     if(file.size>5242880)
                   {
                     document.getElementById('<portlet:namespace/>imageFile').focus();
                     uploadSize=false;
                   }else{
                        uploadSize=true;
                   }
                   size+=file.size;
             }
            alert(uploadSize);
          }    
</script>
	<style type="text/css">
	.ie8 #customFieldDiv {
    	display: none;
    }
.dashboard-fieldsetalign {
    margin-left: 20px !important;
}
</style>
	
