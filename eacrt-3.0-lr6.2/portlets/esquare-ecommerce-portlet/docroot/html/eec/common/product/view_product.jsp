<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ include file="/html/eec/common/product/init.jsp" %>
<%
	String plns = "";
	if (renderRequest != null) {
		plns = renderResponse.getNamespace();
	}
	
	List<ProductDetails> searchProductList=(List<ProductDetails>)request.getAttribute("SEARCH_PRODUCT_LIST");
	int catalogSize = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
	
	PortletURL actionURL = renderResponse.createActionURL();
	
	PortletURL addProductURL = renderResponse.createRenderURL();
	addProductURL.setParameter(EECConstants.CMD,EECConstants.ADD);
	
	PortletURL bulkUploadURL = renderResponse.createRenderURL();
	bulkUploadURL.setParameter(EECConstants.CMD, EECConstants.BULK_UPLOAD);
	
	PortletURL editURL = renderResponse.createRenderURL();
	editURL.setParameter(EECConstants.CMD,EECConstants.EDIT);
	
	PortletURL portleURL = renderResponse.createRenderURL();
	int count = ProductDetailsLocalServiceUtil.getProductDetailList(themeDisplay.getCompanyId()).size();
	List<ProductDetails> productList=new ArrayList<ProductDetails>();
	
	List<ProductImages> productImages=null;
	boolean isSearch = false;
	if(Validator.isNotNull(searchProductList)){
		isSearch=true;
		productList=searchProductList;
	}else if(themeDisplay.getPermissionChecker().isOmniadmin() || themeDisplay.getPermissionChecker().isCompanyAdmin() || isShopAdmin){
		productList = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(),0, count);
	}else{
		productList = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(),EECConstants.PRODUCT_VISIBLITY_TRUE,0,count);
	}
	Layout layoutname = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), true, "/catalog");
	
	int totalNoProducts = 0;
	 CartPermission cartPermissionProduct  = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.PRODUCT_COUNT);
	 if(cartPermissionProduct!=null){
		 totalNoProducts = Integer.parseInt(cartPermissionProduct.getValue());	 
	 }
	 
	 
	String msg = (String) SessionErrors.get(renderRequest, "XLS_ERROR");
%>

 <c:if test="<%= (msg !=null) %>">
<liferay-ui:error key="XLS_ERROR" message="<%=msg%>" /> 
 </c:if>
 
<div>
<c:if test='<%= SessionMessages.contains(renderRequest, "Image_Error") %>'>
<liferay-ui:message key='<%= HtmlUtil.escape((String)SessionMessages.get(renderRequest, "Image_Error")) %>'/>
</c:if>
</div>
<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle"> Products</div>
<hr style="margin-top: 13px;">

<%
if(catalogSize==0){ %>
<div id="productNameId"><div class="Helpingmessage">There are No catalogs created in this store.Before adding product you must add a 
<a href="<%=layoutname.getRegularURL(request)%>">Catalog here</a> or You can create catalog & product using<a href="<%=bulkUploadURL.toString()%>">Bulk Upload</a></div></div>
<%}else{ %>


	<aui:form method="post" name="fm1" >
		<div id="productnamefielddiv" >
		   	<div id="<portlet:namespace />productNameDivId">
	       	<table><tr><td><aui:input type="text" name="productName" id="productNameId1" onkeypress ="javascript:search(event);" label="Product Name"/></td><td>
	       	<button onclick="javascript:_product_WAR_esquareecommerceportlet_searchProduct();" name="Search" id="detail_pro_added_to_wishlist"><i class="icon-search"></i>Search</button> 
	       	<%-- <button name="Search" value="Search" id="searchproduct" onclick="javascript:<%= plns %>searchProduct();">Search</button> --%>
	       	</td></tr></table></div>
	    </div>
		</aui:form>
		<aui:form method="post" name="fm">
		<div>
			<aui:button-row>
				<div >
					<%--  <aui:button onClick= '<%= renderResponse.getNamespace() + "deleteProduct();"  %>' value="Delete"/>  --%>
					<button  onClick= '<%= renderResponse.getNamespace() + "deleteProduct();"  %>' class="productdelete">Delete</button>
					<aui:button onClick='<%=bulkUploadURL.toString()%>' value="Bulk Upload" id="addproduct"  />
					<c:if test="<%=totalNoProducts > count %>">
						<span id="dbproduct-addproduct">	
						<aui:button onClick='<%=addProductURL.toString()%>' value="Add Product"  id="addproduct"/>
						
						
						</span>
					</c:if>		
				</div>
			</aui:button-row>
			
			
			</div>
			<aui:input name="deleteProductIds" type="hidden" />
		
	
			
			<div id="viewproductdiv">
			
			<liferay-ui:search-container emptyResultsMessage='<%=(isSearch)?"No Matching Product available" :"There are no Products are available <a href="+addProductURL.toString()+">  click here </a> To Add Product"%>' delta="10" rowChecker="<%=new RowChecker(renderResponse) %>">
			<liferay-ui:search-container-results total="<%=productList.size()%>"
			results="<%=ListUtil.subList(productList, searchContainer.getStart(),
							searchContainer.getEnd())%>" />
			<liferay-ui:search-container-row className="com.esquare.ecommerce.model.ProductDetails" keyProperty="productDetailsId" modelVar="productDetails">
					<%
					ProductInventory productInventory=null;
					//Image
					String image=StringPool.BLANK;
					try{
					productInventory=ProductInventoryLocalServiceUtil.findByProductDetailsId(productDetails.getProductDetailsId());
					productImages=ProductImagesLocalServiceUtil.findByInventoryId(productInventory.getInventoryId());
					FileEntry dlFileEntry=DLAppLocalServiceUtil.getFileEntry(productImages.get(0).getImageId());
					String imageURL = "/documents/" + dlFileEntry.getGroupId() 
							+ "/" + dlFileEntry.getFolderId() + "/"
							+ dlFileEntry.getTitle()+"/"+dlFileEntry.getUuid();
					image="<div id='productimg'>"+"<img src='"+imageURL+"' alt='"+productDetails.getName()+"'  />"+"</div>";
					}catch(Exception e){}
					%>
					<liferay-ui:search-container-column-text 
					name="Image" value="<%= image %>" />
					
					<%
					// Name and Description
					String name="<div id='productName'>"+productDetails.getName()+"</div>";
					String description=StringPool.BLANK;
					if (Validator.isNotNull(productDetails.getDescription())) {
						description=productDetails.getDescription();
						if(description.length() > 130) description = description.substring(0,130)+"......";
					}
					else {
						description=PortletPropsValues.DEFAULT_ECC_CATALOG_DESCRIPTION;
					}
					%>
					<liferay-ui:search-container-column-text 
					name="name" value="<%= name %>" />
					<%
					
					%>
					<liferay-ui:search-container-column-text 
					name="description" value="<%= description %>" />
					
					<%
					//Quantity and Price
					String quantity = StringPool.BLANK;
					String price = StringPool.BLANK;
					if(Validator.isNotNull(productInventory)){
						quantity="<div class='inventory'>"+productInventory.getQuantity()+"</div>";
						price="<div class='inventory'>"+productInventory.getPrice()+"</div>";
					}
					if(Validator.isNotNull(productDetails)){
					editURL.setParameter("productDetailsId", String.valueOf(productDetails.getProductDetailsId()));
					editURL.setParameter("catalogId", String.valueOf(productDetails.getCatalogId()));
					}
					%>
					<liferay-ui:search-container-column-text 
					name="quantity" value="<%= quantity %>" />
					<liferay-ui:search-container-column-text 
					name="price" value="<%= price %>" />
					
					<liferay-ui:search-container-column-text 
					name="" href="<%= editURL.toString() %>" value="Edit"/>
				
				</liferay-ui:search-container-row>
				<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
	</div >
	</aui:form>
<%} %>
</div>
<aui:script>

	function <portlet:namespace/>searchProduct() {
	var productName=document.getElementById("<portlet:namespace />productNameId1").value;
	document.<portlet:namespace />fm1.action="<%= actionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.SEARCH %>";
	if(productName==''){
		alert('Please Select a Product To Be Searched');
	}else{
		submitForm(document.<portlet:namespace />fm1);
	}
	}
	
	function search(event){
		var productName=document.getElementById("<portlet:namespace />productNameId1").value;
		document.<portlet:namespace />fm1.action="<%= actionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.SEARCH %>";
		var code = (event.keyCode ? event.keyCode : event.which);
		   if(code == 13) { 
			   submitForm(document.<portlet:namespace />fm1);
		   }
	}
	
	Liferay.provide(
			window,
			'<portlet:namespace/>deleteProduct',
			function() {
						document.<portlet:namespace />fm.action="<%= actionURL.toString()%>" + "&<portlet:namespace /><%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
						var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fm, "<portlet:namespace />allRowIds");
						if(checkBoxValue==""||checkBoxValue==null){
								alert("Please Select atleast One Entry to Delete");
								e.preventDefault();
						}
						if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "Are You Sure Want to Delete This Selected Products?") %>')) {
						document.<portlet:namespace />fm.<portlet:namespace />deleteProductIds.value=checkBoxValue;
						submitForm(document.<portlet:namespace />fm);
				}
			},
			['liferay-util-list-fields']
		);
	
	
</aui:script>


<%-- <aui:script use="autocomplete-list,aui-base,autocomplete-filters,autocomplete-highlighters">

    var dataSource = new A.DataSource.IO(
        {
            source: '<portlet:resourceURL />'
        }
    );

    var autocomplete1 = new A.AutoComplete(
        {
            dataSource: dataSource,
            contentBox: '#<portlet:namespace />productNameDivId',
            input:'#<portlet:namespace/>productNameId1',
            matchKey: 'name',
            schema: {
                resultListLocator: 'response',
                resultFields: ['name']
            },
            schemaType:'json',
            typeAhead: true,
            focused:true
        }
    );

    autocomplete1.generateRequest = function(query) {
        return {
            request: '&q=' + query+'&<%=EECConstants.AUTOCOMPLETE %>='+"<%=EECConstants.PRODUCT %>"+'&<%=EECConstants.AUTOCOMPLETE_SEARCH %>='+"<%=EECConstants.PRODUCT %>"
        };
    }
    
    
    autocomplete1.render();
  
</aui:script> --%>
<style>
.helper-reset{
overflow: scroll;
height: 170px;
}
#detail_pro_added_to_wishlist {
    background-color: #45a5d5;
    border: 0 none;
    color: white;
    font-family: myfont !important;
    font-size: medium !important;
    height: 40px !important;
    margin-left: -2px;
    margin-top: 17px;
    min-width: 110px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s;
}
#detail_pro_added_to_wishlist:hover {
	background-color: #CDDC39;
}
#productimg > img {
    width: 60px;
}
#_product_WAR_esquareecommerceportlet_productDetailsesSearchContainer_col-name{
width: 100px;
}
#_product_WAR_esquareecommerceportlet_productDetailsesSearchContainer_col-price{
width: 70px;
}
#productnamefielddiv{
padding-left: 20px;
}

@media (max-width: 600px) {
 .aui input[type="text"]{
	border: 1px solid #ddd !important;
    border-radius: 2px !important;
    box-shadow: 0 0 2px rgba(0, 0, 0, 0.075) inset !important;
    font-family: myfont;
    font-size: small;
    height: 30px;
    width: 175px;
}
}

</style>

