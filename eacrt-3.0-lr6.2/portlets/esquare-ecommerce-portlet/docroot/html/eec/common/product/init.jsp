<%@ include file="/html/eec/common/init.jsp" %>
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/jquery-1.10.1.min.js"></script>
<link href="<%=request.getContextPath() %>/css/eec/common/product/product.css" rel="stylesheet" type="text/css" />
<%@page import="com.esquare.ecommerce.model.ProductDetails"%>
<%@page import="com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.ProductInventory"%>
<%@page import="com.esquare.ecommerce.service.ProductImagesLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.ProductImages"%>

<%@page import="com.esquare.ecommerce.model.EcustomField"%>
<%@page import="com.esquare.ecommerce.service.EcustomFieldLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.EcustomFieldValue"%>
<%@page import="com.esquare.ecommerce.service.EcustomFieldValueLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.NoSuchCatalogException"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.DuplicateProductNameException"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.ShippingRegion"%>
<%@page import="com.esquare.ecommerce.NoSuchProductDetailsException"%>
<%@page import="com.esquare.ecommerce.NoSuchProductInventoryException"%>