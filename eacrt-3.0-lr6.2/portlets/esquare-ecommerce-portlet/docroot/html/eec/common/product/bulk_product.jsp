<%@page import="com.esquare.ecommerce.util.EECConstants"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ include file="/html/eec/common/product/init.jsp" %>
<portlet:defineObjects />
<div class="file1">
This is the <b>File Upload</b> portlet in View mode.

 <% 
 PortletURL actionURL = renderResponse.createActionURL();
 actionURL.setParameter(EECConstants.CMD, EECConstants.BULK_UPLOAD);

 
 String fileURL = PortletPropsValues.DEFAULT_EXCELSHEET_DOWNLOAD;
 %>

<aui:form enctype="multipart/form-data" method="post" name="fm" action="<%=actionURL.toString() %>">
	<aui:input type="file" name="filename" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
	<input id="imageFiles" type="file" name="imageFiles" multiple="multiple" accept="image/*" onchange="getFileSizeandName(this);"/>
	<input type="hidden" name="fileNames" id="fileNames"/>
	<div id="selectedFiles"></div>
	<aui:button name="bulkUpload" type="submit" />
</aui:form>

<a href="<%= fileURL %>">Download Excel File Here</a>
</div>

<script type="text/javascript">
    var totalsizeOfUploadFiles = 0;
    function getFileSizeandName(input)
    {
    	var filename = "";
        for(var i =0; i<input.files.length; i++)
        {           
            
            filename += input.files[i].name
            if(i<input.files.length-1){
            	filename= filename+",";

            }
          
        }    
       
       document.getElementById("fileNames").value = filename;
    }

    </script>