<%@ include file="/html/eec/common/catalog/init.jsp" %>

<style type="text/css">
#sitemap li{
    font-size: 12px;
    font-weight: bold;
    }
    
#sitemap li a {
    color: highlight;
    text-decoration: none;
}

#sitemap li span, #sitemap li span.collapsed {
    margin-left: 3px;
    margin-top: -3px;
    height: 40px;
    width: 27px;
}
</style>
<%
	Catalog catalogObj = (Catalog)request.getAttribute("CATALOG");
	long catalogId = BeanParamUtil.getLong(catalogObj, request, "catalogId", EECConstants.DEFAULT_PARENT_CATALOG_ID);
	int total = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
	List<Catalog> results = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId(), 0,0,total);
%>



<div>
<div class="sidebarmenu">
<h4> Please Select the Catalog</h4>
<ul id="sitemap" style="margin-left: -10px;">
	<%
	 String subCategoryList = "";
	for (Catalog catalog : results) {
		int count = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), catalog.getCatalogId());
	%> 
	<li style="width: 100%;">
	<%if(count>0){%>
		<a href="" style="color:black;"><%= catalog.getName() %></a>
	<%}else{%>
		<a href="" onclick="return CloseMySelf('<%=catalog.getCatalogId()%>','<%=UnicodeFormatter.toString(catalog.getName())%>');"><%= catalog.getName() %></a>
	<%}
		 subCategoryList = EECCatalogUtil.getSubCatalogPopupList(themeDisplay.getCompanyId(), catalog.getCatalogId(),ctxPath);
	  %>
	<%=subCategoryList %>
	</li>
	
	<% } %>
	 
</ul>
</div>
</div> 
<br/><br/>

<script>
function CloseMySelf(catalogId,catalogName) {
   try {
        window.opener.<portlet:namespace />selectCatalog(catalogId,catalogName);
    }
    catch (err) {}
    window.close();
    return false;
}
</script>