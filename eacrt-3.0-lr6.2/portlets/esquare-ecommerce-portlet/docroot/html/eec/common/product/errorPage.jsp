<%@page import="java.nio.channels.SeekableByteChannel"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %> 
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %> 
<%@ page import="javax.portlet.PortletPreferences" %>

<portlet:defineObjects />

<% 
String msg = (String) SessionErrors.get(renderRequest, "XLS_ERROR");
%>

 <liferay-ui:error key="XLS_ERROR" message="<%=msg%>" /> 
