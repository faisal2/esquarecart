<%@ include file="/html/eec/common/product/init.jsp"%>

<div id='<%= renderResponse.getNamespace() + "dynamicFieldDiv" %>'>
	

<%
		EcustomField ecustomField=null;
		EcustomFieldValue custvalue=null;
		ProductInventory productInventory=null;
		boolean iscustomField=true;
		long customFieldRecId=0;
		String ecustomFieldName = request.getParameter("ecustomFieldName");
		long productDetailsId = ParamUtil.getLong(request, "productDetailsId");
		try{
			if(productDetailsId>0){
					productInventory=ProductInventoryLocalServiceUtil.findByProductDetailsId(productDetailsId);
					ecustomField= EcustomFieldLocalServiceUtil.findByTitle(themeDisplay.getCompanyId(), ecustomFieldName);
					custvalue=EcustomFieldValueLocalServiceUtil.findByInventoryId(themeDisplay.getCompanyId(), productInventory.getInventoryId());
					customFieldRecId=ecustomField.getCustomFieldRecId();
			}else{
				ecustomField= EcustomFieldLocalServiceUtil.findByTitle(themeDisplay.getCompanyId(), ecustomFieldName);
			}
		}catch(Exception e){
		}
	%>
	<c:if test="<%= ecustomField != null %>">
	<aui:input name="customFieldRecId" type="hidden" value="<%= ecustomField.getCustomFieldRecId() %>" />
	</c:if>
	
<% 
if(Validator.isNotNull(ecustomField) && Validator.isNull(custvalue)) {
	List<ShippingRegion> shipRegionlist = EECUtil.readXMLContent(ecustomField.getXml(),PortletPropsValues.CUSTOM_FIELD_TAG);
	 for(ShippingRegion field: shipRegionlist) {
		
		//Define Field add
	%>
<c:choose>
	<c:when test='<%= field.getKeyValue().equals("text") %>'>
		<aui:input
			label="<%= HtmlUtil.escape(field.getName()) %>"
			name="<%= field.getName() %>" value="" />
	</c:when>
	<c:when test='<%= field.getKeyValue().equals("textarea") %>'>
		<aui:input
			label="<%= HtmlUtil.escape(field.getName()) %>"
			name="<%= field.getName() %>" type="textarea" value=""
			wrap="soft" style="height: 40px; width: 200px;"/>
	</c:when>
	
</c:choose>
<% }}
	//Define Field edit
	
else if(customFieldRecId>0){ 
	EcustomField custFieldobj =EcustomFieldLocalServiceUtil.getEcustomField(customFieldRecId);
	List<ShippingRegion> ecustomFieldlist = EECUtil.readXMLContent(custFieldobj.getXml(),PortletPropsValues.CUSTOM_FIELD_TAG);
	List<ShippingRegion> ecustomValuelist = EECUtil.readXMLContent(custvalue.getXml(),PortletPropsValues.CUSTOM_FIELD_TAG);
	HashMap<String, String> productMap = new HashMap<String, String>();
		for (ShippingRegion ecustomValue : ecustomValuelist) {
		   productMap.put(ecustomValue.getName(), ecustomValue.getKeyValue());
		}
		for(ShippingRegion customField:ecustomFieldlist){
			
	%>
	<aui:input type='hidden' value='<%=custvalue.getCustomValueRecId()%>' name="customValueRecId"/>
	<aui:input name="customFieldRecId" type="hidden" value="<%= custFieldobj.getCustomFieldRecId() %>" />

	<%
	
	String value= StringPool.BLANK;
	String customValue = productMap.get(String.valueOf(customField.getId()));
	//String customValue = productMap.get(customField.getName());
	try{
		if(customValue.equals(PortletPropsValues.DEFINE_FIELDS_NOTAVAILABLE)){
			value =StringPool.BLANK;
		}
		else if(customValue!=null) {
			value = customValue;
		}
		}catch(NullPointerException nullPointerException){
			value=StringPool.BLANK;
		}
	%>
	<c:if test='<%=customField.getKeyValue().equals("text")%>'>
	
		<aui:input
			label="<%= HtmlUtil.escape(customField.getName()) %>"
			name="<%= customField.getName() %>" value="<%=value %>" />
	</c:if>
	
	<c:if test='<%=customField.getKeyValue().equals("textarea")%>'>
		<aui:input
			label="<%= HtmlUtil.escape(customField.getName()) %>"
			name="<%= customField.getName() %>" type="textarea" value="<%=value%>"
			wrap="soft" />
	</c:if>
	
			
	<%		
}				
		}
	else {
%>
<p>There are no Product Specification .....</p></fieldset>
<%
	}
%>
</div>
