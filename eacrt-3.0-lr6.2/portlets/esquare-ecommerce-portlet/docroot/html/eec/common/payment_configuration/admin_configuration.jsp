 <%@ include file="/html/eec/common/payment_configuration/init.jsp"%>

<%
long companyId= themeDisplay.getCompanyId();
List <PaymentConfiguration> payment_configurations= PaymentConfigurationLocalServiceUtil.findBycompanyId(companyId);

PaymentConfiguration paypal_configuration=null;
PaymentConfiguration other_configuration=null;
PaymentConfiguration cod_configuration=null;
String payPalMerchantId="";
String otherPaymentType="";
String otherMerchantId="";
String otherSecretKey="";
boolean otherActive=Boolean.FALSE;
boolean payPalActive=Boolean.FALSE;
boolean codActive=Boolean.FALSE;
for( PaymentConfiguration pConfiguration: payment_configurations){
 if(pConfiguration.getPaymentGatewayType().equalsIgnoreCase(EECConstants.PAYPAL)){
	 payPalMerchantId=pConfiguration.getMerchantId();
	 payPalActive=pConfiguration.getStatus();
 }

 if(pConfiguration.getPaymentGatewayType().equalsIgnoreCase(EECConstants.EBS)||pConfiguration.getPaymentGatewayType().equalsIgnoreCase(EECConstants.CC_AVENUE)){
	 otherPaymentType=pConfiguration.getPaymentGatewayType();
	 otherMerchantId=pConfiguration.getMerchantId();
	 otherSecretKey=pConfiguration.getSecretKey();
	 otherActive=pConfiguration.getStatus();
 }
 
 if(pConfiguration.getPaymentGatewayType().equalsIgnoreCase(EECConstants.COD)){
			codActive = pConfiguration.getStatus();
		}

	}
%>
<liferay-portlet:renderURL var="editPageURL">
<liferay-portlet:param name="jspPage" value="/html/eec/common/payment_configuration/edit_configuration.jsp"/>
</liferay-portlet:renderURL>
<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle">Payment Configuration</div>
<hr id="top1">
<input type="button" value="Edit" class="nextdomain" onClick="location.href='<%=editPageURL.toString()%>';" />
<br/>
<br/> 
<%-- <aui:button name="edit" type="button" value="edit" href="<%=editPageURL.toString()%>" id="button1" /> --%>
<liferay-ui:panel-container>
	<liferay-ui:panel title="Paypal Details" collapsible="true"
		extended="true"> 
		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
					Merchant Id:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=payPalMerchantId.equals(StringPool.BLANK)?"N/A":payPalMerchantId%>
			</aui:column>
		</aui:layout>

		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
			Status:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=payPalActive?"Enabled":"Disabled"%>
		</aui:column>

		</aui:layout>
	</liferay-ui:panel><br/>
	<liferay-ui:panel title="Other Payment Details" collapsible="true"
		extended="true">
		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
					Payment Type:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=otherPaymentType.equals(StringPool.BLANK)?"N/A":otherPaymentType%>
			</aui:column>
		</aui:layout>
		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
					Merchant Id:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=otherMerchantId.equals(StringPool.BLANK)?"N/A":otherMerchantId%>
			</aui:column>
		</aui:layout>
		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
					Secret Key:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=otherSecretKey.equals(StringPool.BLANK)?"N/A":otherSecretKey%>
			</aui:column>
		</aui:layout>

		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
			        Status:
		</aui:column>

			<aui:column columnWidth="75" last="true">
		<%=otherActive?"Enabled":"Disabled"%>
		</aui:column>

		</aui:layout>
	</liferay-ui:panel><br/>
	<liferay-ui:panel title="Cash On Delivery Details" collapsible="true"
		extended="true">
		<aui:layout id="web1">
			<aui:column columnWidth="25" first="true">
					Status:
		</aui:column>

			<aui:column columnWidth="75" last="true">
			<%=codActive?"Enabled":"Disabled"%>
			</aui:column>
		</aui:layout>
		
	</liferay-ui:panel>
</liferay-ui:panel-container>
</div>