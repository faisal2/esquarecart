<%@ include file="/html/eec/common/payment_configuration/init.jsp"%>

<%
	long companyId = themeDisplay.getCompanyId();
	List<PaymentConfiguration> payment_configurations = PaymentConfigurationLocalServiceUtil
			.findBycompanyId(companyId);

	PaymentConfiguration paypal_configuration = null;
	PaymentConfiguration other_configuration = null;
	PaymentConfiguration cod_configuration = null;
	String payPalMerchantId = "";
	String otherPaymentType = "";
	String otherMerchantId = "";
	String otherSecretKey = "";
	String paypalSecretKey = "";
	String paypalWorkingKey = "";
	String accessCode = "";

	long payPalId = 0l;
	long otherId = 0l;
	long codId = 0l;
	boolean otherActive = Boolean.FALSE;
	boolean payPalActive = Boolean.FALSE;
	boolean codActive = Boolean.FALSE;
	for (PaymentConfiguration pConfiguration : payment_configurations) {
		if (pConfiguration.getPaymentGatewayType().equalsIgnoreCase(
				EECConstants.PAYPAL)) {
			payPalId = pConfiguration.getId();
			payPalMerchantId = pConfiguration.getMerchantId();
			payPalActive = pConfiguration.getStatus();
			paypalSecretKey = pConfiguration.getSecretKey();
			paypalWorkingKey = pConfiguration.getWorkingKey();
		}
		if (pConfiguration.getPaymentGatewayType().equalsIgnoreCase(
				EECConstants.EBS)
				|| pConfiguration.getPaymentGatewayType()
						.equalsIgnoreCase(EECConstants.CC_AVENUE)) {
			otherId = pConfiguration.getId();
			otherPaymentType = pConfiguration.getPaymentGatewayType();
			otherMerchantId = pConfiguration.getMerchantId();
			otherSecretKey = pConfiguration.getSecretKey();
			accessCode = pConfiguration.getWorkingKey();
			otherActive = pConfiguration.getStatus();
		}

		if (pConfiguration.getPaymentGatewayType().equalsIgnoreCase(
				EECConstants.COD)) {
			codId = pConfiguration.getId();
			codActive = pConfiguration.getStatus();
		}
	}
%>
<liferay-portlet:actionURL var="updatePaymentConfiurationsURL" />
<div class="dashboard-maindiv2">
	<div class="dashboard-pagemainheading70" id="dashboard-maindiv">Edit
		Payment Configuration</div>
	<aui:form action="<%=updatePaymentConfiurationsURL.toString()%>"
		method="post" name="updatePaymentConfiurationsForm">
		<aui:input type="hidden" name="paypal_configuration_id"
			value="<%=payPalId%>" />
		<aui:input type="hidden" name="other_configuration_id"
			value="<%=otherId%>" />
		<aui:input type="hidden" name="cod_configuration_id"
			value="<%=codId%>" /><br/>
		<liferay-ui:panel-container>
			<liferay-ui:panel title="Paypal Details" collapsible="true"
				extended="true">
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Merchant Id:
			</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:input name="payPalMerchantId" type="text"
							value="<%=payPalMerchantId%>" label="">
							<aui:validator name="custom"
								errorMessage="Please enter payPalMerchantId">
		function (val, fieldNode, ruleValue) {
		var payPalActive = document.getElementById("<portlet:namespace />payPalActive").value; 
		var isTrue = (payPalActive.toLowerCase() === 'true');
			if(isTrue && val=="")
			{
				return false;
			}
				return true;
			}
		</aui:validator>
						</aui:input>
					</aui:column>
				</aui:layout>
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Secret Key:
		</aui:column>
					<aui:column columnWidth="75" last="true">
						<aui:input name="paypalSecretKey" value="<%=paypalSecretKey%>"
							label=""></aui:input>
					</aui:column>
				</aui:layout>
				<aui:layout>
					<aui:column columnWidth="25" first="true">
				Certificate Id :
		</aui:column>
					<aui:column columnWidth="75" last="true">
						<aui:input name="certificateId" value="<%=paypalWorkingKey%>"
							label=""></aui:input>
					</aui:column>

				</aui:layout>

				<aui:layout>
					<aui:column columnWidth="25" first="true">
			Status:
		</aui:column>
					<aui:column columnWidth="75" last="true">
						<aui:select name="payPalActive" label="">
							<aui:option selected="<%=payPalActive%>" value="<%=true%>">Enable</aui:option>
							<aui:option selected="<%=!payPalActive%>" value="<%=false%>">Disable</aui:option>
						</aui:select>
					</aui:column>

				</aui:layout>
			</liferay-ui:panel><br/>
			<liferay-ui:panel title="Other Payment Details" collapsible="true"
				extended="true">
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Payment Type:
		</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:select name="otherPaymentType" label="">

							<%
								for (int i = 0; i < EECConstants.otherPaymentType.length; i++) {
							%>

							<aui:option label="<%=EECConstants.otherPaymentType[i]%>"
								selected="<%=otherPaymentType
												.equals(EECConstants.otherPaymentType[i])%>" />

							<%
								}
							%>
						</aui:select>
					</aui:column>
				</aui:layout>
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Merchant Id:
		</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:input name="otherMerchantId" type="text"
							value="<%=otherMerchantId%>" label="">
							<aui:validator name="custom"
								errorMessage="Please enter merchantId">
		function (val, fieldNode, ruleValue) {
		var otherActive = document.getElementById("<portlet:namespace />otherActive").value; 
		var isTrue = (otherActive.toLowerCase() === 'true');
			if(isTrue && val=="")
			{
				return false;
			}
				return true;
			}
		</aui:validator>
						</aui:input>
					</aui:column>
				</aui:layout>
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Secret Key:
		</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:input name="otherSecretKey" type="text"
							value="<%=otherSecretKey%>" label="">
							<aui:validator name="custom"
								errorMessage="Please enter secretKey">
		function (val, fieldNode, ruleValue) {
		var otherActive = document.getElementById("<portlet:namespace />otherActive").value; 
		var isTrue = (otherActive.toLowerCase() === 'true');
			if(isTrue && val=="")
			{
				return false;
			}
				return true;
			}
		</aui:validator>
						</aui:input>
					</aui:column>
				</aui:layout>
				<%-- <aui:layout>
			<aui:column columnWidth="25" first="true">
			        Working Key:
		</aui:column>
		<aui:column columnWidth="75" last="true">
			<aui:input name="otherWorkingKey" type="text" value="<%=accessCode%>" label="">
			</aui:input>
			</aui:column>
		</aui:layout> --%>
				<aui:layout>
					<aui:column columnWidth="25" first="true">
			        Status:
		</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:select name="otherActive" label="">
							<aui:option selected="<%=otherActive%>" value="true">Enable</aui:option>
							<aui:option selected="<%=!otherActive%>" value="false">Disable</aui:option>
						</aui:select>
					</aui:column>

				</aui:layout>
			</liferay-ui:panel><br/>
			<liferay-ui:panel title="Cash On Delivery Details" collapsible="true"
				extended="true">
				<aui:layout>
					<aui:column columnWidth="25" first="true">
					Status:
		</aui:column>

					<aui:column columnWidth="75" last="true">
						<aui:select name="codActive" label="">
							<aui:option selected="<%=codActive%>" value="true">Enable</aui:option>
							<aui:option selected="<%=!codActive%>" value="false">Disable</aui:option>
						</aui:select>
					</aui:column>
				</aui:layout>

			</liferay-ui:panel>
		</liferay-ui:panel-container>
		<div class="dashboard-buttons">
		<div class="sub">
			<aui:button type="submit" value="Save"
				style="margin-top: 15px; margin-bottom: 17px; margin-left: 10px;" /></div>
		</div>
	</aui:form>
</div>