<%@ include file="/html/eec/common/init.jsp" %>

<%@ page import="com.esquare.ecommerce.model.ProductDetails"%>
<%@ page import="com.esquare.ecommerce.service.ProductDetailsLocalServiceUtil"%>

<%@ page import="com.liferay.portal.service.CompanyLocalServiceUtil"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>

<%@ page import="com.esquare.ecommerce.model.Coupon"%>
<%@ page import="com.esquare.ecommerce.service.CouponLocalServiceUtil" %>

<%@ page import="com.esquare.ecommerce.model.Order"%>
<%@ page import="com.esquare.ecommerce.service.OrderLocalServiceUtil" %>

<%@ page import="com.esquare.ecommerce.model.OrderItem"%>
<%@ page import="com.esquare.ecommerce.service.OrderItemLocalServiceUtil" %>

<%@ page import="com.esquare.ecommerce.service.InstanceLocalServiceUtil" %>

<%@ page import="com.esquare.ecommerce.model.CartPermission"%>
<%@ page import="com.esquare.ecommerce.service.CartPermissionLocalServiceUtil" %>

<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>

<%@ page import="com.esquare.ecommerce.dashboard.util.DashboardUtil" %>
<%@ page import="com.esquare.ecommerce.model.PaymentConfiguration"%>
<%@ page import="com.esquare.ecommerce.service.PaymentConfigurationLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.service.ProductInventoryLocalServiceUtil"%>

