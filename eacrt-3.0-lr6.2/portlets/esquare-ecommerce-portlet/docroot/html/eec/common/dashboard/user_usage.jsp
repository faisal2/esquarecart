<%
if (totalNoUsers==0){
	if(usedUsers==0){
		unlimited_endValue = 1;
	}else {
		unlimited_endValue = usedUsers * 2;
	}	
}else{
	interval = totalNoUsers/5;
	first_part= (int)(totalNoUsers * 0.4);
	second_part = 2 * first_part;
}
%>

<script type="text/javascript">

$(function () 	{
	$("#<portlet:namespace />chartContainerUser").dxCircularGauge({
	scale: {
		startValue: 0,
		endValue: <%= totalNoUsers %> ,
		majorTick: { tickInterval: <%= interval %> }
	},
	rangeContainer: {
		backgroundColor: "none",
		ranges: [
			{
				startValue: 0,
				endValue: <%= first_part %> ,
				color: "#A6C567"
			},
			{
				startValue: <%= first_part %> ,
				endValue: <%= second_part %> ,
				color: "#FCBB69"
			},
			{
				startValue: <%= second_part %>,
				endValue: <%= totalNoUsers %>,
				color: "#E19094"
			}
		]
	},
	needles: [{value: <%= usedUsers%>}],
	markers: [{value: <%= usedUsers%>,color: "#968F89"}],	
    commonMarkerSettings: {
      text: { 
        format : 'fixedPoint'      
      } 
    }
 });
	
	$("#unlimitedUserchartContainer").dxLinearGauge({
		geometry: {
			orientation: "horizontal"
		},	
		scale: {
			startValue: 0,
			endValue: <%= unlimited_endValue%>,
			majorTick: {
				showCalculatedTicks: false,
				customTickValues: [0]
			}
		},	
		rangeContainer: {
			ranges: [
				{
					startValue: 0,
					endValue: <%= unlimited_endValue%>,
					color: "#E19094"
				}
			]
		},	
		needles: [{ value: <%= usedUsers%> }],	
		markers: [{ value: <%= usedUsers%> ,color : "#FCBB69"}],	
		rangeBars: [{ value: <%=usedUsers%> ,color : "#A6C567"}],		
	    commonMarkerSettings: {
	        text: { 
	          format : 'fixedPoint'      
	        } 
	      }
	});
});
</script>

