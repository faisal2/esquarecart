<%@page import="javax.portlet.PortletPreferences"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@page import="com.esquare.ecommerce.util.EECConstants"%>
<%@page import="com.liferay.portal.kernel.util.PrefsParamUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<portlet:defineObjects />

<%

  PortletPreferences preferences = null;
   String portletResource = ParamUtil.getString(request, "portletResource");

    if (Validator.isNotNull(portletResource)) {
		preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
	}
	String report = PrefsParamUtil.getString(preferences, renderRequest, "report");	
	
%>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />

<aui:form action="<%= configurationURL %>" method="post" name="fm">
	
	
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants
	.USAGE %>" id="id_radio1" <%= (report.equalsIgnoreCase(EECConstants.USAGE))? "checked":"" %>/> Usage</br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.ORDER_COUNT%>" id="id_radio2" <%= (report.equalsIgnoreCase(EECConstants.ORDER_COUNT))? "checked":"" %>/> Order Count</br>
		
<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>
