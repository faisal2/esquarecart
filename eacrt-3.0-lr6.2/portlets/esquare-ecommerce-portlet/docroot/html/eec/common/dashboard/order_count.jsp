

<%@page import="java.text.DecimalFormat"%>
<%@page import="com.esquare.ecommerce.service.OrderItemLocalServiceUtil"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>

<%@ page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.ParseException"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@ page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@ page import="com.esquare.ecommerce.model.Order"%>
<%@ page import="com.esquare.ecommerce.service.OrderLocalServiceUtil" %>

<%@ page import="java.util.List" %>
<%@ page import="com.esquare.ecommerce.util.EECConstants"%>
<link href="<%=request.getContextPath() %>/css/eec/common/dashboard/dashboard.css" rel="stylesheet" type="text/css" media="screen" />
<%
//Current Instance ***
String currentInstance = themeDisplay.getCompany().getWebId();

String currentPackage = InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId()).getPackageType();

 //Today Amount Of Order
 double todayAmountOrder = 0.00;
 todayAmountOrder = (double)OrderItemLocalServiceUtil.getTodayOrderAmount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());

 //Yesterday Amount of Order
 double yesterdayAmountOrder = 0.00;
 yesterdayAmountOrder = (double)OrderItemLocalServiceUtil.getYesterdayOrderAmount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 DecimalFormat twoDForm = new DecimalFormat("##.##"); 
double percent = 100.00;
	if(yesterdayAmountOrder==0 && todayAmountOrder==0){
		percent = 0.00;
	}else if(yesterdayAmountOrder==0 || todayAmountOrder==0){
		if (yesterdayAmountOrder < todayAmountOrder){
			percent = 100.00;
		}else{
			percent = -100.00;	
		}		
	}else{
		if (yesterdayAmountOrder < todayAmountOrder){
			percent = (( todayAmountOrder - yesterdayAmountOrder ) / yesterdayAmountOrder ) * 100 ;
		}else{
			percent = -(( yesterdayAmountOrder -  todayAmountOrder) / yesterdayAmountOrder ) * 100 ;
		}		
	}

//Today purchased Order Item 
int todayPurchasedOrderItem = 0;
todayPurchasedOrderItem = OrderItemLocalServiceUtil.getTodayPurchasedOrderItem(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
//Today Purchased User Count
int todayPurchasedUserCount =0;
todayPurchasedUserCount = OrderLocalServiceUtil.getTodayPurchasedUserCount(EECConstants.STATUS_CHECKOUT, themeDisplay.getCompanyId());

 //Today order count
 int todayOrderCount = 0;
 todayOrderCount = OrderLocalServiceUtil.getTodayOrderCount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 
//Last week each day count
Map<String,String> eachDay =  OrderLocalServiceUtil.getLastWeekEachDayCount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
Iterator iter = eachDay.entrySet().iterator();
String data ="";
while (iter.hasNext()) {
	Map.Entry mEntry = (Map.Entry) iter.next();
	 data += "{Date:\""+mEntry.getKey()+"\",Order:"+mEntry.getValue()+"},";	 
}
data = data.substring(0, data.length() - 1);
%>


<table width="100%" align="center" >	
	<%-- <tr>	    
		<th colspan="3" id="dashboard-breadcrum"> <span> DASHBOARD</span>
		<span style="float:right;"><%= currentInstance %> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Current Package  : <span style="color:#A72A30;"> <%= currentPackage %> </span></span></th>
	</tr>
	<tr>
		<td><br/>
		</td>
	</tr> --%>
	<tr>
		<td width="31%">
			 <table class="dash-table1 heightrow1">
			 	<tr>
					<th class="dash-boxheader"><span> Sales Today</span>
						<% if (percent>0) {%>
						<span id="dash-salepert"><%= Double.valueOf(twoDForm.format(percent))%> %</span>
						<% }else{%>
						<span id="dash-saleperzero"><%= Double.valueOf(twoDForm.format(percent))%> %</span>	
						<%}%>
					</th>
				</tr> 
				<tr>
					<td><span id="dash-saletoday"><%=currencyFormat.format(todayAmountOrder)%></span></td>					
				</tr>
				<tr>
					<td><span id="dash-saletodaytotal">TOTAL</span></td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree" style="margin: 25px 0 -12px -20px;">
							<tr>
							  <td><%=todayPurchasedUserCount %></td>
							  <td><%=todayPurchasedOrderItem %></td>
							  <td><%= todayOrderCount%></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span>Customers</span></td>
							  <td class="dash-row3"><span>Product</span></td>
							  <td class="dash-row3"><span>Orders</span></td>
						 	</tr>
						</table>
					</td>
				</tr>								 				
			</table>
		</td>
		<td width="3%"></td>
		<td width="66%">
			<table class="dash-table1 heightrow1">
 				<tr>
					<th class="dash-boxheader">Last 7 days Orders</th>
				</tr>
				<tr>
					<td>
						<div id="chartContainer" class="dash-sevendayorder"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<script type="text/javascript">
var dataSource = [<%=data%>];
$("#chartContainer").dxChart({
	
	commonSeriesSettings: {
		label: { visible: true }
	},  
	tooltip: {
        enabled: true,
        customizeText: function () {
            return this.valueText;
        }
    },
    valueAxis: {
	    title: {
	         text: "Order Count"
	     },
	     min: 0
    },
    dataSource: dataSource, 
    series: {
        argumentField: "Date",
        valueField: "Order",
        name: "Last week",
        type: "line",
        color: "#88A60A"
    }
});
</script>


	



	
	







