<%@ include file="/html/eec/common/dashboard/init.jsp" %>
<link href="<%=request.getContextPath() %>/css/eec/common/dashboard/dashboard.css" rel="stylesheet" type="text/css" media="screen" />



<%


//Used Catalog count
int usedCatelogs = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(),0);

//Over All Product counts
 int usedProducts = 0;
 List<ProductDetails> productDetails = ProductDetailsLocalServiceUtil.getProductDetailList(themeDisplay.getCompanyId()); 
 if(productDetails!=null){
	 usedProducts = productDetails.size();
 } 

//Visibility Product Counts
int  visibilityProductCounts = 0;
List<ProductDetails> visibilityProduct = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(),true,-1,-1);
if(visibilityProduct!=null){
	visibilityProductCounts = visibilityProduct.size();
 }

//InVisibility Products Counts
int  inVisibilityProductCounts = 0; 
List<ProductDetails> inVisibilityProduct  = ProductDetailsLocalServiceUtil.getProductDetails(themeDisplay.getCompanyId(),false,-1,-1);
if(inVisibilityProduct!=null){
	inVisibilityProductCounts = inVisibilityProduct.size();
 }

// Used Users
int usedUsers = UserLocalServiceUtil.getCompanyUsersCount(themeDisplay.getCompanyId())-3;

//Total Number of Coupons 
 int totalNoOfCoupons = 0;
 DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Coupon.class);
 dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId",themeDisplay.getCompanyId())); 
 List<Coupon> couponList= CouponLocalServiceUtil.dynamicQuery(dynamicQuery);
 if(couponList!=null){
	 totalNoOfCoupons = couponList.size();
}
 
 //Total Number of Details 
 int totalNoUsers = 0;
 CartPermission cartPermissionUsers = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.MAX_USER);
 if(cartPermissionUsers!=null){
	 totalNoUsers = Integer.parseInt(cartPermissionUsers.getValue());	
 }
 
 
 int totalNoProducts = 0;
 CartPermission cartPermissionProduct  = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.PRODUCT_COUNT);
 if(cartPermissionUsers!=null){
	 totalNoProducts = Integer.parseInt(cartPermissionProduct.getValue());	 
 }

 
 int totalNoCatalogs = 0;
 CartPermission cartPermissionCatalog = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.CATALOG_COUNT);
 if(cartPermissionUsers!=null){
	 totalNoCatalogs = Integer.parseInt(cartPermissionCatalog.getValue()); 
	 
 }

//Number of Order
long totalNoOfOrder = 0; 
totalNoOfOrder=OrderLocalServiceUtil.getTotalOrderCount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 //Number of purchased products
 int noOfPurchasedProducts = 0; 
 noOfPurchasedProducts = OrderItemLocalServiceUtil.getPurchasedProductCount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 
 int lastWeekPurchasedProductCount = 0;
 lastWeekPurchasedProductCount = OrderItemLocalServiceUtil.getPurchasedLastweekMonthProductCount(7,EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 
 int lastMonthPurchasedProductCount =0;
 lastMonthPurchasedProductCount = OrderItemLocalServiceUtil.getPurchasedLastweekMonthProductCount(30,EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 
 
 //Total Amount of Orders
 double totalAmountOfOrder = 0l; 
 totalAmountOfOrder = (double) OrderItemLocalServiceUtil.getTotalAmountOrder(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
 
 
//last week Order Count
int lastWeekOrderCount = 0;
 lastWeekOrderCount = OrderLocalServiceUtil.getLastNoOfDaysOrderCount(7, EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());

//last month Order Count
	int lastMonthCount = 0;
 lastMonthCount = OrderLocalServiceUtil.getLastNoOfDaysOrderCount(30, EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());

 
 //Payment Configuration
List<PaymentConfiguration> paymentConfigList =  PaymentConfigurationLocalServiceUtil.findBycompanyId(themeDisplay.getCompanyId()); 
 
//Stock details 
int outOfStock=0; 
int lessStock =0;
outOfStock = ProductInventoryLocalServiceUtil.getStockCount(themeDisplay.getCompanyId(),0 );
lessStock = ProductInventoryLocalServiceUtil.getStockCount(themeDisplay.getCompanyId(),Integer.parseInt(EECConstants.LESS_STOCK));

//Last WEEK OrderAmountDaywise
Map<String,String> lastWeekOrderAmountDaywise =  OrderItemLocalServiceUtil.getLastWeekMonthOrderAmountDaywise(7,EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());
Iterator lsamout = lastWeekOrderAmountDaywise.entrySet().iterator();

//Last Week Amount of Order 
double lastWeekAmountOfOrder = OrderItemLocalServiceUtil.getLastWeekMonthOrderAmount(7,EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId()); 

//Last MONTH OrderAmountDaywise
double lastMonthAmountOfOrder =OrderItemLocalServiceUtil.getLastWeekMonthOrderAmount(30,EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId()); 

long layoutId = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), true, "/product").getPlid();
PortletURL productLessStockURL = PortletURLFactoryUtil.create(request,"product_WAR_esquareecommerceportlet", layoutId, ActionRequest.ACTION_PHASE);
productLessStockURL.setWindowState(WindowState.NORMAL);
productLessStockURL.setPortletMode(PortletMode.VIEW);
productLessStockURL.setParameter("cmd", "stockAction");

//Order Tracker Status Count
int order_status_processing = 0;
int order_status_onshipping = 0;
int order_status_delivered = 0;
int order_status_cancelled = 0;
int order_status_return = 0;
int order_status_cancel = 0;

order_status_processing = OrderItemLocalServiceUtil.getOrderItemStatusCount(EECConstants.ORDER_STATUS_PROCESSING, themeDisplay.getCompanyId());

order_status_onshipping = OrderItemLocalServiceUtil.getOrderItemStatusCount(EECConstants.ORDER_STATUS_ONSHIPPING, themeDisplay.getCompanyId());

order_status_delivered = OrderItemLocalServiceUtil.getOrderItemStatusCount(EECConstants.ORDER_STATUS_DELIVERED, themeDisplay.getCompanyId());

order_status_cancelled  = OrderItemLocalServiceUtil.getOrderItemStatusCount(EECConstants.ORDER_STATUS_CANCELLED, themeDisplay.getCompanyId());

order_status_return  = OrderItemLocalServiceUtil.getOrderItemStatusCount(EECConstants.ORDER_STATUS_RETURN, themeDisplay.getCompanyId());



 //Interval Logic
 int interval=0;
 int first_part=0;
 int second_part=0;	 
 int unlimited_endValue = 1;
 
 String report = PrefsParamUtil.getString(preferences, renderRequest, "report");
%>
<%
if(report.equalsIgnoreCase(EECConstants.ORDER_COUNT)) {
%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/eec/common/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/eec/common/dashboard/globalize.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/eec/common/dashboard/dx.chartjs.js"></script>
<%@ include file="/html/eec/common/dashboard/order_count.jsp" %>
<%}


	if(report.equalsIgnoreCase(EECConstants.USAGE))
	{
		
%>
	<%@ include file="/html/eec/common/dashboard/user_usage.jsp" %>
	<%@ include file="/html/eec/common/dashboard/catalog_usage.jsp" %>
	<%@ include file="/html/eec/common/dashboard/product_usage.jsp" %>
<br/>
<table width="100%" align="center">

	
	<tr>
		<td width="30%">
		
 			<table class="dash-table1 heightrow2">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="user-usage" /></th>
				</tr> 
				<tr>
					<td align="center">
						<% if(totalNoUsers==0) {%>
						<div id="unlimitedUserchartContainer" class="dash-totaluserzero"></div>
						<%}else{ %>
						<div id="<portlet:namespace />chartContainerUser" class="dash-totaluser"></div>
						<%} %>
					</td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%= totalNoUsers==0 ? "" :totalNoUsers %></td>
							  <td><%= usedUsers%></td>
							  <td><%= totalNoUsers==0 ? "" :totalNoUsers - usedUsers%></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span><%= totalNoUsers==0 ? "Unlimited" :"Max"%></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="used" /></span></td>
							  <td class="dash-row3"><span><%= totalNoUsers==0 ? "   " :"Avail"%></span></td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
			
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow2">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="product-usage" /></th>
				</tr>
				<tr>
					<td align="center">
						<% if(totalNoProducts==0) {%>
						<div id="unlimitedProductchartContainer" class="dash-totaluserzero"></div>
						<%}else{ %>
						<div id="<portlet:namespace />chartContainerProduct" class="dash-totaluser"></div>
						<%} %>
					</td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%= totalNoProducts==0 ? "":totalNoProducts %></td>
							  <td><%= usedProducts%></td>
							  <td><%= totalNoProducts==0 ? "":totalNoProducts - usedProducts%></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span><%= totalNoProducts==0 ? "Unlimited":"Max"%></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="used" /></span></td>
							  <td class="dash-row3"><span><%= totalNoProducts==0 ? "   ":"Avail"%></span></td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow2">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="catalog-usage" /></th>
				</tr>
				<tr>
					<td align="center">
						<% if(totalNoCatalogs==0) {%>
						<div id="unlimitedCatalogchartContainer" class="dash-totaluserzero"></div>
						<%}else{ %>
						<div id="<portlet:namespace />chartContainerCatalog" class="dash-totaluser"></div>
						<%} %>
					</td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%= totalNoCatalogs == 0 ? "" :totalNoCatalogs %></td>
							  <td><%= usedCatelogs%></td>
							  <td><%= totalNoCatalogs == 0 ? "" :totalNoCatalogs - usedCatelogs%></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span><%= totalNoCatalogs == 0 ? "Unlimited" :"Max"%></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="used" /> </span></td>
							  <td class="dash-row3"><span><%= totalNoCatalogs == 0 ? "   ":"Avail"%></span></td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
	<tr>
		<td><br/></td>
	</tr>
	<tr>
		<td width="30%">
 			<table class="dash-table1 heightrow3">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="no-of-order" /></th>
				</tr> 				
				<tr>
					<td><span class="dash-totalorder-sale-invoice"><%=totalNoOfOrder %></span></td>					
				</tr>
				<tr>
					<td><span class="dash-total"><liferay-ui:message key="total" /></span></td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%=lastWeekOrderCount %></td>
							  <td><%=lastMonthCount %></td>
							  <td></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span><liferay-ui:message key="last-7-days" /></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="last-30-days" /></span></td>
							  <td class="dash-row3"><span></span></td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow3">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="no-of-products-sold" /></th>
				</tr>				
				<tr>
					<td><span class="dash-totalorder-sale-invoice"><%=noOfPurchasedProducts %></span></td>					
				</tr>
				<tr>
					<td><span  class="dash-total"><liferay-ui:message key="total" /></span></td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%=lastWeekPurchasedProductCount %></td>
							  <td><%=lastMonthPurchasedProductCount %></td>
							  <td></td>
							</tr>
		             		<tr>
							  <td class="dash-row3"><span><liferay-ui:message key="last-7-days" /></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="last-30-days" /></span></td>
							  <td class="dash-row3"></td>
						 	</tr>
						</table>
					</td>
				</tr>								
			</table>
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow3">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="sales-invoice" /></th>
				</tr>
				<tr>
					<td><span class="dash-totalorder-sale-invoice"><%=currencyFormat.format(totalAmountOfOrder/100000) %> L</span></td>					
				</tr>
				<tr>
					<td><span class="dash-total"><liferay-ui:message key="total" /></span></td>
				</tr>
				<tr>
					<td>
						<table id="dash-tablerowthree">
							<tr>
							  <td><%=currencyFormat.format(lastWeekAmountOfOrder /100000) %> L</td>
							  <td><%=currencyFormat.format(lastMonthAmountOfOrder  / 100000)%> L</td>							  
							  <td></td>
							</tr>
		             		<tr>
		             		  <td class="dash-row3"><span><liferay-ui:message key="last-7-days" /></span></td>
							  <td class="dash-row3"><span><liferay-ui:message key="last-30-days" /></span></td>							 
							  <td class="dash-row3"><span></span></td>
						 	</tr>
						</table>
					</td>
				</tr>				
			</table>
		</td>
	</tr>	
	
	<tr>
		<td><br/></td>
	</tr>
	<tr>
		<td width="30%">
 			<table class="dash-table1 heightrow4">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="payment-configuration" /></th>
				</tr> 
				<tr>
					<td align="center">
					<div class="row4-divheight">
<%
String Enabled= "<img src= "+themeDisplay.getPathThemeImages()+"/eec/common/enable.png >";
String Disabled= "<img src= "+themeDisplay.getPathThemeImages()+"/disabled.png >";
%>
							
					    <table  class="row4-tablewidth">
					    		<tr class="dash-titlebackgroundcolor">
					    			<td class="dash-titlecolor"><liferay-ui:message key="name" /></td>
					    			<td class="dash-row3" ></td>
					    			<td class="dash-titlecolor"><liferay-ui:message key="status" /></td>
					    		</tr>
					    		<tr class="row4-rowheight">
								    	<td align = "left"><span class="dash-pay-stock"><liferay-ui:message key="paypal" /></span></td>
								    	<td class="dash-row3" align="center">:</td>
								    	<td class="dash-row3" align="center"><%=!paymentConfigList.isEmpty()?(paymentConfigList.get(0).getStatus()?Enabled:Disabled):Disabled %></td>
							    	</tr>
						    	<tr class="row4-rowheight">
								    	<td align = "left"><span class="dash-pay-stock"><%=!paymentConfigList.isEmpty() && paymentConfigList.get(1).getStatus()?paymentConfigList.get(1).getPaymentGatewayType().toUpperCase():"OTHER PAYMENT"%></span></td>
								    	<td class="dash-row3" align="center">:</td>
								    	<td class="dash-row3" align="center"><%=!paymentConfigList.isEmpty()?(paymentConfigList.get(1).getStatus()?Enabled:Disabled):Disabled %></td>
							    </tr>
							    <tr class="row4-rowheight">
								    	<td align = "left"><span class="dash-pay-stock"><liferay-ui:message key="cash-on-delivery" /></span></td>
								    	<td class="dash-row3" align="center">:</td>
								    	<td class="dash-row3" align="center"><%=!paymentConfigList.isEmpty()?(paymentConfigList.get(2).getStatus()?Enabled:Disabled):Disabled %></td>
							    </tr>					    	
					    </table>	
					    </div>					
					</td>
				</tr>
			</table>
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow4">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="stock-details" /></th>
				</tr>
				<tr>
					<td align="center">
						<div class="row4-divheight">
						   <table   class="row4-tablewidth">					   
						    		<tr class="dash-titlebackgroundcolor">
						    			<td class="dash-titlecolor"><liferay-ui:message key="criteria" /></td>
						    			<td class="dash-row3" ></td>
						    			<td class="dash-titlecolor"><liferay-ui:message key="count" /></td>
						    		</tr>
						    		<tr class="row4-rowheight">
								    	<td align = "left"><a href="<%=productLessStockURL.toString() %>&outofStock=0"><span class="dash-pay-stock"><liferay-ui:message key="out-of-stock" /></span></a></td>
								    	<td class="dash-row3" align="center">:</td>
								    	<td class="dash-row3" align="center"><a href="<%=productLessStockURL.toString() %>&outofStock=0"><%=outOfStock %></a></td>
							    	</tr>
							    	<tr class="row4-rowheight">
								    	<td align = "left"><a href="<%=productLessStockURL.toString() %>&lessStock=<%=EECConstants.LESS_STOCK%>"><span class="dash-pay-stock"><liferay-ui:message key="less-stock" /></span> </a></td>
								    	<td class="dash-row3" align="center">:</td>
								    	<td class="dash-row3" align="center"><a href="<%=productLessStockURL.toString() %>&lessStock=<%=EECConstants.LESS_STOCK%>"><%= lessStock %></a></td>
							    	</tr>							    					    	
						    </table>	
					   </div> 					
					</td>					
				</tr>				
			</table>
		</td>
		<td width="3%"></td>
		<td width="30%">
			<table class="dash-table1 heightrow4">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="general" /></th>
				</tr>
				<tr>
					<td align="center">
					<div class="row4-divheight">
					   <table  class="row4-tablewidth">					   
					    		<tr class="dash-titlebackgroundcolor">
					    			<td class="dash-titlecolor"><liferay-ui:message key="name" /></td>
					    			<td class="dash-row3" ></td>
					    			<td class="dash-titlecolor"><liferay-ui:message key="count" /></td>
					    		</tr>
					    		<tr class="row4-rowheight">
							    	<td align = "left"> <span class="dash-pay-stock"><liferay-ui:message key="visible-products" /></span></td>
							    	<td class="dash-row3" align="center">:</td>
							    	<td class="dash-row3" align="center"><%=visibilityProductCounts %></td>
						    	</tr>
						    	<tr class="row4-rowheight">
							    	<td align = "left"> <span class="dash-pay-stock"> <liferay-ui:message key="in-visible-products" /> </span></td>
							    	<td class="dash-row3" align="center">:</td>
							    	<td class="dash-row3" align="center"><%=inVisibilityProductCounts %></td>
						    	</tr>
						    	<tr class="row4-rowheight">
							    	<td align = "left"> <span class="dash-pay-stock"><liferay-ui:message key="coupon" /></span></td>
							    	<td class="dash-row3" align="center">:</td>
							    	<td class="dash-row3" align="center"><%=totalNoOfCoupons %></td>
						    	</tr>					    	
					    </table>	
					   </div> 			
					</td>					
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="100%" align="center">
	<tr>
		<td width="31%">
			 <table class="dash-table1 heightrow5">
			 	<tr>
					<th class="dash-boxheader"><liferay-ui:message key="last-30-days-sales" /></th>
				</tr> 
				<tr>
					<td align="center">
					<div id="row5-divheight">
					   <table  style="width: 100%;">
					    		<tr class="dash-titlebackgroundcolor">
					    			<td class="dash-titlecolor"><liferay-ui:message key="date" /></td>
					    			<td class="dash-row3" ></td>
					    			<td class="dash-titlecolor"><liferay-ui:message key="amount" /></td>
					    		</tr>
					    	<% 
							while (lsamout.hasNext()) {
								Map.Entry lsamoutEntry = (Map.Entry) lsamout.next(); %>
						    		<tr style="height: 25px;">
							    		<td class="dash-row3" align="center"><%= lsamoutEntry.getKey()%></td>
							    		<td class="dash-row3" align="center">:</td>
							    		<td class="dash-row3" align="center"><%=currencyFormat.format(Double.parseDouble(lsamoutEntry.getValue().toString()))%></td>
						    		</tr>
					    	<% } %>
					    </table>	
					   </div> 			
					</td>					
				</tr>								 				
			</table>
		</td>
		<td width="3%"></td>
		<td width="66%">
			<table class="dash-table1 heightrow5">
 				<tr>
					<th class="dash-boxheader"><liferay-ui:message key="order-item-status" /></th>
				</tr>
				<tr>
					<td>
						<div id="stockContainer" style="width:100%;height:260px;"></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<%
} 	
%>

	
<script type="text/javascript">
var dataSource = [
                  { status: "", Processing: <%=order_status_processing%> },
                  { status: "",Shipping: <%=order_status_onshipping%> },
                  { status: "", Delivered: <%=order_status_delivered%> },
                  { status: "", Cancel: <%=order_status_cancelled%> },
                  { status: "",Return: <%=order_status_return%> }
                ];

    $("#stockContainer").dxChart({
        dataSource: dataSource,
        equalBarWidth: {
            width: 05
        },
        commonSeriesSettings: {
            argumentField: "status",
            type: "bar",
            label: { visible: true }
        },
        series: [
            { valueField: "Processing", name: "Processing" },
            { valueField: "Shipping", name: "Shipping" },
            { valueField: "Delivered", name: "Delivered" },
            { valueField: "Cancel", name: "Cancel" },
            { valueField: "Return", name: "Return" }
        ],
        legend: {
            horizontalAlignment: "right"
        },
        valueAxis: {
    	    title: {
    	         text: "Order Item Count"
    	     },
    	     min: 0
        }
    });
</script>




	
	







