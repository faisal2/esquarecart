<%@ include file="/html/eec/common/ordertracker/init.jsp"%>

<%
	long orderId =0l;
	List<OrderItem> orderItems=new ArrayList<OrderItem>();
	ProductInventory pId = null;
  	String cmd = ParamUtil.getString(request,EECConstants.CMD);
  	
  	
	String curStatus = ParamUtil.getString(request,"curStatus");
	String orderItemId=ParamUtil.getString(request,"orderItemId");
	String redirectURL=ParamUtil.getString(request,EECConstants.REDIRECTURL);
	try{
	orderId=Long.valueOf(ParamUtil.getString(request,"orderId"));
	orderItems=OrderItemLocalServiceUtil.findByOrderItemStatus(orderId, curStatus);
	}catch(NumberFormatException numberFormatException)
	{
		OrderItem  orderItem= OrderItemLocalServiceUtil.getOrderItem(Long.valueOf(orderItemId));
		orderItems.add(orderItem);
	}
	
	Calendar date = Calendar.getInstance();
	Calendar expectedDeliveryDate = Calendar.getInstance();
	String value  = "";
	
	String inStockId = "";
	String outOfStockName = "";
	String confirmationMessage = LanguageUtil.get(locale,"confirmation.order");
%>
<div id="shippingform">
	<aui:form  method="post" name="fms">
	<aui:input name="<%=EECConstants.CMD %>" type="hidden" value="<%=cmd%>"/>
	<aui:input name="orderId" type="hidden" value="<%=orderId%>"/>
	<aui:input name="curStatus" type="hidden" value="<%=curStatus%>"/>
	<aui:input name="<%=EECConstants.REDIRECTURL%>" type="hidden" value="<%=redirectURL%>"/>
	<aui:input name="orderItemIds" type="hidden" />
	<aui:input name = "itemIds" type="hidden"/>
	<liferay-ui:search-container  delta="5"  emptyResultsMessage="No items to display." rowChecker="<%=new RowChecker(renderResponse) %>" >
	<liferay-ui:search-container-results total="<%=orderItems.size()%>"
		results="<%=ListUtil.subList(orderItems,searchContainer.getStart(),
						searchContainer.getEnd())%>"  />
	
	<liferay-ui:search-container-row modelVar="orderItem"
		className="com.esquare.ecommerce.model.OrderItem" keyProperty="orderItemId" >
		<%
		expectedDeliveryDate.setTime(orderItem.getExpectedDeliveryDate());
		int quantiy=orderItem.getQuantity();
		
		pId = ProductInventoryLocalServiceUtil.fetchProductInventory(Long.valueOf(orderItem.getProductInventoryId()));
		if (pId.getQuantity() < 1) {
			outOfStockName += orderItem.getName() +",";
		} else {
			inStockId += String.valueOf(orderItem.getOrderItemId()) +",";
		}
		//pValue = String.valueOf(pId.getQuantity());
		
		value = renderResponse.getNamespace()+"changeStatus();";
		//if(Long.valueOf(pValue) > 0){
		%>
		<input type="hidden" id="<%= orderItem.getProductInventoryId()%>" value="<%= orderItem.getName()%>" />
		<liferay-ui:search-container-column-text name="name" />
		<liferay-ui:search-container-column-text name="quantity" />
		<c:if test="<%=cmd.equalsIgnoreCase(EECConstants.ORDER_STATUS_ONSHIPPING)%>">
		<liferay-ui:search-container-column-text name="Expected Delivery Date" >
		<%-- <aui:input name="startDate" /> --%>
		<%-- <liferay-ui:input-date formName="dateV" yearRangeStart="2000"
			yearRangeEnd="2100" yearValue="<%=expectedDeliveryDate.get(Calendar.YEAR)%>"
			monthValue="<%=expectedDeliveryDate.get(Calendar.MONTH)%>"
			dayValue="<%=expectedDeliveryDate.get(Calendar.DATE)%>" dayParam='<%="d"+orderItem.getOrderItemId()%>' monthParam='<%="m"+orderItem.getOrderItemId()%>'
			yearParam='<%="y"+orderItem.getOrderItemId()%>' /> --%>
			<%-- <liferay-ui:input-date
				dayParam="d"
				dayValue="<%= date.get(Calendar.DATE) %>"
				disabled="<%= false %>"
				firstDayOfWeek="<%= date.getFirstDayOfWeek() - 1 %>"
				monthParam="m"
				monthValue="<%= date.get(Calendar.MONTH) %>"
				name="dateV"
				yearParam="y"
				yearValue="<%= date.get(Calendar.YEAR) %>"
				/> --%>
				<div class="row-fluid"><div class="span6"><aui:input name="shippingDate" label="" /></div><div class="span6"><aui:input name="shippingTime" label=""/></div></div>
				<%-- <div class="row-fluid"><div class="span6"><aui:input name="dateV" label="" /></div><div class="span6"><aui:input name="timeV" label=""/></div></div> --%>
		</liferay-ui:search-container-column-text>
		</c:if>
		<c:if test="<%=cmd.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)%>">
		<liferay-ui:search-container-column-text name="Quantity To be Cancelled">
		<aui:select  name='<%="quantity"+orderItem.getOrderItemId()%>' label="" >
		<%
		for(int i=1;i<=quantiy;i++){ 
		%>
		<aui:option label="<%=i%>"  value="<%=i%>" selected="<%=i==quantiy%>"/>
		<%
		}
		%>
		</aui:select>
		</liferay-ui:search-container-column-text>
		</c:if>
		<%//} %>
		</liferay-ui:search-container-row>
		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" paginate="false"/>
		</liferay-ui:search-container>
		<c:if test="<%=!cmd.equalsIgnoreCase(EECConstants.ORDER_STATUS_ONSHIPPING)%>">
		<aui:layout>
			<aui:column columnWidth="15" first="true">
			Date:
			</aui:column>
			<aui:column columnWidth="20" >
		<%-- <liferay-ui:input-date formName="dateV" yearRangeStart="2000"
			yearRangeEnd="2100" yearValue="<%=date.get(Calendar.YEAR)%>"
			monthValue="<%=date.get(Calendar.MONTH)%>"
			dayValue="<%=date.get(Calendar.DATE)%>" dayParam="d" monthParam="m"
			yearParam="y" /> --%>
			<liferay-ui:input-date
				dayParam="d"
				dayValue="<%= date.get(Calendar.DATE) %>"
				disabled="<%= false %>"
				firstDayOfWeek="<%= date.getFirstDayOfWeek() - 1 %>"
				monthParam="m"
				monthValue="<%= date.get(Calendar.MONTH) %>"
				name="dateV"
				yearParam="y"
				yearValue="<%= date.get(Calendar.YEAR) %>"
				/>
			
				                                                        
			
			
			</aui:column>
			<aui:column columnWidth="15" >
		<liferay-ui:input-time amPmParam="ampm" amPmValue="<%=date.get(Calendar.AM_PM) %>" hourParam="hour" hourValue="<%=date.get(Calendar.HOUR) %>"
			minuteParam="min" minuteValue="<%=date.get(Calendar.MINUTE) %>" /> 
		</aui:column>
		</aui:layout>
		</c:if>
		<c:if test="<%=cmd.equals(EECConstants.ORDER_STATUS_ONSHIPPING)%>">
		<aui:layout>
			<aui:column columnWidth="15" first="true">
			shippingTrackerId:
		</aui:column>
			<aui:column columnWidth="85" last="true">
				<aui:input name="shippingTrackerId" type="text"
			label="" />
		</aui:column>
		</aui:layout>
		</c:if>
		<aui:layout>
		<aui:column columnWidth="15" first="true">
		Location:
		</aui:column>
		<aui:column columnWidth="85" last="true">
		<aui:input name="location" label=""/>
		</aui:column>
		</aui:layout>
		<aui:layout>
		<aui:column columnWidth="15" first="true">
		Comment:
		</aui:column>
		<aui:column columnWidth="85" last="true">
		<aui:input name="comment" label="" type="textarea"/>
		</aui:column>
		</aui:layout>
		<div align="center">
		<aui:button value="submit" cssClass="ordersubmit" onClick='<%= value %>'  />
	     </div>
	</aui:form>
</div>
<aui:script >
AUI().use('aui-base','aui-io-request','aui-node', function(A){
	var cmd = "<%=cmd%>";
	var date = "";
	var newDate = new Date();
AUI().use('aui-datepicker','aui-timepicker', function(A) {
	new A.DatePicker({
	trigger : '#<portlet:namespace/>shippingDate',
	popover : {
	zIndex : 1
	},
	on: {
	    selectionChange: function(event) {
	   	  date = new Date(event.newSelection);
	    }
	}

	});
	
	new A.TimePicker(
		      {
		        trigger: '#<portlet:namespace/>shippingTime',
		        popover: {
		          zIndex: 1
		        }
		       
		      }
		    );
	
	});
Liferay.provide(
		window,
		'<portlet:namespace />changeStatus',
		function() {
			
			var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />fms, "<portlet:namespace />allRowIds");
			if(checkBoxValue==""||checkBoxValue==null){
					alert("Please select atleast One entry");
					return false;
			}
			
			var outOfStack = '<%=outOfStockName%>';
			var inStock = '<%=inStockId%>';
			document.getElementById("<portlet:namespace />orderItemIds").value = inStock;
			 document.getElementById("<portlet:namespace />itemIds").value = checkBoxValue; 
			if(outOfStack!="") {
				if (confirm(outOfStack.substring(0, outOfStack.length-1)+"<%=confirmationMessage%>")) {
					if(cmd == "ONSHIPPING" ){
						if(date >= newDate ){
					    submitForm(document.<portlet:namespace />fms, '<portlet:actionURL  windowState="<%=LiferayWindowState.NORMAL.toString()%>"/>');
					Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');  
					window.top.location.reload();   
					}
						else{
							alert("Expected Delivery Date should be grater than or Equal to Current Date ")
							return false;
						}
					}
					else{
						  submitForm(document.<portlet:namespace />fms, '<portlet:actionURL  windowState="<%=LiferayWindowState.NORMAL.toString()%>"/>');
							Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');  
							window.top.location.reload(); 
					}
					} else {
					return false;
				}
			}else {
				if(cmd == "ONSHIPPING" ){
				if(date >= newDate ){
					  submitForm(document.<portlet:namespace />fms, '<portlet:actionURL  windowState="<%=LiferayWindowState.NORMAL.toString()%>"/>');
					Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');  
					window.top.location.reload();  
					}
				
					else{
						alert("Expected Delivery Date should be grater than or Equal to Current Date ")
						return false;
					}
				}
				else{
					 submitForm(document.<portlet:namespace />fms, '<portlet:actionURL  windowState="<%=LiferayWindowState.NORMAL.toString()%>"/>');
					Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');  
					window.top.location.reload(); 
				}
				}
			
		},
		['liferay-util-list-fields']
	);
});
</aui:script>

