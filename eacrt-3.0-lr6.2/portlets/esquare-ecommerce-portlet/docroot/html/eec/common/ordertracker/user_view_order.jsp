<%@ include file="/html/eec/common/ordertracker/init.jsp"%>
<liferay-ui:error key="NoRecord"
	message="No Records Found For You Search" />
<%		
		
		PortletURL viewURL = renderResponse.createRenderURL();
		viewURL.setParameter("jspPage", "/html/eec/common/ordertracker/view_order.jsp");
		viewURL.setWindowState(LiferayWindowState.MAXIMIZED);
		List<Order>	orderList=OrderLocalServiceUtil.findByUserId(company.getCompanyId(), themeDisplay.getUserId(),EECConstants.STATUS_CHECKOUT);
		
PortletURL cartViewURL = PortletURLFactoryUtil.create(request,"", 0, PortletRequest.RENDER_PHASE);
		
		String header=UserViewUtil.getHeader(themeDisplay.getScopeGroupId(),PortletPropsValues.ORDER_TRACKER_TITLE_NAME, 0,cartViewURL,ctxPath, EECConstants.STYLE_DISPLAY_NONE);
%>
<br>
<div id="tracking">
	<%=header%>

	
	

	<div>
	<div class="ordertextsearch">
		<aui:form name="fm" method="post" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "orderSearch();" %>'>
			<aui:input name="<%=EECConstants.CMD%>" type="hidden" />
				<div id="orders">
					<div style="margin-top: 10px;">Order Number:</div>
					<div id="<portlet:namespace />orderNumberUserDivId" style="float: left;" >
						<div style="float: left;" >
							<aui:input type="text" name="orderNumber" label="" />
						</div>
					</div>	
			<div id="dbordertracker-searchbut" style="float: left;">
	<button  name="Search" id="search"><i class="icon-search" style="margin-right: 10px"></i>Search</button> 		
			<!-- <input type="submit"  name="Search" value="Search" id="search"/> -->
			</div>
	</aui:form>
</div>
		
	<div class="ordersearchcontainer">	
		<liferay-ui:search-container emptyResultsMessage="No orders to display." >
			
			<liferay-ui:search-container-results results="<%=  ListUtil.subList(orderList,searchContainer.getStart(),
				searchContainer.getEnd())%>"
			total="<%=orderList.size()%>" />
			<liferay-ui:search-container-row modelVar="order"
			className="com.esquare.ecommerce.model.Order">
			<%
						viewURL.setParameter("orderId",
								String.valueOf(order.getOrderId()));
			%>
			<liferay-ui:search-container-column-text property="number" name="Order Number"
				href="<%=viewURL.toString()%>"  />
				<liferay-ui:search-container-column-jsp name="Order Items"
					path="/html/eec/common/ordertracker/user_view_item.jsp" />
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" paginate="false" />
		</liferay-ui:search-container>
	</div>
</div>

<aui:script use="autocomplete-list,aui-base,autocomplete-filters,autocomplete-highlighters">
 
  var states = JSON.parse('<%=EECUtil.getOrderNumber(themeDisplay.getCompanyId(), themeDisplay.getUserId()) %>');
   new A.AutoCompleteList(
       {
        allowBrowserAutocomplete: 'false',
        activateFirstItem: 'true',
        inputNode: '#<portlet:namespace/>orderNumber',
        focused :'true',
        resultTextLocator: 'name',
        resultHighlighter:['phraseMatch'],
        resultFilters: ['charMatch','phraseMatch'], 
        minQueryLength: 1, 
        maxResults: 10, 
        render: 'true',
        source:states
   });  
</aui:script>
<script>
function <portlet:namespace />orderSearch(){
    var orderNumber=document.getElementById("<portlet:namespace />orderNumber").value;
    if(orderNumber=="" && orderNumber==0){
    document.getElementById("<portlet:namespace />orderNumber").focus();
   return false;
 }
    else{
    	 document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=EECConstants.ADMIN_SEARCH%>";
    		submitForm(document.<portlet:namespace />fm, '<portlet:actionURL/>');
    	}
}
</script>

<style>
.dashboard-fields-lable, .field-label, .field-label-inline-label, .choice-label{
color: gray;}



</style>
