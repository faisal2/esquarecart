<%@ include file="/html/eec/common/ordertracker/init.jsp"%>

<%		
		ResultRow row4 = (ResultRow) request
		.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
		Order order1=(Order)row4.getObject();
		
		PortletURL viewURL = renderResponse.createRenderURL();
		viewURL.setParameter("jspPage", "/html/eec/common/ordertracker/view_order.jsp");
		viewURL.setParameter("orderId",String.valueOf(order1.getOrderId()));
		List<OrderItem>	orderItemList=OrderItemLocalServiceUtil.getOrderItems(order1.getOrderId());
%>
			  <liferay-ui:search-container 
	emptyResultsMessage="No orders to display."  >

	<liferay-ui:search-container-results results="<%=  ListUtil.subList(orderItemList,
			searchContainer.getStart(),
			searchContainer.getEnd())%>"
		total="<%=orderItemList.size()%>" 
	/>
	<liferay-ui:search-container-row modelVar="orderItem"
		className="com.esquare.ecommerce.model.OrderItem">
		<%
				CancelledOrderItems cancelledOrderItem = null;
							try {
								cancelledOrderItem = CancelledOrderItemsLocalServiceUtil
										.getCancelledOrderItems(orderItem
												.getOrderItemId());
							} catch (NoSuchCancelledOrderItemsException e) {
							}

							StringBundler quantity = new StringBundler();
							quantity.append("<ul>");
							if (orderItem.getQuantity() > 0) {
								quantity.append("<li>");
								quantity.append(orderItem.getQuantity());
								quantity.append("</li>");
							}
							if (Validator.isNotNull(cancelledOrderItem)) {
								quantity.append("<li>");
								quantity.append(cancelledOrderItem.getQuantity());
								quantity.append("</li>");

							}
							quantity.append("</ul>");

							StringBundler orderItemStatus = new StringBundler();
							orderItemStatus.append("<ul>");
							if (orderItem.getQuantity() > 0) {
								orderItemStatus.append("<li>");
								orderItemStatus.append(orderItem
										.getOrderItemStatus());
								orderItemStatus.append("</li>");
							}

							if (Validator.isNotNull(cancelledOrderItem)) {
								orderItemStatus.append("<li>");
								orderItemStatus
										.append(EECConstants.ORDER_STATUS_CANCELLED);
								orderItemStatus.append("</li>");

							}
							orderItemStatus.append("</ul>");

							StringBundler orderItemTotal = new StringBundler();
							orderItemTotal.append("<ul>");
							if (orderItem.getQuantity() > 0) {
								orderItemTotal.append("<li>");
								orderItemTotal.append(currencyFormat
										.format((orderItem.getQuantity()
												* orderItem.getPrice())+orderItem.getTax()+orderItem.getShippingPrice()));
								orderItemTotal.append("</li>");
							}

							if (Validator.isNotNull(cancelledOrderItem)) {
								orderItemTotal.append("<li>");
								orderItemTotal.append(currencyFormat
										.format(cancelledOrderItem.getQuantity()
												* orderItem.getPrice()));
								orderItemTotal.append("</li>");

							}
							orderItemTotal.append("</ul>");
			%>
			<liferay-ui:search-container-column-text name="name" href="<%=viewURL.toString()%>" />
			<liferay-ui:search-container-column-text name="quantity"
				value="<%=quantity.toString()%>" />
			<liferay-ui:search-container-column-text name="order-item-status"
				value="<%=orderItemStatus.toString()%>" />
			<liferay-ui:search-container-column-text name="total"
				value="<%=orderItemTotal.toString()%>" />
		<liferay-ui:search-container-column-jsp name="Action"
					path="/html/eec/common/ordertracker/action.jsp" />
	</liferay-ui:search-container-row>
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" paginate="false"/>
</liferay-ui:search-container>