<%@ include file="/html/eec/common/ordertracker/init.jsp"%>
<script language="javascript" type="text/javascript">
        function printDiv(divToPrint) {
            var divElements = document.getElementById(divToPrint).innerHTML;
            var oldPage = document.body.innerHTML;
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";
            window.print();
            document.body.innerHTML = oldPage;
        }
    </script>
<a href="#"> <img src="<%=ctxPath%>/images/eec/common/ordertracker/print.png"
	id="btnPrint" onclick="javascript:printDiv('divToPrint')" title="Print this Page"></a>


<%
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	String orderId = ParamUtil.getString(request, "orderId");
	String tabs1 = ParamUtil.getString(request, "tabs1");
	Order order = OrderLocalServiceUtil.getOrder(Long
			.parseLong(orderId));
	order = order.toEscapedModel();
	String countryName = "";
	String regionName = "";
	try {
		Country countryobj = CountryServiceUtil.fetchCountryByA2(order
				.getBillingCountry());
		countryName = countryobj.getName();
		Region regionobj = RegionServiceUtil.getRegion(Long
				.parseLong(order.getBillingState()));
		regionName = regionobj.getName();
	} catch (Exception e) {
		e.printStackTrace();
	}
	String paymentType = order.getPaymentType();
	String paymentStatus = order.getPaymentStatus();
	List<OrderItem> orderItems = OrderItemLocalServiceUtil
			.getOrderItems(order.getOrderId());
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("orderId", orderId);
	portletURL.setParameter("jspPage",
			"/html/eec/common/ordertracker/view_order.jsp");
%>

<portlet:renderURL var="cancelURL" windowState="<%=LiferayWindowState.NORMAL.toString()%>">
	<portlet:param name="jspPage"
		value="/html/eec/common/ordertracker/orders.jsp" />
	<portlet:param name="tabs1" value="<%=tabs1%>" />
</portlet:renderURL>

<portlet:actionURL var="editOrderURL" />
<a href="<%=cancelURL%>"
	class="backorder"><i class="icon-circle-arrow-left" style="font-size: 18px; margin-right: 5px;"></i>Go Back</a>
<aui:form action="<%=editOrderURL%>" method="post" name="fm">
	<aui:input name="<%=EECConstants.CMD%>" type="hidden" />
	<aui:input name="<%=EECConstants.REDIRECTURL%>" type="hidden"
		value="<%=currentURL%>" />
	<aui:input name="orderId" type="hidden" value="<%=orderId%>" />
	<aui:input name="number" type="hidden" value="<%=order.getNumber()%>" />
	<aui:input name="emailType" type="hidden" />

	<div id="divToPrint">
		<div class="row-fluid">
			<div class="span3" style="margin-left: 12%">
				<div class="order_back">
				<div class="orderheading">Order Details</div>
					<br>
					    <span class="minalign">Order No:</span>
						<span class="answer"><%=order.getNumber()%></span>
					<br>
						<span class="minalign">Order Date:</span>
						<span class="answer"><%=sdf.format(order.getCreateDate())%></span>
					<br>
						<span class="minalign">Payment Type:</span>
						<span class="answer"><%=paymentType%></span>
					<br>
						<span class="minalign">Payment Status:</span>
						<span class="answer"><%=paymentStatus%></span>
			</div>
			</div>
			
			<div class="span3">
			  <div class="order_back">
				<div class="orderheading">Order Payment Details </div>
			<br>	
					<span class="minalign"><liferay-ui:message key="subtotal" />:</span>
					<span class="answer"><%=currencyFormat.format(EECUtil.calculateActualSubtotal(orderItems))%></span>
			<br>
					<span class="minalign"><liferay-ui:message key="tax" />:</span>
					<span class="answer"><%=currencyFormat.format(order.getTax())%></span>
			<br>
				<span class="minalign"><liferay-ui:message key="shipping" /> <%=Validator.isNotNull(order.getAltShipping()) ? "("
						+ order.getAltShipping() + ")" : StringPool.BLANK%></span>
				<span><%=currencyFormat.format(order.getShipping())%></span>
			<br>
				<span class="minalign"><liferay-ui:message key="coupon-discount" />:</span>
				<span class="answer"><%=currencyFormat.format(order.getCouponDiscount())%></span>
			<br>
				<span class="minalign"><liferay-ui:message key="wallet-amount" />:</span>
				<span class="answer"><%=currencyFormat.format(order.getWalletAmount())%></span>
			<br>
				<span class="minalign"> <liferay-ui:message key="total" />:</span>
				<span class="answer"><%=currencyFormat.format(EECUtil.calculateTotal(order)
						- order.getWalletAmount())%></span>
		</div>
		</div>
			
	<div class="span3">
	<div class="order_back">
			<div class="orderheading">Shipping Address</div>
				<br>
					<span class="minalign">Name:</span>
					<span class="answer"><%=order.getShippingFirstName()%></span>
				<br>	
					<span class="minalign">Address:</span>
					<span class="answer"><%=order.getShippingStreet()%></span>
				
				<br>
					<span class="minalign">Phone:</span>
					<span class="answer"><%=order.getShippingPhone()%></span>
					<br>				
					<span class="minalign"><%=order.getShippingCity()%>,<%=regionName%>,<%=order.getShippingZip()%></span>
		</div>
		</div>
</div>
</div>
		<!-- Main Div Ends -->

		<liferay-ui:search-container delta="5"
			emptyResultsMessage="No orders to display."
			iteratorURL="<%=portletURL%>">

			<liferay-ui:search-container-results
				results="<%=ListUtil.subList(orderItems,
							searchContainer.getStart(),
							searchContainer.getEnd())%>"
				total="<%=orderItems.size()%>" />
			<liferay-ui:search-container-row modelVar="orderItem"
				className="com.esquare.ecommerce.model.OrderItem"
				keyProperty="orderItemId">
				<liferay-ui:search-container-column-text name="sku" />
				<liferay-ui:search-container-column-text name="name" />
				<%
					CancelledOrderItems cancelledOrderItem = null;
								try {
									cancelledOrderItem = CancelledOrderItemsLocalServiceUtil
											.getCancelledOrderItems(orderItem
													.getOrderItemId());
								} catch (NoSuchCancelledOrderItemsException e) {
								}

								StringBundler quantity = new StringBundler();
								quantity.append("<ul>");
								if (orderItem.getQuantity() > 0) {
									quantity.append("<li>");
									quantity.append(orderItem.getQuantity());
									quantity.append("</li>");
								}
								if (Validator.isNotNull(cancelledOrderItem)) {
									quantity.append("<li>");
									quantity.append(cancelledOrderItem.getQuantity());
									quantity.append("</li>");

								}
								quantity.append("</ul>");

								StringBundler orderItemStatus = new StringBundler();
								orderItemStatus.append("<ul>");
								if (orderItem.getQuantity() > 0) {
									orderItemStatus.append("<li>");
									orderItemStatus.append(orderItem
											.getOrderItemStatus());
									orderItemStatus.append("</li>");
								}

								if (Validator.isNotNull(cancelledOrderItem)) {
									orderItemStatus.append("<li>");
									orderItemStatus
											.append(EECConstants.ORDER_STATUS_CANCELLED);
									orderItemStatus.append("</li>");

								}
								orderItemStatus.append("</ul>");

								StringBundler orderItemTotal = new StringBundler();
								orderItemTotal.append("<ul>");
								if (orderItem.getQuantity() > 0) {
									orderItemTotal.append("<li>");
									orderItemTotal.append(currencyFormat
											.format(orderItem.getQuantity()
													* orderItem.getPrice()));
									orderItemTotal.append("</li>");
								}

								if (Validator.isNotNull(cancelledOrderItem)) {
									orderItemTotal.append("<li>");
									orderItemTotal.append(currencyFormat
											.format(cancelledOrderItem.getQuantity()
													* orderItem.getPrice()));
									orderItemTotal.append("</li>");

								}
								orderItemTotal.append("</ul>");
				%>
				<liferay-ui:search-container-column-text name="quantity"
					value="<%=quantity.toString()%>" />
				<liferay-ui:search-container-column-text name="order-item-status"
					value="<%=orderItemStatus.toString()%>" />
				<liferay-ui:search-container-column-text name="subtotal"
					value="<%=orderItemTotal.toString()%>" />
				<liferay-ui:search-container-column-text name="shipping-tracker-id">
					<c:choose>
						<c:when
							test="<%=Validator.isNotNull(orderItem
											.getShippingTrackerId())%>">
							<%=orderItem.getShippingTrackerId()%>
						</c:when>
						<c:otherwise>N/A</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-text
					name="expected-delivery-date">
					<c:choose>
						<c:when
							test="<%=!orderItem
											.getOrderItemStatus()
											.equalsIgnoreCase(
													EECConstants.ORDER_STATUS_DELIVERED)%>">
							<fmt:formatDate
								value="<%=orderItem
											.getExpectedDeliveryDate()%>"
								pattern="dd-MM-yyyy" />
						</c:when>
						<c:otherwise>N/A</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-text name="delivered-date">
					<c:choose>
						<c:when
							test="<%=Validator.isNotNull(orderItem
											.getDeliveredDate())%>">
							<fmt:formatDate value="<%=orderItem.getDeliveredDate()%>"
								pattern="dd-MM-yyyy" />
						</c:when>
						<c:otherwise>N/A</c:otherwise>
					</c:choose>
				</liferay-ui:search-container-column-text>
				<liferay-ui:search-container-column-jsp name="Action"
					path="/html/eec/common/ordertracker/action.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>"
				paginate="false" />
		</liferay-ui:search-container>
		<br />
		
		
		
		<%
			String historyXML = order.toEscapedModel().getHistory();
				if (Validator.isNotNull(historyXML))
					out.println(EECUtil.generateHistory(historyXML));
		%>

	</div>
	<%
		String taglibSendEmailShipping = renderResponse.getNamespace()
					+ "sendEmail('shipping');";
	%>
	<div align="center">
		<a href="#" onclick="<%=taglibSendEmailShipping%>"
			id="ddd">Resend</a> <a
			href="<%=cancelURL.toString()%>" id="ddd">Go
			Back</a>
	</div>
</aui:form>


<aui:script>
	function <portlet:namespace />sendEmail(emailType) {
	
		document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=EECConstants.RESENDMAIL%>";
		document.<portlet:namespace />fm.<portlet:namespace />emailType.value = "<%=EECConstants.CONFIRMATION_MAIL%>";
		submitForm(document.<portlet:namespace />fm);
		}
</aui:script>
<style>

</style>