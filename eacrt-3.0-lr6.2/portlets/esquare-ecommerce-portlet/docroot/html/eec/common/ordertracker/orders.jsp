<%@ include file="/html/eec/common/ordertracker/init.jsp" %>

<%-- <c:if test='<%= SessionMessages.contains(renderRequest, "couponApplicableItems") %>'>

					<%
					String couponApplicableItems = (String)SessionMessages.get(renderRequest, "couponApplicableItems");
					String minimumOrder = (String)SessionMessages.get(renderRequest, "minimumOrder");
					%>

					<div class="portlet-msg-error">
						<%= LanguageUtil.format(pageContext, "unable-to-delete-items.Please-select-more-quantity-from-x-or-cancel-the-order-item-amount-greater-than-x",new String[]{couponApplicableItems,minimumOrder}) %>
					</div>
</c:if> --%>
<c:choose>
  <c:when test="<%=isShopAdmin%>">
<liferay-util:include page="/html/eec/common/ordertracker/admin_view.jsp"  servletContext="<%=this.getServletContext() %>" />
  </c:when>
  
  <c:otherwise>
 <liferay-util:include page="/html/eec/common/ordertracker/user_view_order.jsp"  servletContext="<%=this.getServletContext() %>" />
  </c:otherwise>
</c:choose>