<%@ include file="/html/eec/common/ordertracker/init.jsp"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<portlet:renderURL var="addCountryURL" windowState="<%=LiferayWindowState.POP_UP.toString()%>">
	<portlet:param name="curStatus" value="orderItemStatus"/>
	<portlet:param name="jspPage" value="/html/eec/common/ordertracker/admin_form.jsp"/>
	</portlet:renderURL>
<%
	ResultRow row3 = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	Order order =null;
	OrderItem orderItem=null;
	String orderId =null;
	String orderItemStatus=null;
	try{
		order=(Order) row3.getObject();
		orderId = String.valueOf(order.getPrimaryKey());
		orderItemStatus = (String) request.getAttribute("curStatus");
	}catch(ClassCastException classCastException){
		orderItem=(OrderItem)row3.getObject();
		orderItemStatus=orderItem.getOrderItemStatus();
	}

	
	PortletURL renderURL = renderResponse.createRenderURL();
	renderURL.setWindowState(LiferayWindowState.POP_UP);
	if(Validator.isNotNull(orderId)){
		renderURL.setParameter("orderId", orderId);
		}
	else{
		orderItem = OrderItemLocalServiceUtil.getOrderItem(orderItem.getOrderItemId());
		renderURL.setParameter("orderId", String.valueOf(orderItem.getOrderId()));
		renderURL.setParameter("orderItemId",String.valueOf(orderItem.getOrderId()));
	}
	renderURL.setParameter("curStatus",orderItemStatus);
	renderURL.setParameter("jspPage",
			"/html/eec/common/ordertracker/admin_form.jsp");

	boolean processing = orderItemStatus
			.equalsIgnoreCase(EECConstants.ORDER_STATUS_PROCESSING);
	boolean onShipping = orderItemStatus
			.equalsIgnoreCase(EECConstants.ORDER_STATUS_ONSHIPPING);
	boolean orderReturn = orderItemStatus
			.equalsIgnoreCase(EECConstants.ORDER_STATUS_RETURN);
	boolean orderDelivered = orderItemStatus
			.equalsIgnoreCase(EECConstants.ORDER_STATUS_DELIVERED);
	
	String cancel = "javascript:popup('" +renderURL.toString()+"&"+renderResponse.getNamespace()+"cmd="+EECConstants.ORDER_STATUS_CANCELLED+"','Cancel Form');";
	String delivery = "javascript:popup('" +renderURL.toString()+"&"+renderResponse.getNamespace()+"cmd="+EECConstants.ORDER_STATUS_DELIVERED+"','Delivery Form');";
	String shipping = "javascript:popup('" + renderURL.toString()+"&"+renderResponse.getNamespace()+"cmd="+EECConstants.ORDER_STATUS_ONSHIPPING+"','Shipping Form');";
	String returnOrder = "javascript:popup('" + renderURL.toString()+"&"+renderResponse.getNamespace()+"cmd="+EECConstants.ORDER_STATUS_RETURN+"','Return Form');";

%>
	

<liferay-ui:icon-menu>
<c:if test="<%=(processing||orderReturn)&&isShopAdmin%>">
	<liferay-ui:icon image="edit" message='<%=processing?"Move To Shipping":"Process Order Again"%>'
		url="<%=shipping%>" />
</c:if>
<c:if test="<%=onShipping&&isShopAdmin%>">
	<liferay-ui:icon image="edit" message="Delivered" url='<%=delivery%>' />
	<liferay-ui:icon image="delete" message="Return Order"
		url='<%=returnOrder%>' />
</c:if>
<c:if test="<%=processing||(orderReturn&& isShopAdmin)%>">
	<liferay-ui:icon image="delete" message="cancel Order"
		url='<%=cancel%>' />
</c:if>
<c:if test="<%=orderDelivered%>" >
<liferay-ui:icon image="delete" message="Return Order"
		url='<%=returnOrder%>' />
</c:if>
</liferay-ui:icon-menu>		

<aui:script use="aui-base,aui-io-plugin-deprecated,liferay-util-window">



  	Liferay.provide(window,'popup',
			function(url,titles1) {
  			Liferay.Util.openWindow({
        	    dialog: {
        	    centered: true,
        	    height: 500,
        	    modal: true,
        	    width: 800
        	    },
        	    id: '<portlet:namespace/>dialog',
        	    title: titles1,
        	    uri: url
        	    });
  		
  		 Liferay.provide(
  				window,
  				'closePopup',
  				function(dialogId) {
  					
  					var dialog = Liferay.Util.getWindow(dialogId);

  					Liferay.fire(
  						'closeWindow',
  						{
  							id:'<portlet:namespace/>dialog'
  						}
  					);
  				},
  				['aui-base','liferay-util-window']
  			);	
  		
  	});
  	
</aui:script>
