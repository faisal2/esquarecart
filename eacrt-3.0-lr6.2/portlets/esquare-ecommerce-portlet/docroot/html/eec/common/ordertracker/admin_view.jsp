<%@ include file="/html/eec/common/ordertracker/init.jsp"%>
<liferay-ui:error key="NoRecord"
	message="No Records Found For You Search" />
<%		
		String tabs1 = ParamUtil.getString(request, "tabs1",EECConstants.ORDER_STATUS_PROCESSING);
		PortletSession ps = renderRequest.getPortletSession();
		String orderByCol = ParamUtil.getString(request, "orderByCol");
		String orderByType = ParamUtil.getString(request, "orderByType", "asc");
		request.setAttribute("curStatus",tabs1);
		
		PortletURL portletURL = renderResponse.createRenderURL();
		portletURL.setParameter("tabs1", tabs1);
		
		PortletURL viewURL = renderResponse.createRenderURL();
		viewURL.setParameter("jspPage", "/html/eec/common/ordertracker/view_order.jsp");
		viewURL.setParameter("tabs1",tabs1);
		viewURL.setWindowState(LiferayWindowState.MAXIMIZED);
	    
		List<Order> orderList=OrderLocalServiceUtil.getOrderItemStatusList(company.getCompanyId(), tabs1);
	
		String tabNames =EECConstants.ORDER_STATUS_PROCESSING
		+ "," + EECConstants.ORDER_STATUS_ONSHIPPING + ","
		+ EECConstants.ORDER_STATUS_RETURN + ","
		+ EECConstants.ORDER_STATUS_DELIVERED + ","
		+ EECConstants.ORDER_STATUS_CANCELLED;
		
		PortletURL cartViewURL = PortletURLFactoryUtil.create(request,"", 0, PortletRequest.RENDER_PHASE);
		
		String header=UserViewUtil.getHeader(themeDisplay.getScopeGroupId(),PortletPropsValues.ORDER_TRACKER_TITLE_NAME, 0,cartViewURL,ctxPath, EECConstants.STYLE_DISPLAY_NONE);
%>
<div class="dashboard-maindiv2" id="ordertrackingheader">
<%=header%>
	<aui:form name="fm" method="post" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "orderSearch();" %>'>
		<aui:input name="<%=EECConstants.CMD%>" type="hidden" />
		<div>
		<div id="<portlet:namespace />orderNumberUserDivId">
			<table class="ordertextsearchadmin">
			<tr>
			<td>
			<aui:input type="text" name="orderNumber" label="" placeholder="Search Order Number"/>
			</td>
			<td class="ordersearch">
			<input type="submit" id="search" name="Search" value="Search"  />
			</td>
			</tr>
			</table>
		</div>
		</div>
	
			
	
				
	</aui:form>
	<div>
	<liferay-ui:tabs names="<%=tabNames%>" value="<%=tabs1%>"
		url="<%=portletURL.toString()%>" />
	<c:choose>
		<c:when
			test="<%=!tabs1.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED)%>">

			<liferay-ui:search-container delta="5"
				emptyResultsMessage="No orders to display."
				iteratorURL="<%=portletURL%>" orderByCol="<%=orderByCol%>"
				orderByType="<%=orderByType%>">

				<liferay-ui:search-container-results>
				<%
					if(Validator.isNotNull(orderByCol)) {
		orderByCol = orderByCol.substring(0,1).toLowerCase().concat(orderByCol.substring(1, orderByCol.length()));
		BeanComparator comp = new BeanComparator(orderByCol);
		String ordertype = (String) ps.getAttribute("ORDER_BY_TYPE", PortletSession.APPLICATION_SCOPE);
		if(Validator.isNotNull(ordertype)) orderByType = ordertype;
		if(Validator.isNotNull(orderList)){
		Collections.sort(orderList, comp);
		if (orderByType.equals("asc") || Validator.isNull(orderByType)) {
			ps.setAttribute("ORDER_BY_TYPE", "desc", PortletSession.APPLICATION_SCOPE);
		} 
		if(orderByType.equals("desc")) {
			Collections.reverse(orderList);
			ps.setAttribute("ORDER_BY_TYPE", "asc", PortletSession.APPLICATION_SCOPE);
		}
	}

	}
				results =ListUtil.subList(orderList, searchContainer.getStart(),searchContainer.getEnd());
				total = orderList.size();
			    pageContext.setAttribute("results", results);
				pageContext.setAttribute("total", total);
 %>
					</liferay-ui:search-container-results>
					
					
					</div>
				<liferay-ui:search-container-row modelVar="order"
					className="com.esquare.ecommerce.model.Order">
					<%
						String customerName = order.getBillingFirstName()
														+ order.getBillingLastName();
												String customerPhoneNumber =order.getBillingPhone();
												
												viewURL.setParameter("orderId",
														String.valueOf(order.getOrderId()));
					%>
					<liferay-ui:search-container-column-text name="number"
						href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="number"/>
					<liferay-ui:search-container-column-text name="Purchased Date"
						href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="createDate">
						<fmt:formatDate value="<%=order.getCreateDate()%>"
							pattern="dd-MM-yyyy" />
					</liferay-ui:search-container-column-text>
					<liferay-ui:search-container-column-text name="Name"
						value="<%=customerName%>" href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="createDate"/>
					<liferay-ui:search-container-column-text name="Phone Number"
						value="<%=customerPhoneNumber%>" href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="createDate" />
					<liferay-ui:search-container-column-text name="Payment Type"
						property="paymentType" href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="createDate" />
					<liferay-ui:search-container-column-text name="Payment Status"
						property="paymentStatus" href="<%=viewURL.toString()%>" orderable="<%= true %>" orderableProperty="createDate" />
					<c:if
						test="<%=!(tabs1.equalsIgnoreCase(EECConstants.ORDER_STATUS_CANCELLED))%>">
						<liferay-ui:search-container-column-jsp name="Action"
							path="/html/eec/common/ordertracker/action.jsp" />
					</c:if>
				</liferay-ui:search-container-row>

				<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
			</liferay-ui:search-container>
		</c:when>
		<c:otherwise>
			<%@ include file="/html/eec/common/ordertracker/admin_view_cancelled_order.jspf" %>
		</c:otherwise>
	</c:choose>
</div>

<aui:script use="autocomplete-list,aui-base,autocomplete-filters,autocomplete-highlighters">
 
  var states = JSON.parse('<%=EECUtil.getOrderNumber(themeDisplay.getCompanyId(), 0) %>');
   new A.AutoCompleteList(
       {
        allowBrowserAutocomplete: 'false',
        activateFirstItem: 'true',
        inputNode: '#<portlet:namespace/>orderNumber',
        focused :'true',
        resultTextLocator: 'name',
        resultHighlighter:['phraseMatch'],
        resultFilters: ['charMatch','phraseMatch'], 
        minQueryLength: 1, 
        maxResults: 10, 
        render: 'true',
        source:states
   });  
</aui:script>
<script>
function <portlet:namespace />orderSearch(){
    var orderNumber=document.getElementById("<portlet:namespace />orderNumber").value;
    if(orderNumber=="" && orderNumber==0){
    document.getElementById("<portlet:namespace />orderNumber").focus();
   return false;
 }
    else{
    	 document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=EECConstants.ADMIN_SEARCH%>";
    		submitForm(document.<portlet:namespace />fm, '<portlet:actionURL/>');
    	}
}
</script>
<style>

.tab-active .tab-label {
    color: #FFFFFF;
}
#search{
	background-color: #006cb4;
    border: 0 none;
    color: white;
    font-family: myfont !important;
    font-size: medium !important;
    min-width: 110px;
    height: 38px;
     margin-top: -7px;
}
.nav.nav-tabs {
    padding-left: 106px;
}
.tab {
    height: 38px;
    
}


</style>
