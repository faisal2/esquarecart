<%@ include file="/html/eec/common/init.jsp"%>
<portlet:actionURL var="editinstanceURL" name="editInstance" />

<portlet:resourceURL var="resouceURL" />

<%-- <portlet:resourceURL var="captchaURL">
            <portlet:param name="<%=EECConstants.CMD %>" value="<%=EECConstants.CAPTCHA %>" />
</portlet:resourceURL> --%>
<div class="row-fluid">
	<div class="span6" id="instancecrt">
    <aui:form action="<%=editinstanceURL%>" name="fm" method="post" id="fm">
        <div id="instance_panal">&nbsp;</div>
         <aui:fieldset> 
            <div id="instancecrt_content">
			
                    <aui:input type="hidden" name="packageType" value="<%= PortletPropsValues.PACKAGE_TYPE_FREE%>"/>
			</div>
	</div>
					
					
<div class="row-fluid">
    <div class="span12">
        <img src="<%=ctxPath%>/images/eec/common/instance/storename.png" id="storename">
        <aui:input type="text" class="icon1" name="<%=PortletPropsValues.INSTANCE_NAME%>" inlineField="true"
        placeholder="Store Name" label=""  >
        <aui:validator name="required"></aui:validator>
        <aui:validator name="rangeLength">
        [4,12]
        </aui:validator>
        </aui:input>
                                   
		<div id="insNA" class="hide">
            <span class="form-validator-message">Domain is already Exist</span>
		</div>
    </div>
                                
</div>			
					
<div class="row-fluid">
	<div class="span12">
        <img src="<%=ctxPath%>/images/eec/common/instance/storemail.png" id="storemail"/>
        <aui:input type="text" name="email"
        inlineField="true" placeholder="E-mail" label="" >
        <aui:validator name="required"></aui:validator>
        <aui:validator name="email" />
        </aui:input>
	</div>
</div>					
					
<div class="row-fluid">
	<div class="span12">
        <img src="<%=ctxPath%>/images/eec/common/instance/storepass.png" id="storepass"/>
        <aui:input type="password" name="password" inlineField="true" placeholder="Password" label="" >
        <aui:validator name="required"></aui:validator>
        <aui:validator name="rangeLength" errorMessage="Please enter password between 4 and 12 characters.">
		[4,12]
        </aui:validator>
        </aui:input>
	</div>
</div>	
			<%-- <aui:column>
			<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />
			<liferay-ui:captcha url="<%= captchaURL %>" />
			</aui:column> --%>
                            					
<div class="row-fluid">
	<div class="span6">
        <aui:button type="submit" name="save" value="Create a Store" />
</div>
    </div>
					
					
					
					
		 </aui:fieldset>			
</div>
</aui:form> 
<%-- <img src="<%=ctxPath%>/images/eec/common/instance/storename.png" id="storename"/>
<img src="<%=ctxPath%>/images/eec/common/instance/storemail.png" id="storemail"/>
<img src="<%=ctxPath%>/images/eec/common/instance/storepass.png" id="storepass"/> --%>
</div>
</div>
<aui:script use="aui-io-request-deprecated, aui-loading-mask-deprecated">
    AUI().use( 'aui-tooltip-deprecated','aui-loading-mask-deprecated', function(A){

    var myDivA = A.one('#insA');
    var myDivNA = A.one('#insNA');
    
    A.one('#<portlet:namespace />save').on('click', function() {
                         var instName = document.getElementById('<portlet:namespace />instanceName').value;
                         var email = document.getElementById('<portlet:namespace />email').value;
                         var pass = document.getElementById('<portlet:namespace />password').value;
                         var regex=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                         if(instName=="Store Name" || email=="" || pass=="" || instName=="" ||email=="E-mail" || pass=="Password" || !email.match(regex)){
                         return false;
                         }
                        
                         else{
                            // alert('in else ');
                             if (A.one('#wrapper').loadingmask == null)
                              A.one('#wrapper').plug(A.LoadingMask, 
                              { 
                                background: '#000',
                                strings: {
                                loading: 'setup in progress please wait ...'
                              } 
                              });    
                             A.one('#wrapper').loadingmask.toggle();
                             document.<portlet:namespace />fm.submit();
                       }      
              });
        
     A.one('#<portlet:namespace />instanceName').on('blur', function() {
            //var A = AUI();
                var instName = document.getElementById('<portlet:namespace />instanceName').value;
                var url = '<%=resouceURL.toString()%>';
                //myDivA.hide();
                A.io.request(
                    url,
                    {
                        //data to be sent to server
                        data: {
                            <portlet:namespace />param1: instName,
                            <portlet:namespace />param2: 'hello2',
                            
                        },
                        dataType: 'json',
                        
                        on: {
                            failure: function() {
                            },
                            
                            success: function(event, id, obj) {
                                var instance = this;
                                
                                //JSON Data coming back from Server
                                var message = instance.get('responseData');
                                
                                if (message) {
                                    document.getElementById('<portlet:namespace />instanceName').value = "";
                                    myDivNA.show();
                                    myDivA.hide();
                                }
                                else {
                                    myDivNA.hide();
                                    if(instName && instName!='Store Name') {
                                    myDivA.show();
                                    }else {
                                    myDivA.hide();
                                    }
                                }
                            }
                            
                        }
                    }
                    
                ); //END of io Request
        },
        ['aui-io-deprecated']
    );  //End of Provide 
    });
</aui:script>
<script>
$(function () {
    $('input,textarea,text').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
               .attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
});
</script>
<style>
.aui input, .aui input-text, .aui .form-validator-error {
    /* width:500px; */
    height: 25px;
    border-radius:10px;
    color:#ffffff;
    font-weight:bold;
    opacity:0.30; 
}

.aui .btn-primary:hover, .aui .btn-primary:focus, .aui .btn-primary:active, .aui .btn-primary.active, .aui .btn-primary.disabled, .aui .btn-primary[disabled]{
background-color:silver;
}

.aui .btn-primary {
background-color:silver;
}

.aui .btn:hover, .aui .btn:focus {
background-position:0px;
}

.aui .control-group.error .control-label, .aui .control-group.error .help-block, .aui .control-group.error .help-inline {
    color:#ffffff;
}

 .aui .control-group.error input{
 color:#ffffff;
 }
 
#insttab td {
    padding-top:18px;
}

.aui .help-inline {
    top: 1px;
    border:2px solid #A82C36;
    opacity:0.8;
    font-size:12px;
    background:#212121;
    position: relative;
    top: -4px;
}


#storename {
    position: absolute; 
    top:40px;
    margin-left: 15px; 
    
}

#storemail {
    position: absolute; 
	bottom: 155px;
    margin-left: 12px; 
}

#storepass {
    position: absolute; 
    bottom: 100px;
    margin-left: 13px;
}

.safari #storename{
    top: -220px; 
}

.safari #storemail{
    top: -164px; 
}

.safari #storepass{
    top: -110px; 
}

.ie #storename{
top: -220px; 
}

.ie #storemail{
top: -164px; 
}

.ie #storepass{
top: -110px;
}

#p_p_id_instancecreation_WAR_esquareecommerceportlet_ .portlet-borderless-container {
background:none;
}

.aui .control-group {
    margin-bottom: 10px;
}


#_instancecreation_WAR_esquareecommerceportlet_instanceName {
	background:#000000;
	font-size:20px; 
	font-family: centurygothic; 
	padding-left:30px; 
	width: 491px;
	padding-top: 7px;
	padding-bottom: 7px;
	border-radius: 10px;
	margin-left: 8px;
}	

#_instancecreation_WAR_esquareecommerceportlet_email {
	background:#000000;
	font-size:20px; 
	font-family: centurygothic; 
	padding-left:30px; 
	width: 491px;
	padding-top: 7px;
	padding-bottom: 7px;
	border-radius: 10px;
	margin-left: 8px;
}

#_instancecreation_WAR_esquareecommerceportlet_password {
	background:#000000;
	font-size:20px; 
	font-family: centurygothic; 
	padding-left:30px; 
	width: 491px;
	padding-top: 7px;
	padding-bottom: 7px;
	border-radius: 10px;
	margin-left: 8px;
}	

#_instancecreation_WAR_esquareecommerceportlet_save {
	text-shadow:none;
	width: 157px;
	height: 47px;
	position:relative;
	font-family: arial;
    font-size: 14px;
	margin-left: 373px; 
	border-radius:4px;
	background-image:url("/esquarecart-theme/css/../images/eec/create-acct.png");
}

#instancecrt {
width: 100% !important;
}

@media (max-width: 480px) {
#instancecrt {
width:0 !important;
}
#instancecreation {
    margin-left: 0 !important;
    margin-top: 0 !important;
}
#_instancecreation_WAR_esquareecommerceportlet_instanceName, #_instancecreation_WAR_esquareecommerceportlet_email, 
#_instancecreation_WAR_esquareecommerceportlet_password  {
width:187px;
padding-bottom: 0px;
padding-top: 0px;

}
#storename{
top:35px;
}
#_instancecreation_WAR_esquareecommerceportlet_save {
margin-left: 40px;
height: 48px;
}
#storemail {
bottom: 148px;
} 
#storepass {
bottom: 103px;
}
body {
overflow-x: hidden;
}
}

@media screen and (min-width: 481px) and (max-width: 768px){
#instancecreation {
    margin-left: 0 !important;
    margin-top: 0 !important;
}
#_instancecreation_WAR_esquareecommerceportlet_instanceName, #_instancecreation_WAR_esquareecommerceportlet_email,
#_instancecreation_WAR_esquareecommerceportlet_password {
width: 337px;
padding-top: 0px;
padding-bottom: 4px;
}
#storename {
top: 37px;
}
#storemail {
bottom: 154px;
}
#storepass {
bottom: 105px;
}
#_instancecreation_WAR_esquareecommerceportlet_save {
margin-left: 246px;
height: 44px;
width: 157px;
}
body {
overflow-x: hidden;
}
}
</style>