<%@ include file="/html/eec/common/compare/init.jsp" %>

<%
PortletSession pSession = renderRequest.getPortletSession();
String redirect = PortalUtil.getCurrentURL(request);
String details=StringPool.BLANK;

String inventoryId = null;
Cookie cookie = EECUtil.getCookie(renderRequest,
		"compare-inventory-ids");
if (Validator.isNotNull(cookie))
	inventoryId = cookie.getValue();
%>
<c:if test='<%=Validator.isNull(inventoryId)%>'>
<style>
#_compare_WAR_esquareecommerceportlet_fm{
display:none;
}
</style>
</c:if>

<%
long compareLayoutId = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.COMPARE_PAGE_FRIENDLY_URL).getPlid();

PortletURL actionURL=EECUtil.getPortletURL(renderRequest,EECConstants.COMPARE_PORTLET_NAME, compareLayoutId, ActionRequest.RENDER_PHASE,"/html/eec/common/compare/compare_products.jsp", null,LiferayWindowState.NORMAL, LiferayPortletMode.VIEW);
if(Validator.isNotNull(inventoryId))actionURL.setParameter("compare_inventory_ids", inventoryId);


PortletURL userviewURL = renderResponse.createActionURL();
long inventoryIdCount=0;
if(Validator.isNotNull(inventoryId)){
	String[] productInventoryIds=inventoryId.split(StringPool.COMMA);
	String bodyContent =  UserViewUtil.getTemplate(themeDisplay.getScopeGroupId(),"Compare Products Template");
	StringBuffer sb=new StringBuffer();
		for(String productInventoryId:productInventoryIds){
			if(Validator.isNotNull(productInventoryId)){
				inventoryIdCount+=1;
			    details = UserViewUtil.getCompareProductsContent(bodyContent,themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(), Long.parseLong(productInventoryId), themeDisplay.getPathThemeImages(),actionURL,userviewURL,ctxPath,renderResponse.getNamespace(),sb,renderRequest);
			}
		}
	}

%>
<div align="center">

<aui:form method="post" name="fm" style="z-index:999;"  >

<input type="hidden" id="inventoryId" name="inventoryId" value="<%=inventoryId%>" />
<input type="hidden" id="redirect" name="redirect" value="<%= redirect %>" /> 
<div class="compare-view">
<img src="<%=renderRequest.getContextPath()%>/images/eec/common/compare/close.png" onclick="deleteAllProducts()" style="float: right; cursor: pointer;"/>
<%=details%>
<%-- 	<% for (inventoryIdCount =1; inventoryIdCount <=4 ; inventoryIdCount++) {%>
		
	
	<li>
    	<div id="eeccompare-maindiv"  >
        	<div id="eeccompare-proimgdiv" style="float:left" >
       			<div style="width: 10px; float: left; height: 10px;">
                	<img src="<%=themeDisplay.getPathThemeImages()%>/eec/compare.png" alt="Image" id="eeccompare-proimg"/>
            	</div> 
            	<div style="width:10px;float:left;color:#000000" class="ecart-normfontfam">Add Item
            	</div>
        	</div>
    	</div>
	</li>
<%} %> --%>


<c:choose>
<c:when test="<%=inventoryIdCount>1%>" >
	<%-- <input type="image" name="compare" src="<%=themeDisplay.getPathThemeImages()%>/eec/compare.png" onclick="compareProducts();"/> --%>
	<div id="eeccaart-comaprebutton" style="display:inline-block;">
	<input type="button" value="Compare"  onclick="compareProducts();" class="comparebutton"/>
	</div>
</c:when>
<c:otherwise>
<div id="eeccaart-comaprebutton" style="display:inline-block;" >
	<div style="color: rgb(255, 255, 255); cursor: not-allowed; height: 26px; padding-top: 8px; text-align: center; text-shadow: none; background-color: gray; width: 130px; font-family: myfont;" >Compare </div>
	</div>
</c:otherwise>

</c:choose>

</div>


	<%-- <c:if test="<%=inventoryIdCount>1%>">
	<input type="image" name="compare" src="<%=themeDisplay.getPathThemeImages()%>/eec/compare.png" onclick="compareProducts();"/>
	<div id="eeccaart-comaprebutton">
	<input type="button" id="detail_pro_added_to_wishlist" value="Compare"  onclick="compareProducts();" style="width: 157px;height: 35px;" />
	</div>
	</c:if> --%>
</aui:form>
</div>

<%
//pSession.removeAttribute("productInventoryId",pSession.PORTLET_SCOPE);
%>
<portlet:resourceURL var="rurl" />
<aui:script>
    Liferay.on('CompareProduct',
            function(event,p_data) {
            var A = AUI(); 
        A.use('aui-io-request', function(aui) {
            A.io.request("<%=rurl %>", { 
                method : 'POST', 
                data: {<portlet:namespace />selProductInventoryId: event.curInventoryId},
                on : { 
                    success : function() { 
                    	Liferay.Portlet.refresh('#p_p_id<portlet:namespace/>');
                    } 
                } 
            });
        });
           
     });
     
    
     
function compareProducts(){
	var inventoryId=document.getElementById("inventoryId").value;
	document.<portlet:namespace />fm.action="<%= actionURL.toString()%>" + "&productInventoryIds=" + inventoryId+"&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>";
	submitForm(document.<portlet:namespace />fm);
}


function <portlet:namespace />deleteSelectedProduct(id) {
	var redirect = document.getElementById("redirect").value;
	var inventoryId=document.getElementById("inventoryId").value;	
	var msg= "Do you really want to delete this Product?";
	if (confirm(msg)){
		document.<portlet:namespace />fm.action="<%= userviewURL.toString()%>" + "&productInventoryIds=" + inventoryId+"&selectedInventoryId=" + id+"&redirect=" + redirect+"&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
		submitForm(document.<portlet:namespace />fm);
	}
	 
}


function deleteAllProducts() {
	var redirect = document.getElementById("redirect").value;
	var inventoryId=document.getElementById("inventoryId").value;	
	var msg= "Do you really want to delete all Products?";
	if (confirm(msg)){
		document.<portlet:namespace />fm.action="<%= userviewURL.toString()%>" + "&productInventoryIds=" + inventoryId+"&selectedInventoryId=<%=StringPool.BLANK%>&redirect=" + redirect+"&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
		submitForm(document.<portlet:namespace />fm);
	}
	 
}


</aui:script>
<script>

	$(window).scroll(function() {
		var $compare = $('#_compare_WAR_esquareecommerceportlet_fm');
		var scrollpos = $(window).scrollTop();
		console.log(scrollpos);
		
		
		if (scrollpos >= 180 ) {
					if($('#p_p_id_compare_WAR_esquareecommerceportlet_').parent().attr('id') != "layout-column_column-2"){	
						$compare.css({
						"top" : "0",
						"position" : "fixed",
						"width":"100%",
						});
					}else{
						$compare.css({
							"top" : "0",
							"position" : "fixed",
							"width":"100%",
							});
					}
		} else {
			$compare.css({
				"position" : "relative",
				"top" : "0",
				"width":"100%",
			});
		}
	});
</script>




<style type="text/css">
ol, ul, dl{
margin: 0;
}


</style>