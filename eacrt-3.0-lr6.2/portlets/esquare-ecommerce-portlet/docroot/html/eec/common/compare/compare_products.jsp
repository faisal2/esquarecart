<%@ include file="/html/eec/common/compare/init.jsp" %>


<%
PortletSession ps = renderRequest.getPortletSession();
String parentCatalogId = (String) ps.getAttribute("parentCatalogId",PortletSession.APPLICATION_SCOPE);
long cartPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.CART_PAGE_FRIENDLY_URL).getPlid();
PortletURL cartViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.CART_PORTLET_NAME, cartPlid,
		ActionRequest.ACTION_PHASE, null,null, LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);

String inventoryId = (String) request.getParameter("compare_inventory_ids");
long inventoryIdCount=0;
long catalogId=0;
if(Validator.isNotNull(parentCatalogId)){
	//catalogId=parentCatalogId;
	Catalog catalog=CatalogLocalServiceUtil.getCatalog(Long.parseLong(parentCatalogId));
	catalogId=catalog.getParentCatalogId();
}
String[] keys = PortletPropsValues.COMPARE_COMMON_PROPERTIES;

ProductInventory compare1 = null;
ProductInventory compare2 = null; 
ProductInventory compare3 = null; 
ProductInventory compare4 = null; 

List<EcustomFieldValue>  fieldValueList= null;
%>
<div class="container-fluid">
<aui:form method="post" name="frm" >
	<table border='1' width="100%" id="eec_compare_table">
		<%
			if(Validator.isNotNull(inventoryId)){
			String[] productInventoryIds=inventoryId.split(StringPool.COMMA);
			for(int i=0; i<productInventoryIds.length; i++){
				if(i==0)
				 compare1 = ProductInventoryLocalServiceUtil.getProductInventory(Long.parseLong(productInventoryIds[0]));
				if(i==1)
				 compare2 = ProductInventoryLocalServiceUtil.getProductInventory(Long.parseLong(productInventoryIds[1]));
				if(i==2)
				 compare3 = ProductInventoryLocalServiceUtil.getProductInventory(Long.parseLong(productInventoryIds[2]));
				if(i==3)
				 compare4 = ProductInventoryLocalServiceUtil.getProductInventory(Long.parseLong(productInventoryIds[3])); 
			}
		%>
		<%
			String buyNowURL1="addToCartPage('"+String.valueOf(compare1.getInventoryId())+"')";
			String buyNowURL2="addToCartPage('"+String.valueOf(compare2.getInventoryId())+"')";
			int i=1;
			
			
				
		%>
	
			 <%-- <%for (String key :keys) { %>
			 	<div class="row-fluid">
			<tr id="xyz" style="display:none" >
				<td style="border-right-width: 0px; border-bottom-width: 0px; padding: 0px;">
					<table  >
							<tr class="afix">
									<td class="key" id="productscroll" ><%=key%></td>
									<td class="compontent"  id="productscrollimage" >
										<%String comp1=EECUtil.getCompareProductDetails(key,compare1); %>
										<%=comp1 %>
										<div>
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center>
							<input type="button" id="detail_pro_added_to_wishlist"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></center>
							</c:if>
						</div>
									</td>
									<td class="compontent"  id="productscrollimage1">
										<%				String comp2=EECUtil.getCompareProductDetails(key,compare2);
										 %>
										<%=comp2 %>
										<div>
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center>
							<input type="button" id="detail_pro_added_to_wishlist"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></center>
							</c:if>
						</div>
									</td>
									<td class="compontent" id="productscrollimage2">
									<c:if test="<%= compare3!=null%>" >
										<% 
											String comp5=EECUtil.getCompareProductDetails(key,compare3);
										%>
										<%=comp5%>
										<div>
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center>
							<input type="button" id="detail_pro_added_to_wishlist"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></center>
							</c:if>
						</div>
									</c:if>
									</td>
									<td class="compontent" id="productscrollimage3">
									<c:if test="<%= compare4!=null%>" >
										<% 
											String comp6=EECUtil.getCompareProductDetails(key,compare4);
										%>
										<%=comp6%>
										<div>
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center>
							<input type="button" id="detail_pro_added_to_wishlist"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></center>
							</c:if>
						</div>
									</c:if>
									</td>
							</tr>
					</table>		
				</td>			
					
			</tr>
			</div>
			<% } %> --%>
			 
			
			
			<tr id="eec_compare_tableRow" ><td colspan="5" class="eec_compare_title"> Product Comparison</td></tr>
			<%for (String key :keys) { %>
			
			 <tr id="eec_compare_tableRow1" class="<%=(i%2==0) ? "even":"odd" %> "> 
			
				
					<td class="key"  ><%=key%>		
					</td>
					<td class="compontent" >
					<%				String comp1=EECUtil.getCompareProductDetails(key,compare1);
					 %>
						<%=comp1%>
						<div>
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center>
							<input type="button" id="comparebuy"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></center>
							</c:if>
						</div>
					</td>
					
					
					<td class="compontent" >
					<%				String comp2=EECUtil.getCompareProductDetails(key,compare2);
					 %>
						<%=comp2 %>
						<div >
							<c:if test='<%= key.equalsIgnoreCase("product")%>'>
							<br/><center><input type="button" id="comparebuy"   value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL2%>" /></center>
							</c:if>
						</div>
					</td>
					
					<td class="compontent"  >
						<c:choose>
						<c:when test="<%= compare3!=null%>">
							<%
							String comp3=EECUtil.getCompareProductDetails(key,compare3);
							String buyNowURL3="addToCartPage('"+String.valueOf(compare3.getInventoryId())+"')";
							%>
							<%=comp3 %>
							<div >
							<c:if  test='<%= key.equalsIgnoreCase("product")%>'>
								<br/><center><input type="button" id="comparebuy"  value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL3%>" /></center>
							</c:if>
								</div>
							</c:when>
							<c:otherwise >
								<%-- <c:if  test='<%= key.equalsIgnoreCase("product")%>'>
									<select name="sel">
										<option value="Alto">Alto</option>
										<option value="Esteem">Esteem</option>
										<option value="Honda City">Honda City</option>
										<option value="Chevrolet">Chevrolet</option>
									</select> 
								</c:if> --%>
								<div align="center">
								<a href="/home" class="compareshop">Shop More</a>
								</div>
								<c:if  test='<%= key.equalsIgnoreCase("price") || key.equalsIgnoreCase("availability") %>'>
									<span>NA</span>
								</c:if>
							</c:otherwise>
						</c:choose>					
					</td>
					
					<td class="compontent" >
						<c:choose>
							<c:when test="<%= compare4!=null%>">
								<%
								String comp4=EECUtil.getCompareProductDetails(key,compare4);
								String buyNowURL4="addToCartPage('"+String.valueOf(compare4.getInventoryId())+"')"; 
								%>
								<%=comp4 %>
								<c:if test='<%= key.equalsIgnoreCase("product")%>'>
									<br/><center><input type="button" id="comparebuy"  value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL4%>" /></center>
								</c:if>
							</c:when>
						<c:otherwise >
								<%-- <c:if  test='<%= key.equalsIgnoreCase("product")%>'>
							<%	
							
							DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Catalog.class);
							dynamicQuery.add(RestrictionsFactoryUtil.eq("parentCatalogId", 0L));
							dynamicQuery.add(RestrictionsFactoryUtil.eq("companyId", PortalUtil.getCompanyId(request)));
							List<Catalog> catalogs = CatalogLocalServiceUtil.dynamicQuery(dynamicQuery);
								%>
									<select name="sel">
									<%for(Catalog catalog : catalogs) {%>
										<option value="<%=catalog.getCatalogId()%>"><%=catalog.getName() %></option>
									<% }%>
									</select>
								</c:if> --%>
								<div align="center">
									<a href="/home" class="compareshop">Shop More</a>
									</div>
								<c:if  test='<%= key.equalsIgnoreCase("price") || key.equalsIgnoreCase("availability") %>'>
									<span>NA</span>
								</c:if>
							</c:otherwise>
						</c:choose>	
					</td>
					</div>
				</tr>

									
			<% i++; 
			}
			
			int j=1;
			try{
			EcustomField custFieldobj =EcustomFieldLocalServiceUtil.getEcustomField(compare1.getCustomFieldRecId());
			List<ShippingRegion> ecustomFieldlist = EECUtil.readXMLContent(custFieldobj.getXml(),PortletPropsValues.CUSTOM_FIELD_TAG);
			%>			
			<tr id="eec_compare_tableRow" ><td colspan="5" class="eec_compare_title"> General Features</td></tr><br/>
			<%
				for(ShippingRegion customField:ecustomFieldlist){
				%>
				<tr id="eec_compare_tableRow" class="<%=(j%2==0) ? "even":"odd" %>">
					<td class="key"><%= customField.getName() %></td>
					<td class="compontent">
						<%
						String value1=EECUtil.getCustomFieldsValue(themeDisplay.getCompanyId(),customField,compare1);
						%>
						<%= value1%> 
					</td>
					<td class="compontent">
						<%
						String value2=EECUtil.getCustomFieldsValue(themeDisplay.getCompanyId(),customField,compare2);
						%>
						<%= value2%>  
					</td>
					<td class="compontent">
						 <c:if test="<%= compare3!=null%>">
						<%
						String value3=EECUtil.getCustomFieldsValue(themeDisplay.getCompanyId(),customField,compare3);
						%>
						<%= value3%>   
						</c:if> 
					</td>
					
					<td class="compontent">
						 <c:if test="<%= compare4!=null%>">
						<%
						String value4=EECUtil.getCustomFieldsValue(themeDisplay.getCompanyId(),customField,compare3);
						%>
						<%= value4%>  
						</c:if> 
					</td>
				</tr>
		
				<% j++; }
				}catch(Exception e){}%>		
				<%-- <tr id="eec_compare_tableRow">
					<td></td>
					<td align="center"><input type="button" id="detail_pro_added_to_wishlist"  value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL1%>" /></td>
					<td align="center"><input type="button" id="detail_pro_added_to_wishlist"  value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL2%>" /></td>
					<td align="center">
						<c:if test="<%= compare3!=null%>">
						<%
						String buyNowURL3="addToCartPage('"+String.valueOf(compare3.getInventoryId())+"')";
						%>
							<input type="button" id="detail_pro_added_to_wishlist"  value="Buy Now" name="Buythisnow" onclick="<%=buyNowURL3%>" />
						</c:if>
					</td>
					<td align="center">
						<c:if test="<%= compare4!=null%>">
						<%
						String buyNowURL4="addToCartPage('"+String.valueOf(compare4.getInventoryId())+"')"; 
						%>
							<input type="button" id="detail_pro_added_to_wishlist" value="Buy Now" name="Buythisnow"  onclick="<%=buyNowURL4%>" />
						</c:if>
					</td>
				</tr>	 --%>
		<%}%>		
	</table>
</aui:form>
</div>
<!-- 	
<style>
	.eec_compare_title{
	font-weight: bold;
	font-size: 15px;
	}
	table#eec_compare_table { border-collapse:separate;border: 1px dotted #EC4D25; }
	table#eec_compare_table tr,table#eec_compare_table td 
	{
	border: 1px dotted #EC4D25;
	}
	tr#eec_compare_tableRow td{
	font-size: 13px;
	}
</style> -->

<aui:script>

	function addToCartPage(curInventoryId){
 	document.<portlet:namespace />frm.action="<%= cartViewURL.toString()%>" + "&productInventoryIds=" + curInventoryId+"&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<%= EECConstants.CMD1 %>=<%= EECConstants.CART_UPDATE %>";
	submitForm(document.<portlet:namespace />frm);
	}
	
</aui:script>

<script>

	
	$(window).scroll(function() {
		var $compare = $('#xyz');
		var scrollpos = $(window).scrollTop();
		console.log(scrollpos);
		
		
		if (scrollpos >= 280 ) {
			$compare.css({
						"display":"block",
						"top" : "0",
						"position" : "fixed",
						"z-index": "999",
						"background" : "#F5F5F5",
						});
					} else {
						$compare.css({
							"position" : "relative",
							"top" : "0",
							"display":"none",
						});
					}
	});
		
</script>
<style>
@media only screen and (max-width: 960px) {
    #eec_compare_table td {
        display:block;
    }
    #xyz{
    display:none !important;
    }
}

</style>