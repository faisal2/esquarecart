<%@ include file="/html/eec/common/wishList/init.jsp" %>

<%
PortletURL detailsURL = renderResponse.createActionURL();
detailsURL.setParameter("javax.portlet.action", "redirectToDetailsPage");

long cartPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.CART_PAGE_FRIENDLY_URL).getPlid();
PortletURL cartViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.CART_PORTLET_NAME, cartPlid,
		ActionRequest.ACTION_PHASE, null,null, LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);

String 	details = StringPool.BLANK;
PortletURL actionURL = renderResponse.createActionURL();
PortletURL renderURL = renderResponse.createRenderURL();

WishList wishList = EECUtil.getWishList(renderRequest);
Map productInventories = wishList.getProductinventories();


 if(Validator.isNotNull(wishList)){
try{
	details = UserViewUtil.getWishListTemplateContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),productInventories,renderURL,cartViewURL,
							request.getContextPath(),renderResponse.getNamespace(),themeDisplay.isSignedIn(),themeDisplay.getPortalURL(),renderRequest);
}
catch(Exception e){
} 
}

%>

<aui:form method="post" action="<%=actionURL%>" name="fm">
<aui:input name="productInventoryIds" type="hidden" />
<aui:input name="<%= EECConstants.CMD %>" type="hidden" value="<%= EECConstants.UPDATE %>" />
<aui:input name="<%= EECConstants.CMD1 %>" type="hidden" value="<%=EECConstants.WISHLIST_UPDATE%>" />
<%=details%>
</aui:form>


<aui:script>

	function <portlet:namespace />addToDetailsPage(curInventoryId){
 	document.<portlet:namespace />fm.action="<%= actionURL.toString()%>" + "&curProductId=" + curInventoryId+"&<%= EECConstants.CMD1 %>=<%= EECConstants.PRODUCT %>";
	submitForm(document.<portlet:namespace />fm);
	}

 function addToCartPage(curProductId){
 	document.<portlet:namespace />fm.action="<%= cartViewURL.toString()%>" + "&productInventoryIds=" + curProductId+"&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<%= EECConstants.CMD1 %>=<%= EECConstants.WISHLIST_UPDATE %>";
	submitForm(document.<portlet:namespace />fm);
	}
	
	function <portlet:namespace />deleteSelectedWishList(id) {
		var productInventoryIds = "";
		var count = 0;
		<%
		Iterator iterat = productInventories.entrySet().iterator();
	
		for (int i = 0; iterat.hasNext(); i++) {
			Map.Entry entry = (Map.Entry)iterat.next();
	
			WishListItem wishListItem = (WishListItem)entry.getKey();
	
			ProductInventory productInventory = wishListItem.getProductInventory();
		%>
					if('<%= productInventory.getInventoryId() %>' != id) productInventoryIds += "<%= productInventory.getInventoryId()%>,";
			
		<%}	%>
	       var msg= "Do you really want to delete this Product?";
	       if (confirm(msg)){
	      		document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = productInventoryIds;
				submitForm(document.<portlet:namespace />fm);
			}
	 
	}
	
	</aui:script>