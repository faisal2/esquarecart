<%@ include file="/html/eec/common/similarProduct/init.jsp" %>

<%
		int priceRange=0;
		try{
		priceRange = Integer.parseInt(EECUtil.getPreference(themeDisplay.getCompanyId(), EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT));
		}catch(NumberFormatException exception){
		priceRange=PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
		}

		PortletSession pSession=renderRequest.getPortletSession();
		String eventProductId=(String)pSession.getAttribute("EVENT_PRODUCTID",pSession.PORTLET_SCOPE);
		long productId=0;
		if(Validator.isNotNull(eventProductId)){
			productId=Long.parseLong(eventProductId);
		}
		String 	details =StringPool.BLANK;
		String type=(String)request.getParameter("type");
		
		long userViewPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(
				themeDisplay.getScopeGroupId(), false, "/productdetails").getPlid();
		PortletURL actionURL = PortletURLFactoryUtil.create(request,
				EECConstants.USER_VIEW_PORTLET_NAME, userViewPlid,
				PortletRequest.ACTION_PHASE);
		actionURL.setWindowState(LiferayWindowState.NORMAL);
		actionURL.setParameter("javax.portlet.action", "redirectToDetailsPage");
		
		long layoutId = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, "/products").getPlid();
		PortletURL userViewURL = PortletURLFactoryUtil.create(request,EECConstants.USER_VIEW_PORTLET_NAME, layoutId, "ACTION_PHASE");
		userViewURL.setParameter("javax.portlet.action", "navigationProcess");
		userViewURL.setWindowState(WindowState.NORMAL);
		userViewURL.setPortletMode(PortletMode.VIEW);
		
		if(productId>0){
		details = UserViewUtil.getSimilarProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(), productId, type,themeDisplay.getPathThemeImages(),actionURL,userViewURL,ctxPath,priceRange,renderRequest);
		}
	
%>
<aui:form method="post" name="fm">
<%
if(Validator.isNotNull(eventProductId)){%>
<div class="similarproduct">
	<%=details %>
	</div>
<%}%>
	
</aui:form>
<%
pSession.removeAttribute("EVENT_PRODUCTID",pSession.PORTLET_SCOPE);
%>


