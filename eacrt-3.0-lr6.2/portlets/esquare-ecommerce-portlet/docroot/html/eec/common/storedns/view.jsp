<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="javax.portlet.RenderResponse"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/jquery-1.10.1.min.js"></script>

<%@page import="com.liferay.portal.model.Company"%>
<%@page import="com.liferay.portal.service.CompanyLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.util.PortletPropsValues"%>
<%@page import="com.esquare.ecommerce.service.InstanceLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.Instance"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<% 
	Instance curInstance = InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId());
	PortletURL portletURL = renderResponse.createRenderURL();
	String tabs1 = ParamUtil.getString(request, "tabs1", "Change Domain Name");
	portletURL.setParameter("tabs1", tabs1);
	
	StringBundler tabName= new StringBundler("Change Domain Name");
	if(!curInstance.getPackageType().equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_ENTERPRISE)){
		tabName.append(StringPool.COMMA);
		tabName.append("Change Package");
	}
	PortletURL editPackageUrl = renderResponse.createRenderURL();
%>

<div class="dashboard-maindiv2">
<div class="headingsetting">Domain</div>
<hr style="margin-top: 13px;">
<!-- <h3>Store Settings</h3> -->
<!-- <div id="dbsetting-domaildnsnam">Store Settings</div> -->

<portlet:actionURL var="editpackageURL" />

<aui:form name="frm" action="<%=editpackageURL %>" method="post">
<div id="dbdomain-maindiv">
	<span class="dashboardsetting-emailtab" >
	<liferay-ui:tabs
	names='<%=tabName.toString()%>'
	portletURL="<%= portletURL %>"
	tabsValues="<%=tabs1%>" param="tabs1"
	refresh="false">
		
		<liferay-ui:section>
			<%@ include file="/html/eec/common/storedns/change_dns.jsp"%>
		</liferay-ui:section>
		
		<liferay-ui:section>
			<%@ include file="/html/eec/common/storedns/change_package.jsp"%>
		</liferay-ui:section>
		
	</liferay-ui:tabs>
	</span>
	</div>
</aui:form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#changeDomain").hide();
		$("#editdomain").on('click' , function(){
			//alert('test');
			$("#changeDomain").show();
			});
		$("#<portlet:namespace />next").on('click' , function(){
			//$("#changeDomain").show();
			var selPck = $("#<portlet:namespace />packageType").val();
			//alert('next'+selPck);
			document.<portlet:namespace />frm.action="<%= editPackageUrl.toString()%>" + "&selectedPck="+ selPck;
			document.<portlet:namespace/>frm.submit();
			});
	});
</script>
