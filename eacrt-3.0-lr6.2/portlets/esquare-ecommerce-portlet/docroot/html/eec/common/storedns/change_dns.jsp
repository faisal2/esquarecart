
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.model.Company"%>
<%@page import="com.liferay.portal.service.CompanyLocalServiceUtil"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<div style="margin-left: 20px;">
<div style="font-size: large;font-family: centurygothic; color: #888">Store Domain Name Setting</div>
<br>
<%
	Company companyDetail = CompanyLocalServiceUtil
			.getCompanyById(themeDisplay.getCompanyId());
%>

<span id="dbsetting-domaildnsnam" class="name1"> Domain Name : </span>
<span id="dbsetting-domaildnsvalue" class="name1"> <%=companyDetail.getVirtualHostname()%></span>
<br>


<span id="dbsetting-domaildnsnam" class="name1"> EmailAddress&nbsp;: </span>
<span id="dbsetting-domaildnsvalue" class="name1"> <%=themeDisplay.getUser().getEmailAddress()%></span>
<br>
<br>
<span id="editdomain" style="cursor:pointer"> <img
	src="<%=ctxPath%>/images/eec/common/catalog/edit.png" height="13"/><span
	id="dbsetting-domaildnsnam" style="margin-left:2px;">Edit</span></span>
</div>
<div id="changeDomain">
	<aui:input name="virtualHost" lable="Enter New Domain Name"	inlineLabel="true">
     <aui:validator name="custom" errorMessage="please-enter-valid-domain-name">
		function (val, fieldNode, ruleValue) {
		var regexp ="^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
			if(val.search(regexp) != -1){
					return true;
			}
			return false;
		}
		</aui:validator>
	</aui:input>
	<aui:input name="hostName" type="hidden"
		value="<%=companyDetail.getVirtualHostname()%>" />
	<span class="dashboard-buttons"> <aui:button type="submit"
			name="update"
			style="margin-left: 33px; margin-bottom: 15px; margin-top: 7px;"></aui:button></span>
</div>



