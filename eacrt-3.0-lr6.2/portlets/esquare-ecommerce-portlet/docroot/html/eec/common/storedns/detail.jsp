<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.esquare.ecommerce.model.PaymentUserDetail"%>
<%@page import="com.esquare.ecommerce.service.PaymentUserDetailLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.AddressLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Phone"%>
<%@page import="com.liferay.portal.model.Address"%>
<%@page import="com.liferay.portal.service.PhoneLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalService"%>
<%@page import="com.esquare.ecommerce.util.EBSUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="java.util.HashMap"%>

<script src="<%=request.getContextPath()%>/js/eec/common/template/jquery.min.js"></script>
<script src="/html/js/liferay/service.js" type="text/javascript"> </script>

<%
	themeDisplay.setIncludePortletCssJs(true);

%>

<portlet:actionURL var="returnURLEBS">
	<portlet:param name="<%= EECConstants.CMD %>" value="<%= EECConstants.PAY_EBS %>" />
</portlet:actionURL>

<%
	long userId = themeDisplay.getUserId();
	User selUser = themeDisplay.getUser();
	
	List<Address> address = AddressLocalServiceUtil.getAddresses(themeDisplay.getCompanyId(),"com.liferay.portal.model.Contact",themeDisplay.getContact().getContactId());
	List<Phone> phones = PhoneLocalServiceUtil.getPhones(themeDisplay.getCompanyId(),"com.liferay.portal.model.Contact",themeDisplay.getContact().getContactId());
	
	PaymentUserDetail userDetails  = null;
	try{
		userDetails =  PaymentUserDetailLocalServiceUtil.findByUserId(userId);
	}catch(Exception e){
		//
	}
	
	String packageType = (String)request.getAttribute("packageType");
	String domainName = (String)request.getAttribute("domainName");
	
	String md5HashData = PortletPropsValues.EBS_SECRET_KEY;
	String paymentMode = PortletPropsValues.EBS_PAYMENT_MODE;
	String accountNumber = PortletPropsValues.EBS_ACCOUNT_ID;
	
	String amount = "";
	
	String street = "";
	String zip = "";
	String city =  "";
	String phone =  "";
	String mobile = "";
	long countryId = 0l;
	long regionId = 0l;

	if(!address.isEmpty() && address!=null){
		street = address.get(0).getStreet1();
		zip = address.get(0).getZip();
		city = address.get(0).getCity();
		countryId = address.get(0).getCountryId();
		regionId = address.get(0).getRegionId();
	}
	
	if(!phones.isEmpty() && phones.size()>1 ){
		phone = phones.get(1).getNumber();	
	}
	if(!phones.isEmpty() && phones!=null){	
		mobile = phones.get(0).getNumber();
	}
	if(Validator.isNotNull(packageType) && packageType.equalsIgnoreCase(EECConstants.BASIC)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_BASIC_PACKAGE_CHARGES);
	}else if(Validator.isNotNull(packageType) && packageType.equalsIgnoreCase(EECConstants.STANDARD)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_STANDARD_PACKAGE_CHARGES);
	}else if(Validator.isNotNull(packageType) && packageType.equalsIgnoreCase(EECConstants.PREMIUM)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_PREMIUM_PACKAGE_CHARGES);
	}else if(Validator.isNotNull(packageType) && packageType.equalsIgnoreCase(EECConstants.ENTERPRISE)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_ENTERPRISE_PACKAGE_CHARGES);
	}
	String returnURL=returnURLEBS.toString()+"&DR={DR}";
	
	md5HashData += '|' + accountNumber + '|' + amount + '|'+ String.valueOf(userId) + '|' + returnURL + '|' + paymentMode;
	
	System.out.println("phone   "+mobile );
%>
<div class="dashboard-maindiv2">
<div class="headingsetting">Domain Details</div>
<hr style="margin-top: 13px;">
<aui:form  action="https://secure.ebs.in/pg/ma/sale/pay" name="fm" method="post" id="fm">
	<input name="account_id" type="hidden" value="<%=accountNumber%>">
	<input name="return_url" type="hidden" size="60" value="<%=returnURL%>">
	<input name="mode" type="hidden" size="60" value="<%=paymentMode%>">
	<input name="reference_no" type="hidden" value='<%=userId %>'>	
	<input name="secure_hash" type="hidden" size="60" value="<%=EBSUtil.md5(md5HashData)%>"/>	
	<input name="description" type="hidden" value="<%= packageType%>"> 
	<input name="country" type="hidden" id="country">
	<input name="state" type="hidden" id="state">
	<input name="ship_name" type="hidden" value="<%= packageType%>">
	<table id="web1"><tr><td >
		WebId :</td><td><input name="webId" lable="WebId" type="text" value="<%=domainName %>" readOnly="readOnly"/></td></tr>
		<tr><td>
		Email :</td><td><input name="email" type="text" class="field-required field-email" value='<%= userDetails != null ? userDetails.getEmail() : selUser.getEmailAddress()  %>'> </td></tr>
		<tr><td>
		Name  : </td><td><input name="name" type="text" class="field-required" maxlength="40" value='<%= userDetails != null ?userDetails.getName() : selUser.getFirstName()%>'></td></tr>
		<tr><td>
		Phone No : </td><td><input name="phone" class="field-required field-digits" type="text" maxlength="20" value='<%= userDetails != null ? userDetails.getPhoneNo() : mobile %>'> </td></tr>
		<tr><td>
		Amount   : </td><td><input name="amount" type="text" class="field-required" value="<%=amount%>" readOnly="readOnly"/> </td></tr>
		<tr><td>
		Street : </td><td><input name="address" type="text" class="field-required" maxlength="150"	value='<%= userDetails != null ? userDetails.getAddress() : street %>'> </td></tr>
		<tr><td>
		Zip code : </td><td><input name="postal_code" type="text" class="field-required field-digits" maxlength="6" value='<%= userDetails != null ? userDetails.getPinCode() : zip%>'></td></tr>
		<tr><td>Counry :</td><td><aui:select label="" name="countryName" /></td> </tr>
		<tr><td>Region :</td> <td><aui:select label="" name="stateName" /></td></tr>
		<tr><td>
			City : </td><td>
			<input name="city" type="text" class="field-required" maxlength="55" value='<%= userDetails != null ? userDetails.getCity() : city%>'>
	</table>
	
		 
	 <aui:button-row>
		<aui:button type="submit" name="submit" value="Pay Via EBS" cssClass="nextdomain" />
	</aui:button-row>  
	
</aui:form>
</div>

<aui:script use="liferay-dynamic-select,aui-io-request-deprecated">
  new Liferay.DynamicSelect(
      [
          {		
              select: '<portlet:namespace />countryName',
              selectData: Liferay.Address.getCountries,
              selectDesc: 'name',
              selectId: 'countryId',
              selectVal: '<%= userDetails != null ? userDetails.getCountry() : countryId%>'
          },
          {
              select: '<portlet:namespace />stateName',
              selectData: Liferay.Address.getRegions,
              selectDesc: 'name',
              selectId: 'regionId',
              selectVal: '<%= userDetails != null ? userDetails.getState() : regionId%>'
          }
      ]
  );
</aui:script>

<style>
.form-validator-message {
	position: relative;
	margin-bottom: -33px;
	margin-left: 224px;
	width: 62%;
}

.field-label{
	display: none;
}
#_storeDNS_WAR_esquareecommerceportlet_fm{
margin-left: 20px;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function(){
	 $("#<portlet:namespace />submit").click(function(){
	       var country = $( "#<portlet:namespace />countryName" ).val();
	       var state = $( "#<portlet:namespace />stateName" ).val();
	       $('#country').val(country);
	       $('#state').val(state);
	    }); 
});
</script>
<style>
</style>