
<%@page import="com.esquare.ecommerce.util.EECUtil"%>
<%@page import="com.liferay.portal.model.Company"%>
<%@page import="com.liferay.portal.service.CompanyLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.util.PortletPropsValues"%>
<%@page import="com.esquare.ecommerce.service.InstanceLocalServiceUtil"%>
<%@page import="com.esquare.ecommerce.model.Instance"%>

<%
	Instance instance = InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId());
	Company companyDetails = CompanyLocalServiceUtil.getCompanyById(themeDisplay.getCompanyId());
	String[] packType = PortletPropsValues.PACKAGE_TYPES;
	
	editPackageUrl.setParameter(Constants.CMD, Constants.EDIT);
	editPackageUrl.setParameter("domainName", companyDetails.getVirtualHostname());
%>
<div style="margin-left: 20px;">
<div style="font-size: large;font-family: centurygothic;color:#888">Store Package Setting</div> <br>
<span id="dbsetting-domaildnsnam" class="name1"> Domain Name     &nbsp;&nbsp;&nbsp;&nbsp;:</span > <span id="dbsetting-domaildnsvalue" class="name1"><%= companyDetails.getVirtualHostname() %></span><br>
<span id="dbsetting-domaildnsnam" class="name1"> Current Package :</span> <span id="dbsetting-domaildnsvalue" class="name1"> <%= instance.getPackageType() %></span><br>

<br>
<aui:input name="companyId" type="hidden" value="<%= companyDetails.getCompanyId() %>"/>
<div id="dbsetting-domaildnsdiv">
<aui:select name="packageType" inlineLabel="true" label="Select Package">
	<%for(String pckType : packType){ 
	if(EECUtil.getPackageValue(pckType)>EECUtil.getPackageValue(instance.getPackageType())){
	%>
		<aui:option value="<%= pckType %>" ><%= pckType %></aui:option>
	<% } }%>
</aui:select>
</div>
<br>
<aui:button type="button" name="next" value="Next" cssClass="nextdomain"></aui:button>
</div>

<style>

</style>
