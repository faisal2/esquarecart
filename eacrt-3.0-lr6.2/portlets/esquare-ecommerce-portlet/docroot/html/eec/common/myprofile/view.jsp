<%@ include file="/html/eec/common/myprofile/init.jsp"%>
<%
	themeDisplay.setIncludePortletCssJs(true);
%>

<script src="/html/js/liferay/service.js" type="text/javascript"></script>
<%
	User selUser = themeDisplay.getUser();
	Contact selContact = null;
	if (selUser != null) {
		selContact = selUser.getContact();
	}
	List<Address> address = AddressLocalServiceUtil.getAddresses(
			themeDisplay.getCompanyId(),
			"com.liferay.portal.model.Contact", themeDisplay
					.getContact().getContactId());
	List<Phone> phones = PhoneLocalServiceUtil.getPhones(themeDisplay
			.getCompanyId(), "com.liferay.portal.model.Contact",
			themeDisplay.getContact().getContactId());
	String street = "";
	String zip = "";
	String city = "";
	String phone = "";
	String mobile = "";
	long countryId = 0l;
	long regionId = 0l;

	if (!address.isEmpty() && address != null) {
		street = address.get(0).getStreet1();
		zip = address.get(0).getZip();
		city = address.get(0).getCity();
		countryId = address.get(0).getCountryId();
		regionId = address.get(0).getRegionId();
	}
	if (!phones.isEmpty() && phones.size() > 1) {
		phone = phones.get(1).getNumber();
	}
	if (!phones.isEmpty() && phones != null) {
		mobile = phones.get(0).getNumber();
	}
	Calendar birthday = CalendarFactoryUtil.getCalendar();
	birthday.set(Calendar.MONTH, Calendar.JANUARY);
	birthday.set(Calendar.DATE, 1);
	birthday.set(Calendar.YEAR, 1970);
	if (selContact != null) {
		birthday.setTime(selContact.getBirthday());
	}
%>


<liferay-portlet:actionURL var="updateProfileURL" />
<div class="dashboard-maindiv" id="profile">
	<!-- <div class="dashboard-pagemainheading100">Profile Page</div> -->
	<aui:form action='<%=updateProfileURL.toString()%>' name="fm"
		method="post" enctype="multipart/form-data">
		<div id="myprofileMaindiv">


			<div class="dashboard-headingstyle" id="db-personaldetails-headerid">Personal Details </div>
				<br>
			<div class="esq_separator">
				<!-- Placeholder -->
			</div>


			<div id="db-personaldetails" class="dashboard-fields-lable">
				<div class="row-fluid">
					<div class="span4 offset2">
						<aui:input id="db-pfirstname" type="text" size="27"
							bean="<%=selUser%>" model="<%=User.class%>" name="firstName"
							showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="alpha" />
							<aui:validator name="maxLength">'15'</aui:validator>
						</aui:input>
					</div>
					<div class="span6">
						<aui:input id="db-plastname" type="text" size="27"
							bean="<%=selUser%>" model="<%=User.class%>" name="lastName"
							showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="alpha" />
							<aui:validator name="maxLength">'15'</aui:validator>
						</aui:input>

					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 offset2" id="db-personldetails-clm1">
						<aui:input id="db-dob" bean="<%=selContact%>"
							model="<%=Contact.class%>" lable="Date of birth"
							name="birthday" value="<%=birthday%>" />
					</div>
					<div class="span6">
						<aui:select bean="<%=selContact%>" label="gender"
							model="<%=Contact.class%>" name="male">
							<aui:option label="male" value="true" />
							<aui:option label="female" value="false" />
						</aui:select>
					</div>
				</div>
			</div>


			<div class="dashboard-headingstyle" id="db-contactdetails-header">MyContacts</div>
			<br>
			<div class="esq_separator">
				<!-- Placeholder -->
			</div>
			<div id="db-personaldetails" class="db-contactdetailscls">
				<div class="row-fluid" id="db-contactdetails-layout">
					<div class="span4 offset2" id="db-contactdetails-column1">
						<aui:input type="text" size="27" label="Email ID"
							name="emailAddress" placeholder="Enter Your Mail Id" bean="<%=selUser%>"
							model="<%=User.class%>" showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="email" />
							<aui:validator name="custom"
								errorMessage="Please enter valid email address">
						function (val, fieldNode, ruleValue) {
							var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 	
							var first_part= val.split("@");										
							if(val.match(mailformat)&&first_part[0].length<=30){
								return true;
							}else{
							    return false;
							}
						}
					</aui:validator>
						</aui:input>
					</div>
					<div class="span6" id="details">
						<aui:input type="text" size="27" label="Mobile" name="mobile"
							value="<%=mobile%>" showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="digits" />
							<aui:validator name="rangeLength">[8,15]</aui:validator>
						</aui:input>

					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 offset2" id="db-contactdetails-column2">
						<aui:input type="text" size="27" label="Skype ID"
							bean="<%=selContact%>" model="<%=Contact.class%>"
							name="skypeSn">
							<aui:validator name="maxLength">'30'</aui:validator>
						</aui:input>
					</div>
				</div>
			</div>

			<div class="dashboard-headingstyle" id="db-addressdetails-header">My Address</div>
<br>
			<div class="esq_separator">
				<!-- Placeholder -->
			</div>
			<div id="db-addressdetails" class="db-contactdetailscls">
				<div class="row-fluid">
					<div class="span4 offset2">
						<aui:input type="text" size="27" label="Street" name="street"
							value="<%=street%>" showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="maxLength">'40'</aui:validator>
						</aui:input>
					</div>
					<div class="span6">
						<aui:input type="text" size="27" label="City" name="city"
							value="<%=city%>" showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="alpha" />
							<aui:validator name="maxLength">'25'</aui:validator>
						</aui:input>

					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 offset2">
						<aui:input type="text" size="27" label="Zip Code" name="zip"
							value="<%=zip%>" showRequiredLabel="false">
							<aui:validator name="required" />
							<aui:validator name="digits" />
							<aui:validator name="maxLength">'6'</aui:validator>
						</aui:input>


					</div>
					<div class="span6" id="details">
						<aui:select label="Country" name='countryId' />

					</div>
				</div>
				<div class="row-fluid">
					<div class="span4 offset2">
						<aui:input type="text" size="27" label="Phone" name="phone"
							value="<%=phone%>" showRequiredLabel="false">
							<aui:validator name="digits" />
							<aui:validator name="rangeLength">[5,20]</aui:validator>
						</aui:input>
					</div>
					<div class="span6" id="details">
						<aui:select label="Region" name='regionId' />
					</div>
				</div>


			</div>

			<div id="db-password-maindiv">

				<liferay-ui:panel id="db-password-pannel" title="Change Password"
					collapsible="true" extended="false">
					<div id="db-password-seperator">
						<!-- Placeholder -->
					</div>
					<div id="db-password-change" class="db-contactdetailscls">
						<div class="row-fluid">
							<div class="span4 offset2">

								<aui:input size="27" autocomplete="off" label="New Password"
									name="password1" type="password" showRequiredLabel="false">
									<aui:validator name="rangeLength">[6,20]</aui:validator>
								</aui:input>
							</div>

							<div class="span6">
								<aui:input size="27" label="Confirm Password" name="password2"
									type="password" showRequiredLabel="false">
									<aui:validator name="equalTo">
							'#<portlet:namespace />password1'
						</aui:validator>
								</aui:input>
							</div>
						</div>
					</div>


				</liferay-ui:panel>
			</div>
			<br>
			<div class="dashboard-headingstyle" id="db-addimage-header">Add Image</div>
			<div class="esq_separator">
				<!-- Placeholder -->
			</div>
			<div id="db-addimage" class="db-contactdetailscls">
				<div class="row-fluid">
					<div class="span9 offset3 text-center">

						<aui:input inlineLabel="true" label="Upload your Profile Picture"
							name="myImageFile" type="file" showRequiredLabel="false">
							<aui:validator name="acceptFiles"
								errorMessage='Please select a image with a valid extension ({0}).'>
                   		'jpeg,png,bmp,gif,jpg'
 	             	</aui:validator>
						</aui:input>
					</div>
				</div>
				
				
				
			</div>
			<div class="row-fluid">
				<div class="span12 text-center">
					<aui:button type="submit" cssClass="couponadd" value="save" id="savebutton"/>
					
				</div>
			</div>





		</div>
	</aui:form>
</div>
<aui:script use="liferay-dynamic-select,aui-io-request-deprecated">
					new Liferay.DynamicSelect(
						[
							{
								select: '<portlet:namespace />countryId',
								selectData: Liferay.Address.getCountries,
								selectDesc: 'name',
								selectId: 'countryId',
								selectVal: '<%=countryId%>'
							},
							{
								select: '<portlet:namespace />regionId',
								selectData: Liferay.Address.getRegions,
								selectDesc: 'name',
								selectId: 'regionId',
								selectVal: '<%=regionId%>'
							}
						]
				);
</aui:script>











