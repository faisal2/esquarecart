
<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
	List<OrderRefundTracker> refundTrackerList=null;
	double walletAmount = EECUtil.myWalletAmount(themeDisplay
			.getUserId());
	
	String tabs=(String)request.getParameter("tabs1");
	if(Validator.isNull(tabs)){
		tabs=EECConstants.CLAIM_REQUEST;
	}
	
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("tabs1", tabs);
	
	PortletURL detailURL = renderResponse.createRenderURL();
	detailURL.setParameter("tabs", tabs);
	detailURL.setParameter("jspPage",
			"/html/eec/common/my_wallet/detail_wallet.jsp");
	String header=null;
%>

<c:choose>
	<c:when test='<%=tabs.equals(EECConstants.CLAIM_REQUEST) %>'>
		<portlet:actionURL var="actionURL"/>
			<aui:form name="fm" method="POST" action="<%=actionURL%>" >
				<aui:input type="hidden" name="<%=EECConstants.CMD %>" value="<%=EECConstants.ADD%>"/>
				<liferay-ui:error exception="<%= WalletLimitException.class %>" message="please-enter-a-valid-amount" />
				
				
		<aui:column columnWidth="50" first="true">
		<div id="walletuser">
			<aui:field-wrapper label="my-wallet">
				<div class="mywalletBaldiv">
					<liferay-ui:message key="balance-in-my-wallet" />&nbsp;-&nbsp;<%=currencyFormat.format(walletAmount)%>
				</div>
				
			</aui:field-wrapper>
			</div>	
		</aui:column>
		
		<aui:column columnWidth="50">
			<c:if test="<%=walletAmount > 0%>">
					<aui:input name="claimWalletAmount" type="text" value="" onkeyup="checkDec(this);" autocomplete="off" style="width:283px;">
						<aui:validator name="custom" errorMessage="please-enter-valid-amount">
					function (val, fieldNode, ruleValue) {
							return (val <= <%=walletAmount%> && val>0);
							}
					</aui:validator>
					</aui:input>
					
					<div class="dashboard-buttons">
					<aui:button type="submit" value="Claim Amount" id="wallet"/>
					</div><br/><br/><br/>
				</c:if>
		</aui:column>
			</aui:form>
	</c:when>
	<c:otherwise>
	
	<c:if test="<%=tabs.equals(EECConstants.WALLET_TRACKER) %>">
	<%
	refundTrackerList=OrderRefundTrackerLocalServiceUtil.findByUserId(themeDisplay.getCompanyId(),themeDisplay.getUserId(),StringPool.BLANK);
	PortletURL cartViewURL = PortletURLFactoryUtil.create(request,"", 0, PortletRequest.RENDER_PHASE);
	
	 header=UserViewUtil.getHeader(themeDisplay.getScopeGroupId(),PortletPropsValues.MY_WALLET_TRACKER, 0,cartViewURL,ctxPath, EECConstants.STYLE_DISPLAY_NONE);
	
	%>
	</c:if>
	<c:if test="<%=tabs.equals(EECConstants.WALLET_USED_AMOUNT) %>">
	<%
	refundTrackerList=OrderRefundTrackerLocalServiceUtil.findByUserId(themeDisplay.getCompanyId(),themeDisplay.getUserId(),EECConstants.WALLET);
	PortletURL cartViewURL = PortletURLFactoryUtil.create(request,"", 0, PortletRequest.RENDER_PHASE);
	
	 header=UserViewUtil.getHeader(themeDisplay.getScopeGroupId(),PortletPropsValues.WALLET_USED_AMOUNT, 0,cartViewURL,ctxPath, EECConstants.STYLE_DISPLAY_NONE);
	
	%>
	</c:if>
	 <div><%=header%></div>
	 
		 <liferay-ui:search-container delta="5"
			emptyResultsMessage="No Wallet records to display."
			iteratorURL="<%=portletURL%>">
			
			<liferay-ui:search-container-results results="<%=  ListUtil.subList(refundTrackerList,
				searchContainer.getStart(),
				searchContainer.getEnd())%>"
			total="<%=refundTrackerList.size()%>" />
			<liferay-ui:search-container-row modelVar="refundTracker" keyProperty="refundTrackerId"
			className="com.esquare.ecommerce.model.OrderRefundTracker">
			<%
			detailURL.setParameter("refundTrackerId",String.valueOf(refundTracker.getRefundTrackerId()));
			String name=StringPool.BLANK;
			Order order=OrderLocalServiceUtil.getLatestOrder(refundTracker.getUserId(), themeDisplay.getCompanyId()); 
			%>
			<c:if test="<%=order!=null%>">
			<%
			name=order.getBillingFirstName()+StringPool.SPACE+order.getBillingLastName();
			%>
			</c:if>
			<liferay-ui:search-container-column-text name="Customer Id"
				property="userId" href="<%=detailURL%>" />
			<liferay-ui:search-container-column-text name="Customer Name"
				value="<%=name%>" href="<%=detailURL%>"/>
				<%
				String amountClaimed=StringPool.BLANK;
				if(Validator.isNotNull(refundTracker.getAmountClaimed()))
					amountClaimed=dformat.format(refundTracker.getAmountClaimed());
				%>
			<liferay-ui:search-container-column-text name="Amount"
				value="<%=amountClaimed%>" href="<%=detailURL%>"/>
			
			<c:if test="<%=tabs.equals(EECConstants.WALLET_TRACKER) %>">
				<liferay-ui:search-container-column-text name="Reference Number"
					property="referenceNumber" href="<%=detailURL%>" />
					<%
					String expectedDeliveryDate=StringPool.BLANK;
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");	
					if(Validator.isNotNull(refundTracker.getExpectedDeliveryDate()))
						expectedDeliveryDate=sdf.format(refundTracker.getExpectedDeliveryDate());
					%>
				
				<liferay-ui:search-container-column-text name="Expected Delivery Date" 
				value="<%=expectedDeliveryDate %>" href="<%=detailURL%>"/>
				
				
				<%
				String status=PortletPropsValues.WALLET_PENDING_STATUS;
				%>
				<c:if test="<%=refundTracker.getPaymentStatus()%>">
				<%
				status=PortletPropsValues.WALLET_RECEIVED_STATUS;
				%>
				</c:if>
				<liferay-ui:search-container-column-text name="Status" 
				value="<%=status%>" href="<%=detailURL%>"/>
			</c:if>
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
function checkDec(el){
	  var txt = el.value;
	    if(txt.indexOf(".")>-1 && txt.split(".")[1].length>2)
	    {
	      var substr = txt.split(".")[1].substring(0,2);
	      el.value = txt.split(".")[0]+"."+substr;
	    }
	}
</script>

<style>
.tab {
    height: 38px;
}
</style>


