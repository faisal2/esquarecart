<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
String refundTrackerId=request.getParameter("refundTrackerId");
String tabs=(String)request.getParameter("tabs");
OrderRefundTracker refundTracker=null;

PortletURL cancelURL = renderResponse.createRenderURL();
cancelURL.setParameter("jspPage","/html/eec/common/my_wallet/view.jsp");
cancelURL.setParameter("tabs1", tabs);

PortletURL editURL = renderResponse.createActionURL();

String[] paymenttypes=PortletPropsValues.WALLET_PAYMENT_TYPE;
String expectedDeliveryDate = StringPool.BLANK;
%>
<c:if test="<%= refundTrackerId!=null%>">
<%
refundTracker=OrderRefundTrackerLocalServiceUtil.getOrderRefundTracker(Long.parseLong(refundTrackerId));
%>
</c:if>
<c:if test="<%= refundTracker!=null%>">
<%
String name=StringPool.BLANK;
String billingStreet=StringPool.BLANK;
String billingCity=StringPool.BLANK;
String billingState=StringPool.BLANK;
String billingCountry=StringPool.BLANK;
String billingPhone=StringPool.BLANK;
String billingZip=StringPool.BLANK;
Order order=OrderLocalServiceUtil.getLatestOrder(refundTracker.getUserId(), themeDisplay.getCompanyId());
SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");	
if(Validator.isNotNull(refundTracker.getExpectedDeliveryDate()))
	expectedDeliveryDate=sdf.format(refundTracker.getExpectedDeliveryDate());
%>
<div class="dashboard-maindiv2">
	
	
	<aui:form name="editfrm" method="post" action="<%=editURL.toString()%>">
		<aui:input type="hidden" name="<%=EECConstants.CMD %>" value="<%=EECConstants.EDIT%>"/>
		<aui:input type="hidden" name="refundTrackerId" value="<%=refundTracker.getRefundTrackerId()%>"/>
		<aui:input type="hidden" name="claimWalletAmount" value="<%=refundTracker.getAmountClaimed()%>"/>
		
		<c:if test="<%=order!=null%>">
		<%
		try {
			name=order.getBillingFirstName()+StringPool.SPACE+order.getBillingLastName();
			billingStreet=order.getBillingStreet();
			billingCity=order.getBillingCity();
			billingPhone=order.getBillingPhone();
			billingZip=order.getBillingZip();
			Country countryobj = CountryServiceUtil.fetchCountryByA2(order.getBillingCountry());
			billingCountry = countryobj.getName();
			Region regionobj = RegionServiceUtil.getRegion(Long
					.parseLong(order.getBillingState()));
			billingState = regionobj.getName();
		} catch (Exception e) {
		}
		%>
		</c:if>
		<br>
		<div class="row-fluid">
			<div class="span4 offset2">
					<div class="order_back">
				<div class="orderheading"> Wallet Tracker Details </div>
				<br>
		 		<span class="minalign">Customer Id: </span><span><%=refundTracker.getUserId()%></span>
		 		<br>
				<span class="minalign">Customer Name:</span><span><%=name%> </span>
				<br>
				<span class="minalign">Claim Wallet Amount:	</span>	<span><%=dformat.format(refundTracker.getAmountClaimed()) %></span>
				<br>
			 <span class="minalign">Payment Mode: </span><span><%=refundTracker.getPaymentMode() %> </span>
			 <br>
				 	<span class="minalign">Reference Number: </span><span><%=refundTracker.getReferenceNumber() %> </span>
				 	<br>
				<span class="minalign">Comments:</span><span><%=refundTracker.getComments() %> </span>
				<br>
					<span class="minalign">Expected Delivery Date: </span><span><%=expectedDeliveryDate%></span>
			</div>
	</div>
			<div class="span4 offset0">
				<div class="order_back">
				<div class="orderheading">Billing Address </div>
						<br>
						<span class="minalign"> Name:</span> <span><%=name%> </span>
						<br>
					<span class="minalign"> Address: </span><span> <%=billingStreet%></span>
					<br>
					<span class="minalign">		<%=billingCity%>,<%=billingState%> </span>
					<br>
						<span class="minalign"> Country:</span> <span><%=billingCountry %>-<%=billingZip%></span>
						<br>
					<span class="minalign"> Phone: </span><span><%=billingPhone%></span>
						</div>
				</div>
				</div>
				<div align="center">
		<input type="button" value="Back" onClick="location.href='<%=cancelURL.toString()%>';" id="detail_pro_added_to_wishlist" style="margin-top: 10px; margin-bottom: 10px;"/>
		</div>
	</aui:form>
</div>
</c:if>
<style>
.wallet_details_fields{
font-size: 12px;color:#012E69;
}


</style>