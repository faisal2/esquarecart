<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
String tabs1 = ParamUtil.getString(request, "tabs1");

PortletURL portletURL = renderResponse.createRenderURL();
portletURL.setParameter("tabs1", tabs1);
portletURL.setWindowState(LiferayWindowState.NORMAL);
portletURL.setParameter("jspPage","/html/eec/common/my_wallet/view.jsp");
String adminview_tabnames=EECConstants.CUSTOMER_CLAIM+StringPool.COMMA+
							EECConstants.PAID_LIST+StringPool.COMMA+
							EECConstants.WALLET_USED_AMOUNT;
String userview_tabnames=EECConstants.CLAIM_REQUEST+StringPool.COMMA+
						 	EECConstants.WALLET_TRACKER+StringPool.COMMA+
						 	EECConstants.WALLET_USED_AMOUNT;
%>
<div class="dashboard-maindiv2" id="liferaytabs">
<c:choose>
  <c:when test="<%=themeDisplay.getLayout().isPrivateLayout()%>">
  <div class="dashboard-pagemainheading70" id="wall">Wallet Tracker</div>
  <hr style="margin-top:10px;">
  <liferay-ui:tabs names="<%=adminview_tabnames%>" value="<%=tabs1%>" refresh="true"
	url="<%=portletURL.toString()%>" />
	<liferay-util:include page="/html/eec/common/my_wallet/admin_view_wallet.jsp"  servletContext="<%=this.getServletContext() %>" />
  </c:when>
  
  <c:otherwise>
  <liferay-ui:tabs names="<%=userview_tabnames%>" value="<%=tabs1%>" refresh="true"
	url="<%=portletURL.toString()%>" />
  	<liferay-util:include page="/html/eec/common/my_wallet/user_view_wallet.jsp"  servletContext="<%=this.getServletContext() %>" />
  </c:otherwise>
</c:choose>
</div>

<style>
#column-1
{
margin:1% 0%;
padding-left:1%;
padding-right:1%;
}
</style>