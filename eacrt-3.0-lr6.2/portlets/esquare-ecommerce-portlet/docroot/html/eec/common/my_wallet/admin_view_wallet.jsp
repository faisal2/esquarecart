<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
	List<OrderRefundTracker> refundTrackerList=null;
	PortletURL actionURL = renderResponse.createActionURL();
	
	String tabs=(String)request.getParameter("tabs1");
	
	Object ob = (String)request.getParameter("tabs1");
	String value =EECConstants.CUSTOMER_CLAIM;

	if (ob != null) {
	    value = (String) ob;
	    }
	System.out.println("ob>>>"+ob);
	
	
	
	if(Validator.isNull(tabs)){
		tabs=EECConstants.CUSTOMER_CLAIM;
	}
	
	PortletURL portletURL = renderResponse.createRenderURL();
	portletURL.setParameter("tabs1", tabs);
	
	String expectedDeliveryDate=StringPool.BLANK;
	boolean isPaid=false;
	boolean isWalletUsed=false;
	request.setAttribute("curStatus",value);
%>
	<c:if test="<%=Validator.isNotNull(tabs) && tabs.equals(EECConstants.CUSTOMER_CLAIM) %>">
	<%
	refundTrackerList=OrderRefundTrackerLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId(),Boolean.FALSE);
	%>
	</c:if>
	<c:if test='<%=Validator.isNotNull(tabs) && tabs.equals(EECConstants.PAID_LIST)%>'>
	<%
	refundTrackerList=OrderRefundTrackerLocalServiceUtil.findByPaymentMode(themeDisplay.getCompanyId(),StringPool.BLANK);
	isPaid=true;
	
	%>
	</c:if>
	<c:if test="<%=Validator.isNotNull(tabs) && tabs.equals(EECConstants.WALLET_USED_AMOUNT) %>">
	<%
	refundTrackerList=OrderRefundTrackerLocalServiceUtil.findByPaymentMode(themeDisplay.getCompanyId(),EECConstants.WALLET);
	isWalletUsed=true;
	%>
	</c:if>
	
<liferay-ui:error key="NoRecord"
	message="No Records Found For You Search" />

	<aui:form name="frm" method="POST" >
	
	   <liferay-ui:search-container delta="5"
			emptyResultsMessage="No Wallet records to display."
			iteratorURL="<%=portletURL%>">
			
			<liferay-ui:search-container-results results="<%=  ListUtil.subList(refundTrackerList,
				searchContainer.getStart(),
				searchContainer.getEnd())%>"
			total="<%=refundTrackerList.size()%>" />
			<liferay-ui:search-container-row modelVar="refundTracker" keyProperty="refundTrackerId"
			className="com.esquare.ecommerce.model.OrderRefundTracker">
			<%
			String name=StringPool.BLANK;
			Order order=OrderLocalServiceUtil.getLatestOrder(refundTracker.getUserId(), themeDisplay.getCompanyId()); 
			%>
			<c:if test="<%=order!=null%>">
				<%
				name=order.getBillingFirstName()+StringPool.SPACE+order.getBillingLastName();
				%>
			</c:if>
			<liferay-ui:search-container-column-text name="Customer Id"
				property="userId"  />
			<liferay-ui:search-container-column-text name="Cusomer Name"
				value="<%=name%>" />
			<%
				String amountClaimed=StringPool.BLANK;
				if(Validator.isNotNull(refundTracker.getAmountClaimed()))
					amountClaimed=dformat.format(refundTracker.getAmountClaimed());
			%>
			<liferay-ui:search-container-column-text name="Amount"
				value="<%=amountClaimed%>" />
			<c:if test="<%=!isWalletUsed%>">
					<liferay-ui:search-container-column-text name="Reference Number"
						property="referenceNumber"  />
			</c:if>
			<%
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");	
			if(Validator.isNotNull(refundTracker.getExpectedDeliveryDate()))
				expectedDeliveryDate=sdf.format(refundTracker.getExpectedDeliveryDate());
			%>
			<c:if test="<%=isPaid%>">
				<liferay-ui:search-container-column-text name="Expected Delivery Date" 
				value="<%=expectedDeliveryDate %>"/>
			</c:if>
			<c:if test="<%=!isWalletUsed%>">
				<liferay-ui:search-container-column-jsp 
						path="/html/eec/common/my_wallet/action.jsp" />
			</c:if>
		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
	</aui:form>

<%-- <aui:script>
Liferay.provide(
		window,
		'<portlet:namespace />deleteRefundTrcker',
		function() {
					document.<portlet:namespace />frm.action="<%= actionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
					var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />frm, "<portlet:namespace />allRowIds");
					if(checkBoxValue==""||checkBoxValue==null){
							alert("Please select atleast One entry to Delete");
							return false;
					}
					if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "Are you sure want to delete this selected Records?") %>')) {
					document.<portlet:namespace />frm.<portlet:namespace />deleteRefundTrackerIds.value=checkBoxValue;
					submitForm(document.<portlet:namespace />frm);
			}
		},
		['liferay-util-list-fields']
	);
	
	
</aui:script>

 --%>