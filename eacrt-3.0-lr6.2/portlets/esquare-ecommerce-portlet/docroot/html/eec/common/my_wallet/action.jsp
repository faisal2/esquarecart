<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
ResultRow row = (ResultRow) request
.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

OrderRefundTracker refundTracker =null;
String refundTrackerId=StringPool.BLANK;
String tabStatus = (String) request.getAttribute("curStatus");
boolean isPaid=false;
try{
	refundTracker=(OrderRefundTracker) row.getObject();
	refundTrackerId = String.valueOf(refundTracker.getRefundTrackerId());
	isPaid=refundTracker.getPaymentStatus();
}catch(ClassCastException classCastException){
}
PortletURL editURL = renderResponse.createRenderURL();
editURL.setParameter("refundTrackerId",refundTrackerId);
editURL.setParameter("tabStatus",tabStatus);

editURL.setParameter("jspPage",
		"/html/eec/common/my_wallet/edit_wallet.jsp");


		
%>
<liferay-ui:icon-menu>
<c:choose>
<c:when test="<%=isPaid %>">
<liferay-ui:icon image="edit" message="Edit"
		url="<%=editURL.toString()%>" />
</c:when>
<c:otherwise>
<liferay-ui:icon image="add" message="Pay"
		 url="<%=editURL.toString()%>" />
</c:otherwise>
</c:choose>
	<%-- <liferay-ui:icon image="delete" 
		 url="<%=deleteURL.toString()%>" /> --%>
</liferay-ui:icon-menu>		