<%@page import="java.util.Date"%>
<%@ include file="/html/eec/common/my_wallet/init.jsp"%>
<%
String refundTrackerId=request.getParameter("refundTrackerId");
OrderRefundTracker refundTracker=null;

PortletURL cancelURL = renderResponse.createRenderURL();
cancelURL.setParameter("jspPage","/html/eec/common/my_wallet/view.jsp");

String tabStatus=(String)request.getParameter("tabStatus");

PortletURL editURL = renderResponse.createActionURL();
editURL.setParameter("tabStatus",tabStatus);

String[] paymenttypes=PortletPropsValues.WALLET_PAYMENT_TYPE;
Calendar expectedDeliveryDate = Calendar.getInstance(); 
%>
<c:if test="<%= refundTrackerId!=null%>">
<%
refundTracker=OrderRefundTrackerLocalServiceUtil.getOrderRefundTracker(Long.parseLong(refundTrackerId));
%>
</c:if>
<c:if test="<%= refundTracker!=null%>">
<%
String paymentMode=refundTracker.getPaymentMode();
String name=StringPool.BLANK;
String billingStreet=StringPool.BLANK;
String billingCity=StringPool.BLANK;
String billingState=StringPool.BLANK;
String billingCountry=StringPool.BLANK;
String billingPhone=StringPool.BLANK;
String billingZip=StringPool.BLANK;
Order order=OrderLocalServiceUtil.getLatestOrder(refundTracker.getUserId(), themeDisplay.getCompanyId());
if(Validator.isNotNull(refundTracker.getExpectedDeliveryDate()))
expectedDeliveryDate=EECUtil.dateToCalendar(refundTracker.getExpectedDeliveryDate());
%>
<div class="dashboard-maindiv2">
	<div class="dashboard-pagemainheading70">Edit Wallet Tracker</div>
	
	<aui:form name="editfrm" method="post">
		<aui:input type="hidden" name="<%=EECConstants.CMD %>" value="<%=EECConstants.EDIT%>"/>
		<aui:input type="hidden" name="refundTrackerId" value="<%=refundTracker.getRefundTrackerId()%>"/>
		<aui:input type="hidden" name="claimWalletAmount" value="<%=refundTracker.getAmountClaimed()%>"/>
		
		<c:if test="<%=order!=null%>">
		<%
		try {
			name=order.getBillingFirstName()+StringPool.SPACE+order.getBillingLastName();
			billingStreet=order.getBillingStreet();
			billingCity=order.getBillingCity();
			billingPhone=order.getBillingPhone();
			billingZip=order.getBillingZip();
			Country countryobj = CountryServiceUtil.fetchCountryByA2(order.getBillingCountry());
			billingCountry = countryobj.getName();
			Region regionobj = RegionServiceUtil.getRegion(Long
					.parseLong(order.getBillingState()));
			billingState = regionobj.getName();
		} catch (Exception e) {
		}
		%>
		</c:if>
		
		
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:field-wrapper label="wallet-cutomer-id">
					<span class="walletmarginleft"><%=refundTracker.getUserId()%> </span>
				</aui:field-wrapper>
				<aui:field-wrapper label="wallet-cutomer-name">
					<span class="walletmarginleft"><%=name%></span>
				</aui:field-wrapper>
				<aui:field-wrapper label="claim-wallet-amount">
					<span class="walletmarginleft"><%=dformat.format(refundTracker.getAmountClaimed()) %> </span>
				</aui:field-wrapper>
				
				<aui:select name="claimMethod" label="request-to-refund" style="margin-left: 10px;">
				<%for(int i=0;i<paymenttypes.length;i++){ %>
				<aui:option selected="<%=paymentMode.equals(paymenttypes[i]) %>" value="<%=paymenttypes[i]%>"><%=paymenttypes[i]%></aui:option> 
				<%} %>
				
				</aui:select><br/>
				<aui:input name="referenceNumber" type="text" value="<%=refundTracker.getReferenceNumber() %>" label="Reference Number">
				<aui:validator name = "required"/>
				</aui:input>
				<aui:input name="comments" type="textarea" value="<%=refundTracker.getComments()%>" style="height:48px;width:201px;"/>
				<br/>
				<p class="wallet_details_fields">Expected Delivery Date:</p>
				<%-- <liferay-ui:input-date formName="dateV" yearValue="<%=expectedDeliveryDate.get(Calendar.YEAR)%>"
					monthValue="<%=expectedDeliveryDate.get(Calendar.MONTH)%>"
					dayValue="<%=expectedDeliveryDate.get(Calendar.DATE)%>" dayParam='<%="d"+refundTracker.getRefundTrackerId()%>' monthParam='<%="m"+refundTracker.getRefundTrackerId()%>'
					yearParam='<%="y"+refundTracker.getRefundTrackerId()%>' /> --%>
					<div class="row-fluid"><div class="span6"><aui:input name="dateV" label="" /></div><div class="span6"><aui:input name="timeV" label=""/></div></div>
					
					<br/><br/><br/>
				<div class="dashboard-buttons" style="padding-bottom: 8px;">
				<aui:button value = "save" id="detail_pro_added_to_wishlist" name ="save" />
				<input type="button" value="Cancel" onClick="location.href='<%=cancelURL.toString()%>';" id="detail_pro_added_to_wishlist" />
				</div>
			</aui:column>
			<br/>
			<aui:column columnWidth="50" >
				<div class="wallet_address_fields">Billing Address:</div>
				<%=billingStreet%><br/>
				<%=billingCity%><br/>
				<%=billingState%><br/>
				<%=billingCountry %>-<%=billingZip%><br/><br/>
				Phone:<%=billingPhone%><br/>
			</aui:column>
		</aui:layout>
	</aui:form>
</div>
</c:if>
<script>

AUI().use('aui-base','aui-io-request','aui-node', function(A){
	var ref = false;
	A.one("#<portlet:namespace/>referenceNumber").on('blur',function(){
		<portlet:namespace/>checkIsreferenceNumber();
	});
	
	
	var date = "";
	var newDate = new Date();
AUI().use('aui-datepicker','aui-timepicker', function(A) {
	new A.DatePicker({
	trigger : '#<portlet:namespace/>dateV',
	popover : {
	zIndex : 1
	},
	on: {
	    selectionChange: function(event) {
	   	  date = new Date(event.newSelection);
	    }
	}

	});
	
	
	new A.TimePicker(
		      {
		        trigger: '#<portlet:namespace/>timeV',
		        popover: {
		          zIndex: 1
		        }
		       
		      }
		    );
	
	
	});
		Liferay.provide(window, '<portlet:namespace/>checkIsreferenceNumber', function(){
		var referenceNum = parseInt(0);
		var referenceNumber=A.one("#<portlet:namespace/>referenceNumber").get("value");
		var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
		for (var i = 0; i < referenceNumber.length; i++) {
			 if (iChars.indexOf(referenceNumber.charAt(i)) != -1) {
				 referenceNum++
			 }
			 
		}
		if(referenceNum > 0){
			ref = false;
			alert('Should not alow Special Character !');
		}
		else{
			ref = true;
		}
		
	});
		
		
		A.one("#<portlet:namespace/>save").on('click',function(){
			<portlet:namespace/>checkIsreferenceNumber();
			if(ref == true){
				if(date >= newDate ){
				submitForm(document.<portlet:namespace />editfrm, "<%=editURL.toString()%>");
				}
				else{
					alert("Expected Delivery Date should be grater than or Equal to Current Date ")
					return false;
				}
			}
			
		});
});

</script>
