
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.esquare.ecommerce.service.CatalogLocalServiceUtil"%>
<link href="<%=request.getContextPath() %>/css/eec/common/catalog/catalog.css" rel="stylesheet" type="text/css" media="screen" />

<%
	Catalog catalog = (Catalog)request.getAttribute("CATALOG");
	long parentCatalogId = BeanParamUtil.getLong(catalog, request, "parentCatalogId", EECConstants.DEFAULT_PARENT_CATALOG_ID);
%>

<%
	String parentCatalogName = "";
	
	try {
		Catalog parentCatalog = CatalogLocalServiceUtil.getCatalog(parentCatalogId);
		parentCatalogName = parentCatalog.getName();
	}
	catch (Exception e) {
	}
%>
	<portlet:renderURL var="selectCatalogURL" windowState="<%= LiferayWindowState.POP_UP.toString() %>">
		<portlet:param name="jspPage" value="/html/eec/common/catalog/select_catalog.jsp" />
		<portlet:param name="catalogId" value="<%= String.valueOf(parentCatalogId) %>" />
	</portlet:renderURL>
<%
	String taglibOpenCatalogWindow = "var catalogWindow = window.open('" + selectCatalogURL + "', 'Catalog', 'directories=no,height=640,location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,width=680'); void('');catalogWindow.focus();";
%>	
	<aui:input name="parentCatalogId" type="hidden" value="<%= parentCatalogId %>" />
<%if(CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0)!=0){ %>	
	
	<div id="table1">
	<table><tr><td class="selectCatalogheader">
	<div id="catalog-selectparent" ><liferay-ui:message key="select-parent-catalog" />:</div></td>
	<td class="CatalogsOpenWindowSelectCatalogs"><aui:a href="#" id="parentCatalogName" label="<%= parentCatalogName %>" /></td>
	<td>
	<div id="selctparentCatalogdiv">
	<div id="dbcatalog-selectparentcatalogdiv">
	<button onClick="<%= taglibOpenCatalogWindow %>" class="ecartselectbutton">Select</button>
	
	<div id="dbcatalog-removeparentcatalogdiv" style="display:none;padding-left:146px;margin-top:-39px;">
	<button onClick='<%= renderResponse.getNamespace() + "removeCatalog();" %>' value="Remove" Class="ecartselectbutton">Remove</button>
	</div>
	</div></div></td></tr></table>
	<%}%>
	
	<%-- <%
	String star = " <font color='red' style='font-size: medium;'>*</font>";
	%> --%>
	
	
<div id="dbcatalog-fieldset">
	<aui:fieldset  style="border-width: 0px;">
	
		<div id="dbcatalog-addcatalogname">
		
			<aui:input  name="name" type="text" id="catalogName" maxlength="50" label='Name<span style="color:red;font-size: 14px;">*</span>'> 
				<%-- <div id="dbcatelog-star"><%=star%></div> --%>
				<aui:validator name="required"  errorMessage="Catalog name field is required"  />
			</aui:input>
			
			<div id="catalogNameValMsg" style="color:#b50303; font-family: Georgia;font-size: 14px;">
			</div> 
		</div>
		

	<div id="dbcatalog-addcatalogdesc">	<aui:input cssClass="lfr-textarea-container"  name="description"  type="textarea" /></div>
	<div id="dbcatalog-addcatalognav"><aui:input  type="checkbox" name="showNavigation" label="Show Navigation" /></div>
		
	</aui:fieldset>
	
	</div>
	</div>
	
	<aui:script use="aui-io-request-deprecated">
	
	Liferay.provide(
		window,
		'<portlet:namespace />selectCatalog',
		function(parentCatalogId, parentCatalogName) {
			var A = AUI();
			document.<portlet:namespace />fm.<portlet:namespace />parentCatalogId.value = parentCatalogId;
				
			var nameEl = document.getElementById("<portlet:namespace />parentCatalogName");
			nameEl.href = "<portlet:renderURL><portlet:param name="struts_action" value="/message_boards/view" /></portlet:renderURL>&<portlet:namespace />catalogId=" + parentCatalogId;
			nameEl.innerHTML = parentCatalogName + "&nbsp;";

			if (parentCatalogId != <%= EECConstants.DEFAULT_PARENT_CATALOG_ID %>) {
				var mergeWithParent = A.one('#<portlet:namespace />merge-with-parent-checkbox-div');

				if (mergeWithParent) {
					mergeWithParent.show();
				}
			}
			var parentName=document.getElementById("<portlet:namespace />parentCatalogName").innerHTML;
			//alert(parentName);
        	if(parentName!=''){
        		document.getElementById("dbcatalog-removeparentcatalogdiv").style.display='block';
        		document.getElementById('<portlet:namespace />catalogName').focus();
        	}else{
        		document.getElementById("dbcatalog-removeparentcatalogdiv").style.display='none';
        	}
		},
		['aui-base']
	);
	
	Liferay.provide(
		window,
		'<portlet:namespace />removeCatalog',
		function() {
			var A = AUI();

			document.<portlet:namespace />fm.<portlet:namespace />parentCatalogId.value = "<%= EECConstants.DEFAULT_PARENT_CATALOG_ID %>";

			var nameEl = document.getElementById("<portlet:namespace />parentCatalogName");

			nameEl.href = "";
			nameEl.innerHTML = "";

			var mergeWithParent = A.one('#<portlet:namespace />merge-with-parent-checkbox-div');
			var mergeWithParentCategory = A.one('#<portlet:namespace />mergeWithParentCategoryCheckbox');

			if (mergeWithParent) {
				mergeWithParent.hide();
			}

			if (mergeWithParentCategory) {
				mergeWithParentCategory.set('checked', false);
			}
			document.getElementById("dbcatalog-removeparentcatalogdiv").style.display='none';
			
		},
		['aui-base']
	);
	
	
        function addCatalogOnLoad() {
        	var parentCatalogName = document.getElementById("<portlet:namespace />parentCatalogName").innerHTML;
        	if(parentCatalogName==''){
        		document.getElementById("dbcatalog-removeparentcatalogdiv").style.display='none';
        	}else{
        		document.getElementById("dbcatalog-removeparentcatalogdiv").style.display='block';
        	}
			
        }
        window.onload = addCatalogOnLoad;
	
      
	Liferay.provide(
			window, '<portlet:namespace />saveCatalog',
			function() { 
		var companyId= document.getElementById('<portlet:namespace />companyId').value;
 		var parentCatalogId=document.<portlet:namespace />fm.<portlet:namespace />parentCatalogId.value;
		var editCatalogName = document.getElementById('<portlet:namespace />catalogName').value;
		var url = '<%=resouceURL.toString()%>';
		A.io.request(
			url,
			{
				//data to be sent to server
				data: {
					<portlet:namespace />catalogName: editCatalogName,
					<portlet:namespace />companyId: companyId,
					<portlet:namespace />parentCatalogId: parentCatalogId,
					},
					dataType: 'json',
						on: {
							failure: function() {
							},
							
							success: function() {
								var message = this.get('responseData').retVal;
								if (message) {
									$('#catalogNameValMsg').show();
									$('#catalogNameValMsg').html("Catalog Name is already Exist!");
								}else {
									$('#catalogNameValMsg').hide();
									submitForm(document.<portlet:namespace />fm);
								}
							}
							
						}
					}
					
				); //END of io Request
	});
	
      
		
 	
	
	</aui:script>