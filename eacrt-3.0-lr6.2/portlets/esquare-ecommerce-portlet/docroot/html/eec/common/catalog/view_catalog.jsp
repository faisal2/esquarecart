<%@ include file="/html/eec/common/catalog/init.jsp" %>
<link href="<%=request.getContextPath() %>/css/eec/common/catalog/catalog.css" rel="stylesheet" type="text/css" media="screen" />


<%
	Catalog curCatalog = (Catalog)request.getAttribute("CATALOG");
	long catalogId = BeanParamUtil.getLong(curCatalog, request, "catalogId", EECConstants.DEFAULT_PARENT_CATALOG_ID);
	int total = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
	List<Catalog> results = CatalogLocalServiceUtil.getCatalogs(themeDisplay.getCompanyId(), 0,0,total);
	
	int catalogSize = CatalogLocalServiceUtil.getCatalogsCount(themeDisplay.getCompanyId(), 0);
	
	PortletURL deleteURL = renderResponse.createActionURL();
	deleteURL.setParameter(Constants.CMD, Constants.DELETE);
	
	PortletURL editURL = renderResponse.createRenderURL();
	editURL.setParameter("cmd","customField");
	
	PortletURL addCatalogURL = renderResponse.createRenderURL();
	addCatalogURL.setParameter("cmd_add","ADD_CATALOG");
	addCatalogURL.setParameter("cmd","customField");
	
	
	 int totalNoCatalogs = 0;
	 CartPermission cartPermissionCatalog = CartPermissionLocalServiceUtil.getCartPermissionCI_PK(themeDisplay.getCompanyId(),EECConstants.CATALOG_COUNT);
	 if(cartPermissionCatalog!=null){
		 totalNoCatalogs = Integer.parseInt(cartPermissionCatalog.getValue()); 
	 }
%>

<div class="dashboard-maindiv2">
<!-- <div class="dashboard-pagemainheading70" id="dashboard-maindiv" >Catalogs</div> -->

<form name="frm" method="post">

<c:if test="<%=totalNoCatalogs > total || totalNoCatalogs==0%>">

	<div id="db-viewcatalogpage">
	<div class="dashboard-headingstyle">Add Catalog</div>	
	<hr style="margin-top: 13px;">
	<a href="<%=addCatalogURL.toString()%>" style="text-decoration: none;">
	<input type="button" name="<portlet:namespace />Add Catalog" value="Add Catalog" class="ecartaddbutton" />
	<%-- <img src='<%=request.getContextPath() %>/images/eec/common/catalog/add_catalog.png' 
	width='116px' height='32px' style="margin-left: -11px;" /> --%></a>
</div>
</c:if>


<div style="padding-right: 20px; padding-left: 25px;">
<div class="sidebarmenu">

<ul id="sitemap" style="margin-left: -10px;">
	<%
	 String subCategoryList = "";
	for (Catalog catalog : results) {
	%> 
	<li>
	<div class="row-fluid">
	<div class="span1">
	<img src="<%=ctxPath%>/images/eec/common/catalog/liback.png" style="padding-left: 12px;"/>
	</div>
	<div class="span6" style="margin-left: 0px;">
	<p class="catalogname"><%= catalog.getName() %></p>
	</div>
	
	<div onmouseover="showActionDiv('actions<%=catalog.getCatalogId()%>');"  onmouseout="hideActionDiv('actions<%=catalog.getCatalogId()%>');"  id="onmouseActions">
	<div class="span5">
	<a href="javascript:showDiv('rowId<%=catalog.getCatalogId()%>');"></a> 
					  <div id="actions<%=catalog.getCatalogId()%>" style="float: right;display: none;">
					  <a href='#' style="font-weight:normal;padding-left: 7px;" onclick="editCatalog(<%= String.valueOf(catalog.getCatalogId()) %>);"><img src="<%=ctxPath%>/images/eec/common/catalog/edit.png" title="Edit" /></a>
					   <a href="#" style="font-weight:normal;padding-left: 7px;" onclick="addCatalog(<%= String.valueOf(catalog.getCatalogId()) %>);"><img src="<%=ctxPath%>/images/eec/common/catalog/plus.png" width="16px" height="16px" title="Add" /></a>
					  <a href="#"  style="font-weight:normal;padding-left: 4px;"onclick="confirmDel(<%= String.valueOf(catalog.getCatalogId()) %>);" style="font-weight:normal;"><img src="<%=ctxPath%>/images/eec/common/catalog/delete.png" title="Delete" /></a></div>
	</div>
		</div>
		</div>
		
<div>
		 <% 
		  subCategoryList = EECCatalogUtil.getSubCatalogList(themeDisplay.getCompanyId(), catalog.getCatalogId(),editURL,addCatalogURL,deleteURL,ctxPath);
		 %>
</div>		
 		<div><%=subCategoryList %></div>
 		
	</li>
	<% } %>
</ul>
</div>


</div>
</form>
</div>
<script>
function confirmDel(param) {
	msg = "Are you sure to delete the selected Catalogue and its SubCatelogues";
			if(confirm(msg)) {
				document.frm.action="<%= deleteURL.toString()%>" + "&catalogId=" + param;
				
				document.frm.submit();
			}
}

function editCatalog(param) {
	document.frm.action="<%= editURL.toString()%>" + "&catalogId=" + param;
	document.frm.submit();
}
function addCatalog(param) {
	document.frm.action="<%= addCatalogURL.toString()%>" + "&parentCatalogId=" + param;
	document.frm.submit();
}

function showDiv(id) {
   var e = document.getElementById(id);
	if (e.style.display == 'none') {
	   e.style.display = 'block';
    }
    else if(e.style.display == 'block'){
       e.style.display = 'none';
    }
}
function showActionDiv(actionDivId){
	var e = document.getElementById(actionDivId);
	e.style.display = 'block';
   }
function hideActionDiv(actionDivId){
	var e = document.getElementById(actionDivId);
	e.style.display = 'none';
   }

</script>

