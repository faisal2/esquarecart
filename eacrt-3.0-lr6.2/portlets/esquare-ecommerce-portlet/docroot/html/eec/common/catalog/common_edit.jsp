<%@ include file="/html/eec/common/catalog/init.jsp" %>

<%
	String cmd_add=(String)request.getParameter("cmd_add");
	String catalogId=(String)request.getParameter("catalogId");
	String redirect = ParamUtil.getString(renderRequest, "redirect");

	PortletURL backURL = renderResponse.createRenderURL();
	backURL.setParameter("jspPage", "/html/eec/common/catalog/view_catalog.jsp");
%>
<portlet:resourceURL var="resouceURL" />
<liferay-portlet:actionURL 
	 var="configurationURL" />
<div class="dashboard-maindiv2"> 
<div class="dashboard-headingstyle" id="dashboard-maindiv" >Add Catalog</div>
<hr style="margin-top: 13px;">
<aui:form action="<%= configurationURL %>" method="post" name="fm" 
		onSubmit='<%= "event.preventDefault(); "+renderResponse.getNamespace() + "saveCatalog();" %>'>
	<aui:input name="<%= Constants.CMD %>" type="hidden"
		value="<%= Constants.UPDATE %>" />
	<aui:input name="redirect" type="hidden" value="<%= redirect %>" />
	<aui:input name="catalogId" type="hidden" value="<%= catalogId %>" />
	<aui:input type="hidden" name="companyId" id="companyId" value="<%=themeDisplay.getCompanyId()%>" />
	
    <div id="catalogportletdiv">
    
      <fieldset  style="border-width: 0px; padding-left: 20px;">
				<%-- <legend class="legend_Catalog">
					<h5 class="header_Catalog">
						<liferay-ui:message key="Catalog" />
					</h5>
				</legend> --%>

		<c:if test="<%= cmd_add != null %>">
		<%@ include file="/html/eec/common/catalog/add_catalog.jsp"%>
		</c:if>
		<c:if test="<%= cmd_add == null %>">
		<%@ include file="/html/eec/common/catalog/edit_catalog.jsp"%>
		</c:if>
		
		
		
	<aui:button-row>
	
			<aui:button type="submit" value="Save" cssClass="submitcoupon"/>
			<aui:button type="reset" value="Reset" cssClass="cancelcoupon1"/>
			
		<%-- <aui:button type="button" value="Cancel" onClick="location.href='<%=backURL.toString()%>';" /> --%>
			
	</aui:button-row>
	
		
		</fieldset>
	</div>
</aui:form>
</div>
<style>
#catalog-selectparent {
    color: #888;
    font-family: arial;
    font-size: 13px;
}
</style>