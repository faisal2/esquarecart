
<%@ include file="/html/eec/common/init.jsp" %>

<%
	PortletSession ps = renderRequest.getPortletSession();
	String webId = (String)ps.getAttribute("InstanceInfo",PortletSession.APPLICATION_SCOPE);
	ps.removeAttribute("InstanceInfo");
%>

<portlet:actionURL var="loginURL" name="loginURL"></portlet:actionURL>

	<portlet:renderURL var="instanceURL">
   		 <portlet:param name="instanceName" value="insname" />
    </portlet:renderURL>
	
	<aui:form action="<%= loginURL.toString() %>" name="fm" method="post">
		<br/>
		<div align="center" style="margin-top: 10%;">
				<img src="<%=ctxPath%>/images/eec/common/cart/gotomyshop.png"/>
			<div id="successimg"></div><br/>
			<aui:input name="webId" value="<%=webId%>" type="hidden" />
			
			<div id="GOTOMYPORTALButton"><aui:button type="submit" name="save" value="Go to my shop"/></div>
		</div>
		<br/>
	</aui:form>
	
	<style>
	#_InstanceListener_WAR_esquareecommerceportlet_save{
	background-image: linear-gradient(to bottom, #a5132a, #d52c41);
    border-radius: 3px !important;
    font-family: myfont;
    font-size: 16px !important;
    font-weight: bold;
    padding: 8px 25px;
    text-transform: uppercase;
    }
	.aui .btn:hover, .aui .btn:focus {
	background-position: 0 0px !important;
	}
	</style>