<%@ include file="/html/eec/common/cart/init.jsp" %>

<style type="text/css">
  
.field-label, .field-label-inline-label {
    font-size: 12px;
}
</style>


<%

String 	details = "";
PortletURL actionURL = renderResponse.createActionURL();
PortletURL renderURL = renderResponse.createRenderURL();

Cart cart = EECUtil.getCart(renderRequest);
Map productinventories = cart.getProductinventories();
Coupon coupon = cart.getCoupon();

long loginPlid=LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.LOGIN_PAGE_FRIENDLY_URL).getPlid();
PortletURL loginURL=EECUtil.getPortletURL(renderRequest, EECConstants.LOGIN_PORTLET_NAME, loginPlid, PortletRequest.RENDER_PHASE, null, null, LiferayWindowState.POP_UP, LiferayPortletMode.VIEW);
loginURL.setParameter("isPopupLogin", "1");

 if(Validator.isNotNull(cart)){
try{
	details = UserViewUtil.getCartTemplateContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),productinventories,renderURL,actionURL,request.getContextPath(),
			renderResponse.getNamespace(),themeDisplay.isSignedIn(),themeDisplay.getPortalURL(),renderRequest);
}
catch(Exception e){
} 
}
	double couponDiscount=EECUtil.calculateCouponDiscount(renderRequest,themeDisplay.getCompanyId(),themeDisplay.getUserId(),productinventories, coupon,null,null);
	double myWalletAmount=EECUtil.myWalletAmount(themeDisplay.getUserId());
	String couponError=(String)portletSession.getAttribute("couponError",PortletSession.APPLICATION_SCOPE);
	portletSession.removeAttribute("couponError", PortletSession.APPLICATION_SCOPE);
	
	Order order=OrderLocalServiceUtil.getLatestOrder(themeDisplay.getUserId(), themeDisplay.getCompanyId());
	double grandTotal = EECUtil.calculateTotal(
			themeDisplay.getCompanyId(),themeDisplay.getUserId(),
			cart.getProductinventories(), order.getShippingCountry(),
			order.getShippingState(), coupon, cart.getAltShipping(),
			cart.isInsure());
%>
<aui:form method="post" action="<%=actionURL%>" name="fm">
<liferay-ui:error exception="<%=WalletLimitException.class%>" message="please-enter-a-valid-wallet-value" />
<aui:input name="productInventoryIds" type="hidden" />
<aui:input name="productInventoryId" id="productInventoryId" type="hidden" />
<aui:input name="<%= EECConstants.CMD %>" type="hidden" value="<%= EECConstants.UPDATE %>" />
<aui:input name="<%= EECConstants.CMD1 %>" type="hidden" value="<%=EECConstants.CART_UPDATE%>" />
<%=details%>
<% if(productinventories.size()>0){ %>
<aui:fieldset>

		<%
		double subtotal = EECUtil.calculateSubtotal(productinventories);
		double actualSubtotal = EECUtil.calculateActualSubtotal(productinventories);
		double discountSubtotal = EECUtil.calculateDiscountSubtotal(productinventories);
		try {
			if(myWalletAmount<cart.getWalletAmount())
			cart.setWalletAmount(myWalletAmount);
			CartLocalServiceUtil.updateCart(cart);
		}
		catch (Exception e) {
		}
		%>
		
		<aui:field-wrapper  cssClass="ecart-headfontfam ecart-cart-wallet-baldiv">
		<c:if test='<%=myWalletAmount>0 || cart.getWalletAmount()>0%>' >
		<aui:field-wrapper label="balance-in-my-wallet" cssClass="ecart-headfontfam ecart-cart-wallet-bal" >
			<aui:field-wrapper cssClass="ecart-cart-wallet-bal-amt ecart-headfontfam"> <%=myWalletAmount%></aui:field-wrapper>
		</aui:field-wrapper>
		<div>
			
			<div><aui:input  name="walletAmount" label="" placeholder="Wallet Amount" inlineLabel="true" type="text"  value="<%=cart.getWalletAmount()%>" ></aui:input>
			</div>
		</div>
		
		</c:if>
		</aui:field-wrapper>
		<c:if test='<%=couponDiscount==0&&Validator.isNotNull(couponError)&&cart.getCouponCodes()!=""%>'>
			<aui:field-wrapper label="">
			<div class="portlet-msg-info">
					<%=couponError%>
				</div>
			</aui:field-wrapper>
		</c:if>
		
<%-- 	<aui:field-wrapper label="subtotal" > --%>
<%-- 			<c:if test="<%= subtotal == actualSubtotal %>"> --%>
<%-- 				<%= currencyFormat.format(subtotal) %> --%>
<%-- 			</c:if> --%>

<%-- 			<c:if test="<%= subtotal != actualSubtotal %>"> --%>
<%-- 				<strike><%= currencyFormat.format(subtotal) %></strike>  --%>
<%-- 				<div class="portlet-msg-success"><%= currencyFormat.format(actualSubtotal) %></div> --%>
<%-- 			</c:if> --%>
<%-- 		</aui:field-wrapper> --%>

<%-- 		<c:if test="<%= subtotal != actualSubtotal %>"> --%>
<%-- 			<aui:field-wrapper label="you-save"> --%>
<!-- 				<div class="esquarecart-portlet-msg-error"> -->
<%-- 					<%= currencyFormat.format(discountSubtotal) %> (<%= percentFormat.format(EECUtil.calculateDiscountPercent(productinventories)) %>) --%>
<!-- 				</div> -->
<%-- 			</aui:field-wrapper> --%>
<%-- 		</c:if> --%>
		<c:if test="<%= coupon != null && couponDiscount!=0%>">
			<aui:field-wrapper label="coupon-discount">
				<div class="esquarecart-portlet-msg-error">
					<%=couponDiscount%>
				</div>
			</aui:field-wrapper>
		</c:if>
		<c:if test="<%= coupon != null&&(coupon.getDiscountType().equals(EECConstants.DISCOUNT_TYPE_FREE_SHIPPING)||coupon.getDiscountType().equals(EECConstants.DISCOUNT_TYPE_TAX_FREE))%>">
			<aui:field-wrapper label="coupon-discount">
			<div class="portlet-msg-info">
					<%=coupon.getDiscountType()%>
				</div>
			</aui:field-wrapper>
		</c:if>
		
	
		
		
		
</aui:fieldset>
<%
} %>
</aui:form>

<style type="text/css">
.ecart-cart-wallet-baldiv{
background: #fffff;

}
.ecart-cart-wallet-bal{
background: #fffff;
 }




 .field-label, .field-label-inline-label, .choice-label{
 color: black;
 }
 
 .field-label, .field-label-inline-label {
    font-size: 15px;
}

</style>

<aui:script>
function <portlet:namespace />emptyCart() {
	msg = "Are you sure to delete this Cart Products?";
			if(confirm(msg)) {
				document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD %>.value = "<%=EECConstants.UPDATE%>";
				document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD1 %>.value = "<%= EECConstants.CART_UPDATE %>"; 
				document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = "";
				document.<portlet:namespace />fm.couponCodes.value = "";
				submitForm(document.<portlet:namespace />fm);
			}
	}
	
	function changeQuantity(productInventoryId,i,quantity,outOfStockPurchase){
		var	itemCount=0;
		document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD %>.value = "<%=EECConstants.UPDATE%>";
		document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD1 %>.value = "<%= EECConstants.CART_UPDATE %>"; 
		if(document.getElementById(productInventoryId).innerHTML=='Save'){
	 	document.getElementById('input_'+productInventoryId).readOnly = true;
		var y=document.getElementById('input_'+productInventoryId).value;
		var numbers = /^[0-9]+$/;  
		if(y==0 || !(y.match(numbers))){
			alert("Invalid Quantity");
			document.getElementById('input_'+productInventoryId).readOnly = false;
			document.getElementById('input_'+productInventoryId).focus();
			return;
		}else if(parseInt(y)>parseInt(quantity) && !outOfStockPurchase ){
			if(quantity <= 0 ){
			alert("This Item is not available");
			<portlet:namespace />updateQuantity();
			return;
			}
			if(confirm("Only "+quantity+" Items are available \n Do you wish to add availabe item in to the cart")){
			document.getElementById('input_'+productInventoryId).value = quantity;
			<portlet:namespace />updateQuantity();
			}
			else{
			document.getElementById('input_'+productInventoryId).readOnly = false;
			document.getElementById('input_'+productInventoryId).focus();
			return;
			}
		}
		else if(parseInt(y) <= parseInt(quantity) || outOfStockPurchase ){
		document.getElementById('input_'+productInventoryId).value = y;
		<portlet:namespace />updateQuantity();
		}
		document.getElementById(productInventoryId).innerHTML='Change';
	}
			else{
				document.getElementById('input_'+productInventoryId).readOnly = false;
				document.getElementById(productInventoryId).innerHTML='Save';
			}
	 }
       
       
	function <portlet:namespace />updateQuantity() {
		var productInventoryIds = <portlet:namespace />getproductInventoryIds();
		document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = productInventoryIds;
			submitForm(document.<portlet:namespace />fm);
	}
  
	function <portlet:namespace />deleteSelectedCart(id) {
		var productInventoryIds = "";
		var count = 0;
		var itemCount=<%=productinventories.size() %>
		<%
		Iterator iterat = productinventories.entrySet().iterator();
	
		for (int i = 0; iterat.hasNext(); i++) {
			Map.Entry entry = (Map.Entry)iterat.next();
	
			CartItem cartItem = (CartItem)entry.getKey();
	
			ProductInventory productInventory = cartItem.getProductInventory();
		%>
	
			count = document.<portlet:namespace />fm.productInventory_<%= productInventory.getInventoryId() %>_<%= i %>_count.value;
			for (var i = 0; i < count; i++) {
					if('<%= productInventory.getInventoryId() %>' != id) productInventoryIds += "<%= productInventory.getInventoryId()%>,";
				}
				count = 0;
			<%
			}
			%>
	       var msg= "Do you really want to delete this Product?";
	       if (confirm(msg)){
	      		document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = productInventoryIds;
	      		if(itemCount <=1){
	      		document.<portlet:namespace />fm.couponCodes.value = "";
	      		}
				submitForm(document.<portlet:namespace />fm);
			}
	 
	}
 
	function <portlet:namespace />checkOut(){
		if (!themeDisplay.isSignedIn()) {
	
					Liferay.Util.openWindow(
						{
							dialog: {
								centered: true,
								modal: true
							},
							id: 'signInDialog',
							title: Liferay.Language.get('sign-in'),
							uri: '<%= loginURL.toString() %>'
						}
					);
		}
		else
		{
					document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD1 %>.value = "<%= EECConstants.CHECKOUT %>"; 
					document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD %>.value = "<%=StringPool.BLANK%>"
					<portlet:namespace />updateCart();
		}
	} 
	
	if (window.opener) {
	window.close();
	window.opener.parent.location.href = "<%= HtmlUtil.escapeJS(PortalUtil.getPortalURL(renderRequest) + themeDisplay.getURLSignIn()) %>";
	}
		
	function <portlet:namespace />deleteCoupon() {
	var couponValue=document.getElementById('couponCodes').value;
	if(<%=Validator.isNull(cart.getCouponCodes())%> && couponValue!=""){
	document.getElementById('couponCodes').value="";
	return false;
	}
	else if(<%=Validator.isNull(cart.getCouponCodes())%>)
	{
	return false;
	}
	var productInventoryIds = <portlet:namespace />getproductInventoryIds();
	msg = "Are you sure to remove this Coupon?";
			if(confirm(msg)) {
				document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = productInventoryIds;
				document.<portlet:namespace />fm.couponCodes.value = "";
				submitForm(document.<portlet:namespace />fm);
			}
	}
	function <portlet:namespace />getproductInventoryIds(){
	var productInventoryIds = "";
	<%
		Iterator itr = productinventories.entrySet().iterator();

		for (int i = 0; itr.hasNext(); i++) {
			Map.Entry entry = (Map.Entry)itr.next();

			CartItem cartItem = (CartItem)entry.getKey();

			ProductInventory productInventory = cartItem.getProductInventory();
		%>	
			count = document.<portlet:namespace />fm.productInventory_<%= productInventory.getInventoryId() %>_<%= i %>_count.value;
			var y=document.getElementById('input_'+<%= productInventory.getInventoryId() %>).value;
			if (<%=productInventory.getQuantity()%><= 0 && !<%=productInventory.isOutOfStockPurchase() %> ){
			count=0;
			}
			else if(count > <%=productInventory.getQuantity() %> && !<%=productInventory.isOutOfStockPurchase() %>){
			count=<%=entry.getValue()%>;
			}
			for (var i = 0; i < count; i++) {
				productInventoryIds += "<%= cartItem.getCartItemId() %>,";
			}
			count = 0;

		<%
		}
		%>
	return productInventoryIds;
	} 
	
	function <portlet:namespace />updateCoupon() {
	var couponValue=document.getElementById('couponCodes').value;
	if( couponValue==""|| couponValue==null){
	return false;
	}
	document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD %>.value = "<%=EECConstants.UPDATE%>";
	document.<portlet:namespace />fm.<portlet:namespace /><%= EECConstants.CMD1 %>.value = "<%= EECConstants.CART_UPDATE %>"; 
	<portlet:namespace />updateCart();
	}
	
	function <portlet:namespace />updateCart() {
		var productInventoryIds = <portlet:namespace />getproductInventoryIds();
		var isChange =true;
		document.<portlet:namespace />fm.<portlet:namespace />productInventoryIds.value = productInventoryIds;
		if(productInventoryIds==0){
			alert("Please finalize the Quantity of Products" );
		}
		if(productInventoryIds!=0){
		var itemCount=<%=productinventories.size() %>
		<%
		StringBuffer sb=new StringBuffer();
		ProductDetails productDetails=null;
		Iterator itre = productinventories.entrySet().iterator();
	    boolean inStock=false;
	    boolean inStock2=true;
		for (int i = 0; itre.hasNext(); i++) {
			Map.Entry entry = (Map.Entry)itre.next();
	
			CartItem cartItem = (CartItem)entry.getKey();
			Integer count = (Integer) entry.getValue();
			ProductInventory productInventory = cartItem.getProductInventory();
			inStock=EECUtil.isInStock(productInventory,count);
			if(!inStock){
				productDetails=ProductDetailsLocalServiceUtil.getProductDetails(productInventory.getProductDetailsId());
				if(productInventory.getQuantity()>0){
				sb.append(productDetails.getName());
				sb.append(" has ");
				sb.append(productInventory.getQuantity());
				sb.append(" Quantity Only");
				}
				else {
					sb.append("Please delete ");
					sb.append(productDetails.getName());
					sb.append(" from cart");
				}
				sb.append(StringPool.NEW_LINE);
			}
			inStock2=inStock&&inStock2;
		%>
			if(document.getElementById('<%= productInventory.getInventoryId() %>').innerHTML=='Save'){
			isChange=false;
			}
			if(<%=!inStock%>){
			            document.getElementById('input_'+<%=productInventory.getInventoryId()%>).readOnly = false;
						document.getElementById('input_'+<%=productInventory.getInventoryId()%>).focus();
						document.getElementById(<%=productInventory.getInventoryId()%>).innerHTML='Save';
			}
			<%
			}
			%>
			if(isChange&&<%=inStock2%>){
			 var submit=<portlet:namespace />validateWalletAmount();
			if(submit)submitForm(document.<portlet:namespace />fm);
			}else{
			if(!isChange){
			alert("Click on Save Link then Checkout");
			return;
			}
			alert('<%=HtmlUtil.escapeJS(sb.toString())%>');
			}
		}
	}
	
	function <portlet:namespace />validateWalletAmount(){
	if(<%=myWalletAmount <= 0%>){
	return true;
	}
	var useWalletAmount=document.getElementById('<portlet:namespace />walletAmount').value;
	var numbers = /^[0-9].+$/;  
		if( !(useWalletAmount.match(numbers))){
		if(useWalletAmount != 0){
			alert("Invalid amount");
			document.getElementById('<portlet:namespace />walletAmount').focus();
			return false;
			}
	}
	if(<%=myWalletAmount>0%>&&<%=myWalletAmount>grandTotal%> && useWalletAmount><%=grandTotal%>){
		if(confirm("Requested wallet amount is greater than Grand Total \n Do you wish to use required amount ")) {
			document.getElementById('<portlet:namespace />walletAmount').value=<%=grandTotal%>;
			return true;
		}
		else
		{
			document.getElementById('<portlet:namespace />walletAmount').focus();
			return false;
		}
		}
	 else if(useWalletAmount><%=myWalletAmount%>){
	if(confirm("Insufficient amount in wallet. \n Do you wish to use available amount ")) {
			document.getElementById('<portlet:namespace />walletAmount').value=<%=myWalletAmount%>;
			return true;
		}
		else
		{
			document.getElementById('<portlet:namespace />walletAmount').focus();
			return false;
		}
	}
	else{
	return true;
	}
	}
		
</aui:script>
