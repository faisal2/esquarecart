<%@page import="com.liferay.portal.service.CountryServiceUtil"%>
<%@page import="com.liferay.portal.service.RegionServiceUtil"%>
<%@ include file="/html/eec/common/cart/init.jsp" %>


<style type="text/css">


.lfr-search-container {
 font-size: 13px;
}

.lfr-table {
    font-size: 12px;
    
}
#detail_pro_added_to_wishlist
{
width:150px !important;
}

/* #tabtable1 {
    background: none repeat scroll 0 0 #252525;
    height: 35px;
    width: 100%;
    border: 1px solid #B5C9DA;
}
 */
#conf_tab1{
}
#conf_maintab1{
}
#conf_tab2{
}
#conf_maintab2{
}
#conf_tab3{
}
#conf_maintab3{
}
/*#order-tab1{
color: #FFFFFF;
text-decoration: none;
font-size: 16px;
 font-weight: bold;
}
 #tab2{
color: #EC4D25;
text-decoration: none;
font-size: 16px;
 font-weight: bold;
} 
#order-tab3{
color: #ffffff;
text-decoration: none;
font-size: 16px;
 font-weight: bold;
}
#maintab1{
text-align: center;
}
#maintab2{
text-align: center;
background:#ffffff;
}
#maintab3{
text-align: center;
}

*/

    
  .lfr-table tr th {
    font-size: 15px;
}

#tablemainconform1  tr td:first-child
{
    font-weight: bold;
}

#tablemainconform2  tr td:first-child
{
    font-weight: bold;
}
/* #tablebottomconform tr td:first-child{
     font-weight: bold;

} */
#tablemainconform {
    margin-bottom: 10px;
    margin-left: 38px;
    width: 650px;
}


/* 
.addtitle{
   color: #EC4D25;
    font-size: 13px;
    padding-left: 10px;
    } */
/*  #tablebottomconform {
    background: none repeat scroll 0 0 #F7CDBE;
    border: 6px double #FFFFFF;
    height: 100px;
    margin-left: 33px;
    width: 220px;
}  */
</style>

<%
Cart cart = EECUtil.getCart(renderRequest);

Map productInventories = cart.getProductinventories();

Coupon coupon = cart.getCoupon();

int altShipping = cart.getAltShipping();
String altShippingName =null;
// altShippingName=shoppingPrefs.getAlternativeShippingName(altShipping);
Order order = OrderLocalServiceUtil.getLatestOrder(themeDisplay.getUserId(),themeDisplay.getCompanyId());
%>

<portlet:actionURL var="checkoutSecondURL">
	<portlet:param name="<%= EECConstants.CMD1 %>" value="<%= EECConstants.CHECKOUT %>" />
</portlet:actionURL>
<div class="orderconfirm">
<aui:form action="<%= checkoutSecondURL %>" method="post" name="fm" >
<div id="conformtopdiv" class="ecart-normfontfam">
	<aui:input name="<%= EECConstants.CMD %>" type="hidden" value="<%= EECConstants.PAYMENT_TYPE %>" />
	<aui:input name="billingFirstName" type="hidden" value="<%= order.getBillingFirstName() %>" />
	<aui:input name="billingLastName" type="hidden" value="<%= order.getBillingLastName() %>" />
	<aui:input name="billingEmailAddress" type="hidden" value="<%= order.getBillingEmailAddress() %>" />
	<aui:input name="billingCompany" type="hidden" value="<%= order.getBillingCompany() %>" />
	<aui:input name="billingStreet" type="hidden" value="<%= order.getBillingStreet() %>" />
	<aui:input name="billingCity" type="hidden" value="<%= order.getBillingCity() %>" />
	<aui:input name="billingState" type="hidden" value="<%= order.getBillingState() %>" />
	<aui:input name="billingZip" type="hidden" value="<%= order.getBillingZip() %>" />
	<aui:input name="billingCountry" type="hidden" value="<%= order.getBillingCountry() %>" />
	<aui:input name="billingPhone" type="hidden" value="<%= order.getBillingPhone() %>" />
	<aui:input name="shipToBilling" type="hidden" value="<%= order.isShipToBilling() %>" />
	<aui:input name="shippingFirstName" type="hidden" value="<%= order.getShippingFirstName() %>" />
	<aui:input name="shippingLastName" type="hidden" value="<%= order.getShippingLastName() %>" />
	<aui:input name="shippingEmailAddress" type="hidden" value="<%= order.getShippingEmailAddress() %>" />
	<aui:input name="shippingCompany" type="hidden" value="<%= order.getShippingCompany() %>" />
	<aui:input name="shippingStreet" type="hidden" value="<%= order.getShippingStreet() %>" />
	<aui:input name="shippingCity" type="hidden" value="<%= order.getShippingCity() %>" />
	<aui:input name="shippingState" type="hidden" value="<%= order.getShippingState() %>" />
	<aui:input name="shippingZip" type="hidden" value="<%= order.getShippingZip() %>" />
	<aui:input name="shippingCountry" type="hidden" value="<%= order.getShippingCountry() %>" />
	<aui:input name="shippingPhone" type="hidden" value="<%= order.getShippingPhone() %>" />

 		  	<table id="tabtable1">
			<tr>
				<td id="conf_maintab1"><a href="" onclick="javascript:history.go(-1)" id="conf_tab1"></a></td>
				<td id="conf_maintab2"><a href="#tabs-2"  id="conf_tab2"></a></td>
				<td id="conf_maintab3"><a href="#tabs-3" id="conf_tab3"></a></td>
			</tr>
		   </table>
		   
		   
<%-- 	<table class="lfr-table" id="tablemainconform">
	<tr>
		<td id="eecart-orderreviewbilling">
			<span class="ecart-headfontfam"><liferay-ui:message key="billing-address" /></span>

			<br /><br />

			<table class="lfr-table" id="tablemainconform1">
			<tr>
				<td>
					<liferay-ui:message key="first-name" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingFirstName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="last-name" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingLastName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="email-address" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingEmailAddress()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="company" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingCompany()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="street" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingStreet()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="city" />
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingCity()) %>:
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="state" />:
				</td>
				<td><%Region billState=RegionServiceUtil.getRegion(Long.parseLong(order.getBillingState())); %>
					<%= HtmlUtil.escape(billState.getName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="postal-code" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingZip()) %>
				</td>
			</tr>
			<tr>
				<td>
				<liferay-ui:message key="country" />:
				</td>
				
				<td><% Country billCountry=CountryServiceUtil.getCountryByA2(order.getBillingCountry());%>
					<%= HtmlUtil.escape(billCountry.getName()) %>
				</td>
				
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="phone" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getBillingPhone()) %>
				</td>
			</tr>
			</table>
		</td>
		<td class="lfr-top" id="eecart-orderreviewshippinf">
			<span class="ecart-headfontfam" ><liferay-ui:message key="shipping-address" /></span>

			<br /><br />

			<table class="lfr-table" id="tablemainconform2">
			<tr>
				<td>
					<liferay-ui:message key="first-name" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingFirstName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="last-name" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingLastName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="email-address" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingEmailAddress()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="company" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingCompany()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="street" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingStreet()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="city" />
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingCity()) %>:
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="state" />:
				</td>
				
				<td><%Region shippingState=RegionServiceUtil.getRegion(Long.parseLong(order.getShippingState())); %>
					<%= HtmlUtil.escape(shippingState.getName()) %>
				</td>
				
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="postal-code" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingZip()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="country" />:
				</td>
				<td><% Country shippingCountry=CountryServiceUtil.getCountryByA2(order.getShippingCountry());%>
					<%= HtmlUtil.escape(shippingCountry.getName()) %>
				</td>
			</tr>
			<tr>
				<td>
					<liferay-ui:message key="phone" />:
				</td>
				<td>
					<%= HtmlUtil.escape(order.getShippingPhone()) %>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table> --%>

<div>
	<span >  . </span>
</div>

	<%
	boolean showAvailability = PrefsPropsUtil.getBoolean(company.getCompanyId(), PropsKeys.SHOPPING_ITEM_SHOW_AVAILABILITY);

	StringBundler productInventoryIds = new StringBundler();

	SearchContainer searchContainer = new SearchContainer();

	List<String> headerNames = new ArrayList<String>();

	headerNames.add("sku");
	headerNames.add("product-name");

	if (showAvailability) {
		headerNames.add("availability");
	}

	headerNames.add("quantity");
	headerNames.add("price");
	headerNames.add("total");

	searchContainer.setHeaderNames(headerNames);
	searchContainer.setHover(false);

	Set results = productInventories.entrySet();
	int total = productInventories.size();
	searchContainer.setTotal(total);

	List resultRows = searchContainer.getResultRows();

	Iterator itr = results.iterator();

	for (int i = 0; itr.hasNext(); i++) {
		Map.Entry entry = (Map.Entry)itr.next();

		CartItem cartItem = (CartItem)entry.getKey();
		Integer count = (Integer)entry.getValue();

		ProductInventory productInventory = cartItem.getProductInventory();
		ProductDetails productDetails=ProductDetailsLocalServiceUtil.fetchProductDetails(productInventory.getProductDetailsId());
		int descriptionLength=productDetails.getDescription().length();
		String name=productDetails.getName();
		
		for (int j = 0; j < count.intValue(); j++) {
			productInventoryIds.append(cartItem.getCartItemId());
			productInventoryIds.append(",");
		}

		ResultRow row = new ResultRow(productInventory, productInventory.getInventoryId(), i);


		row.addText(productInventory.getSku());

		// Description

			row.addText(HtmlUtil.escape(name));

		// Availability

		if (EECUtil.isInStock(productInventory,count)) {
			row.addText(PortletPropsValues.PRODUCT_IN_STOCK);
		}
		else {
			row.addText(PortletPropsValues.PRODUCT_OUT_OF_STOCK);
		}

		// Quantity

		row.addText(count.toString());

		// Price

		row.addText(currencyFormat.format(EECUtil.calculateActualPrice(productInventory, count.intValue()) / count.intValue()));

		// Total

		row.addText(currencyFormat.format(EECUtil.calculateActualPrice(productInventory, count.intValue())));

		// Add result row

		resultRows.add(row);
	}
	%>

	<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />

	<aui:input name="productInventoryIds" type="hidden" value="<%= productInventoryIds %>" />
	<aui:input name="couponCodes" type="hidden" value="<%= cart.getCouponCodes() %>" />

<div id="eec-confrmbut" align="center">
		<input type="submit" id="detail_pro_added_to_wishlist" class="saveandcontinue" value="Save and Continue" />
	
</span>
</div>
	
	</div>
</aui:form>

</div>

<style>

 .align-left.col-2.col-description.valign-middle {
    text-align: left;
}

.align-left.col-5.col-price.valign-middle {
    text-align: left;
}

.align-left.col-6.col-total.last.valign-middle {
    text-align: left;
}
#_cart_WAR_esquareecommerceportlet_fm{
	padding-left: 22px;
}
</style>