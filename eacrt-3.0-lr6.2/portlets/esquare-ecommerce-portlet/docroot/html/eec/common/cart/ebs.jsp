<%@ include file="/html/eec/common/cart/init.jsp"%>


<portlet:actionURL var="returnURLEBS">
<portlet:param name="<%= EECConstants.CMD %>" value="<%= EECConstants.EBS_RESPONSE %>" />
</portlet:actionURL>
<%
	String returnURL = returnURLEBS.toString() + "&DR={DR}";
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_id");
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_count");
	returnURL = HttpUtil.removeParameter(returnURL, "p_p_col_pos");
	
	Cart cart = EECUtil.getCart(renderRequest);

	Map productInventories = cart.getProductinventories();

	Coupon coupon = cart.getCoupon();

	int altShipping = cart.getAltShipping();
	Order order = OrderLocalServiceUtil.getLatestOrder(
			themeDisplay.getUserId(), themeDisplay.getCompanyId());

	String md5HashData = EBSUtil.getEbsSecretKey(themeDisplay.getCompanyId());
	String paymentMode = PortletPropsValues.EBS_PAYMENT_MODE;
	String accountNumber = EBSUtil.getEbsMerchantId(themeDisplay.getCompanyId());
	String amount = currencyFormat.format(EECUtil.calculateTotal(
			themeDisplay.getCompanyId(),themeDisplay.getUserId(),productInventories,
			order.getShippingCountry(), order.getBillingState(),
			coupon, altShipping, cart.isInsure())-cart.getWalletAmount());
	String address = order.getBillingStreet() + ","
			+ order.getBillingCompany();
	amount = amount.substring(3, amount.length()).replace(",", "");
	md5HashData += '|' + accountNumber + '|' + amount + '|'
			+ order.getOrderId() + '|' + returnURL + '|' + paymentMode;
%>

<form method="post"	name="frmTransaction" id="frmTransaction">
	
	<input name="account_id" type="hidden" value="<%=accountNumber%>">
	<input name="return_url" type="hidden" size="60" value="<%=returnURL%>">
	<input name="mode" type="hidden" size="60" value="<%=paymentMode%>">
	<input name="reference_no" type="hidden" value="<%=order.getOrderId()%>"> 
	<input name="amount" type="hidden" value="<%=amount%>" /> 
	<input name="description" type="hidden" value="<%=order.getOrderId()%>"> 
	<input name="name" type="hidden" maxlength="255" value="<%=order.getBillingFirstName() + StringPool.SPACE+ order.getBillingLastName()%>">
	<input name="address" type="hidden" maxlength="255"	value="<%=address%>"> 
	<input name="city" type="hidden" maxlength="255" value="<%=order.getBillingCity()%>">
	<input name="state" type="hidden" maxlength="255" value="<%=order.getBillingState()%>"> 
	<input name="postal_code" type="hidden" maxlength="255" value="<%=order.getBillingZip()%>">
	<input name="country"	type="hidden" maxlength="255" value="<%=order.getBillingCountry()%>">
	<input name="phone" type="hidden" maxlength="255" value="<%=order.getBillingPhone()%>"> 
	<input name="email"	type="hidden" size="60" value="<%=order.getBillingEmailAddress()%>">
	<input name="ship_name" type="hidden" maxlength="255" value="<%=order.getShippingFirstName() + StringPool.SPACE+ order.getShippingLastName()%>" />
	<input name="shippingEmailAddress" type="hidden" maxlength="255" value="<%=order.getShippingEmailAddress()%>" /> 
	<input name="ship_address" type="hidden" maxlength="255" value="<%=order.getShippingStreet() + ","+ order.getShippingCompany()%>" />
	<input name="ship_city" type="hidden" maxlength="255"value="<%=order.getShippingCity()%>" /> 
	<input name="ship_state" type="hidden" maxlength="255" value="<%=order.getShippingState()%>" />
	<input name="ship_postal_code" type="hidden" maxlength="255" value="<%=order.getShippingZip()%>" /> 
	<input name="ship_country" type="hidden" maxlength="255" value="<%=order.getShippingCountry()%>" />
	<input name="ship_phone" type="hidden" maxlength="255"	value="<%=order.getShippingPhone()%>" /> 
	<input name="secure_hash" type="hidden" size="60" value="<%=EBSUtil.md5(md5HashData)%>"> 
	
<div class="row-fluid">
	<div class="span4 offset2">
  		<img src="<%=ctxPath%>/images/eec/common/cart/ebs.jpg" id="ebsimage"/> 
   </div>
   <div class="span5 offset1">
   <div id="ebspaybutton">  <input type="button" class="ebsbutton" name="<portlet:namespace />submitted" value="Pay Via EBS"  onClick="submitToEbs();" />  </div>
  </div>
</div>
</form>
<liferay-portlet:renderURL var="redirectURL">
<liferay-portlet:param name="jspPage" value="/html/eec/common/cart/view.jsp"/>
</liferay-portlet:renderURL>
<liferay-portlet:resourceURL var="resourceURL" />
<script>
	function submitToEbs() {
		AUI().use('aui-base','aui-io-request-deprecated', function(A){
			A.io.request('<%=resourceURL.toString()%>',{
			                        dataType: 'json',
			                        method: 'GET',
			                          on: {
			                                    success: function() {
			                                    	
			                                    	var myWalletAmount = this.get('responseData').retVal;
			                                    	if(myWalletAmount < <%=cart.getWalletAmount()%>){
			                                    		<%
			                                    		/*  not working  */
			                                    		SessionErrors.add(request,WalletLimitException.class.getName());
			                                    		%>
			                                    		window.location.href="<%=redirectURL.toString()%>";
			                                		}
			                                		else
			                                		{
			                                			document.frmTransaction.action="https://secure.ebs.in/pg/ma/sale/pay";
	 			                                		document.frmTransaction.submit();
			                                		}
			                                    }
			                        }
			            });
			});
	}
</script>

