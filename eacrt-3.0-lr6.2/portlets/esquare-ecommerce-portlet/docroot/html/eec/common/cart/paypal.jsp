<%@ include file="/html/eec/common/cart/init.jsp"%>

<portlet:actionURL var="paypalResponse" name="paypalResponse">
</portlet:actionURL>

<portlet:actionURL var="returnURLPAYPAL" name="redirectToPaypal">
<portlet:param name="<%= EECConstants.CMD %>" value="<%= EECConstants.PAYPAL %>" />
<portlet:param name="paypalResponse" value="<%= paypalResponse %>" />
</portlet:actionURL>




<form action="<%=returnURLPAYPAL%>" method="post" name="frm">


<div class="row-fluid">
	<div class="span4 offset2">
		<img src="<%=ctxPath%>/images/eec/common/cart/paypal.png" id="paypalimage"/>
	</div>
   <div class="span5 offset1">
		<div id="paypalbutton">
			<input type="button" class="ebsbutton" value="PAYPAL" onclick="addToCartPage()" />
		</div>
	</div>
</div>
</form>

<script type="text/javascript">
function addToCartPage(){
 	document.frm.action="<%= returnURLPAYPAL.toString()%>";
	document.frm.submit();
	}
</script>
