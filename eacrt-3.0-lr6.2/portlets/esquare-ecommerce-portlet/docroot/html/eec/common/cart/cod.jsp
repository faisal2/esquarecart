<%@ include file="/html/eec/common/cart/init.jsp" %>
<%
Cart cart = EECUtil.getCart(renderRequest);
Order order = OrderLocalServiceUtil.getLatestOrder(
		themeDisplay.getUserId(), themeDisplay.getCompanyId());
double grandTotal = EECUtil.calculateTotal(
		themeDisplay.getCompanyId(), themeDisplay.getUserId(),
		cart.getProductinventories(), order.getShippingCountry(),
		order.getShippingState(), cart.getCoupon(), cart.getAltShipping(),
		cart.isInsure());
double amountpayable=EECUtil.amountPayable(grandTotal, cart.getWalletAmount()) ;

%>


<portlet:resourceURL var="captchaURL">
			<portlet:param name="<%=EECConstants.CMD %>" value="<%=EECConstants.CAPTCHA %>" />
</portlet:resourceURL>


		
			<div class="row-fluid">
	<div class="span6">	
				<liferay-ui:error exception="<%= CaptchaTextException.class %>" message="text-verification-failed" />
				<liferay-ui:captcha url="<%= captchaURL %>" />
			</div>
		<div class="span6">
			<div class="verify">
				<p class="verifyheading">Verify Order</p>
				<p class="verifytext">Type the characters you see in the image on the left. Letters shown are case-sensitive.</p>
			</div>
		</div>
		</div>
	<br>		

<div class="row-fluid">
	<div class="span6">	
		<img src="<%=ctxPath%>/images/eec/common/cart/secure.png"/> 
	</div>
	<div class="span5 offset1">	
		<input type="button" class="codbutton" onClick='<%= renderResponse.getNamespace() + "submitForms();" %>' value="Place Order"/>
	</div>
</div>
<script>
function <portlet:namespace/>submitForms() {
        document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=(amountpayable>0)?EECConstants.COD:EECConstants.WALLET%>";
        document.<portlet:namespace/>fm.submit();
       }
</script>

<style>

</style>