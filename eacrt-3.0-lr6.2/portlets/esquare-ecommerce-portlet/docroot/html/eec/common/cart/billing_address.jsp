
<%@ include file="/html/eec/common/cart/init.jsp"%>
<script src="/html/js/liferay/service.js" language="Javacript"> </script>

<%
	themeDisplay.setIncludePortletCssJs(true);
%>
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
	
</script>

<script type="text/javascript">
	$(document).ready(function() {

		$("#slidingDiv").css("display", "none");
		$("#shipToBilling").click(function() {
			if ($("#shipToBilling").is(":checked")) {
				$("#slidingDiv").css("display", "none");
			} else {
				$("#slidingDiv").css("display", "block");
			}

		});

	});
</script>


<style type="text/css">
.lfr-panel.lfr-extended .lfr-panel-titlebar {
	background: none repeat scroll 0 0 #EC4D25;
	border: none;
	color: #FFFFFF;
	line-height: 1.6;
	padding: 2px;
}

#billingtopdiv {
	border-radius: 3px 3px 3px 3px;
}

#slidingDiv {
	display: none;
}
#shipToBilling
{
margin-top:-1px !important;
}
#detail_pro_added_to_wishlist
{
width:150px !important;
}
</style>

<%
	String star = "&nbsp;<font color='red' style='font-size: medium;'>*</font>";
	Order order = OrderLocalServiceUtil.getLatestOrder(
			themeDisplay.getUserId(), themeDisplay.getCompanyId());
	List addresses = AddressServiceUtil.getAddresses(
			Contact.class.getName(), contact.getContactId());

	long shippingStateId = 0l;
	long shippingCountryId = 0l;
	long billingStateId = 0l;
	long billingCountryId = 0l;

	if (Validator.isNotNull(order) && order.getBillingCountry() != "") {
		billingCountryId = CountryServiceUtil.fetchCountryByA2(
				order.getBillingCountry()).getCountryId();
		shippingCountryId = CountryServiceUtil.fetchCountryByA2(
				order.getShippingCountry()).getCountryId();
		billingStateId = Long.parseLong(order.getBillingState());
		shippingStateId = Long.parseLong(order.getShippingState());
	}
%>

<portlet:actionURL var="checkoutURL">
	<portlet:param name="<%=EECConstants.CMD1%>"
		value="<%=EECConstants.CHECKOUT%>" />
</portlet:actionURL>

<aui:form action="<%=checkoutURL%>" method="post" name="fm">
<!-- <p id="bill"></p> -->
	<div id="billingtopdiv">
	<div id="checkout"></div>
		<aui:input name="<%=Constants.CMD%>" type="hidden"
			value="<%=Constants.UPDATE%>" />



		<aui:model-context bean="<%=order%>" model="<%=Order.class%>" />

		<liferay-ui:error exception="<%=BillingCityException.class%>"
			message="please-enter-a-valid-city" />
		<liferay-ui:error exception="<%=BillingCountryException.class%>"
			message="please-enter-a-valid-country" />
		<liferay-ui:error exception="<%=BillingEmailAddressException.class%>"
			message="please-enter-a-valid-email-address" />
		<liferay-ui:error exception="<%=BillingFirstNameException.class%>"
			message="please-enter-a-valid-first-name" />
		<liferay-ui:error exception="<%=BillingLastNameException.class%>"
			message="please-enter-a-valid-last-name" />
		<liferay-ui:error exception="<%=BillingPhoneException.class%>"
			message="please-enter-a-valid-phone" />
		<liferay-ui:error exception="<%=BillingStateException.class%>"
			message="please-enter-a-valid-state" />
		<liferay-ui:error exception="<%=BillingStreetException.class%>"
			message="please-enter-a-valid-street" />
		<liferay-ui:error exception="<%=BillingZipException.class%>"
			message="please-enter-a-valid-postal-code" />

		<aui:fieldset>

			<table id="tabtable1">
				<tr>
					<td id="billing_maintab1"><a href="#" id="billing_tab1"></a></td>
					<td id="billing_maintab2"><a href="#tabs-2" id="billing_tab2"></a></td>
					<td id="billing_maintab3"><a href="#tabs-3" id="billing_tab3"></a></td>
				</tr>
			</table>

			<br class="break">
			<br class="break">
			<br />


			<div id="showDiv" class="ecart-headfontfam">Enter Billing
				Address</div>
			
			<div class="bill_border"></div>

			<div class="row-fluid">
				<div class="span6">
					First Name
					<%=star%>
					<aui:input label="" name="billingFirstName" type="text" />
					<div style="color: red;" id="finame"></div>
				</div>
				<div class="span6">
					Last Name
					<%=star%>
					<aui:input label="" name="billingLastName" type="text" />
					<div style="color: red;" id="laname"></div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6">
					Address
					<%=star%>
					<aui:input label="" type="text" name="billingStreet" maxlength="50" />
					<div style="color: red;" id="strt"></div>
				</div>

				<div class="span6">
					Billing City
					<%=star%>
					<aui:input label="" type="text" name="billingCity" maxlength="20" />
					<div style="color: red;" id="cty"></div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span6">
					Email Address
					<%=star%>
					<aui:input label="" name="billingEmailAddress" type="text" />
					<div style="color: red;" id="mailid"></div>
					</td>
				</div>

				<div class="span6">
					Pincode
					<%=star%>
					<aui:input label="" type="text" name="billingZip" />
					<div style="color: red;" id="pin"></div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span6">
					Phone
					<%=star%>
					<aui:input label="" type="text" name="billingPhone" />
					<div style="color: red;" id="phn"></div>
				</div>
				<div class="span6">
					Billing Country
					<%=star%>
					<aui:select label="" name="billingCountry" />
				</div>
			</div>

			<div class="row-fluid">
				<div class="span6"></div>
				<div class="span6">
					Billing State
					<%=star%>
					<aui:select label="" name="billingState" />
					<div id="stt"></div>
				</div>
			</div>

			<div class="row-fluid">
				<div class="span12">
					<input label="same-as-billing" type="checkbox" id="shipToBilling"
						name='<portlet:namespace />shipToBilling' checked="checked" />&nbsp;Same
					as Billing
				</div>
			</div>




		</aui:fieldset>


		<liferay-ui:error exception="<%=ShippingCityException.class%>"
			message="please-enter-a-valid-city" />
		<liferay-ui:error exception="<%=ShippingCountryException.class%>"
			message="please-enter-a-valid-country" />
		<liferay-ui:error exception="<%=ShippingEmailAddressException.class%>"
			message="please-enter-a-valid-email-address" />
		<liferay-ui:error exception="<%=ShippingFirstNameException.class%>"
			message="please-enter-a-valid-first-name" />
		<liferay-ui:error exception="<%=ShippingLastNameException.class%>"
			message="please-enter-a-valid-last-name" />
		<liferay-ui:error exception="<%=ShippingPhoneException.class%>"
			message="please-enter-a-valid-phone" />
		<liferay-ui:error exception="<%=ShippingStateException.class%>"
			message="please-enter-a-valid-state" />
		<liferay-ui:error exception="<%=ShippingStreetException.class%>"
			message="please-enter-a-valid-street" />
		<liferay-ui:error exception="<%=ShippingZipException.class%>"
			message="please-enter-a-valid-postal-code" />

		<aui:fieldset>



			<div id="slidingDiv">
				<div id="showDiv" class="ecart-headfontfam">Enter ShippingAddress</div>
				<div class="bill_border"></div>

				<div class="row-fluid">
					<div class="span6">
						First Name
						<%=star%>
						<aui:input label="" name="shippingFirstName" type="text" placeholder="Firstname"/>
					</div>
					<div class="span6">
						Shipping Address
						<%=star%>
						<aui:input label="" type="text" name="shippingStreet" />
						<div style="color: red;" id="strt"></div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						Last Name
						<%=star%>
						<aui:input label="" name="shippingLastName" type="text" />
					</div>
					<div class="span6">
						Shipping City
						<%=star%>
						<aui:input label="" type="text" name="shippingCity" />
						<div style="color: red;" id="cty"></div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						Email Address
						<%=star%>
						<aui:input label="" name="shippingEmailAddress" type="text" />
					</div>
					<div class="span6">
						Pincode
						<%=star%>
						<aui:input label="" type="text" name="shippingZip" />
						<div style="color: red;" id="pin"></div>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						Phone
						<%=star%>
						<aui:input label="" type="text" name="shippingPhone" />
						<div style="color: red;" id="phn"></div>
					</div>
					<div class="span6">
						Shipping Country
						<%=star%>
						<aui:select label="" name="shippingCountry" type="text" />
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6"></div>
					<div class="span6">
						Shipping State
						<%=star%>
						<aui:select label="" name="shippingState" type="text" />

					</div>
				</div>
			</div>




		</aui:fieldset>

		
		<div align="center">
			<input type="submit" class="saveandcontinue" id="detail_pro_added_to_wishlist"
				value="Save and Continue" />
		</div>
	</div>
</aui:form>

<aui:script use="liferay-dynamic-select,aui-io-request-deprecated">
            new Liferay.DynamicSelect(
                [
                    {
                        select: '<portlet:namespace />billingCountry',
                        selectData: Liferay.Address.getCountries,
                        selectDesc: 'name',
                        selectId: 'countryId',
                        selectVal: '<%=(billingCountryId>0)?billingCountryId:""%>'
                    },
                    {
                        select: '<portlet:namespace />billingState',
                        selectData: Liferay.Address.getRegions,
                        selectDesc: 'name',
                        selectId: 'regionId',
                        selectVal: '<%=(billingStateId>0)?billingStateId:""%>'
                    }
                    
                ]
            );

            new Liferay.DynamicSelect(
                [
                    {
                        select: '<portlet:namespace />shippingCountry',
                        selectData: Liferay.Address.getCountries,
                        selectDesc: 'name',
                        selectId: 'countryId',
                        selectVal: '<%=(shippingCountryId>0)?shippingCountryId:""%>'
                    },
                    {
                        select: '<portlet:namespace />shippingState',
                        selectData: Liferay.Address.getRegions,
                        selectDesc: 'name',
                        selectId: 'regionId',
                        selectVal: '<%=(shippingStateId>0)?shippingStateId:""%>'
                    }
                ]
            );
            
            <c:if
		test='<%=Validator.isNull(order)||(Validator.isNotNull(order)&&order.getShippingState()=="") %>'>
             new Liferay.DynamicSelect(
                [
                    {
                        select: '<portlet:namespace />billingCountry',
                        selectData: Liferay.Address.getCountries,
                        selectDesc: 'name',
                        selectId: 'countryId',
                        selectVal: '<%=(billingCountryId>0)?billingCountryId:""%>'
                    },
                    {
                        select: '<portlet:namespace />shippingState',
                        selectData: Liferay.Address.getRegions,
                        selectDesc: 'name',
                        selectId: 'regionId',
                        selectVal: '<%=(shippingStateId>0)?shippingStateId:""%>'
                    }
                ]
            );
            </c:if>
</aui:script>

<aui:script>
	function <portlet:namespace />updateAddress(addressId, type) {

		<%
	for (int i = 0; addresses != null && i < addresses.size(); i++) {
			Address address = (Address) addresses.get(i);

			Region region = address.getRegion();
			Country country = address.getCountry();
%>

			if ("<%=address.getAddressId()%>" == addressId) {
				//document.getElementById("<portlet:namespace />" + type + "FirstName").value = "<%=user.getFirstName()%>";
				//document.getElementById("<portlet:namespace />" + type + "LastName").value = "<%=user.getLastName()%>";
				//document.getElementById("<portlet:namespace />" + type + "EmailAddress").value = "<%=user.getEmailAddress()%>";
				document.getElementById("<portlet:namespace />" + type + "Street").value = "<%=address.getStreet1()%>";
				document.getElementById("<portlet:namespace />" + type + "City").value = "<%=address.getCity()%>";

				var stateSel = document.getElementById("<portlet:namespace />" + type + "StateSel");
				var stateSelValue = "<%=region.getRegionCode()%>";

				for (var i = 0; i < stateSel.length; i++) {
					if (stateSel[i].value == stateSelValue) {
						stateSel.selectedIndex = i;

						break;
					}
				}

				document.getElementById("<portlet:namespace />" + type + "Zip").value = "<%=address.getZip()%>";
				document.getElementById("<portlet:namespace />" + type + "Country").value = "<%=country.getName()%>";
			}

		<%
 	}
 %>

	}

	<c:if test="<%=windowState.equals(WindowState.MAXIMIZED)%>">
		Liferay.Util.focusFormField(document.<portlet:namespace />fm.<portlet:namespace />billingFirstName);
	</c:if>
</aui:script>
<aui:script
	use="aui-form-validator, aui-overlay-context-panel,aui-base,event">
			var shipToBill = document.getElementById("shipToBilling").checked;
			var b=A.one(shipToBilling);
			var shipToBilling= A.one('#shipToBilling');
			AUI().ready('aui-base','event','node',function(A) {
			var validator1 = new A.FormValidator({
			boundingBox: document.<portlet:namespace />fm,
			rules: {
			
			<portlet:namespace />billingFirstName: {
			required: true
			},
			<portlet:namespace />billingStreet: {
			required: true
			},
			<portlet:namespace />billingLastName: {
			required: true
			},
			<portlet:namespace />billingCity: {
			required: true
			},
			<portlet:namespace />billingEmailAddress: {
			required: true,
			email:true
			},
			
			
			<portlet:namespace />billingZip: {
			required: true,
			digits:true,
			rangeLength: [6,6]
			},
			<portlet:namespace />billingPhone: {
			required: true,
			digits:true,
			rangeLength: [10,13]
			},
			<portlet:namespace />billingCountry: {
			required: true
			},
			<portlet:namespace />billingState: {
			required: true
			}
			}
			,
			fieldStrings: {
			<portlet:namespace />billingCountry: {
			required: '<liferay-ui:message key="please select billing country" />'
			},
			<portlet:namespace />billingCountry: {
			required: '<liferay-ui:message key="please select billing state" />'
			}
			}
			});
			shipToBilling.on('click', function(){
			shipToBill = document.getElementById("shipToBilling").checked;
			var flag = true;
			if(shipToBill){
			flag = false;
			<portlet:namespace />assignShippingValue();
			} 
			 
			var validator2 = new A.FormValidator({
			boundingBox: document.<portlet:namespace />fm,
			rules: {
			<portlet:namespace />shippingFirstName: {
			required:flag
			},  
			<portlet:namespace />shippingLastName: {
			required:flag
			},
			<portlet:namespace />shippingEmailAddress: {
			required:flag,
			email:flag
			},
			<portlet:namespace />shippingStreet: {
			required:flag,
			},
			<portlet:namespace />shippingCountry: {
			required:flag
			},
			<portlet:namespace />shippingState: {
			required:flag
			},
			<portlet:namespace />shippingZip: {
			required:flag
			},
			<portlet:namespace />shippingCity: {
			required:flag
			},
			<portlet:namespace />shippingPhone: {
			required:flag,
			digits:flag,
			rangeLength:[10,13]
			}
			},
			fieldStrings: {
			<portlet:namespace />shippingCountry: {
			required: '<liferay-ui:message key="please select shipping country" />'
			},
			<portlet:namespace />shippingState: {
			required: '<liferay-ui:message key="please select shipping state" />'
			}
			}
			})
			   });
			   
			   var buttonEvent= A.one('#savecontinuebilling');
			   buttonEvent.on('click', function(){
			   var shipToBill = document.getElementById("shipToBilling").checked;
			   if(shipToBill){
			       <portlet:namespace />assignShippingValue();
			   }
			   });
});
Liferay.provide(
		window,
		'<portlet:namespace />assignShippingValue',
		function() {
		document.getElementById("<portlet:namespace />shippingFirstName").value = document.getElementById("<portlet:namespace />billingFirstName").value;
		document.getElementById("<portlet:namespace />shippingLastName").value = document.getElementById("<portlet:namespace />billingLastName").value;
		document.getElementById("<portlet:namespace />shippingEmailAddress").value = document.getElementById("<portlet:namespace />billingEmailAddress").value;
		document.getElementById("<portlet:namespace />shippingStreet").value = document.getElementById("<portlet:namespace />billingStreet").value;
		document.getElementById("<portlet:namespace />shippingCountry").value = document.getElementById("<portlet:namespace />billingCountry").value;
		document.getElementById("<portlet:namespace />shippingState").value = document.getElementById("<portlet:namespace />billingState").value;
		document.getElementById("<portlet:namespace />shippingZip").value = document.getElementById("<portlet:namespace />billingZip").value;
		document.getElementById("<portlet:namespace />shippingCity").value = document.getElementById("<portlet:namespace />billingCity").value;
		document.getElementById("<portlet:namespace />shippingPhone").value = document.getElementById("<portlet:namespace />billingPhone").value;
		});
</aui:script>
<style>
.aui input[type="text"]{
	box-shadow: 0 0px 2px rgba(0, 0, 0, 0.075) inset !important;
    border-radius: 3px !important;
    border: 1px solid #ddd !important;
     height: 28px;
    width: 60%;
}
.aui select{
	box-shadow: 0 0px 2px rgba(0, 0, 0, 0.075) inset !important;
    border-radius: 3px !important;
    border: 1px solid #ddd !important;
      height:38px;
    width: 63%;
}
#_cart_WAR_esquareecommerceportlet_fm{
	padding-left: 22px;
}
</style>