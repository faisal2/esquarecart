<%@ include file="/html/eec/common/cart/init.jsp" %>

<script type="text/javascript"src="<%=request.getContextPath()%>/js/eec/common/cart/jquery-1.7.1.min.js"></script>
<script type="text/javascript"src="<%=request.getContextPath()%>/js/eec/common/cart/jquery.easytabs.min.js"></script>

<script type="text/javascript"src="<%=request.getContextPath()%>/js/eec/common/cart/jquery.hashchange.min.js"></script>

<style type="text/css">

/* #tabtable1 {
    background: none repeat scroll 0 0 #252525;
    height: 35px;
    width: 100%;
    border: 1px solid #B5C9DA;
} */
/*
#payment-tab1{
color: #ffffff;
text-decoration: none;
 font-size: 16px;
 font-weight: bold;
}
#payment-tab2{
color: #ffffff;
text-decoration: none;
 font-size: 16px;
 font-weight: bold;
}
 #payment-tab3{
color: #EC4D25;
text-decoration: none;
 font-size: 16px;
 font-weight: bold;
} */
#maintab1{
text-align: center;
border-right: 1px solid lavender;
}
#maintab2{
text-align: center;
border-right: 2px solid #B5C9DA;
}
#maintab3{
text-align: center;
background:#ffffff;
}
/* #paymenttopdiv{
   border: 1px solid #B5C9DA;
   border-radius: 3px 3px 3px 3px;
   height: 300px;
 
} */

#etabs { margin: 0; padding: 0;position: absolute; }
.tab { display:table-row; zoom:1; *display:inline; background: #FFFFFF; border: medium none; border: medium none; -moz-border-radius: 4px 4px 0 0; -webkit-border-radius: 4px 4px 0 0;height: 60px; }

.tab a:hover { text-decoration: none; }
.tab.active { background: #fff; padding-top: 6px; position:static; top: 1px; border-color: #666;border: medium none; }
.tab-container .panel-container { background: #fff; border: solid #666 1px; padding: 10px; -moz-border-radius: 0 4px 4px 4px; -webkit-border-radius: 0 4px 4px 4px; }
 #tab-container{
 width:100%;
 
 }   
#tabs1-cash{
 width: 100%;
 margin-left: 100px;
 margin-top: 0;
 border-radius: 8px 0 0 8px;
}
@media (max-width:500px){
#tabs1-cash{
margin-top: 52%;
 margin-left: 0;
}
}
#tabs1-net{
margin-left: 161px;
margin-top: 0;
display:none;
}

#tabs1-ebs{
 /* margin-top: 45px; */
 margin-left: 161px;
 display:none;
}
/* .tab a.active {
    background: none repeat scroll 0 0 #EC4D25;
    color: #FFFFFF;
    font-family: tahoma;
    font-weight: bold;
    text-decoration: none;
}  */
</style>
<%
// Cart cart = EECUtil.getCart(renderRequest);
PaymentConfiguration cod= PaymentConfigurationLocalServiceUtil.findByPaymentType(themeDisplay.getCompanyId(),EECConstants.COD);
PaymentConfiguration ebs= PaymentConfigurationLocalServiceUtil.findByPaymentType(themeDisplay.getCompanyId(),EECConstants.EBS);
PaymentConfiguration paypal= PaymentConfigurationLocalServiceUtil.findByPaymentType(themeDisplay.getCompanyId(),EECConstants.PAYPAL);
Cart cart = EECUtil.getCart(renderRequest);
Order order = OrderLocalServiceUtil.getLatestOrder(
		themeDisplay.getUserId(), themeDisplay.getCompanyId());
double grandTotal = EECUtil.calculateTotal(
		themeDisplay.getCompanyId(), themeDisplay.getUserId(),
		cart.getProductinventories(), order.getShippingCountry(),
		order.getShippingState(), cart.getCoupon(), cart.getAltShipping(),
		cart.isInsure());
double amountpayable=EECUtil.amountPayable(grandTotal, cart.getWalletAmount()) ;

String tabs1 = ParamUtil.getString(request, "tabs1", "ListView");
PortletURL viewURL = renderResponse.createRenderURL();
viewURL.setParameter("tabs1", tabs1);
Role role = RoleLocalServiceUtil.getRole(themeDisplay.getCompanyId(),PortletPropsValues.DEFAULT_SHOP_ADMIN_ROLE);
%>
<portlet:actionURL var="caheckOutUrl" >
		<portlet:param name="<%=EECConstants.CMD1 %>" value="<%=EECConstants.CHECKOUT %>" />
</portlet:actionURL>


    
    <div id="paymenttopdiv">
 
     	<table id="tabtable1">
			<tr>
				<td id="payment_maintab1"><a href="" onclick="javascript:history.go(-2)" id="payment-tab1"></a></td>
				<td id="payment_maintab2"><a href="#tabs-2"  onclick="javascript:history.go(-1)" id="payment-tab2"></a></td>
				<td id="payment_maintab3"><a href="#tabs-3" id="payment-tab3"></a></td>
			</tr>
		   </table>
<div id="tab-container" class="tab-container">

 <ul class='ecart-headfontfam' id="etabs">
 <c:if test="<%=Validator.isNotNull(cod)&&cod.isStatus()%>">
    <li class='tab'><a href="#tabs1-cash"><%=amountpayable>0?"Cash on Delivery":"Use My Wallet"%></a></li>
    </c:if>
    
    <c:if test="<%=Validator.isNotNull(paypal)&&paypal.isStatus()&&amountpayable>0%>">
     <li class='tab'><a href="#tabs1-net">Pay Via PAYPAL</a></li>
     </c:if>
     <c:if test="<%=Validator.isNotNull(ebs)&&ebs.isStatus()&&amountpayable>0%>">
    <li class='tab'><a href="#tabs1-ebs">Pay Via EBS</a></li>
    </c:if>
 </ul>
 <div id="tabs1-cash">
<aui:form name="fm" method="post" action="<%=caheckOutUrl %>">
<aui:input name="<%= EECConstants.CMD %>" type="hidden" />
<c:if test="<%=Validator.isNotNull(cod)&&cod.isStatus()%>">
<liferay-util:include page="/html/eec/common/cart/cod.jsp"  servletContext="<%=this.getServletContext() %>" />
</c:if>
 </aui:form>
    </div>


 <div id="tabs1-net">
<c:if test="<%=Validator.isNotNull(paypal)&&paypal.isStatus()&&amountpayable>0%>">
<liferay-util:include page="/html/eec/common/cart/paypal.jsp"  servletContext="<%=this.getServletContext() %>" />
</c:if>
   </div>
    <div id="tabs1-ebs">
<c:if test="<%=Validator.isNotNull(ebs)&&ebs.isStatus()&&amountpayable>0%>">
<liferay-util:include page="/html/eec/common/cart/ebs.jsp"  servletContext="<%=this.getServletContext() %>" />
</c:if>
</div>
<c:if test="<%=(Validator.isNull(cod)&&Validator.isNull(ebs)&&Validator.isNull(paypal)||(!cod.isStatus()&&!ebs.isStatus()&&!paypal.isStatus()))%>" >
 <div id="paygatnconf">
  <span id="paygatnconftext"> Payment Gateway is not yet configured.
  <c:choose>
  <c:when test="<%=!isShopAdmin%>" >
  Please contact customer support.
  </c:when>
 <c:otherwise >
  Please Configure <a href="/group/admin1/payment-config">here</a>. </span>
 </c:otherwise>
 </c:choose>
 </div>
</c:if>
        
        </div>
 </div>




 
  <script type="text/javascript">
    $(document).ready( function() {
      $('#tab-container').easytabs();
    });
  </script>
 <style>
 
 </style>