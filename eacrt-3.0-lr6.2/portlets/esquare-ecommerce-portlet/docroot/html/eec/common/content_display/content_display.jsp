<%@ include file="/html/eec/common/content_display/init.jsp"%>
<%
	DDMTemplate template = null;
	String editorContent = StringPool.BLANK;

	String friendlyURL = themeDisplay.getLayout().getFriendlyURL()
			.substring(1);
	Group group = GroupLocalServiceUtil.getGroup(
			themeDisplay.getCompanyId(), EECConstants.SHOP_ADMINNAME);
	template = EECUtil.getTemplateByGroupId(group.getGroupId(),
			friendlyURL);
	if (Validator.isNotNull(template))
		editorContent = template.getScript();

	String templateId = BeanParamUtil.getString(template, request,
			"templateId");
%>
<c:choose>
<c:when test="<%=isShopAdmin%>">
<liferay-portlet:actionURL var="saveContentURL" />
<liferay-portlet:renderURL var="previewURL">
<portlet:param name="jspPage" value="/html/eec/common/content_display/admin_preview.jsp" />
<portlet:param name="redirectURL" value="<%=themeDisplay.getURLCurrent()%>"  />
</liferay-portlet:renderURL>
<aui:form action="<%=saveContentURL%>" method="post" name="fm"
	onSubmit='<%="event.preventDefault(); "+ renderResponse.getNamespace() + "saveContent();"%>'>
	<liferay-ui:input-editor editorImpl="<%=EDITOR_WYSIWYG_IMPL_KEY%>" />
	<aui:input name="<%=EECConstants.CMD%>" type="hidden" />
	<aui:input name="name" value="<%=friendlyURL%>" type="hidden"/>
	<aui:input name="editorContent" type="hidden" >
	</aui:input>
	<aui:input name="templateId" type="hidden" value="<%=templateId%>" />
	<aui:button-row>
		<aui:button type="submit"  />
		<aui:button type="button" onClick="<%=previewURL.toString()%>" value="preview" />
	</aui:button-row>
</aui:form>
</c:when>
<c:otherwise >
<%=editorContent%>
</c:otherwise>
</c:choose>
<%!public static final String EDITOR_WYSIWYG_IMPL_KEY = "editor.wysiwyg.portal-web.docroot.html.eec.common.content_display.content_display.jsp";%>
<aui:script>
function <portlet:namespace />initEditor() {
		return "<%=UnicodeFormatter.toString(editorContent)%>";
	}
Liferay.provide(
		window,
		'<portlet:namespace />saveContent',
		function() {
			document.<portlet:namespace />fm.<portlet:namespace /><%=EECConstants.CMD%>.value = "<%=(template == null) ? EECConstants.ADD
						: EECConstants.UPDATE%>";
			document.<portlet:namespace />fm.<portlet:namespace />editorContent.value = window.<portlet:namespace />editor.getHTML();
		
			submitForm(document.<portlet:namespace />fm);
		},
		['liferay-util-list-fields']
	);
</aui:script>