<%@ include file="/html/eec/common/content_display/init.jsp"%>
<%	String redirectURL=ParamUtil.getString(request, "redirectURL");
	DDMTemplate template = null;
	String editorContent = StringPool.BLANK;
	String friendlyURL = themeDisplay.getLayout().getFriendlyURL()
			.substring(1);
	Group group = GroupLocalServiceUtil.getGroup(
			themeDisplay.getCompanyId(), EECConstants.SHOP_ADMINNAME);
	template = EECUtil.getTemplateByGroupId(group.getGroupId(),
			friendlyURL);
	if (Validator.isNotNull(template))
		editorContent = template.getScript();
%>
<liferay-ui:header
	backURL="<%= redirectURL %>" title="" showBackURL="true" 
/>
<%=editorContent%>
