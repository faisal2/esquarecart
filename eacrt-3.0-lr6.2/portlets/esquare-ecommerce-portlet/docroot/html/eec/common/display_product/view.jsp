<%@ include file="/html/eec/common/display_product/init.jsp" %>

<%
	String content="";
	
	long productsPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.PRDODUCTS_PAGE_FRIENDLY_URL).getPlid();
	PortletURL userViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productsPlid,
			ActionRequest.ACTION_PHASE, null,"navigationProcess", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);

	
	long productdetailsPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(
			themeDisplay.getScopeGroupId(), false,EECConstants.PRODUCT_DETAILS_PAGE_FRIENDLY_URL).getPlid();
	PortletURL detailviewURL=EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productdetailsPlid,
			ActionRequest.ACTION_PHASE, null,"redirectToDetailsPage", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
	
	
	long cartPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.CART_PAGE_FRIENDLY_URL).getPlid();
	PortletURL cartViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.CART_PORTLET_NAME, cartPlid,
			ActionRequest.ACTION_PHASE, null,null, LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
	cartViewURL.setParameter(EECConstants.CMD , EECConstants.ADD);
	cartViewURL.setParameter(EECConstants.CMD1 , EECConstants.CART_UPDATE);
		
	PortletURL renderURL=EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productsPlid,
			ActionRequest.ACTION_PHASE, null,"redirectToDetailsPage", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
	renderURL.setParameter("cmd","detailsView");
	
	int viewCount = PrefsParamUtil.getInteger(preferences, renderRequest, "viewCount");
	String type = PrefsParamUtil.getString(preferences, renderRequest, "type");
	long parentCatalogId = PrefsParamUtil.getLong(preferences, renderRequest, "parentCatalogId");
	try{
	content=UserViewUtil.getProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),viewCount,type,parentCatalogId,detailviewURL,cartViewURL,userViewURL,ctxPath,PortletPropsValues.EEC_HOME_GRIDCLASS,0,renderRequest);
	}
	catch(Exception e){
	}
	 
%>
	<%=content%>


<style>

.home-gridclass{
display: inline-table;
}

@-moz-document .home-gridclass{
display: -moz-inline-box;
}
.eec-displaylist{
list-style: none;
background-color: white;
}
#def-headerul
{
text-align:center !important;
}
#hompage-gridlist
{
max-width:20%;
}
#offper
{
margin-left:-25px;
}
</style>

