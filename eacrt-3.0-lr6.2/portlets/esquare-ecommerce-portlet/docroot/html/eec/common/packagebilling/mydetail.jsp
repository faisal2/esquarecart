<%@page import="java.util.Date"%>
<%@ include file="/html/eec/common/packagebilling/init.jsp"%>
<%
	PaymentUserDetail paymentUserDetail = PaymentUserDetailLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId());
String countryName=null;
String regionName=null;
try{
	Country countryobj =CountryServiceUtil.getCountry(Long.parseLong(paymentUserDetail.getCountry()));	
	countryName=countryobj.getName();
	Region regionobj= RegionServiceUtil.getRegion(Long.parseLong( paymentUserDetail.getState()));
	regionName=regionobj.getName();
	   }
catch(Exception e){
	   }

	   %>
<portlet:renderURL var="editMyDetail">
<portlet:param name="jspPage" value="/html/eec/common/packagebilling/packagebilling.jsp"/>
<portlet:param name="recId" value='<%=Validator.isNotNull(paymentUserDetail)?String.valueOf(paymentUserDetail.getRecId()):""%>'/>
</portlet:renderURL>

<div class="dbaccount-mydetailadd" id="form">
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Name :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getName():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Address :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getAddress():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Country :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(countryName)?countryName:""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					State :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(regionName)?regionName:""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					City :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getCity():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Pin :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getPinCode():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					E-mail :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getEmail():""%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="25" first="true">
					Phone :
		</aui:column>
	<aui:column columnWidth="75" last="true">
		<%=Validator.isNotNull(paymentUserDetail)?paymentUserDetail.getPhoneNo():""%>
	</aui:column>
</aui:layout><br/>
<input type="button" value="Edit" id="addproduct" onClick="location.href='<%=editMyDetail.toString()%>';" />
<c:if test="<%=Validator.isNotNull(paymentUserDetail)%>">

<%-- <aui:button  name="button" value="Edit" href="<%=editMyDetail.toString()%>"/> --%>
</c:if>


</div>

<style>
#_packagebilling_WAR_esquareecommerceportlet_button {
    margin-left: 0;
    margin-top: 12px;
}
</style>
