<%@ include file="/html/eec/common/packagebilling/init.jsp"%>
<script src="/html/js/liferay/service.js" language="Javacript"> </script>
<%
	themeDisplay.setIncludeServiceJs(true);
	String recIdString = ParamUtil.getString(request, "recId", null);
	PaymentUserDetail paymentUserDetail= null;
	long stateId=0l;
	try{
	if (Validator.isNotNull(recIdString)) {
		paymentUserDetail = PaymentUserDetailLocalServiceUtil
				.fetchPaymentUserDetail(Long.valueOf(recIdString));
		stateId=Long.parseLong(paymentUserDetail.getState());
	}
	}
	catch(NumberFormatException exception){
		stateId=0l;
	}
	catch(Exception exception){}
%>

<portlet:actionURL var="editMyDetail" />
<aui:form name="fm" action="<%=editMyDetail.toString()%>" method="post">
	<aui:model-context bean="<%=paymentUserDetail%>"
		model="<%=PaymentUserDetail.class%>" />
	<aui:input name="recId" type="hidden"
		value="<%=Long.valueOf(recIdString)%>" />
		<aui:input name="<%=EECConstants.CMD %>" type="hidden"
		value="<%=EECConstants.EDIT%>" />
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					Name:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="name" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					Address:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="address" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					Country:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:select name="country" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					State:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:select name="state" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					City:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="city" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					Pin:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="pinCode" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					E-mail:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="email" label="" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
					Phone:
		</aui:column>
		<aui:column columnWidth="50">
			<aui:input name="phoneNo" label="" />
		</aui:column>
	</aui:layout>
	<aui:button-row>
		<aui:button type="submit" value="Confirm" />
	</aui:button-row>
</aui:form>
<aui:script use="liferay-dynamic-select,aui-io-request-deprecated">
					new Liferay.DynamicSelect(
						[
							{
								select: '<portlet:namespace />country',
								selectData: Liferay.Address.getCountries,
								selectDesc: 'name',
								selectId: 'countryId',
								selectVal: '<%=Long.parseLong(paymentUserDetail.getCountry())%>'
							},
							{
								select: '<portlet:namespace />state',
								selectData: Liferay.Address.getRegions,
								selectDesc: 'name',
								selectId: 'regionId',
								selectVal:'<%=stateId%>'
							}
						]
				);
</aui:script>
