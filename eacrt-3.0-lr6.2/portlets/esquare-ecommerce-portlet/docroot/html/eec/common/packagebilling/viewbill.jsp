<%@ include file="/html/eec/common/packagebilling/init.jsp"%>
<%
Calendar curDate = Calendar.getInstance();
Date yearRangeStart=PortletPropsValues.PACKAGE_BILLING_VIEW_YEAR_RANGE_START;
Calendar yearRangeStartCalendar = Calendar.getInstance();
yearRangeStartCalendar.setTime(yearRangeStart);
%>
<portlet:resourceURL var="resourceURL" />
<div id="dbaccount-viewbillmdiv">
<aui:form name="fm" method="POST" action="<%=resourceURL.toString()%>" >
<aui:input name="<%=EECConstants.CMD%>" value="<%=EECConstants.DOWNLOAD%>" type="hidden"/>
	<aui:layout id="web1" style="padding-left:20px;">
		<aui:column columnWidth="35" first="true">
	Please View Your Statement of Account:
		</aui:column>
		<aui:column columnWidth="10" >
			<aui:select name="year" label="">
			<%
			for(int i=yearRangeStartCalendar.get(Calendar.YEAR);i<=curDate.get(Calendar.YEAR);i++){
			%>
			<aui:option label="<%=i%>" value="<%=i%>" selected="<%=(i==curDate.get(Calendar.YEAR))?true:false%>" />
			<%
			}
			%>
			</aui:select>
			</aui:column>
			<aui:column columnWidth="15" >
			<aui:select name="month" label="" />
			</aui:column>
		
	</aui:layout>
	<aui:button-row>
		<aui:button onClick='<%= renderResponse.getNamespace() + "submitForms();" %>' value="Submit" style="margin-left: 20px;" id="addproduct" />
	</aui:button-row>
</aui:form>
</div>

<script type="text/javascript" >
$(document).ready(function(){
	var url="<%=resourceURL%>";
	  var yearValueReady=$("#<portlet:namespace />year").val();
	  jSonCall(url,yearValueReady); 
 $("#<portlet:namespace />year").change(function(){
	 var yearValueChange=$("#<portlet:namespace />year").val();
  jSonCall(url,yearValueChange);
 });
});

function jSonCall(url,yearValueChange){
	jQuery.getJSON(url+"&year="+yearValueChange+"&cmd=MONTH" , function(data)  {
		  if(data.monthValue.length>0){
			  var month=new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	     for(i=0;i<data.monthValue.length;i++){
	    	 $("#<portlet:namespace />month").append("<option value='"+ data.monthValue[i] +"'>"+month[data.monthValue[i]]+"</option> " );
	   }  
		  }else {
			  $("#<portlet:namespace />month").empty();
		}
	  });
}
 </script>

<%-- <aui:script use="aui-io-request-deprecated">
Liferay.provide(
		window,
		'<portlet:namespace />submitForm',
		function() {
		alert("coming  here");
		var url = '<portlet:resourceURL />'+'&cmd='+'<%=EECConstants.DOWNLOAD%>';
	A.io.request(url, {
  form: {
      id: '<portlet:namespace />fm'
  }
});
})
			</aui:script> --%>
			<aui:script>
Liferay.provide(
		window,
		'<portlet:namespace />submitForms',
		function() {
		var A = AUI();
		var month=A.one('#<portlet:namespace/>month').val();
		if(month == "") {
                       alert("Bill is Not Available");
                       return false;
                }
                else{
                submitForm(document.<portlet:namespace />fm);
                }
		});
</aui:script>