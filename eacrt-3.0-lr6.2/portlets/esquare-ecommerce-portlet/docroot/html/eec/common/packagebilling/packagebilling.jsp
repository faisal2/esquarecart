<%@ include file="/html/eec/common/packagebilling/init.jsp"%>
<%
boolean isFreePackage=InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId()).getPackageType().equalsIgnoreCase(PortletPropsValues.PACKAGE_TYPE_FREE);
%>
<c:choose>
<c:when test="<%=isFreePackage%>" >
<div class="portlet-msg-info">
<liferay-ui:message key="you-do-not-have-permission-to-access-this-section.please-upgrade-your-package" />
</div>
</c:when>
<c:otherwise >
<%
	String[] tabName = PortletPropsValues.PACKAGE_BILLING_TABS
			.split(",");
	String tabs = ParamUtil.getString(request, "tabs", tabName[0]);
	String recIdString=ParamUtil.getString(request,"recId",null);
	String transactionId=ParamUtil.getString(request,"transactionId",null);
	boolean isTransactionFailed=ParamUtil.getBoolean(request, "isTransactionFailed",false);
	boolean isFlagged=ParamUtil.getBoolean(request, "isFlagged",false);
%>
<portlet:renderURL var="portletURL">
	<portlet:param name="tabs" value="<%=tabs%>" />
</portlet:renderURL>
<div class="dashboard-maindiv2">
<div class="dashboard-pagemainheading70" id="dashboard-maindiv" >Account</div>
<hr style="margin-top: 13px;">
<div id="dbaccount-maindv">
<span class="dashboardsetting-emailtab" >
<liferay-ui:tabs names="<%=PortletPropsValues.PACKAGE_BILLING_TABS%>"
	param="tabs" url="<%=portletURL.toString()%>" />
	</span></div>


	<%			
		for (String curTab : tabName) {
			StringBuffer path = new StringBuffer("/html/eec/common/packagebilling/");
			path.append(curTab.replaceAll("\\s","").toLowerCase());
			if(Validator.isNotNull(recIdString)) {
				path.append("_edit.jsp");
			} 
			else if(Validator.isNotNull(transactionId)){
				path.append("_payment_success.jsp");
			}
			else if(isTransactionFailed){
				path.append("_payment_failure.jsp");
			}
			else if(isFlagged){
				path.append("_payment_flagged.jsp");
			}
			else {
				path.append(".jsp");
			}
	%>
	<c:if test='<%=tabs.equals(curTab)%>'>
		<liferay-util:include
			page="<%=path.toString()%>"
			servletContext="<%=this.getServletContext()%>" />
	</c:if>
	<%
		}
	%>
</div>
</c:otherwise>
</c:choose>