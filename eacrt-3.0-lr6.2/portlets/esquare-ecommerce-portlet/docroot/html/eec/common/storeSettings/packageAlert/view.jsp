<% 
String currentInstance = themeDisplay.getCompany().getWebId();
String currentPackage = InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId()).getPackageType();

Date packageCreateDate =  InstanceLocalServiceUtil.getInstance(themeDisplay.getCompanyId()).getCreatedDate();
java.sql.Date packageCreatedDate = new java.sql.Date(packageCreateDate.getTime());	

DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
int max_Days = 0;
if(currentPackage.equals(PortletPropsValues.PACKAGE_TYPE_FREE)){
	max_Days = Integer.parseInt(EECConstants.MAX_LICENCE_DAYS[0]);
}else{
	max_Days = Integer.parseInt(EECConstants.MAX_LICENCE_DAYS[1]);
}
 
//Package Expiry Date
Calendar calendar = Calendar.getInstance();	 
calendar.setTime(packageCreateDate);
calendar.add(Calendar.DATE, max_Days);	
Date expiryDate=dateformat.parse(dateformat.format(calendar.getTime())); 
java.sql.Date packageExpiryDate = new java.sql.Date(expiryDate.getTime());	

//Diff between 2 dates then count
int remainingDays = DateUtil.getDaysBetween(DateUtil.newDate(), packageExpiryDate);
%>

	<div id="storesetting-packagealert-div1">
		
					<div class="dashboard-headingstyle">Package Alert</div>	
					<hr style="margin-top: 13px;">
			<table class="table1" id="storesetting-packagealert-table1">
			<tr>
				<td align="left">
					<!-- <div id="storesetting-packagealert-body"> -->
					   <table id="oddeven"  class="storesetting-packagealert-td">
							
							<tr class="dashboard-pakagealert">
											<td class="dashboard-pakagealert-criteria1"> Current Package</td>
											<td class="dashboard-colon"> : </td>
											<td class="dashboard-pakagealert-statusvalue1"  > <%= currentPackage%></td>
							</tr>
							
							<tr class="dashboard-pakagealert">
											<td class="dashboard-pakagealert-criteria2">  Created Date </td>
											<td class="dashboard-colon"> :  </td>
											<td class="dashboard-pakagealert-statusvalue2" ><%= packageCreatedDate%></td>
								
								
							</tr>
							
							<tr class="dashboard-pakagealert">
								
											<td class="dashboard-pakagealert-criteria1">Expire Date</td>
											<td class="dashboard-colon"> : </td>
											<td class="dashboard-pakagealert-statusvalue1" >  <%= packageExpiryDate%></td>
							</tr>
							<tr class="dashboard-pakagealert">
								
											<td class="dashboard-pakagealert-criteria2">License</td>
											<td class="dashboard-colon"> : </td>
											<td class="dashboard-pakagealert-statusvalue2" > <%= max_Days%> Days</td>
							</tr>
							<tr class="dashboard-pakagealert">
											<td class="dashboard-pakagealert-criteria1">Remaining</td>
											<td class="dashboard-colon"> : </td>
											<td class="dashboard-pakagealert-statusvalue1" > <%= remainingDays%> Days </td>
							</tr>
						</table>
					<!-- </div> -->
				</td>
			</tr>
		</table>
		</div>
		<style type="text/css">
			
			.dashboard-border{
				border-bottom: 1px solid #C3D0E1;
			}
			
.dashboard-pakagealert-criteria1,.dashboard-pakagealert-criteria2,.dashboard-pakagealert-statusvalue1,.dashboard-pakagealert-statusvalue2{
	color: gray;
    font-family: arial;
    font-size: 13px;
}
#storesetting-packagealert-div1 {
     border: 1px solid #cccccc;
    border-radius: 4px;
    padding-bottom: 20px;
    padding-top: 15px;
    position: relative;
    width: 75%;
    margin-left:45px;
}
.dashboard-pakagealert-statusvalue1,.dashboard-pakagealert-statusvalue2{
	padding-left: 15px;
}
.storesetting-packagealert-td{
	margin-left: 20px;
}

		</style>
