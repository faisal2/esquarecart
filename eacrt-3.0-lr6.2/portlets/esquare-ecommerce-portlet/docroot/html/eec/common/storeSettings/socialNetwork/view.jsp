<%
	String tabs = ParamUtil.getString(request, "tabs", "facebook");
	String redirect = ParamUtil.getString(request, "redirect");
	boolean appEnabled = false;
	String appId = StringPool.BLANK;
	String appSecret = StringPool.BLANK;
	long companyId = themeDisplay.getCompanyId();
	appEnabled = SocialNetworkConnectUtil.isAppEnabled(companyId, tabs);
	appId = SocialNetworkConnectUtil.getAppId(companyId, tabs);
	appSecret = SocialNetworkConnectUtil.getAppSecret(companyId, tabs);
	String facebookAuthRedirectURL = themeDisplay.getPortalURL()
			+ "/c/login/facebook_connect_oauth";
	String url = "https://"	+ ((tabs.equalsIgnoreCase("twitter")) ? "dev." : "developers.") + tabs + ".com";
	url = "<a  target=_blank href=" + url + ">create application in " + tabs + "</a>";
%>


<liferay-portlet:actionURL var="configurationURL" />
<div class="dashboard-maindiv2">
<div style="font-size: large;font-family: helveticaneue;margin-left: 20px;color:#1a1a1a;">Social Network</div>
<hr style="margin-top: 13px;">
	<aui:form action="<%=configurationURL%>" method="post" name="fm">
		<aui:input name="tabs" type="hidden" value="<%=tabs%>" />
		<aui:input name="<%=EECConstants.CMD_SETTING%>" type="hidden"
			value="<%=EECConstants.SOCIAL_NETWORK%>" />

		<liferay-portlet:renderURL portletConfiguration="true"
			var="portletURL">
			<portlet:param name="tabs" value="<%=tabs%>" />
			<portlet:param name="redirect" value="<%=redirect%>" />
		</liferay-portlet:renderURL>
		<%-- <liferay-ui:tabs names="facebook,linkedIn,twitter,google" param="tabs"
			url="<%=portletURL.toString()%>" /> --%>
		<liferay-ui:tabs names="facebook,google" param="tabs"
			url="<%=portletURL.toString()%>" />
		<aui:fieldset style="margin-left: 20px;">
		
			<c:choose>
				<c:when test='<%= tabs!=null && tabs.equals("google") %>'>
					<%
						boolean googleAuthEnabled = PrefsPropsUtil.getBoolean(company.getCompanyId(), "google-auth-enabled", true);
						String googleClientId = PrefsPropsUtil.getString(company.getCompanyId(), "google-client-id");
						String googleClientSecret = PrefsPropsUtil.getString(company.getCompanyId(), "google-client-secret");
					%>

					<aui:input label="enabled" name='<%= "settings--google-auth-enabled--" %>' type="checkbox" value="<%= googleAuthEnabled %>" />
				
					<aui:input label="google-client-id" name='<%= "settings--google-client-id--" %>' type="text" value="<%= googleClientId %>" wrapperCssClass="lfr-input-text-container" />
				
					<aui:input label="google-client-secret" name='<%= "settings--google-client-secret--" %>' type="text" value="<%= googleClientSecret %>" wrapperCssClass="lfr-input-text-container" />
				</c:when>
				
				<c:otherwise>
					<aui:input label='<%=tabs + " enabled"%>'
				name='<%="settings--" + tabs + ".connect.auth.enabled--"%>'
				type="checkbox" value="<%=appEnabled%>" helpMessage="<%=url%>" />
			<aui:input label='<%=tabs + " app Id"%>'
				name='<%="settings--" + tabs + ".connect.app.id--"%>' type="text"
				value="<%=appId%>">
				<aui:validator name="custom" errorMessage='<%="please enter "+tabs+" app id"%>' >
		function (val, fieldNode, ruleValue) {
				var isEnabled = document.getElementById('<portlet:namespace /><%=tabs + ".connect.auth.enabledCheckbox"%>').checked; 
				return !(isEnabled&&val.length == 0);
				}
		        </aui:validator>
			</aui:input>
			<aui:input label='<%=tabs + " app secret"%>'
				name='<%="settings--" + tabs + ".connect.app.secret--"%>'
				type="text" value="<%=appSecret%>">
				<aui:validator name="custom" errorMessage='<%="please enter "+tabs+" app secret"%>' >
		function (val, fieldNode, ruleValue) {
				var isEnabled = document.getElementById('<portlet:namespace /><%=tabs + ".connect.auth.enabledCheckbox"%>').checked; 
				return !(isEnabled&&val.length == 0);
				}
		          </aui:validator>
			</aui:input>
			<c:if test='<%=tabs.equalsIgnoreCase("facebook")%>'>
				<aui:input
					name='<%="settings--" + tabs
								+ ".connect.oauth.redirect.url--"%>'
					type="hidden" value="<%=facebookAuthRedirectURL%>" />
			</c:if>
				</c:otherwise>
			</c:choose>
			
		</aui:fieldset>
		<div class="dashboard-buttons">
			<aui:button-row>
				<aui:button type="submit" style="margin-left: 20px"/>
			</aui:button-row>
		</div>
	</aui:form>
</div>
