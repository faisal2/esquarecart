<%@ include file="/html/eec/common/storeSettings/init.jsp" %>
<%
themeDisplay.setIncludePortletCssJs(true); 
PortletURL actionURL = renderResponse.createActionURL();
String redirect = ParamUtil.getString(request, "redirect");
String countryId = ParamUtil.getString(request, "countryId");
%>
<portlet:resourceURL var="resouceURL" />
<aui:form action="<%=actionURL %>" method="post" name="frm"
				onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(true);" %>'>
<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
<aui:input type="hidden" name="country" value="<%= countryId %>" />
<aui:input type="hidden" name="companyId" id="companyId" value="<%=themeDisplay.getCompanyId()%>" />
<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.SHIPPING_SETTING %>" />

<div id="rangesValMsg" style="display: none;" class="hide">
		<span style="color:red;">This Price Ranges is already Exist</span>
</div>
	
<aui:input type="text" name="name" id="shippingName" label='Name<span style="color:red;font-size: 14px;">*</span>'>
<aui:validator name="required" errorMessage="Name field is required" />
<aui:validator name="custom"  errorMessage="Please enter alpha only">
        function (val, fieldNode, ruleValue) {
                var returnValue = true;
                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>?0123456789";
                for (var i = 0; i < val.length; i++) {
                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
                     returnValue = false;
                    }
                }
                return returnValue;
        }
    </aui:validator>
</aui:input>
<!-- <div id="nameValidateMsg" class="hide">
							<span style="color:red;">Shipping Name is already Exist</span>
</div> -->
<aui:select name="criteriaType" label="Criteria Type" onChange='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
<aui:option value="<%=PortletPropsValues.SHIPPING_WEIGHT_CRITERIA %>"><%=PortletPropsValues.SHIPPING_WEIGHT_CRITERIA %></aui:option>
<aui:option value="<%=PortletPropsValues.SHIPPING_PRICE_CRITERIA %>"><%=PortletPropsValues.SHIPPING_PRICE_CRITERIA %></aui:option>
</aui:select>
<aui:input type="text" name="rangeFrom" label='Range From<span style="color:red;font-size: 14px;">*</span>' onblur='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
<aui:validator name="required" errorMessage="Range From field is required" />
<aui:validator name="custom" errorMessage="Please enter value greater than Zero">
		function (val, fieldNode, ruleValue) {
		var numpattern=/^-?\d*(\.\d+)?$/;
		if(val == 0||val == 0.0 || !numpattern.test(val)){
		return false;
		}
		return true;
		}
</aui:validator>
</aui:input>
<aui:input type="text" name="rangeTo" label='Range To<span style="color:red;font-size: 14px;">*</span>' onblur='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
<aui:validator name="required" errorMessage="Range To field is required" />
<aui:validator name="number" errorMessage="Range To value is Invalid" />
<aui:validator name="custom" errorMessage="Please enter value greater than Range From ">
		function (val, fieldNode, ruleValue) {
var rangeFrom =A.one('#<portlet:namespace/>rangeFrom').val();

if(parseInt(val) <= parseInt(rangeFrom)){
return false;
}
return true;
}
		</aui:validator>
</aui:input>
<aui:input type="text" name="price" label='Shipping Amount<span style="color:red;font-size: 14px;">*</span>'>
<aui:validator name="required"  />
</aui:input><span id ="errorsmsg"/>
<%-- <aui:button-row>
<span class="dashboard-buttons">	<aui:button type="submit" value="Add" /></span>
	 <aui:button type="button" value="cancel" name="close"/> 
	</aui:button-row> --%>
<aui:button-row>
<span class="dashboard-buttons"><aui:button type="submit" value="Add" name="submitShipRate"/></span>
<aui:button  value="cancel" name="cancel" onClick="closewindow();"/>
</aui:button-row>
</aui:form>

<script>
   	jQuery(".close-content yui3-widget btn-content").click(function () {
   		alert()
	    		window.location.reload();
	        });
   	
    $("#<portlet:namespace/>price").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message
	        $("#errorsmsg").html("Digits Only").show().fadeOut("slow");
	               return false;
	    }
	   });
   	function closewindow(){
   		location.reload();
   	}
   	
</script> 
<aui:script use="aui-io-request-deprecated">
	Liferay.provide(window, '<portlet:namespace />updateShippingRate', function(issubmit) { 
			var companyId= document.getElementById('<portlet:namespace />companyId').value;
			var countryId= document.getElementById('<portlet:namespace />country').value;
			var criteriaType = document.getElementById('<portlet:namespace />criteriaType').value;
			var rangeFrom = document.getElementById('<portlet:namespace />rangeFrom').value;
			var rangeTo = document.getElementById('<portlet:namespace />rangeTo').value;
			var price = document.getElementById('<portlet:namespace />price').value;
			if((companyId>0) &&(criteriaType!='null') &&(rangeFrom>0.0) &&(rangeTo>0.0) &&(price>=0.0)){
			var url = '<%=resouceURL.toString()%>';
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />companyId: companyId,
							<portlet:namespace />countryId:countryId,
							<portlet:namespace />criteriaType: criteriaType,
							<portlet:namespace />rangeFrom: rangeFrom,
							<portlet:namespace />rangeTo: rangeTo,
							<portlet:namespace /><%=EECConstants.CMD%>: "<%=EECConstants.SHIPPING_SETTING%>",
							<portlet:namespace /><%=EECConstants.CMD1%>: "RANGES",
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function(event, id, obj) {
								var country = this;
								
								//JSON Data coming back from Server
								var message = country.get('responseData').retVal;
								if (message) {
									A.one('#rangesValMsg').show();
								}
								else {
									A.one('#rangesValMsg').hide();
									if(issubmit){
									 submitForm(document.<portlet:namespace />frm); 
									 Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');
									}
								}
							}
							
						}
					}
					
				); //END of io Request
				}
	},
		['aui-io-deprecated']
	);  
	
</aui:script>
<style>
.aui button.close, .aui button.btn.close{
display:none;
}
</style>