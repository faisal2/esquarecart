<%
String tabs = ParamUtil.getString(request, "tabs", "SMS");
String redirect = ParamUtil.getString(request, "redirect");
PortletPreferences preference= PrefsPropsUtil.getPreferences(themeDisplay.getCompanyId());

String smsSubject = StringPool.BLANK;
String smsSubjectPref = StringPool.BLANK;
String userName = StringPool.BLANK;
String password = StringPool.BLANK;

List<SMSConfiguration> smsConfigurationList = SMSConfigurationLocalServiceUtil.findByCompanyId(themeDisplay.getCompanyId());

for (SMSConfiguration smsConfiguration : smsConfigurationList) {
	if(smsConfiguration.getSmsType().equalsIgnoreCase(EECConstants.SMS_HHORIZON)){
		userName = smsConfiguration.getUserName();
		password = smsConfiguration.getPassword();
	}else if(smsConfiguration.getSmsType().equalsIgnoreCase(EECConstants.VIANET)){
		userName = smsConfiguration.getUserName();
		password = smsConfiguration.getPassword();
	}
}

smsSubjectPref = PrefsParamUtil.getString(preference, request, tabs+".smsConfirmation.subject");

if(!tabs.equals("SMS")){
	if(Validator.isNotNull(smsSubjectPref)){
		smsSubject = ContentUtil.get(PortletProps.get(tabs+".smsConfirmation.subject"));;
	}else{
		smsSubject = ContentUtil.get(PortletProps.get(tabs+".smsConfirmation.subject"));
	}
}
%>

<% if(SessionMessages.contains(renderRequest.getPortletSession(),"SMS-send-success")){%>
<liferay-ui:success key="SMS-send-success" message="SMS has been sent successfully!!!." />
<%} %>
<% if(SessionErrors.contains(renderRequest.getPortletSession(),"SMS-send-error")){%>
<liferay-ui:error key="SMS-send-error" message="There is problem in Request URL to send SMS." />
<%} %>

<liferay-portlet:actionURL var="configurationURL" />

<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle">SMS Configuration</div>
<hr>
<aui:form action="<%= configurationURL %>" method="post" name="fm">
<aui:input name="tabs" type="hidden" value="<%= tabs %>" />
<aui:input name="<%= EECConstants.CMD_SETTING%>" type="hidden"  value="<%= EECConstants.SMS%>" />						

<liferay-portlet:renderURL portletConfiguration="true" var="portletURL">
	<portlet:param name="tabs" value="<%= tabs %>" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</liferay-portlet:renderURL>

<span class="dashboardsetting-emailtab" >
<liferay-ui:tabs
	names="SMS,CreateAccount,SendCoupon,Order,Shipping,Cancel,Return,Delivered,CancelReq,SubscribedUser" 
	param="tabs"
	url="<%= portletURL.toString() %>"
  >
</liferay-ui:tabs>
</span>

<c:choose>
		<c:when test='<%= tabs.equals("SMS") %>'>
			<aui:fieldset>
				<aui:select name="type" id="type" label ="Select type" >
						<aui:option  selected="true" value="<%= EECConstants.SMS_HHORIZON %>"><%=EECConstants.SMS_HHORIZON %></aui:option>
						<aui:option  value="<%= EECConstants.VIANET %>"><%= EECConstants.VIANET %></aui:option>
						<aui:option  value="<%= EECConstants.BULKSMS %>"><%= EECConstants.BULKSMS %></aui:option>
				</aui:select>
				<aui:input name="userName" id="userName" label="User Name" value="<%= userName %>" />
				<aui:input name="password" id="password" label="Password" value="<%= password %>" />
			</aui:fieldset>
		</c:when>
		
		<c:otherwise>
			<aui:fieldset>
				<aui:input cssClass="lfr-input-text-container" label="Message" name="textMessage" type="textarea" value="<%= smsSubject %>" />
			</aui:fieldset>
		</c:otherwise>
</c:choose>

<div class="dashboard-buttons">
<aui:button-row>
	<aui:button type="submit" style="margin-left: 12px;" />
</aui:button-row>
</div>
</aui:form>
</div>
