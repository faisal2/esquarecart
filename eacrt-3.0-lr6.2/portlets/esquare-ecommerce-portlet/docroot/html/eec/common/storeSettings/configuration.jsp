<%@page import="com.esquare.ecommerce.util.EECConstants"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>

<%
String ctxPath = request.getContextPath();
if (ctxPath != null && ctxPath.length()==1) {
	ctxPath = "";
}
%>

<%@page import="com.liferay.portal.kernel.util.PrefsParamUtil"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<script src="http://code.jquery.com/jquery-latest.js" type="text/javascript"></script>

<portlet:defineObjects />
<%
	PortletPreferences preferences = null;
   	String portletResource = ParamUtil.getString(request, "portletResource");

    if (Validator.isNotNull(portletResource)) {
		preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
	}
	String type = PrefsParamUtil.getString(preferences, renderRequest, "type");

%>


<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />

<aui:form action="<%= configurationURL %>" method="post" name="fm">
	<input type="hidden" name="<portlet:namespace />type" value="<%= type %>" id="typeId"/>
	
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.GENERAL%>" id="general_radio" <%= (type.equals(EECConstants.GENERAL))? "checked":"" %>/> <%=EECConstants.GENERAL%> </br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.SHIPPING%>" id="shipping_radio" <%= (type.equals(EECConstants.SHIPPING))? "checked":"" %>/> <%=EECConstants.SHIPPING%></br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.TAXES%>" id="taxes_radio" <%= (type.equals(EECConstants.TAXES))? "checked":"" %> /> <%=EECConstants.TAXES%></br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.EMAIL%>" id="email_radio" <%= (type.equals(EECConstants.EMAIL))? "checked":"" %>/> <%=EECConstants.EMAIL%></br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.PACKAGE_ALERT%>" id="packageAlert_radio" <%= (type.equals(EECConstants.PACKAGE_ALERT))? "checked":"" %>/> <%=EECConstants.PACKAGE_ALERT%></br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.SOCIAL_NETWORK%>" id="socialNetwork_radio" <%= (type.equals(EECConstants.SOCIAL_NETWORK))? "checked":"" %>/> <%=EECConstants.SOCIAL_NETWORK%></br>
	<input type="radio" name="<portlet:namespace />radio" value="<%= EECConstants.SMS%>" id="sms_radio" <%= (type.equals(EECConstants.SMS))? "checked":"" %>/> <%=EECConstants.SMS%></br>	
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("input:radio[name=<portlet:namespace />radio]").click(function() {
		    var value = jQuery(this).val();
		    document.getElementById("typeId").value = value;
		});
		
});
</script>