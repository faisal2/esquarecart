<%@ include file="/html/eec/common/storeSettings/init.jsp" %>
<%
PortletURL actionURL = renderResponse.createActionURL();
String redirect = ParamUtil.getString(request, "redirect");
List<Country> countrylist=CountryServiceUtil.getCountries();
%>
<portlet:resourceURL var="resouceURL" />
<aui:form action="<%=actionURL %>" method="post" name="shippingfrm">
<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
<aui:input type="hidden" name="companyId" id="companyId" value="<%=themeDisplay.getCompanyId()%>" />
<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.SHIPPING_SETTING %>" />
<aui:input type="hidden" name="<%= EECConstants.CMD%>" value="<%= EECConstants.ADD_COUNTRY %>" />
<aui:select  label='Country<span style="color:red;font-size: 14px;">*</span>' name="country" id="country" onblur="validateCountry();">
		<aui:option value=""></aui:option>	
		<%for(Country country:countrylist){%>
			<aui:option value="<%=country.getA2()%>"><%=country.getName()%></aui:option>	
		<%} %>
</aui:select>
<span id="countryValidateMsg" style="color:red;display:none">Country field is required</span>
	<div id="countryValMsg" class="hide">
		<span style="color:red;">Country is already Exist</span>
	</div>
	
<%-- <aui:button-row> --%>
<div class="sub">
	  <aui:button id="submitCountry" type="button" value="Add"  name="submitCountry" /> 
	 </div>
<%-- </aui:button-row> --%>
</aui:form>

<aui:script use="aui-io-request-deprecated">
            
            var myDivA = A.one('#countryValMsg');
            A.one('#<portlet:namespace />country').on('change', function() {
				var countryName = document.getElementById('<portlet:namespace />country').value;
				var companyId= document.getElementById('<portlet:namespace />companyId').value;
				var url = '<%=resouceURL.toString()%>';
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />countryName: countryName,
							<portlet:namespace />companyId: companyId,
							<portlet:namespace /><%=EECConstants.CMD%>: "<%=EECConstants.SHIPPING_SETTING%>",
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function(event, id, obj) {
								var country = this;
								
								//JSON Data coming back from Server
								var message = country.get('responseData').retVal;
								
								if (message) {
									myDivA.show();
								}
								else {
									myDivA.hide();
								}
							}
							
						}
					}
					
				); //END of io Request
			
		},
		['aui-io-deprecated']
	);  
	
	
	 A.one('#<portlet:namespace />submitCountry').on('click', function() {
		  	var a1Check  = jQuery(document.getElementById('countryValMsg')).prop('class');
		  	var country=document.getElementById('<portlet:namespace />country').value;
		   	if(country==''){
		  		document.getElementById('countryValidateMsg').style.display = 'block'; 
		  	}
		    if((a1Check=='hide') && (country!='')) {
		  		submitForm(document.<portlet:namespace />shippingfrm);
		  	}
		},
		['aui-io-deprecated']			
	); 
			
		
</aui:script>
<script>
jQuery("#closethick").click(function () {
		window.location.reload();
});

function validateCountry(){
	var country=document.getElementById('<portlet:namespace />country').value;
  	if(country!=null){
  		document.getElementById('countryValidateMsg').style.display = 'none';
  	}
}
</script>