<%@ include file="/html/eec/common/storeSettings/init.jsp" %>
<%
	themeDisplay.setIncludePortletCssJs(true); 
	PortletURL actionURL = renderResponse.createActionURL();
	String redirect = ParamUtil.getString(request, "redirect");
	String recId = ParamUtil.getString(request, "recId");
	ShippingRates shipSetting=ShippingRatesLocalServiceUtil.getShippingRates(Long.parseLong(recId));
	List<ShippingRegion> empList = EECUtil.readXMLContent(shipSetting.getXml(),"region");
	boolean isSelectedWeight = false;
	boolean isSelectedPrice = false;
	
%>
<portlet:resourceURL var="resouceURL" />
<aui:form action="<%=actionURL %>" method="post" id="frm" name="frm"
			onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(true);" %>'>
	<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
	<aui:input type="hidden" name="recId" value="<%= recId %>" />
	<aui:input type="hidden" name="country" value="<%= shipSetting.getCountryId() %>" />
	<aui:input type="hidden" name="companyId" id="companyId" value="<%=themeDisplay.getCompanyId()%>" />
	<aui:input type="hidden" name="shippingname" value="<%= shipSetting.getName()%>" id="curShippingName"/>
	<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.SHIPPING_SETTING %>" />
	<div id="rangesValMsg" style="display:none;" class="hide">
		<span style="color:red;">This Price Ranges is already Exist</span>
	</div>
	<aui:input type="text" name="name" label='Name<span style="color:red;font-size: 14px;">*</span>' value="<%= shipSetting.getName()%>" id="editShippingName">
		<aui:validator name="required" errorMessage="Name field is required" />
		<aui:validator name="custom"  errorMessage="Please enter alpha only">
        function (val, fieldNode, ruleValue) {
                var returnValue = true;
                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>?0123456789";
                for (var i = 0; i < val.length; i++) {
                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
                     returnValue = false;
                    }
                }
                return returnValue;
        } 
    </aui:validator>
	</aui:input>
	<!-- <div id="nameValidateMsg" class="hide">
							<span style="color:red;">Shipping Name is already Exist</span>
	</div> -->
	<aui:select name="criteriaType" label="Criteria Type" onChange='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
	<%
	if(shipSetting.getShippingType().equals(PortletPropsValues.SHIPPING_WEIGHT_CRITERIA)){
		isSelectedWeight=true;
	}else if(shipSetting.getShippingType().equals(PortletPropsValues.SHIPPING_PRICE_CRITERIA)){
		isSelectedPrice=true;
	}
	%>
		<aui:option value="<%=PortletPropsValues.SHIPPING_WEIGHT_CRITERIA %>" selected="<%=isSelectedWeight%>"><%=PortletPropsValues.SHIPPING_WEIGHT_CRITERIA %></aui:option>
		<aui:option value="<%=PortletPropsValues.SHIPPING_PRICE_CRITERIA %>"  selected="<%=isSelectedPrice%>"><%=PortletPropsValues.SHIPPING_PRICE_CRITERIA %></aui:option>
	</aui:select>
	<aui:input type="text" name="rangeFrom" label='Range From<span style="color:red;font-size: 14px;">*</span>' value="<%=shipSetting.getRangeFrom() %>" onblur='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
	<aui:validator name="required" errorMessage="Range From field is required" />
	<aui:validator name="custom" errorMessage="Please enter value greater than Zero">
		function (val, fieldNode, ruleValue) {
		var numpattern=/^-?\d*(\.\d+)?$/;
		if(val == 0||val == 0.0 || !numpattern.test(val)){
		return false;
		}
		return true;
		}
	</aui:validator>
	</aui:input>
	<aui:input type="text" name="rangeTo" label='Range To<span style="color:red;font-size: 14px;">*</span>' value="<%=shipSetting.getRangeTo() %>" onblur='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "updateShippingRate(false);" %>'>
	<aui:validator name="required" errorMessage="Range To field is required" />
	<aui:validator name="number" errorMessage="Range To value is Invalid" />
	<aui:validator name="custom" errorMessage="Please enter value greater than Range From ">
		function (val, fieldNode, ruleValue) {
			var rangeFrom =A.one('#<portlet:namespace/>rangeFrom').val();
				if(parseInt(val) <= parseInt(rangeFrom)){
					return false;
					}
					return true;
					}
		</aui:validator>
	</aui:input>
	<aui:input type="hidden" name="price" value="<%=shipSetting.getPrice() %>"/>
	<br/><br/>
	<table>
		<tr><td width="10%"></td><td width="40%">States Name</td>
			<td width="10%"></td><td width="30%">Shipping Amount<span style="color:red;font-size: 14px;">*</span></td>
		</tr>
		<%
		for(int i=0;i<empList.size();i++){
	//	for(ShippingRegion emp : empList){%>
			<tr><td></td><td><%=empList.get(i).getName()%></td>
			<td><aui:input type="hidden" value="<%=empList.get(i).getId()%>" name="regionId" size="8"/>
				<aui:input type="hidden" value="<%=empList.get(i).getName()%>" name="regionName"/></td>
				<td><aui:input type="text" value="<%=empList.get(i).getPrice()%>" name='<%= "regionValue" + i%>' size="8" label="">
				<aui:validator name="required" errorMessage="Taxes Rate field is required" />
				<aui:validator name="number" errorMessage="Taxes Rate value is Invalid" />
				</aui:input></td>
			</tr>
		<%} %>
	</table>
	<%-- <aui:button-row>
		<aui:button type="submit" value="Update" />
		<aui:button type="button" value="cancel"/>
	</aui:button-row> --%>
	<aui:button-row>
	<span class="dashboard-buttons"><aui:button type="submit" value="Update" name="submitShipRate"/></span>
	</aui:button-row>
</aui:form>
<script>
   	jQuery("#closethick").click(function () {
	    		window.location.reload();
	        });
</script> 
<aui:script use="aui-io-request-deprecated">
	Liferay.provide(window, '<portlet:namespace />updateShippingRate', function(issubmit) { 
			var companyId= document.getElementById('<portlet:namespace />companyId').value;
			var countryId= document.getElementById('<portlet:namespace />country').value;
			var recId = document.getElementById('<portlet:namespace />recId').value;
			var criteriaType = document.getElementById('<portlet:namespace />criteriaType').value;
			var rangeFrom = document.getElementById('<portlet:namespace />rangeFrom').value;
			var rangeTo = document.getElementById('<portlet:namespace />rangeTo').value;
			var price = document.getElementById('<portlet:namespace />price').value;
			if((companyId>0) && (recId>0) &&(criteriaType!='null') &&(rangeFrom>=0.0) &&(rangeTo>=0.0) &&(price>=0.0)){
			var url = '<%=resouceURL.toString()%>';
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />companyId: companyId,
							<portlet:namespace />countryId:countryId,
							<portlet:namespace />recId: recId,
							<portlet:namespace />criteriaType: criteriaType,
							<portlet:namespace />rangeFrom: rangeFrom,
							<portlet:namespace />rangeTo: rangeTo,
							<portlet:namespace /><%=EECConstants.CMD%>: "<%=EECConstants.SHIPPING_SETTING%>",
							<portlet:namespace /><%=EECConstants.CMD1%>: "RANGES",
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function(event, id, obj) {
								var country = this;
								
								//JSON Data coming back from Server
							var	message = country.get('responseData').retVal;
								
								if (message) {
									A.one('#rangesValMsg').show();
								}
								else {
									A.one('#rangesValMsg').hide();
									if(issubmit){
									submitForm(document.<portlet:namespace />frm);
									Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');
									}
								}
							}
							
						}
					}
					
				); //END of io Request
				}
	},
		['aui-io-deprecated']
	);  
</aui:script>
