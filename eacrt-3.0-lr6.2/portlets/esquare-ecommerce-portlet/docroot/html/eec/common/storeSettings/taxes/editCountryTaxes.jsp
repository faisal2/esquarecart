<%@ include file="/html/eec/common/storeSettings/init.jsp" %>
<%
	themeDisplay.setIncludePortletCssJs(true); 
	PortletURL actionURL = renderResponse.createActionURL();
	String redirect = ParamUtil.getString(request, "redirect");
	String recId = ParamUtil.getString(request, "recId");
	TaxesSetting taxesSetting=TaxesSettingLocalServiceUtil.getTaxesSetting(Long.parseLong(recId));
	List<ShippingRegion> shipRegionlist = EECUtil.readXMLContent(taxesSetting.getXml(),"region");
	boolean isSelectedWeight = false;
	boolean isSelectedPrice = false;
	
%>

<aui:form action="<%=actionURL %>" method="post" id="frm" name="frm">
	<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
	<aui:input type="hidden" name="recId" value="<%= recId %>" />
	<aui:input type="hidden" name="country" value="<%= taxesSetting.getCountryId() %>" />
	<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.TAXES_SETTING%>" />
	<aui:input type="hidden" name="<%= EECConstants.CMD%>" value="<%= EECConstants.EDIT%>" />
	<br/><br/>
	<aui:input type="text" name="name" label='Name<span style="color:red;font-size: 14px;">*</span>' value="<%= taxesSetting.getName()%>">
	<aui:validator name="required" errorMessage="Name field is required" />
	<aui:validator name="custom"  errorMessage="Please enter alpha only">
        function (val, fieldNode, ruleValue) {
                var returnValue = true;
                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>?0123456789";
                for (var i = 0; i < val.length; i++) {
                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
                     returnValue = false;
                    }
                }
                return returnValue;
        }
    </aui:validator>
	</aui:input>
	
	<aui:input type="hidden" name="price" value="<%=taxesSetting.getTaxRate() %>"/>
	<br/><br/>
	
	<table>
		<tr><td width="10%"></td><td width="30%"><b>States Name</b></td>
			<td width="20%"></td><td width="40%"><b>Tax Rate<span style="color:red;font-size: 14px;">*</span></b></td>
		</tr>
		
		<%
		for(int i=0;i<shipRegionlist.size();i++){
		//for(ShippingRegion region : shipRegionlist){%>
			<tr><td></td><td><%=shipRegionlist.get(i).getName()%></td>
			<td>
				<aui:input type="hidden" value="<%=shipRegionlist.get(i).getId()%>" name="regionId"></aui:input>
				<aui:input type="hidden" value="<%=shipRegionlist.get(i).getName()%>" name="regionName"></aui:input>
			</td>
			
			<td>
				<aui:input type="text" value="<%=shipRegionlist.get(i).getPrice()%>" name='<%= "regionValue" + i%>' size="8" label="">
					<aui:validator name="required" errorMessage="Taxes Rate field is required" />
					<aui:validator name="number" errorMessage="Taxes Rate value is Invalid" />
					<aui:validator name="range" >
								[0,100]
					</aui:validator>
				</aui:input>
			</td>
			</tr>
		<%} %>
	</table>
	<div class="dashboard-buttons">
	<aui:button-row>
		<aui:button type="submit" value="Update" />
		<%-- <aui:button type="button" value="cancel"/> --%>
	</aui:button-row>
	</div>
</aui:form>
<script>
   	jQuery("#closethick").click(function () {
	    		window.location.reload();
	        });
</script> 