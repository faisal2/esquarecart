<%
String tabs = ParamUtil.getString(request, "tabs", "Email");
String redirect = ParamUtil.getString(request, "redirect");
PortletPreferences preference= PrefsPropsUtil.getPreferences(themeDisplay.getCompanyId());
String emailFromName =  StringPool.BLANK;
String emailFromAddress = StringPool.BLANK;
String emailToName = StringPool.BLANK;
String emailToAddress = StringPool.BLANK;
String emailsubject = StringPool.BLANK;
String editorContent = StringPool.BLANK;
String editorContentPref = StringPool.BLANK;
String emailsubjectPref = StringPool.BLANK;



if(tabs.equals("CreateAccount")){
	editorContentPref = PrefsPropsUtil.getContent(themeDisplay.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_BODY);
	emailsubjectPref =  PrefsPropsUtil.getContent(themeDisplay.getCompanyId(), PropsKeys.ADMIN_EMAIL_USER_ADDED_SUBJECT);
}else{
	emailsubjectPref = PrefsParamUtil.getString(preference, request, tabs+".emailConfirmation.subject");	
	editorContentPref = PrefsParamUtil.getString(preference, request, tabs+".emailConfirmation.body");	

}

emailFromName = EECUtil.getEmailName(request, themeDisplay.getCompanyId(),PropsKeys.ADMIN_EMAIL_FROM_NAME);
emailFromAddress = EECUtil.getEmailAddress(request, themeDisplay.getCompanyId(),PropsKeys.ADMIN_EMAIL_FROM_ADDRESS);
emailToName=EECUtil.getEmailName(request, themeDisplay.getCompanyId(),EECConstants.ADMIN_EMAIL_TO_NAME);
emailToAddress=EECUtil.getEmailAddress(request, themeDisplay.getCompanyId(),EECConstants.ADMIN_EMAIL_TO_ADDRESS);

%>

<liferay-portlet:actionURL var="configurationURL" />
<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle">Email Configuration</div>
<hr id="top1">
<aui:form action="<%= configurationURL %>" method="post" name="fm" onSubmit='<%= "event.preventDefault(); " + renderResponse.getNamespace() + "saveConfiguration();" %>'>
<aui:input name="tabs" type="hidden" value="<%= tabs %>" />
<aui:input name="<%= EECConstants.CMD_SETTING%>" type="hidden"  value="<%= EECConstants.EMAIL%>" />						

<liferay-portlet:renderURL portletConfiguration="true" var="portletURL">
	<portlet:param name="tabs" value="<%= tabs %>" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</liferay-portlet:renderURL>
<span class="dashboardsetting-emailtab" >
<liferay-ui:tabs
	names="Email,CreateAccount,SendCoupon,Order,Shipping,Cancel,Return,Delivered,CancelReq,SubscribedUser" 
	param="tabs"
	url="<%= portletURL.toString() %>"
  >
</liferay-ui:tabs>
</span>

<c:choose>
		<c:when test='<%= tabs.equals("Email") %>'>
			<aui:fieldset style="margin-left: 20px;">
				<div id="dbsetting-emailconfgsr">Sender:</div>
				<aui:input cssClass="lfr-input-text-container" label="name" name="emailFromName" type="text" value="<%= emailFromName %>" />
				<aui:input cssClass="lfr-input-text-container" label="address" name="emailFromAddress" type="text" value="<%= emailFromAddress %>" />				
			</aui:fieldset>
			<br>
			<aui:fieldset style="margin-left: 20px;">
				<div id="dbsetting-emailconfgsr">Receiver:</div>
				<aui:input cssClass="lfr-input-text-container" label="name" name="emailToName" type="text" value="<%= emailToName %>" />
				<aui:input cssClass="lfr-input-text-container" label="address" name="emailToAddress" type="text" value="<%= emailToAddress %>" />				
			</aui:fieldset>
		</c:when>
		
		<c:otherwise>
			<%		
			
			StringBuffer sb = new StringBuffer();
			String mailContent = StringPool.BLANK;
			
			sb.append(ContentUtil.get(PortletProps.get(tabs+".details.table.header")));
			sb.append(ContentUtil.get(PortletProps.get(tabs+".details.table.content")));
			sb.append(ContentUtil.get(PortletProps.get(tabs+".details.table.footer")));
			
			if(Validator.isNotNull(emailsubjectPref)){
				emailsubject = emailsubjectPref;
			}else{
				emailsubject = ContentUtil.get(PortletProps.get(tabs+".emailConfirmation.subject"));
			}			
			
			if(Validator.isNotNull(editorContentPref)){
				editorContent = editorContentPref;
			}else{
				editorContent = sb.toString();
			}		
			%>
			<aui:fieldset style="margin-left: 20px;margin-right:20px;">
				<aui:input cssClass="lfr-input-text-container" label="subject" name="emailsubject" type="text" value="<%= emailsubject %>" />		
				<liferay-ui:input-editor editorImpl="<%= EDITOR_WYSIWYG_IMPL_KEY %>"/>
				<aui:input name="editorContent" type="hidden" value="" />
			</aui:fieldset>
		</c:otherwise>
</c:choose>


<aui:button-row>
	<aui:button type="submit" style="margin-left: 20px;" />
</aui:button-row>

</aui:form>
</div>
<aui:script>
	function <portlet:namespace />initEditor() {
		return "<%= UnicodeFormatter.toString(editorContent) %>";
	}	
	
	Liferay.provide(
		window,
		'<portlet:namespace />saveConfiguration',
		function() {
		
		<c:if test='<%= !tabs.equals("Email") %>'>
			document.<portlet:namespace />fm.<portlet:namespace />editorContent.value = window.<portlet:namespace />editor.getHTML();
		</c:if>
		
			submitForm(document.<portlet:namespace />fm);
		},
		['liferay-util-list-fields']
	);
</aui:script>

<%!
public static final String EDITOR_WYSIWYG_IMPL_KEY = "editor.wysiwyg.portal-web.docroot.html.eec.common.storeSettings.email.view.jsp";
%>