<%@ include file="/html/eec/common/storeSettings/init.jsp" %>

<%
	String type = PrefsParamUtil.getString(preferences, renderRequest, "type");
%>
	<c:if test="<%=type.equals(EECConstants.GENERAL) %>">
	<%@ include file="/html/eec/common/storeSettings/general/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.SHIPPING)%>">
	<%@ include file="/html/eec/common/storeSettings/shipping/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.TAXES)%>">
	<%@ include file="/html/eec/common/storeSettings/taxes/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.EMAIL)%>">
	<%@ include file="/html/eec/common/storeSettings/email/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.SMS)%>">
	<%@ include file="/html/eec/common/storeSettings/sms/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.PACKAGE_ALERT)%>">
	<%@ include file="/html/eec/common/storeSettings/packageAlert/view.jsp"%>
	</c:if>
	<c:if test="<%=type.equals(EECConstants.SOCIAL_NETWORK)%>">
	<%@ include file="/html/eec/common/storeSettings/socialNetwork/view.jsp"%>
	</c:if>
	
	
<style>
/*aui popup modification*/
.panel-hd{
  background: url('<%=themeDisplay.getPathThemeImages()%>/eec/buttonbg.png');
  }
  .panel-hd-text {
  color: #FFFFFF;
  text-shadow: 0 0 #FFFFFF;
  }
  .dialog-content{
  padding:1px; 
  border-color: #A72A30;
  border-radius: 6px;
  border-width: 2px;
  }
</style>	