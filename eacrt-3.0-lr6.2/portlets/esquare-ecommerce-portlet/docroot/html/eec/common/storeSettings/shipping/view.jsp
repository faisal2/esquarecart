<%
themeDisplay.setIncludePortletCssJs(true);
String redirect = PortalUtil.getCurrentURL(request);
List<ShippingRates> shipSettinglist=ShippingRatesLocalServiceUtil.getShippingRatesDetails(themeDisplay.getCompanyId());
PortletURL actionURL = renderResponse.createActionURL();
%>

<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle">Shipping</div>
<hr id="top1">
<aui:form method="post" action="<%=actionURL %>" name="fm">
	<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.SHIPPING_SETTING %>" />
	<input type="hidden" id="redirect" name="redirect" value="<%= redirect %>" /> 
	<input type="button" onclick="javascript:addCountryPopup();" value="Add Country" id="addcountry" style="margin-left: 20px;"/>
<%
if(Validator.isNotNull(shipSettinglist)){
	for(ShippingRates shipSettingobj:shipSettinglist){
	List<ShippingRates> shipCountrylist=ShippingRatesLocalServiceUtil.findByCountry(themeDisplay.getCompanyId(),shipSettingobj.getCountryId());
	Country countryobj=CountryServiceUtil.getCountryByA2(shipSettingobj.getCountryId());
%>
	<br/>
	<br>
	<table width="100%" style="background-color:#45a5d5;color: white">
		<tr>
		<td width="60%">
		<span style="margin-left: 10px;font-family: myfont;font-size: large;text-transform: capitalize">
		<%=countryobj.getName()%></span></td>
		<td width="20%"></td>
		<td width="20%">
		<input type="button" value="Add Shipping Rate" id="addshipping" onclick="javascript:addShippingRatePopup('<%=shipSettingobj.getCountryId()%>');" /></td>
		</tr>
	</table>

	<liferay-ui:search-container emptyResultsMessage="There is No Shipping Records in this country" delta="20">
		<liferay-ui:search-container-results total="<%=shipCountrylist.size()%>"
		results="<%=ListUtil.subList(shipCountrylist, searchContainer.getStart(),
						searchContainer.getEnd())%>" />
		<liferay-ui:search-container-row className="com.esquare.ecommerce.model.ShippingRates" keyProperty="recId" modelVar="shippingRates">
						
				<liferay-ui:search-container-column-text 
				name="Shipping Name" property="name" />
				
				<liferay-ui:search-container-column-text 
				name="Shipping Type" property="shippingType" />
				
				<liferay-ui:search-container-column-text 
				name="Range From" property="rangeFrom" />
				
				<liferay-ui:search-container-column-text 
				name="Range To" property="rangeTo" />
				
				<%
				String editURL="javascript:editShippingRatePopup('"+shippingRates.getRecId()+"');";
				%>
				<liferay-ui:search-container-column-text 
				name="" href="<%= editURL %>" value="Edit"/>
			
				<%
				String deleteURL="javascript:deleteShippingRate('"+shippingRates.getRecId()+"');";
				%>
				<liferay-ui:search-container-column-text 
				name="" href="<%= deleteURL %>" value="Delete"/>
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	</liferay-ui:search-container>
<%}}else{%>
<p style="font-weight:bold;">There is No Shipping Records</p>
<%}%>
</aui:form>
</div>
<portlet:renderURL var="addCountryURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/storeSettings/shipping/addCountry.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="addShippingURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/storeSettings/shipping/addShippingRate.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="editShippingURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/storeSettings/shipping/editShippingRate.jsp"/>
</portlet:renderURL>
<script type="text/javascript">

function addCountryPopup() 
{
	var redirect = document.getElementById("redirect").value;
	var url = '<%= addCountryURL.toString() %>&redirect='+redirect;
    AUI().use('liferay-util-window', 'aui-io-deprecated', 'event', 'event-custom', function(A) {
        var dialog = Liferay.Util.Window.getWindow(
			{
			dialog: {
                centered: true,
                draggable: true,
                modal: true,
                width:400,
            },
			title: '<%= UnicodeLanguageUtil.get(pageContext, "Add Country") %>',
			}).plug(A.Plugin.IO, {uri: url}).render();
        
        dialog.show();
    });
}



    function addShippingRatePopup(countryId) 
    {
    	var redirect = document.getElementById("redirect").value;
    	var url = '<%= addShippingURL.toString() %>&redirect='+redirect+'&countryId='+countryId;
        AUI().use('liferay-util-window', 'aui-io-deprecated', 'event', 'event-custom', function(A) {
            var dialog = Liferay.Util.Window.getWindow(
				{
				dialog: {
                    centered: true,
                    draggable: true,
                    modal: true,
                    width:400,
                },
    			title: '<%= UnicodeLanguageUtil.get(pageContext, "add-shipping-rate") %>',
    			}).plug(A.Plugin.IO, {uri: url}).render();
            
            dialog.show();
        });
    }
   
 
    
    function editShippingRatePopup(recId) 
    {
    	var redirect = document.getElementById("redirect").value;
    	var url = '<%= editShippingURL.toString() %>&redirect='+redirect+'&recId='+recId;
        AUI().use('liferay-util-window', 'aui-io-deprecated', 'event', 'event-custom', function(A) {
            var dialog = Liferay.Util.Window.getWindow(
				{
				dialog: {
                    title: 'Edit Shipping Rate',
                    centered: true,
                    draggable: true,
                    modal: true,
                    width:400,
                    height:450,
                },
    			title: '<%= UnicodeLanguageUtil.get(pageContext, "edit-shipping-rate") %>',
    			}).plug(A.Plugin.IO, {uri: url}).render();
            
            dialog.show();
        });
    }
    
   </script>
   
   <aui:script>
   function deleteShippingRate(recId){
     	document.<portlet:namespace />fm.action="<%=actionURL.toString()%>" + "&recId=" + recId+"&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
     	if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are-you-sure-want-to-delete") %>')) {
    		submitForm(document.<portlet:namespace />fm);
    	}
    	}
    
   </aui:script>
   <style>
   #addcountry,#addshipping{
  background: #ffc107 !important;
    border: 0 none !important;
    color: white !important;
    font-family: myfont !important;
    font-size: medium !important;
    height: 38px;
    min-width: 125px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s !important;
   }
   #addcountry:hover,#addshipping:hover{
  background: #CDDC39 !important;
  }
   </style>
   