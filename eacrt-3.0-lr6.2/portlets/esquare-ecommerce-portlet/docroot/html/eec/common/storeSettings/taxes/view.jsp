
<%
//themeDisplay.setIncludeServiceJs(true);
String redirect = PortalUtil.getCurrentURL(request);
List<TaxesSetting> taxesSettinglist=TaxesSettingLocalServiceUtil.getTaxesSettingDetails(themeDisplay.getCompanyId());
PortletURL taxesActionURL = renderResponse.createActionURL();
%>
<div class="dashboard-maindiv2">
<div class="dashboard-headingstyle">Taxes</div>
<hr id="top1">
<aui:form method="post" action="<%=taxesActionURL %>" name="taxViewfm" id="taxViewfm">
	<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.TAXES_SETTING %>" />
	<aui:input name="deleteTaxesRecIds" type="hidden" />
	<input type="hidden" id="redirect" name="redirect" value="<%= redirect %>" /> 
	<input type="button" onclick="javascript:addCountryTaxesPopup();" value="Add Country" id="addcountrytax" style="margin-left: 20px;"/>
	<c:if test="<%=Validator.isNotNull(taxesSettinglist) && (taxesSettinglist.size()>0) %>">
	<input type="button" onClick='<%= renderResponse.getNamespace() + "deleteTaxRecords();" %>' value="Delete" id="deletecountrytax"/>
	</c:if>
	
	
		<liferay-ui:search-container emptyResultsMessage="There is No Taxes Records" delta="10" rowChecker="<%=new RowChecker(renderResponse) %>">
		<liferay-ui:search-container-results total="<%=taxesSettinglist.size()%>"
		results="<%=ListUtil.subList(taxesSettinglist, searchContainer.getStart(),
						searchContainer.getEnd())%>" />
		<liferay-ui:search-container-row modelVar="taxSetting" className="com.esquare.ecommerce.model.TaxesSetting" keyProperty="recId">
				<%
				Country countryobj=CountryServiceUtil.getCountryByA2(taxSetting.getCountryId());
				%>
				
										
				<liferay-ui:search-container-column-text 
				name="Country Name" value="<%= countryobj.getName() %>" />
				
				<liferay-ui:search-container-column-text 
				name="Tax Type" value="<%= taxSetting.getName() %>" />
				<%
				String editURL="javascript:editTaxesRatePopup('"+taxSetting.getRecId()+"');";
				%>
				<liferay-ui:search-container-column-text 
				name="" href="<%= editURL %>" value="Edit"/>
			
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
</liferay-ui:search-container>

</aui:form>

<portlet:renderURL var="addTaxesURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/storeSettings/taxes/addCountryTaxes.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="editTaxesURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>">
<portlet:param name="jspPage" value="/html/eec/common/storeSettings/taxes/editCountryTaxes.jsp"/>
</portlet:renderURL>

</div>
<script type="text/javascript">

function addCountryTaxesPopup() 
{
	var redirect = document.getElementById("redirect").value;
	var url = '<%= addTaxesURL.toString() %>&redirect='+redirect;
    AUI().use('liferay-util-window', 'aui-io-deprecated', 'event', 'event-custom', function(A) {
        var dialog = Liferay.Util.Window.getWindow(
			{
			dialog: {
                centered: true,
                draggable: true,
                modal: true,
                width:300,
            },
			title: '<%= UnicodeLanguageUtil.get(pageContext, "Add Country") %>',
			}).plug(A.Plugin.IO, {uri: url}).render();
        
        dialog.show();
    });
}


function editTaxesRatePopup(recId) 
    {
    	var redirect = document.getElementById("redirect").value;
    	var url = '<%= editTaxesURL.toString() %>&redirect='+redirect+'&recId='+recId;
        AUI().use('liferay-util-window', 'aui-io-deprecated', 'event', 'event-custom', function(A) {
            var dialog = Liferay.Util.Window.getWindow(
{
dialog: {
                    centered: true,
                    draggable: true,
                    modal: true,
                    width:450,
                    height:500,
                },
    			title: '<%= UnicodeLanguageUtil.get(pageContext, "edit-taxe-rate") %>',
    			}).plug(A.Plugin.IO, {uri: url}).render();
            
            dialog.show();
        });
    }
    
</script>

<aui:script>

Liferay.provide(
		window,
		'<portlet:namespace />deleteTaxRecords',
		function() {
					document.<portlet:namespace />taxViewfm.action="<%= taxesActionURL.toString()%>" + "&<%= EECConstants.CMD %>=<%= EECConstants.DELETE %>";
					var checkBoxValue = Liferay.Util.listCheckedExcept(document.<portlet:namespace />taxViewfm, "<portlet:namespace />allRowIds");
					if(checkBoxValue==""||checkBoxValue==null){
							alert("Please select atleast One entry to Delete");
							return false;
					}
					if (confirm('<%= UnicodeLanguageUtil.get(pageContext, "are you sure want to delete") %>')) {
					document.<portlet:namespace />taxViewfm.<portlet:namespace />deleteTaxesRecIds.value=checkBoxValue;
					submitForm(document.<portlet:namespace />taxViewfm);
			}
		},
		['liferay-util-list-fields']
	);

 </aui:script>
 <style>
 #addcountrytax,#deletecountrytax{
  background: #ffc107 !important;
    border: 0 none !important;
    color: white !important;
    font-family: myfont !important;
    font-size: medium !important;
    height: 38px;
    min-width: 125px;
    transition: background 0.3s linear 0s, color 0.3s linear 0s !important;
 }
 #addcountrytax:hover,#deletecountrytax:hover{
background: #CDDC39 !important;
 }
 </style>