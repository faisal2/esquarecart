<%
PortletURL actionURL = renderResponse.createActionURL();
String redirect = ParamUtil.getString(request, "redirect");

Group liveGroup = 	GroupLocalServiceUtil.getGroup(themeDisplay.getCompanyId(),EECConstants.GUEST);
UnicodeProperties groupTypeSettings = null;
if (liveGroup != null) {
	groupTypeSettings = liveGroup.getTypeSettingsProperties();
}
else {
	groupTypeSettings = new UnicodeProperties();
}
String googleAnalyticsId = PropertiesParamUtil.getString(groupTypeSettings, request, "googleAnalyticsId");

String priceRangeType=PortletPropsValues.FILTER_PRICE_CHECKBOX_TYPE;
String priceRangeTypePref=EECUtil.getPreference(themeDisplay.getCompanyId(), "priceRangeType");
if(Validator.isNotNull(priceRangeTypePref)){
	priceRangeType=priceRangeTypePref;
}

String businessdays_min=StringPool.BLANK;
String businessdays_max=StringPool.BLANK;
String businessdays_minPref =EECUtil.getPreference(themeDisplay.getCompanyId(), "businessdays_min");
String businessdays_maxPref =EECUtil.getPreference(themeDisplay.getCompanyId(), "businessdays_max");
if(Validator.isNotNull(businessdays_minPref)||Validator.isNotNull(businessdays_maxPref)){
	businessdays_min=businessdays_minPref;
	businessdays_max=businessdays_maxPref;
}

int similar_product_offset_limit=0;
try{
similar_product_offset_limit =Integer.parseInt(EECUtil.getPreference(themeDisplay.getCompanyId(), EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT));
}catch(NumberFormatException exception){
	similar_product_offset_limit=PortletPropsValues.SIMILAR_PRODUCT_OFFSET_LIMIT;
}
%>

<div class="dashboard-maindiv2">
	<div class="dashboard-headingstyle"> General</div>
	<hr style="margin-top: 13px;">
	<div style="padding-left: 20px;">
<liferay-ui:message key="set-the-google-analytics-id-that-will-be-used-for-this-set-of-pages" />
</div>

<aui:form action='<%= actionURL.toString() %>' name="fm" method="post"  enctype="multipart/form-data">
<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
<aui:input type="hidden" name="<%= EECConstants.CMD%>" value="<%= EECConstants.GENERAL_ADMIN_SETTINGS %>" />
	<div id="db-generalsettingid" class="dashboard-fields-lable " >
	<aui:field-wrapper  label="google-analytics-id">	
		<input name="<portlet:namespace />googleAnalyticsId" size="30" type="text"  value="<%= HtmlUtil.escape(googleAnalyticsId) %>" class="dbsetting-generlinput" />
	</aui:field-wrapper>
	</div>

<div class="dbsetting-generlgoogleid">
<a href="https://www.google.com/analytics/web/#report/" target="_blank" >View Google Analytics  Report</a>

</div>	
<br/>
<div class="dashboard-lineseperator" ><!-- Placeholder --></div>
	<div id="db-generalsettingbrs" class="dashboard-fields-lable" >
		<div class="dashboard-normalfont">Upload Company Logo</div>

				<aui:input label="" name="myImageFile" type="file" showRequiredLabel="false" >
				<aui:validator name="acceptFiles" errorMessage='Please select a image with a valid extension ({0}).'>
                   		'jpeg,png,bmp,gif,jpg'
 	             </aui:validator>
				</aui:input>
	</div>
<br>
<div class="dashboard-lineseperator" ><!-- Placeholder --></div>
	<div id="db-generalsettingprice">
	<aui:layout>
		<aui:column>
		<div class="dashboard-normalfont">Filter Price Range Type</div>
			<aui:select label="" name="settings--priceRangeType--" class="dbsetting-generlselect">
			<aui:option value="">Select</aui:option>
<%-- 			temporarly disabled
<aui:option value="<%=PortletPropsValues.FILTER_PRICE_SLIDER_TYPE%>" selected="<%=priceRangeType.equals(PortletPropsValues.FILTER_PRICE_SLIDER_TYPE) %>"><%=PortletPropsValues.FILTER_PRICE_SLIDER_TYPE%></aui:option> --%>
			<aui:option value="<%=PortletPropsValues.FILTER_PRICE_CHECKBOX_TYPE%>" selected="<%=priceRangeType.equals(PortletPropsValues.FILTER_PRICE_CHECKBOX_TYPE) %>"><%=PortletPropsValues.FILTER_PRICE_CHECKBOX_TYPE%></aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>	
	</div>
	
	<br/>
	<div class="dashboard-lineseperator" ><!-- Placeholder --></div>
		<aui:field-wrapper  label="Delivered Business Days">
			<aui:input name="settings--businessdays_min--" size="30" type="text" label='Min<span style="color:red;font-size: 14px;">*</span>'
					value="<%= (Validator.isNotNull(businessdays_min))?businessdays_min:PortletPropsValues.DELIVER_BUSINESSDAYS_MIN %>" class="dbsetting-generlinput" showRequiredLabel="false" >
				<aui:validator name="required"/>
				<aui:validator name="digits"/>
			</aui:input>
			<aui:input name="settings--businessdays_max--" size="30" type="text" label='Max<span style="color:red;font-size: 14px;">*</span>' 
					value="<%= (Validator.isNotNull(businessdays_max))?businessdays_max:PortletPropsValues.DELIVER_BUSINESSDAYS_MAX %>" class="dbsetting-generlinput" showRequiredLabel="false">
				<aui:validator name="required"/>
				<aui:validator name="digits"/>
				<aui:validator name="custom" errorMessage="Please enter value greater than Min value ">
				function (val, fieldNode, ruleValue) {
					var min =A.one('#<portlet:namespace/>businessdays_min').val();
						if(parseInt(val) <= parseInt(min)){
							return false;
							}
							return true;
							}
				</aui:validator>
			</aui:input><br/>
		</aui:field-wrapper>
		<div class="dashboard-lineseperator" ><!-- Placeholder --></div>
		<aui:field-wrapper  label='Similar Product Offset Limit<span style="color:red;font-size: 14px;">*</span>' >
		<aui:input type="text" name='<%="settings--"+EECConstants.SIMILAR_PRODUCT_OFFSET_LIMIT+"--"%>' value="<%=similar_product_offset_limit%>" size="30" label="">
		<aui:validator name="required" />
		<aui:validator name="number" />
		<aui:validator name="max">
		['100']
		</aui:validator>
		</aui:input><br/>
		</aui:field-wrapper>
		
		<div class="dashboard-lineseperator" ><!-- Placeholder --></div>
		<aui:field-wrapper  label="">
		<aui:select label="currency" name="settings--currencyType--">
					<%
					for (int i = 0; i < EECUtil.CURRENCY_IDS.length; i++) {
						%>
							<aui:option label="<%= EECUtil.CURRENCY_IDS[i] %>" selected="<%=currencyType.equals(EECUtil.CURRENCY_IDS[i]) %>"/>
						<%
					}
					%>
		</aui:select>
		</aui:field-wrapper>
	<aui:button-row>
		<div class="dashboard-buttons">
			<aui:button  type="submit" value="Save" cssClass="couponadd"/>
		</div>
	</aui:button-row>
</aui:form>
</div>
<style>
.dashboard-normalfont{
	color: gray;
    font-family: arial;
    font-size: small;
}
#_storeSettings_WAR_esquareecommerceportlet_INSTANCE_6NHKYmVTJdgu_fm{
padding-left: 20px;
}
.column-content, .column-content-center {
    padding: 0;
}
</style>