<%@ include file="/html/eec/common/storeSettings/init.jsp" %>
<%
PortletURL actionURL = renderResponse.createActionURL();
String redirect = ParamUtil.getString(request, "redirect");
List<Country> countrylist=CountryServiceUtil.getCountries();
%>
<portlet:resourceURL var="resouceURL" />

<aui:form action="<%=actionURL%>" method="post" id="countryFormId" name="countryForm">

	<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
	<aui:input type="hidden" name="companyId" id="companyId" value="<%= String.valueOf(themeDisplay.getCompanyId())%>" />
	<aui:input type="hidden" name="<%= EECConstants.CMD_SETTING%>" value="<%= EECConstants.TAXES_SETTING %>" />
	<aui:input type="hidden" name="<%= EECConstants.CMD%>" value="<%= EECConstants.ADD%>" />
	<br/><br/>
	<aui:select  label='Country<span style="color:red;font-size: 14px;">*</span>' name="country" id="country" onblur="validateCountry();">
		<aui:option value=""></aui:option>	
		<%for(Country country:countrylist){%>
			<aui:option value="<%=country.getA2()%>"><%=country.getName()%></aui:option>	
		<%} %>
	</aui:select>
	<span id="countryValidateMsg" style="color:red;display:none">Country field is required</span>
	<div id="countryValMsg" class="hide">
		<span style="color:red;">Country is already Exist</span>
	</div>
	
	<aui:input type="text" name="name" id="taxName" onblur="validateTaxname();" value="VAT" label='Name<span style="color:red;font-size: 14px;">*</span>'>
		<aui:validator name="required" errorMessage="Name field is required" />
		<aui:validator name="custom"  errorMessage="Please enter alpha only">
        function (val, fieldNode, ruleValue) {
                var returnValue = true;
                var iChars = "~`!@#$%^&*()_+=-[]\\\';,./{}|\":<>?0123456789";
                for (var i = 0; i < val.length; i++) {
                    if (iChars.indexOf(val.charAt(i)) != -1) {                  
                     returnValue = false;
                    }
                }
                return returnValue;
        }
    </aui:validator>
	</aui:input>
	<span id="taxNameValMsg" style="color:red;display:none;">Name field is required</span>
	
	<aui:input type="text" name="taxRate" id="taxRate" onblur="validateTaxrate();" label='TaxRate<span style="color:red;font-size: 14px;">*</span>'>
		<aui:validator name="required" errorMessage="Taxes Rate field is required" />
		 <aui:validator name="max">100</aui:validator>
	</aui:input><span id="errorsmsg"/>
	<span id="taxRateValMsg" style="display:none;color:red;">Taxes Rate field is required</span>
	
	<aui:button-row>
	<div class="sub">
		<aui:button  value="Add" id="Country" name="submitCountry"/>
		</div>
		<%-- <aui:button  value="cancel" name="cancel" onClick="closewindow();"/> --%>
		<%-- <aui:button type="button" value="cancel"/> --%>
	</aui:button-row>
	<input type="button" value="Cancel" id="addproduct" onClick="closewindow();" />
</aui:form>
<aui:script use="aui-io-request-deprecated">
$("#<portlet:namespace/>taxRate").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       //display error message
       $("#errorsmsg").html("Digits Only").show().fadeOut("slow");
              return false;
   }
  });
            
            var myDivA = A.one('#countryValMsg');
            A.one('#<portlet:namespace />country').on('change', function() {
				var countryName = document.getElementById('<portlet:namespace />country').value;
				var companyId= document.getElementById('<portlet:namespace />companyId').value;
				var url = '<%=resouceURL.toString()%>';
				
				A.io.request(
					url,
					{
						//data to be sent to server
						data: {
							<portlet:namespace />param1: countryName,
							<portlet:namespace />param2: companyId,
						},
						dataType: 'json',
						
						on: {
							failure: function() {
							},
							
							success: function(event, id, obj) {
								var country = this;
								
								//JSON Data coming back from Server
								var message = country.get('responseData');
								
								if (message) {
									myDivA.show();
								}
								else {
									myDivA.hide();
								}
							}
							
						}
					}
					
				); //END of io Request
			
		},
		['aui-io-deprecated']
	);  
	
	
	 A.one('#<portlet:namespace />submitCountry').on('click', function() {
		 
		  	var a1Check  = jQuery(document.getElementById('countryValMsg')).prop('class');
		  	var country=document.getElementById('<portlet:namespace />country').value;
		  	var taxname=document.getElementById('<portlet:namespace />taxName').value;
		  	var taxrate=document.getElementById('<portlet:namespace />taxRate').value;
		  	if(country==''){
		  		document.getElementById('countryValidateMsg').style.display = 'block'; 
		  	}
		  	if(taxname==''){
		  		document.getElementById('taxNameValMsg').style.display = 'block'; 
		  	}
			if(taxrate==''){
		  		document.getElementById('taxRateValMsg').style.display = 'block'; 
		  	}
		  if((a1Check=='hide') && (country!='')&&(taxname!='')&&(taxrateNumberValidate(taxrate))) {
		  		submitForm(document.<portlet:namespace />countryForm);
		  	}
		},
		['aui-io-deprecated']			
	); 
		
		

</aui:script>
<script>
function closewindow(){
		location.reload();
	}

jQuery("#closethick").click(function () {
		window.location.reload();
});

function taxrateNumberValidate(taxrate){
	var numbers =/^(?=.)[0-9]?[0-9]?(\.[0-9][0-9]?)?$/;  
	if(taxrate.match(numbers)){
	 return true;
	}
}	
function validateTaxname(){
	var taxname=document.getElementById('<portlet:namespace />taxName').value;
  	if(taxname!=null){
  		document.getElementById('taxNameValMsg').style.display = 'none'; 
  	}
} 

function validateTaxrate(){
  	var taxrate=document.getElementById('<portlet:namespace />taxRate').value;
  	if(taxrate!=null){
  				document.getElementById('taxRateValMsg').style.display = 'none';
     }
} 
function validateCountry(){
	var country=document.getElementById('<portlet:namespace />country').value;
  	if(country!=null){
  		document.getElementById('countryValidateMsg').style.display = 'none';
  	}
}
</script>
<style>
.aui button.close, .aui button.btn.close{
display:none;
}

</style>
