<%@ include file="/html/eec/common/userview/init.jsp" %>
 <style>
   #p_p_id_filter_WAR_esquareecommerceportlet_{
   display: none;
   }
   </style>
<%

PortletSession pSession=renderRequest.getPortletSession();
String eventProductId=(String)pSession.getAttribute("EVENT_PRODUCTID",PortletSession.PORTLET_SCOPE);
String wishListUpdate = (String)pSession.getAttribute(EECConstants.WISHLIST_UPDATE_ATTRIBUTE,PortletSession.APPLICATION_SCOPE);
if(Validator.isNotNull(wishListUpdate)){
	EECUtil.updateWishList(renderRequest, Long.valueOf(wishListUpdate));
	pSession.removeAttribute(EECConstants.WISHLIST_UPDATE_ATTRIBUTE, PortletSession.APPLICATION_SCOPE);
}
String curProductId=(String)request.getParameter("curProductId");
String wishListProductIds=StringPool.BLANK;
if(themeDisplay.isSignedIn()){
	wishListProductIds=EECUtil.getwishListProductIds(themeDisplay.getUserId(),themeDisplay.getCompanyId());	
}	
if(Validator.isNotNull(eventProductId)){
	curProductId=eventProductId;
}
		String type=(String)request.getParameter("type");
		PortletURL renderURL = renderResponse.createRenderURL();
		
		long cartPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.CART_PAGE_FRIENDLY_URL).getPlid();
		PortletURL cartViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.CART_PORTLET_NAME, cartPlid,
				ActionRequest.ACTION_PHASE, null,null, LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
		
		long wishlistPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.WISHLIST_PAGE_FRIENDLY_URL).getPlid();
		PortletURL wishlistURL=EECUtil.getPortletURL(renderRequest, EECConstants.WISHLIST_PORTLET_NAME, wishlistPlid, 
									ActionRequest.ACTION_PHASE, null, null,LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
		
		long loginPlid=LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.LOGIN_PAGE_FRIENDLY_URL).getPlid();
		PortletURL loginURL=EECUtil.getPortletURL(renderRequest, EECConstants.LOGIN_PORTLET_NAME, loginPlid, PortletRequest.ACTION_PHASE, null, null, LiferayWindowState.POP_UP, LiferayPortletMode.VIEW);
		loginURL.setParameter("isPopupLogin", "1");
		
		String 	details = UserViewUtil.getDetailviewTemplateContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),Long.parseLong(curProductId),type,themeDisplay.getPathThemeImages(),renderURL,cartViewURL,wishlistURL,ctxPath,PortletPropsValues.DEFAULT_DETAIL_TEMPLATE,wishListProductIds, themeDisplay.getPortalURL(),renderRequest);
		 
	
%>
<portlet:resourceURL var="ajaxUrl" />
<aui:form method="post" name="fm">
<aui:input name="<%= EECConstants.CMD %>" type="hidden" value="<%= EECConstants.ADD %>" />
<aui:input name="<%= EECConstants.CMD1 %>" type="hidden" value="<%= EECConstants.CART_UPDATE %>" />
<aui:input name="productId" type="hidden" />
	<%=details%>
</aui:form>
<%
//pSession.removeAttribute("EVENT_PRODUCTID",pSession.PORTLET_SCOPE);
%>
<aui:script>
 function addToCartPage(curInventoryId){
 	document.<portlet:namespace />fm.action="<%= cartViewURL.toString()%>" + "&productInventoryIds=" + curInventoryId+"&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<%= EECConstants.CMD1 %>=<%= EECConstants.CART_UPDATE %>";
	submitForm(document.<portlet:namespace />fm);
	}
	
	function addToWishListPage(curInventoryId){
 	document.<portlet:namespace />fm.action="<%= wishlistURL.toString()%>" + "&productInventoryIds=" + curInventoryId+"&<%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<%= EECConstants.CMD1 %>=<%= EECConstants.WISHLIST_UPDATE %>";
	submitForm(document.<portlet:namespace />fm);
	}
	
	function wishListLogin(curProductId){
		if (!themeDisplay.isSignedIn()) {
	
					Liferay.Util.openWindow(
						{
							dialog: {
								centered: true,
								modal: true
							},
							id: 'signInDialog',
							title: Liferay.Language.get('sign-in'),
							uri: '<%= loginURL.toString() %>' + "&productInventoryIds=" + curProductId+"&<%= EECConstants.CMD %>=<%= EECConstants.WISHLIST_UPDATE %>"
						}
					);
		}
		else
		{
			JSONaddToWishListPage(curProductId);
		}
		
	} 
	if (window.opener) {
	window.close();
	window.opener.parent.location.href = "<%= HtmlUtil.escapeJS(PortalUtil.getPortalURL(renderRequest) + themeDisplay.getURLSignIn()) %>";
	}
	
	function JSONaddToWishListPage(curProductId) {  
		var url = '<%= ajaxUrl.toString() %>&curProductId='+curProductId+"&<%= EECConstants.CMD1 %>=<%= EECConstants.WISHLIST_UPDATE %>";
		jQuery.ajax({ type: 'POST', url: url, success: function() {
			Liferay.Portlet.refresh('#p_p_id<portlet:namespace />');
	    }}); 
  
     }
</aui:script>
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/userview/jquery.cookie.js"></script>
<script>
function addProducToCompare(curInventoryId,customFieldRecId) {
	var compareInventoryIds=$.cookie("compare-inventory-ids");
	if(compareInventoryIds!=null){
	var compareInventoryIds_array = compareInventoryIds.split(',');
     }
     if(compareInventoryIds!=null && compareInventoryIds_array.length >= 5 ){
		alert('<%= UnicodeLanguageUtil.get(pageContext, "compare-limit-has-been-reached") %>');
		return;
		}
			var customFieldId=$.cookie("compare-custom-field-id");
			if (customFieldId!=null && (customFieldId!=customFieldRecId)){
					  	alert('<%= UnicodeLanguageUtil.get(pageContext, "you-cant-compare-this-item-with-above-items") %>');
					return;  	
				}
				else {
				     Liferay.fire('CompareProduct', {
			       		 curInventoryId : curInventoryId
			         });
				     document.getElementById('details_added_compare').style.display='block';
				  	 document.getElementById('details_add_to_compare').style.display='none';
			   }
	  	}
</script>

  


  
