<%@ include file="/html/eec/common/userview/init.jsp" %>
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/userview/jquery.cookie.js"></script>

<portlet:resourceURL var="rurl"/>
   
<div id="loadMoreDiv">
  <%	
 PortletSession ps = renderRequest.getPortletSession();
 long productId=0;
String CurProductId = (String) ps.getAttribute("CurProductId",PortletSession.APPLICATION_SCOPE);
String wishListUpdate = (String)ps.getAttribute(EECConstants.WISHLIST_UPDATE_ATTRIBUTE,PortletSession.APPLICATION_SCOPE);
if(Validator.isNotNull(wishListUpdate)){
EECUtil.updateWishList(renderRequest, Long.valueOf(wishListUpdate));
ps.removeAttribute(EECConstants.WISHLIST_UPDATE_ATTRIBUTE, PortletSession.APPLICATION_SCOPE);
}

if(Validator.isNotNull(CurProductId)){
productId=Long.parseLong(CurProductId);
}
    int limit=PortletPropsValues.VIEWMORE_LIMIT;
int endLimit=limit;
int total=0;
List<ProductInventory> totalList=null;
 
String limits= (String)request.getParameter("LIMITS");
    String cName = request.getParameter("className");
    if(Validator.isNotNull(limits)){
    limit=Integer.parseInt(limits);
    }
    String wishListProductIds=StringPool.BLANK;
    if(themeDisplay.isSignedIn()){
    wishListProductIds=EECUtil.getwishListProductIds(themeDisplay.getUserId(),themeDisplay.getCompanyId());	
    }	
long loginPlid=LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.LOGIN_PAGE_FRIENDLY_URL).getPlid();
PortletURL loginURL=EECUtil.getPortletURL(renderRequest, EECConstants.LOGIN_PORTLET_NAME, loginPlid, PortletRequest.ACTION_PHASE, null, null, LiferayWindowState.POP_UP, LiferayPortletMode.VIEW);
loginURL.setParameter("isPopupLogin", "1"); 
    %>
    <input type="hidden" id="limitId" value="<%=limit%>"/>
    <input type="hidden" id="endLimitId" value="<%=endLimit%>"/>
      <div id="<portlet:namespace/>placeholder">
    <%
   String subcatalogIds=StringPool.BLANK;
    String selCatalogIds=StringPool.BLANK;
    String priceRange=StringPool.BLANK;
   List<ProductInventory> filterContentList=null;
   LinkedHashMap<String,String> childCatalogMap=new LinkedHashMap<String,String>();
 
   
   String catalogIds = (String) ps.getAttribute("CATALOGIDS",PortletSession.PORTLET_SCOPE);
String parentCatalogId = (String) ps.getAttribute("parentCatalogId",PortletSession.APPLICATION_SCOPE);
String productName=(String) ps.getAttribute("productName",PortletSession.APPLICATION_SCOPE);	
ps.setAttribute("BRAND_CATALOGID",catalogIds,PortletSession.PORTLET_SCOPE);
String catalogId = (String) ps.getAttribute("catalogId",PortletSession.PORTLET_SCOPE);
String cmd = (String) ps.getAttribute("CMD",PortletSession.PORTLET_SCOPE);
String slider_pricerange = (String) ps.getAttribute("SLIDER_PRICERANGE",PortletSession.PORTLET_SCOPE);
//String maximum = (String) ps.getAttribute("max",ps.PORTLET_SCOPE); 
String chkbox_priceRange = (String) ps.getAttribute("CHKBOX_PRICERANGE",PortletSession.PORTLET_SCOPE);
if(Validator.isNotNull(slider_pricerange)){
priceRange=slider_pricerange;
}else{
priceRange=chkbox_priceRange;
}
ps.setAttribute("MULTIPLE_PRICE_RANGES",priceRange,ps.PORTLET_SCOPE);
String sortByNameandPrice = (String) ps.getAttribute("SORT_BY_NAME&PRICE",ps.PORTLET_SCOPE);
if(Validator.isNotNull(parentCatalogId)) {
subcatalogIds=UserViewUtil.getChildCatalogIds(themeDisplay.getCompanyId(), Long.parseLong(parentCatalogId),new StringBuffer());
}
if(Validator.isNotNull(catalogIds)){
selCatalogIds=catalogIds;
}else if(Validator.isNotNull(cmd)){
if(cmd.equals(EECConstants.NEWLAUNCH)){
ProductInventoryLocalServiceUtil.getCatalogIdsbyCreateDate(themeDisplay.getCompanyId(),childCatalogMap);
}else if(cmd.equals(EECConstants.HOTDEAL)){
ProductInventoryLocalServiceUtil.getCatalogIdsbyDiscount(themeDisplay.getCompanyId(),childCatalogMap);
}else if(cmd.equals(EECConstants.SEARCH)){
ProductInventoryLocalServiceUtil.getCatalogIdsbySearch(themeDisplay.getCompanyId(), productName, childCatalogMap);
}else if(cmd.equals(EECConstants.SIMILAR_PRODUCTS)){
ProductInventoryLocalServiceUtil.getCatalogIdsbySimilarProduct(themeDisplay.getCompanyId(), productId, childCatalogMap);
}else if(cmd.equals(EECConstants.RELATED_PRODUCT)){
ProductInventoryLocalServiceUtil.getCatalogIdsbyRelatedProduct(themeDisplay.getCompanyId(), productId, childCatalogMap);
}	
selCatalogIds=childCatalogMap.values().toString().substring(1, childCatalogMap.values().toString().length()-1).replaceAll("\\s+","");
}
if(Validator.isNotNull(selCatalogIds)){	
  filterContentList=ProductInventoryLocalServiceUtil.getShowMoreProductList(themeDisplay.getCompanyId(),productId,productName,selCatalogIds,priceRange,sortByNameandPrice,cmd,limit);
  totalList=ProductInventoryLocalServiceUtil.getProductListByPricePange(themeDisplay.getCompanyId(),productId,productName,selCatalogIds,priceRange,sortByNameandPrice,cmd);
  total=totalList.size();
}else if(Validator.isNotNull(subcatalogIds)){	
filterContentList=ProductInventoryLocalServiceUtil.getShowMoreProductList(themeDisplay.getCompanyId(),productId,productName,subcatalogIds,priceRange,sortByNameandPrice,StringPool.BLANK,limit);
totalList=ProductInventoryLocalServiceUtil.getProductListByPricePange(themeDisplay.getCompanyId(),productId,productName,subcatalogIds,priceRange,sortByNameandPrice,StringPool.BLANK);
total=totalList.size();
} 
%>
 
   
<%

    String tabs1 = ParamUtil.getString(request, "tabs1", "ListView");
String className = PortletPropsValues.EEC_GRIDCLASS;
if(tabs1.equals("ListView")) {
className = PortletPropsValues.EEC_LISTCLASS;
}
if(Validator.isNotNull(cName)){
className=cName;
}
PortletURL viewURL = renderResponse.createRenderURL();
viewURL.setParameter("tabs1", tabs1);
viewURL.setWindowState(LiferayWindowState.NORMAL);
long cartPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.CART_PAGE_FRIENDLY_URL).getPlid();
PortletURL cartViewURL=EECUtil.getPortletURL(renderRequest, EECConstants.CART_PORTLET_NAME, cartPlid,
ActionRequest.ACTION_PHASE, null,null, LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
cartViewURL.setParameter(EECConstants.CMD , EECConstants.ADD);
cartViewURL.setParameter(EECConstants.CMD1 , EECConstants.CART_UPDATE);
long wishlistPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false, EECConstants.WISHLIST_PAGE_FRIENDLY_URL).getPlid();
PortletURL wishlistURL=EECUtil.getPortletURL(renderRequest, EECConstants.WISHLIST_PORTLET_NAME, wishlistPlid, 
ActionRequest.ACTION_PHASE, null, null,LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
long productDetailsPlid = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(), false,EECConstants.PRODUCT_DETAILS_PAGE_FRIENDLY_URL).getPlid();
PortletURL detailViewURL = EECUtil.getPortletURL(renderRequest, EECConstants.USER_VIEW_PORTLET_NAME, productDetailsPlid,
ActionRequest.ACTION_PHASE, null,"redirectToDetailsPage", LiferayWindowState.NORMAL,LiferayPortletMode.VIEW);
String content = "";
try{
if(Validator.isNotNull(filterContentList)){
content = UserViewUtil.getProductsFilterContent(filterContentList,themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),0,cmd,0,detailViewURL,cartViewURL,cartViewURL,
ctxPath,className,wishListProductIds,renderRequest);
}/* else if(Validator.isNotNull(parentCatalogId)){
content = UserViewUtil.getProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),0,cmd,Long.parseLong(parentCatalogId),detailViewURL,cartViewURL,ctxPath,className,0);
}else if(Validator.isNotNull(catalogId)){
content = UserViewUtil.getProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),0,cmd,Long.parseLong(catalogId),detailViewURL,cartViewURL,ctxPath,className,0);
}else if(Validator.isNotNull(cmd)){
content = UserViewUtil.getProductsContent(themeDisplay.getScopeGroupId(),themeDisplay.getCompanyId(),themeDisplay.getPathThemeImages(),0,cmd,0,detailViewURL,cartViewURL,ctxPath,className,productId);
} */else{
content="<span style='color: #EC4D25;font-size: 14px;font-weight: bold;'>There are No Products...</span>";
}
}catch(Exception e){
} 
String imageURl= themeDisplay.getPathThemeImages();
%>

 <portlet:renderURL var="renderURL" windowState="<%= LiferayWindowState.EXCLUSIVE.toString() %>" >
<portlet:param name="tabs1" value="<%=tabs1%>"/>
 </portlet:renderURL>
 
<style type="text/css">
#listView{
	background: red;
}
.tab {
    height: 30px !important;
}
.aui .custom-nav .nav > li > a:hover{

border-color:transparent;
}
.aui .custom-nav .nav-tabs > li > a > strong {display:none;}
.aui .custom-nav .nav-tabs > li > a {
	height:auto;
	width:33px;
	background: url('<%=themeDisplay.getPathThemeImages()+"/eec/gridview.png"%>') no-repeat scroll 0 0	transparent;
	font-size:0;
	padding-right:15px;
	border:0px;
	
}

.aui .custom-nav .nav-tabs {
padding-right:7%;
}
.aui .custom-nav .nav-tabs > li:first-child > a > strong {display:none;}

.aui .custom-nav .nav-tabs > li:first-child > a {
	height:auto;
	width:14px;
	background: url('<%=themeDisplay.getPathThemeImages()+"/eec/listview.png"%>') no-repeat scroll 0 0	transparent;
	font-size:0;
	position: relative;
}



.aui .custom-nav .nav-tabs > .active > a, .aui .custom-nav .nav-tabs > .active > a:hover, .aui .custom-nav .nav-tabs > .active > a:focus {
 box-shadow: none;
 border:none;
 background-color:transparent;
}

.aui .custom-nav .nav-tabs > .active > a{
position:relative;
top:0px;
}

.aui .custom-nav .nav-tabs li {
float:right;
  width:50px;  
}

 .aui .custom-nav .tab a {
min-width:15px;
} 

.aui .custom-nav .nav-tabs > li > a {
width:15px;
  margin-right: 0;
    padding-left: 2px;
    padding-right: 2px;
}
.aui .custom-nav .nav-tabs {
    border-bottom: none;
}


.portlet-borderless-container {
    min-height: 0;
    position: relative;
}
.aui .custom-nav .nav {
  
    margin-bottom: 1px;
}

</style>
<div class="custom-nav">
<liferay-ui:tabs names="ListView,GridView" portletURL="<%= viewURL %>"	refresh="true"	/>
</div>
<aui:form method="post" name="fm">
<aui:input name="className" type="hidden" value="<%=className%>" />
<aui:input name="<%= EECConstants.CMD %>" type="hidden" value="relatedProduct_WAR_esquareecommerceportlet" />
<aui:input name="<%= EECConstants.CMD1 %>" type="hidden" value="<%= EECConstants.CART_UPDATE %>" />

<ul class="eec-displaylist" id="myList">
<%=content%>
</ul>

<%
if((total!=limit) &&(total>limit)){
%>
<div id="loadMore"><input type="button" id="viewmorebottom"  value="View More >>" /></div> 
<%} %>
</aui:form>

</div>

</div>
<aui:script>
Liferay.on('FILTER_BY_PRICE', function(event, p_data){
   	var A = AUI(); 
       A.use('aui-io-request', function(aui) {
           A.io.request("<%= rurl %>", { 
               method : 'POST', 
               data: {<portlet:namespace />priceRange: p_data,<portlet:namespace /><%=EECConstants.CMD1%>: "CMD_CHKBOX_PRICE"},
               dataType : 'html', 
               on : { 
                   success : function() { 
                   	AUI().one("#<portlet:namespace/>placeholder").html(this.get('responseData'));
                   	Liferay.Portlet.refresh('#p_p_id<portlet:namespace/>');
                   	setTimeout(function(){setTimeout(function(){A.one('#p_p_id_filter_WAR_esquareecommerceportlet_').loadingmask.hide();},250);},250);
                   } 
               } 
           });
       });
   });
Liferay.on('FILTER_BY_SLIDER_PRICE', function(event, p_data){
   	var A = AUI(); 
       A.use('aui-io-request', function(aui) {
           A.io.request("<%= rurl %>", { 
               method : 'POST', 
               data: {<portlet:namespace />priceRange: p_data,<portlet:namespace /><%=EECConstants.CMD1%>: "CMD_SLIDER_PRICE"},
               dataType : 'html', 
               on : { 
                   success : function() { 
                   	AUI().one("#<portlet:namespace/>placeholder").html(this.get('responseData'));
                   	Liferay.Portlet.refresh('#p_p_id<portlet:namespace/>');
                   	setTimeout(function(){A.one('#p_p_id_filter_WAR_esquareecommerceportlet_').loadingmask.hide();},250);
                   } 
               } 
           });
       });
   });
function addToCartPage(curInventoryId){
document.<portlet:namespace />fm.action="<%= cartViewURL.toString()%>" + "&<portlet:namespace />productInventoryIds=" + curInventoryId+"&<portlet:namespace /><%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<portlet:namespace /><%= EECConstants.CMD1 %>=<%= EECConstants.CART_UPDATE %>";
submitForm(document.<portlet:namespace />fm);
}
function addProducToCompare(curInventoryId,customFieldRecId) {
var compareInventoryIds=$.cookie("compare-inventory-ids");
if(compareInventoryIds!=null){
var compareInventoryIds_array = compareInventoryIds.split(',');
     }
     if(compareInventoryIds!=null && compareInventoryIds_array.length >= 5 ){
     	alert('<%= UnicodeLanguageUtil.get(pageContext, "compare-limit-has-been-reached") %>');
return;
}
var customFieldId=$.cookie("compare-custom-field-id");

if (customFieldId!=null && (customFieldId!=customFieldRecId)){
 	alert('<%= UnicodeLanguageUtil.get(pageContext, "you-cant-compare-this-item-with-above-items") %>');
return;  	
}
else {
    Liferay.fire('CompareProduct', {
      curInventoryId : curInventoryId
        });
      document.getElementById('details_added_compare'+curInventoryId).style.display='block';
 	document.getElementById('details_add_to_compare'+curInventoryId).style.display='none';
  }
 	}
 	
   
   
   Liferay.on('FILTER_BY_BRAND',
           function(event,p_data) {
   	
           var A = AUI(); 
       A.use('aui-io-request', function(aui) {
           A.io.request("<%=rurl %>", { 
               method : 'POST', 
               data: {<portlet:namespace />curCatalogId: event.curCatalogId,<portlet:namespace /><%=EECConstants.CMD1%>: "BRAND"},
               on : { 
                   success : function() { 
                   	Liferay.Portlet.refresh('#p_p_id<portlet:namespace/>');
                   	setTimeout(function(){A.one('#p_p_id_filter_WAR_esquareecommerceportlet_').loadingmask.hide();},250);
                   	
                   } 
               } 
           });
       });
          
    });
    
    
    
   Liferay.on('SORT_BY_NAME&PRICE',
           function(event,p_data) {
           var A = AUI(); 
       A.use('aui-io-request', function(aui) {
           A.io.request("<%=rurl %>", { 
               method : 'POST', 
               data: {<portlet:namespace />sortByNameandPrice: event.sortByNameandPrice,<portlet:namespace /><%=EECConstants.CMD1%>: "SORTBYNAME&PRICE"},
               on : { 
                   success : function() { 
                   	Liferay.Portlet.refresh('#p_p_id<portlet:namespace/>');
                   	setTimeout(function(){A.one('#p_p_id_filter_WAR_esquareecommerceportlet_').loadingmask.hide();},250);
                   } 
               } 
           });
       });
          
    });
    
 window.onload = userviewOnLoad;
function userviewOnLoad(){
Liferay.fire('BRAND_IDS', {
brandIds : '<%=catalogIds%>'
});
Liferay.fire('SORT_BY', {
selsortBy : '<%=sortByNameandPrice%>'
});
Liferay.fire('SEL_PRICE_RANGES', {
priceRanges : '<%=priceRange%>'
});
}
function addToWishListPage(curInventoryId){
 	document.<portlet:namespace />fm.action="<%= wishlistURL.toString()%>" + "&<portlet:namespace />productInventoryIds=" + curInventoryId+"&<portlet:namespace /><%= EECConstants.CMD %>=<%= EECConstants.ADD %>"+"&<portlet:namespace /><%= EECConstants.CMD1 %>=<%= EECConstants.WISHLIST_UPDATE %>";
submitForm(document.<portlet:namespace />fm);
}

function wishListLogin(curProductId){
if (!themeDisplay.isSignedIn()) {
Liferay.Util.openWindow(
{
dialog: {
centered: true,
modal: true
},
id: 'signInDialog',
title: Liferay.Language.get('sign-in'),
uri: '<%= loginURL.toString() %>' + "&<portlet:namespace />productInventoryIds=" + curProductId+"&<portlet:namespace /><%= EECConstants.CMD %>=<%= EECConstants.WISHLIST_UPDATE %>"
}
);
}
else
{
JSONaddToWishListPage(curProductId);
}
} 
if (window.opener) {
window.close();
window.opener.parent.location.href = "<%= HtmlUtil.escapeJS(PortalUtil.getPortalURL(renderRequest) + themeDisplay.getURLSignIn()) %>";
}
function JSONaddToWishListPage(curProductId) {  
var url = '<portlet:resourceURL/>&<portlet:namespace />curProductId='+curProductId+"&<portlet:namespace /><%= EECConstants.CMD1 %>=<%= EECConstants.WISHLIST_UPDATE %>";
jQuery.ajax({ type: 'POST', url: url, success: function() {
Liferay.Portlet.refresh('#p_p_id<portlet:namespace />');
   }}); 
  
     }


</aui:script>


 <aui:script>
 AUI().ready("aui-io-request",function(A){
A.one('#loadMore').on('click', function() {
             var limit= document.getElementById('limitId').value;
           	var endlimit= document.getElementById('endLimitId').value;
           	var className= document.getElementById('<portlet:namespace/>className').value;
            endlimit=parseInt(endlimit)+parseInt(limit);
            document.getElementById('limitId').value=endlimit;
         var url = "<%=renderURL.toString()%>"; 
           url = url+"&<portlet:namespace />LIMITS="+ endlimit+"&<portlet:namespace />className="+ className;
            jQuery('#loadMoreDiv').load(url);
});
 });
</aui:script> 

<style>
#listviewentire{
width:99%;
}
#discount
{
margin-left:35px !important;
}

</style>