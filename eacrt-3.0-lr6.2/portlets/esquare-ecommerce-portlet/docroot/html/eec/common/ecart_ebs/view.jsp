<%@page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ include file="/html/eec/common/init.jsp"%>
				 
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/jquery-1.10.1.min.js"></script>

<portlet:actionURL var="payEBS">
	<portlet:param name="<%= EECConstants.CMD %>" value="<%= EECConstants.ADD_EBS %>" />
</portlet:actionURL>

<portlet:resourceURL var="resouceURL" />

<%
	String packageType = ParamUtil.getString(request, "title");
%>

<c:if test='<%= SessionErrors.contains(request, "InvaliData") %>'>
	<span class="portlet-msg-info">
		Please enter valid Data
	</span>
</c:if>

<c:if test='<%= SessionMessages.contains(request, EECConstants.PAYMENT_PENDING) %>'>
	<span class="portlet-msg-info">
		Payment pending
	</span>
</c:if>

<c:if test='<%= SessionMessages.contains(request, "error-in-create") %>'>
	<span class="portlet-msg-info">
		UR Instance will create once payment is success
	</span>
</c:if>

<aui:form action="<%=payEBS.toString() %>" name="fm" id="fm">
 	<aui:input type="hidden" name="packageType" value="<%= packageType%>"/>
 	<aui:input type="hidden" name="ecartAutoId" value=""/>
	<aui:fieldset>
		<aui:column>
			<div id="<portlet:namespace />instanceDivId" style="display:block">
				<label><h6>Your Store Name</h6></label>
				<aui:input type="text" name="newStoreName" inlineField="true"  label="" style="width:180px" >
					<aui:validator name="required"></aui:validator>
					<aui:validator name="rangeLength">
					[4,12]
					</aui:validator>
				</aui:input>
			</div>
			<div id="insNA" class="hide">
				<span style="color:red;">StoreName is already Exist</span>
			</div>
			
			<aui:input name="email" label="E-mail" type="text" value="" style="width:180px">
				<aui:validator name="required"></aui:validator>
				<aui:validator name="email" />
			</aui:input>
			<aui:input name="password" type="password" value="" style="width:180px">
				<aui:validator name="required"></aui:validator>
				<aui:validator name="rangeLength">
				[5,12]
				</aui:validator>
			</aui:input>
		</aui:column>
	</aui:fieldset>
	<aui:button-row>
		<aui:button type="submit" name="submit" value="Submit"/>
	</aui:button-row>
</aui:form>

<script type="text/javascript">
	jQuery(document).ready(function(){
	    var text = '';
	    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

	    for(var i=0; i < 12; i++)
	    {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }
	    var test = text;
	    document.getElementById('<portlet:namespace />ecartAutoId').value = test;
	});
</script>

<aui:script use="aui-io-request-deprecated">
	AUI().use( 'aui-tooltip-deprecated','aui-loading-mask-deprecated', function(A){
	var myDivNA = A.one('#insNA');
	A.one('#<portlet:namespace />newStoreName').on('blur', function() {
		var instName = document.getElementById('<portlet:namespace />newStoreName').value;
		var url = '<%=resouceURL.toString()%>';
		A.io.request(
			url,
			{
				//data to be sent to server
				data: {
					<portlet:namespace />param1: instName,
				},
				dataType: 'json',
				on: {
					failure: function() {
					},
					success: function(event, id, obj) {
						var instance = this;
						var message = instance.get('responseData');
						
						if (message) {
							document.getElementById('<portlet:namespace />newStoreName').value = "";
							myDivNA.show();
						}
						else {
							myDivNA.hide();
						}
					}
				}
			}); //END of io Request
		},['aui-io-deprecated']);  //End of Provide
	});
</aui:script>