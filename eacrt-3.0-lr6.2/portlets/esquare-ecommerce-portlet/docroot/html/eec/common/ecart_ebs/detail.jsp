<%@ include file="/html/eec/common/init.jsp"%>

<%@page import="com.liferay.portal.service.AddressLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Phone"%>
<%@page import="com.liferay.portal.model.Address"%>
<%@page import="com.liferay.portal.service.PhoneLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalService"%>
<%@page import="com.esquare.ecommerce.util.EBSUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="java.util.HashMap"%>

<script src="<%=request.getContextPath()%>/js/eec/common/template/jquery.min.js"></script>
<script src="/html/js/liferay/service.js" type="text/javascript"> </script>
<%
	themeDisplay.setIncludeServiceJs(true);
%>

<portlet:actionURL var="returnURLEBS">
	<portlet:param name="<%= EECConstants.CMD %>" value="<%= EECConstants.PAY_EBS %>" />
</portlet:actionURL>

<%
String userId = "";
	PortletSession ps = renderRequest.getPortletSession();
	HashMap<String, String> map = (HashMap<String, String>) ps.getAttribute("PACKAGE_DETAILS");
	String webId = map.get("webId");
	String email = map.get("email");
	String ecartAutoId = map.get("ecartAutoId");
	String packageType = map.get("packageType");
	long userIds = 0l;
	User userDetail = null;
	String reference_no = "";
	String userName = "";
	String address = "";
	String postal_code = "";
	String city = "";
	String streetName = "";
	String street = "";
	String phone =  "";
	long countryId = 0l;
	long regionId = 0l;
	
	if(Validator.isNotNull(userId)){
		userDetail = UserLocalServiceUtil.getUser(Long.parseLong(userId));
		userIds = userDetail.getUserId();
		userName = userDetail.getFullName();
	
		List<Address> address1 = AddressLocalServiceUtil.getAddresses(userDetail.getCompanyId(),"com.liferay.portal.model.Contact",userDetail.getContactId());
		List<Phone> phones = PhoneLocalServiceUtil.getPhones(userDetail.getCompanyId(),"com.liferay.portal.model.Contact",userDetail.getContactId());
		
		if(!address1.isEmpty() && address1!=null){
			street = address1.get(0).getStreet1();
			postal_code = address1.get(0).getZip();
			city = address1.get(0).getCity();
			countryId = address1.get(0).getCountryId();
			regionId = address1.get(0).getRegionId();
		}
		if(!phones.isEmpty() && phones!=null){
			phone = phones.get(0).getNumber();
		}
	 }
	String md5HashData = PortletPropsValues.EBS_SECRET_KEY;
	String paymentMode = PortletPropsValues.EBS_PAYMENT_MODE;
	String accountNumber = PortletPropsValues.EBS_ACCOUNT_ID;
	
	String amount = "";
	if(packageType.equalsIgnoreCase(EECConstants.BASIC)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_BASIC_PACKAGE_CHARGES);
	}else if(packageType.equalsIgnoreCase(EECConstants.STANDARD)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_STANDARD_PACKAGE_CHARGES);
	}else if(packageType.equalsIgnoreCase(EECConstants.PREMIUM)){
		amount = String.valueOf(PortletPropsValues.MONTHLY_PREMIUM_PACKAGE_CHARGES);
	}else if(packageType.equalsIgnoreCase(EECConstants.ENTERPRISE)){
		amount = String.valueOf(PortletPropsValues.ENTERPRISE_PACKAGE_PERMISSION_VALUE);
	}
	
	String returnURL=returnURLEBS.toString()+"&DR={DR}";
	
	md5HashData += '|' + accountNumber + '|' + amount + '|'+ ecartAutoId + '|' + returnURL + '|' + paymentMode;
%>

<aui:form  action="https://secure.ebs.in/pg/ma/sale/pay" name="fm" method="post" id="fm">
	<input name="account_id" type="hidden" value="<%=accountNumber%>">
	<input name="return_url" type="hidden" size="60" value="<%=returnURL%>">
	<input name="mode" type="hidden" size="60" value="<%=paymentMode%>">
	<input name="reference_no" type="hidden" value='<%=ecartAutoId %>'>	
	<input name="secure_hash" type="hidden" size="60" value="<%=EBSUtil.md5(md5HashData)%>"/>	
	<input name="description" type="hidden" value="<%= packageType%>"> 
	<input name="country" type="hidden" id="country">
	<input name="state" type="hidden" id="state">
	
	<table><tr><td>
		WebId :</td><td><input name="webId" type="text" value="<%=webId %>" readOnly="true"/></td></tr>
		<tr><td>
		Email :</td><td><input name="email" type="text" value='<%=email %>' readOnly="true" > </td></tr>
		<tr><td>
		Name  : </td><td><input name="name" type="text" class="field-required" maxlength="40" value='<%= userName %>'></td></tr>
		<tr><td>
		Phone No : </td><td><input name="phone" class="field-required field-digits" type="text" maxlength="20" value="<%= phone %>"> </td></tr>
		<tr><td>
		Amount   : </td><td><input name="amount" type="text" class="field-required" value="<%=amount%>" readOnly="true"/> </td></tr>
		<tr><td>
		Street : </td><td><input name="address" type="text" class="field-required" maxlength="150"	value="<%= street %>"> </td></tr>
		<tr><td>
		Zip code : </td><td><input name="postal_code" type="text" class="field-required field-digits" maxlength="6" value="<%= postal_code%>"></td></tr>
		<tr><td>Counry :</td><td><aui:select label="" name="countryName" /></td> </tr>
		<tr><td>Region :</td> <td><aui:select label="" name="stateName" /></td></tr>
		<tr><td>
			City : </td><td><input name="city" type="text" class="field-required" maxlength="55" value="<%= city %>">
	</table>
	
	<aui:button-row>
		<aui:button type="submit" name="submit" value="Pay Via EBS"/>
	</aui:button-row>
</aui:form>

<aui:script use="liferay-dynamic-select,aui-io-request-deprecated">
  new Liferay.DynamicSelect(
      [
          {
              select: '<portlet:namespace />countryName',
              selectData: Liferay.Address.getCountries,
              selectDesc: 'name',
              selectId: 'countryId',
              selectVal: '108'
          },
          {
              select: '<portlet:namespace />stateName',
              selectData: Liferay.Address.getRegions,
              selectDesc: 'name',
              selectId: 'regionId',
              selectVal: '3856'
          }
      ]
  );
</aui:script>

<style>
.form-validator-message {
	position: relative;
	margin-bottom: -25px;
	margin-left: 155px;
	width: 62%;
}

.field-label{
	display: none;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function(){
	 $("#<portlet:namespace />submit").click(function(){
	       var country = $( "#<portlet:namespace />countryName" ).val();
	       var state = $( "#<portlet:namespace />stateName" ).val();
	       $('#country').val(country);
	       $('#state').val(state);
	    }); 
});
</script>
