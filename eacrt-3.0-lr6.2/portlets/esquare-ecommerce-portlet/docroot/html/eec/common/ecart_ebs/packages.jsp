<%@ include file="/html/eec/common/init.jsp"%>
				 
<script type="text/javascript" src="<%=ctxPath %>/js/eec/common/jquery-1.10.1.min.js"></script>

<portlet:renderURL var="packageURL">
	<portlet:param name="jspPage" value="/html/eec/common/ecart_ebs/view.jsp"/>
</portlet:renderURL>
<span id="pckhead"><h4>Select Your Package</h4></span></br>
<div class="pckimg">
	<img src='<%=request.getContextPath() %>/images/eec/common/instance/basic.jpg' alt="<%= PortletPropsValues.PACKAGE_TYPE_BASIC %>" title="<%= PortletPropsValues.PACKAGE_TYPE_BASIC %>" > 
	<img src='<%=request.getContextPath() %>/images/eec/common/instance/standard.jpg' alt="<%= PortletPropsValues.PACKAGE_TYPE_STANDARD %>" title="<%= PortletPropsValues.PACKAGE_TYPE_STANDARD %>" > 
	<img src='<%=request.getContextPath() %>/images/eec/common/instance/premium.jpg' alt="<%= PortletPropsValues.PACKAGE_TYPE_PREMIUM %>" title="<%= PortletPropsValues.PACKAGE_TYPE_PREMIUM %>" > 
	<img src='<%=request.getContextPath() %>/images/eec/common/instance/special-offer.jpg' alt="<%= PortletPropsValues.PACKAGE_TYPE_ENTERPRISE %>" title="<%= PortletPropsValues.PACKAGE_TYPE_ENTERPRISE %>" > 
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
	$("img").click(function() {
	    $(this).css('border', "solid 2px lightblue");  
	    var title = $(this).attr('title');
	    var url='<%= packageURL.toString()%>';
        window.location.href = url+"&title="+title;
	  });
 });
</script>

<style>
div.pckimg img{
    height: 136px;
    width: 166px;
}

#pckhead{
	color: #CC3366;
    font-style: oblique;
    text-decoration: -moz-anchor-decoration;
}
</style>
