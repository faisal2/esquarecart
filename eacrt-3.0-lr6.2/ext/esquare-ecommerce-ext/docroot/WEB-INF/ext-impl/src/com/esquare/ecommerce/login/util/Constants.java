/**
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.login.util;

import java.lang.String;

/**
 * @author mohammad azharuddin
 */
public class Constants {
	public static final String TWITTER_ID_LOGIN = "TWITTER_ID_LOGIN";

	public static final String TWITTER_LOGIN_PENDING = "TWITTER_LOGIN_PENDING";
	
	public static final String LINKED_IN_LOGIN = "LINKED_IN_LOGIN";
	
}