/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
/*

package com.liferay.portal.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.ListType;
import com.liferay.portal.model.User;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.struts.ActionConstants;
import com.liferay.portal.util.PortalUtil;

 *//**
 * @author Brian Wing Shun Chan
 */
/*
public class UpdateTermsOfUseAction extends Action {

@Override
public ActionForward execute(
		ActionMapping mapping, ActionForm form, HttpServletRequest request,
		HttpServletResponse response)
	throws Exception {
	
	
	long userId = PortalUtil.getUserId(request);

	String firstName = ParamUtil.getString(request, "firstName");
	String lastName = ParamUtil.getString(request, "lastName");
	String street = ParamUtil.getString(request, "street");
	String zip = ParamUtil.getString(request, "zip");
	long countryId = ParamUtil.getLong(request, "countryId");
	String city = ParamUtil.getString(request, "city");
	String mobile = ParamUtil.getString(request, "mobile");
	long stateId = ParamUtil.getLong(request, "regionId");
	ServiceContext serviceContext = ServiceContextFactory
			.getInstance(request);
	User user = PortalUtil.getUser(request);

	user.setFirstName(firstName);
	user.setLastName(lastName);

	List<ListType> listTypeAddress = ListTypeServiceUtil
			.getListTypes("com.liferay.portal.model.Contact.address");
	ListType addressType = listTypeAddress.get(0);

	List<ListType> listTypePhone = ListTypeServiceUtil
			.getListTypes("com.liferay.portal.model.Contact.phone");
	ListType phoneType = listTypePhone.get(2);

	AddressLocalServiceUtil.addAddress(userId, Contact.class.getName(),
			user.getContactId(), street, StringPool.BLANK,
			StringPool.BLANK, city, zip, stateId, countryId,
			addressType.getListTypeId(), false, true, serviceContext);

	PhoneLocalServiceUtil.addPhone(userId, Contact.class.getName(),
			user.getContactId(), mobile, StringPool.BLANK,
			phoneType.getListTypeId(), false, serviceContext);

	user.setPasswordReset(false);
	UserLocalServiceUtil.updateUser(user);

	UserServiceUtil.updateAgreedToTermsOfUse(userId, true);

	return mapping.findForward(ActionConstants.COMMON_REFERER_JSP);
	

}

}


 */

/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.portal.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.ListType;
import com.liferay.portal.model.User;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.struts.ActionConstants;
import com.liferay.portal.util.PortalUtil;

/**
 * @author Brian Wing Shun Chan
 */
public class UpdateTermsOfUseAction extends Action {

	@Override
	public ActionForward execute(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		long userId = PortalUtil.getUserId(request);

		String firstName = ParamUtil.getString(request, "firstName");
		String lastName = ParamUtil.getString(request, "lastName");
		String street = ParamUtil.getString(request, "street");
		String zip = ParamUtil.getString(request, "zip");
		long countryId = ParamUtil.getLong(request, "countryId");
		String city = ParamUtil.getString(request, "city");
		String mobile = ParamUtil.getString(request, "mobile");
		long stateId = ParamUtil.getLong(request, "regionId");
		ServiceContext serviceContext = ServiceContextFactory
				.getInstance(request);
		User user = PortalUtil.getUser(request);

		user.setFirstName(firstName);
		user.setLastName(lastName);

		List<ListType> listTypeAddress = ListTypeServiceUtil
				.getListTypes("com.liferay.portal.model.Contact.address");
		ListType addressType = listTypeAddress.get(0);

		List<ListType> listTypePhone = ListTypeServiceUtil
				.getListTypes("com.liferay.portal.model.Contact.phone");
		ListType phoneType = listTypePhone.get(2);

		AddressLocalServiceUtil.addAddress(userId, Contact.class.getName(),
				user.getContactId(), street, StringPool.BLANK,
				StringPool.BLANK, city, zip, stateId, countryId,
				addressType.getListTypeId(), false, true, serviceContext);

		PhoneLocalServiceUtil.addPhone(userId, Contact.class.getName(),
				user.getContactId(), mobile, StringPool.BLANK,
				phoneType.getListTypeId(), false, serviceContext);

		user.setPasswordReset(false);
		UserLocalServiceUtil.updateUser(user);

		UserServiceUtil.updateAgreedToTermsOfUse(userId, true);

		return actionMapping.findForward(ActionConstants.COMMON_REFERER_JSP);
	}

}