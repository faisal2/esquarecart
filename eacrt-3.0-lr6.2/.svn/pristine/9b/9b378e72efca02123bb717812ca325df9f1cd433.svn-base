/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.Catalog;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Catalog in entity cache.
 *
 * @author Esquare
 * @see Catalog
 * @generated
 */
public class CatalogCacheModel implements CacheModel<Catalog>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{catalogId=");
		sb.append(catalogId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", parentCatalogId=");
		sb.append(parentCatalogId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", folderId=");
		sb.append(folderId);
		sb.append(", showNavigation=");
		sb.append(showNavigation);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Catalog toEntityModel() {
		CatalogImpl catalogImpl = new CatalogImpl();

		catalogImpl.setCatalogId(catalogId);
		catalogImpl.setGroupId(groupId);
		catalogImpl.setCompanyId(companyId);
		catalogImpl.setUserId(userId);

		if (createDate == Long.MIN_VALUE) {
			catalogImpl.setCreateDate(null);
		}
		else {
			catalogImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			catalogImpl.setModifiedDate(null);
		}
		else {
			catalogImpl.setModifiedDate(new Date(modifiedDate));
		}

		catalogImpl.setParentCatalogId(parentCatalogId);

		if (name == null) {
			catalogImpl.setName(StringPool.BLANK);
		}
		else {
			catalogImpl.setName(name);
		}

		if (description == null) {
			catalogImpl.setDescription(StringPool.BLANK);
		}
		else {
			catalogImpl.setDescription(description);
		}

		catalogImpl.setFolderId(folderId);
		catalogImpl.setShowNavigation(showNavigation);

		catalogImpl.resetOriginalValues();

		return catalogImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		catalogId = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		parentCatalogId = objectInput.readLong();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
		folderId = objectInput.readLong();
		showNavigation = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(catalogId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(parentCatalogId);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(folderId);
		objectOutput.writeBoolean(showNavigation);
	}

	public long catalogId;
	public long groupId;
	public long companyId;
	public long userId;
	public long createDate;
	public long modifiedDate;
	public long parentCatalogId;
	public String name;
	public String description;
	public long folderId;
	public boolean showNavigation;
}