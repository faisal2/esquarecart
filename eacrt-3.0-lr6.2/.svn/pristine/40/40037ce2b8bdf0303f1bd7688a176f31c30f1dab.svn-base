/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.model.impl;

import com.esquare.ecommerce.model.CancelledOrderItems;
import com.esquare.ecommerce.model.CancelledOrderItemsModel;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the CancelledOrderItems service. Represents a row in the &quot;EEC_CancelledOrderItems&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.esquare.ecommerce.model.CancelledOrderItemsModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CancelledOrderItemsImpl}.
 * </p>
 *
 * @author Esquare
 * @see CancelledOrderItemsImpl
 * @see com.esquare.ecommerce.model.CancelledOrderItems
 * @see com.esquare.ecommerce.model.CancelledOrderItemsModel
 * @generated
 */
public class CancelledOrderItemsModelImpl extends BaseModelImpl<CancelledOrderItems>
	implements CancelledOrderItemsModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a cancelled order items model instance should use the {@link com.esquare.ecommerce.model.CancelledOrderItems} interface instead.
	 */
	public static final String TABLE_NAME = "EEC_CancelledOrderItems";
	public static final Object[][] TABLE_COLUMNS = {
			{ "orderItemId", Types.BIGINT },
			{ "companyId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "orderId", Types.BIGINT },
			{ "price", Types.DOUBLE },
			{ "quantity", Types.INTEGER },
			{ "cancelledDate", Types.TIMESTAMP }
		};
	public static final String TABLE_SQL_CREATE = "create table EEC_CancelledOrderItems (orderItemId LONG not null primary key,companyId LONG,userId LONG,orderId LONG,price DOUBLE,quantity INTEGER,cancelledDate DATE null)";
	public static final String TABLE_SQL_DROP = "drop table EEC_CancelledOrderItems";
	public static final String ORDER_BY_JPQL = " ORDER BY cancelledOrderItems.cancelledDate DESC";
	public static final String ORDER_BY_SQL = " ORDER BY EEC_CancelledOrderItems.cancelledDate DESC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.esquare.ecommerce.model.CancelledOrderItems"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.esquare.ecommerce.model.CancelledOrderItems"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.com.esquare.ecommerce.model.CancelledOrderItems"),
			true);
	public static long COMPANYID_COLUMN_BITMASK = 1L;
	public static long USERID_COLUMN_BITMASK = 2L;
	public static long CANCELLEDDATE_COLUMN_BITMASK = 4L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.esquare.ecommerce.model.CancelledOrderItems"));

	public CancelledOrderItemsModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _orderItemId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setOrderItemId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _orderItemId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return CancelledOrderItems.class;
	}

	@Override
	public String getModelClassName() {
		return CancelledOrderItems.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("orderItemId", getOrderItemId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("orderId", getOrderId());
		attributes.put("price", getPrice());
		attributes.put("quantity", getQuantity());
		attributes.put("cancelledDate", getCancelledDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long orderItemId = (Long)attributes.get("orderItemId");

		if (orderItemId != null) {
			setOrderItemId(orderItemId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long orderId = (Long)attributes.get("orderId");

		if (orderId != null) {
			setOrderId(orderId);
		}

		Double price = (Double)attributes.get("price");

		if (price != null) {
			setPrice(price);
		}

		Integer quantity = (Integer)attributes.get("quantity");

		if (quantity != null) {
			setQuantity(quantity);
		}

		Date cancelledDate = (Date)attributes.get("cancelledDate");

		if (cancelledDate != null) {
			setCancelledDate(cancelledDate);
		}
	}

	@Override
	public long getOrderItemId() {
		return _orderItemId;
	}

	@Override
	public void setOrderItemId(long orderItemId) {
		_orderItemId = orderItemId;
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_columnBitmask |= COMPANYID_COLUMN_BITMASK;

		if (!_setOriginalCompanyId) {
			_setOriginalCompanyId = true;

			_originalCompanyId = _companyId;
		}

		_companyId = companyId;
	}

	public long getOriginalCompanyId() {
		return _originalCompanyId;
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	@Override
	public long getOrderId() {
		return _orderId;
	}

	@Override
	public void setOrderId(long orderId) {
		_orderId = orderId;
	}

	@Override
	public double getPrice() {
		return _price;
	}

	@Override
	public void setPrice(double price) {
		_price = price;
	}

	@Override
	public int getQuantity() {
		return _quantity;
	}

	@Override
	public void setQuantity(int quantity) {
		_quantity = quantity;
	}

	@Override
	public Date getCancelledDate() {
		return _cancelledDate;
	}

	@Override
	public void setCancelledDate(Date cancelledDate) {
		_columnBitmask = -1L;

		_cancelledDate = cancelledDate;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(getCompanyId(),
			CancelledOrderItems.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public CancelledOrderItems toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (CancelledOrderItems)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		CancelledOrderItemsImpl cancelledOrderItemsImpl = new CancelledOrderItemsImpl();

		cancelledOrderItemsImpl.setOrderItemId(getOrderItemId());
		cancelledOrderItemsImpl.setCompanyId(getCompanyId());
		cancelledOrderItemsImpl.setUserId(getUserId());
		cancelledOrderItemsImpl.setOrderId(getOrderId());
		cancelledOrderItemsImpl.setPrice(getPrice());
		cancelledOrderItemsImpl.setQuantity(getQuantity());
		cancelledOrderItemsImpl.setCancelledDate(getCancelledDate());

		cancelledOrderItemsImpl.resetOriginalValues();

		return cancelledOrderItemsImpl;
	}

	@Override
	public int compareTo(CancelledOrderItems cancelledOrderItems) {
		int value = 0;

		value = DateUtil.compareTo(getCancelledDate(),
				cancelledOrderItems.getCancelledDate());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CancelledOrderItems)) {
			return false;
		}

		CancelledOrderItems cancelledOrderItems = (CancelledOrderItems)obj;

		long primaryKey = cancelledOrderItems.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		CancelledOrderItemsModelImpl cancelledOrderItemsModelImpl = this;

		cancelledOrderItemsModelImpl._originalCompanyId = cancelledOrderItemsModelImpl._companyId;

		cancelledOrderItemsModelImpl._setOriginalCompanyId = false;

		cancelledOrderItemsModelImpl._originalUserId = cancelledOrderItemsModelImpl._userId;

		cancelledOrderItemsModelImpl._setOriginalUserId = false;

		cancelledOrderItemsModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<CancelledOrderItems> toCacheModel() {
		CancelledOrderItemsCacheModel cancelledOrderItemsCacheModel = new CancelledOrderItemsCacheModel();

		cancelledOrderItemsCacheModel.orderItemId = getOrderItemId();

		cancelledOrderItemsCacheModel.companyId = getCompanyId();

		cancelledOrderItemsCacheModel.userId = getUserId();

		cancelledOrderItemsCacheModel.orderId = getOrderId();

		cancelledOrderItemsCacheModel.price = getPrice();

		cancelledOrderItemsCacheModel.quantity = getQuantity();

		Date cancelledDate = getCancelledDate();

		if (cancelledDate != null) {
			cancelledOrderItemsCacheModel.cancelledDate = cancelledDate.getTime();
		}
		else {
			cancelledOrderItemsCacheModel.cancelledDate = Long.MIN_VALUE;
		}

		return cancelledOrderItemsCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{orderItemId=");
		sb.append(getOrderItemId());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", orderId=");
		sb.append(getOrderId());
		sb.append(", price=");
		sb.append(getPrice());
		sb.append(", quantity=");
		sb.append(getQuantity());
		sb.append(", cancelledDate=");
		sb.append(getCancelledDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append("com.esquare.ecommerce.model.CancelledOrderItems");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>orderItemId</column-name><column-value><![CDATA[");
		sb.append(getOrderItemId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>orderId</column-name><column-value><![CDATA[");
		sb.append(getOrderId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>price</column-name><column-value><![CDATA[");
		sb.append(getPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantity</column-name><column-value><![CDATA[");
		sb.append(getQuantity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cancelledDate</column-name><column-value><![CDATA[");
		sb.append(getCancelledDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = CancelledOrderItems.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			CancelledOrderItems.class
		};
	private long _orderItemId;
	private long _companyId;
	private long _originalCompanyId;
	private boolean _setOriginalCompanyId;
	private long _userId;
	private String _userUuid;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private long _orderId;
	private double _price;
	private int _quantity;
	private Date _cancelledDate;
	private long _columnBitmask;
	private CancelledOrderItems _escapedModel;
}