/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Date;

import com.esquare.ecommerce.model.EcartEbsPayment;
import com.esquare.ecommerce.service.base.EcartEbsPaymentLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the ecart ebs payment local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.EcartEbsPaymentLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.EcartEbsPaymentLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.EcartEbsPaymentLocalServiceUtil
 */
public class EcartEbsPaymentLocalServiceImpl extends
		EcartEbsPaymentLocalServiceBaseImpl {
	public EcartEbsPayment addEcartEbsPayment(long companyId,
			String ecartAutoId, long userId, long responseCode,
			String responseMessage, Date dateCreated, long paymentId,
			long paymentMethod, String amount, String isFlagged,
			long transactionId) throws SystemException {

		long ebsTrackId = counterLocalService.increment();

		EcartEbsPayment ecartEbsPayment = ecartEbsPaymentPersistence
				.create(ebsTrackId);

		ecartEbsPayment.setCompanyId(companyId);
		ecartEbsPayment.setEcartAutoId(ecartAutoId);
		ecartEbsPayment.setUserId(userId);
		ecartEbsPayment.setResponseCode(responseCode);
		ecartEbsPayment.setResponseMessage(responseMessage);
		ecartEbsPayment.setPaidDate(dateCreated);
		ecartEbsPayment.setPaymentId(paymentId);
		ecartEbsPayment.setPaymentMethod(paymentMethod);
		ecartEbsPayment.setAmount(amount);
		ecartEbsPayment.setIsFlagged(isFlagged);
		ecartEbsPayment.setTransactionId(transactionId);

		ecartEbsPaymentPersistence.update(ecartEbsPayment);

		return ecartEbsPayment;
	}
}