/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.esquare.ecommerce.service.impl;

import java.util.Map;

import com.esquare.ecommerce.NoSuchCouponException;
import com.esquare.ecommerce.WalletLimitException;
import com.esquare.ecommerce.model.CartItem;
import com.esquare.ecommerce.model.Coupon;
import com.esquare.ecommerce.model.MyWallet;
import com.esquare.ecommerce.model.Order;
import com.esquare.ecommerce.model.OrderItem;
import com.esquare.ecommerce.service.OrderItemLocalService;
import com.esquare.ecommerce.service.OrderLocalService;
import com.esquare.ecommerce.service.OrderLocalServiceUtil;
import com.esquare.ecommerce.service.base.MyWalletLocalServiceBaseImpl;
import com.esquare.ecommerce.util.EECConstants;
import com.esquare.ecommerce.util.EECUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portlet.shopping.AmazonException;

/**
 * The implementation of the my wallet local service.
 * 
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link com.esquare.ecommerce.service.MyWalletLocalService} interface.
 * 
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 * 
 * @author Esquare
 * @see com.esquare.ecommerce.service.base.MyWalletLocalServiceBaseImpl
 * @see com.esquare.ecommerce.service.MyWalletLocalServiceUtil
 */
public class MyWalletLocalServiceImpl extends MyWalletLocalServiceBaseImpl {

	public MyWallet updateMyWallet(long companyId, long userId, double amount)
			throws SystemException {

		MyWallet myWallet = myWalletPersistence.fetchByPrimaryKey(userId);
		if (Validator.isNull(myWallet))
			myWallet = myWalletPersistence.create(userId);
		myWallet.setCompanyId(companyId);
		myWallet.setAmount(myWallet.getAmount() + amount);

		myWalletPersistence.update(myWallet);

		return myWallet;
	}

	public void updateMyWallet(long companyId, long userId, long orderId,
			double totalOrderAmount, double remainingOrderActualSubtotal,
			Map<CartItem, Integer> cancelledOrderItems, String countryId,
			String stateId) throws SystemException, PortalException,
			DocumentException {
		double couponDiscount = 0;
		Coupon coupon = null;
		Order order = orderLocalService.getOrder(orderId);
		try {
			coupon = couponPersistence.findByCouponCodeVersion(
					order.getCouponCodes(), order.getCouponVersion());
			couponDiscount = EECUtil.calculateCouponDiscountCancelledItem(
					companyId, userId, orderId, remainingOrderActualSubtotal,
					cancelledOrderItems, coupon, countryId, stateId);
		} catch (NoSuchCouponException suchCouponException) {
		}
		double shippingAmount = EECUtil.calculateRefundShippingAmount(companyId,
				cancelledOrderItems, countryId, stateId,orderId);
		double taxAmount = EECUtil.calculateRefundTaxAmount(companyId, cancelledOrderItems, countryId, stateId,orderId);
		double actualSubtotal = EECUtil
				.calculateActualSubtotal(cancelledOrderItems , orderId);
		double totalAmountCancelled = actualSubtotal + taxAmount
				+ shippingAmount - couponDiscount;
		if (totalAmountCancelled < 0) {
			throw new WalletLimitException();
		}
		if (Validator.isNotNull(coupon)
				&& remainingOrderActualSubtotal < coupon.getMinOrder())
			orderLocalService.deleteOrderCoupon(orderId);
		double remainingOrderAmount = totalOrderAmount - totalAmountCancelled;
		
			if (!order.getPaymentType().equalsIgnoreCase(EECConstants.COD)) {
			
			myWalletLocalService.updateMyWallet(companyId, userId,
					totalAmountCancelled);
						
			orderLocalService.updateCancelledOrder(orderId, taxAmount,
					shippingAmount, couponDiscount, totalAmountCancelled);
		} else if (order.getPaymentType().equalsIgnoreCase(EECConstants.COD)
				&& order.getWalletAmount() > 0) {
			if (order.getWalletAmount() > remainingOrderAmount) {
				myWalletLocalService.updateMyWallet(companyId, userId,
						order.getWalletAmount() - remainingOrderAmount);
				orderLocalService.updateCancelledOrder(orderId, taxAmount,
						shippingAmount, couponDiscount, order.getWalletAmount()
								- remainingOrderAmount);
			} else {
				orderLocalService.updateCancelledOrder(orderId, taxAmount,
						shippingAmount, couponDiscount, 0);
			}

		} else if (order.getPaymentType().equalsIgnoreCase(EECConstants.COD)) {
			orderLocalService.updateCancelledOrder(orderId, taxAmount,
					shippingAmount, couponDiscount, 0);
		}
	}
}